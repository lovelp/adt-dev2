({
    getDetails : function(component, event, helper,cNum,sNum,jNum) {
        console.log('Inside method3');
        var isValidated = false;
        if($A.util.isEmpty(jNum)){
            if($A.util.isEmpty(cNum) || $A.util.isEmpty(sNum)){
                component.set("v.errMsgCntrl",true);
                component.set("v.ErrorDisplay",'Please Enter Customer Number and Site Number OR Job Number ');
                component.set("v.lstAcc", '');
            }else{
                isValidated = true;
                component.set("v.errMsgCntrl",false);
            }
        }else{
            isValidated = true;   
        }
        console.log('Inside method4'+isValidated);
        if(isValidated){
            var getAccountDetails;
            getAccountDetails = component.get("c.searchAccount");
            getAccountDetails.setParams({
                "jobNumber": jNum,
                "custNumber":cNum,
                "siteNumber":sNum
            });
            getAccountDetails.setCallback(this, function(response){
                component.set("v.loadingSpinner",false);
                var state = response.getState();
                if (state == "SUCCESS") {
                    if($A.util.isEmpty(response.getReturnValue())){
                        component.set("v.errMsgCntrl",true);
                        component.set("v.showTable",false);
                        component.set("v.ErrorDisplay",'No Account Found');
                        component.set("v.lstAcc", response.getReturnValue());
                    }else{
                        component.set("v.showTable",true);
                        component.set("v.errMsgCntrl",false);
                        component.set("v.lstAcc",response.getReturnValue());
                        helper.sortBy(component, "Name");
                    }
                }
            });	
            $A.enqueueAction(getAccountDetails);
        }
    },
    sortBy: function(component, field) {
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.lstAcc");
        sortAsc = sortField != field || !sortAsc;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = (!a[field] && b[field]) || (a[field] < b[field]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        component.set("v.sortAsc",sortAsc);
        component.set("v.sortField",field);
        component.set("v.lstAcc",records);
    }
})