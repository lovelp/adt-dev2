({
    doInit : function(component, event, helper) {
        component.set("v.loadingSpinner",true);
        console.log('Inside method1');
        var jNumber,cNumber,sNumber;
        if(!$A.util.isUndefined(component.get("v.pageReference"))){
            jNumber =component.get("v.pageReference").state.c__jobNumber;
            cNumber =component.get("v.pageReference").state.c__customerNumber;
            sNumber =component.get("v.pageReference").state.c__siteNumber; 
        }else{
            jNumber = component.get("v.c__jobNumber");
            cNumber = component.get("v.c__customerNumber");
            sNumber = component.get("v.c__siteNumber");
        }
        console.log('Inside method2')
        component.set("v.jobNumber",jNumber);
        component.set("v.customerNumber",cNumber);
        component.set("v.siteNumber",sNumber);
        var action = component.get("c.getUIThemeDescription");
        action.setCallback(this, function(a) {
            component.set("v.Name", a.getReturnValue());
        });
        $A.enqueueAction(action);
        helper.getDetails(component,event,helper,cNumber,sNumber,jNumber);
    },
    doSubmit : function(component, event, helper) {
        component.set("v.loadingSpinner",true);
        var jNumber = component.get("v.jobNumber");
        var cNumber = component.get("v.customerNumber");
        var sNumber = component.get("v.siteNumber");
        helper.getDetails(component,event,helper,cNumber,sNumber,jNumber);
    },
    sortByName: function(component, event, helper) {
        helper.sortBy(component, "Name");
    },
    shrinkCol: function(component, event, helper){
        var val = event.target.id;
        component.set("v.showDetails", "false");
        component.set("v.recToExpand", val);
    },
    expandCol: function(component, event, helper){
        var val = event.target.id;
        component.set("v.recToExpand", val);
        component.set("v.showDetails", "true");   
    }
})