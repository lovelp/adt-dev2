({
	setPayType : function(component, accId) {
        console.log('Paytype helper called');
		var action = component.get("c.restrictPayType");
        action.setParams({recId: accId});
        action.setCallback(this, $A.getCallback(function (response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var allPicklistVal  = response.getReturnValue();
                var payTypePicklist;
                var stageList
                for (var key in allPicklistVal) {
                  if (allPicklistVal.hasOwnProperty(key)) {
                      console.log(key);
                      if(key == 'PayType'){
                      	payTypePicklist = allPicklistVal[key];     
                      }
                      if(key == 'OptyStage'){
                        stageList = allPicklistVal[key];
                      }	
                  }
                }                
                component.set("v.payTypeList", payTypePicklist);
                component.set("v.stageList", stageList);
                if(component.get("v.qtMode") != 'VIEW'){              
                    component.set("v.optySobject.HomeHealthPaytype__c", Object.values(payTypePicklist)[0].value);
                    component.set("v.optySobject.StageName", Object.values(stageList)[0].value);
                }
            }
            else{
                console.log('picklist Error');
            }           
        }));
        $A.enqueueAction(action); 
	},
    
    setOptyValue: function(component, opty) {
        console.log('Opty for edit'+opty.optyId);
        component.set("v.oppEditId", opty.optyId);
        component.set("v.mode", "edit");
        component.set("v.ShowNewOptyModal", true);        
        component.set("v.optySobject.Name", opty.optyName);                
        component.set("v.optySobject.HomeHealthPaytype__c", opty.payType);
        component.set("v.optySobject.StageName", opty.stageName);
        component.set("v.optySobject.CloseDate", opty.closeDate);
        component.set("v.optySobject.Id", opty.optyId);
        component.set("v.optySobject.BillingCompoundAddress__c", opty.billingCompoundAddress);
        component.set("v.billingId", opty.billingAddrsId);
        component.set("v.shippingId", opty.shippingAddrsId);
        component.set("v.restricEditQuote", opty.restricQuote);
    }
})