({
	getOptyQuoteList : function(component, event, helper) {
        console.log(component.get("v.theme"));
        var recId = component.get("v.accId");
        var agentType = component.get("v.agentType");
        var qtURL = component.get("v.qturl");
        var qtMode = component.get("v.qtMode");
        var optId = component.get("v.optId");
        var qtId = null;        
        if(qtMode =='VIEW'){
        	recId = optId;
			qtId = component.get("v.qtId"); 
            component.set("v.mode", "edit");
        }
        var action = component.get("c.getOptyList");
        action.setParams({
            recId: recId,
            agentType: agentType,
            qtUrl: qtURL,
            qtId: qtId
        });
        action.setCallback(this, $A.getCallback(function (response){
        var state = response.getState();
        if(state === "SUCCESS"){
        	var optyresults  = response.getReturnValue();
        	console.log(Object.values(optyresults));
            component.set("v.AccSobject", optyresults[0].acc);
            if(optyresults != null){
                component.set("v.optyresults", optyresults);                
                if(qtMode =='VIEW'){
                	helper.setOptyValue(component, optyresults[0]); 
                    if(optyresults[0].quotesWraper[0].quoteStatus == 'Paid'){
                    	component.set("v.isCurrentQtPaid", true);    
                    }
                    if(optyresults[0].quotesWraper[0].quoteStatus == 'Submitted'){
                        component.set("v.isCurrentQtSumitted", true);
                    }
                    //This is for rerender address
                    component.set("v.shippingAccSame", true);
                    component.set("v.shippingAccSame", false);
                    component.set("v.billingAccSame", true);
                    component.set("v.billingAccSame", false);
                }
                else{
                	 component.set("v.optySobject.Name", optyresults[0].acc.Name);
                     var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
    				 component.set("v.optySobject.CloseDate", today);
                }       
            }
        }
        else if (state === "ERROR") {
            var errors = response.getError();
			var message = 'Unknown error'; // Default error message
			// Retrieve the error message sent by the server
            if (errors && Array.isArray(errors) && errors.length > 0) {
                message = errors[0].message;
            }
            // Display the message
            component.set("v.isError", true);
        	component.set("v.saveError", message);
    	}
        }));
        $A.enqueueAction(action);
        helper.setPayType(component,component.get("v.accId"));
        
	},
    AddNewOppty: function(component, event, helper){
        component.set("v.mode", "new");
        component.set("v.ShowNewOptyModal", false);
        component.set("v.oppEditId", '');       
        component.set("v.optySobject.Name", '');
        component.set("v.optySobject.HomeHealthPaytype__c", '');
        component.set("v.optySobject.StageName", '');
        component.set("v.optySobject.CloseDate", '');        
        component.set("v.billingId", '');
        component.set("v.shippingId", '');
        component.set("v.billingAccSame", false);
        component.set("v.shippingAccSame", false);
    },
    closeModal: function(component, event, helper) {
		Window.close();        
    },
    saveNewOpty: function(component, event, helper) {
        component.set("v.isError", false);
        var errorMessage = '';
        component.set("v.saveError", errorMessage);
        var optyrec = component.get("v.optySobject");
        console.log('##'+optyrec.Name);
        var shipId = component.get("v.shippingId");
        var bilid = component.get("v.billingId");
        console.log('Shi and Bill'+shipId+bilid);
		optyrec.BillingAddress__c = bilid;
        optyrec.ShippingAddress__c = shipId;
        optyrec.AccountId = component.get("v.AccSobject").Id;
        var isError = false;
        if(optyrec.Name == ''){
            errorMessage = "Opportunity Name is empty.";
            isError = true;
        }
        if(!isError && optyrec.HomeHealthPaytype__c == ''){
            errorMessage = "Select Pay Type.";
            isError = true;
        }
        if(!isError &&  optyrec.StageName == ''){
            errorMessage = "Select Opportunity Stage.";
            isError = true;
        }
        if(!isError && optyrec.CloseDate == ''){
            errorMessage = "Select Opportunity Close Date.";
            isError = true;
        }
        if(!isError && (shipId === undefined || shipId == '')){
            errorMessage = "Shipping address is empty.";
            isError = true;
        }
        if(!isError && (bilid === undefined || bilid == '')){
            errorMessage = "Billing address is empty.";
            isError = true;
        }
        if(isError){
            component.set("v.isError", true);
            component.set("v.saveError", errorMessage);
        }
        else{
            var qturl = component.get("v.qturl");
            var action = component.get("c.saveOpportunity");
            action.setParams({
                opp: optyrec,
                qtURL: qturl
        	});
            action.setCallback(this, $A.getCallback(function (response){
           	var state = response.getState();
            if(state === "SUCCESS"){ 				
            	component.set("v.ShowNewOptyModal", false);
                //Saved for later. To refresh view
                var optyresults  = response.getReturnValue();
                //var refreshView = component.get('c.getOptyQuoteList');
                //$A.enqueueAction(refreshView);
                //var url = component.get("v.qturl");
                console.log('redirect called'+optyresults);
        		var urlEvent = $A.get("e.force:navigateToURL");
            	urlEvent.setParams({
              		"url": optyresults
            	});
            	urlEvent.fire();                    
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
				var message = 'Unknown error'; // Default error message
			// Retrieve the error message sent by the server
            if (errors && Array.isArray(errors) && errors.length > 0) {
                message = errors[0].message;
            }
            // Display the message
            component.set("v.isError", true);
        	component.set("v.saveError", message);
    	}                 
            }));
            $A.enqueueAction(action); 
        }
    },
    viewQuotes: function(component, event, helper) {
        var selectOptyDetails = event.getSource().get("v.value");
        component.set("v.selectedOpty", selectOptyDetails);
        if(selectOptyDetails.quotesWraper !=null){            
            component.set("v.quoteDetails", selectOptyDetails.quotesWraper);            
        }else{
            component.set("v.quoteDetails", null);
        }
    }, 
    setAddressId: function(component, event, helper) {
    	var addressId = event.getParam("addressId");
        var addrsArray = addressId.split(':');
        if(addrsArray[0] == 'Shipping'){
        	component.set("v.shippingId", addrsArray[1]);    
        }else if(addrsArray[0] == 'Billing'){
            component.set("v.billingId", addrsArray[1]); 
        }
        console.log(component.get("v.shippingId"));
    },
    assignAccShip: function(component, event, helper){        
        if(component.get("v.shippingAccSame")){
            var accobj = component.get("v.AccSobject");
            console.log('##'+accobj.AddressID__c);
        	component.set("v.shippingId", accobj.AddressID__c);	    
        }else{
            component.set("v.shippingId", '');	
        }
    },
    assignAccBill: function(component, event, helper){
        if(component.get("v.billingAccSame")){
        	var accobj = component.get("v.AccSobject");
            component.set("v.billingId",accobj.AddressID__c);
        }else{
            component.set("v.billingId",'');
        }
    },
    AddNewQuote: function(component, event, helper){
        var item = event.getSource().get("v.value");
        var agentType = component.get("v.agentType");
        if(agentType == 'FIELD'){
            console.log('##'+item.newQuoteUrl)
            window.open(item.newQuoteUrl,"_self");
        }else{
            console.log('qturl'+item.newQuoteUrl);
            window.open(item.newQuoteUrl,"_self");
            /*var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var text = "";
            for( var i=0; i < 5; i++ ){
            	text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            var quoteURL = item.newQuoteUrl+'&testUniqueVal='+text;
            window.open(quoteURL, "_self");*/
        }

    },
    editOpty: function(component, event, helper){
        var opty = event.getSource().get("v.value");
        //Call opty edit mode
        helper.setOptyValue(component, opty);
    },
    openQuote: function(component, event, helper){
        var accId = event.getSource().get("v.value");
        action.setParams({
            recId: accid
        });
        action.setCallback(this, $A.getCallback(function (response){
        var state = response.getState();
            if(state === "SUCCESS"){
                var url = response.getReturnValue();
                window.open(url,"_self"); 
            }
        }));
        $A.enqueueAction(action);
    },
    
    cloneOpty: function(component, event, helper){
        //helper.setPayType(component, component.get("v.accId"));
        var opty = event.getSource().get("v.value");
        console.log('Opty for edit'+opty.optyId);
        component.set("v.oppEditId", opty.optyId);
        component.set("v.mode", "edit");
        component.set("v.ShowNewOptyModal", true);        
        component.set("v.optySobject.Name", opty.optyName);
        component.set("v.optySobject.HomeHealthPaytype__c", opty.payType);
        component.set("v.optySobject.StageName", opty.stageName);
        component.set("v.optySobject.CloseDate", opty.closeDate);
        //For Clone do not pass object id. 
        //component.set("v.optySobject.Id", opty.optyId);
        component.set("v.billingId", opty.billingAddrsId);
        component.set("v.shippingId", opty.shippingAddrsId);
    },
    
    GoBackToAccount: function(component, event, helper){
        var accId = event.getSource().get("v.value");
        //var baseURL = component.get("v.baseURL");
        var agentType = component.get("v.agentType");
        if(component.get("v.agentType") == 'FIELD'){
            baseURL = '/apex/CPQCreateRedirect?Id='+accId+'&agentType=FIELD';
            window.open(baseURL,"_self");
        }else{
            console.log('close subtab called');
        	var urlEvent = $A.get("e.force:navigateToURL");
            	urlEvent.setParams({
              		"url": "closeSubTab"
            	});
            	urlEvent.fire();
        }
    },
    saveOptyProceed: function(component, event, helper){
        var saveOpty = component.get("c.saveNewOpty");
        $A.enqueueAction(saveOpty);
    },
    quoteOnly: function(component, event, helper){
        var url = component.get("v.qturl");
        console.log(url);
        window.open(url,"_self");
    }
    
})