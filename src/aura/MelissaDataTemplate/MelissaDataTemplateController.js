({
    doInit : function(component, event, helper) {
        var mode = component.get("v.mode");
        if(mode == 'edit'){
            var optyId = component.get("v.oppEditId");
            var action = component.get("c.getAddressDetails");
            action.setParams({"recId":optyId,
                              "addrsType":component.get("v.addressType")});
            console.log('Edit mode called');
            action.setCallback(this,function(response){
           		var state = response.getState();
                console.log(Object.values(response.getReturnValue()));
                if(state === "SUCCESS"){                
                var addrObj  = response.getReturnValue();
                console.log(Object.values(addrObj));   
                if(addrObj != null){                    
                    component.set("v.street1", addrObj.Street__c);
                    if(addrObj.Street2__c != null){
                        component.set("v.street2", addrObj.Street2__c);
                    }
                    console.log('City'+addrObj.City__c);
                    component.set("v.city", addrObj.City__c);
                    console.log('Postalcode'+addrObj.PostalCode__c);
                    component.set("v.postalcode", addrObj.PostalCode__c);
                    component.set("v.country", addrObj.CountryCode__c);
                    component.set("v.state", addrObj.State__c);
                    }
                }
            });
             $A.enqueueAction(action);
        }
	},

    verifyAgain: function(component, event, helper) {
        console.log("Hey verify again");
    	component.set("v.verify", true);			    
    },
    inputaddrschange: function(component, event, helper) {
        console.log("Hey address changed");   				    
    },
    checkMelissa: function(component, event, helper){        
        component.set("v.errors", null);
    	var street1 = component.get("v.street1");
        var street2 = component.get("v.street2");
        var city = component.get("v.city");
        var postalcode = component.get("v.postalcode");
        var country = component.get("v.country");
        var state = component.get("v.state");
        if(street1 == null || postalcode == null){
            component.set("v.errors", 'Street1 or postal code can\'t be empty.');
        }
        else{
        component.set("v.onSpinner", true);
    	var action = component.get("c.addressValidation");
        action.setParams({ street1:street1,
                          street2:street2,
                          city:city,
                          postalcode:postalcode,
                          country:country, 
                          state: state,
                          overRideTrue: false});
        action.setCallback(this,function(response){
       	var state = response.getState();
            if(state === "SUCCESS"){                
                var melissaResult  = response.getReturnValue();
                console.log('MelissaResults'+Object.values(melissaResult));
                if(melissaResult.status == 'Error'){
                	component.set("v.errors", melissaResult.message);
                    component.set("v.verify", true);
                    component.set("v.overridesave", true);
                }else{
                    component.set("v.overridesave", false);
                    component.set("v.verify", false);
                    component.set("v.errors", null);
                    var addId = melissaResult.addressId;
                    var addType = component.get("v.addressType");
                    var concatString = addType+':'+addId;
                    var passaddid = component.getEvent("AddressIdEvent");
                    passaddid.setParams({"addressId" : concatString}); 
                    passaddid.fire();
                    console.log('event fired'+addId);
                }
                console.log(melissaResult.message);
                component.set("v.street1", melissaResult.addrMelissa.street1);
                if(melissaResult.addrMelissa.street2 != null){
                	component.set("v.street2", melissaResult.addrMelissa.street2);
                }
                component.set("v.city", melissaResult.addrMelissa.city);
                component.set("v.postalcode", melissaResult.addrMelissa.postalcode);
                component.set("v.country", melissaResult.addrMelissa.country);
                component.set("v.state", melissaResult.addrMelissa.state);
                component.set("v.onSpinner", false);
             }              
            else{
         	    
            }
         });
         $A.enqueueAction(action);
        }
	},
    addrsChanged: function(component, event, helper){
        component.set("v.verify", true);
    }
})