({
    searchRecs: function(component, event, helper) {
        helper.showSpinner(component, "runall");
        var searchWrapper = event.getParam("leadSearchAttributes");
        var recId = component.get("v.recId");
        var action = component.get("c.searchRecords");
        var displayCards = false;
        if(typeof recId != "undefined"){
            component.set("v.displayLead", "false");
            component.set("v.displayAccTab", "false");
            displayCards = true;
            console.log('Inside here'+recId);
        }
        $A.util.removeClass(component.find('accTable'), 'addFixedHeight');
        $A.util.removeClass(component.find('leadTab'), 'addFixedHeight');
        action.setParams({searchString : searchWrapper,
                          recId : recId});		     
        action.setCallback(this, $A.getCallback(function (response){
            var state = response.getState();
            if (state === "SUCCESS"){            
                var searhResults  = response.getReturnValue(); 
                console.log('Records'+searhResults);
                if(searhResults != null){
                    component.set("v.resultsWrapper", searhResults);
                    component.set("v.accntResults", searhResults.accntlist);  
                    var defaultSize = 5;
                    console.log("Search Result length...",searhResults.accntlist.length);
                    if(searhResults.accntlist.length >0){
                        var adjustedSize1 = searhResults.accntlist.length>defaultSize?defaultSize:searhResults.accntlist.length;
                        if(displayCards){
                            var forCards = searhResults.accntlist.length>4?4:searhResults.accntlist.length;
                            helper.createAccountCards(component, searhResults.accntlist, forCards);
                        }
                        else{
                            helper.createAccountTable(component, searhResults.accntlist, adjustedSize1);
                        }
                    } 
                    else{
                        component.set("v.accCount",0);
                    }
                    if(searhResults.leadList.length > 0){
                        var adjustedSize2 = searhResults.leadList.length>defaultSize?defaultSize:searhResults.leadList.length;
                        helper.createLeadTable(component, searhResults.leadList, adjustedSize2);
                    }
                    else{
                        component.set("v.leadCount",0);
                    }
                }
                else{
                    component.set("v.accCount",0);
                    component.set("v.leadCount",0);
                    
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
            helper.showSpinner(component, "stop");
        }));
        $A.enqueueAction(action);    
    },
    viewMoreAccnts: function(component, event, helper){
        var s1 = component.get("v.resultsWrapper");
        if(s1.accntlist.length > 10){
            $A.util.addClass(component.find('accTable'), 'addFixedHeight');
        }
        helper.createAccountTable(component, s1.accntlist, s1.accntlist.length);
    },
    viewMoreLeads: function(component, event, helper){
        var s2 = component.get("v.resultsWrapper");
        if(s2.leadList.length > 10){
            $A.util.addClass(component.find('leadTab'), 'addFixedHeight');
        }
        helper.createLeadTable(component, s2.leadList, s2.leadList.length);
    },
    viewMoreOpp: function(component, event, helper){
        var s3 = component.get("v.resultsWrapper");
        helper.createOppTable(component, s3.oppList, s3.oppList.length);
    },
    rowAction: function(component, event, helper){		
        var row = event.getParam("row");
        component.set("v.displayModal",true);
        component.set("v.repName",row.Rep_Name__c);
        component.set("v.objectData",row);
        if(row.Id.includes('001')){
            component.set("v.AccountObj",true);
        }
        else if(row.Id.includes('00Q')){
            row.LastModifiedDate = row.LastModifiedDate.substr(0, row.LastModifiedDate.indexOf('T'));
            component.set("v.AccountObj",false);
        }
       var userId = $A.get("$SObjectType.CurrentUser.Id");
        if(userId == row.OwnerId){
            component.set("v.isSameOwner",false);
        }
        else{
           component.set("v.isSameOwner",true); 
        }
    },
    
    viewRowDetails: function (component, event) {
        //alert('Row selcted');
    },
    viewMoreAccntsCards:function(component, event, helper){
        var s4 = component.get("v.resultsWrapper");
        helper.createAccountCards(component, s4.accntlist, s4.accntlist.length);
    },
    redirectCntrl:function(component, event, helper){
        var objectData = component.get("v.objectData");
        if(objectData[0].Id.includes('001')){
            component.set("v.AccountObj",true);
        }
        var action = component.get("c.changeOwnerShip");
        action.setParams({objectId : objectData[0].Id,
                          existingOwner: objectData[0].OwnerId});
        action.setCallback(this, $A.getCallback(function (response){            
            var state = response.getState();
            var returnValue = response.getReturnValue();
            if (state === "SUCCESS"){
               //redirect
            }
            if(state === "ERROR"){
                var errors = '';
                for (var i = 0; response.getError().length > i; i++){
                        errors = errors + response.getError()[i].message;
                } 
                component.set("v.errorMessageDisplay",errors);
                component.set("v.isAnyError",true);
            }
        }));
        $A.enqueueAction(action);
    },
    closeModal:function(component, event, helper){
        component.set("v.displayModal",false);
    }
})