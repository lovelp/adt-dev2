({
    createAccountTable: function(component, accntObj, recSize){
        var rowact = [{ label: 'Go To Account', name: 'AccChangeOwnership' }];
        component.set('v.accCol', [
                    { label: 'Account Name', fieldName: 'Name', type: 'text'},
            		{ label: 'Address', fieldName: 'StandardizedAddress__c', type: 'text'},
            		{ label: 'Phone', fieldName: 'Phone', type: 'text'},
                    { label: 'Email', fieldName: 'Email__c', type: 'email'},
                    { label: 'Status', fieldName: 'StatusFormula__c', type: 'text'},
                 	{ label: 'Channel', fieldName: 'Channel__c', type: 'text'},
                    { label: 'Last Activity Date', fieldName: 'LastActivityDate__c', type:'text'},
            		{label: 'Owner',fieldName: 'Rep_Name__c',type:'text'},
                    { type: 'action', typeAttributes: { rowActions: rowact }} 
                ]);
        component.set('v.accCount',accntObj.length);
        var accountObj = new Object();
        var i;
        for(i=0;i<recSize;i++) {
            accountObj[i] = accntObj[i];  		    
		}
        component.set('v.accntResults', Object.values(accountObj));
    },
    createLeadTable: function(component, leadObj, recSize){
        var actions = [{ label: 'Go To Lead', name: 'LeadChangeOwnership' }];
        component.set('v.leadCol', [
            		{ label: 'Lead Name', fieldName: 'LeadNameBasedOnCompany__c', type: 'text'},
            	    { label: 'Address', fieldName: 'StandardizedAddress__c', type: 'text'},
            		{ label: 'Phone', fieldName: 'Phone', type: 'Phone'},
            		{ label: 'Email', fieldName: 'Email', type: 'email'},
            		{ label: 'Status', fieldName: 'Status', type: 'text'},
                    { label: 'Channel', fieldName: 'Channel__c', type: 'text'},
                    { label: 'Last Modified', fieldName: 'LastModifiedDate', type: 'DateTime'},
            		{label: 'Owner',fieldName: 'Rep_Name__c',type:'text'},
                    { type: 'action', typeAttributes: { rowActions: actions }} 
                ]);
        component.set('v.leadCount',leadObj.length);
        var ldObj = new Object();
        var i;
        for(i=0;i<recSize;i++) {            
            ldObj[i] = leadObj[i];  		    
		}        
        component.set('v.leadResults', Object.values(ldObj));
    },
    createOppTable: function(component, oppObj, recSize){
	    component.set('v.oppCol', [
                    { label: 'Opportunity Name', fieldName: 'Name', type: 'text'},
                    { label: 'Account Name', fieldName: 'AccountName__c', type: 'text'},
                    { label: 'Stage', fieldName: 'StageName', type: 'text'},
                    { label: 'Close Date', fieldName: 'CloseDate', type: 'date'},
                    { label: 'Opportunity Owner', fieldName: 'OwnerName__c', type: 'text'}                    
                ]);
        component.set('v.oppCount',oppObj.length);
        var opObj = new Object();
        var i;
        for(i=0;i<recSize;i++) {            
            opObj[i] = oppObj[i];  		    
		}        
        component.set('v.opptResults', Object.values(opObj));
    },
    redirectToRec : function (component, event, recId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": recId,
          "slideDevName": "related"
        });
        navEvt.fire();
    },
    showSpinner: function(component, val){
    	component.set("v.showSpinner", val);
	},
    createAccountCards: function(component, accntObj, recSize){        
        component.set('v.accCards', [
                    { label: 'Account Name', fieldName: 'Name', type: 'text' } ,
            		{label: 'Action', type: 'button', initialWidth: 0, typeAttributes:
                    { label: '', title: 'Click to View Details', name: 'edit_status', iconName: 'utility:preview'}}            
                ]);
        component.set('v.accCount',accntObj.length);
        var accountObj = new Object();
        var i;
        for(i=0;i<recSize;i++) {
            accountObj[i] = accntObj[i];  		    
		}
        component.set('v.accntResults', Object.values(accountObj));
    }
})