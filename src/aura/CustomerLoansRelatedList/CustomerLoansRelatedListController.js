({
	doInit : function(component, event, helper) {
        component.set("v.spinner","true");
		var rec = component.get("v.recordId");
        component.set("v.recId", rec);
		console.log('FROM LC'+rec+component.get("v.refreshCustLn"));
        var action = component.get('c.getExistingLoans');
        action.setParams({recId: rec,
                          runRefresh: component.get("v.refreshCustLn")});
        action.setCallback(this,function(response){
        	var loans  = response.getReturnValue(); 
                if(loans != null){
                    component.set("v.customerLoans", loans);                    
                    component.set("v.recPresent", "true");
                    component.set("v.custNum",loans[0].CustomerNumber__c);
                    component.set("v.lastModifiedDate", loans[0].lastRefreshedon);
                    var outsite = "Inactive Sites: "+loans[0].outSiteSize;
                    var onAcc = "Number of Loans - This Account: "+loans[0].siteOnthisAcc+"; ";
                    var counttext = onAcc+outsite;
                    component.set("v.totalCount", counttext);
                }
            else{
                component.set("v.recPresent", "false");
                component.set("v.customerLoans", "null"); 
            }
            component.set("v.spinner","false");
        });
        $A.enqueueAction(action);
	},
    expandCol: function(component, event, helper){
        var val = event.target.id;
        component.set("v.recToExpand", val);
    	component.set("v.showDetails", "true");   
    },
    shrinkCol: function(component, event, helper){
        var val = event.target.id;
    	component.set("v.showDetails", "false");
        component.set("v.recToExpand", val);
    },
    refreshTable: function(component, event, helper){        
        component.set("v.spinner","true");
        component.set("v.refreshCustLn", "true");
        var doit = component.get("c.doInit");
        $A.enqueueAction(doit);   
    }

})