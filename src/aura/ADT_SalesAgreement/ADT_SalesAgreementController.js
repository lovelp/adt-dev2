({
    doInit : function(component, event, helper) {
        component.set("v.loadingSpinner",true);
        var oppId = component.get("v.OpportunityId");
        var type = component.get("v.RequestType");
        console.log(oppId);
        var action = component.get("c.initActions");
        action.setParams({'oppId': oppId,'reqType': type });
        action.setCallback(this, function(response) {
            component.set("v.loadingSpinner",false);
            console.log('Response---'+response.getReturnValue()); 
            var wrapperData = response.getReturnValue();
            console.log(wrapperData)
            component.set("v.opp", wrapperData.opp);
            component.set("v.IsSuccess", wrapperData.isSuccess);
            component.set("v.IsSelected", wrapperData.isSelected);
            component.set("v.RequestType", wrapperData.requestType);
            component.set("v.SacReq", wrapperData.sacReq);
            component.set("v.SalReq", wrapperData.salReq);
            component.set("v.SacRes", wrapperData.sacRes);
            component.set("v.SalRes", wrapperData.salRes);
            component.set("v.PageMessage", wrapperData.pageMessage);
            component.set("v.ShowButton", wrapperData.showButton);
            component.set("v.SalesAgreementId", wrapperData.SalesAgreementId);
            component.set("v.SalRes", wrapperData.salRes);
            component.set("v.DataWrapper", wrapperData);
            component.set("v.Address", wrapperData.Address);
            component.set("v.MMBBillingSystems", wrapperData.MMBBillingSystems);
            var wrapperData = component.get("v.DataWrapper");
            console.log('=========DataWrapper=====');
            console.log(JSON.stringify(wrapperData));
        });
        $A.enqueueAction(action);
    },
    LookUpSalesAgreement: function(component, event, helper) {
        component.set("v.loadingSpinner",true);
        component.set("v.RequestType", "Lookup");
        var oppId = component.get("v.OpportunityId");
        var said = component.find("sAgreeName").get("v.value");
        var wrapperData = component.get("v.DataWrapper");
        console.log('=========DataWrapper=====');
        console.log(wrapperData);
        console.log(said);
        var action = component.get("c.submit");
        //action.setParams({'SalesAgreementID': said, 'OppId' :oppId});
        var JSONWrapperData = JSON.stringify(wrapperData);
        action.setParams({"wrapperData": JSONWrapperData});
        console.log('params set');
        action.setCallback(this, function(response) {
            console.log('Response---'+response.getReturnValue()); 
            var wrapperData = response.getReturnValue();
            component.set("v.opp", wrapperData.opp);
            component.set("v.IsSuccess", wrapperData.isSuccess);
            component.set("v.IsSelected", wrapperData.isSelected);
            component.set("v.RequestType", wrapperData.requestType);
            component.set("v.SacReq", wrapperData.sacReq);
            component.set("v.SalReq", wrapperData.salReq);
            component.set("v.SacRes", wrapperData.sacRes);
            component.set("v.SalRes", wrapperData.salRes);
            component.set("v.PageMessage", wrapperData.pageMessage);
            component.set("v.ShowButton", wrapperData.showButton);
            component.set("v.SalesAgreementId", wrapperData.SalesAgreementId);
            component.set("v.SalRes", wrapperData.salRes);
            component.set("v.DataWrapper", wrapperData);
            component.set("v.Address", wrapperData.Address);
            component.set("v.MMBBillingSystems", wrapperData.MMBBillingSystems);
            var wrapperData = component.get("v.DataWrapper");
            console.log('=========DataWrapper=====');
            console.log(wrapperData);
            component.set("v.loadingSpinner",false);
        });
        $A.enqueueAction(action);
    },    
    submitSalesAgreement: function(component, event, helper) {
        component.set("v.loadingSpinner",true);
        var wrapperData = component.get("v.DataWrapper");
        console.log('=========DataWrapper=====');
        console.log(wrapperData);
        var action = component.get("c.submit");
        var JSONWrapperData = JSON.stringify(wrapperData);
        action.setParams({"wrapperData": JSONWrapperData});
        action.setCallback(this, function(response) {
            var wrapperDataRes = response.getReturnValue();
            if(response.getReturnValue().returnURL != "" && !$A.util.isUndefined(response.getReturnValue().returnURL)) {
                component.set("v.loadingSpinner",false);
                window.open(response.getReturnValue().returnURL,"_blank");
                var oppId = component.get("v.OpportunityId");
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url" : "/"+oppId
                });
                urlEvent.fire();
                $A.get('e.force:refreshView').fire();
                window.location.reload();
                $A.get('e.force:refreshView').fire();
                $A.get('e.force:refreshView').fire();
            }else{
                console.log(response);
                component.set("v.loadingSpinner",false);
                component.set("v.PageMessage",wrapperDataRes.pageMessage);   
            }
        });
        $A.enqueueAction(action);
    },
    submitLookupSalesAgreement: function(component, event, helper) {
        component.set("v.loadingSpinner",true);
        var wrapperData = component.get("v.DataWrapper");
        console.log('=========DataWrapper=====');
        console.log(wrapperData);
        var action = component.get("c.selectSalesAgreement");
        var sa_id = component.find("sAgreeName").get("v.value");
        var oppId = component.get("v.OpportunityId");
        var JSONWrapperData = JSON.stringify(wrapperData);
        //action.setParams({'oppId': oppId,'saId': sa_id });
        action.setParams({'wrapperData': JSONWrapperData, 'oppId': oppId,'saId': sa_id});
        action.setCallback(this, function(response) {
            console.log(response);
            var wrapperDataRes = response.getReturnValue();
            if(response.getReturnValue().returnURL != "" && !$A.util.isUndefined(response.getReturnValue().returnURL)) {
                component.set("v.loadingSpinner",false);
                window.location.href = response.getReturnValue().returnURL; 
                window.location.reload();
            }else{
                console.log(response);
                component.set("v.loadingSpinner",false);
                component.set("v.PageMessage",wrapperDataRes.pageMessage);   
            }
        });
        
        $A.enqueueAction(action);
    },
    cancel: function(component, event, helper) {
    	console.log('In Cancel');
        component.set("v.loadingSpinner",true);
        var action = component.get("c.goToSalesPilot");
        var said = component.find("sAgreeName").get("v.value");
        var oppId = component.get("v.OpportunityId");
        action.setParams({'salesAgreementId': said,'oppId': oppId });
        action.setCallback(this, function(response) {
            console.log(response);
            if(response.getReturnValue().returnURL != "") {
                component.set("v.loadingSpinner",false);
                window.location.href = response.getReturnValue().returnURL; 
            }
        });
        $A.enqueueAction(action);
    },
    close:function(component, event, helper){
    	console.log('In Close');
        var oppId = component.get("v.OpportunityId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url" : "/"+oppId
        });
        urlEvent.fire();
        $A.get('e.force:refreshView').fire();
        window.location.reload();
        $A.get('e.force:refreshView').fire();
        $A.get('e.force:refreshView').fire();
    },
    closeModal:function(component, event, helper){
    	console.log('In Close Modal');
        $A.get("e.force:closeQuickAction").fire()
    },
    reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
        console.log('reinit fired');
    }
})