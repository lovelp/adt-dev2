({
	doInit : function(component, event, helper) {
	    component.set("v.spinner", "true");
        var recId = component.get("v.leadId");
        var action = component.get('c.returnSortedAccounts');        
        action.setParams({ recId:  recId});
        action.setCallback(this,function(response){
            console.dir(response);
        	var state = response.getState();
            if(state === "SUCCESS"){
            	var searhResults  = response.getReturnValue(); 
                if(searhResults != null){
                component.set("v.resultsWrapper", searhResults);
                component.set("v.accntResults", searhResults.accntlist);
                component.set("v.accCount",searhResults.accntlist.length);    
                var defaultSize = 3;
                if(searhResults.accntlist.length >0){
                    component.set("v.matchingResults", "true");
                    if(searhResults.accntlist.length > defaultSize){
                    	$A.util.addClass(component.find('accTable'), 'addFixedHeight');
                    }
                	var adjustedSize1 = searhResults.accntlist.length>defaultSize?defaultSize:searhResults.accntlist.length;
                    helper.createAccountTable(component, searhResults.accntlist, adjustedSize1);
                	}                    
                }
                component.set("v.spinner", "false");
            } 
            else{
         	    component.set("v.spinner", "false");
            }
         });
         $A.enqueueAction(action);
	},
    convertLead: function(component, event, helper) {
        component.set("v.spinner", "true");
		var recId = component.get("v.leadId");
		var action = component.get('c.converLead');        
        action.setParams({ recId:  recId});
        action.setCallback(this,function(response){
            console.dir(response);
        	var state = response.getState();
            //var returnValue = response.getReturnValue();
            console.log(state);
            console.log(response.getReturnValue());
            if(state === "SUCCESS"){
                component.set("v.accId", response.getReturnValue());
                component.set("v.spinner", "false");
            } 
            else{
                component.set("v.spinner", "false");
                component.set("v.isError", "true");                       	
            }
         });
         $A.enqueueAction(action);
	},
    rowAction: function(component, event, helper){        
        var row = event.getParam("row");
        console.log('Rec1'+Object.values(row));        
        component.set("v.selctedRecord", row); 
        component.set("v.displayCard", "true");
        component.set("v.showConf", "false");
        component.set("v.showMMBResults", "false");
        component.set("v.RunMMB", "Refresh MMB");
        component.set("v.mmbError", "false");
        var button = component.find('mmblookupbut');
        component.set("v.recordOwner", row.Owner.Name);
    },
    viewMoreAccnts: function(component, event, helper){
        var s1 = component.get("v.resultsWrapper");
        helper.createAccountTable(component, s1.accntlist, s1.accntlist.length);
        component.set("v.accCount",0);
    },
    
    goToAccount: function(component, event, helper){
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var selAcc = component.get('v.selctedRecord');
        var ldid = component.get("v.leadId");
        console.log('#'+userId);
        if(userId != selAcc[0].OwnerId){
            component.set("v.showConf", "true");
            var cmpTarget = component.find('highlight');
            var interval = setInterval(function (){
                $A.util.removeClass(cmpTarget, 'peak');
                $A.util.addClass(cmpTarget, 'normal');
                clearInterval(interval);
            }, 200);
            $A.util.addClass(cmpTarget, 'peak');

        }
        else{
            helper.handleGotoAcc(component, selAcc, ldid);
        }
    },
    //Continue to account after changeownership confirmation
    contToAcc: function(component, event, helper){
        component.set("v.spinner", "true");
        var selAcc = component.get('v.selctedRecord');
        var ldid = component.get("v.leadId");
        console.log(selAcc[0].Id+selAcc[0].OwnerId);
        helper.handleGotoAcc(component, selAcc, ldid);
    },
    //MMB lookup run
    runMMBlookup: function(component, event, helper){
        component.set("v.showRunMMB", "true");
        let mmbbut = component.find('mmblookupbut');
        if(typeof mmbbut != "undefined"){
            //mmbbut.set('v.disabled',true);
        }
        component.set("v.progress", 0);
        var interval = setInterval($A.getCallback(function (){
            var progress = component.get('v.progress');
            component.set('v.progress', progress === 100 ? helper.handleProgressBar(component, interval) : progress + 10);
        }), 500); 
        var action = component.get("c.runMMB");
        var selAcc = component.get('v.selctedRecord');
        action.setParams({ accid:  selAcc[0].Id});
        action.setCallback(this,function(response){
            console.dir(response);
        	var state = response.getState();
            if(state === "SUCCESS"){
                var returnmsg = response.getReturnValue();
                console.log('returnmsg '+returnmsg);
                if(returnmsg != 'failure'){
                    component.set("v.showMMBResults", "true");
                    component.set("v.showRunMMB", "false");
                    //var doit = component.get("c.doInit");
                    //$A.enqueueAction(doit);
                }
                else{
                    component.set("v.showRunMMB", "false");
                    component.set("v.showMMBResults", "false");
                    component.set("v.mmbError", "true");
                }
            }
            else{
                component.set("v.showRunMMB", "false");
                component.set("v.showMMBResults", "false");
                component.set("v.mmbError", "true");
            }
        });
         $A.enqueueAction(action);
    },
    cancelModal: function(component, event, helper){
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
         dismissActionPanel.fire();
    },
    
    updateOpty:function(component, event, helper){
        console.log('opty Saved');
    }
        


})