({
	createAccountTable: function(component, accntObj, recSize){
        component.set('v.accCol', [
                    { label: 'Account Name', fieldName: 'Name', type: 'text' },
                    { label: 'Channel', fieldName: 'Channel__c', type: 'text' },
                    { label: 'Status', fieldName: 'StatusFormula__c', type: 'text' },
                    { label: 'Last Activity', fieldName: 'LastActivityDate__c', type: 'date' },
            		{label: 'Preview', type: 'button', initialWidth: 0, typeAttributes:
                    { label: '', title: 'Click to View Details', name: 'edit_status', iconName: 'utility:preview'}}            
                ]);
        component.set('v.accCount',accntObj.length);
        var accountObj = new Object();
        var i;
        for(i=0;i<recSize;i++) {
            accountObj[i] = accntObj[i];  		    
		}
        component.set('v.accntResults', Object.values(accountObj));

    },
    
    handleProgressBar: function(component, interval){
        clearInterval(interval);
        console.log('at handle progressbar');
        component.set("v.showRunMMB", "false");
        component.set("v.RunMMB", "Refresh MMB Again")
    },
    
    handleGotoAcc: function(component, seldAcc, ldid){
        component.set("v.spinner", "true");
        var action = component.get('c.continueToAccount'); 
        action.setParams({ objId:  seldAcc[0].Id,
                           leadId: ldid,
                           usrid: seldAcc[0].OwnerId});
        action.setCallback(this,function(response){
        	var state = response.getState();
            if(state === "SUCCESS"){
                //Logic to handle add oppt and contact
                component.set("v.addOptyCont", true);
                component.set("v.displayAccSec", false);
                this.handleNewOptyCont(component, seldAcc[0].Id);	
            } 
            else{
         	
            }
         });
         $A.enqueueAction(action);
    },
    handleNewOptyCont: function(component, aid){
        var action = component.get('c.getRelatedOptyCont');
        action.setParams({accid: aid});
                action.setCallback(this,function(response){
        var state = response.getState();
        console.log(response.getError());
        if(state === "SUCCESS"){
            var searhResults  = response.getReturnValue();
            if(searhResults!=null){
                component.set("v.spinner", "false");
                //component.set("v.contResults", searhResults.optylist);
                this.prepareOptyTable(component, searhResults.optylist);
                }
            } 
        else{
         	
            }
         });
         $A.enqueueAction(action);                  
    },
    prepareOptyTable: function(component, optylist){
        component.set('v.optyCol', [
                    { label: 'Opp. Name', fieldName: 'Name', type: 'text', editable: true, typeAttributes: { required: true } },
                    { label: 'Stage', fieldName: 'StageName', type: 'text', editable: true, typeAttributes: { required: true } },
                    {label: 'Close date', fieldName: 'CloseDate', type: 'date', editable: true,
                    typeAttributes: {
                        year: 'numeric',
                        month: 'short',
                        day: 'numeric'
                        }
                    }
                   ]); 
        component.set('v.optyResults', optylist);
    },
    prepareContTable: function(component, contlist){
        
    }

})