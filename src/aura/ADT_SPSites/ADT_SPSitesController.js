({
     shrinkCol: function(component, event, helper){
        var val = event.target.id;
        component.set("v.showDetails", "false");
        component.set("v.recToExpand", val);
    },
    expandCol: function(component, event, helper){
        var val = event.target.id;
        component.set("v.recToExpand", val);
        component.set("v.showDetails", "true");   
    }
})