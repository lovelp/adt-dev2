({    
	doInit : function(component, event, helper) {
        component.set("v.loadingSpinner", true);
        var recId = component.get("v.LeadRecordId");
        console.log(recId);
		var action = component.get('c.assignToManager');        
        action.setParams({ recId: recId});
        action.setCallback(this,function(response){
        	var state = response.getState();
        	var responseMessage = response.getReturnValue();
        	if(state === 'SUCCESS'){
        		if(response.getReturnValue().includes('Success')){
        			console.log('#### success state');                          	
	            }else{
	                console.log('#### error state');
	            	component.set("v.errMsgBoolean", true);
	                component.set("v.loadingSpinner", false);
	            	component.set("v.errorMsg", responseMessage);
	            }
        	}
        	else if(state === 'ERROR'){
        		console.log('#### error state 2');
                component.set("v.errMsgBoolean", true);
                component.set("v.loadingSpinner", false);
            	component.set("v.errorMsg", "Failed to assign to the manager.");  
        	}
	        else{
	        	// default messaging here 
	            console.log('#### default');
	            component.set("v.errMsgBoolean", true);
	            component.set("v.loadingSpinner", false);
	            component.set("v.errorMsg", "Failed to assign to the manager.");    
	          }
         });
         $A.enqueueAction(action);       
    }
})