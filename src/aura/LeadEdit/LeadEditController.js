({
    reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
        console.log('reinit fired');
    },
    
    formatPhone:function(component, event, helper) {
        var phoneValue = component.find("phoneId").get("v.value");
        console.log(phoneValue);
        if(phoneValue.length > 3 && phoneValue.length < 6 && (!phoneValue.includes("(") &&  !phoneValue.includes(")"))){
            component.find("phoneId").set("v.value","("+phoneValue.substr(0,3)+") "+phoneValue.substr(3,phoneValue.length));
        }
        if(phoneValue.length > 9 && !phoneValue.includes("-")){
            component.find("phoneId").set("v.value",phoneValue.substr(0,9)+"-"+phoneValue.substr(9,phoneValue.length));
        }
        if(phoneValue.length < 3 && (phoneValue.includes("(") || phoneValue.includes(")"))){
            component.find("phoneId").set("v.value",phoneValue.substr(1,phoneValue.length));
        }
    },
    doInit : function(component, event, helper) {
        var resLead;
        var leadIdValue = component.get("v.pageReference").state.c__leadId;
        var getExistingLeadDetails;
        var action;
        var getUser1;
        var usr;
        if(leadIdValue){
            console.log('lead exists');
            component.set("v.isUpdateContext",true);
            getExistingLeadDetails = component.get('c.getExistingLead');
            getExistingLeadDetails.setParams({
                "leadIdValue": leadIdValue
            });
            
            getExistingLeadDetails.setCallback(this, function(response) {
                var state = response.getState();
                if (state == "SUCCESS") {
                    console.log('existing lead details');
                    resLead = response.getReturnValue();
                    console.log(resLead);
                    component.set("v.LeadCreation", resLead);                        
                }
            });
            
            var allValues;
            //add all values for channel list 
            action = component.get("c.getPickListValuesList");
            action.setParams({
                objectType: "Lead",
                selectedField: "Channel__c"
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                allValues = response.getReturnValue();
                if(state === "SUCCESS"){
                    console.log(allValues);
                    component.set("v.channelPicklistValues", allValues);
                    var opts = [];
                    if(resLead.Business_Id__c.includes('1100')){
                        component.set("v.businessIdValueResi",true);
                        component.set("v.businessIdValueSB",false);
                    }
                    else{
                        component.set("v.businessIdValueResi",false);
                        component.set("v.businessIdValueSB",true);
                    }
                    for(var i=0; i < allValues.length;i++){
                        opts.push(allValues[i]);
                    }
                    component.set("v.disabledChannel",false);
                    component.set("v.businessIdChannel",opts);
                    component.set("v.errMsgBoolean",false); 
                    component.find("businessIDSelect").set("v.value", resLead.Business_Id__c);
                    component.find("channel").set("v.value", resLead.Channel__c);
                    component.find("street2CustomId").set("v.value",resLead.SiteStreet2__c);
                    //component.set(v.LeadCreation.SiteStateProvince__c ,resLead.SiteStateProvince__c);
                }
            }); 
            
        }
        else{
            console.log('lead not exists');
            component.set("v.isSaveContext",true);
            getUser1 = component.get('c.getUser');
            
            getUser1.setCallback(this, function(response) {
                var state = response.getState();
                if (state == "SUCCESS") {
                    console.log('User details');
                    usr = response.getReturnValue();
                    console.log(usr.Business_Unit_Calculated__c);
                    if(usr.Business_Unit_Calculated__c){
                        component.find("businessIDSelect").set("v.value", usr.Business_Unit_Calculated__c);
                        if(usr.Business_Unit_Calculated__c.includes('Commercial')){
                            //Changed for National Account Sales
                            if(usr.Business_Unit__c.includes('Business')){
                                component.find("channel").set("v.value", "Business Direct Sales");
                            }else if(usr.Business_Unit__c.includes('National')){
                                component.find("channel").set("v.value", "National Account Sales");
                            }
                        }
                    }
                    else{
                        component.find("businessIDSelect").set("v.value", "1100 - Residential");
                    }
                }
            });
            var allValues;
            var channelValue;
            //add all values for channel list 
            action = component.get("c.getPickListValuesList");
            action.setParams({
                objectType: "Lead",
                selectedField: "Channel__c"
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                allValues = response.getReturnValue();
                if(state === "SUCCESS"){
                    console.log('set the values of channel'+allValues);
                    component.set("v.channelPicklistValues", allValues);
                    var opts = [];
                    for(var i=0; i < allValues.length;i++){
                        //if((allValues[i].indexOf("Business ") > -1)){
                        opts.push(allValues[i]);
                        //}
                    }
                    component.set("v.businessIdChannel",opts);
                    //assign channel
                    var sel = component.find("businessIDSelect");
                    var nav = sel.get("v.value");
                    console.log('### selected value '+nav);
                    if(nav == "1100 - Residential"){
                        console.log('inside resi');
                        component.set("v.businessIdValueResi",true);
                        component.set("v.businessIdValueSB",false);
                        component.find("channel").set("v.value", "Resi Resale");
                    }else if(nav == "1200 - Small Business"){
                        console.log('inside smb');
                        component.set("v.businessIdValueResi",false);
                        component.set("v.businessIdValueSB",true);
                        component.find("channel").set("v.value", "SB Resale");
                    }else if(nav == "1300 - Commercial"){
                        console.log('inside commercial');
                        component.set("v.businessIdValueResi",false);
                        component.set("v.businessIdValueSB",true);
                        //component.find("channel").set("v.value", "Business Direct Sales");
                        if(usr.Business_Unit_Calculated__c){
                            if(usr.Business_Unit_Calculated__c.includes('Commercial')){
                                //Changed for National Account Sales
                                if(usr.Business_Unit__c.includes('Business')){
                                    component.find("channel").set("v.value", "Business Direct Sales");
                                    component.set("v.channelValues","Business Direct Sales")
                                }else if(usr.Business_Unit__c.includes('National')){
                                    component.find("channel").set("v.value", "National Account Sales");
                                     component.set("v.channelValues","National Account Sales");
                                }
                            }
                        }
                    }else{
                        console.log('inside no match');
                        var opts = [];
                        component.set("v.disabledChannel",true);
                        component.set("v.businessIdChannel",opts);
                    }
                    
                    var a = component.get('c.onBusinessIdChange');
                    $A.enqueueAction(a);
                    
                }
            }); 
            
        }
        if(leadIdValue){
            $A.enqueueAction(getExistingLeadDetails);
        }
        else{
            $A.enqueueAction(getUser1);   
        }
        $A.enqueueAction(action);
        
    },
    doSubmit : function(component, event, helper) {
        var isValid = true;
        //Address Validation
        var addrVal = component.find("addrId");
        var cityValue = addrVal.get("v.city");
        var street1Value = addrVal.get("v.street");
        var pCodeValue = addrVal.get("v.postalCode");
        //End
        //Phone Validation
        var phoneId = component.find("phoneId");
        var phoneVal = phoneId.get("v.value");
        var bId = component.find("businessIDSelect");
        var bIdValue = bId.get("v.value");
        console.log('### on submit value of bus is'+bIdValue);
        if($A.util.isEmpty(bIdValue)){
            $A.util.addClass(bId, 'slds-has-error');
            isValid = false;
            component.set("v.errMsgBoolean",true);
        }else{
            $A.util.removeClass(bId, 'slds-has-error');
            component.set("v.errMsgBoolean",false);
            if((bIdValue == '1200 - Small Business')||(bIdValue == "1300 - Commercial")){
                var lName = component.find("lNameIdSB");
                var lNameValue = lName.get("v.value"); 
                var cName = component.find("cNameId");
                var cNameValue = cName.get("v.value");
                if($A.util.isEmpty(lNameValue)){                   
                    $A.util.addClass(lName, 'slds-has-error');
                    isValid = false;
                    component.set("v.errMsgBoolean",true);
                }else if(isValid){
                    $A.util.removeClass(lName, 'slds-has-error');
                    isValid = true;
                    component.set("v.errMsgBoolean",false);
                }
                if($A.util.isEmpty(cNameValue)){
                    $A.util.addClass(cName, 'slds-has-error');
                    isValid = false;
                    component.set("v.errMsgBoolean",true);
                }else if(isValid){
                    $A.util.removeClass(cName, 'slds-has-error');
                    isValid = true;
                    component.set("v.errMsgBoolean",false);
                }
                if(($A.util.isEmpty(cityValue))||($A.util.isEmpty(street1Value))||($A.util.isEmpty(pCodeValue))){
                    component.set("v.errMsgBoolean",true);
                    $A.util.addClass(addrVal, 'slds-has-error');
                    isValid = false;
                }else if(isValid){
                    $A.util.removeClass(addrVal, 'slds-has-error');
                    isValid = true;
                    component.set("v.errMsgBoolean",false);
                }
            }
            else if(bIdValue == '1100 - Residential'){  
                console.log('inside residential');
                var lName = component.find("lNameId");
                console.dir(lName);
                var lNameValue = lName.get("v.value");
                var fName = component.find("fNameId");
                var fNameValue = fName.get("v.value"); 
                console.log(fName+fNameValue);
                if($A.util.isEmpty(fNameValue)){
                    component.set("v.errMsgBoolean",true);
                    $A.util.addClass(fName, 'slds-has-error');
                    isValid = false;
                }else if(isValid){
                    $A.util.removeClass(fName, 'slds-has-error');
                    isValid = true;
                    component.set("v.errMsgBoolean",false);
                }
                if($A.util.isEmpty(lNameValue)){
                    $A.util.addClass(lName, 'slds-has-error');
                    isValid = false;
                    component.set("v.errMsgBoolean",true);
                }else if(isValid){
                    $A.util.removeClass(lName, 'slds-has-error');
                    isValid = true;
                    component.set("v.errMsgBoolean",false);
                }
            }
            if($A.util.isEmpty(phoneVal)){
                component.set("v.phnMsgBoolean",false);
            }else{
                if(phoneVal.length < 14){
                    $A.util.addClass(phoneId, 'slds-has-error');
                    isValid = false;
                    component.set("v.phnMsgBoolean",true);
                }else{
                    $A.util.removeClass(phoneId, 'slds-has-error');
                    isValid = true;
                    component.set("v.phnMsgBoolean",false);
                }
            }
        }
        if(isValid){ 
            component.set("v.loadingSpinner",true);
            var addressParameters = component.find("addrId");
            var street2 = component.get("v.street2");
            var overrideBtn = component.get("v.OverRideButton");
            var leadSearchCon = component.get("v.LeadCreation");
            var action = component.get('c.createLead');        
            action.setParams({ leadSearch : leadSearchCon,
                              street1 : addressParameters.get('v.street'),
                              street2 : component.get("v.street2"),
                              city : addressParameters.get('v.city'),
                              state : addressParameters.get('v.province'),
                              postalCode : addressParameters.get('v.postalCode'),
                              isOverRide : overrideBtn
                             });
            action.setCallback(this,function(response){
                console.log(overrideBtn);
                var state = response.getState();
                var returnValue = response.getReturnValue();
                console.log(state+returnValue);
                if(state === "SUCCESS"){
                    if(returnValue.includes("true")){
                        component.set("v.loadingSpinner",false);
                    }else if(returnValue.includes("Error")){
                        component.set("v.OverRideButton",true);
                        component.set("v.ErrorDisplay",returnValue);
                        component.set("v.loadingSpinner",false);
                        component.set("v.errMsgCntrl",true);
                    } else{
                        component.set("v.loadingSpinner",false);
                        component.set("v.ErrorDisplay",returnValue);
                        component.set("v.errMsgCntrl",true);
                    }
                }else if(state === "ERROR"){
                    component.set("v.loadingSpinner",false);
                    component.set("v.ErrorDisplay",'Cannot Save! Please contact Salesforce Support Team.');
                }
                
            });
            $A.enqueueAction(action);
        }
    },
    clearObjectValues: function (component, event, helper){
        var url = window.location.href; 
        var value = url.substr(0,url.lastIndexOf('/') + 1);
        window.history.back();
        return false;
    },
    onBusinessIdChange: function (component,event,helper){
        var sel = component.find("businessIDSelect");
        var nav = sel.get("v.value");
        component.find("businessIDSelect").set("v.value", nav);
        var allValues = component.get("v.channelPicklistValues");
        var opts = [];
        if(nav == "1100 - Residential"){
            console.log('inside residential onchange');
            component.set("v.businessIdValueSB",false);
            component.set("v.businessIdValueResi",true);
            for(var i=0; i < allValues.length;i++){
                if((allValues[i].indexOf("Resi") > -1) || allValues[i].indexOf("Home") > -1 || allValues[i].indexOf("HOA") > -1){
                    opts.push(allValues[i]);
                }
            }
            //component.find("channel").set("v.value", "Resi Resale");
            component.set("v.disabledChannel",false);
            component.set("v.businessIdChannel",opts);
            component.set("v.errMsgBoolean",false);
            component.set("v.errMsgCntrl",false);
        }else if(nav == "1200 - Small Business"){
            component.set("v.businessIdValueResi",false);
            component.set("v.businessIdValueSB",true);
            for(var i=0; i < allValues.length;i++){
                if((allValues[i].indexOf("SB") > -1)){
                    opts.push(allValues[i]);
                }
            }
            //component.find("channel").set("v.value", "SB Resale");
            component.set("v.disabledChannel",false);
            component.set("v.businessIdChannel",opts);
            component.set("v.errMsgBoolean",false);
            component.set("v.errMsgCntrl",false);
        }else if(nav == "1300 - Commercial"){
            component.set("v.businessIdValueResi",false);
            component.set("v.businessIdValueSB",true);
            for(var i=0; i < allValues.length;i++){
                if((allValues[i].indexOf("Business") > -1) || allValues[i].indexOf("National") > -1){
                    opts.push(allValues[i]);
                }
            }
            var channelValue = component.get("v.channelValues");
            if($A.util.isUndefined(channelValue)){
                component.find("channel").set("v.value", "Business Direct Sales");
            }else{
                component.find("channel").set("v.value", channelValue);
            }
            component.set("v.disabledChannel",false);
            component.set("v.businessIdChannel",opts);
            component.set("v.errMsgBoolean",false);
            component.set("v.errMsgCntrl",false);
        }else{
            var opts = [];
            component.set("v.disabledChannel",true);
            component.set("v.businessIdChannel",opts);
        }
    },
    doSearch : function(component, event, helper){
        component.set("v.showSaveUpdate",true);
        var addressParameters = component.find("addrId");
        var leadSearchCon = component.get("v.LeadCreation");
        var action = component.get('c.searchLead'); 
        action.setParams({ leadSearch : leadSearchCon,
                          street1 : addressParameters.get('v.street'),
                          street2 : component.get("v.street2"),
                          city : addressParameters.get('v.city'),
                          state : addressParameters.get('v.province'),
                          country : addressParameters.get('v.country'),
                          postalCode : addressParameters.get('v.postalCode')
                         });
        action.setCallback(this,function(response){
            var state = response.getState();
            var returnValue = response.getReturnValue();
            if(state === "SUCCESS"){
                var leadSearchAppEvent = $A.get("e.c:LeadSearchEvent");
                leadSearchAppEvent.setParams({"leadSearchAttributes" : returnValue}); 
                leadSearchAppEvent.fire();
                console.log('event fired'+returnValue);
            }
        });
        $A.enqueueAction(action);
    },
    redirectCntrl:function(component, event, helper){
        alert("hello");
    },
    doUpdate : function(component,event,helper){
        // controller method to update lead
        var isValid = true;
        //Address Validation
        var addrVal = component.find("addrId");
        var cityValue = addrVal.get("v.city");
        var street1Value = addrVal.get("v.street");
        var pCodeValue = addrVal.get("v.postalCode");
        //End
        //Phone Validation
        var phoneId = component.find("phoneId");
        var phoneVal = phoneId.get("v.value");
        var bId = component.find("businessIDSelect");
        var bIdValue = bId.get("v.value");
        console.log('### on submit value of bus is'+bIdValue);
        if($A.util.isEmpty(bIdValue)){
            $A.util.addClass(bId, 'slds-has-error');
            isValid = false;
            component.set("v.errMsgBoolean",true);
        }else{
            $A.util.removeClass(bId, 'slds-has-error');
            //isValid = true;
            component.set("v.errMsgBoolean",false);
            if(bIdValue == '1100 - Residential'){               
                var fName = component.find("fNameId");
                var fNameValue = fName.get("v.value");   
                var lName = component.find("lNameId");
                var lNameValue = lName.get("v.value");   
                if($A.util.isEmpty(fNameValue)){
                    component.set("v.errMsgBoolean",true);
                    $A.util.addClass(fName, 'slds-has-error');
                    isValid = false;
                }else if(isValid){
                    $A.util.removeClass(fName, 'slds-has-error');
                    isValid = true;
                    component.set("v.errMsgBoolean",false);
                }
                if($A.util.isEmpty(lNameValue)){
                    $A.util.addClass(lName, 'slds-has-error');
                    isValid = false;
                    component.set("v.errMsgBoolean",true);
                }else if(isValid){
                    $A.util.removeClass(lName, 'slds-has-error');
                    isValid = true;
                    component.set("v.errMsgBoolean",false);
                }
            }else if((bIdValue == '1200 - Small Business')||(bIdValue == "1300 - Commercial")){
                var lName = component.find("lNameIdSB");
                var lNameValue = lName.get("v.value"); 
                var cName = component.find("cNameId");
                var cNameValue = cName.get("v.value");
                if($A.util.isEmpty(lNameValue)){                   
                    $A.util.addClass(lName, 'slds-has-error');
                    isValid = false;
                    component.set("v.errMsgBoolean",true);
                }else if(isValid){
                    $A.util.removeClass(lName, 'slds-has-error');
                    isValid = true;
                    component.set("v.errMsgBoolean",false);
                }
                if($A.util.isEmpty(cNameValue)){
                    $A.util.addClass(cName, 'slds-has-error');
                    isValid = false;
                    component.set("v.errMsgBoolean",true);
                }else if(isValid){
                    $A.util.removeClass(cName, 'slds-has-error');
                    isValid = true;
                    component.set("v.errMsgBoolean",false);
                }
                if(($A.util.isEmpty(cityValue))||($A.util.isEmpty(street1Value))||($A.util.isEmpty(pCodeValue))){
                    component.set("v.errMsgBoolean",true);
                    $A.util.addClass(addrVal, 'slds-has-error');
                    isValid = false;
                }else if(isValid){
                    $A.util.removeClass(addrVal, 'slds-has-error');
                    isValid = true;
                    component.set("v.errMsgBoolean",false);
                }
            }
            if($A.util.isEmpty(phoneVal)){
                component.set("v.phnMsgBoolean",false);
            }else{
                if(phoneVal.length < 14){
                    $A.util.addClass(phoneId, 'slds-has-error');
                    isValid = false;
                    component.set("v.phnMsgBoolean",true);
                }else{
                    $A.util.removeClass(phoneId, 'slds-has-error');
                    isValid = true;
                    component.set("v.phnMsgBoolean",false);
                }
            }
        }
        if(isValid){
            var action = component.get('c.doLeadUpdate'); 
            var street2 = component.get("v.street2");
            var overrideBtn = component.get("v.OverRideButtonUpdate");
            var leadToUpdate = component.get("v.LeadCreation");
            var action = component.get('c.doLeadUpdate');
            leadToUpdate.SiteStreet2__c = street2;
            action.setParams({ leadToUpdate : leadToUpdate,
                              isOverRide : overrideBtn
                             });
            action.setCallback(this,function(response){
                var state = response.getState();
                var returnValue = response.getReturnValue();
                if(state === "SUCCESS"){
                    console.log('success'+returnValue);
                    //redirect to the updated lead in the apex controller
                    if(returnValue.includes("true")){
                        //component.set("v.loadingSpinner",false);
                    }else if(returnValue.includes("Error")){
                        component.set("v.OverRideButtonUpdate",true);
                        component.set("v.ErrorDisplay",returnValue);
                        component.set("v.loadingSpinner",false);
                        component.set("v.errMsgCntrl",true);
                    } else{
                        component.set("v.loadingSpinner",false);
                        component.set("v.ErrorDisplay",returnValue);
                        component.set("v.errMsgCntrl",true);
                    }
                }
                else if(state === "ERROR"){
                    component.set("v.loadingSpinner",false);
                    component.set("v.ErrorDisplay",'Cannot Save! Please contact Salesforce Support Team.');
                }
            });
            $A.enqueueAction(action);
        }  
    }
})