/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PendingDiscoArchiveBatchTest {
	
	static testMethod void testExecutePendingDiscoToRif() {
        
        Id rifOwnerUser = Utilities.getRIFAccountOwner();
		RecordType rifAcctRecType = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account');
        
        User admin1 = TestHelperClass.createUser(100);
        User admin2 = TestHelperClass.createUser(200);
         
        Account a;
        
		System.runAs(admin1) {
			a = TestHelperClass.createAccountData();
        	a.Type = IntegrationConstants.TYPE_PENDING_DISCO;
        	a.Data_Source__c = IntegrationConstants.DATA_SOURCE_MASTERMIND_BUSINESS;
        	a.LeadStatus__c = 'Active';
            a.RecordTypeId = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account').Id;
        	update a;
		}
        
        Test.startTest();
        
        System.runAs(admin2) {
        	PendingDiscoArchiveBatch pdab = new PendingDiscoArchiveBatch();
        	pdab.query = 'select id, LeadStatus__c, Type, Data_Source__c, OwnerId, ProcessingType__c, RecordTypeId, UnassignedLead__c, NewLead__c from Account where id =\'' + a.Id + '\'';
       		Database.executeBatch(pdab);
        }	
            
        Test.stopTest();
        
        Account aAfter = [select id, LeadStatus__c, Type, Data_Source__c, OwnerId, ProcessingType__c, RecordTypeId, UnassignedLead__c, NewLead__c from Account where id = : a.Id];
       
        System.assert(aAfter != null, 'Account should exist');
        System.assertEquals(IntegrationConstants.TYPE_RIF, aAfter.Type);
        System.assertEquals(IntegrationConstants.PROCESSING_TYPE_RIF, aAfter.ProcessingType__c);
        System.assertEquals(IntegrationConstants.DATA_SOURCE_GDW, aAfter.Data_Source__c);
        System.assertEquals('Active', aAfter.LeadStatus__c);
        System.assertEquals(rifAcctRecType.Id, aAfter.RecordTypeId);
        System.assertEquals(rifOwnerUser, aAfter.OwnerId);
        System.assert(aAfter.UnassignedLead__c, 'Account UnassignedLead should be true');
        System.assert(aAfter.NewLead__c, 'Account NewLead/Non-worked Indicator should be true');
    }
    
    static testMethod void testExecutePendingDiscoToArchived() {
        
        User admin1 = TestHelperClass.createUser(100);
        User admin2 = TestHelperClass.createUser(200);
         
        Account a;
        
		System.runAs(admin1) {
			a = TestHelperClass.createAccountData();
        	a.Type = IntegrationConstants.TYPE_PENDING_DISCO;
        	a.Data_Source__c = 'ADMIN';
        	a.LeadStatus__c = 'Active';
            a.RecordTypeId = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account').Id;
        	update a;
		}
        
        Test.startTest();
        
        System.runAs(admin2) {
        	PendingDiscoArchiveBatch pdab = new PendingDiscoArchiveBatch();
        	pdab.query = 'select id, LeadStatus__c, Type, Data_Source__c, OwnerId, ProcessingType__c, RecordTypeId, UnassignedLead__c, NewLead__c from Account where id =\'' + a.Id + '\'';
       		Database.executeBatch(pdab);
        }	
            
        Test.stopTest();
        
        Account aAfter = [select id, LeadStatus__c, Type, Data_Source__c, OwnerId, ProcessingType__c, RecordTypeId, UnassignedLead__c, NewLead__c from Account where id = : a.Id];
       
        System.assert(aAfter != null, 'Account should exist');
        System.assertEquals(IntegrationConstants.TYPE_PENDING_DISCO, aAfter.Type);
        System.assertEquals('ADMIN', aAfter.Data_Source__c);
        System.assertEquals('Archived', aAfter.LeadStatus__c);
        
    }

}