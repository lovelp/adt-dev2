/************************************* MODIFICATION LOG ********************************************************************************************
* NewMoverScheduler
*
* DESCRIPTION : A schedulable class that initiates the batch class NewMoverBatch
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover            		3/12/2012			- Original Version
*
*													
*/

global class NewMoverScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		NewMoverBatch nmb = new NewMoverBatch();
		Database.executeBatch(nmb);
	}
}