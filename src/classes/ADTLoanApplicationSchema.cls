/************************************* MODIFICATION LOG ********************************************************************************************
*
* DESCRIPTION : EDS API Schema
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                   TICKET              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Varun Sinha               11/29/2018                                 - Original Version
*/
public class ADTLoanApplicationSchema {
    public String leadManagementId;
    public string jobNumber;
    public string orderNumber;
    public boolean threePayAllowed;
    public boolean thirdPartyAllowed;
    public boolean adtFinancingAllowed;
    public integer minFinancingAmountADT;
    public integer minFinancingAmountThirdParty;
    public integer maxFinancingAmount;
    public string thirdPartyCustomerNumber;
    public Decimal currentLoanAmount;
    public decimal currentMonthlyPayment;
    public datetime tilaPnoteAcceptedDtTm;
    public datetime lastZootUpdatedDtTm;
    public datetime lastUpdatedDtTm;
    public decimal term;
    public dateTime loanAcceptedDateTime;
    public string message;
    public string loanApplicationNumber;
    public string submittedUserDepartment;
    public string requestStatusDescription;
    public string requestStatusCode;
    public string email;
    public class LoanApplicationRetrieveRequest{
        public string customerNumber;
        public string siteNumber;
        public string leadManagementId;
        public string jobNumber;
        public string orderNumber;
        public string ssnLastFour;
        public string dateOfBirth;
        public string requestor;
        public string action;
    }
    public class LoanApplicationUpdateRequest{
        public string customerNumber;
        public string siteNumber;
        public integer term;
        public decimal amtFinanced;
        public decimal salesTaxAmt;	
        public decimal purchasePriceAmt;
        public string jobNumber;
        public string orderNumber;
        public string leadManagementId;
        public dateTime tilaPnoteAcceptedDtTm;
        public boolean needRemoteSigning;
        public string email;
        public boolean jobClosed;
        public string action;
        public string requestor;
        public string message;
    }
    public class MessageResponse{
        public String message;
        public string errorCode;
    }
}