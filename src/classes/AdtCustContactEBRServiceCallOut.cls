/************************************* MODIFICATION LOG ********************************************************************************************
* AdtCustContactEBRServiceBatch
*
* 
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Viraj Shah                    20/09/2019              - Original Version

*/
global with sharing class AdtCustContactEBRServiceCallOut {

    public static RequestQueue__c doPossibleNowGryphonCallout(RequestQueue__c req){
        Httprequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        try{            
            String userName = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPUsername__c:'abc';
            String password = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPPassword__c:'def';
            String endPointURL = (!Test.isRunningTest())? IntegrationSettings__c.getInstance().DpPossibleNowGryphonEndPoint__c:'xyz';
            Blob headerValue = Blob.valueOf(userName + ':' + password);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            request.setHeader('Authorization', authorizationHeader);
            request.setHeader('Content-Type', 'application/json');
            request.setEndpoint(endPointURL);
            request.setTimeout(Integer.ValueOf(120000));
            request.setMethod('POST');
            request.setbody(req.ServiceMessage__c);
            // Send the request
            response = http.send(request);
        }catch(exception e){
            // error response
            req.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            req.ErrorDetail__c = 'Exception: '+ e.getMessage();
            req.Counter__c += 1;
            return req;
        }
        if(response != null && (response.getStatusCode() == 202 || response.getStatusCode() == 204)){
            // success response
            req.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_SUCCESS;
            req.ErrorDetail__c = null;
        }else{
            // error response
            req.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            req.ErrorDetail__c = '\n Response: ';
            req.ErrorDetail__c += (response != null && response.getBody() != null)? response.getBody(): 'response is invalid';
            req.Counter__c += 1;
        }
        return req;
    }
}