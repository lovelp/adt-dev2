/**
 Description- This test class used for DOBinformationAction.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class DOBinformationActionTest {

    static testMethod void testMethod1() {
    
            List<Id> quoteIdList= new List<Id>();
            
            test.startTest();
            
            List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
            rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
            rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
    
            insert rgvList;
            
            IntegrationSettings__c is=new IntegrationSettings__c();
            is.MMBSearchEndpoint__c = 'https://dev.api.adt.com/adtclup';
            is.MMBSearchUsername__c = 'salesforceclup';
            is.MMBSearchPassword__c = 'abcd1234';
            is.MMBSearchCalloutTimeout__c = 60000;
            insert is;  
      
            Account a1 = TestHelperClass.createAccountData();
            a1.DOB_encrypted__c = '10/10/2016';
            update a1;
            
            Opportunity opp = New Opportunity();
            opp.AccountId = a1.Id;
            opp.Name = 'TestOpp1';
            opp.StageName = 'Prospecting';
            opp.CloseDate = Date.today();
            insert opp;
                 
            scpq__SciQuote__c q = new scpq__SciQuote__c();
            q.Name = 'Test Quote 1';
            q.Account__c = a1.Id;
            q.scpq__OpportunityId__c = opp.id;
            q.scpq__OrderHeaderKey__c = 'abcxyz';
            q.scpq__SciLastModifiedDate__c = Date.today();
            q.scpq__QuoteId__c = '783188231';
            q.MMBJobNumber__c = '213561';
            q.createdDate = Date.today()-2;
            q.MMBOrderType__c = 'test';
            q.MMBOutOrderType__c = 'test out';
            q.Order_Source__c = 'FIELD';
            q.scpq__TotalDiscountPercent__c = 16.00;
            insert q;
            
            scpq__SciQuote__c scObj= [select id from scpq__SciQuote__c where id=: q.id limit 1];
            system.assertEquals(scObj.Id, q.Id); 
            quoteIdList.add(q.id);
            Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteSearchApiGenerator()); 
            
            test.stopTest();
            DOBinformationAction.getQuoteinfo(quoteIdList);
    }
    
}