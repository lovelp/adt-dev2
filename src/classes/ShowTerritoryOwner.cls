/************************************* MODIFICATION LOG ********************************************************************************************
* ShowTerritoryOwner
*
* DESCRIPTION : Supports the display of the owner of a territory
*               It is a Controller Extension in the SFDC MVC architecture.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2011			- Original Version
*
*													
*/

public with sharing class ShowTerritoryOwner {
	public string OwnerName {get; set;}

	public ShowTerritoryOwner(ApexPages.StandardController controller)
	{
		Id territoryId = controller.getId();
		Territory__c TerrInfo = [select ownerid from Territory__c where id = : territoryId limit 1];
		
		User owner = [select name from User where id = : TerrInfo.OwnerId Limit 1];
		OwnerName = owner.Name;
	}	
}