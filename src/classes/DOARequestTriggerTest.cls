/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class DOARequestTriggerTest {
    
    static testMethod void testInsert() {
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
            TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        User salesRep = TestHelperClass.createSalesRepUser();
        Account a;
        Opportunity opp;
        scpq__SciQuote__c q = New scpq__SciQuote__c();
        
        System.runAs(salesRep) {
            
            // create accounts
            ResaleGlobalVariables__c resGloFin = new ResaleGlobalVariables__c();
            resGloFin.Name = 'DOA_VP_Finance';
            resGloFin.Value__c = '005a000000AoMyB';
            insert resGloFin;
            ResaleGlobalVariables__c resGloDef = new ResaleGlobalVariables__c();
            resGloDef.Name = 'DOA_VP_Default';
            resGloDef.Value__c = '00530000005hzxK';
            insert resGloDef;
            BatchGlobalVariables__c batGlo = new BatchGlobalVariables__c();
            batGlo.Name = 'GlobalAdminId';
            batGlo.Value__c = '00530000005dBvw';
            insert batGlo;
            a = TestHelperClass.createAccountData();
            opp = New Opportunity();
            opp.AccountId = a.Id;
            opp.Name = 'TestOpp1';
            opp.StageName = 'Prospecting';
            opp.CloseDate = Date.today();
            insert opp;
            q.Name = 'TestQuote1';
            q.scpq__OpportunityId__c = opp.Id;
            q.scpq__OrderHeaderKey__c = 'whoknows';
            q.scpq__SciLastModifiedDate__c = Date.today();
            q.scpq__Status__c = 'Created';
            q.Activity_Data__c = '<?xml version="1.0" encoding="UTF-8"?><Activity/>';
            q.Job_Details__c = '<?xml version="1.0" encoding="UTF-8"?> <Jobs DOAApprovalLockID="6182cad0-2bd2-4bd1-be59-b53c3913cefd" ' +
                'DOALock="N" DepositWaived="N" DepositWaiverLock="Y" EasyPay="N"> <Job ADSCAddOn="0.00" ADSCBase="1,599.00" ADSCCorpDiscount="0.00" ADSCInstallPrice="1,519.05" ADSCOtherAddOn="0.00" ' +
                'ADSCOtherCorpDiscount="-79.95" ANSCAdditionalMonitoring="0.00" ANSCAdditionalQSP="0.00" ANSCBaseMonitoring="0.00" ANSCBaseQSP="20.00" ANSCCorpDiscount="0.00" ANSCFinalRMR="20.00" ' +
                'ANSCOtherCorpDiscount="0.00" DOAApprovalComment="test" DOAApproverLevel="Regional Sales Director" DOALock="N" DefaultContract="SB017" JobNo="" JobType="INST" ProductFamily="AIPhone" RequireElectrician="N" ' +
                'RequireLocksmith="N" SaleType="" TaskCode="SCA" TechnologyId="CARD"> <Packages> <Package ID="6403 BUSI OS" Name="Best Package - Audio and Video"/> </Packages> ' +
                '<Promotions/> <JobLines> <JobLine Item="JFS-2AED" Name="Audio/Video Boxed Set" Package="6403 BUSI OS" PackageQty="1.00"/> <JobLine Item="487343" ' +
                'Name="100 Cable - 22/4, PFL/CM, NP" Package="6403 BUSI OS" PackageQty="1.00"/> </JobLines> </Job> </Jobs>';
            q.DOALockID__c = '1234565';
            q.CreditBypassRequested__c = false;
            q.Rep_User__c = UserInfo.getUserId();
            insert q;
            q = [Select Id, DOALockID__c, scpq__QuoteId__c, CreditBypassRequested__c From scpq__SciQuote__c Where Id =:q.Id];
            Test.startTest();

            DOA_Request__c doaR = new DOA_Request__c();
            doaR.Name = a.Name;
            doaR.Account__c = a.id;
            doaR.Sales_Rep__c = salesRep.Id;
            doaR.Quote__c = q.ID;
            doaR.Quote_ID__c = q.scpq__QuoteId__c;
            doaR.DOALockID__c = '123456';
            doaR.Deposit_Waiver_Lock__c = true;
            doaR.DOAApproverLevel__c = 'VP';
            doaR.DOA_Lock__c = true;
            doaR.Approver__c = salesRep.ID;
            doaR.OwnerID = salesRep.id;
            insert doaR;
                        
            DOA_Request__c doaR1 = new DOA_Request__c();
            doaR1.Name = a.Name;
            doaR1.Account__c = a.id;
            doaR1.Sales_Rep__c = salesRep.Id;
            doaR1.Quote__c = q.ID;
            doaR1.Quote_ID__c = q.scpq__QuoteId__c;
            //doaR1.DOALockID__c = '123456';
            doaR1.Deposit_Waiver_Lock__c = true;
            doaR1.DOAApproverLevel__c = 'VP';
            doaR1.DOA_Lock__c = true;
            doaR1.Approval_Status__c = 'Automatic';
            doaR1.DOALockID__c = q.DOALockID__c;
            doaR1.OwnerID = salesRep.id;
            insert doaR1;
            
            doaR1.Approval_for_No_Email_in_Account__c = true;
            doaR1.Approval_Status__c = 'Yes';
            update doaR1;
            
            doaR1.Approval_Status__c = 'No';
            update doaR1;
            
            doaR1.Approval_Status__c = 'Cancelled';
            update doaR1;
            
            q.CreditBypassRequested__c = true;
            update q;
            Test.stopTest();
        }
    }
    
    static testMethod void createDOARequestByRequestQueue() {
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
            TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        User salesRep = TestHelperClass.createSalesRepUser();
        Account a;
        Opportunity opp;
        scpq__SciQuote__c q = New scpq__SciQuote__c();
        
        System.runAs(salesRep) {
            a = TestHelperClass.createAccountData();
            opp = New Opportunity();
            opp.AccountId = a.Id;
            opp.Name = 'TestOpp1';
            opp.StageName = 'Prospecting';
            opp.CloseDate = Date.today();
            insert opp;
            
            q.Name = 'TestQuote1';
            q.scpq__OpportunityId__c = opp.Id;
            q.scpq__OrderHeaderKey__c = 'whoknows';
            q.scpq__SciLastModifiedDate__c = Date.today();
            q.scpq__Status__c = 'Created';
            q.Activity_Data__c = '<?xml version="1.0" encoding="UTF-8"?><Activity/>';
            q.Job_Details__c = '<?xml version="1.0" encoding="UTF-8"?> <Jobs DOAApprovalLockID="6182cad0-2bd2-4bd1-be59-b53c3913cefd" ' +
                'DOALock="N" DepositWaived="N" DepositWaiverLock="Y" EasyPay="N"> <Job ADSCAddOn="0.00" ADSCBase="1,599.00" ADSCCorpDiscount="0.00" ADSCInstallPrice="1,519.05" ADSCOtherAddOn="0.00" ' +
                'ADSCOtherCorpDiscount="-79.95" ANSCAdditionalMonitoring="0.00" ANSCAdditionalQSP="0.00" ANSCBaseMonitoring="0.00" ANSCBaseQSP="20.00" ANSCCorpDiscount="0.00" ANSCFinalRMR="20.00" ' +
                'ANSCOtherCorpDiscount="0.00" DOAApprovalComment="test" DOAApproverLevel="Regional Sales Director" DOALock="N" DefaultContract="SB017" JobNo="" JobType="INST" ProductFamily="AIPhone" RequireElectrician="N" ' +
                'RequireLocksmith="N" SaleType="" TaskCode="SCA" TechnologyId="CARD"> <Packages> <Package ID="6403 BUSI OS" Name="Best Package - Audio and Video"/> </Packages> ' +
                '<Promotions/> <JobLines> <JobLine Item="JFS-2AED" Name="Audio/Video Boxed Set" Package="6403 BUSI OS" PackageQty="1.00"/> <JobLine Item="487343" ' +
                'Name="100 Cable - 22/4, PFL/CM, NP" Package="6403 BUSI OS" PackageQty="1.00"/> </JobLines> </Job> </Jobs>';
            q.DOALockID__c = '1234565';
            q.CreditBypassRequested__c = false;
            q.Rep_User__c = UserInfo.getUserId();
            insert q;
            q = [Select Id, DOALockID__c, scpq__QuoteId__c, CreditBypassRequested__c From scpq__SciQuote__c Where Id =:q.Id];
            
            Test.startTest();
            DOA_Request__c doaR = new DOA_Request__c();
            doaR.Name = a.Name;
            doaR.Account__c = a.id;
            doaR.Sales_Rep__c = salesRep.Id;
            doaR.Quote__c = q.ID;
            doaR.Quote_ID__c = q.scpq__QuoteId__c;
            doaR.DOALockID__c = '123456';
            doaR.Deposit_Waiver_Lock__c = true;
            doaR.DOAApproverLevel__c = 'VP';
            doaR.DOA_Lock__c = true;
            doaR.Approver__c = salesRep.ID;
            doaR.OwnerID = salesRep.id;
            doaR.Approval_Status__c = 'Pending';
            insert doaR;
            // Test Regular DOA process
            DOARequestUtils.processRequestQueueForDOA();
            // Test Credit bypass DOA process
            q.CreditBypassRequested__c = true;
            update q;
            DOARequestUtils.processRequestQueueForDOA();
            Test.stopTest();
        }
    }

    // Covers DOARequestUtils.createNoEmailDOAReq
    public static testMethod void testcreateNoEmailDOAReqWithManager() {
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {
            TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        User salesRep = TestHelperClass.createSalesRepUser();
        User manager = TestHelperClass.createManagerUser();
        salesRep.managerId = manager.Id;
        update salesRep;
        
        System.runAs(salesRep) {
            Account a = new Account();
            a = TestHelperClass.createAccountData();
            a.No_Email_Available__c = true;
            update a;
            
            Test.startTest();
            DOARequestUtils.createNoEmailDOAReq(a,salesRep);
            Test.stopTest();
        }
    }
    
    // Covers DOARequestUtils.createNoEmailDOAReq
    public static testMethod void testcreateNoEmailDOAReqWithoutManager() {
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {
            TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        User salesRep = TestHelperClass.createSalesRepUser();
        System.runAs(salesRep) {
            Account a = new Account();
            a = TestHelperClass.createAccountData();
            a.No_Email_Available__c = true;
            update a;
            
            Test.startTest();
            DOARequestUtils.createNoEmailDOAReq(a,salesRep);
            Test.stopTest();
        }
    }
}