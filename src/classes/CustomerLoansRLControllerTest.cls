@isTest
private class CustomerLoansRLControllerTest {

	@TestSetup static void testData() {
        List<Equifax__c> elist = new List<Equifax__c>();
        elist.add(new Equifax__c(Name='Employee Condition Code', value__c='CME3'));
        elist.add(new Equifax__c(Name='Employee Risk Grade', value__c='U'));
        elist.add(new Equifax__c(Name='Default Condition Code', value__c='  CAE1'));
        elist.add(new Equifax__c(Name='Default Risk Grade', value__c='W'));
        insert elist;       
      
        // Batch Global Variables
        List<BatchGlobalVariables__c> batchGloballist = new List<BatchGlobalVariables__c>();
        batchGloballist.add(new BatchGlobalVariables__c(Name='LS_ADTEmployee', value__c='1'));
        batchGloballist.add(new BatchGlobalVariables__c(Name='LS_DataSourceExclusion', value__c='WEB'));
        batchGloballist.add(new BatchGlobalVariables__c(Name='LS_DaysBeforeReassignmentToField', value__c='1'));
        batchGloballist.add(new BatchGlobalVariables__c(Name='LS_NoOfMonthsOfRecordCreation', value__c='1'));
        insert batchGloballist;
        
        List<ResaleGlobalVariables__c> ResaleGlobalVariables = new List<ResaleGlobalVariables__c>();
        ResaleGlobalVariables.add(new ResaleGlobalVariables__c(Name = 'DataRecastDisableAccountTrigger', Value__c = 'FALSE'));
        ResaleGlobalVariables.add(new ResaleGlobalVariables__c(Name = 'DataConversion', Value__c = 'FALSE'));
        insert ResaleGlobalVariables;
     }
	
	private static testMethod void testCustomerLoansRLController() {
         //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
    
        insert addr;
        
        Account acc = TestHelperClass.createAccountData(addr, true);
        acc = new Account(Id = acc.Id);
        acc.MMBCustomerNumber__c = '12345678';
        update acc;
        
        CustomerLoan__c customerLoan = new CustomerLoan__c();
        customerLoan.CustomerNumber__c = '12345678';
        customerLoan.Active__c = true;
        customerLoan.QuoteOrderNumber__c = '123456';
        insert customerLoan;
        
        CustomerLoansRLController.getExistingLoans(acc.Id, false);
        
        CustomerLoansRLController.refreshRecords('12345678');
	}
	
	private static testMethod void testCustomerLoansRLController1() {
         //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
    
        insert addr;
        
        Account acc = TestHelperClass.createAccountData(addr, true);
        acc = new Account(Id = acc.Id);
        acc.MMBCustomerNumber__c = '12345678';
        update acc;
        
        CustomerLoan__c customerLoan = new CustomerLoan__c();
        customerLoan.CustomerNumber__c = '12345678';
        customerLoan.SiteNumber__c = '12345';
        customerLoan.Active__c = true;
        customerLoan.QuoteOrderNumber__c = '123456';
        customerLoan.SiteStatus__c = 'OUT';
        insert customerLoan;
        
        CustomerLoansRLController.getExistingLoans(acc.Id, false);
	}
	
	private static testMethod void testCustomerLoansRLController2() {
         //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
    
        insert addr;
        
        Account acc = TestHelperClass.createAccountData(addr, true);
        acc = new Account(Id = acc.Id);
        acc.MMBCustomerNumber__c = '12345678';
        update acc;
        
        CustomerLoan__c customerLoan = new CustomerLoan__c();
        customerLoan.CustomerNumber__c = '12345678';
        customerLoan.SiteNumber__c = '12345';
        customerLoan.Active__c = true;
        customerLoan.QuoteOrderNumber__c = '123456';
        customerLoan.SiteStatus__c = 'IN';
        
        CustomerLoan__c customerLoan1 = new CustomerLoan__c();
        customerLoan1.CustomerNumber__c = '12345678';
        customerLoan1.SiteNumber__c = '12345';
        customerLoan1.Active__c = true;
        customerLoan1.QuoteOrderNumber__c = '123456';
        customerLoan1.SiteStatus__c = 'IN';
        insert new List<CustomerLoan__c>{customerLoan, customerLoan1};
        
        CustomerLoansRLController.getExistingLoans(acc.Id, false);
	}
	
	private static testMethod void testCustomerLoansRLController3() {
         //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
    
        insert addr;
        
        Account acc = TestHelperClass.createAccountData(addr, true);
        acc = new Account(Id = acc.Id);
        acc.MMBCustomerNumber__c = '12345678';
        update acc;
        
        CustomerLoan__c customerLoan = new CustomerLoan__c();
        customerLoan.CustomerNumber__c = '123456789';
        customerLoan.SiteNumber__c = '12345';
        customerLoan.Active__c = true;
        customerLoan.QuoteOrderNumber__c = '123456';
        customerLoan.SiteStatus__c = 'IN';
        insert customerLoan;
        
        CustomerLoansRLController.getExistingLoans(acc.Id, true);
	}

}