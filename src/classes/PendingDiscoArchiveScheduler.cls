/************************************* MODIFICATION LOG ********************************************************************************************
* PendingDiscoArchiveScheduler
*
* DESCRIPTION : A schedulable class that initiates the batch class PendingDiscoArchiveBatch
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*              				 10/14/2011			- Original Version
*
*													
*/

global class PendingDiscoArchiveScheduler implements Schedulable {
	
   global void execute(SchedulableContext SC) {
		Id batchProcessId = Database.executebatch(new PendingDiscoArchiveBatch(), 100);
		
   }
}