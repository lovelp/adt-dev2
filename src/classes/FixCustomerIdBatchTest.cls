/*
 * Description: This is a test class for the following classes
 * 1.FixCustomerIdBatch
 * Created By: Mounika Anna
 *
 */
@isTest
public class FixCustomerIdBatchTest {
	 static  void createTestData() {
    //Create address first
     User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
            TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        Account a;
    
        System.runAs(current) { 
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            update u;
            
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
            insert addr;
            
            a = TestHelperClass.createAccountData(addr, true);
            a.FirstName__c = 'TestFName';
            a.LastName__c = 'TestLName';
            a.Name = 'TestFName TestLName';
            a.OwnerId = u.Id;
            a.LeadExternalID__c ='1234567-89';
            update a;	
        }
     }
    static testMethod void FixCustomerIdBatchTest(){
    	createTestData();
        test.startTest();
        Id batchId=Database.executeBatch(new FixCustomerIdBatch());
    	test.stopTest();
    }
    
}