/************************************* MODIFICATION LOG ********************************************************************************************
* EventTriggerHelper
*
* DESCRIPTION : Helper methods for event trigger
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE             TICKET         REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  1/27/2012                       - Original Version
* Sunil Addepalli               4/25/2012                       - Added conditional logic for phase 2 data conversion
                                                                (Account id/what id will be set based on the telemar account number field)
* Mounika Anna                  06/21/2018      HRM-7272        - DNIS Updates for Field                                        
*/

public without sharing class EventTriggerHelper {
    
    public static void processBeforeInsertEvents(List<Event> events) {
        validatePostalCode(events);
        setEventRecordType(events);
        setWhatIdForDataConversion(Events);
        updateEventAssignedTo(events, null);
        setEventReminder(events);
        updateDevicePhone(events);
        //HRM-7272 DNIS Updates for Field - Mounika Anna
        updateAccountDNISFields(events);
    }
    
    public static void processBeforeUpdateEvents(List<Event> events, Map<Id,Event> oldMap) {
        validatePostalCode(events);
        updateEventAssignedTo(events, oldMap);
        updateEventReminder(events);
        updateDevicePhone(events);
         //HRM-5893 - Added by TCS
         recprevSchTimeOnUpdate(events,oldMap);
    }

    /**
     * Handles after validation on event records
     *
     * @method processAfterInsertEvent
     *
     */
   
    public class EventDateTimeWrapper {
        Datetime startDateTime;
        DateTime endDateTime;
        Id evId;
         public EventDateTimeWrapper(DateTime stDt, DateTime endDt, Id eId){
             startDateTime = stDt;
             endDateTime = endDt;
             evId = eId;
         }
    }

    public static void processAfterInsertEvent( Map<Id, Event> newEvMap){
        
        Set<Id> ownerIds = new Set<Id>();
        Set<Date> dateNew = new Set<Date>();
        Map<Id,Set<EventDateTimeWrapper>> OwnerTimeMap = new Map<Id,Set<EventDateTimeWrapper>>();       
        
        for( Event evNew: newEvMap.values() ){
            if( !String.isBlank(evNew.Appointment_Type__c) ){
                system.debug(' ### New Event available');
                ownerIds.add( evNew.OwnerId );
                dateNew.add(evNew.StartDateTime.dateGMT());
                Set<EventDateTimeWrapper> evDateTimeMapper = new Set<EventDateTimeWrapper>();
                if( OwnerTimeMap != null && OwnerTimeMap.containsKey(evNew.OwnerId) ){
                    evDateTimeMapper = OwnerTimeMap.get(evNew.OwnerId);
                    system.debug('### evDateTimeMapper populated with same rep'+evNew.OwnerId);                    
                }
                evDateTimeMapper.add( new EventDateTimeWrapper(evNew.StartDateTime,evNew.endDateTime, evNew.Id) );
                OwnerTimeMap.put(evNew.OwnerId, evDateTimeMapper);
            }
        }
        
        system.debug('### OwnerTimeMap: '+OwnerTimeMap);
        
        for( Event evExist: [SELECT StartDateTime, EndDateTime, OwnerId FROM Event WHERE OwnerId In :ownerIds 
                                   AND DAY_ONLY(StartDateTime) IN :dateNew
                                   AND Status__c <> :EventManager.STATUS_CANCELED
                                   AND ShowAs <> :EventManager.SHOW_AS_FREE
                                   AND ID NOT IN :newEvMap.keySet()]){
            if(OwnerTimeMap != null && OwnerTimeMap.containsKey(evExist.OwnerId)){
                system.debug('### Old events for OwnerId found');
                Boolean isError = false;
                List<EventDateTimeWrapper> evDateTimeMapper = new List<EventDateTimeWrapper>();
                evDateTimeMapper.addAll( OwnerTimeMap.get(evExist.OwnerId) );
                Integer counter = evDateTimeMapper.size();
                while(!isError && counter >= 1){
                    counter --;
                    EventDateTimeWrapper evWrapper = evDateTimeMapper[counter];
                    system.debug('### evWrapper: '+ evWrapper);
                    if((checkForMaximumStartDate(evExist.StartDateTime,evWrapper.startDateTime) < checkForMinimumEndDate(evExist.EndDateTime,evWrapper.endDateTime) ) || (evExist.StartDateTime == evWrapper.startDateTime && evExist.EndDateTime == evWrapper.endDateTime) ) {
                        System.debug('### Event clash');
                        isError = true;
                        Event eObj = newEvMap.get(evWrapper.evId);
                        eObj.addError('The DLL tech attempting to be scheduled is no longer available.  Please perform the search again to see if that time slot is still available with another DLL Tech or book another time slot.');
                    }
                }
            }
        }
        
    }
    
    private static DateTime checkForMaximumStartDate(DateTime startDt, DateTime stdt){
        if(startDt > stdt){
            return startDt;
        }
        else 
        return stdt;
    }
    
    private static DateTime checkForMinimumEndDate(DateTime startDt, DateTime stdt){
         if(startDt < stdt){
            return startDt;
        }
        else 
        return stdt;
    }
    
    private static void setWhatIdForDataConversion(List<Event> events)
    {
        List<String> telemarAccountNumbers = new List<String>();
        for(Event e : events)
        {
            if(e.IncomingConversionRecord__c == true)
            {
                if(e.TelemarAccountNumber__c != null && e.TelemarAccountNumber__c != '')
                {
                    telemarAccountNumbers.add(e.TelemarAccountNumber__c);
                }
                else
                {
                    e.adderror('Telemar Account Number is missing. Insert failed');
                }
            }
        }
        List<Account> allMatchingAccounts = [Select id , TelemarAccountNumber__c from Account where TelemarAccountNumber__c in : TelemarAccountNumbers];
        Map<String, Account> TAAccountsMap = new Map<String, Account>();
        for (Account a : allMatchingAccounts)
        {
            TAAccountsMap.put(a.TelemarAccountNumber__c, a);
        }
        for(Event e : events)
        {
            if(e.IncomingConversionRecord__c == true && e.TelemarAccountNumber__c != null && e.TelemarAccountNumber__c != '')
            {
                if(TAAccountsMap.get(e.TelemarAccountNumber__c) == null)
                {
                    e.adderror('No matching account found for the event.');
                }
                else
                {
                    e.WhatId = TAAccountsMap.get(e.TelemarAccountNumber__c).id;
                }
            }
        }
    }
    
    /**
        updateEventAssignedTo   Keep event owner id and related account owner id the same
        
        @param  events  Newly inserted or updated events
    */
    private static void updateEventAssignedTo(List<Event> events, Map<Id,Event> oldMap)
    {
        Id globaluser = Utilities.getGlobalUnassignedUser();
        Set<Id> aids = new Set<Id>();   //event whatids
        for (Event e : events)
        {
            if (e.WhatId != null && e.OwnerId != globaluser) //only need to check accounts where the event is not owned by global user
                aids.add(e.WhatId);
        }
        //accounts related to events
        List<Account> accounts = new List<Account>([
            select Id, OwnerId from Account where Id IN :aids
        ]);
        
        for (Event e : events)
        {
            Event old = null;
            if (oldMap != null && oldMap.containsKey(e.Id)) {
                old = oldMap.get(e.Id);
            }
            for (Account a : accounts)
            {
                if (e.Status__c == EventManager.STATUS_CANCELED) {
                    e.OwnerId = globaluser;
                }
                else if (a.Id == e.WhatId && a.OwnerId != e.OwnerId && (old == null || (old.OwnerId == e.OwnerId))) //event owner id not set properly
                {
                    e.OwnerId = a.OwnerId;
                    break;
                }
            }
        }
    }
    
    /**
        setEventRecordType  Sets the record type of an event based on Task code for any Events created in Telemar that are sent to SFDC
        
        @param  events  Newly inserted or updated events
    */
    private static void setEventRecordType(List<Event> events)
    {
        Map<Id, String> rectypes = Utilities.getRecordTypesForObject('Event');
        //I need to reverse this map
        Map<String, Id> ert = new Map<String, Id>();
        for (String key : rectypes.keyset())
        {
            if (!ert.containsKey(rectypes.get(key))){
                ert.put(rectypes.get(key), key);
            }
        }
        for(Event e : events)
        {       
           if(!String.isBlank(e.Appointment_Type__c) && e.Appointment_Type__c  == 'In House Activation') {
                e.DllUnique__c = e.OwnerId + String.ValueOf(e.StartDateTime)+String.valueOf(e.endDateTime);
            }
            // Dropping task code logic in favor of incoming type logic
            if (e.IncomingType__c == EventManager.INCOMING_TYPE_SG) {
                e.RecordTypeId = ert.get( RecordTypeName.SELF_GENERATED_APPOINTMENT );
            }
            else if (e.IncomingType__c == EventManager.INCOMING_TYPE_INSTALL) {
                e.RecordTypeId = ert.get( RecordTypeName.INSTALL_APPOINTMENT );
            }
            else if (e.IncomingType__c == EventManager.INCOMING_TYPE_CG) {
                e.RecordTypeId = ert.get(RecordTypeName.COMPANY_GENERATED_APPOINTMENT);
            }
            //e.DllUnique__c = e.OwnerId + String.ValueOf(e.StartDateTime)+String.valueOf(e.endDateTime);
            System.debug('### unique'+e.DllUnique__c);
            e.IncomingType__c = null;
        }
    }
    
    private static void setEventReminder(List<Event> events) {
        Map<Id, String> rectypes = Utilities.getRecordTypesForObject('Event');
        //I need to reverse this map
        Map<String, Id> ert = new Map<String, Id>();
        for (String key : rectypes.keyset())
        {
            if (!ert.containsKey(rectypes.get(key))){
                ert.put(rectypes.get(key), key);
            }
        }
        
        for (Event e : events) {
            
            if (e.RecordTypeId == ert.get( RecordTypeName.INSTALL_APPOINTMENT) ) {
                e.ShowAs = EventManager.SHOW_AS_FREE;
            }
            
            DateTime current = system.now();
            
            if (e.RecordTypeId == ert.get( RecordTypeName.INSTALL_APPOINTMENT) || e.Status__c == EventManager.STATUS_CANCELED || e.StartDateTime < current) {
                e.IsReminderSet = false;
            }
            else if (e.RecordTypeId == ert.get( RecordTypeName.COMPANY_GENERATED_APPOINTMENT) || (e.RecordTypeId == ert.get( RecordTypeName.SELF_GENERATED_APPOINTMENT ) && ProfileHelper.isIntegrationUser())) {
                e.IsReminderSet = true;
                e.ReminderDateTime = e.StartDateTime.addMinutes(-15);
            }
            
        }
    }
    
    private static void updateEventReminder(List<Event> events) {
        Map<Id, String> rectypes = Utilities.getRecordTypesForObject('Event');
        //I need to reverse this map
        Map<String, Id> ert = new Map<String, Id>();
        for (String key : rectypes.keyset())
        {
            if (!ert.containsKey(rectypes.get(key))){
                ert.put(rectypes.get(key), key);
            }
        }
        
        DateTime current = system.now();
        for (Event e : events) {
            if(!String.isBlank(e.Appointment_Type__c) && e.Appointment_Type__c  == 'In House Activation' && e.Status__c == 'Canceled'){
                e.DllUnique__c = '';
            }
            if (e.Status__c == EventManager.STATUS_CANCELED || e.StartDateTime < current) {
                e.isReminderSet = false;
            }
            else if (e.RecordTypeId == ert.get( RecordTypeName.COMPANY_GENERATED_APPOINTMENT )  || (e.RecordTypeId == ert.get( RecordTypeName.SELF_GENERATED_APPOINTMENT) && ProfileHelper.isIntegrationUser())) {
                e.IsReminderSet = true;
                e.ReminderDateTime = e.StartDateTime.addMinutes(-15);
            }           
        }
    }
    
    private static void validatePostalCode(List<Event> events) {
        for (Event e : events) {
            if (e.PostalCode__c != null && !EventUtilities.validatePostalCode(e.PostalCode__c)) {
                e.addError('Events must have a valid postal code.');
            }
        }
    }
    
    private static void updateDevicePhone(List<Event> events) {
        // get task codes and record type names
        map<String, ResaleGlobalVariables__c> settings = ResaleGlobalVariables__c.getAll();
        String rectypestr = settings.get('EventRecTypeNameAllowed').value__c;
        String taskcodestr = settings.get('DisqualifyEventTaskCode').value__c;
        String[] rectypes = rectypestr.split(',');
        String[] taskcodes = taskcodestr.split(',');
        map<Id, String> recmap = Utilities.getRecordTypesForObject('Event');

        // use owner id set to get users that own events
        set<Id> oids = new set<id>();
        for (Event e : events) {
            if (
                    recmap.containsKey(e.RecordTypeId) && Utilities.contains(rectypes, recmap.get(e.RecordTypeId)) &&
                    e.TaskCode__c != null && !Utilities.contains(taskcodes, e.TaskCode__c)
                ) {
                oids.add(e.OwnerId);
            }
        }

        // get users and store in map for later
        map<Id, User> ownerUserMap = new map<Id, User>();
        list<User> owners = new list<User>([
            Select Id, DevicePhoneNumber__c
            from User
            where Id IN :oids
        ]);
        for (User u : owners) {
            ownerUserMap.put(u.Id, u);
        }
        
        // loop through events and check device phone
        for (Event e : events) {
            User owner = ownerUserMap.get(e.OwnerId);
            if (owner != null && owner.DevicePhoneNumber__c != e.DevicePhoneNumber__c) {
                e.DevicePhoneNumber__c = owner.DevicePhoneNumber__c;
            }
        }
    }
    
    // HRM# 5893 - Field Reschedule Appointment - By TCS
    //Logic to record Appointment previous timings when appointment is rescheduled on update.
    private static void recprevSchTimeOnUpdate(list<Event> newEventList, Map<id,Event> oldEventMap){
        set<String> profileException = new set<String>();
        //Add any profile name to this set to except the logic
        profileException.add('ADT NSC Sales Representative');
        String currentUserProf = [SELECT Name FROM Profile WHERE Id=:Userinfo.getprofileId()].Name;
        if(!profileException.contains(currentUserProf)){
            for(Event newEvent:newEventList){
                if(newEvent.WhatId != null && String.valueOf(newEvent.WhatId.getSObjectType()) == 'Account'){
                   if(newEvent.StartDateTime != oldEventMap.get(newEvent.Id).StartDateTime || newEvent.EndDateTime != oldEventMap.get(newEvent.Id).EndDateTime){
                       newEvent.PriorAppointmentStartDateTime__c =  oldEventMap.get(newEvent.Id).StartDateTime;
                       newEvent.Rescheduler__c = userinfo.getName();
                       newEvent.TimesAppointmentRescheduled__c = oldEventMap.get(newEvent.Id).TimesAppointmentRescheduled__c!=null?oldEventMap.get(newEvent.Id).TimesAppointmentRescheduled__c+1:1;
                   }
                }
            }    
        }       
    }
    
    //HRM-7272 DNIS Updates for Field - Mounika Anna
    private static void updateAccountDNISFields(list<event> events){
        User loggedInuser = [select id,Name,Rep_Team__c from User where id =: userinfo.getUserId()];
        set<id> accids = new set<id>();
        for(event e: events){
            if(e.whatid!=null)
                accids.add(e.whatid);
        }  
        List<Account> acc = [select id,Name,DNIS__c,LastActivityDate__c,LastmodifiedDate,DNIS_Modified_Date__c,DNIS_Modifier_Name__c from Account where id =:accids ];
        Date todaysDate = system.today();
        list<Account> accsToUpdate = new list<Account>();
        for(Account a : acc){
            Integer dnisDiffDates;
            if( a.DNIS_Modified_Date__c!=null){
                dnisDiffDates = a.DNIS_Modified_Date__c.Date().daysBetween(Date.today()); 
            }
        
            if(loggedInUser.rep_team__c.containsIgnoreCase('FIELDSALES') &&
             ((a.LastActivityDate__c.daysBetween(todaysDate) > Integer.valueOf(ResaleGlobalVariables__c.getinstance('DaysToPopulateDNISOnAccount').value__c) && a.DNIS_Modified_Date__c == null) ||
             (a.LastActivityDate__c.daysBetween(todaysDate) > Integer.valueOf(ResaleGlobalVariables__c.getinstance('DaysToPopulateDNISOnAccount').value__c) && dnisDiffDates > Integer.valueOf(ResaleGlobalVariables__c.getinstance('DaysToPopulateDNISOnAccount').value__c))))
            {
                a.DNIS__c = 'SF00000001';
                a.DNIS_Modifier_Name__c = loggedInUser.id;
                a.DNIS_Modified_Date__c = a.LastmodifiedDate;
                accsToUpdate.add(a);
            }
        }
        if(accsToUpdate.size()>0)
            Update accsToUpdate;
    }
}