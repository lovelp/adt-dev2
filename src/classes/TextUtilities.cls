/************************************* MODIFICATION LOG ********************************************************************************************
* TextUtilities
*
* DESCRIPTION : Provides the capability to convert a string to a character array with diacritical marks removed  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					7/11/2012				- Original Version
*
*													
*/
public with sharing class TextUtilities {
	
	private static Map<String,Integer> hexMap = new Map<String,Integer>();
	
	static {
		// Build map to convert hex to decimal
		for (integer nLoop = 0; nLoop < 16; nLoop++) {
			hexMap.put('0123456789abcdef'.subString(nLoop, nLoop+1), nLoop);
		}            
	}
	
	public static String removeDiacriticalMarks(String strInput) {
	 	
	 	String output = strInput;
	 	if (strInput != null) {
	 		List<Integer> charArray = TextUtilities.stringToCharArray(strInput, true);
	 	
	 		if (charArray.size() > 0 || charArray.size() < strInput.length()) {
	 			output = String.fromCharArray(charArray);
	 		}
	 	}
	 	return output;
	}
	 
	 
	public static List<Integer> stringToCharArray(String strInput) {
	 	
	 	return stringToCharArray(strInput, false);
	 	
	}

	private static List<Integer> stringToCharArray(String strInput, boolean removeDiacriticalMarks) {

 		List<Integer> charList = new List<Integer>();        

        if (strInput == null || strInput == '') {
        	return charList;
        }	

     	String strHex = EncodingUtil.convertToHex(Blob.valueOf(strInput));

        if (strHex == null || strHex == '') {
        	return charList;
        }	

        Integer increment = 2;

        for(Integer i = 0; i < strHex.length(); i += increment) {

     		Integer out = 0;
            Integer c1 = (hexMap.get(strHex.subString(i,i + 1)) * 16) + (hexMap.get(strHex.subString(i + 1,i + 2)));
            Integer c2 = 0;                  
            Integer c3 = 0;
            Integer c4 = 0;

            if (c1 <128) {
            	out = c1;
                increment = 2;
            }
			else {
				if(c1 > 193 && c1 < 224) {
                	// first of 2
                    increment = 4;                          
                }
                if(c1 > 223 && c1 < 240) {
					// first of 3
					increment = 6;
				}
                if(c1 > 239 && c1 < 245) {
					// first of 4
					increment = 8;
                }
                c2 = (hexMap.get(strHex.subString(i + 2,i + 3)) * 16) + (hexMap.get(strHex.subString(i + 3,i + 4)));                                     
               	if(increment == 4) {
                 	out = (c1 - 192) * 64 + c2 - 128;
                }
                else if(increment == 6) {
					c3 = (hexMap.get(strHex.subString(i + 4,i + 5)) * 16) + (hexMap.get(strHex.subString(i + 5,i + 6)));
					out = (c1-224)*4096 + (c2-128)*64 + c3 - 128;
                }
                else if(increment == 8) {
                  	c4 = (hexMap.get(strHex.subString(i + 6,i + 7)) * 16) + (hexMap.get(strHex.subString(i + 7,i + 8)));
                  	out = (c1 - 240) * 262144 + (c2 - 128) * 4096 + (c3 - 128) * 64 + c4 - 128;
             	}

    		}
    		
           	if ((out != 0) && isInValidCharacterRange(out)) {
           		if (removeDiacriticalMarks) {
           			charList.add(deriveBaseCharacter(out)); 
           		}	
           		else {	 
     				charList.add(out);
           		}	
       		}            

       }
       return charList;
   
	}
	
	// Constants for base characters
	// Derived by converting hex values found here: http://msdn.microsoft.com/en-us/goglobal/cc305145.​aspx
	private static final Integer LATIN_UPPERCASE_LETTER_A = 65;
	private static final Integer LATIN_UPPERCASE_LETTER_C = 67;
	private static final Integer LATIN_UPPERCASE_LETTER_E = 69;
	private static final Integer LATIN_UPPERCASE_LETTER_I = 73;
	private static final Integer LATIN_UPPERCASE_LETTER_N = 78;
	private static final Integer LATIN_UPPERCASE_LETTER_O = 79;
	private static final Integer LATIN_UPPERCASE_LETTER_U = 85;
	private static final Integer LATIN_LOWERCASE_LETTER_A = 97;
	private static final Integer LATIN_LOWERCASE_LETTER_C = 99;
	private static final Integer LATIN_LOWERCASE_LETTER_E = 101;
	private static final Integer LATIN_LOWERCASE_LETTER_I = 105;
	private static final Integer LATIN_LOWERCASE_LETTER_N = 110;
	private static final Integer LATIN_LOWERCASE_LETTER_O = 111;
	private static final Integer LATIN_LOWERCASE_LETTER_U = 117;
   
  	private static Integer deriveBaseCharacter(Integer baseChar) {
  		
  		Integer output = baseChar;
  		
  		if (baseChar >= 192 && baseChar <= 197) {
  			output = LATIN_UPPERCASE_LETTER_A;
  		}
  		else if (baseChar == 199) {
  			output = LATIN_UPPERCASE_LETTER_C;
  		}
  		else if (baseChar >= 200 && baseChar <= 203) {
  			output = LATIN_UPPERCASE_LETTER_E;
  		}
  		else if (baseChar >= 204 && baseChar <= 207) {
  			output = LATIN_UPPERCASE_LETTER_I;
  		}
  		else if (baseChar == 209) {
  			output = LATIN_UPPERCASE_LETTER_N;
  		}
  		else if (baseChar >= 210 && baseChar <= 214) {
  			output = LATIN_UPPERCASE_LETTER_O;
  		}
  		else if (baseChar >= 217 && baseChar <= 220) {
  			output = LATIN_UPPERCASE_LETTER_U;
  		}		
  		else if (baseChar >= 224 && baseChar <= 229) {
  			output = LATIN_LOWERCASE_LETTER_A;
  		}		
  		else if (baseChar == 231) {
  			output = LATIN_LOWERCASE_LETTER_C;
  		}	
  		else if (baseChar >= 232 && baseChar <= 235) {
  			output = LATIN_LOWERCASE_LETTER_E;
  		}	
  		else if (baseChar >= 236 && baseChar <= 239) {
  			output = LATIN_LOWERCASE_LETTER_I;
  		}	
  		else if (baseChar == 241) {
  			output = LATIN_LOWERCASE_LETTER_N;
  		}
  		else if (baseChar >= 242 && baseChar <= 246) {
  			output = LATIN_LOWERCASE_LETTER_O;
  		}	
  		else if (baseChar >= 249 && baseChar <= 252) {
  			output = LATIN_LOWERCASE_LETTER_U;
  		}

  		return output;
  		
  	}    
         

    // Returns true if a given character is in the range for valid characters.
    private static boolean isInValidCharacterRange(integer character) {
        
        return (character >= 32 && character <= 255);

    }

      
}