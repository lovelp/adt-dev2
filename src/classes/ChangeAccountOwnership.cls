/************************************* MODIFICATION LOG ********************************************************************************************
* ChangeAccountOwnership
*
* DESCRIPTION : Responsible for changing account or lead ownership based on the custom territory management component
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*                               10/14/2011            - Original Version
*  Jaydip Bhattacharya          05/09/2013            - Modified as part of Sprint#10 - builder leads should be assigned to builder channel if available. Otherwise based on postal code.                                                 
*  Mike Tuckman (001)           11/01/2013            - Support for DEVCON
*/

public with sharing class ChangeAccountOwnership {
    
    public String ChangeAccountOwnershipWithZipAndUser(Map<Id, Id> ZipCodeAssignments, Map<Id, Id> managers){
        //[Modified because of salesforce limitation]
        //Instead of creating a batch rightaway, insert the records to be processed in an onject
        //and have a scheduled job pick up the records and go through the assignment cycle.
        String Error = '';
        Set<Id> zipIds = ZipCodeAssignments.keySet();
        System.debug('Zipcodes for assignment'+zipIds);
        PostalCodeReassignmentQueue__c PCRQ;
        List<PostalCodeReassignmentQueue__c> PCRQs = new List<PostalCodeReassignmentQueue__c>();
        for(id zipid : zipIds){
            PCRQ = new PostalCodeReassignmentQueue__c();
            PCRQ.PostalCodeId__c = zipid;
            PCRQ.PostalCodeNewOnwer__c = ZipCodeAssignments.get(zipid);
            PCRQs.add(PCRQ);
        }
        insert PCRQs;
        return Error;
    }
    
    public static String ChangeAccountOwnershipWithListOfLeads(List<Lead> Leads){
        List<Id> PostalCodeIds = new List<Id>();
        Map<String, String> PCOwners = new Map<String, String>();
        Set<String> allTowns = new Set<String>();
        Map<Id, String> PCTownsMap = new Map<Id, String>();
        Map<String, String> MTOwners = new Map<String, String>();
        List<Id> ownerIdList = new List<Id> ();
        Map <Id,String> ownerID_USAAMap = new Map <Id,String> ();
        String Error = '';
        String GlobalUnassingnedUser = Utilities.getGlobalUnassignedUser();
        //get all postal codes from accounts + postal code and channels
        for(Lead led : Leads){
            if(led.postalcodeId__c != null){
                PostalCodeIds.add(led.postalcodeId__c);
            }
        }
        //get all towns related to the postal codes
        List<Postal_Codes__c> allPostalCodes = [Select Id, TownId__c, BusinessId__c, TownUniqueId__c from Postal_Codes__c where Id in : PostalCodeIds];
        for(Postal_Codes__c pcs : allPostalCodes){
            allTowns.add(pcs.TownId__c);
            PCTownsMap.put(pcs.id, pcs.TownId__c);
        }
        //check of the postal codes are assigned
        List<TerritoryAssignment__c> allTAs = [Select Id, PostalCodeId__c, OwnerId, TerritoryType__c from TerritoryAssignment__c where PostalCodeId__c in : PostalCodeIds];
        for(TerritoryAssignment__c TA : allTAs){
            PCOwners.put(TA.PostalCodeId__c + ';' + TA.TerritoryType__c, TA.OwnerId);
            ownerIdList.add(TA.OwnerId);            
        }
        
        List <user> ownerIDs_USAA = [Select Id, qualification__c from user where Id in : ownerIdList];
        
        for (user owners : ownerIDs_USAA ){
            ownerID_USAAMap.put(owners.Id,owners.qualification__c);
        }
        
        List<ManagerTown__c> MTs = [Select id, ManagerId__c, TownId__c, BusinessId__c, Type__c from ManagerTown__c where TownId__c in : allTowns];
        for(ManagerTown__c MT : MTs){
            for(String mtts : MT.Type__c.split(';')){
                MTOwners.put(MT.TownId__c + ';' + MT.BusinessId__c + ';' + mtts, MT.ManagerId__c);
            }
        }
        for(Lead led : Leads){
            String BussId = led.Business_Id__c.contains('1100')? '1100' : '1200';
            string tempChannel = null;
            
            string TnId;
            string mgrId;

            if (led.Affiliation__c == 'Builder'){
                tempChannel = Channels.TERRITORYTYPE_BUILDER;
                TnId=PCTownsMap.get(led.postalCodeID__c);
                if (TnId!=null){
                    if (MTOwners.containsKey(TnID+';1100;Resi Direct Sales')){
                        mgrId=MTOwners.get(TnID+';1100;Resi Direct Sales');
                    }
                }
            }
            
            if(tempChannel != null && PCOwners.containsKey(led.PostalCodeId__c + ';' + tempChannel))
            {
                led.OwnerId =PCOwners.get(led.PostalCodeId__c + ';' + tempChannel);
                led.DateAssigned__c = DateTime.now();
                led.UnassignedLead__c = false;
                led.NewLead__c = true;
                System.debug('led.OwnerId line 308'+led.OwnerId);
            }else if(tempChannel != null && mgrId!=null ){
                led.OwnerId=mgrId;
                led.DateAssigned__c = DateTime.now();
                led.UnassignedLead__c = false;
                led.NewLead__c = true;
                System.debug('led.OwnerId line 315'+led.OwnerId);
            }else if(PCOwners.get(led.PostalCodeId__c + ';' + led.Channel__c) != null){
                String ownerId = PCOwners.get(led.PostalCodeId__c + ';' + led.Channel__c);
                led.DateAssigned__c = DateTime.now();
                led.UnassignedLead__c = false;
                led.NewLead__c = true;
                if ((led.Affiliation__c == IntegrationConstants.AFFILIATION_USAA && ownerID_USAAMap.get(ownerId)== IntegrationConstants.AFFILIATION_USAA) || (led.Affiliation__c != IntegrationConstants.AFFILIATION_USAA) ) {
                    led.OwnerId = PCOwners.get(led.PostalCodeId__c + ';' + led.Channel__c);
                    System.debug('led.Affiliation__c ,led.OwnerId line 338'+led.OwnerId);
                }else if (MTOwners.get(PCTownsMap.get(led.PostalCodeId__c) + ';' + BussId + ';' + led.Channel__c) != null){
                    led.OwnerId = MTOwners.get(PCTownsMap.get(led.PostalCodeId__c) + ';' + BussId + ';' + led.Channel__c);
                    System.debug('led.Affiliation__c ,led.OwnerId line 343'+led.OwnerId);
                }else{
                    led.OwnerId = GlobalUnassingnedUser;
                    System.debug('GlobalUnassingnedUser,led.OwnerId line 348'+led.OwnerId);
                }
            }else if (MTOwners.get(PCTownsMap.get(led.PostalCodeId__c) + ';' + BussId + ';' + led.Channel__c) != null){
                led.OwnerId = MTOwners.get(PCTownsMap.get(led.PostalCodeId__c) + ';' + BussId + ';' + led.Channel__c);
                led.DateAssigned__c = DateTime.now();
                led.UnassignedLead__c = true;       
                led.NewLead__c = true; 
                System.debug('led.OwnerId line 367'+led.OwnerId);
            }else{
                led.OwnerId = GlobalUnassingnedUser;
                led.DateAssigned__c = DateTime.now();
                led.UnassignedLead__c = true;
                led.NewLead__c = true;
                System.debug('GlobalUnassingnedUser,led.OwnerId line 375'+led.OwnerId);
            }
        }
        return Error;
    }   
}