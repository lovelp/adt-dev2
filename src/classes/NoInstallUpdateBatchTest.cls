/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class NoInstallUpdateBatchTest {

    static testMethod void testNoInstallUpdateBatch() {
        
        setup();

        //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
        insert addr;
        
        Account a1 = TestHelperClass.createAccountData(addr);
        a1.AccountStatus__c = 'SLS';
        a1.SoldDate__c = system.today().addDays(-50);
        a1.ScheduledInstallDate__c = system.today().addDays(-1);
        update a1;
        
        Account a2 = TestHelperClass.createAccountData(addr);
        a2.AccountStatus__c = 'SLS';
        a2.SoldDate__c = system.today().addDays(-50);
        a2.ScheduledInstallDate__c = system.today().addDays(+5);
        update a2;

        Account a3 = TestHelperClass.createAccountData(addr);
        a3.AccountStatus__c = 'SLS';
        a3.SoldDate__c = system.today().addDays(-20);
        a3.ScheduledInstallDate__c = system.today().addDays(-5);
        update a3;

        Test.startTest();
            NoInstallUpdateBatch niu = new NoInstallUpdateBatch();
            niu.query += ' and ID IN (\'' + a1.Id + '\',\'' + a2.Id + '\') limit 200';
            Database.executeBatch(niu);
        Test.stopTest();
        
        List<Account>confirm = [select Id from Account where AccountStatus__c = 'NIO'];
        system.assertEquals(1,confirm.size());
        
    }
    
    private static void setup() {
        User current = [select Id from User where Id=:UserInfo.getUserId()];
        System.runAs(current) {        
            TestHelperClass.createReferenceDataForTestClasses();
            TestHelperClass.createReferenceUserDataForTestClasses();
        }
    }
    
    static testmethod void testNoInstallUpdateScheduler() {
        ResaleGlobalVariables__c globalVar = new ResaleGlobalVariables__c();
        globalVar.name = 'NoInstallUpdateDays';
        globalVar.value__c = '2';
        insert globalVar;

        // CRON expression: midnight on March 15.
        // Because this is a test, job executes
        // immediately after Test.stopTest().
        Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        // Schedule the test job
        String jobId = System.schedule('NewMoverScheduler',CRON_EXP, new NoInstallUpdateScheduler());
        Test.stopTest();
    }
}