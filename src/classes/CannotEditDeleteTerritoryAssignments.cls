/************************************* MODIFICATION LOG ********************************************************************************************
* CannotEditDeleteTerritoryAssignments
*
* DESCRIPTION : Disables the edit and delete functions for Territory Assignments.
*               It is a Controller Extension in the SFDC MVC architecture.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2011			- Original Version
*
*													
*/

public with sharing class CannotEditDeleteTerritoryAssignments {

	public CannotEditDeleteTerritoryAssignments(ApexPages.StandardController controller)
	{
		
	}
	
	public pageReference redirectToErrorPage()
	{
		return new pageReference('/apex/ShowError?Error=NEDTA');
	}

}