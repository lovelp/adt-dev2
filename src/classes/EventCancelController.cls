/************************************* MODIFICATION LOG ********************************************************************************************
* EventCancelController
*
* DESCRIPTION : Controller for event cancel page
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  2/10/2012               - Original Version
*
*                                                   
*/

public with sharing class EventCancelController {
    
    private Boolean success;
    private Event e;
    
    public String returl {get;set;}
    
    public EventCancelController( ApexPages.Standardcontroller sc )
    {
        e = (Event)sc.getRecord();
        //need to get all required event fields
        if (e.Id != null) {  // this is just a sanity check, should always be set
            e = [
                    Select e.WhoId, e.WhatId, e.Description,
                        e.TaskCode__c, e.Subject, e.Status__c, e.StartDateTime, 
                        e.ShowAs, e.ScheduleID__c, e.ReminderDateTime, e.RecordTypeId, 
                        e.OwnerId, e.IsReminderSet, e.InstallTechnician__c, e.EndDateTime,
                        e.PostalCode__c, e.Quote_Id__c, e.Appointment_Type__c
                    From Event e 
                    where e.Id = :e.Id
                ];
        }
    }
    
    public PageReference cancelEvent()
    {
        success = EventManager.cancelEvent(e);
        returl = ApexPages.currentPage().getParameters().get('returl');
        if (!success)
        {
            ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to cancel event.');
            ApexPages.addMessage(m);
            return null;
        }
            // Don't want to return to event on cancel.
        if(Userinfo.getUiThemeDisplayed() == 'Theme4d'){
            // Redirect to leads page
            PageReference redirectPage = new PageReference('/'+e.WhatId);
            return redirectPage;
        }
        else{
            return new PageReference('/home/home.jsp');
        }
    }
}