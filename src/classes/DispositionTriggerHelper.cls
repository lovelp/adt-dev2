/************************************* MODIFICATION LOG ********************************************************************************************
* DispositionTriggerHelper
*
* DESCRIPTION : Defines helper methods for use by DispositionTrigger
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*                            10/14/2011         - Original Version
*
*                                                   
*/

public with sharing class DispositionTriggerHelper {
    
    String dataConversion = 'FALSE';
    Id IntegrationUser;
    
    public DispositionTriggerHelper()
    {
    
    }   
    
    public void processBeforeInsert(List<disposition__c> insertedDispositions)
    {
        List<disposition__c> tempDispoClone = new List<disposition__c>();
        List<disposition__c> tempDispo = new List<disposition__c>();
        
        Map<String, List<disposition__c>> dispositionedByIdsMap = new Map<String, List<disposition__c>>();
        List<User> allRelatedUsers = new List<User>();
        Map<String, User> allRelatedUsersMap = new Map<String, User>(); 
        
        Map<Id, disposition__c> allDispositionedAccountIds = new Map<Id, disposition__c>();
        
        Boolean isIntUser = ProfileHelper.isIntegrationUser();
        System.debug('isIntUser:' + isIntUser);
        for(disposition__c insertedDispo : insertedDispositions)
        {
            //first check if this disposition record belings to an account
            if(insertedDispo.AccountID__c != null)
            {
                if(!isIntUser)
                {
                    //send the disposition to telemar based on the disposition code in custom settings
                    allDispositionedAccountIds.put(insertedDispo.AccountID__c, insertedDispo);
                }
                else
                {
                    //if added by integration user
                    if(insertedDispo.DispositionEmployeeNumber__c != null && insertedDispo.DispositionEmployeeNumber__c != ''){
                        //dispositionedByIdsMap.put(insertedDispo.DispositionEmployeeNumber__c, insertedDispo);
                        List<disposition__c> dList = dispositionedByIdsMap.get(insertedDispo.DispositionEmployeeNumber__c);
                        if (dList == null) {
                            dList = new List<disposition__c>();
                            dispositionedByIdsMap.put(insertedDispo.DispositionEmployeeNumber__c, dList);
                        } 
                        dList.add(insertedDispo);
                        
                    } else {
                        //accept the record. No actions needed as of now. Just dont populate the id for dispositioned by field.
                    }                   
                }
            }
        }
        
        if(dispositionedByIdsMap.size() > 0)
        {
            allRelatedUsers = [Select id, EmployeeNumber__c from User where EmployeeNumber__c in : dispositionedByIdsMap.keySet()];
            for(User usr : allRelatedUsers)
            {
                allRelatedUsersMap.put(usr.EmployeeNumber__c, usr);
            }
        }
        for(String userEmployeeId : dispositionedByIdsMap.keySet())
        {
            if(allRelatedUsersMap.get(userEmployeeId) != null){
                List<disposition__c> dList = dispositionedByIdsMap.get(userEmployeeId);
                for (disposition__c disp : dList) {
                    disp.DispositionedBy__c = allRelatedUsersMap.get(userEmployeeId).id;
                }   
            } else {
                //dont do anything for now. accept the record
            }
        }
        
        //create Telemar messages for dispositions
        if(allDispositionedAccountIds.size() > 0)
        {
            Map<String, DispositionCodes__c> allDispoMappings = DispositionCodes__c.getAll();
            Map<String, String> mappingsToSendToTelemar = new Map<String, String>();
            for(DispositionCodes__c dc : allDispoMappings.values())
            {
                mappingsToSendToTelemar.put(dc.SalesforceDisposition__c, dc.TelemarDispositionCode__c);
            }
            Map<Id, Account> allAccounts = new Map<id, Account>([select Id, OwnerId, TelemarAccountNumber__c, Name, FirstName__c, LastName__c, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, Phone, Email__c, DispositionComments__c, QueriedSource__c, ReferredBy__c from Account where id in : allDispositionedAccountIds.keyset()]);
            Map<Account, disposition__c> acctToDispMap = new Map<Account, disposition__c>();
            for(disposition__c dispo : allDispositionedAccountIds.values()) 
            {
                String source = dispo.DispositionDetail__c;
                if (source == null || source == '') {
                    source = dispo.DispositionType__c;
                }
                String target = mappingsToSendToTelemar.get(source);
                if (target == null) {
                    source = dispo.DispositionType__c;
                    target = mappingsToSendToTelemar.get(source);
                }
                if(target != null)
                {
                    //The following steps send the disposition and account information to TelemarGateway
                    //Before sending the data to Telemar, dispositions in Salesforce are mapped to Telemar 
                    //dispositions using DispositionDetail as source or if that value isn't in use, DispositionType 
                    //as the source.  These disposition mappings are stored as custom settings and the Telemar
                    //disposition value needs to be calculated before sending details to Telemar. For this 
                    //purpose, we clone the disposition record and set the disposition code as the Telemar
                    //disposition code on the cloned record and passed on to TelemarGateway.
                    tempDispo = new List<disposition__c>();
                    tempDispoClone = new List<disposition__c>(); 
                    tempDispo.add(dispo);
                    tempDispoClone = tempDispo.deepClone();
                    tempDispoClone[0].DispositionType__c = target;
                    Account tempAcct = allAccounts.get(dispo.AccountId__c);
                    if (tempAcct != null) {
                        acctToDispMap.put(tempAcct, tempDispoClone[0]);
                    }
                    
                }
            }
            if (acctToDispMap != null && acctToDispMap.size() > 0) {
                TelemarGateway.addDisposition(acctToDispMap);
            }   
        }
    }
}