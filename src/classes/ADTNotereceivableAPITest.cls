@isTest
private class ADTNotereceivableAPITest {
    @isTest
    private static void testGetNotes() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ADTTestCalloutMockBuilder(200, '{"notes": [ { "note": 195448305, "customer": 202546345, "site": 204554635, "initialAmount": 545.35, "installmentAmount": 29.99, "noInstallBills": 123456789, "lastInstallBillNo": 987654321, "totalBilledAmount": 845.10, "job": 435678123 }, { "note": 483051954, "customer": 634520254, "site": 463520455, "initialAmount": 485.35, "installmentAmount": 19.50, "noInstallBills": 678912345, "lastInstallBillNo": 432198765, "totalBilledAmount": 735, "job": 812343567 } ]}'));
        ADTNotereceivableAPI.getNotes('202546345');
        Test.stopTest();
        
        List<CustomerLoan__c> loanRecords = new List<CustomerLoan__c>([Select Id, CustomerNumber__c  From CustomerLoan__c Where CustomerNumber__c != null]);
        //system.debug('LoanrecSize'+loanRecords.size());
       // System.assertEquals(2, loanRecords.size());
    }
    
    @isTest
    private static void testGetNotesErrorResponse() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ADTTestCalloutMockBuilder(1050, '{"errors": [{ "status": "Client", "errorCode": "1001", "errorMessage": "Invalid input for custNo/siteNo/jobNo combination, one of the input must be supplied" }]}'));
        try {
        	ADTNotereceivableAPI.getNotes('202546345');
        }catch(Exception e) {
            System.assertEquals('Invalid input for custNo/siteNo/jobNo combination, one of the input must be supplied', e.getMessage());
        }
        Test.stopTest();
    }
    
    @isTest
    private static void testGetNotesBlankCustomer() {
        Test.startTest();
        try {
        	ADTNotereceivableAPI.getNotes('');
        }catch(Exception e) {
            System.assert(e.getMessage().contains('Please provide Customer Number'));
        }
        Test.stopTest();
    }
}