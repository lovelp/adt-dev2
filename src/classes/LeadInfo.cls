public with sharing class LeadInfo {
    public static String leadid;
    public static Lead ld;
    
    public static Lead getLeadDetails(){
        Lead ld = new Lead();
        if(String.isNotBlank(leadid)){
        	// Sid - LeadManagementId changes
            ld = [SELECT id,Ownerid,FirstName,LastName,Email,phone,Channel__c,Business_Id__c,leadSource,Type__c,leadOrigin__c,
                 AddressId__c,AddressID__r.State__c,PostalCodeID__c, SiteStreet__c,SiteStreet2__c,TownNumber__c,PostalCodeID__r.BusinessID__c, 
                 SiteCity__c,SiteStateProvince__c,SitePostalCode__c,SiteCounty__c,SitePostalCodeAddOn__c,AddressID__r.PostalCode__c,
                 TelemarLeadSource__c,GenericMedia__c, QueriedSource__c,TelemarAccountNumber__c,LeadManagementId__c
                 FROM Lead WHERE id=:leadid];
        }
        return ld;
    }
}