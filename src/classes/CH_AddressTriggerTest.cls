@isTest
private class CH_AddressTriggerTest {
	// Test method for the CH_AddressTrigger
    static testMethod void insertCHAddress() {
		Address__c addr = new Address__c();
		addr.Street__c = '1501 Yamato Rd';
		addr.Street2__c = 'ADT';
		addr.City__c = 'Boca Raton';
		addr.State__c = 'FL';
		addr.PostalCode__c = '33431';
		addr.OriginalAddress__c = '1501 YAMATO RD+ADT+BOCA RATON+FL+33431';
		Insert addr;

		CH_Address__c chAddr = new CH_Address__c();
		chAddr.Street__c = '1501 YAMATO RD';
		chAddr.Street2__c = 'ADT';
		chAddr.City__c = 'BOCA RATON';
		chAddr.State__c = 'FL';
		chAddr.PostalCode__c = '33431';
		insert chAddr;
    }
}