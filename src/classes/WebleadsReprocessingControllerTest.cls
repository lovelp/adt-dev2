/************************************* MODIFICATION LOG ********************************************************************************************
* 
* WebleadsReprocessingControllerTest
*
* DESCRIPTION :
* Test Coverage for WebleadsReprocessingBatch, WebleadsReprocessingController class
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE             Ticket         REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* 
* Jitendra Kothari            11/22/2018      		-    - Test Coverage
*/
@isTest (SeeAllData = false)
private class WebleadsReprocessingControllerTest {
	private static list<AuditLog__c> auditLogs;
	private static void createTestdata(){
		auditLogs = new list<AuditLog__c>();
		AuditLog__c auditLogObj = new AuditLog__c(LineOfBusiness__c = '1100 - Residential',email__c='abc@test.com',Error__c='DNIS_ERROR;POSTALCODE_ERROR');
        auditLogObj.PromotionCode__c='abc';
        auditLogObj.type__c='Lead';
        auditLogObj.status__c='Form Error';
        auditLogObj.ZipCode__c='302021';
        auditLogs.add(auditLogObj);
        
        AuditLog__c auditLogObj1 = new AuditLog__c(LineOfBusiness__c = '1100 - Residential',email__c='abc@test.com',Error__c='DNIS_ERROR;POSTALCODE_ERROR');
        auditLogObj1.PromotionCode__c='abc1';
        auditLogObj1.type__c='Lead';
        auditLogObj1.status__c='Form Error';
        auditLogObj1.ZipCode__c='3020211';
        auditLogs.add(auditLogObj1);
        insert auditLogs;
        
        list<DNIS__c> dnis = new list<DNIS__c>();
        
        DNIS__c dd = new DNIS__c(Name = 'abc');
        dnis.add(dd);
        /*DNIS__c dd1 = new DNIS__c(Name = 'abc1');
        dnis.add(dd1);*/
        insert dnis;
        
        List<Postal_Codes__c> pp = new List<Postal_Codes__c>();
	    Postal_Codes__c p = new Postal_Codes__c(Name = '302021',BusinessID__c = '1100');
	    pp.add(p);
	    /*postal_Codes__c p1 = new Postal_Codes__c(Name = '3020211',BusinessID__c = '1100');
	    pp.add(p1);*/
	    insert pp;
	}
    static testMethod void myUnitTest() {
        createTestdata();
        Test.startTest();
        
        Test.setCurrentPageReference(Page.WebleadsReprocessingPage);
		ApexPages.StandardSetController stdcon = new Apexpages.StandardSetController(auditLogs);
        WebleadsReprocessingController webLead = new WebleadsReprocessingController(stdcon);
        webLead.getgroupIdOption();
        webLead.selectedGroup = auditLogs.get(0).Id + ',' + auditLogs.get(1).Id;
        webLead.errType = 'POSTALCODE_ERROR';
		webLead.processBatch();
		
		webLead.selectedGroup = auditLogs.get(0).Id + ',' + auditLogs.get(1).Id;
        webLead.errType = 'DNIS_ERROR';
		webLead.processBatch();
        Test.stopTest();
    }
}