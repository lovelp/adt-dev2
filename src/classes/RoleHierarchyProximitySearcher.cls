/************************************* MODIFICATION LOG ********************************************************************************************
* RoleHierarchyProximitySearcher
*
* DESCRIPTION : Extends ProximitySearcher and implements search algorithms to find nearby leads and accounts according to the 
*               role hierarchy present in SFDC.  These algorithms apply to Sales Executives and System Administrators and expect the
*               use of the with sharing keyword 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani			3/23/2012			- Original Version
*
*													
*/

public with sharing class RoleHierarchyProximitySearcher extends ProximitySearcher
{	
	private enum SObjectType {ACCOUNT, LEAD} 
	
	private static final String CLASS_NAME = 'RoleHierarchyProximitySearcher';
	
	public override String getName() {
		return CLASS_NAME;
	}   
     	
    public override List<Lead> findNearbyLeads(String boundingCondition, String endLimit, User currUser, Boolean isSalesRep, Boolean isManager)
    {
    	list<Lead> leadList = new list<Lead>();
    	
    	try
        {
        	whereString = getWhereString(currUser, isSalesRep, isManager, SObjectType.LEAD);	
        	System.debug('findNearbyLeads.....QUERY...: '+ NEARBY_LEAD_BASE_QUERY + whereString + boundingCondition + ' LIMIT ' + endLimit);				
			leadList =  database.Query(NEARBY_LEAD_BASE_QUERY + whereString + boundingCondition + ' LIMIT ' + endLimit);			
		}
        catch(Exception e)
        {
	        System.debug('findNearbyLeads----------Exception occurred: ' + e.getMessage() + ' ' + e.getStackTraceString());        	
        }
        return leadList;
    }
    
    public override List<Account> findNearbyAccounts(String boundingCondition, String endLimit, User currUser, Boolean isSalesRep, Boolean isManager)
    {   
        list<Account> accountList = new list<Account>();       
        
        try
        {
        	whereString = getWhereString(currUser, isSalesRep, isManager, SObjectType.ACCOUNT);	
        	System.debug('findNearbyAccounts.....QUERY...: '+ NEARBY_ACCOUNT_BASE_QUERY + whereString + boundingCondition + ' LIMIT ' + endLimit);
        	accountList =  database.Query(NEARBY_ACCOUNT_BASE_QUERY + whereString + boundingCondition + ' LIMIT ' + endLimit);			
		}
        catch(Exception e)
        {
	        System.debug('findNearbyAccounts----------Exception occurred: ' + e.getMessage() + ' ' + e.getStackTraceString());        	
        }
        return accountList; 
    }
    
    private String getWhereString(User currUser, Boolean isSalesRep, Boolean isManager, SObjectType sob)
	{
		//common fields
		String wherestr = ' WHERE ';
					
		// Active filter
		if (sob == SObjectType.ACCOUNT)
		{						
			wherestr += ' LeadStatus__c = \'Active\'';
		}
		else if (sob == SObjectType.LEAD)
		{
			wherestr += ' IsConverted = false';
		}
		
		wherestr += ' AND ';	
					
		return wherestr;
	}	
}