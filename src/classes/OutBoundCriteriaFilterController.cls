public with sharing class OutBoundCriteriaFilterController {
    public String selectedField { get; set; }
    public static Map<Boolean,String> isBalanced = new Map<Boolean,String>();
    public String campaignId{get;set;}
    public Set<String> dataTypeFieldsTextList = new Set<String>();
    public Set<String> dataTypeFieldsDateList = new Set<String>();
    public Set<String> dataTypeFieldsIntegerList = new Set<String>();
    public Set<String> dataTypeFieldsDoubleList = new Set<String>();
    public String primarySOQLCondition{get;set;}
    public String channelSOQLCondition{get;set;}
    
    
    public class SelectionCriteriaObjectDataWrap {
        public String selectedValueRowData;
        public String selectedOperatorRowData;
        public String selectedObjectRowData;
        public String selectedFieldRowData;
        public String filterLogic;
        public String dataTypeValue;
        
        
        SelectionCriteriaObjectDataWrap(){
          selectedValueRowData ='';
          selectedOperatorRowData ='';
          selectedObjectRowData ='';
          selectedFieldRowData ='';
          filterLogic = '';
          dataTypeValue = '';
        } 
     }
     
    public class PrimarySOQLJsonResponse{
        public String primaryFieldValue;
        public String primaryOperatorValue;
        public String primaryInputValue; 
        primarySOQLJsonResponse(){
         primaryFieldValue ='';
         primaryOperatorValue ='';
         primaryInputValue =''; 
        }
    }
    
    /*
    public class PrimaryChannelJsonResponse {
        public String primaryChannelOperator;
        public String primaryChannelValue;
        public String primaryChannelField;
        PrimaryChannelJsonResponse(){
            primaryChannelOperator = '';
            primaryChannelValue = '';
            primaryChannelField = '';
        }
    }
    */
    public OutBoundCriteriaFilterController(){
        campaignId=ApexPages.currentPage().getParameters().get('campaignId');
        this.assignDataTypeValues();
    }
    
    public String getDataTypeDateValues(){
        if(dataTypeFieldsDateList != null){
            return JSON.serialize(dataTypeFieldsDateList);
        }
        else {
            return null;
        }
        
    }
    
    
    public String getDataTypeTextValues(){
        if(dataTypeFieldsDateList != null){
            return JSON.serialize(dataTypeFieldsTextList);
        }
        else {
            return null;
        }
        
    }
    
    public String getDataTypeIntegerValues(){
        if(dataTypeFieldsDateList != null){
            return JSON.serialize(dataTypeFieldsDoubleList);
        }
        else {
            return null;
        }
        
    }
    
     public String getDataTypeDoubleValues(){
        if(dataTypeFieldsDateList != null){
            return JSON.serialize(dataTypeFieldsIntegerList);
        }
        else {
            return null;
        }
        
    }
    public static Map<String,string> operatorMapping=new Map<String,String>{'>' => 'Greater Than','<'=>'Less Than','='=>'Equal To','!='=>'Not Equal To','>='=>'Greater or Equal','<='=>'Less or Equal','IN'=>'IN','NOT IN'=>'NOT IN','LIKE'=>'LIKE','Age'=>'Age'};
    
    public void ParsePrimarySOQL(){
        List<primarySOQLJsonResponse> primarySOQLData=new List<primarySOQLJsonResponse>();
        List<primarySOQLJsonResponse> channelSOQLData=new List<primarySOQLJsonResponse>();
        String primarySOQL;
        primarySOQLJsonResponse primarySOQLObject = new primarySOQLJsonResponse();
        primarySOQLJsonResponse channelSOQLObject = new primarySOQLJsonResponse();
        campaign cp=[select id,primary_soql__c from campaign where id=:campaignId];
        if(!String.isBlank(cp.primary_soql__c)){
            primarySOQL=cp.primary_soql__c;
            
            Integer Startindex=primarySOQL.indexof('from Account WHERE')+19;
            Integer endIndex;
            if(primarySOQL.contains('Channel__c')){
                endIndex=primarySOQL.indexof('Channel__c')-5;
            }else{
                endIndex=primarySOQL.length();
            }
            
            String wherecondition=primarySOQL.substring(startindex,endindex);
            List<String> soqldata=wherecondition.split(' ');
            
            if(soqldata.size()==3){
                primarySOQLObject.primaryFieldValue=soqlData[0];
                primarySOQLObject.primaryOperatorValue=OutBoundCriteriaFilterController.operatorMapping.get(soqlData[1]);
                primarySOQLObject.primaryInputValue=soqlData[2];
            
            }
            
            else{
                
                
                primarySOQLObject.primaryFieldValue=soqlData[0];
                primarySOQLObject.primaryOperatorValue='Age';
                
                Date myDate = date.today();
                date selecteddate=date.valueof(soqldata[2]);
                integer daysBetween1=selecteddate.daysbetween(mydate);
                
                selecteddate=date.valueof(soqldata[6]);
                integer daysBetween2=selecteddate.daysbetween(mydate);
                
                primarySOQLObject.primaryInputValue=String.valueof(daysBetween1)+','+string.valueof(daysBetween2);
                
                
            }
            
            if(primarySOQL.contains('Channel__c')){
                Startindex=primarySOQL.indexof('Channel__c');
                endIndex=primarySOQL.length();
                wherecondition=primarySOQL.substring(startindex,endindex);
                
                soqldata=wherecondition.split(' ');
                
                
                
                channelSOQLObject.primaryFieldValue=soqlData[0];
                channelSOQLObject.primaryOperatorValue=OutBoundCriteriaFilterController.operatorMapping.get(soqlData[1]);
                String channelValue=wherecondition.subString(wherecondition.indexOf('IN')+3,wherecondition.length());
                
                
                channelValue=channelValue.remove('\'');
                channelValue=channelValue.remove(')');
                channelValue=channelValue.remove('(');
                channelValue=channelValue.remove('%');
                
                channelSOQLObject.primaryInputValue=channelValue;
             
                
    
            }
            
            
        }
        primarySOQLData.add(primarySOQLObject);
        channelSOQLData.add(channelSOQLObject);
        system.debug('primary soql data is ----------------------------------------------'+primarySOQLObject);
        primarySOQLCondition= JSON.serialize(primarySOQLData);
        system.debug('primary soql data is ----------------------------------------------'+channelSOQLData);
        channelSOQLCondition= JSON.serialize(channelSOQLData);
        system.debug('channel SOQL condition'+channelSOQLCondition);
    }
    
  
    public void assignDataTypeValues(){
        ParsePrimarySOQL();
        Map<String,String> objectMap = new Map<String,String>();
        Map<String,String> fieldMapType = new Map<String,String>();
        Schema.DescribeFieldResult objectResultList = Campaign_Selection_Criteria_item__c.ObjectsList__c.getDescribe();
        List<Schema.PicklistEntry> ple = objectResultList.getPicklistValues();
            for(Schema.PicklistEntry p : ple){
                fieldMapType = OutBoundFilterLogicValidation.FieldtypeMapping(p.getValue());
                for(String typeValue : fieldMapType.keySet()){
                    if(fieldMapType.get(typeValue).equalsIgnoreCase('Date') || fieldMapType.get(typeValue).equalsIgnoreCase('DateTime')){
                        dataTypeFieldsDateList.add(typeValue);
                    }
                    else if(fieldMapType.get(typeValue).equalsIgnoreCase('TEXTAREA') || fieldMapType.get(typeValue).equalsIgnoreCase('String')){
                        dataTypeFieldsTextList.add(typeValue);
                    }
                    else if(fieldMapType.get(typeValue).equalsIgnoreCase('Integer')){
                        dataTypeFieldsIntegerList.add(typeValue);
                    }
                    else if(fieldMapType.get(typeValue).equalsIgnoreCase('Double')){
                        dataTypeFieldsDoubleList.add(typeValue);
                    }
                }
            }

    }
    
    
    
    
    
    public Integer getCriteriaRowsLimit(){
        CampaignDefaults__c cd = CampaignDefaults__c.getInstance();
        Integer rowl = Integer.valueOf(cd.campaignCriteriaLimit__c);
        if(rowl != null){
            return rowl;
        }
        else {
            return 20;
        }
    } 
 
    public String getCampaignName() {
        String campaignName;
        if(campaignId != null){
            Campaign campaign = [SELECT Id,Name FROM Campaign WHERE Id =: campaignId];
            if(campaign != null){
                    campaignName = campaign.Name;
            }
        }
        
        return campaignName;
    }
    
    public Boolean isDateTime(String val){
        Boolean flag = false;
        if( val.countMatches(':') >= 2){
            flag = true;
        }
        return flag;
    }
     public String getCurrentData(){
        if(campaignId != null){
        List<SelectionCriteriaObjectDataWrap> dataWrapList = new List<SelectionCriteriaObjectDataWrap>();
        List<Campaign_Selection_Criteria_item__c> campaignSelectionCriteriaItems = [SELECT Id,Campaign__c,
        Selection_Value__c,
        Selection_Comparison__c,ObjectsList__c,Field__c,Campaign__r.Selection_Logic__c FROM Campaign_Selection_Criteria_item__c 
        WHERE Campaign__c =: campaignId];
        
        for(Campaign_Selection_Criteria_item__c selectionItem : campaignSelectionCriteriaItems){
            SelectionCriteriaObjectDataWrap selectionCriteriaObj = new SelectionCriteriaObjectDataWrap();
            if(selectionItem.Selection_Value__c != null ){
                if(isDateTime(selectionItem.Selection_Value__c)){
                    String[] dateVal = selectionItem.Selection_Value__c.split('T');
                    selectionCriteriaObj.selectedValueRowData = dateVal[0];
                }
                else {
                    selectionCriteriaObj.selectedValueRowData = selectionItem.Selection_Value__c;
                }
                
            }
            if(selectionItem.Field__c != null ){
                String[] fieldValue = selectionItem.Field__c.split(':');
                if(fieldValue.size() > 1 && fieldValue[1] != null){
                    selectionCriteriaObj.selectedFieldRowData = fieldValue[1];
                }
                else {
                    if(dataTypeFieldsTextList.contains(selectionItem.Field__c)){
                        selectionCriteriaObj.dataTypeValue = 'text';
                    }
                    else if(dataTypeFieldsDateList.contains(selectionItem.Field__c)){
                        selectionCriteriaObj.dataTypeValue = 'text';
                    }
                    else if(dataTypeFieldsIntegerList.contains(selectionItem.Field__c)){
                        selectionCriteriaObj.dataTypeValue = 'text';
                    }
                    else {
                        selectionCriteriaObj.dataTypeValue = 'text';
                    }
                    selectionCriteriaObj.selectedFieldRowData = selectionItem.Field__c;
                }
            }
            if(selectionItem.Selection_Comparison__c != null ){
                selectionCriteriaObj.selectedOperatorRowData = selectionItem.Selection_Comparison__c;
            }
            if(selectionItem.ObjectsList__c != null ){
                selectionCriteriaObj.selectedObjectRowData = selectionItem.ObjectsList__c;
            }
            if(selectionItem.Campaign__r.Selection_Logic__c != null ){
                selectionCriteriaObj.filterLogic = selectionItem.Campaign__r.Selection_Logic__c;
            }
            
            dataWrapList.add(selectionCriteriaObj);
        }
        return JSON.serialize(dataWrapList);
        }
        else {
            return null;
        }
     } 
      
      
    @RemoteAction
    public static Map<String,String> getChannelFields(){
        Map<String,String> channelFieldMap = new Map<String,String>();
        Schema.DescribeFieldResult channelFieldList = Account.Channel__c.getDescribe();
          List<Schema.PicklistEntry> ple = channelFieldList.getPicklistValues();
            for(Schema.PicklistEntry p : ple){
            channelFieldMap.put(p.getLabel(),p.getValue());
            }
         return channelFieldMap;
         return null;   
    }
      
      
    @RemoteAction
    public static Map<String,String> getBusinessIdValues(){
        Map<String,String> businessIdMap = new Map<String,String>();
        Schema.DescribeFieldResult businessIdFieldList = Account.Business_Id__c.getDescribe();
          List<Schema.PicklistEntry> ple = businessIdFieldList.getPicklistValues();
            for(Schema.PicklistEntry p : ple){
            businessIdMap.put(p.getLabel(),p.getValue());
            }
         return businessIdMap;
    } 
    
     @RemoteAction
    public static Map<String,String> getLeadOriginValues(){
        Map<String,String> leadOriginMap = new Map<String,String>();
        Schema.DescribeFieldResult leadOriginFieldList = Account.Lead_Origin__c.getDescribe();
          List<Schema.PicklistEntry> ple = leadOriginFieldList.getPicklistValues();
            for(Schema.PicklistEntry p : ple){
            leadOriginMap.put(p.getLabel(),p.getValue());
            }
         return leadOriginMap;
    }
    
     @RemoteAction
    public static Map<String,String> getDispositionDetailValues(){
        Map<String,String> dispositionDetailMap = new Map<String,String>();
        List<Call_Disposition__c> dispoDetailList = [SELECT Name FROM Call_Disposition__c WHERE Active__c = true AND Tier_One__c != null ];
            for(Call_Disposition__c p : dispoDetailList){
                if(String.isNotBlank(p.Name)){
                    dispositionDetailMap.put(p.Name,p.Name);
                }            
            }
         return dispositionDetailMap;
    } 
    
     @RemoteAction
    public static Map<String,String> getDispositionTypeValues(){
        Map<String,String> dispositionTypeMap = new Map<String,String>();
        List<Call_Disposition__c> dispoTypeList = [SELECT Name FROM Call_Disposition__c WHERE Active__c = true AND Tier_One__c = null ];
            for(Call_Disposition__c p : dispoTypeList){
                if(String.isNotBlank(p.Name)){
                    dispositionTypeMap.put(p.Name,p.Name);
                }            
            }
         return dispositionTypeMap;
    } 
     
    @RemoteAction
    public static Map<String,String> getObjectOptions() {
        Map<String,String> objectMap = new Map<String,String>();
        Schema.DescribeFieldResult objectResultList = Campaign_Selection_Criteria_item__c.ObjectsList__c.getDescribe();
        List<Schema.PicklistEntry> ple = objectResultList.getPicklistValues();
            for(Schema.PicklistEntry p : ple){
            objectMap.put(p.getLabel(),p.getValue());
            }
            
        return objectMap;
    }
    
    @RemoteAction
    public static Map<String,String> getFieldOptions() {
        Map<String,String> objectField = new Map<String,String>();
            Schema.DescribeFieldResult fieldResultList = Campaign_Selection_Criteria_item__c.Field__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResultList.getPicklistValues();
            for(Schema.PicklistEntry p : ple){
                objectField.put(p.getLabel(), p.getValue());
            }
               
        return objectField;
    }
    
    @RemoteAction
    public static Map<String,String> getOperatorOptions() {
        Map<String,String> objectOperator = new Map<String,String>();
            Schema.DescribeFieldResult operatorResultList = Campaign_Selection_Criteria_item__c.Selection_Comparison__c.getDescribe();
            List<Schema.PicklistEntry> ple = operatorResultList.getPicklistValues();
            for(Schema.PicklistEntry p : ple)
              objectOperator.put(p.getLabel(), p.getValue()); 
        return objectOperator;
    }
    
    @RemoteAction
    public static Map<Boolean,String> validateFilterLogic(String primaryRow,String channelData,String SOQLData, String filterLogic,String campaignId) {
        String SQOLQuery = '';
        OutBoundFilterLogicValidation validateLogic = new OutBoundFilterLogicValidation(primaryRow,channelData,SOQLData,filterLogic,campaignId);
        isBalanced = validateLogic.validateFilterLogic();
        return isBalanced;
    }
    
    @RemoteAction
    public static Map<Boolean,String> saveCampaign(String primaryRow,String channelData,String SOQLData, String filterLogic,String campaignId){
                
                system.debug('Primary SOQL '+primaryRow);
                system.debug('Secondary SOQL'+soqldata);
                system.debug('Channel data '+channelData);
                Map<Boolean,String> SQOLQueryMap = new Map<Boolean,String>();
                OutBoundFilterLogicValidation validateLogic = new OutBoundFilterLogicValidation(primaryRow,channelData,SOQLData,filterLogic,campaignId);
                SQOLQueryMap = validateLogic.buildQuery();
                return SQOLQueryMap;
    }
    
    @RemoteAction
    public static String backToCampaign(String campaignId){
        if(campaignId != null){
            String baseUrlCampaign = URL.getSalesforceBaseUrl().toExternalForm();
            baseUrlCampaign = baseUrlCampaign+'/'+campaignId;
            return baseUrlCampaign;
        }
        else {
            return null;
        }
    }
   
}