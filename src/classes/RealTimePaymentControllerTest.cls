@isTest(seealldata=false)
public class RealTimePaymentControllerTest{
    public static Real_Time_Payment__c rtpayment;
    private static void createTestData(){
        //IntegrationSettings__c is=[select id,MMBSearchUsername__c,MMBSearchPassword__c from IntegrationSettings__c limit 1];
        IntegrationSettings__c is= new IntegrationSettings__c();
        is.MMBSearchUsername__c='salesforceclup';
        is.MMBSearchPassword__c='abcd1234';
        is.MMBSearchCalloutTimeout__c=60000;
        is.MMBSearchEndpoint__c='https://sgbeta.adt.com/adtclup';
        upsert is;
        rtpayment = new Real_Time_Payment__c();
        rtpayment.MMBCustomerNo__c = '2371739';
        rtpayment.MMBJobNo__c = '75869881';
        rtpayment.PaymentAmount__c = 1.0;
        rtpayment.Client__c = RealTimePaymentController.Telemar;
        insert rtpayment;
    }
    private static testmethod void testRealTimePaymentController1(){
        createTestData();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseCustomerInfoApi());
        Test.setCurrentPageReference(new PageReference('Page.RealTimePayment')); 
        System.currentPageReference().getParameters().put('c','2371739');
        System.currentPageReference().getParameters().put('j','75869881');
        System.currentPageReference().getParameters().put('p','1000');
        RealTimePaymentController rtpc=new RealTimePaymentController();
        rtpc.init();
        rtpc.inPayAmt='1';
        rtpc.rtp = rtpayment;
        Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteSearchApiGenerator());
        rtpc.inTranJson = '{"transId":"1","appCode":"1","name":"1","paymentType":"1","ccNumber":"123443211234","expMonth":"11","expYear":"2019",' + 
            '"ccType":"Mastercard","cardBrandSelected":"1","CVVMatch":"1","AVSMatch":"1","tokenId":"1","approvalCode":"1",' + 
            '"transactionStart":"1","transactionEnd":"1","hostedSecureID":"1","sessionId":"1","status":"1","amount":"100.00",' + 
            '"customer_id":"1","customer_email":"test@test.com","extToken":"1","customerRefNum":"1","ctiAffluentCard":"1",' + 
            '"ctiCommercialCard":"1","ctiDurbinExemption":"1","ctiHealthcareCard":"Y","ctiLevel3Eligible":"Y",' + 
            '"ctiPayrollCard":"Y","ctiPrepaidCard":"Y","ctiPINlessDebitCard":"Y","ctiSignatureDebitCard":"Y",'+
            '"ctiIssuingCountry":"1","profileProcStatus":"1","profileProcStatusMsg":"1","garbage":"1"}';
        
        rtpc.postPayment();
        Test.stopTest();
    }
    private static testmethod void testRealTimePaymentController(){
        createTestData();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseCustomerInfoApi());
        Test.setCurrentPageReference(new PageReference('Page.RealTimePayment')); 
        System.currentPageReference().getParameters().put('c','2371739');
        System.currentPageReference().getParameters().put('j','75869881');
        System.currentPageReference().getParameters().put('p','1000');
        //System.currentPageReference().getParameters().put('cl','MOBILETECH');
        RealTimePaymentController rtpc=new RealTimePaymentController();
        RealTimePaymentController.TranJson tj=new RealTimePaymentController.TranJson();
        rtpc.init();
        rtpc.search();
        rtpc.inGatewayCode = '';
        rtpc.outDepositPaid = '';
        rtpc.outOutstandingBalance = '';
        rtpc.inDesc = '';
        rtpc.inPayAmt='1';
        rtpc.payme();
        rtpc.payme();
        rtpc.cancelRequest();
        rtpc.inError = 'Error1|Error2';
        rtpc.inGatewayMsg = 'Test Message';
        rtpc.errorResponse();
        Test.stopTest();
    }
    public static testMethod void custLoanSuccess(){
        //createTestData();
        IntegrationSettings__c is= new IntegrationSettings__c(); 
        is.DPUsername__c='salesforceclup';
        is.DPPassword__c='abcd1234';
        is.FlexFinanceEndPoint__c='https://sgbeta.adt.com/success';
        upsert is;
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'TRUE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        insert rgvList;
        Address__c addr1 = new Address__c();
        addr1.Latitude__c = 38.72686000000000;
        addr1.Longitude__c = -77.25470100000000;
        addr1.Street__c = '8507 Oak Pointe Way';
        addr1.City__c = 'Fairfax Station';
        addr1.State__c = 'VA';
        addr1.PostalCode__c = '22o39';
        addr1.OriginalAddress__c = '8507 Oak Pointe Way, Fairfax Station, VA 22o39';			
        insert addr1;
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.name = '22102';
        pc.BusinessID__c = '1100';
        insert pc;
        Account a = TestHelperClass.createAccountData();
        a.Business_Id__c = '1100 - Residential';
        a.Channel__c = 'Resi Direct Sales';
        a.MMBOrderType__c = 'R1';
        a.PostalCodeId__c = pc.Id;
        a.MMBCustomerNumber__c = '300010540';
        a.MMBSiteNumber__c = '48857037';
        a.Equifax_Last_Check_DateTime__c = Datetime.now();
        a.EquifaxApprovalType__c = 'APPROV';
        a.TelemarAccountNumber__c = '12345';
        a.EquifaxRiskGrade__c = 'A';
        a.AddressID__c = addr1.id;
        update a;
        Opportunity opp = New Opportunity();
        opp.AccountId = a.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp; 
        scpq__SciQuote__c quo = new scpq__SciQuote__c();
        quo.Account__c = a.id;
        quo.scpq__OpportunityId__c = opp.id;
        quo.scpq__OrderHeaderKey__c = 'abcxyz';
        quo.scpq__SciLastModifiedDate__c = Date.today();
        quo.scpq__QuoteId__c = '123';
        quo.MMBJobNumber__c = '123456789';
        quo.MMBOrderType__c = 'N1';
        quo.QuotePaidOn__c = Datetime.now();
        quo.Job_Details__c = '<?xml version="1.0" encoding="UTF-8"?><Jobs AltPricing="Y" DisallowFinancing="N" Installment="36" CommRepHRID="z1232345z" CommRepName="Caroline Maitland" CommRepPhone="44 7740301000" CommRepType="FIELD" ContractSigned="N" CreatedByLoginId="ichang@adt.com.adtdev2" DOALock="N" DepositWaived="N" DepositWaiverLock="N" EasyPay="Y" EquifaxApprovalType="CAE1" EquifaxRiskGrade="A" LastConfigBy="cmaitland@adt.com.adtdev2" PaymentTakenBy="ichang@adt.com.adtdev2"><Job ADSCAddOn="0.00" ADSCBase="649.00" ADSCCorpDiscount="0.00" ADSCInstallPrice="674.00" ADSCInstallerCorpDiscount="0.00" ADSCInstallerLineDiscount="0.00" ADSCOtherAddOn="25.00" ADSCOtherCorpDiscount="0.00" ANSCAdditionalMonitoring="0.00" ANSCAdditionalQSP="0.00" ANSCBaseMonitoring="40.99" ANSCBaseQSP="7.00" ANSCCorpDiscount="0.00" ANSCFinalRMR="47.99" ANSCInstallerCorpDiscount="0.00" ANSCInstallerLineDiscount="0.00" ANSCOtherCorpDiscount="0.00" CSNumber="" DLL="N" DOALock="N" DefaultContract="SP040" JobNo="" JobType="INSTP" ProductFamily="Home Security" RequireElectrician="N" RequireLocksmith="N" SaleType="NewSale" TaskCode="RAI" TechnologyId="1048"><Packages><Package ID="L2 001" Name="Remote" /></Packages><Promotions /><JobLines> <JobLine Item="TS BUNDLE BA" Name="TS Wireless Sensors - Motion - Keyfob Bundle with:" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA020R" Name="1 Heat Detector Wireless" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA015" Name="Broadband External Gateway &amp; Interactive Module" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA027" Name="Interactive Chip (Required for older existing SWPro3K Panels)" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA017-2" Name="Pulse dimming lamp module" Package="L2 001" PackageQty="1.00" /><JobLine Item="L2 TS" Name="TSSC COMPLETE PACK, WHITE, 2-1-1, WITH RADIO - ATT LTE" Package="L2 001" PackageQty="1.00" /></JobLines></Job></Jobs>';
        insert quo;
        Quote_Job__c qJob = new Quote_Job__c();
        qJob.JobNo__c = '123456789';
        qJob.ParentQuote__c = quo.id;
        insert qJob;
        CustomerLoan__c cLoan = new CustomerLoan__c();
        cLoan.QuoteJobNumber__c = '123456789';
        cLoan.PaymentStatus__c = 'Not Paid';
        cLoan.AmountBilled__c  = 100;
        cLoan.AmountFinanced__c = 50;
        cLoan.NoteNumber__c = '123456789';
        insert cLoan;
        Test.setCurrentPageReference(new PageReference('Page.RealTimePayment')); 
        System.currentPageReference().getParameters().put('showLoanDataFlag','CustLoan');
        System.currentPageReference().getParameters().put('jobNumber','123456789');
        System.currentPageReference().getParameters().put('lId',cLoan.Id);
        RealTimePaymentController rtpc=new RealTimePaymentController();
        RealTimePaymentController.TranJson tj=new RealTimePaymentController.TranJson();
        rtpc.init();
        rtpc.inTranJson = '{"transId":"1","appCode":"1","name":"1","paymentType":"1","ccNumber":"123443211234","expMonth":"11","expYear":"2019",' + 
            '"ccType":"Mastercard","cardBrandSelected":"1","CVVMatch":"1","AVSMatch":"1","tokenId":"1","approvalCode":"1",' + 
            '"transactionStart":"1","transactionEnd":"1","hostedSecureID":"1","sessionId":"1","status":"1","amount":"100.00",' + 
            '"customer_id":"1","customer_email":"test@test.com","extToken":"1","customerRefNum":"1","ctiAffluentCard":"1",' + 
            '"ctiCommercialCard":"1","ctiDurbinExemption":"1","ctiHealthcareCard":"Y","ctiLevel3Eligible":"Y",' + 
            '"ctiPayrollCard":"Y","ctiPrepaidCard":"Y","ctiPINlessDebitCard":"Y","ctiSignatureDebitCard":"Y",'+
            '"ctiIssuingCountry":"1","profileProcStatus":"1","profileProcStatusMsg":"1","garbage":"1"}';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHTTPMMBResponse());
        rtpc.postPayment();
        Test.stopTest();        
    }
    public static testMethod void custLoanError(){
        Profile p = [select id from profile where name='System Administrator'];
        User u=new User(alias = 'stand', email='standarduser@testorg.com',
                        emailencodingkey='UTF-8', lastname='ADMIN',firstname='TestGlobal', languagelocalekey='en_US',
                        localesidkey='en_US', profileid = p.Id,
                        timezonesidkey='America/Los_Angeles', username='testing100@adt.com',EmployeeNumber__c = '12345678',
                        StartDateInCurrentJob__c = Date.today());
        insert u;
        System.runAs(u){
            //createTestData();
            IntegrationSettings__c is= new IntegrationSettings__c(); 
            is.DPUsername__c='salesforceclup';
            is.DPPassword__c='abcd1234';
            is.FlexFinanceEndPoint__c='https://sgbeta.adt.com/error500';
            upsert is;
            List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
            rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'TRUE'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
            rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
            insert rgvList;
            Address__c addr1 = new Address__c();
            addr1.Latitude__c = 38.72686000000000;
            addr1.Longitude__c = -77.25470100000000;
            addr1.Street__c = '8507 Oak Pointe Way';
            addr1.City__c = 'Fairfax Station';
            addr1.State__c = 'VA';
            addr1.PostalCode__c = '22o39';
            addr1.OriginalAddress__c = '8507 Oak Pointe Way, Fairfax Station, VA 22o39';			
            insert addr1;
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.name = '22102';
            pc.BusinessID__c = '1100';
            insert pc;
            Account acct = new Account();
            acct.Name = 'Test Sinha';
            acct.FirstName__c = 'Test';
            acct.LastName__c = 'Sinha';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.ShippingPostalCode = '22102';
            acct.Data_Source__c = 'TEST';
            acct.Business_Id__c = '1100 - Residential';
            acct.Channel__c = 'Resi Direct Sales';
            acct.MMBOrderType__c = 'R1';
            acct.PostalCodeId__c = pc.Id;
            acct.MMBCustomerNumber__c = '300010540';
            acct.MMBSiteNumber__c = '48857037';
            acct.Equifax_Last_Check_DateTime__c = Datetime.now();
            acct.EquifaxApprovalType__c = 'APPROV';
            acct.TelemarAccountNumber__c = '12345';
            acct.EquifaxRiskGrade__c = 'A';
            acct.AddressID__c = addr1.id;
            insert acct;
            Opportunity opp = New Opportunity();
            opp.AccountId = acct.Id;
            opp.Name = 'TestOpp1';
            opp.StageName = 'Prospecting';
            opp.CloseDate = Date.today();
            insert opp; 
            scpq__SciQuote__c quo = new scpq__SciQuote__c();
            quo.Account__c = acct.id;
            quo.scpq__OpportunityId__c = opp.id;
            quo.scpq__OrderHeaderKey__c = 'abcxyz';
            quo.scpq__SciLastModifiedDate__c = Date.today();
            quo.scpq__QuoteId__c = '123';
            quo.MMBJobNumber__c = '123456789';
            quo.MMBOrderType__c = 'N1';
            quo.QuotePaidOn__c = Datetime.now();
            quo.Job_Details__c = '<?xml version="1.0" encoding="UTF-8"?><Jobs AltPricing="Y" DisallowFinancing="N" Installment="36" CommRepHRID="z1232345z" CommRepName="Caroline Maitland" CommRepPhone="44 7740301000" CommRepType="FIELD" ContractSigned="N" CreatedByLoginId="ichang@adt.com.adtdev2" DOALock="N" DepositWaived="N" DepositWaiverLock="N" EasyPay="Y" EquifaxApprovalType="CAE1" EquifaxRiskGrade="A" LastConfigBy="cmaitland@adt.com.adtdev2" PaymentTakenBy="ichang@adt.com.adtdev2"><Job ADSCAddOn="0.00" ADSCBase="649.00" ADSCCorpDiscount="0.00" ADSCInstallPrice="674.00" ADSCInstallerCorpDiscount="0.00" ADSCInstallerLineDiscount="0.00" ADSCOtherAddOn="25.00" ADSCOtherCorpDiscount="0.00" ANSCAdditionalMonitoring="0.00" ANSCAdditionalQSP="0.00" ANSCBaseMonitoring="40.99" ANSCBaseQSP="7.00" ANSCCorpDiscount="0.00" ANSCFinalRMR="47.99" ANSCInstallerCorpDiscount="0.00" ANSCInstallerLineDiscount="0.00" ANSCOtherCorpDiscount="0.00" CSNumber="" DLL="N" DOALock="N" DefaultContract="SP040" JobNo="" JobType="INSTP" ProductFamily="Home Security" RequireElectrician="N" RequireLocksmith="N" SaleType="NewSale" TaskCode="RAI" TechnologyId="1048"><Packages><Package ID="L2 001" Name="Remote" /></Packages><Promotions /><JobLines> <JobLine Item="TS BUNDLE BA" Name="TS Wireless Sensors - Motion - Keyfob Bundle with:" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA020R" Name="1 Heat Detector Wireless" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA015" Name="Broadband External Gateway &amp; Interactive Module" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA027" Name="Interactive Chip (Required for older existing SWPro3K Panels)" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA017-2" Name="Pulse dimming lamp module" Package="L2 001" PackageQty="1.00" /><JobLine Item="L2 TS" Name="TSSC COMPLETE PACK, WHITE, 2-1-1, WITH RADIO - ATT LTE" Package="L2 001" PackageQty="1.00" /></JobLines></Job></Jobs>';
            insert quo;
            Quote_Job__c qJob = new Quote_Job__c();
            qJob.JobNo__c = '123456789';
            qJob.ParentQuote__c = quo.id;
            insert qJob;
            CustomerLoan__c cLoan = new CustomerLoan__c();
            cLoan.QuoteJobNumber__c = '123456789';
            cLoan.PaymentStatus__c = 'Not Paid';
            cLoan.AmountBilled__c  = 100;
            cLoan.AmountFinanced__c = 50;
            cLoan.NoteNumber__c = '123456789';
            insert cLoan;
            Test.setCurrentPageReference(new PageReference('Page.RealTimePayment')); 
            System.currentPageReference().getParameters().put('showLoanDataFlag','CustLoan');
            System.currentPageReference().getParameters().put('jobNumber','123456789');
            System.currentPageReference().getParameters().put('lId',cLoan.Id);
            RealTimePaymentController rtpc=new RealTimePaymentController();
            RealTimePaymentController.TranJson tj=new RealTimePaymentController.TranJson();
            rtpc.init();
            rtpc.inTranJson = '{"transId":"1","appCode":"1","name":"1","paymentType":"1","ccNumber":"123443211234","expMonth":"11","expYear":"2019",' + 
                '"ccType":"Mastercard","cardBrandSelected":"1","CVVMatch":"1","AVSMatch":"1","tokenId":"1","approvalCode":"1",' + 
                '"transactionStart":"1","transactionEnd":"1","hostedSecureID":"1","sessionId":"1","status":"1","amount":"100.00",' + 
                '"customer_id":"1","customer_email":"test@test.com","extToken":"1","customerRefNum":"1","ctiAffluentCard":"1",' + 
                '"ctiCommercialCard":"1","ctiDurbinExemption":"1","ctiHealthcareCard":"Y","ctiLevel3Eligible":"Y",' + 
                '"ctiPayrollCard":"Y","ctiPrepaidCard":"Y","ctiPINlessDebitCard":"Y","ctiSignatureDebitCard":"Y",'+
                '"ctiIssuingCountry":"1","profileProcStatus":"1","profileProcStatusMsg":"1","garbage":"1"}';
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHTTPMMBResponse());
            rtpc.postPayment();
            Test.stopTest();
        }
    }
}