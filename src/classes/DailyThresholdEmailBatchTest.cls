@isTest
private class DailyThresholdEmailBatchTest {
    
 @isTest(SeeAllData=true)
  static void testUpdateGeocodingBatch() 
  { 
   Test.startTest();
    DailyThresholdEmailBatch z= new DailyThresholdEmailBatch();
        DateTime timenow = system.now().addMinutes(5);
        String seconds = String.valueOf(timenow.second()); //Execute at Zero Seconds
        String minutes = String.valueOf(timenow.minute()); //Execute at every 10th minute of hour
        String hours = String.valueOf(timenow.hour()); // Execute Every Hour
        String sch = seconds + ' ' + minutes + ' * * * ?';
        String jobName = 'Threshold Email' + timenow;
        system.schedule(jobName, sch, z);
      Daily_Threshold__c dailyThreshold= new Daily_Threshold__c(Admin__c=1, BUDCO_CA__c=1, BUDCO_Not_CA__c=1, Broadview__c=1, Canadian_AS400__c=1, Informix__c=1, MMB__c=1, RIF__c=1);
      insert dailyThreshold;
      
          z.query='SELECT Admin__c, BUDCO_CA__c, BUDCO_Not_CA__c, Broadview__c, Canadian_AS400__c, CreatedById, CreatedDate, Name, IsDeleted, Informix__c, LastModifiedById, LastModifiedDate, MMB__c, OwnerId, RIF__c, Id, SystemModstamp FROM Daily_Threshold__c WHERE Id=\''+dailyThreshold.Id+'\''; 
      z= new DailyThresholdEmailBatch();
      //Database.executeBatch(z);
           Test.stopTest();
  }
  
  
  
  @isTest(SeeAllData=true)
  static void testUpdateGeocodingBatch1() 
  { 
   Test.startTest();
    DailyThresholdEmailBatch z= new DailyThresholdEmailBatch();
       // DateTime timenow = system.now().addMinutes(5);
        DateTime timenow = DateTime.Now().AddDays(-1);
 
        String seconds = String.valueOf(timenow.second()); //Execute at Zero Seconds
        String minutes = String.valueOf(timenow.minute()); //Execute at every 10th minute of hour
        String hours = String.valueOf(timenow.hour()); // Execute Every Hour
        String sch = seconds + ' ' + minutes + ' * * * ?';
        String jobName = 'Threshold Email' + timenow;
        Datetime mydate = DateTime.newInstance(system.today().addDays(-1).year(), system.today().addDays(-1).month(), system.today().addDays(-1).day());
        Datetime newDate = mydate.addDays(1);

        z.dowtoday=newDate.format('EEEE');
        system.schedule(jobName, sch, z);
      Daily_Threshold__c dailyThreshold= new Daily_Threshold__c(Admin__c=1, BUDCO_CA__c=1, BUDCO_Not_CA__c=1, Broadview__c=1, Canadian_AS400__c=1, Informix__c=1, MMB__c=1, RIF__c=1);
      insert dailyThreshold;
      
          z.query='SELECT Admin__c, BUDCO_CA__c, BUDCO_Not_CA__c, Broadview__c, Canadian_AS400__c, CreatedById, CreatedDate, Name, IsDeleted, Informix__c, LastModifiedById, LastModifiedDate, MMB__c, OwnerId, RIF__c, Id, SystemModstamp FROM Daily_Threshold__c WHERE Id=\''+dailyThreshold.Id+'\''; 
      z= new DailyThresholdEmailBatch();
      
      Test.stopTest();
  }
  
  @isTest(SeeAllData=true)
  static void testUpdateGeocodingBatch2() 
  { 
   Test.startTest();
    DailyThresholdEmailBatch z= new DailyThresholdEmailBatch();
        DateTime timenow = DateTime.Now().AddDays(1);
 
        String seconds = String.valueOf(timenow.second()); //Execute at Zero Seconds
        String minutes = String.valueOf(timenow.minute()); //Execute at every 10th minute of hour
        String hours = String.valueOf(timenow.hour()); // Execute Every Hour
        String sch = seconds + ' ' + minutes + ' * * * ?';
        String jobName = 'Threshold Email' + timenow;
        Datetime mydate = DateTime.newInstance(system.today().addDays(1).year(), system.today().addDays(1).month(), system.today().addDays(1).day());
        Datetime newDate = mydate.addDays(2);

        z.dowtoday=newDate.format('EEEE');
        system.schedule(jobName, sch, z);
        Daily_Threshold__c dailyThreshold1= new Daily_Threshold__c(Admin__c=1, BUDCO_CA__c=1, BUDCO_Not_CA__c=1, Broadview__c=1, Canadian_AS400__c=1, Informix__c=1, MMB__c=1, RIF__c=1);
        insert dailyThreshold1;
      
        z.query='SELECT Admin__c, BUDCO_CA__c, BUDCO_Not_CA__c, Broadview__c, Canadian_AS400__c, CreatedById, CreatedDate, Name, IsDeleted, Informix__c, LastModifiedById, LastModifiedDate, MMB__c, OwnerId, RIF__c, Id, SystemModstamp FROM Daily_Threshold__c WHERE Id=\''+dailyThreshold1.Id+'\''; 
        z= new DailyThresholdEmailBatch();
        Test.stopTest();
  }    
}