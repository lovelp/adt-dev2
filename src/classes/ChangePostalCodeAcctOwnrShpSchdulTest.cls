/*
 * Description: This is a test class for the following classes
                1. ChangePostalCodeAccountOwnershipSchedule
                2. ChangePostalCodeAccountOwnershipBatch
 * Created By: Shiva Pochamalla
 *
 * ---------------------------------------------------------------------------Changes below--------------------------------------------------------------------------------
 * 
 *
 */


@isTest
private class ChangePostalCodeAcctOwnrShpSchdulTest{
    public static PostalCodeReassignmentQueue__c pcrq;
    public static void createTestData(){
        
        //create custom settings
        
        BatchState__c bs=new BatchState__c();
        bs.name='PCOwnerChgScheduledBatchInterval';
        bs.value__c='2';
        insert bs;
        
        TestHelperClass.createReferenceDataForTestClasses();
        
        //create postal code
        List<Postal_Codes__c> postalCodes= TestHelperClass.createPostalCodes();
        
        
        //create postal code reasignment queue
        pcrq=new PostalCodeReassignmentQueue__c();    
        pcrq.TerritoryType__c ='Resi Direct Sales';
        pcrq.PostalCodeNewOnwer__c=userinfo.getuserid();
        pcrq.PostalCodeId__c=postalCodes[0].id;
        insert pcrq;
        
        
        //create users
        User u=testHelperClass.createUser(234343);
        List<user> users=new List<user>();
        
        Profile p = [select id from profile where name='System Administrator'];
        integer i= 47598347;
        User integrationUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='TestIntegration', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(integrationUser);
        
        i= 387463;
        User NewRehashUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='NewRehash', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(NewRehashUser);
        i= 38947;
        User ResaleAdmin=new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='ADMIN',firstname='TestGlobal', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(ResaleAdmin);
        
        INSERT users;
        
        //TestHelperClass.createrehashOwnerfortestclasses();
        
        //create address for creating accounts
        Address__c addr = new Address__c();
        addr.Latitude__c = 29.91760000000000;
        addr.Longitude__c = -90.17870000000000;
        addr.Street__c = '56528 E Crystal Ct';
        addr.City__c = 'Westwego';
        addr.State__c = 'LA';
        addr.PostalCode__c = '70094';
        addr.OriginalAddress__c = '56528 E CRYSTAL CT+WESTWEGO+LA+70094';
         
        insert addr;
        
        //create lead for the batch
        TestHelperClass.createLead(u,true); 
        
        Lead l = new Lead();
        l.LastName = 'TestLastName';
        l.Company = 'TestCompany';
        l.OwnerId = u.Id;
        l.LeadSource = 'BUDCO';
        l.NewMoverType__c = 'RL';
        l.SGCOM3__SG_infoGroupId__c = l.LeadSource == 'Salesgenie.com' ? '12345' : null;
        l.DispositionCode__c = 'Phone - No Answer';
        l.Channel__c = 'Resi Direct Sales';
        l.SiteStreet__c = '56528 E Crystal Ct';
        l.SiteCity__c = 'Westwego';
        l.SiteStateProvince__c = 'LA';
        l.SiteCountryCode__c = 'US';
        l.SitePostalCode__c = '70094';
        l.PostalCodeID__c = postalCodes[0].id;
        l.AddressID__c = addr.Id;
        l.Business_Id__c = '1100 - Residential';
        l.NewMoverDate__c = Date.today().addDays(-30);
        l.Affiliation__c='USAA';
        Insert l;
        l.leadsource='BUDCO';
        update l;
        
        
        
        //create accounts
        
        Account acct=TestHelperClass.createAccountData(addr,postalCodes[0].id,false);
        acct.channel__c='Resi Resale';
        acct.recordtypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('ADT NA Resale').getRecordTypeId();
        insert acct;
        
        Account acct1=TestHelperClass.createAccountData(addr,postalCodes[0].id,false);
        acct1.channel__c='Resi Resale Phone Sales';
        acct1.recordtypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('ADT NA Resale').getRecordTypeId();
        insert acct1;
        
    }
    
    //test method for RESI DIRECT SALES
    static testMethod void TestChangePostalCodeAccountOwnershipSchedule() {        
         createTestData();
         List<lead> leads=[select id from lead];
         system.debug('Leads inserted '+leads);
         test.starttest();
             ChangePostalCodeAccountOwnershipSchedule myClass = new ChangePostalCodeAccountOwnershipSchedule();   
             String chron = '0 0 23 * * ?';        
             system.schedule('Test Sched', chron, myClass);
         test.stopTest();
                  
    }
    
    //test method for RESI RESALE
    static testMethod void TestChangePostalCodeAccountOwnershipSchedule1() {        
         createTestData();
         pcrq.TerritoryType__c ='Resi Resale';
         update pcrq;
         test.starttest();
             ChangePostalCodeAccountOwnershipSchedule myClass = new ChangePostalCodeAccountOwnershipSchedule();   
             String chron = '0 0 21 * * ?';        
             system.schedule('Test Sched', chron, myClass);
         test.stopTest();
             
    }
    
     //test method for RESI RESALE
    static testMethod void TestChangePostalCodeAccountOwnershipSchedule2() {        
         createTestData();
         pcrq.TerritoryType__c ='Resi Resale Phone Sales';
         update pcrq;
         test.starttest();
             ChangePostalCodeAccountOwnershipSchedule myClass = new ChangePostalCodeAccountOwnershipSchedule();   
             String chron = '0 0 21 * * ?';        
             system.schedule('Test Sched', chron, myClass);
         test.stopTest();
             
    }
    
}