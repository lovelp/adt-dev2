/************************************* MODIFICATION LOG ************************************************************
* GetServicePowerApptService
*
* DESCRIPTION : Restful webservice to Service Power to get Install appointment dates
*-------------------------------------------------------------------------------------------------------------------
* DEVELOPER            	DATE          	TICKET			REASON
*-------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan     	01/25/2018		HRM - 6319 		Original Version
* Srinivas Yarramsetti	04/01/2018		HRM - 6577		Added PartnerId__c to separate ADT & RV
*/
public with sharing class GetServicePowerApptService{
    // Request
    public class GetSPApptOffersRequest{
        public GetServicePowerApptOffersInstall getApptOffersInstallRequest;
    }
    
    public class GetServicePowerApptOffersInstall{
        public integer numberOfOffers;
        public string appointmentType;
        public integer startIndex;
        public string postalCode;
        public string days;
        public string startDate;
        public string endDate;
        public integer importance;
        public string promSet;
        public integer branchNumber;
        public JobRequests[] jobRequests;
    }
    
    public class JobRequests{
        public string jobRequest;
        public string jobType;
        public integer extraDuration;
        public ExtraSkills[] extraSkills;
    }
    
    public class ExtraSkills{
        public string skillId;
    }
    
    // Response
    public class GetSPApptOffersResponse{
        public GetServicePowerApptOffersInstallResponse getApptOffersInstallResponse;
    }
    
    public class GetServicePowerApptOffersInstallResponse{
        public JobRequestInResponse[] jobRequests;
    }
    
    public class JobRequestInResponse{
        public String jobRequest;
        public String errorFlag;
        public String errorMessage;
        public Offer[] returnValue;
    }
    
    public class Offer{
        public string startDate;
        public string endDate;
        public string promBand;
        public string promType;
        public string day;
        public string offerToken;
        public string offerText;
        public string travelTime;
    }
    
    public class errorWrapper{
        public errorBody[] errors;
    }
    
    public class errorBody{
        public string status;
        public string errorCode;
        public string errorMessage;
    }
    public Boolean isBadResponse;
    public errorWrapper errWrap; 
    
    public GetServicePowerApptOffersInstallResponse getOffers(String accountId){
        Account a = new account();
        a = getAccountInfo(accountId);
        // Not considering default 999 value of MMB Town Id
        if(String.isNotBlank(a.SitePostalCode__c) && a.MMB_TownID__c != '999'){
            // Custom setting & Custom Meta Data initialization
            InstallAppointmentsSPConfig__c setting = InstallAppointmentsSPConfig__c.getOrgDefaults();
            
            // Create Request parameters
            GetServicePowerApptOffersInstall reqParam = new GetServicePowerApptOffersInstall();
            reqParam.numberOfOffers = (setting.No_Of_Offers__c != null)? Integer.ValueOf(setting.No_Of_Offers__c):1;
            reqParam.appointmentType = setting.Appointment_Type__c;
            reqParam.startIndex = 1;
            reqParam.postalCode = a.SitePostalCode__c;
            reqParam.days = setting.DaysOfTheWeek__c;
            reqParam.startDate = String.ValueOf(Date.Today()).remove(' 00:00:00');
            Integer dateFrame = (setting.No_Of_Days__c != null)? Integer.ValueOf(setting.No_Of_Days__c) : 30;
            reqParam.endDate = String.ValueOf(Date.Today() + dateFrame).remove(' 00:00:00');
            reqParam.importance = (setting.Importance__c != null)? Integer.ValueOf(setting.Importance__c):9;
            reqParam.promSet = setting.Prom_Set__c;
            reqParam.branchNumber = Integer.ValueOf(a.MMB_TownID__c);
            List<JobRequests> jobReqList = new List<JobRequests>();
            
            for(Job_Request__mdt metaDataSetting: [Select DeveloperName,MasterLabel, Duration__c, Skill1__c, Skill2__c, PartnerId__c From Job_Request__mdt Where Active__c = true And BusinessId__c =: a.Business_Id__c]){
                if (string.isNotBlank(string.valueOf(metaDataSetting.PartnerId__c)) )
                {                    
                    list<string> lstPartnerId = string.valueOf(metaDataSetting.PartnerId__c).toLowerCase().split(',');
                    set<string> setPartnerId = new set<string> ();
                    for (string str : lstPartnerId)
                    {
                        setPartnerId.add(str.trim());
                    }
                    if (setPartnerId.contains('ADT'.toLowerCase().trim()))
                    {                        
                        JobRequests jReq = new JobRequests();
                        jReq.jobRequest = metaDataSetting.MasterLabel;
                        jReq.jobType = metaDataSetting.Skill1__c.substring(0,4) + metaDataSetting.Skill2__c.substring(0,2);
                        jReq.extraDuration = Integer.ValueOf(metaDataSetting.Duration__c);
                        
                        List<ExtraSkills> extraSkillsList = new List<ExtraSkills>();
                        ExtraSkills skill1 = new ExtraSkills();
                        skill1.skillID = metaDataSetting.Skill1__c;
                        extraSkillsList.add(skill1);
                        ExtraSkills skill2 = new ExtraSkills();
                        skill2.skillID = metaDataSetting.Skill2__c;
                        extraSkillsList.add(skill2);
                        ExtraSkills skill3 = new ExtraSkills();
                        skill3.skillID = 'Branch-'+a.MMB_TownID__c;
                        extraSkillsList.add(skill3);
                        jReq.extraSkills = extraSkillsList;
                        jobReqList.add(jReq);
                    }
                }                
               
            }
            reqParam.JobRequests = jobReqList;
            
            GetSPApptOffersRequest getSPOffers = new GetSPApptOffersRequest();
            getSPOffers.getApptOffersInstallRequest = reqParam;
            if(jobReqList.size() > 0){
                string payLoad = JSON.serialize(getSPOffers);

                Http http = new Http();
                HttpRequest req = new HttpRequest();
                String userName = IntegrationSettings__c.getinstance().GetInstallOffersSPUsername__c;
                String password = IntegrationSettings__c.getinstance().GetInstallOffersSPPassword__c;
                Integer timeout = Integer.ValueOf(IntegrationSettings__c.getinstance().GetInstallOffersSPTimeout__c);
                String endpoint = IntegrationSettings__c.getinstance().GetInstallOffersSPEndPoint__c;
                
                //postman mock server setup by Srini.
                //req.setMethod('GET');
                //String endpoint = 'https://d47e450c-23ae-4d89-be55-d2c809558afc.mock.pstmn.io/TestCase2';
                
                Blob headerValue = Blob.valueOf(userName + ':' + password);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                system.debug('Header :'+authorizationHeader);
                req.setHeader('Authorization', authorizationHeader);
                req.setHeader('Content-Type', 'application/json');
                req.setEndpoint(endpoint);
                req.setTimeout(timeout);
                req.setMethod('POST');
                req.setBody(payLoad);
                system.debug('HTTP Request: '+payLoad);
                HttpResponse res = new HttpResponse();
                res = http.send(req);
                system.debug('HTTP Response: '+res.getBody());
                if(res.getStatusCode() == 200){
                    // Parse the response
                    GetSPApptOffersResponse resp = new GetSPApptOffersResponse();
                    resp = (GetSPApptOffersResponse)JSON.deserialize(res.getBody(),GetSPApptOffersResponse.class);
                    system.debug('Deserialized Class: '+resp);
                    return resp.getApptOffersInstallResponse;
                }
                else{
                    isBadResponse = true;
                    errWrap = new errorWrapper();
                    errWrap =  (errorWrapper)JSON.deserialize(res.getBody(),errorWrapper.class);
                    
                    // for any invalid input parameters, log a record into ADTLog
                    ADTApplicationMonitor.log ('Get Install Service Power Callout Error', errWrap.errors[0].errorMessage, true, ADTApplicationMonitor.CUSTOM_APP.SERVICEPOWER);
                    system.debug('Deserialized Class: '+errWrap);
                    return null;
                }
            }
        }
        return null;
    }
    
    private account getAccountInfo(String accountId){
        return [Select Business_Id__c, SitePostalCode__c, MMB_TownID__c from Account Where Id =: accountId];
    }
}