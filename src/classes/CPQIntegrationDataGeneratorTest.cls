/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : AddressHelperTest is a test class for CPQIntegrationDataGenerator.cls
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan            02/17/2016          - Initial Version
* Jitendra Kothari           03/26/2019          - HRM-9627
*
*                                                    
*/
@isTest
private class CPQIntegrationDataGeneratorTest {
    private static Account acc;
    static testMethod void testIntegrationData(){
        Town__c town = new Town__c();
        town.BusinessID__c = '1100';
        town.Oracle_Town__c = '1234';
        town.APM_town__c = true;
        town.Active__c = true;
        town.P2_Enabled__c = true;
        town.Name = 'Test';
        insert town;
        
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.Name = '12345';
        pc.Town_Lookup__c = town.id;
        pc.TownId__c = '1234';
        pc.APM_PostalCode__c = true;
        insert pc;
        
        Address__c add = new Address__c();
        add.Street__c = 'abc';
        add.OriginalAddress__c = 'xyz';
        add.PostalCode__c = '12345';
        insert add;  
        
        ResaleGlobalVariables__c globalVar1 = new ResaleGlobalVariables__c();
        globalVar1.Name = 'DataRecastDisableAccountTrigger';
        globalVar1.value__c = 'FALSE';
        insert globalVar1;
        ResaleGlobalVariables__c globalVar2 = new ResaleGlobalVariables__c();
        globalVar2.Name = 'IntegrationUser';
        globalVar2.value__c = 'Integration User';
        insert globalVar2;
        ResaleGlobalVariables__c globalVar3 = new ResaleGlobalVariables__c();
        globalVar3.Name = 'RIFAccountOwnerAlias';
        globalVar3.value__c = 'ADT RIF';
        insert globalVar3;
        ResaleGlobalVariables__c globalVar4 = new ResaleGlobalVariables__c();
        globalVar4.Name = 'DataConversion';
        globalVar4.value__c = 'FALSE';
        insert globalVar4;
        ResaleGlobalVariables__c globalVar5 = new ResaleGlobalVariables__c();
        globalVar5.Name = 'Alternate Pricing Model Order Types';
        globalVar5.value__c = 'N1;R1';
        insert globalVar5;
        
        ResaleGlobalVariables__c globalVar6 = new ResaleGlobalVariables__c();
        globalVar6.name = 'SimpleSellOrderTypes';
        globalVar6.value__c = 'N1;N3;R1;R2;R4';
        insert globalVar6;
        
        ProposalOnlyStates__c pos=new ProposalOnlyStates__c(name='States',states__c='AL,FL,IL,LA,MD,MA,MI,MS,SC,TN,TX,UT,VA,NV');
        insert pos;
        
        Map<String, String> EquifaxSettings = new Map<String, String>
        {'CreditCheckAttempts'=>'3'
            , 'Order Types For Credit Check'=>'N1;R1'
            , 'Order Types For Credit Check - Renters'=>'N1;R1'
            , 'Failure Risk Grade'=>'X'
            , 'Failure Condition Code'=>'APPROV'                     
            , 'Days to be considered out of date'=>'1'
            , 'Days to allow additional check'=>'1'
            , 'Default Risk Grade'=>'W'
            , 'Default Condition Code'=>'CAE1'
            , 'Employee Risk Grade'=>'U'
            , 'Employee Condition Code'=>'CME3'
            , 'Default Approved Condition Code' => 'APPROV'
            , 'Default Approved Risk Grade' => 'Q'
            , 'FlexFiApprovalRiskGrade' => 'A,B,C,X'};
                List<Equifax__c> EquifaxSettingList = new List<Equifax__c>(); 
        for( String sN: EquifaxSettings.keySet()){
            Equifax__c eS = new Equifax__c( name = sN );
            eS.value__c = EquifaxSettings.get(sN);
            EquifaxSettingList.add( eS );
        }
        insert EquifaxSettingList;
        
        list<FlexFiConfig__c> flexConfigs = new list<FlexFiConfig__c>();
        FlexFiConfig__c fc1 = new FlexFiConfig__c(value__c='test');
        fc1.Name = 'ThirdPartyCustomerNumber';
        flexConfigs.add(fc1);
        
        /*FlexFiConfig__c fc2 = new FlexFiConfig__c(value__c='test');
fc2.name='TransactionType';
flexConfigs.add(fc2);

FlexFiConfig__c fc3 = new FlexFiConfig__c(value__c='test');
fc3.name='autoBookFlg';
flexConfigs.add(fc3);*/
        
        insert flexConfigs;
        
        Equifax_Conditions__c eCon1 = new Equifax_Conditions__c();
        eCon1.Name = 'CAE1';
        eCon1.Payment_Frequency__c = 'A';
        eCon1.Deposit_Required_Percentage__c = 100.00;
        eCon1.Three_Pay_Allowed__c = true;
        eCon1.EasyPay_Required__c = true;
        insert eCon1;
        
        Equifax_Conditions__c eCon2 = new Equifax_Conditions__c();
        eCon2.Name = 'CME3';
        eCon2.Payment_Frequency__c = 'A';
        eCon2.Deposit_Required_Percentage__c = 100.00;
        eCon2.Three_Pay_Allowed__c = false;
        eCon2.EasyPay_Required__c = false;
        insert eCon2;
        
        RecordType standardAcctRecType = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account');
        Account a = new Account();
        a.RecordTypeId = standardAcctRecType.Id;
        a.Name = 'test';
        a.Business_Id__c = '1100 - Residential';
        a.Channel__c = 'Resi Direct Sales';
        a.AddressID__c = add.Id;
        a.CLV__c = '4';
        a.Churn_Value__c = 'At Risk';
        a.MMB_Past_Due_Balance__c = 2.00;
        a.MMBOrderType__c = 'R1';
        a.MMBBillingSystem__c = 'INF';
        a.MMB_Relo_Billing_System__c = 'INF';
        a.PreviousBillingCustomerNumber__c = '12345';
        a.EquifaxRiskGrade__c = 'W';
        a.EquifaxApprovalType__c = 'CAE1';
        a.Equifax_Last_Check_DateTime__c = system.now();
        a.Profile_RentOwn__c = 'O';
        a.SiteStreet3__c = 'Garage';
        a.PostalCodeID__c = pc.Id;
        a.ADTEmployee__c = true;
        insert a;
        
        ProductCatalog__c pCat = new ProductCatalog__c();
        pCat.ProductName__c = 'Product A';
        pCat.product_quantity__c = '2';
        pCat.ProductAccount__c = a.id;
        insert pCat;
        
        
        Trip_Fee__c fee = new Trip_Fee__c();
        fee.State__c = 'FL';
        fee.Fee_Type__c = '';
        fee.Minimum_Limit__c = 1.00;
        fee.Maximum_Limit__c = 100.00;
        insert fee;
        
        //getAccountRecord(a.Id);
        CPQIntegrationDataGenerator.IntegrationDataSource iSource = new CPQIntegrationDataGenerator.IntegrationDataSource();
        iSource.a = a;
        iSource.fee = fee;
        String Json = CPQIntegrationDataGenerator.getCPQIntegrationJSON( iSource );
        Address__c addr1 = new Address__c();
        addr1.Latitude__c = 43.0401814;
        addr1.Longitude__c = -87.9029031;
        addr1.Street__c = '700 East Mason Street';
        addr1.City__c = 'Milwaukee';
        addr1.State__c = 'WI';
        addr1.PostalCode__c = '53202';
        addr1.OriginalAddress__c = '700 East Mason Street, Milwaukee, WI 53202';            
        insert addr1;
        a.MMBOrderType__c = 'N1';
        a.Channel__c = 'SB Direct Sales';
        a.ADTEmployee__c = true;
        a.Equifax_Last_Check_DateTime__c = system.now()-2;
        a.AddressID__c = addr1.id;
        update a;
        
        Account a1 = getAccountRecord(a.Id);
        Permit__c p = new Permit__c();
        p.Line_of_Business__c = 'Small Business';
        p.Payee_Name__c = 'ADT';
        p.Permit_Type__c = 'Alarm';
        p.Permit_Amount__c = 10.00;
        p.Zip_Code__c = '53202';
        insert p;
        iSource = new CPQIntegrationDataGenerator.IntegrationDataSource(a1.Id);
        isource.equifaxRiskGrade = 'W';
        isource.equifaxApprovalType = 'CAE1';
        iSource.a = a1;
        iSource.fee = fee;
        Json = CPQIntegrationDataGenerator.getCPQIntegrationJSON( iSource );
        a.MMBOrderType__c = 'N1';
        // a.Channel__c = 'SB Direct Sales';
        a.ADTEmployee__c = true;
        a.Equifax_Last_Check_DateTime__c = system.now()-2;
        a.AddressID__c = addr1.id;
        update a;
        
        Account a2 = getAccountRecord(a.Id);
        Permit__c p1 = new Permit__c();
        p1.Payee_Name__c = 'ADT';
        p1.Permit_Type__c = 'Alarm';
        p1.Permit_Amount__c = 10.00;
        p1.Zip_Code__c = '53202';
        insert p1;
        iSource = new CPQIntegrationDataGenerator.IntegrationDataSource(a2.Id);
        isource.equifaxRiskGrade = 'W';
        isource.equifaxApprovalType = 'CAE1';
        iSource.a = a2;
        iSource.fee = fee;
        Json = CPQIntegrationDataGenerator.getCPQIntegrationJSON( iSource );
        
        //HRM-9627
        
        Account a3 = getAccountRecord(a.Id);
        a3.ADTEmployee__c = false;
        a3.EquifaxRiskGrade__c = 'A';
        update a3;
        
        LoanApplication__c lApp = new LoanApplication__c();
        lApp.Account__c = a3.id;
        lApp.BillingAddress__c =addr1.id;
        lApp.ShippingAddress__c =addr1.id;
        lApp.SubmitLoanTerms_ConditionAcceptedDate__c =DateTime.parse('12/05/2018 11:46 AM');
        lApp.SubmittedDateTime__c = system.now();
        lApp.CreditCardName__c ='Test Account';
        lApp.CreditCardExpirationDate__c ='12/05/2018';
        lApp.CardType__c ='AMEX';
        lApp.CreditCardNumber__c ='1234567812345678';
        lApp.PaymentechProfileId__c='101';
        lApp.DecisionStatus__c = 'APPROVED';
        insert lApp;
        
        iSource = new CPQIntegrationDataGenerator.IntegrationDataSource(a3.Id);
        iSource.a = a3;
        iSource.fee = fee;
        Json = CPQIntegrationDataGenerator.getCPQIntegrationJSON( iSource );
        
        a3 = getAccountRecord(a.Id);
        a3.ADTEmployee__c = false;
        a3.MMBOrderType__c = 'A1';
        a3.Channel__c = 'Resi Direct Sales';
        a3.EquifaxRiskGrade__c = 'W';
        a3.Profile_RentOwn__c = 'R';
        update a3;
        
        iSource = new CPQIntegrationDataGenerator.IntegrationDataSource(a3.Id);
        iSource.a = a3;
        iSource.fee = fee;
        Json = CPQIntegrationDataGenerator.getCPQIntegrationJSON( iSource );
        
        Test.startTest();
        User u = TestHelperClass.createUser(1010101);
        u.SimpleSell__c = true;
        update u;
        system.runAs(u){
            a3.MMBOrderType__c = 'N1';
            //a3.EquifaxRiskGrade__c = 'A';
            a3.Equifax_Last_Check_DateTime__c = system.now().addMonths(-4);
            update a3;
            iSource = new CPQIntegrationDataGenerator.IntegrationDataSource(a3.Id);
            iSource.a = a3;
            iSource.fee = fee;
            Json = CPQIntegrationDataGenerator.getCPQIntegrationJSON( iSource );
        }
        Test.stopTest();
    }
    
    private static Account getAccountRecord(String aId){
        // Requery account to pass the address postal code 
        // Requery account to pass the address postal code 
        acc = [SELECT Id,VIPDepositWaiver__c, Business_Id__c, Channel__c, SiteStreet3__c, ADTEmployee__c, CLV__c, Churn_Value__c, AddressID__c, AddressID__r.PostalCode__c, 
               MMBBillingSystem__c,MMBSystemType__c , ResaleTownNumber__c, MMB_Relo_Billing_System__c, MMBOrderType__c, MMB_Past_Due_Balance__c, PreviousBillingCustomerNumber__c, 
               sitestate__c, Profile_RentOwn__c, EquifaxRiskGrade__c, EquifaxApprovalType__c, Equifax_Last_Check_DateTime__c, PostalCodeID__c,PostalCodeID__r.Town_Lookup__c,
               PostalCodeID__r.Town_Lookup__r.name,MMBCustomerStartDate__c, MMBSiteStartDate__c,MMBSystemStartDate__c  FROM Account WHERE Id =: aId];
        return acc;
    }
}