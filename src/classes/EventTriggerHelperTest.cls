/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EventTriggerHelperTest {

    private static final double TIME_TOLERANCE_MS = 3000;
    
    static testMethod void testAccountOwnerChange() {
        Event e;
        Account a;
        User u1;
        User u2;
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs ( thisUser ) {
            //get test users
            u2 = TestHelperClass.createManagerUser();
            u1 = TestHelperClass.createSalesRepUser();
            
            //create test account
            a = TestHelperClass.createAccountData();
            a.OwnerId = u1.Id;
            a.Chargeback__c = 'N';
            update a;
            
            //create event
            e = TestHelperClass.createEvent(a, u1);
            e.WhatId = a.Id;
            e.Subject = 'account event';
            e.Status__c = EventManager.STATUS_SCHEDULED;
            update e;
        }
        
        //reassign account
        a.OwnerId = u2.Id;
        test.StartTest();
            update a;
        test.StopTest();
        
        Event eConfirm = [Select OwnerId, WhatId from Event where Id = :e.Id limit 1];
        Account aConfirm = [Select Id, OwnerId from Account where Id = :a.Id];
        system.assertEquals(u2.Id, aConfirm.OwnerId);
        system.assertEquals(aConfirm.Id, eConfirm.WhatId);
        //uncomment this when the event trigger helper is being called from the account trigger
        // system.assertEquals(aConfirm.OwnerId, eConfirm.OwnerId);
    }
    
    static testMethod void testUpdateEventAssignedTo()
    {
        Account a;
        Event e;
        User u1;
        User u2;
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs ( thisUser ) {
            u1 = [select Id from User where Id = :UserInfo.getUserId()];
             Profile p = [select id from profile where name='System Administrator'];
            u2 = new User(alias = 'stand7', email='standarduser7@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testing7'+'@adt.com', EmployeeNumber__c='T7' ,
            StartDateInCurrentJob__c = Date.today());
            a = TestHelperClass.createAccountData();
            a.OwnerId = u1.Id;
            update a;
            e = TestHelperClass.createEvent(a, u1);
            e.OwnerId = a.Id;
        }
        
        test.StartTest();
            e.WhatId = a.Id;
            test.stopTest();
        
        Event confirmEvent = [Select id,OwnerId from Event where Id = :e.Id];
        system.assertEquals(u1.Id, confirmEvent.OwnerId);
    }
    
    static testMethod void testSetEventRecordTypeSG()
    {
        Event e;
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
            e.IncomingType__c = EventManager.INCOMING_TYPE_SG;
        }
            
        test.startTest();
        
            insert e;
            
        test.stopTest();
        
        Event econfirm = [Select RecordTypeId from Event where Id = :e.Id];
        
        System.assertEquals(Utilities.getRecordType(RecordTypeName.SELF_GENERATED_APPOINTMENT, 'Event').Id, econfirm.RecordTypeId);
            
    }
    
    static testMethod void testSetEventRecordTypeCG() {
        Event e;
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
            e.IncomingType__c = EventManager.INCOMING_TYPE_CG;
        }
            
        test.startTest();
        
            insert e;
            
        test.stopTest();
        
        Event econfirm = [Select RecordTypeId from Event where Id = :e.Id];
        
        System.assertEquals(Utilities.getRecordType(RecordTypeName.COMPANY_GENERATED_APPOINTMENT, 'Event').Id, econfirm.RecordTypeId);
            
    }
    
    static testMethod void testShowAs() {
        Event e;
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
            e.IncomingType__c = EventManager.INCOMING_TYPE_INSTALL;
        }
        
        test.startTest();
            insert e;
        test.stopTest();
        
        Event econfirm = [Select RecordTypeId, ShowAs from Event where Id = :e.Id];
        
        system.assertEquals( EventManager.SHOW_AS_FREE, econfirm.ShowAs);
    }
    
    static testMethod void testReminder() {
        Event e;
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
            e.IncomingType__c = EventManager.INCOMING_TYPE_CG;
        }
        
        test.startTest();
            insert e;
        test.stopTest();
        
        Event econfirm = [Select isReminderSet, ReminderDateTime, StartDateTime from Event where Id = :e.Id];
        
        system.assertEquals(true, econfirm.IsReminderSet);
        
        // modify assertion for tolerance 
        double min = econfirm.StartDateTime.addMinutes(-15).getTime() - TIME_TOLERANCE_MS;
        double max = econfirm.StartDateTime.addMinutes(-15).getTime() + TIME_TOLERANCE_MS;
        System.assert( econfirm.ReminderDateTime.getTime() >= min && econfirm.ReminderDateTime.getTime() <= max);
    }
    
    static testMethod void testUpdateDevicePhone() {
        Event e;
        User u1;
        User u2;
        User current = [Select Id from User where ID = :UserInfo.getUserId()];
        system.runas(current) {
            u1 = TestHelperClass.createSalesRepUser();
            u1.DevicePhoneNumber__c = '5555555555';
            update u1;
            u2 = TestHelperClass.createManagerUser();
            u2.DevicePhoneNumber__c = '9999999999';
            update u2;
            e = TestHelperClass.createEvent(u1);
            e.Subject = 'phone device test';
            e.TaskCode__c = 'RHS';
            e.RecordTypeId = Utilities.getRecordType( RecordTypeName.COMPANY_GENERATED_APPOINTMENT, 'Event').Id;
            update e;
        }
        
        Event eConfirm = [Select DevicePhoneNumber__c from Event where Id = :e.Id];
        system.assertEquals(u1.DevicePhoneNumber__c, eConfirm.DevicePhoneNumber__c);
        
        test.startTest();
            e.OwnerId = u2.Id;
            update e;
        test.stopTest();
        
        eConfirm = [Select DevicePhoneNumber__c from Event where Id = :e.Id];
        system.assertEquals(u2.DevicePhoneNumber__c, eConfirm.DevicePhoneNumber__c);
    }
    
    static testmethod void testDataConversionProcess()
    {
        Event e;
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            a.TelemarAccountNumber__c = 'TUniq1_123';
            update a;
            e = TestHelperClass.createEvent(a, current, false);
            e.IncomingType__c = EventManager.INCOMING_TYPE_CG;
            e.IncomingConversionRecord__c = true;
            e.TelemarAccountNumber__c = 'TUniq1_123';
        }
        
        test.startTest();
            insert e;
        test.stopTest();
        
        Event econfirm = [Select isReminderSet, ReminderDateTime, StartDateTime from Event where Id = :e.Id];
        
        system.assertEquals(e.OwnerId, current.id);
        system.assertEquals(e.WhatId, a.Id);
    }  
      
}