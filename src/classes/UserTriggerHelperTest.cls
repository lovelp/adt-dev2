@isTest
private class UserTriggerHelperTest {
    
    /** DEPRECATED - Test method was related to deprecated code on user trigger
    static testMethod void TestUserTrgHlp() {
        User usrRec=TestHelperClass.createUser(1234);
       
        usrRec.Business_Unit__c='Home Health';
        update usrRec;
        usrRec.Business_Unit__c='Resi Resale';
        update usrRec;
        
    }
    */
    
    static testMethod void TestProcessUserBefore() {
        
        User SalesRepUserObj = TestHelperClass.createSalesRepUser();
        SalesRepUserObj = [ SELECT alias, email, emailencodingkey, lastname, languagelocalekey, 
                                   localesidkey, UserRoleId, EmployeeNumber__c, TimeZoneSidKey,
                                   username, StartDateInCurrentJob__c, UserRole.ParentRoleId,
                                   Id, ProfileId, Profile.Name 
                            FROM User 
                            WHERE ID = :SalesRepUserObj.Id ];

        // Changing user's time zone
        SalesRepUserObj.TimeZoneSidKey = 'America/New_York';
        update SalesRepUserObj;
        
        // Updating User's Role
        UserRole urExisting = [select id, OpportunityAccessForAccountOwner from UserRole where name like '%Rep' limit 1];
        /*UserRole urNew = new UserRole( name = 'Test Sales Rep123', 
                                       OpportunityAccessForAccountOwner = urExisting.OpportunityAccessForAccountOwner, 
                                       parentRoleId = SalesRepUserObj.UserRole.ParentRoleId );
        insert urNew;*/
        
        UserRole urNew = [Select id,Name,OpportunityAccessForAccountOwner from UserRole where Name ='NA Atlantic Resi D115 Rep' limit 1];
        
        SalesRepUserObj.UserRoleId = urNew.Id;
        update SalesRepUserObj;  
        
        // Deactivating Manager OR Changing a manager's role
        User SalesRepManager = TestHelperClass.createManagerUser();
        SalesRepManager = [SELECT alias, email, emailencodingkey, lastname, languagelocalekey, localesidkey, 
                                  profileid, EmployeeNumber__c, timezonesidkey, username, StartDateInCurrentJob__c,
                                  UserRoleId, UserRole.ParentRoleId
                           FROM User
                           WHERE Id = :SalesRepManager.Id];
                
        SalesRepManager.UserRoleId = SalesRepUserObj.UserRole.ParentRoleId;
        update SalesRepManager;  
        
    }    
    
    
    
    
}