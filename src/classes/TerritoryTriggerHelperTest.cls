@isTest
private class TerritoryTriggerHelperTest {
	
	static testMethod void testUpdateTerritoryOwnerNoTerrAssignments() {
		
		User manager1 = TestHelperClass.createManagerUser();
		User manager2 = TestHelperClass.createManagerWithTeam();
		
		List<Territory__c> terrList = new List<Territory__c>();
		Territory__c terr1;
		Territory__c terr2;
		System.runAs(manager1) {
			terr1 = new Territory__c();
			terr1.Name = 'Resi-Test';
			
			terr2 = new Territory__c();
			terr2.Name = 'Small Bus-Test';
			
			terrList.add(terr1);
			terrList.add(terr2);
			
			insert terrList;
			
		}
		
		Territory__c terr1Before = [select Name, OwnerId from Territory__c where Id = :terr1.Id];
		Territory__c terr2Before = [select Name, OwnerId from Territory__c where Id = :terr2.Id];
		
		terr1Before.OwnerId = manager2.Id;
		terr2Before.OwnerId = manager2.Id;
		
		List<Territory__c> updateList = new List<Territory__c>();
		updateList.add(terr1Before);
		updateList.add(terr2Before);
		
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		
		
		Test.startTest();
		
		System.runAs(thisUser) {
			// this update will initiate TerritoryTrigger which will call TerritoryTriggerHelper, the focus of this test
			update updateList;
		
			Territory__c terr1After = [select Name, OwnerId from Territory__c where Id = :terr1.Id];
			Territory__c terr2After = [select Name, OwnerId from Territory__c where Id = :terr2.Id];
		
			System.assertEquals(manager2.Id, terr1After.OwnerId, 'Manager 2 should own Territory 1');
			System.assertEquals(manager2.Id, terr2After.OwnerId, 'Manager 2 should own Territory 2');
		}
		
		Test.stopTest();

	}
	
	static testMethod void testUpdateTerritoryOwnerNameChangeOnly() {
		
		User manager1 = TestHelperClass.createManagerUser();
		User manager2 = TestHelperClass.createManagerWithTeam();
		
		List<Territory__c> terrList = new List<Territory__c>();
		Territory__c terr1;
		Territory__c terr2;
		System.runAs(manager1) {
			terr1 = new Territory__c();
			terr1.Name = 'Resi-Test';
			
			terrList.add(terr1);
			
			insert terrList;
			
		}
		
		Territory__c terr1Before = [select Name, OwnerId from Territory__c where Id = :terr1.Id];
		
		terr1Before.Name = 'Resi-New Name';
		
		List<Territory__c> updateList = new List<Territory__c>();
		updateList.add(terr1Before);
		
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];	
		
		Test.startTest();
		
		System.runAs(thisUser) {
			// this update will initiate TerritoryTrigger which will call TerritoryTriggerHelper, the focus of this test
			update updateList;
		
			Territory__c terr1After = [select Name, OwnerId from Territory__c where Id = :terr1.Id];
		
			System.assertEquals('Resi-New Name', terr1After.Name, 'Territory name should have been changed');
		}
		
		Test.stopTest();

	}
	

	
	static testMethod void testUpdateTerritoryOwnerTerrAssignments() {
		
		User manager1 = TestHelperClass.createManagerUser();
		User manager2 = TestHelperClass.createManagerWithTeam();
		
		List<Territory__c> terrList = new List<Territory__c>();
		Territory__c terr1;
		Territory__c terr2;
		List<TerritoryAssignment__c> taList = new List<TerritoryAssignment__c>();
		TerritoryAssignment__c ta1;
		TerritoryAssignment__c ta2;
		TerritoryAssignment__c ta3;
		System.runAs(manager1) {
			terr1 = new Territory__c();
			terr1.Name = 'Resi-Test';
			
			terr2 = new Territory__c();
			terr2.Name = 'Small Bus-Test';
			
			terrList.add(terr1);
			terrList.add(terr2);
			
			insert terrList;
			
			ta1 = new TerritoryAssignment__c();
			ta1.PostalCodeID__c = TestHelperClass.inferPostalCodeID('221o2', '1100');
			ta1.TerritoryID__c = terr1.Id;
			
			ta2 = new TerritoryAssignment__c();
			ta2.PostalCodeID__c = TestHelperClass.inferPostalCodeID('221o3', '1100');
			ta2.TerritoryID__c = terr1.Id;
			
			ta3 = new TerritoryAssignment__c();
			ta3.PostalCodeID__c = TestHelperClass.inferPostalCodeID('221o2', '1200');
			ta3.TerritoryID__c = terr1.Id;
			
			taList.add(ta1);
			taList.add(ta2);
			taList.add(ta3);
			
			insert taList;
			
		}
		
		Territory__c terr1Before = [select Name, OwnerId from Territory__c where Id = :terr1.Id];
		Territory__c terr2Before = [select Name, OwnerId from Territory__c where Id = :terr2.Id];
		
		terr1Before.OwnerId = manager2.Id;
		terr2Before.OwnerId = manager2.Id;
		
		List<Territory__c> updateList = new List<Territory__c>();
		updateList.add(terr1Before);
		updateList.add(terr2Before);
		
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		
		
		Test.startTest();
		
		System.runAs(thisUser) {
			// this update will initiate TerritoryTrigger which will call TerritoryTriggerHelper, the focus of this test
			update updateList;
		
			Territory__c terr1After = [select Name, OwnerId from Territory__c where Id = :terr1.Id];
			Territory__c terr2After = [select Name, OwnerId from Territory__c where Id = :terr2.Id];
		
			System.assertEquals(manager2.Id, terr1After.OwnerId, 'Manager 2 should own Territory 1');
			System.assertEquals(manager2.Id, terr2After.OwnerId, 'Manager 2 should own Territory 2');
			
			TerritoryAssignment__c ta1After = [select OwnerId from TerritoryAssignment__c where Id = :ta1.Id];
			TerritoryAssignment__c ta2After = [select OwnerId from TerritoryAssignment__c where Id = :ta2.Id];
			TerritoryAssignment__c ta3After = [select OwnerId from TerritoryAssignment__c where Id = :ta3.Id];
			
			System.assertEquals(manager2.Id, ta1After.OwnerId, 'Manager 2 should own Territory Assignment 1');
			System.assertEquals(manager2.Id, ta2After.OwnerId, 'Manager 2 should own Territory Assignment 2');
			System.assertEquals(manager2.Id, ta3After.OwnerId, 'Manager 2 should own Territory Assignment 3');
		}
		
		Test.stopTest();

	}
	
	//COMMENTED OUT FOR DEPLOYMENT
	static testMethod void testDeleteTerritoryAssociation() {
		
		User manager1 = TestHelperClass.createManagerUser();
		
		List<Territory__c> terrList = new List<Territory__c>();
		Territory__c terr1;
		Territory__c terr2;
		List<TerritoryAssignment__c> taList = new List<TerritoryAssignment__c>();
		TerritoryAssignment__c ta1;
		TerritoryAssignment__c ta2;
		TerritoryAssignment__c ta3;
		System.runAs(manager1) {
			terr1 = new Territory__c();
			terr1.Name = 'Resi-Test';
			
			terr2 = new Territory__c();
			terr2.Name = 'Small Bus-Test';
			
			terrList.add(terr1);
			terrList.add(terr2);
			
			insert terrList;
			
			ta1 = new TerritoryAssignment__c();
			ta1.PostalCodeID__c = TestHelperClass.inferPostalCodeID('221o2', '1100');
			ta1.TerritoryID__c = terr1.Id;
			
			ta2 = new TerritoryAssignment__c();
			ta2.PostalCodeID__c = TestHelperClass.inferPostalCodeID('221o3', '1100');
			ta2.TerritoryID__c = terr1.Id;
			
			ta3 = new TerritoryAssignment__c();
			ta3.PostalCodeID__c = TestHelperClass.inferPostalCodeID('221o2', '1200');
			ta3.TerritoryID__c = terr1.Id;
			
			taList.add(ta1);
			taList.add(ta2);
			taList.add(ta3);
			
			insert taList;
			
		}
		
		Territory__c terr1Before = [select Name, OwnerId from Territory__c where Id = :terr1.Id];
		Territory__c terr2Before = [select Name, OwnerId from Territory__c where Id = :terr2.Id];
		
		List<Territory__c> deleteList = new List<Territory__c>();
		deleteList.add(terr1Before);
		deleteList.add(terr2Before);
		
		Set<Id> deleteSet = new Set<Id>();
		deleteSet.add(terr1Before.Id);
		deleteSet.add(terr2Before.Id);
		
		String terrCountSOQL = 'select count() from Territory__c where Id in (\'' + terr1Before.Id + '\',\'' + terr2Before.Id + '\')';
		String taCountSOQL = 'select count() from TerritoryAssignment__c where TerritoryID__c in (\'' + terr1Before.Id + '\',\'' + terr2Before.Id + '\')';
		
		Test.startTest();
		
		System.runAs(manager1) {
			// this delete will initiate TerritoryTrigger which will call TerritoryTriggerHelper, the focus of this test
			delete deleteList;
		
		}
			
		Integer countOfTerritories = database.Countquery(terrCountSOQL);
		Integer countOfTerritoryAssignments = database.countQuery(taCountSOQL);
		
		System.assertEquals(0, countOfTerritories, 'Manager should own no territories');
		System.assertEquals(0, countOfTerritoryAssignments, 'Manager should own no territory assignments');
			
		Test.stopTest();

	}


	
}