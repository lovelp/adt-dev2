/************************************* MODIFICATION LOG ********************************************************************************************
* NoInstallUpdateScheduler
*
* DESCRIPTION : A schedulable class that initiates the batch class NewMoverBatch
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover            		3/12/2012			- Original Version
*
*													
*/

global class NoInstallUpdateScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		NoInstallUpdateBatch niu = new NoInstallUpdateBatch();
		niu.query += ' limit 10000';
		Database.executeBatch(niu);
	}
}