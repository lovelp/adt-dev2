public with sharing class EmailMessageUtilities
{

    public static void SendEmailNotification(Id SendTo, string Subject, string BodyText, String LinkURL, Boolean SendToManager)
    {
        User usr = [select id, email, profile.name, isActive from User where Id = : SendTo];
        Boolean isLimitedUser = ProfileHelper.isLimitedAccessUser(usr.Profile.Name);
        if (usr.id != Utilities.getGlobalUnassignedUser() && !isLimitedUser && usr.isActive)
        {
            if (ProfileHelper.isManager(usr.Profile.Name) && SendToManager)
            {
                //  SendEmailNotification(new String[] {usr.Email}, Subject, BodyText, LinkURL);
                SendEmailNotification(SendTo, Subject, BodyText, LinkURL);
            }
            else if (!ProfileHelper.isManager(usr.Profile.Name))
            {
                //  SendEmailNotification(new String[] {usr.Email}, Subject, BodyText, LinkURL);
                SendEmailNotification(SendTo, Subject, BodyText, LinkURL);
            }
        }
    }
    /*
    public static void SendEmailNotification(
        string SendTo, 
        string Subject,
        string BodyText, 
        String LinkURL
    ){
        SendEmailNotification(new String[] {SendTo}, Subject, BodyText, LinkURL);
    }
    */
    
    /**
     *  Sends an email notification to a user using a visual force email template set with an Event as the target object.
     *
     *  @param  TargetEventId       Event Id to use on this email alert
     *
     *  @param  TargetRecipientId   User we'll send this email to
     *
     */
    public static void SendAppointmentNotification(Id TargetEventId, Id TargetRecipientId){
        SendAppointmentNotification(TargetEventId, TargetRecipientId, false);
    }
    
    /**
     *  Sends an email notification to a user using a visual force email template set with an Event as the target object.
     *
     *  @param  TargetEventId       Event Id to use on this email alert
     *
     *  @param  TargetRecipientId   User we'll send this email to
     *
     *  @param  isReminder          When set it'll use a reminder template instead of the standard new appointment template
     *
     */
    public static void SendAppointmentNotification(Id TargetEventId, Id TargetRecipientId, Boolean isReminder){
        try{
            //New instance of a single email message
            Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
            String TemplateName = ( isReminder )?'NSCSalesAppointmentReminder':'NSCSalesAppointment';
            EmailTemplate et = [SELECT id FROM EmailTemplate WHERE developerName = :TemplateName];
            // Who you are sending the email to
            mail.setTargetObjectId(TargetRecipientId);
            // The email template ID used for the email
            mail.setTemplateId(et.Id);
            // What record is used to generate this email
            mail.setWhatId(TargetEventId);    
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setSenderDisplayName('National Sales Center');
            mail.setSaveAsActivity(false);  
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        catch(Exception err){
            ADTApplicationMonitor.log(err, 'EmailMessageUtilities', 'SendAppointmentNotification', ADTApplicationMonitor.CUSTOM_APP.MATRIX);
        }       
    }
    
    /**
     *  Notifies a user of a new appointment by email2SMS carrier service if this user has SMS enabled
     *
     *  @param ID       Id of the event scheduled to this user
     *
     *  @param Boolean  Flag to indicate if this message should be treated as just a confirmation of service enabled
     *
     */
    //@future
    public static void NotifyUserBySMS(Id euId, Boolean isConfirm){
        try{
            Event e;
            User u;
            String AppointmentURL;
            String subject;
            String msg;
            String StartDateTimeStr;
            String userPrefix = Schema.getGlobalDescribe().get('User').getDescribe().getKeyPrefix();
            // If the Id sent is the user's then we just need to notify this user of SMS activation
            if( !String.isBlank(euId) && ((String)euId).startsWith(userPrefix)  ){
                u = [SELECT Name, SMS_Enabled__c, MobilePhone FROM User WHERE Id = :euId];
            }
            // If the Id is not the user then defaults to an Event Id
            else {
                e = [SELECT Subject, Status__c, TaskCode__c, WhatId, WhoId, OwnerId, StartDateTime, EndDateTime,OwnerWhenCanceled__c, DurationInMinutes, Description FROM Event WHERE Id = :euId];
                // Modified for HPQC 1227
                if (e.OwnerWhenCanceled__c!=null && e.OwnerWhenCanceled__c!=''){
                    u = [SELECT SMS_Enabled__c, MobilePhone FROM User WHERE Name = :e.OwnerWhenCanceled__c AND IsActive=true];
                	System.debug('@@@User canceled'+u);
                }
                else{
                	u = [SELECT SMS_Enabled__c, MobilePhone FROM User WHERE Id = :e.OwnerId];
                    System.debug('@@@All other Users'+u);
                }
                StartDateTimeStr = e.StartDateTime.format('MMMM dd, YYYY') +' at ' +e.StartDateTime.format('h:mm a');
                System.debug('$$Event id'+e+'$UserId'+u+'Event owner iD'+e.OwnerId);
            }
            system.debug(u);
            // A user was found and this user has a Mobile phone number we can use
            if( u!= null && u.SMS_Enabled__c && !String.isBlank(u.MobilePhone) ){
                if( e!=null){
                    AppointmentURL = URL.getSalesforceBaseUrl().toExternalForm().replace('-api', '') + '/' + e.Id;
                    System.debug(u);
                }
                // Gather all enabled carrier's email2sms services as we don't know what carrier this user is in
                List<Wireless_Carrier__c> WirelessCarrierList = Wireless_Carrier__c.getAll().values();
                system.debug('carrier'+WirelessCarrierList);
                if( WirelessCarrierList != null && !WirelessCarrierList.isEmpty() ){
                    String regexNumeric = '[^a-zA-Z0-9]';
                    String mobileNumber = u.MobilePhone.replaceAll(regexNumeric, '').trim();
                    List<String> SendTo = new List<String>();
                    // Iterate every carrier and use those enabled with an email2sms email address
                    for( Wireless_Carrier__c wCarrier: WirelessCarrierList ){
                        if( wCarrier.SMS_Enabled__c && !String.isBlank(wCarrier.SMS_Email_Service__c)  ){
                            SendTo.add( wCarrier.SMS_Email_Service__c.replaceFirst('number', mobileNumber) );
                        }
                    }
                    // It is a confirmation SMS to notify the reps they'll start receiving these alerts to their phone
                    if( isConfirm ){
                        subject = 'Confirmation';
                        msg = 'This is a confirmation message to '+u.Name+' for an election made to opt IN to receive SMS for same day appointments';
                    }
                    // Not a confirmation message ask if this is a cancellation
                    else if( e!=null && e.Status__c == EventManager.STATUS_CANCELED ){
                        subject = 'Cancelled Appointment';
                        msg = 'Cancelled - '+e.TaskCode__c+' for '+StartDateTimeStr+'. Review your schedule or call ADT InTouch for appt details.';
                    }
                    // SMS Content defaults to new appointment
                    else{
                        subject = 'New Appointment';
                        msg = 'New - '+e.TaskCode__c+' for '+StartDateTimeStr+'. Review your schedule or call ADT InTouch for appt details.';
                    }
                    system.debug( SendTo );
                    // Send sms using email2sms services from each active carrier
                    SendEmailNotification( SendTo, subject, msg);
                }
            }
        }
        catch(Exception err){
            ADTApplicationMonitor.log(err, 'EmailMessageUtilities', 'NotifyUserBySMS', ADTApplicationMonitor.CUSTOM_APP.MATRIX);
            system.debug('\nError: '+err.getMessage()+' \n Trace:'+err.getStackTraceString());
        }       
    }
    
    public static void SendEmailNotification(Id SendTo, /*string[] SendTo, */ string Subject, string BodyText, String LinkURL)
    {
        if (ResaleGlobalVariables__c.getInstance('SendAppointmentEmails').Value__c.equals('true'))
        {
            try
            {
                //  Messaging.reserveSingleEmailCapacity(1);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(SendTo);
                mail.saveAsActivity = false;
                //  mail.setToAddresses(SendTo);
                mail.setSenderDisplayName('Salesforce');
                mail.setSubject(Subject);

                if (LinkURL != null)
                {
                    //  mail.setHtmlBody(BodyText + '<br />' + 'Click for details : ' + LinkURL);
                    mail.setHtmlBody(BodyText + '<br />' + 'Click for details : ' + '<a href=' + LinkURL + '>' + LinkURL + '</a>');
                }
                else
                {
                    mail.setHtmlBody(BodyText);
                }
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
            }
            catch (Exception ex)
            {
            }
        }
    }

    public static void SendEmailNotification(string[] SendTo, string Subject, string BodyText)
    {
        try
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.saveAsActivity = false;
            mail.setToAddresses(SendTo);
            mail.setSenderDisplayName('Salesforce');
            mail.setSubject(Subject);
            mail.setHtmlBody(BodyText);
            if(!Test.isRunningTest())
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});

        }
        catch (Exception ex)
        {
        }
    }
    
    public static Boolean isDelegateEmail(User u){
        try{
            return !Utilities.isEmptyOrNull(u.DelegatedEmail__c);
        }
        catch(Exception err){
            system.debug(' [EmailMessageUtilities]::[isDelegateEmail]'+u+' Exception thrown :'+err.getMessage()+' --- '+err.getStackTraceString());
            return false;
        }
    }  
    
    

}