/************************************* MODIFICATION LOG ********************************************************************************************
* CreateStreetSheetController
*
* DESCRIPTION : Supports the custom list view button Create Street Sheet.    
*               It is a Controller in the SFDC MVC architecture.
*
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*             				 10/14/2011			- Original Version
*
*													
*/

public with sharing class CreateStreetSheetController {
	
	public List<Account> selectedAccounts {get; set;}
	
	public CreateStreetSheetController(ApexPages.StandardSetController controller)
	{
		selectedAccounts = controller.getSelected();
		
	}
	
	public PageReference save() {
		
		if(selectedAccounts.size() == 0)
		{
			return new PageReference('/apex/ShowError?Error=ANS');
		}
		
		Set<String> streetSheetItemSet = new Set<String>();
		
		for (Account a: selectedAccounts) {
			streetSheetItemSet.add(a.Id);
		}
		
		String streetSheetId = null;
		try {
			streetSheetId = StreetSheetManager.create(streetSheetItemSet);
		}
		catch (Exception e) {
			system.debug('Exception creating a Street Sheet: ' + e.getMessage());
			return new PageReference('/apex/ShowError?Error=DATAERROR');
		}
		PageReference pr = new PageReference ('/'+ streetSheetId);
        pr.setRedirect(true);
        return pr; 
		
	}

}