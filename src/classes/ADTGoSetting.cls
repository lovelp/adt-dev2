/************************************* MODIFICATION LOG ********************************************************************************************
* ADTGoSetting
* 
DESCRIPTION : Returns custom setting values from Custom_Setting__c
*             It is a Controller in the SFDC MVC architecture. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Mike Tuckman    				8/15/13				- Original Version
*
*													
*/


public with sharing class ADTGoSetting {
	
	public static String getSetting (String setkey){
		
		ADTGo_Settings__c cs = ADTGo_Settings__c.getInstance(setkey);
        if (cs != null && cs.Setting__c != null && cs.Setting__c.length() > 0){
			return cs.Setting__c;
        }
        else return null;
        
	}

}