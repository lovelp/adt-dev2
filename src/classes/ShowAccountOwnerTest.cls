/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ShowAccountOwnerTest {

    static testMethod void myUnitTest() {
    	User u = TestHelperClass.createManagerUser();

        Account a = new Account();
        a.Name = 'Test';
        a.OwnerId = u.Id;
        a.Business_Id__c = '1100 - Residential';
        a.Data_Source__c = 'BUDCO';
        a.ShippingStreet = '2251 Pimmit Drive';
		a.ShippingCity = 'Falls Church';		
		a.ShippingState = 'VA';
		a.ShippingPostalCode = '221o2';
		a.ShippingCountry = 'US';
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account').Id;
		a.TelemarAccountNumber__c = 'AKA1100211';
        insert a;
        
        String globalAdminName = ResaleGlobalVariables__c.getinstance('GlobalResaleAdmin').value__c;
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        test.setCurrentPageReference(Page.ShowAccountOwner);
        test.startTest();
        	ShowAccountOwner sao = new ShowAccountOwner(sc);
        test.stopTest();
        
        
        //system.assertEquals(globalAdminName, sao.OwnerName);
    }
}