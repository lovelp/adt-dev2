global class DialerListBatchProcessor implements Database.Batchable<sObject>, Database.Stateful,Database.AllowsCallouts {
    
    global String campaignjobId;
    global String campaignjobname;
    global String SOQL;
    global Campaign camp;
    global Boolean calloutSuccess;
    global String fieldsToAdd = '';
    global Map<String,String> commentMapping = new Map<String,String>();
    global DialerListBatchProcessor(String campaignjobId){
         this.campaignjobId=campaignjobId;
         calloutSuccess=true;
         camp=[select id,Email_Notification_user__c,ownerid,status,name from campaign where id=:campaignJobId];
    }
    

    //Dialer Gap HRM-7045
  public Map<String,String> getCommentMaping(String commentJsonString){
    try{
      List<Object> results = (List<Object>)JSON.deserializeUntyped(commentJsonString);
          Map<String,String> commentMap = new Map<String,String>();
          for (Object obj : results) {
              Map<String, Object> objMap = (Map<String, Object>)obj;
              String key = '';
              String value = '';
              for(String str : objMap.keySet()){
                  System.debug('string is'+str+'value is'+objMap.get(str));
                  if(str.equalsIgnoreCase('name')){
                      key = string.ValueOf(objMap.get(str));
                  }
                  else if(str.equalsIgnoreCase('value')){
                      value = String.valueof(objMap.get(str));
                  }            
              }
              commentMap.put(key,value);
          }        
          return commentMap;
    }
    catch(exception e){
      system.debug('error while deserializing comment json string in the batch class');
      return null;
    }
  
  }

    global Database.QueryLocator start( Database.Batchablecontext bc ) {
        
      System.debug('### campaign job id from the batch is'+campaignjobId);
        List<Campaign> cmpList = [SELECT Dialer_List_Output_Control__c,Dialer_List_Output_Control__r.CommentFieldMapping__c FROM Campaign WHERE Id=:campaignjobId LIMIT 1];
        if(String.isNotBlank(cmpList[0].Dialer_List_Output_Control__r.CommentFieldMapping__c)){
          commentMapping = getCommentMaping(cmpList[0].Dialer_List_Output_Control__r.CommentFieldMapping__c); 
        }
        if(!commentMapping.isEmpty()){
          for(String strKey : commentMapping.keySet()){
              if(!commentMapping.get(strKey).equalsIgnoreCase('channel__c')){
                  fieldsToAdd += 'account__r.'+commentMapping.get(strKey)+',';
              }           
          }
          System.debug('### fields to add to the batch is'+fieldsToAdd);
          SOQL='select id,Account__c,campaign.DNIS__r.name,campaign.parent.name,campaign.name,Contact.name,CampaignId,';
          SOQL +=fieldsToAdd;
          SOQL += 'phone,account__r.PhoneNumber3__c,account__r.TelemarAccountNumber__c,account__r.lastname__c,account__r.firstname__c,account__r.siteStreet__c,account__r.siteStreet2__c,account__r.SiteState__c,account__r.sitecity__c,account__r.siteCountrycode__c,account__r.sitepostalcode__c,account__r.createddate,account__r.channel__c FROM CampaignMember WHERE CampaignId=:campaignjobId';
        }
        else{
            SOQL='select id,Account__c,campaign.DNIS__r.name,campaign.parent.name,campaign.name,Contact.name,CampaignId,phone,account__r.PhoneNumber3__c,account__r.TelemarAccountNumber__c,account__r.lastname__c,account__r.firstname__c,account__r.siteStreet__c,account__r.siteStreet2__c,account__r.SiteState__c,account__r.sitecity__c,account__r.siteCountrycode__c,account__r.sitepostalcode__c,account__r.channel__c,account__r.createddate,account__r.ResaleTown__c FROM CampaignMember WHERE CampaignId=:campaignjobId';
        }
        System.debug('### SOQL build from the start batch class is'+SOQL);
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        System.debug('### callout value'+callOutSuccess);
        if(calloutSuccess){
            List<CampaignMember> campList=(List<CampaignMember>)scope;
            asyncApexJob asj=[select id,TotalJobItems,JobItemsProcessed from asyncApexJob where id=:bc.getjobid()];
            system.debug('total job items-'+asj.TotalJobItems);
            system.debug('job items processed-'+asj.jobitemsprocessed);
            DialerListAPI dla=new DialerListAPI();
            String action='Continue';
            if(asj.jobitemsprocessed+1==asj.TotalJobItems){
                action='STOP';
            }
            
             integer retryCount=0;
             Map<Boolean,String> resultMap;
             String errorMessage='';
                do{
                    
                    try{
                        //make call out to broker
                        resultMap=dla.sendEnvelope(dla.createDialerListEnvelope(campaignJobId,asj.jobitemsprocessed+1,asj.TotalJobItems,campList,action));
                        for(Boolean b:resultMap.keyset()){
                            System.debug('### result after callout is'+b);
                            callOutSuccess=b;
                            errorMessage=resultMap.get(b);
                        }
                        if(!calloutSuccess){
                            retryCount++;
                            }
                        //callout end
                    }catch(exception ex){
                        callOutSuccess=false;
                        System.debug('### exception from batch'+ex);
                        errorMessage=String.valueOf(ex);
                        retryCount++;
                    }
                }while(!callOutSuccess && retryCount<3);
                
                if(!calloutSuccess){
                    //send an email and 
                    String notificationUserId;
                    
                    if(String.isNotBlank(camp.Email_Notification_User__c))
                        notificationUserId = camp.Email_Notification_User__c;
                    else
                        notificationUserId = camp.OwnerId;
                    CampaignDefaults__c cd = CampaignDefaults__c.getorgDefaults();
                    
                    List<String> targetToAddresses=new List<String>();
                    if(cd.ErrorEmailRecepients__c!=null && !String.isBlank(cd.ErrorEmailRecepients__c))
                        targetToAddresses=cd.ErrorEmailRecepients__c.split(';');
                    
                    targetToAddresses.add(notificationUserId);
                    
                    
                    Messaging.singleEmailmessage Email = new Messaging.singleEmailmessage();
                    email.setsubject('Campaign - '+ camp.name + ': Push to dialer failed');
                    String body='An unexpected error occured while sending the extracted list to dialer: '+errorMessage;
                    body+='\nPlease try to manually Push to Dialer. If the issue is not resolved contact the System Administrator.';
                    email.setPlainTextbody(body);
                     
                    //email.setTargetObjectId(notificationUserId);
                    email.setToAddresses(targetToAddresses);
                    email.saveAsActivity = false;
                    Messaging.sendemailResult[] r = Messaging.sendemail(new Messaging.singleEmailmessage[]{email});
                    
                    try{
                    camp.status='Send Failed';
                    update camp;
                    }catch(exception ex){
                        System.debug('### exception'+ex);
                    }
                    
                    //abort the job as its not needed to loop through rest of the data set
                    System.abortJob(bc.getJobId());
                }
                
        }
    }
    
    global void finish(Database.BatchableContext bc) {       
        //send an email
        String notificationUserId;
                    
        if(String.isNotBlank(camp.Email_Notification_User__c))
            notificationUserId = camp.Email_Notification_User__c;
        else
            notificationUserId = camp.OwnerId;
        
        Messaging.singleEmailmessage Email = new Messaging.singleEmailmessage();
        email.setsubject('Campaign - '+ camp.name + ': Push to dialer complete');
        email.setPlainTextbody('All the campaign members were successfully sent to dialer');
        email.setTargetObjectId(notificationUserId);
        email.saveAsActivity = false;
        Messaging.sendemailResult[] r = Messaging.sendemail(new Messaging.singleEmailmessage[]{email});
        camp.status='Sent to dialer';
        update camp;
    }   
    
}