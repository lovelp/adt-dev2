/************************************* MODIFICATION LOG ********************************************************************************************
* MyRoleHierarchyController
*
* DESCRIPTION : Intended for visualization of UserRole hierarchy 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera			1/20/2015			- Original Version
*													
*/

public class MyRoleHierarchyController {

	public String userOrRoleId { get; set; }
	
	public String userName {
		get{
			return UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
		}
	}
	
	public MyRoleHierarchyController() {
		userOrRoleId = Apexpages.currentPage().getParameters().get('userOrRoleId');
	}
	
	public String getJSONString()  {
    	String JsonData = '[]';    	
		JsonData = RoleUtils.getTreeJSON( (Utilities.isEmptyOrNull(userOrRoleId))?UserInfo.getUserId():userOrRoleId, -1);
        return JsonData;
    }
    
}