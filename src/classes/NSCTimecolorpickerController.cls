public without sharing class NSCTimecolorpickerController {

    public string newcolorvalue{get;set;}
    public string newcolorvalueid{get;set;}
    public List<NSCTimecolorpicker__c> listA{get;set;}
    public string mytimevalue{get;set;}
    public string colorvalue{get;set;}
    public String getParam(String name) {
        return ApexPages.currentPage().getParameters().get(name);  
    }
    public String newValue{get;set;}
    public String clickedValue{get;set;}
    public List<Timewrapper> timelist{get;set;}
    
    /** Constructor **/
    public NSCTimecolorpickerController(){
        mytimevalue='';
        colorvalue='';
        //listA = NSCTimecolorpicker__c.getAll().values();
        listA = [SELECT name,Color__c FROM NSCTimecolorpicker__c ORDER BY name];
        timelist=new List<timewrapper>();
        for(NSCTimecolorpicker__c tcp:listA){
            timeList.add(new Timewrapper(tcp));
            timeList.sort();
        }
        
    }
    
    /** New time and color save **/
    public pagereference save(){
        NSCTimecolorpicker__c settings = new NSCTimecolorpicker__c(Name=mytimevalue, Color__c=colorvalue);
        try{
            insert settings;
            listA.add(settings);
            timeList.add(new Timewrapper(settings));
            timeList.sort();
        }catch(DmlException e){ 
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Enter a new Time value and resave.'));
              // ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to reset relocation choice.' + e.getMessage()));
            return null;
        }
        
        return null;
    }
    
    /** Existing time and color save **/
    public pagereference savenew(){
    try{
        NSCTimecolorpicker__c updatecolorpickerrecord=[SELECT Id,Name FROM NSCTimecolorpicker__c WHERE ID=:clickedValue];
        updatecolorpickerrecord.name=newValue;
        Update updatecolorpickerrecord;
    }catch(Exception e){}
        listA = [SELECT name,Color__c FROM NSCTimecolorpicker__c ORDER BY name];
        timeList.clear();
        for(NSCTimecolorpicker__c tcp:listA){
            timeList.add(new Timewrapper(tcp));
            timeList.sort();
        }
        return null;
    }
 
    public pagereference updatecolor(){
    try{
        system.debug('value of new color'+newcolorvalue+'new color value id is '+newcolorvalueid);
        NSCTimecolorpicker__c upcolorpickerrecord=[SELECT Id,Color__c FROM NSCTimecolorpicker__c WHERE ID=:newcolorvalueid];
        upcolorpickerrecord.Color__c=newcolorvalue;
        Update upcolorpickerrecord;
    }catch(Exception e){}
        listA = [SELECT name,Color__c FROM NSCTimecolorpicker__c ORDER BY name];
        timeList.clear();
        for(NSCTimecolorpicker__c tcp:listA){
            timeList.add(new Timewrapper(tcp));
            timeList.sort();
        }
        return null;
    }
    
    public pagereference del(){
    try{
        String delid = getParam('delid');
        NSCTimecolorpicker__c delcolorpickerrecord=[SELECT Id FROM NSCTimecolorpicker__c WHERE ID=:delid];
        DELETE delcolorpickerrecord;
    }catch(Exception e){}
        listA = [SELECT name,Color__c FROM NSCTimecolorpicker__c ORDER BY name];
        timeList.clear();
        for(NSCTimecolorpicker__c tcp:listA){
            timeList.add(new Timewrapper(tcp));
            timeList.sort();
        }
        return null;
    } 
    
    public class Timewrapper implements Comparable {
        public Integer name;
        public NSCTimecolorpicker__c tcp{get;set;}
        public Timewrapper(NSCTimecolorpicker__c cp){
            tcp=cp;
            name=Integer.valueof(cp.name);
        }
        public Integer compareTo(Object compareTo) {
            Timewrapper Timewrap = (Timewrapper)compareTo;
            if (name == Integer.valueof(Timewrap.tcp.name)) return 0;
            if (name > Integer.valueof(Timewrap.tcp.name)) return 1;
            return -1;       
        }
    }
    

}