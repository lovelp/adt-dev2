/************************************* MODIFICATION LOG ********************************************************************************************
* AccountAffiliationTriggerHelper
*
* DESCRIPTION : Adding Builder value to Extra Skills on Account on Insert
*              - Remove Builder value from Extra Skills on Account on Update and on delete
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE        TICKET        REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Kalpesh Bhirud        4/17/2018      HRM - 6681    - Added Builder leads for appointments
* 
*/
public class AccountAffiliationTriggerHelper {
    
    public void OnAfterInsert(Account_Affiliation__c[] newAccountAffiliations){
          // EXECUTE AFTER INSERT LOGIC
          processAccountUpdates(newAccountAffiliations);
    }
    
    public void OnAfterUpdate(Account_Affiliation__c[] oldAccountAffiliations, Account_Affiliation__c[] updatedAccountAffiliations, Map<ID, Account_Affiliation__c> newMap, Map<ID, Account_Affiliation__c> oldMap) {
        // EXECUTE AFTER UPDATE LOGIC
        // processAccountUpdates(updatedAccountAffiliations);
        processAccountAfterUpdates(oldAccountAffiliations,updatedAccountAffiliations,newMap,oldMap);
    }
    
    public void onAfterDelete(Account_Affiliation__c[] deletedAccountAffiliations, Map<ID, Account_Affiliation__c> oldMap){
        // EXECUTE AFTER DELETE LOGIC
        processAccountAfterDelete(deletedAccountAffiliations,oldMap);
    }
    
    public void processAccountUpdates(Account_Affiliation__c[] accountAffiliations){
        
        set<Id> accountIds = new set<Id>();
        set<Id> affiliateIds = new set<Id>();
        List<Account> AccountsToUpdate = new List<Account>();
        
        for(Account_Affiliation__c accAff : accountAffiliations){
            accountIds.add(accAff.Member__c);
            affiliateIds.add(accAff.Affiliate__c);
        }
        
        Map<Id, Account> AccountMap = new Map<Id, Account>([Select Id, ExtraSkills__c From Account Where Id IN:accountIds]);
        
        Map<Id, Account_Affiliation__c> AffiliationMap = new Map<Id, Account_Affiliation__c>();
        
        for(Account_Affiliation__c accAff:[Select Id, Member__c, Affiliate__r.Builder__c From Account_Affiliation__c Where Affiliate__r.Builder__c = true AND Affiliate__r.Active__c = true AND Affiliate__c IN:affiliateIds]){
            AffiliationMap.put(accAff.Member__c,accAff);
        }
        system.debug('AffiliationMap--->'+AffiliationMap);
        
        for(Account acc : AccountMap.values()){
           
            Boolean checkflag = false;
            Account_Affiliation__c accAff = new Account_Affiliation__c();
            accAff = AffiliationMap.get(acc.Id);
            system.debug('accAff Value'+accAff);
            
            if(accAff != null){
                if(acc.ExtraSkills__c == NULL){
                    acc.ExtraSkills__c = Channels.TERRITORYTYPE_BUILDER;
                    checkflag = true;
                }else if(!(acc.ExtraSkills__c.contains(Channels.TERRITORYTYPE_BUILDER))){
                    acc.ExtraSkills__c += ';'+ Channels.TERRITORYTYPE_BUILDER;
                    checkflag = true;
                }
            }
             
            if(checkflag){
                AccountsToUpdate.add(acc);
            }
            
        }
        system.debug('AccountsToUpdate--->'+AccountsToUpdate);
        if(!AccountsToUpdate.isEmpty()){
            database.update(AccountsToUpdate,false);
        }
         
    }
    
    
    public void processAccountAfterUpdates(Account_Affiliation__c[] oldAccountAffiliations, Account_Affiliation__c[] updatedAccountAffiliations, Map<ID, Account_Affiliation__c> newMap, Map<ID, Account_Affiliation__c> oldMap){
    
        set<ID> accountIds = new set<ID>();
        set<ID> affiliateIds = new set<ID>();
        
        for(Account_Affiliation__c accAff : updatedAccountAffiliations){
            accountIds.add(accAff.Member__c);
            affiliateIds.add(accAff.Affiliate__c);
        }
        
        Map<Id, Account_Affiliation__c> AccountAffMap = new Map<Id, Account_Affiliation__c>([Select Id, Builder__c, Active__c, MemberNumber__c From Account_Affiliation__c Where Member__c IN:accountIds AND Builder__c = true AND Active__c = true]);
        Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id, ExtraSkills__c FROM Account WHERE ID IN:accountIds]);
        List<Account> accountToUpdate = new List<Account>();
        
        if(AccountAffMap.isEmpty()){
            if(!accountMap.isEmpty()){
                for(Id accId : accountMap.keySet()){
                    Account acc = accountMap.get(accId);
                    if(String.isNotBlank(acc.ExtraSkills__c) && acc.ExtraSkills__c.contains(Channels.TERRITORYTYPE_BUILDER)){
                        String str = acc.ExtraSkills__c;
                        acc.ExtraSkills__c= str.replaceAll(';?'+Channels.TERRITORYTYPE_BUILDER+';?','');
                        accountToUpdate.add(acc);
                    }
                }
            }
        } else {
            if(!accountMap.isEmpty()){
                for(Id accId : accountMap.keySet()){
                    Account acc = accountMap.get(accId);
                    if(String.isBlank(acc.ExtraSkills__c)){
                    	acc.ExtraSkills__c = Channels.TERRITORYTYPE_BUILDER;
                      	accountToUpdate.add(acc);
                	}else if(!(acc.ExtraSkills__c.contains(Channels.TERRITORYTYPE_BUILDER))){
                    		acc.ExtraSkills__c += ';'+ Channels.TERRITORYTYPE_BUILDER;
                    		accountToUpdate.add(acc);
                    	}
                }
            }
        }
        
        if(!accountToUpdate.isEmpty()){
            update accountToUpdate;
        }
            
        
        
    }
    
    /*
    public void processAccountAfterUpdates(Account_Affiliation__c[] oldAccountAffiliations, Account_Affiliation__c[] updatedAccountAffiliations, Map<ID, Account_Affiliation__c> newMap, Map<ID, Account_Affiliation__c> oldMap){
        
        Map<Id,Account_Affiliation__c> affVsAffiliationMap = new Map<Id,Account_Affiliation__c>();
        Map<Id,Account_Affiliation__c> accVsAffiliationMap = new Map<Id,Account_Affiliation__c>();
        
        for(Account_Affiliation__c accAff : updatedAccountAffiliations){
            if((accAff.Active__c != oldMap.get(accAff.Id).Active__c && !accAff.Active__c) || !accAff.Builder__c){
                affVsAffiliationMap.put(accAff.Affiliate__c,accAff);
                accVsAffiliationMap.put(accAff.Member__c,accAff);
            }
        }
        
        if(!affVsAffiliationMap.isEmpty()){
            Map<Id,Affiliate__c> affilateMap = new Map<Id,Affiliate__c>([SELECT Id,Builder__c FROM Affiliate__c WHERE Id IN:affVsAffiliationMap.keySet()]);
            if(!affilateMap.isEmpty()){
                Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id,ExtraSkills__c FROM Account WHERE ID IN:accVsAffiliationMap.keySet()]);
                if(!accountMap.isEmpty()){
                    List<Account> accountToUpdate = new List<Account>();
                    for(Id accId : accountMap.keySet()){
                        Account acc = accountMap.get(accId);
                        if(String.isNotBlank(acc.ExtraSkills__c) && acc.ExtraSkills__c.contains(Channels.TERRITORYTYPE_BUILDER)){
                            String str = acc.ExtraSkills__c;
                            acc.ExtraSkills__c= str.replaceAll(';?'+Channels.TERRITORYTYPE_BUILDER+';?','');
                            accountToUpdate.add(acc);
                        }
                    }
                    
                    if(!accountToUpdate.isEmpty()){
                        update accountToUpdate;
                    }
                    
                }
            }
        }
        
    }
    */
    
    public void processAccountAfterDelete(Account_Affiliation__c[] deletedAccountAffiliations, Map<ID, Account_Affiliation__c> oldMap){
        
        set<ID> accountIds = new set<ID>();
        set<ID> affiliateIds = new set<ID>();
        
        for(Account_Affiliation__c accAff : deletedAccountAffiliations){
            accountIds.add(accAff.Member__c);
            affiliateIds.add(accAff.Affiliate__c);
        }
        
        Map<Id, Account_Affiliation__c> AccountAffMap = new Map<Id, Account_Affiliation__c>([Select Id, Builder__c, Active__c, MemberNumber__c From Account_Affiliation__c Where Member__c IN:accountIds AND Builder__c = true AND Active__c = true]);
        Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id, ExtraSkills__c FROM Account WHERE ID IN:accountIds]);
        List<Account> accountToUpdate = new List<Account>();
        
        if(AccountAffMap.isEmpty()){
            if(!accountMap.isEmpty()){
                for(Id accId : accountMap.keySet()){
                    Account acc = accountMap.get(accId);
                    if(String.isNotBlank(acc.ExtraSkills__c) && acc.ExtraSkills__c.contains(Channels.TERRITORYTYPE_BUILDER)){
                        String str = acc.ExtraSkills__c;
                        acc.ExtraSkills__c= str.replaceAll(';?'+Channels.TERRITORYTYPE_BUILDER+';?','');
                        accountToUpdate.add(acc);
                    }
                }
            }
        } else {
            if(!accountMap.isEmpty()){
                for(Id accId : accountMap.keySet()){
                    Account acc = accountMap.get(accId);
                    if(String.isBlank(acc.ExtraSkills__c)){
                    	acc.ExtraSkills__c = Channels.TERRITORYTYPE_BUILDER;
                      	accountToUpdate.add(acc);
                	}else if(!(acc.ExtraSkills__c.contains(Channels.TERRITORYTYPE_BUILDER))){
                    		acc.ExtraSkills__c += ';'+ Channels.TERRITORYTYPE_BUILDER;
                    		accountToUpdate.add(acc);
                    	}
                }
            }
        }
        
        if(!accountToUpdate.isEmpty()){
            update accountToUpdate;
        }
        
        
    }
    
    
    /*
    public void processAccountAfterDelete(Account_Affiliation__c[] deletedAccountAffiliations, Map<ID, Account_Affiliation__c> oldMap){
        Map<Id,Account_Affiliation__c> affVsAffiliationMap = new Map<Id,Account_Affiliation__c>(); 
        Map<Id,Account_Affiliation__c> accVsAffiliationMap = new Map<Id,Account_Affiliation__c>();
        
        for(Account_Affiliation__c accAff : deletedAccountAffiliations){
            affVsAffiliationMap.put(accAff.Affiliate__c,accAff);
            accVsAffiliationMap.put(accAff.Member__c,accAff);
        }
        
         if(!affVsAffiliationMap.isEmpty()){
            Map<Id,Affiliate__c> affilateMap = new Map<Id,Affiliate__c>([SELECT Id,Builder__c FROM Affiliate__c WHERE Id IN:affVsAffiliationMap.keySet() and Builder__c=TRUE]);
            if(!affilateMap.isEmpty()){
                Set<Id> accIds = new Set<Id>();
                for(Id affId : affilateMap.keySet()){
                    if(affVsAffiliationMap.containsKey(affId)){
                        accIds.add(affVsAffiliationMap.get(affId).Member__c);
                    }
                }
                if(!accIds.isEmpty()){
                    Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id,ExtraSkills__c FROM Account WHERE ID IN:accIds]);
					Map<Id, Account_Affiliation__c> AccountAffMap = new Map<Id, Account_Affiliation__c>([Select Id, Builder__c, Active__c, MemberNumber__c From Account_Affiliation__c Where Member__c IN:accountIds AND Builder__c = true AND Active__c = true]);
        
                    if(!accountMap.isEmpty() && AccountAffMap.isEmpty()){
                        List<Account> accountToUpdate = new List<Account>();
                        for(Id accId : accountMap.keySet()){
                            Account acc = accountMap.get(accId);
                            if(String.isNotBlank(acc.ExtraSkills__c) && acc.ExtraSkills__c.contains(Channels.TERRITORYTYPE_BUILDER)){
                                String str = acc.ExtraSkills__c;
                                acc.ExtraSkills__c= str.replaceAll(';?'+Channels.TERRITORYTYPE_BUILDER+';?','');
                                accountToUpdate.add(acc);
                              
                            }
                        }
                        
                        if(!accountToUpdate.isEmpty()){
                            update accountToUpdate;
                        }
                    }
                }
                
            }
         }
    
    }
	*/

}