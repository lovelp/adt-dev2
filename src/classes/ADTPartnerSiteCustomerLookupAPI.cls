/************************************* MODIFICATION LOG ************************************************************
* ADTPartnerSiteCustomerLookupAPI
*
* DESCRIPTION : Defines web services to enable SFDC to Partner integration
*               Rest API webservice
*-------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          TICKET         REASON
*-------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan &    09/01/2017                   - Original Version
* Shiva Pochamalla
* Siddarth Asokan      09/20/2017                   - API split into Account Lookup & Site Customer Lookup API
* Jitendra Kothari     04/12/2019   HRM-9466        - 3G Cell change or Panel Change notification
* Jitendra Kothari     04/29/2019   HRM-9547        - FlexFi National: Redventures with FlexFi
*/

@RestResource(urlMapping='/siteCustomerLookup/*')
global class ADTPartnerSiteCustomerLookupAPI {    
    @HttpPost
    global static void doPost() {
        system.debug('***** time when hit by API *******'+datetime.now());
        String jsonCustRequest = RestContext.request.requestBody.toString();
        System.debug('### rquest for json is'+jsonCustRequest);
        RestResponse res = RestContext.response;
        boolean isRequiredFlag = true;
        Set<String> requiredFieldsStringSet = new Set<String>{'partnerId','partnerRepName','callId'};
        ADTPartnerCustomerLookupSchema.CustomerLookupRequest custReq;
        ADTPartnerCustomerLookupSchema.CustomerLookupResponse clr = new ADTPartnerCustomerLookupSchema.CustomerLookupResponse();
        
        try{
            custReq = (ADTPartnerCustomerLookupSchema.CustomerLookupRequest)JSON.deserializeStrict(jsonCustRequest,ADTPartnerCustomerLookupSchema.CustomerLookupRequest.class);       
            system.debug('Cust Req'+custReq+'req params..'+RestContext.request.params);
        }catch(Exception e){
            system.debug('Exception on deserializeStrict: '+e);
            isRequiredFlag = false;
            res.statusCode = 400;
            clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
        }
        if(isRequiredFlag){
            Map<String, Object> resMap = (Map<String, Object>)JSON.deserializeUntyped(jsonCustRequest);
            System.debug('### map for request is'+resMap);
            if(resMap.size() > 0){
                //check for invalid request
                for(String k : requiredFieldsStringSet){
                    if(!resMap.containsKey(k)){
                        isRequiredFlag = false;
                        res.statusCode = 400;
                        clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                    }
                }
            }
            else {
                isRequiredFlag = false;
                res.statusCode = 400;
                clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
            }
        }

        if(isRequiredFlag){
            list<String> allowedFeatures = new list<String>();
            for (PartnerConfiguration__c pConfig : [SELECT SiteCustomerLookupAPI__c FROM PartnerConfiguration__c WHERE PartnerID__c =:custReq.partnerID AND SiteCustomerLookupAPI__c != null limit 1]){
                // Get the allowed features based on the partner Id
                allowedFeatures.addAll(pConfig.SiteCustomerLookupAPI__c.split(';'));
            }
            if(allowedFeatures.size() > 0){
                ADTPartnerAPIController controller = new ADTPartnerAPIController();
                list<Opportunity> oppList = new list<Opportunity>();
                list<Call_Data__c> callDataList = new List<Call_Data__c>();
                callDataList = [SELECT id,Account__c FROM Call_Data__c WHERE id =: custReq.callID limit 1];
                oppList = [SELECT id, name, AccountId FROM Opportunity WHERE id =: custReq.opportunityID limit 1];
                if((oppList.size()>0 || String.isBlank(custReq.opportunityID) || custReq.opportunityId == null) && callDataList.size() > 0){
                    // 1. Site & Customer
                    if(allowedFeatures.contains('MMB Lookup')){
                        try{
                            MMBCustomerSitePartner MMBPartnerController = new MMBCustomerSitePartner();
                            clr.sites = MMBPartnerController.getCustomerSites(custReq);
                        }catch(Exception e){
                            System.debug('Something went wrong in getting customer sites: '+e.getStackTraceString()+' Message: '+e.getMessage());
                            isRequiredFlag = false;
                            if(e.getMessage() == '500'){
                                res.statusCode =  Integer.ValueOf(e.getMessage());
                                clr.message = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
                                clr.sites = null;
                            }
                        }
                    }
                    //2. Opportunity Id
                    if(allowedFeatures.contains('Opportunity Id') && isRequiredFlag){
                        try{
                            clr.opportunityID = controller.getOpportunityId(custReq);
                        }catch(Exception e){
                            isRequiredFlag = false;
                            ADTApplicationMonitor.log(e, 'ADTPartnerSiteCustomerLookupApi', 'getOpportunityId', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                            System.debug('Something went wrong in getting opportunity ID: '+e.getStackTraceString()+' Message: '+e.getMessage());
                        }
                        //3. Credit rating
                        if(allowedFeatures.contains('Credit Rating') && isRequiredFlag){
                            try{
                                clr.creditRating = controller.getcreditRating();
                            }catch(Exception e){
                                isRequiredFlag = false;
                                ADTApplicationMonitor.log(e, 'ADTPartnerSiteCustomerLookupApi', 'getcreditRating', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                                System.debug('Something went wrong in getting credit rating: '+e.getStackTraceString()+' Message: '+e.getMessage());
                            }
                        }
                        //4. Serviceability
                        if(allowedFeatures.contains('Serviceability') && isRequiredFlag){
                            try{
                                clr.orderServiceability = controller.makeServiceabilityCall();
                                clr.leadManagementId = controller.getleadManagementId();
                            }catch(Exception e){
                                isRequiredFlag = false;
                                ADTApplicationMonitor.log(e, 'ADTPartnerSiteCustomerLookupApi', 'makeServiceabilityCall', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                                System.debug('Something went wrong in getting order serviceability: '+e.getStackTraceString()+' Message: '+e.getMessage());
                            }
                        }
                        //5. Loan Application for Site Look API
                        if(allowedFeatures.contains('Loan Application') && isRequiredFlag){
                            String accId = null;
                            if(!oppList.isEmpty() && oppList.get(0).AccountId != Null){
                                accId = oppList.get(0).AccountId;
                            }
                            if(accId == null && !callDataList.isEmpty() && callDataList.get(0).Account__c != null){
                                accId = callDataList.get(0).Account__c;
                            }
                            try{
                                if(String.isNotBlank(accId)){
                                    clr.loanApplication = ADTPartnerLoanAppAPIHelper.getLoanApplication(null, accId);
                                }
                            }catch(Exception e){
                                ADTApplicationMonitor.log(e, 'ADTPartnerSiteCustomerLookupApi', 'GetLoanApplicaitonDetails', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                            }
                        }
                    }
                    
                    // Internal Exception Error
                    if(!isRequiredFlag){
                        res.statusCode =  500;
                        clr.message = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
                        clr.sites = null;
                        clr.creditRating = null;
                        clr.orderServiceability =null;
                        clr.opportunityID = null;
                    }
                }else{
                    res.statusCode = 404;
                    clr.message = PartnerAPIMessaging__c.getinstance('404').Error_Message__c.replace('requestID','CallID or Opportunity ID');
                }
            }else{
                // No Values in feature set
                res.statusCode =  500;
                clr.message = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
                clr.sites = null;
                clr.creditRating = null;
                clr.orderServiceability = null;
                clr.opportunityID = null;
                System.debug('Site Lookup Feature set is empty!');
            }
        }
        String custResponse = JSON.serialize(clr,true);
        system.debug('JSON Resp to RV:'+custResponse);
                
        res.addHeader('Content-Type', 'application/json');
        res.responseBody = Blob.valueOf(custResponse);
        
        system.debug('***** time when done on API *******'+datetime.now());
    }
}