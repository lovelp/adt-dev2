@isTest
public class UpdateOpportunityAgreementAPITest {
    public static Opportunity opp;
    public static Account acc;
    public static AuditLog__c aLog;
    @isTest
    static void testADTEBRAPI(){
        createTestData();
        Test.setMock(HttpCalloutMock.class, new UpdateOpportunityAgreementCallout());
        
        
        SalesAgreementSchema.SalesAgreementLookupResponse reqst=new SalesAgreementSchema.SalesAgreementLookupResponse();
        reqst.sfdcOpportunityID=opp.id;
        
        String JsonMsg=JSON.serialize(reqst);
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/updateOpportunity/*'; //Request URL
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        
        ADTEBRAPI.sendToEBR(acc.Id,aLog.Id);
        list<AuditLog__c> aloglist = new list<AuditLog__c>();
        aloglist.add(aLog);
        list<Account> accnlist = new list<Account>();
        accnlist.add(acc);
        ADTEBRAPI.sendToEBR(aloglist,accnlist);
        
        Test.starttest();
        UpdateOpportunityAgreementAPI.serAllErrors('test',200,opp.Id);
        UpdateOpportunityAgreementAPI.createSuccess('test12');
        UpdateOpportunityAgreementAPI.dopostmethod();
        Test.stoptest();
    }
    @isTest
    static void testNoReq(){
        createTestData();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/updateOpportunity/*'; //Request URL
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('');
        RestContext.request = req;
        RestContext.response= res;
        Test.starttest();
        UpdateOpportunityAgreementAPI.dopostmethod();
        
        SalesAgreementSchema.SalesAgreementLookupResponse reqst=new SalesAgreementSchema.SalesAgreementLookupResponse();
        reqst.sfdcOpportunityID=null;
        
        String JsonMsg=JSON.serialize(reqst);
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        UpdateOpportunityAgreementAPI.dopostmethod();
        
        reqst.sfdcOpportunityID=acc.Id;
        
        JsonMsg=JSON.serialize(reqst);
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        UpdateOpportunityAgreementAPI.dopostmethod();
        
        Test.stoptest();
    }
    static void createTestData(){
        list<CommercialMessages__c> messages = new list<CommercialMessages__c>();
        messages.add(new CommercialMessages__c(Name = 'SalesAgreementPresent', Message__c = 'SalesAgreementPresent'));
        messages.add(new CommercialMessages__c(Name = 'SalesAgreementRequired', Message__c = 'SalesAgreementRequired'));
        messages.add(new CommercialMessages__c(Name = 'requestTypeRequired', Message__c = 'requestTypeRequired'));
        messages.add(new CommercialMessages__c(Name = 'OppRequired', Message__c = 'OppRequired'));
        messages.add(new CommercialMessages__c(Name = 'SalesRepRequired', Message__c = 'OppRequired'));
        messages.add(new CommercialMessages__c(Name = '500', Message__c = 'Error 500'));
        messages.add(new CommercialMessages__c(Name = 'Emptyrequest', Message__c = 'Emptyrequest'));
        messages.add(new CommercialMessages__c(Name = 'OptyNotFound', Message__c = 'OptyNotFound'));
        messages.add(new CommercialMessages__c(Name = 'OppIdRequired', Message__c = 'OppIdRequired'));
        insert messages;
        
        ResaleGlobalVariables__c rgvVar=new ResaleGlobalVariables__c();
        rgvVar.name='DataRecastDisableAccountTrigger';
        rgvVar.value__c='true';
        insert rgvVar;
        acc = TestHelperClass.createAccountData();
        acc.TelemarAccountNumber__c='1234';
        acc.Phone='1234567890';
        acc.PhoneNumber2__c='12345678901';
        acc.PhoneNumber3__c='1234567890';
        acc.PhoneNumber4__c='1234567890';
        update acc;
        aLog = new AuditLog__c();
        aLog.name = 'Test Audit Log';
        aLog.Account__c = acc.id;
        aLog.FormName__c = 'Test Form Name';
        aLog.Email__c = 'test@tcs.com';
        alog.PrimaryPhone__c = '1478523698';
        aLog.Type__c = 'Contact';
        //aLog.LineOfBusiness__c = '1100 - Residential';
        insert aLog;
        
        opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp;
    }
}