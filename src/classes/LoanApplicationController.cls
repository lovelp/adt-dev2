/************************************* MODIFICATION LOG ********************************************************************************
 *
 * DESCRIPTION : LoanApplicationController.cls 
 *
 *---------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                  REASON
 *---------------------------------------------------------------------------------------------------------------------------------------
 * Abhinav Pandey                03/03/2019            Loan Application Controller class.
 * Abhinav Pandey                08/06/2019            HRM - 9932 Date of birth and SSN logic added
 * Abhinav Pandey                03/11/2019                HRM-10881         - Flex Fi Changes
 */

public with sharing class LoanApplicationController {
    
    public Account acc {get;set;}
    public String agentType {get;set;}
    public String accountDetails {get;set;}
    public String loanDetails {get;set;}
    public String shippingAddress {get;set;}
    public String transactionDetails{get;set;}
    public String loanApplicationPermittedMessage {get;set;}
    public String accountSsnLastFourValue {get;set;}
    public String accountDobValue {get;set;}
    public String globalLoanId{get;set;}
    public String TermsAndConditions_PhoneSales_Pragraph1 {get;set;}
    public String TermsAndConditions_PhoneSales_Pragraph2 {get;set;}
    public String TermsAndConditions_PhoneSales_Pragraph3 {get;set;}
    public Boolean isNSCUser {get;set;}
    public StaticResource phoneSalesTermsParagraph1;
    public StaticResource phoneSalesTermsParagraph2;
    public StaticResource phoneSalesTermsParagraph3;
    public LoanApplicationDataAccess dataLayerObject = new LoanApplicationDataAccess();

    public enum ViewState {
        Field,
        Phone
    }
    
    
    public LoanApplicationController(ApexPages.StandardController controller){
        this.initializeAccountFromStandardController(controller)
        .getAccountInfo()
        .initializeExistingDetails()
        .initializeExistingLoanDetails();
    }
    
    private LoanApplicationController initializeAccountFromStandardController(ApexPages.StandardController controller){
        this.transactionDetails ='';    
        this.globalLoanId ='';
        this.acc = (Account)Controller.getRecord();
        this.agentType = ApexPages.currentPage().getParameters().get('agentType');
        return this;
    }
    
    private LoanApplicationController getAccountInfo(){
        System.debug('### Getting Account Info'); 
        if(this.acc != null){
            this.acc = Utilities.queryAccount(this.acc.id);
        }
        return this;
    }
    
    private LoanApplicationController initializeExistingDetails(){
        this.accountDetails='';
        if(this.acc != null){
            //HRM - 9932
            this.accountSsnLastFourValue = String.isNotBlank(this.acc.Equifax_Last_Four__c) ? this.acc.Equifax_Last_Four__c : '';
            this.accountDobValue = String.isNotBlank(this.acc.DOB_encrypted__c) ? formatDate(this.acc.DOB_encrypted__c) :'';
            this.accountDetails = this.dataLayerObject.getExistingAccountDetails(this.acc);
            if(String.isNotBlank(this.accountDetails)){
                this.accountDetails = this.accountDetails.replaceAll('_',' ');
            }
            this.isNSCUser =  this.dataLayerObject.isFieldPhone();                 
            
            this.phoneSalesTermsParagraph1 = this.dataLayerObject.getPhonePragraph1();
            Blob fileContent1 = this.phoneSalesTermsParagraph1.Body;
            this.TermsAndConditions_PhoneSales_Pragraph1 = EncodingUtil.base64Encode(fileContent1);
            fileContent1 = EncodingUtil.base64Decode(this.TermsAndConditions_PhoneSales_Pragraph1);
            this.TermsAndConditions_PhoneSales_Pragraph1 = fileContent1.toString(); 
            
            /* this.phoneSalesTermsParagraph2 = this.dataLayerObject.getPhoneParagraph2();
            Blob fileContent2 = this.phoneSalesTermsParagraph2.Body;
            this.TermsAndConditions_PhoneSales_Pragraph2 = EncodingUtil.base64Encode(fileContent2);
            fileContent2 = EncodingUtil.base64Decode(this.TermsAndConditions_PhoneSales_Pragraph2);
            this.TermsAndConditions_PhoneSales_Pragraph2 = fileContent2.toString(); */            
            
            this.phoneSalesTermsParagraph3 = this.dataLayerObject.getPhoneParagraph3();
            Blob fileContent5 = this.phoneSalesTermsParagraph3.Body;
            this.TermsAndConditions_PhoneSales_Pragraph3 = EncodingUtil.base64Encode(fileContent5);
            fileContent5 = EncodingUtil.base64Decode(this.TermsAndConditions_PhoneSales_Pragraph3);
            this.TermsAndConditions_PhoneSales_Pragraph3 = fileContent5.toString();
                 
        }
        return this;
    }
    
    private LoanApplicationController initializeExistingLoanDetails(){
        this.loanDetails = ' ';
        if(this.acc != null){
            this.loanDetails = this.dataLayerObject.getExistingLoanDetails(this.acc);
            this.shippingAddress = this.dataLayerObject.getAccountAddress(this.acc);            
            this.loanApplicationPermittedMessage = this.dataLayerObject.getLoanApplicationAllowedMessage(this.acc,true,'');//HRM-10881 added true boolean
            if(String.isNotBlank(this.loanDetails)){
                this.loanDetails = this.loanDetails.replaceAll('_',' ');
            }
        }
        return this;
    }
    
    public PageReference init(){       
        if(this.acc != null){
            this.dataLayerObject.validateAndUpdateAddress(this.acc);
        }
        return null;
    }

    public boolean getViewStateField(){
        System.debug('### agentType is'+this.agentType);  
        if(String.isNotBlank(this.agentType) && this.agentType.equalsIgnoreCase(String.valueOf(LoanApplicationController.ViewState.Field))){ 
            System.debug('### field state is true');        
            return true;
        }
        else {
            return false;
        }
    }
    
    public boolean getCfgDisabledFlag(){
        return (FlexFiConfig__c.getinstance('CFG Disabled') != null && String.isNotBlank(FlexFiConfig__c.getinstance('CFG Disabled').Value__c)) && FlexFiConfig__c.getinstance('CFG Disabled').value__c.equalsIgnoreCase('true') ? true : false; 
    }
    public boolean getViewStatePhone(){
        if(String.isNotBlank(this.agentType) && this.agentType.equalsIgnoreCase(String.valueOf(LoanApplicationController.ViewState.Phone))){            
            return true;
        }
        else {
            return false;
        }
    }
    
    public Boolean getAccountHasActiveApp(){
        Boolean accountHasActiveLoanApp = false;
        if(this.acc != null){
             accountHasActiveLoanApp = this.dataLayerObject.hasActiveApplication(this.acc);
        }
        return accountHasActiveLoanApp;
    }
    
    public Boolean getAccountHasApproApp(){
        Boolean flag = false;
        if(this.acc != null){
             flag = this.dataLayerObject.hasApproApp(this.acc);
        }
        return flag;
    }
       
      
     public  PageReference postPayment(){
        String transactionSuccess  = this.dataLayerObject.saveProfileId(transactionDetails,globalLoanId);
        return null;
    }
    
    @RemoteAction
    public static String getFullLoanDetails(String loanId){
        LoanApplicationDataAccess dataObject = new LoanApplicationDataAccess();
        String existingLoanDetails = '';
        existingLoanDetails  = dataObject.getFullLoanDetailsBasedOnId(loanId);
        if(String.isNotBlank(existingLoanDetails)){
            existingLoanDetails = existingLoanDetails.replaceAll('_',' ');
        }
        return existingLoanDetails;
    }
    
     
    @RemoteAction
    public static String getPaymentTechUrl(String loanId){
        LoanApplicationDataAccess dataObject = new LoanApplicationDataAccess();
        String paymentTechUrl = '';
        paymentTechUrl  = dataObject.getPaymentURL(loanId);
        System.debug('### Payment Tech URL'+paymentTechUrl);
        return paymentTechUrl;
    }
    
    @RemoteAction
    public static String saveNewLoanApplication(String accId,String shippingAddress,String billingAddress,String previousAddress,String annualIncome,Boolean isSameBillingShipping,String DOB){
        try{
            LoanApplicationDataAccess dataObject = new LoanApplicationDataAccess();
            System.debug('edit shipping'+shippingAddress+' DOB is'+DOB);
            String saveApplication = 'false';
            saveApplication  = dataObject.saveLoanApplication(accId,shippingAddress,billingAddress,previousAddress,null,annualIncome,isSameBillingShipping,DOB);
            return saveApplication;
        }catch(exception e){
            return e.getMessage();
        }
    }
    
    @RemoteAction
    public static String editNewLoanApplication(String accId,String shippingAddress,String billingAddress,String previousAddress,String loanId,String annualIncome,Boolean isSameBillingShipping,String DOB){
        try{
            LoanApplicationDataAccess dataObject = new LoanApplicationDataAccess();
            System.debug('edit shipping'+shippingAddress+' DOB is'+DOB);
            String saveApplication = 'false';
            saveApplication  = dataObject.saveLoanApplication(accId,shippingAddress,billingAddress,previousAddress,loanId,annualIncome,isSameBillingShipping,DOB);
            return saveApplication;
        }catch(exception e){
            return e.getMessage();
        }
    }
    
    @RemoteAction
    public static String submitToZoot(String loanId,String cvv,String ssn){
        LoanApplicationDataAccess dataObject = new LoanApplicationDataAccess();
        String zootResponse = dataObject.submitToZootAPI(loanId,cvv,ssn);
        System.debug('### zoot response is'+zootResponse+' loan id is'+loanId);
        //return String.isNotBlank(zootResponse) ? zootResponse : '';//uncomment once zoot api is ready
        return zootResponse;
    }
    
    @RemoteAction
    public static String deactivateApplication(String loanId){
        LoanApplicationDataAccess dataObject = new LoanApplicationDataAccess();
        String deactivate = dataObject.deactivateApplication(loanId);
        return deactivate;
    }
    
    //HRM - 9932
    public static string formatDate(String dob){
        if(dob.contains('/')){
            String[] strlist = dob.split('/');
            DateTime newDate = DateTime.newInstance(Integer.valueOf(strlist[2]),Integer.valueOf(strlist[0]),Integer.valueOf(strlist[1]),0,0,0);
            return String.valueOf(newDate.format('MM/dd/YYYY'));
        }
        return dob;
    }
    
}