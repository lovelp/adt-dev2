/************************************* MODIFICATION LOG ********************************************************************************************
 * ProcessCloseCall - 
 *
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                        DATE              TICKET            REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Siju Varghese                04/26/2019          HRM - 9684        - Hot Leads Automation – Assign Lead by disposition
 * Abhinav Pandey               07/25/2019          Ownership Assignment - Added logic to separate Hot Leads and ownership assignment to different class.  
 * Viraj Shah                   09/20/2019          Gryphon/PossibleNow callouts logic.
 */

public without sharing class ProcessCloseCall{
     public static String dispositionMessage =  'Hot Lead Assigned';
     /************************************************************************************************************
     Query the Call_Disposition__c object using selected selected Tier1 Disposition or selected Tier2 Disposition
     If it matched with  Call_Disposition__c.ActionItems__c == 'Hot Lead'  - then ProcessHotLead
     **************************************************************************************************************/
     public static void processDispositionActions(String usId,Account acc,Disposition__c cDisp){
          list<Call_Disposition__c> callDispDetail = new list<Call_Disposition__c>();
          if(String.isNotBlank(cDisp.DispositionDetail__c)){ //Check with selected dispostion2 
               callDispDetail = [ SELECT ActionItems__c FROM Call_Disposition__c WHERE Name =:cDisp.DispositionDetail__c and Active__c = true LIMIT 1];                     
          }else if(String.isNotBlank(cDisp.DispositionType__c )){//Check with selected dispostion1
               callDispDetail = [ SELECT ActionItems__c FROM Call_Disposition__c WHERE Name =:cDisp.DispositionType__c  and Active__c = true LIMIT 1];
          }                    
               
          if(!callDispDetail.isEmpty() && callDispDetail[0].ActionItems__c != null && callDispDetail[0].ActionItems__c.contains('Hot Leads')){
               try{
                    //Hot Leads Processing
                    ProcessHotLeads.processHotLeadsAction(usId,acc,cDisp,'Hot Leads');               
               }catch(Exception e) {
                    ADTApplicationMonitor.log ('ProcessHotLead', e.getMessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);
               }              
          }
          //Ownership Assignment
          if(!callDispDetail.isEmpty() && callDispDetail[0].ActionItems__c != null && callDispDetail[0].ActionItems__c.contains('Reassignment')){
               try{
                    ProcessHotLeads.processHotLeadsAction(usId,acc,cDisp,'Reassignment');               
               }catch(Exception e) {
                    ADTApplicationMonitor.log ('Reassignment', e.getMessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);   
               }              
          }
        
          //Ownership Assignment
          if(!callDispDetail.isEmpty() && callDispDetail[0].ActionItems__c != null && callDispDetail[0].ActionItems__c.containsIgnoreCase('addEbrInquiry')){
               try{
                    List<Account> accList = new List<Account>();
                    accList.add(acc);
                    AdtCustContactEBRService.createRequestQueues(accList,null);
               }catch(Exception e) {
                    ADTApplicationMonitor.log ('ebr enquiry', e.getMessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);
               }              
          }
     }
}