public class DataloadController{
    
    public String selectedGroups{get;set;}
    //public Map<String, dataloadWrapper> dataloadMap{get;set;}
    public String jsonString{get;set;}
    public String salesForceURL{get;set;}
    //Wrapper of record count
    public class dataloadWrapper{
        public String groupdIdName;
        public Integer recordCountUnproc;
        public dataloadWrapper(String gid, Integer rc){
            groupdIdName = gid;
            recordCountUnproc = rc;
        }
    }
    
    public DataloadController(ApexPages.StandardSetController controller){
        //constructor
        Schema.Describesobjectresult res = Schema.Sobjecttype.DataloadTemp__c;
        salesForceURL = URL.getSalesforceBaseUrl().toExternalForm() +'/'+res.getKeyPrefix()+'/o';
    }
    //Prepare the select options
    public list<SelectOption> getgroupIdOption(){
        list<SelectOption> options = new list<SelectOption>();
        list<dataloadWrapper> dataloadlist = new list<dataloadWrapper>();
        Map<String, Integer> dataloadMap = new Map<String, Integer>();
        AggregateResult[] groupedResults = [SELECT GroupId__c, COUNT(Id) FROM DataloadTemp__c WHERE Processed__c = false GROUP BY GroupId__c LIMIT 1000];
        for(AggregateResult ar : groupedResults){
            if(ar.get('GroupId__c') != null){
            options.add(new SelectOption(String.valueOf(ar.get('GroupId__c')).toUpperCase(),String.valueOf(ar.get('GroupId__c')).toUpperCase()));
            //dataloadlist.add(new dataloadWrapper(String.valueOf(ar.get('GroupId__c')),Integer.valueOf(ar.get('expr0'))));
            dataloadMap.put(String.valueOf(ar.get('GroupId__c')).toUpperCase(),Integer.valueOf(ar.get('expr0')));
            }
        }
        jsonString = JSON.serialize(dataloadMap);
        
        return options;
    }
    
    //Process batch job here
    public pageReference processBatch(){
        String selGroup = selectedGroups.replace('[','').replace(']','');
        DataLoadBatch myBatchObject = new DataLoadBatch(selGroup); 
        Id batchId = Database.executeBatch(myBatchObject,100);
        Schema.Describesobjectresult res = Schema.Sobjecttype.DataloadTemp__c;
        PageReference pageRef = new PageReference('/'+res.getKeyPrefix()+'/o' ); 
        pageRef.setRedirect(true); 
        return pageRef; 
       
    }
}