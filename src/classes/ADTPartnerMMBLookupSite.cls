/************************************* MODIFICATION LOG ********************************************************************************************
* 
*
* DESCRIPTION : Perform various MMBLookupCalculations
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                         DATE              TICKET          REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Srinivas Yarramsetti &            01/19/2018                        - Original Version
* Siddarth Asokan & Ravi Pochamalla       
* Mounika Anna                      03/22/2018        HRM-6887        - Past Due Relocations
* Giribabu Geda                     08/08/2018        HRM-6127        - NON payaccount feeding wrong leadsource to MMB
* Srinivas Yarramsetti              10/02/2018        HRM-7486        - Remove address nuls
* Jitendra Kothari                  04/12/2019        HRM-9466        - 3G Cell change or Panel Change notification
* Nishanth Mandala                  07/26/2019        HRM-10450       - Added check for National
* Siddarth Asokan                   08/25/2019        HRM-10618       - SMB Cyber changes (contract number)
* Siddarth Asokan                   08/25/2019        HRM-10618       - P1MMB site changes
* Srinivas Yarramsetti              09/23/2019        HRM-10745       - Site and cust start date
*/

public class ADTPartnerMMBLookupSite {
    
    public enum LookupSelect {CUSTOMER, SITE, CUSTOMER_SITE, NOT_FOUND_NEW}
    public boolean isContractActive;
    public boolean accUpdatedFlag;
    public String strAppName;
    public MMBCustomerSitePartner mmbCustomerSitePartnerController;
    //Added by TCS for HRM 5925
    Public String  requireSelectConfirmMsg;
    //HRM 7492
    public user u;
    public decimal pastdue;
    //Boolean which controls the method from account update or Auto MMB refresh
    public Boolean isAccountUpdate = false;
    public String P1TransferCallPhoneNumber{
        get{
            return String.valueof(ResaleGlobalVariables__c.getInstance('P1RententionPhNumber').value__c);
        }
    }
    public String P1HOAPhoneNumber{
            get{
            return String.valueof(ResaleGlobalVariables__c.getInstance('P1HOAPhNumber').value__c);
        }
    }
    
    public String HOATeamCallPhoneNumber{
        get{
            return String.valueof(ResaleGlobalVariables__c.getInstance('HOATeamPhone').value__c);
        }
    }
    //END HRM 5925
    public ADTPartnerMMBLookupSite (){
        // constructor
        isContractActive = false;
        accUpdatedFlag = false;
        strAppName = 'ADTPartner MMBLookup Error';
        mmbCustomerSitePartnerController = new MMBCustomerSitePartner ();
        //HRM 7492
        u = [select id,profile_type__c,Rep_Team__c from user where id=:userinfo.getUserId()];
    }
    
    public List<MMBCustomerSiteSearchApi.SiteInfoResponse> MMBLookupSiteInformation (string siteId){
        try{
            boolean isP1nADTSite = false;
            isP1nADTSite = siteId.startsWithIgnoreCase('P1') ? true : false;
            
            // create Site info Request
            MMBCustomerSiteSearchApi.SiteInfoRequest siteInfoReq = new MMBCustomerSiteSearchApi.SiteInfoRequest();
            siteInfoReq.FirstName   = '';
            siteInfoReq.LastName    = '';
            siteInfoReq.Address1    = '';
            siteInfoReq.City        = '';
            siteInfoReq.State       = '';
            siteInfoReq.ZipCode     = '';
            //String prefixVal = PartnerAPIMessaging__c.getInstance('SitePrefixes').Error_Message__c;
            siteInfoReq.SiteNo      = siteId.containsIgnoreCase('_') ? siteId.substringAfter('_') : siteId;
            
            system.debug('@@siteInfoReq: '+siteInfoReq);
            MMBCustomerSiteSearchApi.LookupSiteResponse siteResponse = MMBCustomerSiteSearchApi.LookupSiteForPartner(siteInfoReq);
             
            //MMBCustomerSiteSearchApi.LookupSiteResponse siteResponse = mmbCustomerSitePartnerController.getSites('', '', '', '', siteId);
            system.debug('@@siteResponse: '+siteResponse);
            
            List<MMBCustomerSiteSearchApi.SiteInfoResponse> selectedResultSiteList = new List<MMBCustomerSiteSearchApi.SiteInfoResponse>();
            
            // if Success Response
            if (siteResponse !=null && siteResponse.ResultSummaryMessage.equalsIgnoreCase('Success')){
                if (isP1nADTSite){
                    for (MMBCustomerSiteSearchApi.SiteInfoResponse sResponse : siteResponse.siteInfo){
                        system.debug('in for %%%'+sResponse);
                        if (sResponse.CsAccountType != 'Secondary' && sResponse.BillingSystems.contains('P1MMB') && !string.valueOf(sResponse.CS_NO).startsWith('~')){ 
                            selectedResultSiteList.add(sResponse);
                        }                    
                    }
                }else{
                    // If Order Type != N1 (for all other order types), get the response with csAccountType = primary  
                    for (MMBCustomerSiteSearchApi.SiteInfoResponse sResponse : siteResponse.siteInfo){
                        // get the response with csAccountType == Primary
                        //Modified by Srini
                        if (sResponse.CsAccountType.equalsIgnoreCase('Primary') && !string.valueOf(sResponse.CS_NO).startsWith('~')){
                            selectedResultSiteList.add(sResponse);
                        }
                    }
                }
            }else{
                string errMessage = '';
                if(siteResponse != null){
                    if (siteResponse.ResultSummaryMessage == 'NOTFOUND'){
                        system.debug('No site records were found.');
                        errMessage = 'ADTPartnerMMBLookupSite::MMBLookupSiteInformation - ' + siteId + ': No site records were found.';
                    }else{
                        system.debug('Error while searching for site information. '+siteResponse.ResultSummaryMessage);
                        errMessage = 'ADTPartnerMMBLookupSite::MMBLookupSiteInformation - ' +siteId + 'Error while searching for site information. ' +siteResponse.ResultSummaryMessage;
                    }                        
                }else{
                    system.debug('Unable to reach MMB.');
                    errMessage = 'ADTPartnerMMBLookupSite::MMBLookupSiteInformation - Unable to reach MMB.';
                }
                if (string.isNotBlank(errMessage)){
                    ADTApplicationMonitor.log (strAppName, errMessage , true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                }
            }
            return selectedResultSiteList;
        }catch (Exception ex){
            system.debug('@@Exception Occured: '+ex.getMessage());
            ADTApplicationMonitor.log(ex, 'ADTPartnerMMBLookupSite', 'MMBLookupSiteInformation', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            return null;
        }
    }
    
    public MMBCustomerSiteSearchApi.CustomerInfoResponse MMBLookupCustomerInformation (List<MMBCustomerSiteSearchApi.SiteInfoResponse> selectedResultSiteList){
        system.debug('selectedResultSiteList%%%'+selectedResultSiteList);
        try{            
            MMBCustomerSiteSearchApi.LookupCustomerResponse customerResponse = mmbCustomerSitePartnerController.getCustomers(selectedResultSiteList[0].BillingCustNos[0], selectedResultSiteList[0].BillingSystems);
            system.debug('@@customerResponse: '+customerResponse);
            
            MMBCustomerSiteSearchApi.CustomerInfoResponse selectedResultCustomer = new MMBCustomerSiteSearchApi.CustomerInfoResponse ();
            
            // if Success Response
            if (customerResponse != null && customerResponse.success){
                selectedResultCustomer = processCustomerResponse (selectedResultSiteList,customerResponse);                
            }else{
                string errMessage = '';
                if(customerResponse != null){
                    if (customerResponse.ResultSummaryMessage == 'NOTFOUND'){
                        system.debug( 'No customer records were found.');
                        errMessage = 'ADTPartnerMMBLookupSite::MMBLookupCustomerInformation - ' + selectedResultSiteList[0].BillingCustNos[0] + ': No customer records were found.';
                    }else{
                        system.debug('Error while searching for customer information. '+customerResponse.ResultSummaryMessage);
                        errMessage = 'ADTPartnerMMBLookupSite::MMBLookupCustomerInformation - ' +selectedResultSiteList[0].BillingCustNos[0] + 'Error while searching for customer information. '+customerResponse.ResultSummaryMessage;
                    }
                }
                else{
                    system.debug('Unable to reach MMB.');
                    errMessage = 'ADTPartnerMMBLookupSite::MMBLookupCustomerInformation - Unable to reach MMB.';
                }
                if (string.isNotBlank(errMessage)){
                    ADTApplicationMonitor.log (strAppName, errMessage, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                }
            }
            
            system.debug('@@selectedResultCustomer: '+selectedResultCustomer);            
            return selectedResultCustomer;
        }catch (Exception ex){
            system.debug('@@Exception Occured: '+ ex);
            ADTApplicationMonitor.log(ex, 'ADTPartnerMMBLookupSite', 'MMBLookupCustomerInformation', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
            return null;
        }
    }

    public MMBCustomerSiteSearchApi.CustomerInfoResponse processCustomerResponse (MMBCustomerSiteSearchApi.LookupCustomerResponse customerResponse){
        return processCustomerResponse(null,customerResponse);
    }

    public MMBCustomerSiteSearchApi.CustomerInfoResponse processCustomerResponse (List<MMBCustomerSiteSearchApi.SiteInfoResponse> selectedResultSiteList, MMBCustomerSiteSearchApi.LookupCustomerResponse customerResponse){
        try{
            Integer Idx = 0;
            Map<String, MMBCustomerSiteSearchApi.CustomerInfoResponse> mapCustInfoResponse = new Map<String, MMBCustomerSiteSearchApi.CustomerInfoResponse>();
            List<MMBCustomerSiteSearchApi.CustomerInfoResponse> lstCustInfoResponse = new List<MMBCustomerSiteSearchApi.CustomerInfoResponse>();
            MMBCustomerSiteSearchApi.CustomerInfoResponse selectedResultCustomer = new MMBCustomerSiteSearchApi.CustomerInfoResponse ();
            
            for(MMBCustomerSiteSearchApi.CustomerInfoResponse cResponse: customerResponse.customerInfo){
                // Used to determine row item might be deprecated
                cResponse.Idx = Idx;
                // check if we already have this customer's contract number
                if(mapCustInfoResponse.containsKey(cResponse.ContractNo)){
                    String duplicateContractNoKey = cResponse.ContractNo + '-' + cResponse.ServiceActive;
                    MMBCustomerSiteSearchApi.CustomerInfoResponse tempCustRes = mapCustInfoResponse.get(cResponse.ContractNo);
                    //IF a contract is returned more than once, and all of them are either IN or OUT, display them all
                    if( cResponse.ServiceActive == tempCustRes.ServiceActive ){
                        duplicateContractNoKey += String.valueOf(Idx);
                        mapCustInfoResponse.put(duplicateContractNoKey, cResponse);
                    }
                    //IF a contract is returned more than once, and it is in both IN and OUT, then only the IN would display
                    else if(cResponse.ServiceActive == 'IN'){
                        if(tempCustRes.Idx == 0){
                            selectedResultCustomer = cResponse;
                        }
                        cResponse.Idx = tempCustRes.Idx;
                        mapCustInfoResponse.put(cResponse.ContractNo, cResponse);
                    }
                }else{
                    //IF a contract is only returned once, regardless of the Service being IN or OUT – it would display.
                    if(Idx == 0){
                        selectedResultCustomer = cResponse;
                    }
                    mapCustInfoResponse.put(cResponse.ContractNo, cResponse);
                }
                Idx++;
            }
            
            if(!mapCustInfoResponse.isEmpty()){
                lstCustInfoResponse.addAll(mapCustInfoResponse.values());
                lstCustInfoResponse.sort();
                system.debug('Contract numbers are: '+mapCustInfoResponse.keyset());
                if(!lstCustInfoResponse.isEmpty()){
                    for( MMBCustomerSiteSearchApi.CustomerInfoResponse cRes:  lstCustInfoResponse){
                        if(selectedResultSiteList != null && selectedResultSiteList.size() >0){
                            if(!cRes.BillingSystem.equalsIgnoreCase('MMB') || 
                            (cRes.BillingSystem.equalsIgnoreCase('MMB') && cRes.ContractStatus.equalsIgnoreCase('A') &&
                            (String.isBlank(selectedResultSiteList.get(0).ContractNo) || String.isNotBlank(selectedResultSiteList.get(0).ContractNo) && selectedResultSiteList.get(0).ContractNo == cRes.ContractNo))){
                                isContractActive = true;
                                selectedResultCustomer = cRes;
                                break;
                            }
                        }else if(cRes.ServiceActive == 'IN' && (!cRes.BillingSystem.equalsIgnoreCase('MMB') || 
                        (cRes.BillingSystem.equalsIgnoreCase('MMB') && cRes.ContractStatus.equalsIgnoreCase('A')))){
                            isContractActive = true;
                            selectedResultCustomer = cRes;
                            break;
                        }
                        /*}else{
                            if (cRes.ServiceActive == 'IN'){ 
                                if((cRes.ContractStatus == null || string.isBlank(cRes.ContractStatus) || !cRes.ContractStatus.equalsIgnoreCase('A'))  &&  'MMB' .equalsIgnoreCase(cRes.BillingSystem )){
                                    isContractInActive = true;
                                }else{
                                    isContractInActive = false;
                                    selectedResultCustomer = cRes;
                                    break;
                                }
                            }
                        }*/
                    }
                }
            }
            return selectedResultCustomer;
        }catch (Exception ex){
            system.debug ('@@Exception Occurred: ' + ex.getMessage());
            ADTApplicationMonitor.log(ex, 'ADTPartnerMMBLookupSite', 'processCustomerResponse', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
            return null;
        }
    }
    
    // Added by TCS for HRM 5925
    Public String ReturnConfirmMessage(List<MMBCustomerSiteSearchApi.SiteInfoResponse> lstSelectedResultSite, MMBCustomerSiteSearchApi.CustomerInfoResponse selectedResultCustomer, LookupSelect mmbLookupType, id accountId){
        Account saveAcnt = new Account();
        saveAcnt = updateAccountMMB(lstSelectedResultSite,  selectedResultCustomer,  mmbLookupType, accountId);
        update saveAcnt;
        return requireSelectConfirmMsg;
    }

    @TestVisible
    private String getCustomerStatus(String val) {
        if(String.isNotBlank(val)){
            if(val.trim()=='I'){
                return 'Inactive';
            }else if(val.trim()=='A'){
                return 'Active';
            }else{
                val.trim();
            }
        }
        return '';
    }
    
    @TestVisible
    private String getDiscoDate(MMBCustomerSiteSearchApi.CustomerInfoResponse selectedResultCustomer) {
        return (String.isNotBlank(selectedResultCustomer.DiscoDate))? selectedResultCustomer.DiscoDate: 'No Date';
    }
    // END FOR HRM 5925

    public account updateAccountMMB (List<MMBCustomerSiteSearchApi.SiteInfoResponse> lstSelectedResultSite, MMBCustomerSiteSearchApi.CustomerInfoResponse selectedResultCustomer, LookupSelect mmbLookupType, id accountId) {
        requireSelectConfirmMsg = '';
        try{
            Account acc = new Account();
            if(String.isNotBlank(accountId)){
                acc = [select id, Partner_ID__c, Partner__c, FirstName__c, LastName__c, LeadSourceNo__c, DOB_encrypted__c, Phone, 
                    PhoneNumber2__c, Email__c, MMBCustomerNumber__c, MMBSiteNumber__c, MMBOrderType__c, DispositionCode__c, MMBBillingSystem__c,
                    DispositionComments__c, OwnershipProfileCode__c, Profile_BuildingType__c, Profile_YearsInResidence__c, CPQIntegrationData__c,
                    Business_Id__c, Equifax_Last_Check_DateTime__c, EquifaxRiskGrade__c, EquifaxApprovalType__c, PostalCodeId__c, PostalCodeID__r.Name, MMB_Past_Due_Balance__c,
                    AddressID__r.Street__c, AddressID__r.City__c, Channel__c, AddressID__r.State__c, AddressID__r.PostalCode__c, Profile_RentOwn__c, SiteStreet__c, SiteCity__c, 
                    SiteState__c, SitePostalCode__c, AddressID__c, TelemarLeadSource__c, QueriedSource__c, GenericMedia__c, MMB_Relo_Customer__c, OwnerId, Rep_User__c, 
                    DisconnectReason__c, NonPayDiscoReasonCode__c
                    from Account where id =: accountId];
            }
            // Always reset Due Balance on this account, rep selection and customer mmb data determine this value
            acc.MMB_Past_Due_Balance__c = 0.0;
            acc.MMB_Disco_Count__c = 0;
            acc.Inactive_Contract__c = !isContractActive;
            MMBCustomerSiteSearchApi.SiteInfoResponse s;
            Boolean isHOA = false;
            if (!lstSelectedResultSite.isEmpty()){
                if(lstSelectedResultSite.size() > 1 && String.isNotBlank(acc.MMBBillingSystem__c) && acc.MMBBillingSystem__c.containsIgnoreCase('P1MMB')){
                    // If there are more than 1 site search for P1MMB site and if nothing exists, use the 1st record
                    for(MMBCustomerSiteSearchApi.SiteInfoResponse sInfo : lstSelectedResultSite){
                        if(sInfo.billingSystems != null && sInfo.billingSystems.contains('P1MMB')){
                            s = sInfo;
                            break;
                        }
                    }
                }
                if(s == null){
                    // If there is only 1 site in the response or there are no P1MMB sites
                    s = lstSelectedResultSite[0];  
                }
                if (s != null){
                    // Applies to Both Select Site & Select Both
                    if(String.isNotBlank(s.HOA) && s.HOA.equalsIgnoreCase('Y')){
                        acc.HOA__c = true;
                        isHOA = true;
                    }
                    if(String.isNotBlank(s.SystemType)){
                        acc.MMBSystemType__c = s.SystemType;// HRM-9405 SMB CYBER Changes
                    }
                    acc.MMBSystemNumber__c = s.SYSTEM_NO;
                    acc.MMBSiteStartDate__c = String.isNotBlank(s.siteStartDate)? Date.valueOf(s.siteStartDate):null; //HRM-10745
                    acc.MMBSystemStartDate__c = String.isNotBlank(s.SystemStartDate)? Date.valueOf(s.SystemStartDate):null; //HRM-10745
                }
                // Set Channel & Biz Id
                if(String.isNotBlank(acc.Business_Id__c) && acc.Business_Id__c.contains('1100') && (acc.HOA__c || (!acc.HOA__c && acc.Channel__c == Channels.TYPE_HOADIRECTSALES))){
                    // HRM-10678 Siju Added - HOA Direct Sales
                    acc.Channel__c = acc.HOA__c? Channels.TYPE_HOADIRECTSALES : Channels.TYPE_RESIDIRECT;
                }else if (!acc.MMB_Relo_Customer__c && mmbLookupType == LookupSelect.SITE && acc.Channel__c != Channels.TYPE_CUSTOMHOMESALES){
                    // Default Resale Account (Resi or SMB Resale)
                    acc.Channel__c = Channels.getDefaultReSaleChannel(acc.Business_Id__c);
                }else if (!acc.MMB_Relo_Customer__c && String.isNotBlank(acc.Business_Id__c) && s.Sitetype != null && (acc.Business_Id__c.contains('1200') || acc.Business_Id__c.contains('1300')) && 
                (s.SiteType.containsIgnoreCase('Commercial') || s.SiteType.containsIgnoreCase('National'))){ 
                    acc.Business_Id__c = Channels.TYPE_COMMERCIAL;
                    // HRM-10450 - Commercial account (Business Direct Sales or National)
                    if (s.SiteType.containsIgnoreCase('Commercial')){
                        acc.Channel__c = Channels.TYPE_BUSINESSDIRECTSALES;   
                    }else if(s.SiteType.containsIgnoreCase('National')){
                        acc.Channel__c = Channels.TYPE_NATIONAL; 
                    }
                    // Assign a 1300 postal code
                    for(Postal_Codes__c pc: [Select id from Postal_Codes__c where BusinessId__c = '1300' AND Name =:acc.PostalCodeID__r.Name limit 1]){
                        acc.PostalCodeId__c = pc.Id;
                    }
                }
            }
            
            if(mmbLookupType == LookupSelect.SITE){
                system.debug('======> lookup select site');
                if(s != null){
                    acc.MMBContractStartDate__c = '';
                    acc.MMBBillCodes__c = '';
                    acc.MMBBillingSystem__c = '';
                    acc.Tookover_Site_Number__c = '';
                    if(isHOA){
                        if(s.billingSystems.contains('P1MMB')){
                            requireSelectConfirmMsg = '<p>The customer/site you have selected is a Protection One Homeowners Association (HOA) account.</p>';
                            requireSelectConfirmMsg+='<p>Please warm transfer customer to the Protection One HOA group for handling at'+P1HOAPhoneNumber+'</p>';
                        }else{
                            requireSelectConfirmMsg= '<p>The customer/site you have selected is a Homeowners Association (HOA) account.<p>';
                            requireSelectConfirmMsg+='<p>Please warm transfer customer to the HOA group for handling at 1-'+HOATeamCallPhoneNumber+'</p>';
                        }
                    }
                    if(checkDIYSite(s)){
                        acc.MMBSiteResponse__c = JSON.serialize(s);
                    }

                    //Added for HRM-9466 
                    String result = MMBCustomerSiteSearchHelper.getChangeNotification(s);
                    if(String.isNotBlank(result)) {
                        requireSelectConfirmMsg += result;
                    }
                }else{
                    //throw new MMBCustomerSiteLookupCtrlException('No site available for selection.');
                    ADTApplicationMonitor.log (strAppName, 'ADTPartnerMMBLookupSite::updateAccountMMB - No site available for selection.', true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                }
            }else if(mmbLookupType == LookupSelect.CUSTOMER_SITE){
                system.debug('======> lookup select both');
                if(selectedResultCustomer != null){
                    acc.MMBCustomerNumber__c = selectedResultCustomer.BillingCustNo;
                    acc.MMBContractNumber__c = selectedResultCustomer.ContractNo;
                    acc.MMBContractStartDate__c = selectedResultCustomer.ContractStartDate;
                    acc.mmbcustomertype__c = selectedResultCustomer.custtype;
                    acc.MMBCustomerStartDate__c = String.isNotBlank(selectedResultCustomer.customerStartDate)? Date.valueOf(selectedResultCustomer.customerStartDate):null; //HRM-10745
                    if(acc.mmbcustomertype__c == 'Local Key Account'){
                        acc.LKA__c = true;
                    }
                    if(String.isNotBlank(selectedResultCustomer.CLV)){
                        acc.CLV__c=selectedResultCustomer.CLV; 
                    }
                    if(String.isNotBlank(selectedResultCustomer.churn)){
                        acc.Churn_Value__c= selectedResultCustomer.churn;
                    }                    
                    
                    if(selectedResultCustomer.billingsystem =='Informix' || selectedResultCustomer.billingsystem =='INF'){
                        acc.MMBBillCodes__c = selectedResultCustomer.BillCodes.replace('|',';');
                        acc.MMBCustomerNumber__c = (s !=null && s.ServiceActive == 'IN')? s.BillingCustNos[0] : ''; 
                        if(s != null){
                            acc.PreviousBillingCustomerNumber__c = s.BillingCustNos[0];//HRM-7486
                        }
                        acc.installingDealer__c = selectedResultCustomer.InstallingDealerNo;
                        acc.dealer_phone__c = selectedResultCustomer.DealerPhone;
                        acc.dealer_name__c = selectedResultCustomer.DealerName;
                        acc.chargeBack__c = selectedResultCustomer.ChargeBackStatus;
                        acc.Informix_Reinstate__c = selectedResultCustomer.ReinstateFlag == 'N'?false:true;
                        acc.Informix_Reinstate_Reason__c = selectedResultCustomer.ReinstateFlag == 'N'?selectedResultCustomer.ReinstateReason:'';
                        acc.Informix_Relocation__c = selectedResultCustomer.RelocationFlag == 'N'?false:true;
                        acc.Informix_Relocation_Reason__c = selectedResultCustomer.RelocationFlag == 'N'?selectedResultCustomer.RelocationReason:'';
                        acc.Informix_Conversion__c = selectedResultCustomer.ConversionFlag == 'N'?false:true;
                        acc.Informix_Conversion_Reason__c = selectedResultCustomer.ConversionFlag == 'N'?selectedResultCustomer.ConversionReason:'';
                        acc.Informix_Addon__c = selectedResultCustomer.AddOnFlag == 'N'?false:true;
                        acc.Informix_Addon_Reason__c = selectedResultCustomer.AddOnFlag == 'N'?selectedResultCustomer.AddOnReason:'';
                    }else if(String.isNotBlank(selectedResultCustomer.ContractNo) && String.isNotBlank(selectedResultCustomer.billingsystem) && selectedResultCustomer.billingsystem.trim()!='P1MMB'){
                        string contractNo = selectedResultCustomer.ContractNo; 
                        MMBCustomerSiteSearchApi.billingInfoRequest billingInfoRequest = new MMBCustomerSiteSearchApi.billingInfoRequest();
                        billingInfoRequest.ContractNo = ContractNo;              
                        MMBCustomerSiteSearchApi.LookupBillingInfoResponse billingResponse = MMBCustomerSiteSearchApi.LookupBillingInfo(billingInfoRequest);
                        if(billingResponse.billingList.size()>0){
                            String billCodes = '';
                            for(MMBCustomerSiteSearchApi.BillingInfoResponse recurLine:billingResponse.billingList){
                                if(String.isNotBlank(recurLine.billCodeId)){
                                    billCodes = String.isBlank(billCodes)? recurLine.billCodeId : billCodes+';'+recurLine.billCodeId;
                                }
                            }
                            acc.MMBBillCodes__c = billcodes;
                        }
                    }
                    
                    acc.MMBBillingSystem__c = selectedResultCustomer.BillingSystem;
                    acc.MMB_Disco_Count__c = selectedResultCustomer.DiscoCount;
                    if(string.isNotBlank(selectedResultCustomer.DiscoReasonCode)){
                        acc.NonPayDiscoReasonCode__c = selectedResultCustomer.DiscoReasonCode.trim();
                    }
                }
                if(s != null){
                    if(String.isNotBlank(s.ContractNo)){
                        //HRM-10618 SMB Cyber changes, overwrite the contract number if the site info response had a contract number
                        acc.MMBContractNumber__c = s.ContractNo;
                    }
                    if(isHOA){
                        if(s.billingSystems.contains('P1MMB')){
                            system.debug('####1');
                            requireSelectConfirmMsg = '<p>The customer/site you have selected is a Protection One Homeowners Association (HOA) account.</p>';
                            requireSelectConfirmMsg+='<p>Please warm transfer customer to the Protection One HOA group for handling at'+P1HOAPhoneNumber+'</p>';
                        }else{
                            requireSelectConfirmMsg= '<p>The customer/site you have selected is a Homeowners Association (HOA) account.<p>';
                            requireSelectConfirmMsg+='<p>Please warm transfer customer to the HOA group for handling at 1-'+HOATeamCallPhoneNumber+'</p>';
                        }
                    }else if(checkDIYSite(s)){
                        requireSelectConfirmMsg= '<p>You will not be able to proceed with Sales Appointment/ Quoting as the selected site is a P1 DIY Site</p>';
                        requireSelectConfirmMsg+='<p>Please warm transfer the customer to P1 '+P1TransferCallPhoneNumber+'</p>';
                    }
                    
                    if(s.ResaleEligible == 'ELIGIBLE' && s.ServiceActive == 'OUT'){
                        // Reinstatements
                        if(selectedResultCustomer.billingsystem !='P1MMB'){
                            //Added by TCS for HRM 5925
                            ResaleGlobalVariables__c dllDept = ResaleGlobalVariables__c.getValues('AllowDLLwithoutOrder'); //HRM 7492
                            if(dllDept != null && dllDept.Value__c.contains(u.Rep_Team__c)){
                                requireSelectConfirmMsg += '<p>We found this selection to be a <strong>reinstatement</strong>.</p>';
                                requireSelectConfirmMsg += '<p>If disconnect is more than 60 days, a new quote must be created. Transfer to NSC.</p>';
                            }else{
                                requireSelectConfirmMsg += '<p>We found this selection to be a <strong>reinstatement</strong>.</p>';
                            }
                        }else if(selectedResultCustomer.billingSystem == 'P1MMB'){
                            //Added by TCS for HRM 5925
                            pastdue = selectedResultCustomer.ReallyPastDueAmount + selectedResultCustomer.WriteOffAmount;
                            requireSelectConfirmMsg+='You will not be able to proceed with Sales Appointment/ Quoting as the selected Customer/ Site is a P1 Reinstatement';
                            requireSelectConfirmMsg+='<p>Please warm transfer the customer to P1 '+P1TransferCallPhoneNumber+'</p>';
                            requireSelectConfirmMsg +='<p>The account is currently <span style="font-weight:bold;color:red">'+getCustomerStatus(selectedResultCustomer.CustomerStatus)+'</span> with past due balance of $'+pastdue.SetScale(2)+'</p>';
                            requireSelectConfirmMsg +='<p>Disconnected on :'+getDiscoDate(selectedResultCustomer)+'</p>';                               
                            acc.MMBCustomerResponse__c = '['+JSON.serialize(selectedResultCustomer)+']';
                            acc.MMBSiteResponse__c = JSON.serialize(s);
                        }else if(selectedResultCustomer.DiscoCount >= 2){
                            requireSelectConfirmMsg += '<p>The customer has been Disconnected two or more times due to non-payment and will now have to pay 3 months of service upfront at point of sale.</p>';
                        } 
                        //END HRM 5925
                    }
                    if(checkDIYSite(s) || (selectedResultCustomer!= null && selectedResultCustomer.billingsystem == 'P1MMB' && s.ResaleEligible == 'ELIGIBLE' && s.ServiceActive == 'IN')){
                        acc.MMBCustomerResponse__c='['+JSON.serialize(selectedResultCustomer)+']';
                        acc.MMBSiteResponse__c = JSON.serialize(s);
                        acc.Previous_Billing_Site_Number__c='';
                        acc.previousBillingCustomerNumber__c='';
                        requireSelectConfirmMsg +='You will not be able to proceed with Sales Appointment/ Quoting as the selected Customer/Site is a P1 Add-on/Conversion';
                        requireSelectConfirmMsg +='<p>Please warm transfer the customer to P1 '+P1TransferCallPhoneNumber+'</p>';
                    }
                    
                    //Added for HRM-9466 
                    String result = MMBCustomerSiteSearchHelper.getChangeNotification(s);
                    if(String.isNotBlank(result)){
                        requireSelectConfirmMsg += result;
                    }
                }
                //HRM-6887 Past Due Relocations -- Mounika Anna
                if((mmbLookupType == LookupSelect.CUSTOMER_SITE || mmbLookupType == LookupSelect.CUSTOMER) && selectedResultCustomer!= null && (selectedResultCustomer.ReallyPastDueAmount + selectedResultCustomer.WriteOffAmount) > 0 && acc.MMB_Past_Due_Balance__c == 0){
                    acc.MMB_Past_Due_Balance__c = selectedResultCustomer.ReallyPastDueAmount + selectedResultCustomer.WriteOffAmount;
                }
                if(acc.MMB_Past_Due_Balance__c > 0){
                    requireSelectConfirmMsg += '<p>You will be required to collect an amount of <span style="font-weight:bold;color:red">$'+acc.MMB_Past_Due_Balance__c.format()+'</span> upfront from this customer.</p>'; 
                }
            }else{
                ADTApplicationMonitor.log (strAppName, 'ADTPartnerMMBLookupSite::updateAccountMMB - Unsupported selection type '+mmbLookupType, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
            }
            
            acc.MMBLookup__c = true;
            acc.LastMMBLookupRun__c = system.now(); // Added by Prashant for HRM 5925
            accUpdatedFlag = true;
            return acc;
        }catch (Exception ex){
            system.debug('@@Exception Occured:'+ex);
            ADTApplicationMonitor.log(ex, 'ADTPartnerMMBLookupSite', 'updateAccountMMB', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
            return null;
        }
    }

    public Boolean checkDIYSite(MMBCustomerSiteSearchApi.SiteInfoResponse selectedResultSite){
        try{
            return (selectedResultSite != null && selectedResultSite.billingSystems != null && selectedResultSite.billingSystems.contains('P1MMB') && String.isNotBlank(selectedResultSite.CS_NO) && selectedResultSite.CS_NO.trim().substring(0,2) == 'DY');
        }catch (Exception ex){
            ADTApplicationMonitor.log(ex, 'ADTPartnerMMBLookupSite', 'checkDIYSite', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        }
        return false;
    }
    
    public List<MMBCustomerSiteSearchApi.SiteInfoResponse> MMBLookupSiteInformation_Other (string siteId){
        try{
            isAccountUpdate = false;
            // create Site info Request
            MMBCustomerSiteSearchApi.SiteInfoRequest siteInfoReq = new MMBCustomerSiteSearchApi.SiteInfoRequest();
            siteInfoReq.FirstName   = '';
            siteInfoReq.LastName    = '';
            siteInfoReq.Address1    = '';
            siteInfoReq.City        = '';
            siteInfoReq.State       = '';
            siteInfoReq.ZipCode     = '';
            siteInfoReq.SiteNo      = siteId;
            MMBCustomerSiteSearchApi.LookupSiteResponse siteResponse = MMBCustomerSiteSearchApi.LookupSiteForPartner(siteInfoReq);
            List<MMBCustomerSiteSearchApi.SiteInfoResponse> selectedResultSiteList = new List<MMBCustomerSiteSearchApi.SiteInfoResponse>();
            // if Success Response
            if (siteResponse !=null && siteResponse.ResultSummaryMessage.equalsIgnoreCase('Success')){
                // do we need to add index for duplicate sites ??
                // get the SiteInfoResponse
                for (MMBCustomerSiteSearchApi.SiteInfoResponse sResponse : siteResponse.siteInfo){
                     selectedResultSiteList.add(sResponse);
                }
            }
            return selectedResultSiteList;
        }catch (Exception ex){
            system.debug('@@Exception Occured: '+ex.getMessage());
            ADTApplicationMonitor.log(ex, 'ADTPartnerMMBLookupSite', 'MMBLookupSiteInformation', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            return null;
        }
    }

    //HRM-6887 Past Due on Relocations ----- Mounika Anna
    //Customer Information added as part of HRM-6887
    public MMBCustomerSiteSearchApi.CustomerInfoResponse MMBCustomerInformation_Other (string custId,String billingsystemstr){
        return MMBCustomerInformation_Other(custId,billingsystemstr,null);
    }

    public MMBCustomerSiteSearchApi.CustomerInfoResponse MMBCustomerInformation_Other (string custId,String billingsystemstr,List<MMBCustomerSiteSearchApi.SiteInfoResponse> lstSiteInformation){
        try{
           set<String> BillingSysSet = new set<String>();
           BillingSysSet.add(billingsystemstr);
           MMBCustomerSiteSearchApi.CustomerInfoResponse selectedResultCustomer = new MMBCustomerSiteSearchApi.CustomerInfoResponse ();
           MMBCustomerSiteSearchApi.LookupCustomerResponse customerResponse = mmbCustomerSitePartnerController.getCustomers(custId, BillingSysSet);
            // if Success Response
            if (customerResponse !=null && customerResponse.ResultSummaryMessage.equalsIgnoreCase('Success')){
                // get the CustomerInfoResponse
                selectedResultCustomer = processCustomerResponse(lstSiteInformation,customerResponse);
            }else{
                string errMessage = '';
                if(customerResponse != null){
                    if (customerResponse.ResultSummaryMessage == 'NOTFOUND'){
                        system.debug( 'No customer records were found.');
                        errMessage = 'ADTPartnerMMBLookupSite::MMBLookupCustomerInformation - ' + custId + ': No customer records were found.';
                    }else{
                        system.debug('Error while searching for customer information. '+customerResponse.ResultSummaryMessage);
                        errMessage = 'ADTPartnerMMBLookupSite::MMBLookupCustomerInformation - ' +custId + 'Error while searching for customer information. '+customerResponse.ResultSummaryMessage;
                    }
                }else{
                    system.debug('Unable to reach MMB.');
                    errMessage = 'ADTPartnerMMBLookupSite::MMBLookupCustomerInformation - Unable to reach MMB.';
                }
                if (string.isNotBlank(errMessage)){
                    ADTApplicationMonitor.log (strAppName, errMessage, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                }
            }
            system.debug('@@selectedResultCustomer_other: '+selectedResultCustomer);  
            return selectedResultCustomer;
        }catch (Exception ex){
            system.debug('@@Exception Occured: '+ex.getMessage());
            ADTApplicationMonitor.log(ex, 'ADTPartnerMMBLookupSite', 'MMBLookupCustomerInformation_Other', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            return null;
        }
    }
    // End HRM-6887 
}