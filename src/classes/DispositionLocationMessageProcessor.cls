/************************************* MODIFICATION LOG ********************************************************************************************
* DispositionLocationMessageProcessor
*
* DESCRIPTION : Extends LocationDataMessageProcessor and implements the interface specific to the disposition history message.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner                  10/14/2011              - Original Version
*
*                                                   
*/
public class DispositionLocationMessageProcessor extends LocationDataMessageProcessor {
    
    private List <disposition__c> dispositionList {get;set;}
    private List <Account> accountList {get;set;}
    private Map <Id, Account> accountMap {get;set;}
    
    private static final String TIME_ZONE = '-05:00';
    
    private static final String CLASS_NAME = 'DispositionLocationMessageProcessor';
    
    public override String getName() {
        return CLASS_NAME;
    }
    
    public override Boolean isValidInput(String userId, String userIdForTeam, String searchDate) {
        
        // the parent of this class handles much of the validation 
        Boolean validInput = super.isValidInput(userId, userIdForTeam, searchDate);
        if (validInput) {
            // make sure there were some disposition updates on the date in question
            String SOQLstatement = 'select id, name, AccountID__c, DispositionDate__c, DispositionType__c from disposition__c where AccountID__c != null and createdById=\'' + String.escapeSingleQuotes(userId);
            
            // Format the date as required, appending the TIME_ZONE literal
            // This approach is necessary since the date format mask 'yyyy-MM-dd\'T\'HH:mm:ssZ' returns 
            // a value 2011-01-01T00:00:00-0500 whereas the required format is 2011-01-01T00:00:00-05:00
            Datetime dateTimeParameter = Datetime.newInstance(dateParameter, Time.newInstance(0, 0, 0, 0));
            SOQLstatement += '\' and DispositionDate__c >= ' + dateTimeParameter.format('yyyy-MM-dd\'T\'HH:mm:ss') + TIME_ZONE;
            SOQLstatement += ' and DispositionDate__c < ' + dateTimeParameter.addDays(1).format('yyyy-MM-dd\'T\'HH:mm:ss') + TIME_ZONE;
            SOQLstatement += ' order by DispositionDate__c asc';
            
            System.debug('SOQL statement: ' + SOQLstatement);
            dispositionList = database.query(SOQLstatement);
                
            Set <Id> accountIdsSet = new Set <Id>();
            
            for (disposition__c disp: dispositionList) {
                
                if (disp.AccountID__c != null) {
                    accountIdsSet.add(disp.AccountID__c);
                }
            }
            
            if (dispositionList.isEmpty()){
                validInput = false;
                List<Id> userIdList = new List<Id>();
                userIdList.add(userId);
                inferUserNames(userIdList);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No disposition updates by ' + getUserName(userId) + ' occurred on '+ searchDate + '.'));
            }
            
            if (validInput) {
                // find associated accounts
                accountList = [select name, id, SiteStreet__c,  Latitude__c, Longitude__c, AddressID__c from Account
                                            where id in:accountIdsSet];     
        
                accountMap = new Map <Id, Account>();
                for (Account acct: accountList){    
                    accountMap.put(acct.id, acct);
                }
            }
         
        }
        return validInput;
    }
    
    public override void prepareForMapping(List<LocationItem> itemsList) {
        
        mapData = '';
        decimal lat ;
        decimal longX;
        List <LocationDataMessageProcessor.LocationData> tableData = new List <LocationDataMessageProcessor.LocationData>();
        integer m=1;

        for (disposition__c  disp: dispositionList){
            
            
            // Need to compare the date time on the disposition to the date time in itemsList
            // When find the closest breadcrumb timewise (assume it can be before or after the disposition), use that
            // latitude and longitude to construct knockLoc
            
            // set default values for latitude, longitude and the difference between the time stamps
            String latitude = '0';
            String longitude = '0';
            Long timeDifference = 2147483648L;
            
            for (LocationItem locItem: itemsList) {
                // as long as the location time stamp is less than the disposition time stamp
                if (locItem.pDateTime <= disp.DispositionDate__c) {
                    // set the latitude and longitude and calculate the difference between the time stamps
                    latitude = locItem.pLatitude;
                    longitude = locItem.pLongitude;
                    timeDifference = Math.abs(disp.DispositionDate__c.getTime() - locItem.pDateTime.getTime());
                }
                // otherwise, when the location time stamp is greater than the disposition time stamp
                else {
                    // calculate the time difference
                    Long firstTimeDifferenceAfter = Math.abs(disp.DispositionDate__c.getTime() - locItem.pDateTime.getTime());
                    // and if it is closer
                    if (firstTimeDifferenceAfter < timeDifference) {
                        // use its latitude and longitude
                        latitude = locItem.pLatitude;
                        longitude = locItem.pLongitude;
                        // and break out of the loop to skip further processing
                        break;
                    }
                }
                    
            }
                    
            LocationItem knockLoc = new LocationItem(disp, latitude, longitude);
            System.debug('AccountID = ' + disp.AccountID__c);   
            LocationItem acctLoc = new LocationItem(disp, accountMap.get(disp.AccountID__c));
            LocationDataMessageProcessor.LocationItemPair knockAdd = new LocationDataMessageProcessor.LocationItemPair(knockLoc, acctLoc);
            LocationDataMessageProcessor.LocationData  tab = new LocationDataMessageProcessor.LocationData();
            boolean isValid=true;
            system.debug('knockLoc~~~~~'+knockLoc);
            
            
            if (knockLoc.pLatitude==null || knockLoc.pLongitude==null ||acctLoc.pLatitude==null || acctLoc.pLongitude==null){
                System.debug('$$$$ one of the lat/long values is null');
                isValid=false;
            }
            if (isValid){
                //Knock
                mapData += 'k$'+ knockLoc.pLatitude + '$' +
                    knockLoc.pLongitude +'$'+knockLoc.name+'$'+knockLoc.sRecordId+'$'+knockLoc.sRecordText +'$'+knockLoc.sMapLinkText+'$'+knockLoc.pDateTime.format() +'$'+knockLoc.primaryColor+'$'+m+'@';
                //Address
                mapData += 'a$'+ acctLoc.pLatitude +'$'+
                    acctLoc.pLongitude +'$'+acctLoc.name+'$'+acctLoc.sRecordId+'$'+acctLoc.sRecordText +'$'+acctLoc.sMapLinkText+'$'+acctLoc.pDateTime.format()+'$'+acctLoc.primaryColor+'$'+m+'@';
        
                tab.knockList.add(knockAdd);  //To display proximity exceptions only
                tab.count=m;
                tableData.add(tab);
                m++;
            }
             
        }
        locationDataList = tableData;
        System.debug(tableData);
        System.debug(mapData);
        
    }
    
}