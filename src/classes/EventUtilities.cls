/************************************* MODIFICATION LOG ********************************************************************************************
* EventUtilities
*
* DESCRIPTION : Utility methods for event object
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					2/10/2012				- Original Version
*
*													
*/

public with sharing class EventUtilities {
	
	// sometimes used as a default value
	public static final String TIME_ZONE_NEW_YORK = 'America/New_York';
	
	// time zones that do not observe daylight savings time
	private static final String TIME_ZONE_PUERTO_RICO = 'America/Puerto_Rico';
	private static final String TIME_ZONE_PHOENIX = 'America/Phoenix';
	private static final String TIME_ZONE_HONOLULU = 'Pacific/Honolulu';
	
	public static final Set<String> NO_DST_TIMEZONE_SID_KEYS = new Set<String> {TIME_ZONE_PUERTO_RICO,
																					TIME_ZONE_PHOENIX,
																					TIME_ZONE_HONOLULU};
    
    
    public static Boolean validatePostalCode(String zip) {
    	
    	Pattern us = Pattern.compile('^\\d{5,5}$');
    	Pattern usaddon = Pattern.compile('^\\d{5,5}-\\d{4,4}$');
    	Pattern ca = Pattern.compile('^[a-zA-Z0-9]{3,3} [a-zA-Z0-9]{3,3}$');
    	
    	Matcher mUs = us.matcher(zip);
    	Matcher mUsaddon = usaddon.matcher(zip);
    	Matcher mCa = ca.matcher(zip);
    	
    	return mUs.matches() || mUsaddon.matches() || mCa.matches();
    }	
    
    public static String deriveUserTimeZoneOffsetForDate(Datetime aDate, String timeZoneSidKey) {
    
    	
    	String offsetNowForTimeZone = deriveTimeZoneOffset(timeZoneSidKey);
    	
    	// assume return value is the current offset for the time zone
    	String offsetToReturn = offsetNowForTimeZone;
    	
    	// as long as the time zone observes daylight savings
    	if (! NO_DST_TIMEZONE_SID_KEYS.contains(timeZoneSidKey) ) {
    		
    		// the offset at the input date may be an hour ahead or an hour behind what it is now
    		
    		// so need to look at the current user's offset for the input date 
    		String offsetAtDate = aDate.format('Z');
    		// and the current user's offset right now
    		String offsetNow = Datetime.now().format('Z');
    		
    		// extract the first character of the offset at the date since will need it later
    		String firstChar = offsetAtDate.substring(0, 1);
    		// then extract the last four digits of both of the current user's offsets
    		String unsignedOffsetAtDate = offsetAtDate.substring(1);
    		String unsignedOffsetNow = offsetNow.substring(1);
    		// calculate the difference; expect this to be 100 or -100 (1 hour ahead or 1 hour behind)
    		Integer difference = Integer.valueOf(unsignedOffsetAtDate) - Integer.valueOf(unsignedOffsetNow);
    		
    		
    		// Now add the difference to current offset for the time zone
    		String unsignedOffsetNowForTimeZone = offsetNowForTimeZone.substring(1);
    		Integer derivedOffset = Integer.valueOf(unsignedOffsetNowForTimeZone) + difference;
    		// Must prepare the offset in its final format: signXXYY where 
    		//         sign is - or + 
    		//         XX is hours
    		//         YY is minutes
    		if (String.valueOf(derivedOffset).length() != 4) {
    			offsetToReturn = firstChar + '0' + derivedOffset;
    		}  else {
    			offsetToReturn = firstChar + derivedOffset;
    		}
    			
    	}
    	
    	return offsetToReturn;
    }
    
    public static String deriveTimeZoneOffset(String timeZoneSidKey) {
		
		String returnValue = '';
		
		Schema.DescribeFieldResult f = User.TimeZoneSidKey.getDescribe();
                
      	List<Schema.PicklistEntry> picklist = f.getPicklistValues();
      	      
      	for(Schema.PicklistEntry ple: picklist){	
      		      		
			if (ple.getValue() == timeZoneSidKey) {
				
				returnValue = ple.getLabel().substring( ple.getLabel().indexOf('GMT')+3, ple.getLabel().indexOf(')') );
				break;
				
			}
		}
		return returnValue.replace(':', '');
    }	
    
    public static Boolean shouldSetTaskCodeForAccount(String taskCode, String accountType) {
    	
    	Boolean returnVal = true;
    	if (taskCode == IntegrationConstants.TASK_CODE_RIF_VISIT && accountType <> IntegrationConstants.TYPE_RIF) {
    		returnVal = false;
    	}
    	return returnVal;
    	
    }
    
}