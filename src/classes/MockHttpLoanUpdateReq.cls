@isTest
global class MockHttpLoanUpdateReq implements HttpCalloutMock {
    // Implement this interface method
  
    
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method. 
      //  System.assertEquals('http://endpointurl', req.getEndpoint());
       // System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        //res.setBody('{"example":"test"}');
      
        
        
         String Resp =  '{' 
            //+' "citizensUpdateAppResponse": { '
            +'  "responseId": "ReqID",'
            +'  "responseDttm": "2016-12-12T12:22:32Z",'
            +'  "requestId": "a",'
            +'  "loanAppId": "a",'
            +'  "clientDef1": "a",'
            +'  "clientDef2": "a",'
            +'  "clientDef3": "a",'
            +'  "clientDef4": "a",'
            +'  "clientDef5": "a",'
            +'  "orderNbr": "a",'
            +'  "decisionInformation": {'
            +'      "decisionStatus": "APPROVED",'
            +'      "loanStatus": "APPROVED",'
            +'      "decisionReasonCd": "a",'
            +'      "decisionReasonDesc": "a",'
            +'      "retryAllowed": true'
            +'  },'
            +' "loanDetails": {'
            +'      "financePlanType": "a",'
            +'      "term": 1,'
            +'      "interestRate": 1.1,'
            +'      "amtFinanced": 1.1,'
            +'      "salesTaxAmt": 1.1,'
            +'      "initialPaymentAmt": 1.1,'
            +'      "installmentAmt": 1.1,'
            +'      "purchasePriceAmt": 1.1,'
            +'      "shippingAmt": 1.1,'
            +'      "otherFeeAmt": 1.1'
            +'  }'
            +'}';
        //+'}';

        /*
        String Resp =  '{'
	    +' "citizensUpdateAppResponse": {'
		+' "responseId": "a",'
		+' "responseDttm": "2016-12-12T12:22:32Z",'
		+' "requestId": "a",'
		+' "loanAppId": "a",'
		+' "decisionInformation": {'
		+'	"decisionStatus": "APPROVED" '
		+' }'
        +'}, '
        +' "loanDetails": {'
        +'      "term": 1,'
        +'  }'
         +'}';
        */
        res.setBody(Resp);
        res.setStatusCode(200);
        return res;
    }
}