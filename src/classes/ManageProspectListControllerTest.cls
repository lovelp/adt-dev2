/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ManageProspectListControllerTest {
	
	public class testBaseProspectItem extends ManageProspectListController.ProspectItem{
		public testBaseProspectItem(StreetSheetItem__c s){
			super(s);
		}
	}
	
    static testMethod void testGetItems() {
        StreetSheet__c ss = buildStreetSheet();
        
        test.setCurrentPage(Page.ManageProspectList);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ss);
        ManageProspectListController pic;
        
        test.startTest();
        	pic = new ManageProspectListController(sc);
        	ApexPages.currentPage().getParameters().put('displayMessage','true');
        	PageReference pr = pic.displayPageMessage();
        	pic.doCancel();
        test.stopTest();
        
        Boolean hasLead = false;
        Boolean hasAccount = false;
        // Validate we have at least one lead and one account after initialization
        for(ManageProspectListController.ProspectItem pi:pic.StreetSheetResultList){
        	if(hasLead && hasAccount) break;
        	if(pi instanceOf ManageProspectListController.LeadProspectItem && !hasLead){
        		hasLead = true;
        		ManageProspectListController.LeadProspectItem lpi 	= (ManageProspectListController.LeadProspectItem)pi;
        		String NameStr 			= lpi.getName(); 
        		String IdStr 			= lpi.getId();
        		String SiteStreetStr 	= lpi.getSiteStreet();
        		String SiteCityStr 		= lpi.getSiteCity();
        		String SitePostalCodeStr = lpi.getSitePostalCode();
        		String SitePostalCodeAddOnStr = lpi.getSitePostalCodeAddOn();
        		String PhoneStr 		= lpi.getPhone();
        		String DataSourceStr 	= lpi.getDataSource();
        		String DisconnectReasonStr = lpi.getDisconnectReason();
        		String DispositionCodeStr = lpi.getDispositionCode();
        		String TypeStr 			= lpi.getType();
        	}
        	if(pi instanceOf ManageProspectListController.AccountProspectItem && !hasAccount){
        		hasAccount = true;
        		ManageProspectListController.AccountProspectItem lpi 	= (ManageProspectListController.AccountProspectItem)pi;
        		String NameStr 			= lpi.getName(); 
        		String IdStr 			= lpi.getId();
        		String SiteStreetStr 	= lpi.getSiteStreet();
        		String SiteCityStr 		= lpi.getSiteCity();
        		String SitePostalCodeStr = lpi.getSitePostalCode();
        		String SitePostalCodeAddOnStr = lpi.getSitePostalCodeAddOn();
        		String PhoneStr 		= lpi.getPhone();
        		String DataSourceStr 	= lpi.getDataSource();
        		String DisconnectReasonStr = lpi.getDisconnectReason();
        		String DispositionCodeStr = lpi.getDispositionCode();
        		String TypeStr 			= lpi.getType();
        	}
        }
        
        system.assert(hasLead, 'Lead is missing from initialization block when creating SteetSheetItems');
        system.assert(hasAccount, 'Account is missing from initialization block when creating SteetSheetItems');
        
    }
    
    static testMethod void testDispAccount() {
        StreetSheet__c ss = buildStreetSheet();
        
        test.setCurrentPage(Page.ManageProspectList);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ss);
        ManageProspectListController pic;
        
        Account a;
        
        String newDispCode = 'FAKE DISPOSITION';
        
        test.startTest();
        	pic = new ManageProspectListController(sc);
        	a = pic.accounts[0];
        	pic.dispId = a.Id;
        	pic.StreetSheetItemDisp();
        	pic.dispAccount.DispositionCode__c = newDispCode;
        	pic.saveDisp();
        test.stopTest();
    	
    	Account aConfirm = [Select Id, DispositionCode__c from Account where Id = :a.Id];
    	system.assertEquals(newDispCode, aConfirm.DispositionCode__c);
    }
    
    static testMethod void testDispLead() {
        StreetSheet__c ss = buildStreetSheet();
        
        test.setCurrentPage(Page.ManageProspectList);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ss);
        ManageProspectListController pic;
        
        Lead l;
        
        String newDispCode = 'FAKE DISPOSITION';
        
        test.startTest();
        	pic = new ManageProspectListController(sc);
        	l = pic.leads[0];
        	pic.dispId = l.Id;
        	pic.StreetSheetItemDisp();        	
        	pic.dispLead.DispositionCode__c = newDispCode;
        	pic.saveDisp();
        test.stopTest();
    	
    	Lead lConfirm = [Select Id, DispositionCode__c from Lead where Id = :l.Id];
    	system.assertEquals(newDispCode, lConfirm.DispositionCode__c);
    }
    
    static testMethod void testDeleteSSItem() {
    	StreetSheet__c ss = buildStreetSheet();
        
        test.setCurrentPage(Page.ManageProspectList);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ss);
        ManageProspectListController pic;
        
        for(StreetSheetItem__c ssi: [Select Id from StreetSheetItem__c where StreetSheet__c = :ss.Id]){
        	testBaseProspectItem testBaseObj = new testBaseProspectItem(ssi); 
    		String NameStr 			= testBaseObj.getName(); 
    		String IdStr 			= testBaseObj.getId();
    		String SiteStreetStr 	= testBaseObj.getSiteStreet();
    		String SiteCityStr 		= testBaseObj.getSiteCity();
    		String SitePostalCodeStr = testBaseObj.getSitePostalCode();
    		String SitePostalCodeAddOnStr = testBaseObj.getSitePostalCodeAddOn();
    		String PhoneStr 		= testBaseObj.getPhone();
    		String DataSourceStr 	= testBaseObj.getDataSource();
    		String DisconnectReasonStr = testBaseObj.getDisconnectReason();
    		String DispositionCodeStr = testBaseObj.getDispositionCode();
    		String TypeStr 			= testBaseObj.getType();
    		break;
        }
        
        test.startTest();
        	pic = new ManageProspectListController(sc);
        	pic.deleteItemId = pic.accounts[0].Id;
        	pic.deleteSSItem();
        	pic.deleteItemId = pic.leads[0].Id;
        	pic.deleteSSItem();
        test.stopTest();
        
        list<StreetSheetItem__c> ssitems = new list<StreetSheetItem__c>([Select Id from StreetSheetItem__c where StreetSheet__c = :ss.Id]);
        
        system.assertEquals(0, ssitems.size());
    }
    
    private static StreetSheet__c buildStreetSheet() {
    	StreetSheet__c ss;
    	String ssid;
    	Account a;
    	Lead l;
    	User u;
    	User current = [Select Id from User where Id = :UserInfo.getUserId()];
    	system.runas(current) {
    		u = TestHelperClass.createSalesRepUser();
    		a = TestHelperClass.createAccountData();
    		l = TestHelperClass.createLead(u);
    		ssid = StreetSheetManager.create(new set<String>{a.Id, l.Id});
    	}
    	ss = [Select Id from StreetSheet__c where Id = :ssid];
    	return ss;
    }
}