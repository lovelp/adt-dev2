@isTest
private class TaskManagerTest {
	
	static testMethod void testCreateDirectMailExportTask() {
		
		 
        User creator = TestHelperClass.createSalesRepUser();
             
        User manager = TestHelperClass.createManagerUser();
        
        StreetSheet__c ss;
        System.runAs(creator) {    
        	Account a = TestHelperClass.createAccountData();
		
			ss = new StreetSheet__c();
			ss.Name = 'Unit Test Street Sheet';
		
			insert ss;
        }	
            
        Integer initialTaskCount = database.Countquery('select count() from Task where OwnerId = \'' + manager.Id + '\'');
            
      	Test.startTest();
      	
      	System.runAs(creator) {
      		TaskManager.createDirectMailExportTask(creator, manager.Id, ss.Id);
      	}
      	Integer newTaskCount = database.Countquery('select count() from Task where OwnerId = \'' + manager.Id + '\'');
      	
      	System.assertEquals(initialTaskCount+1, newTaskCount, 'Expect the number of tasks for the manager to increase by 1');
      	
      	
      	Test.stopTest();
		
	}

}