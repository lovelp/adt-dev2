/************************************* MODIFICATION LOG ********************************************************************************************
* OptOutContactsAppplicationController
*
* DESCRIPTION : Class contains logic used OptoutContacts Visual force page 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Nitin gottiparthi          7/02/2019           - Original Version
*/

public class OptOutContactsAppplicationController {
    public List<OptOutPhonesWrapper> optOutPhonesWrapList {get; set;}
    public Account acc {get; set;}
    public Id accid {get;set;}
    public Boolean showStatus {get; set;}
    public Boolean enableReturnType {get; set;}
    public Map<String, String> optoutResMap {get; set;}
    public List<String> optOutList = new List<String>();
    
    public OptOutContactsAppplicationController(ApexPages.StandardController ctrl) {
        optOutPhonesWrapList = new List<OptOutPhonesWrapper>();
        optoutResMap = new Map<String, String>();
        Id recId = ctrl.getId();
        String retType = (ApexPages.currentPage().getParameters().containsKey('retType')) ? ApexPages.currentPage().getParameters().get('retType') : null;
        enableReturnType = (retType != null && retType.toLowerCase().equals('enabled')) ? true : false;
        acc = [Select Id, Name, Phone, PhoneNumber4__c, PhoneNumber2__c, PhoneNumber3__c, TelemarAccountNumber__c,MMBCustomerNumber__c,DNIS__c From Account Where id =: recId];
        showStatus = false;
        //the check is to make sure the phone number is not empty and format and add the values to the optOutPhonesWrap object
        if(String.isNotBlank(acc.Phone)){
            optOutPhonesWrapList.add(new OptOutPhonesWrapper(acc.Phone, false, 'Primary Phone', '', recId));
        }
        if(String.isNotBlank(acc.PhoneNumber4__c)){
          string FormattedPhone = Utilities.formatPhoneNumber(acc.PhoneNumber4__c);
            optOutPhonesWrapList.add(new OptOutPhonesWrapper(FormattedPhone, false, 'Spouse Phone', '', recId));
        }
        if(String.isNotBlank(acc.PhoneNumber2__c)){
            string FormattedPhone = Utilities.formatPhoneNumber(acc.PhoneNumber2__c);
            optOutPhonesWrapList.add(new OptOutPhonesWrapper(FormattedPhone, false, 'Cell Phone','', recId));
        }
        if(String.isNotBlank(acc.PhoneNumber3__c)){
            string FormattedPhone = Utilities.formatPhoneNumber(acc.PhoneNumber3__c);
            optOutPhonesWrapList.add(new OptOutPhonesWrapper(FormattedPhone, false, 'Alt Phone', '', recId));
        }
    }
    
    // this function is created to create the map object and the get the list of phone numbers checked and unchecked
    public void getOptOutMap() {
        Map<String, OptOutPhonesWrapper> optOutMap = new Map<String, OptOutPhonesWrapper>();
        Map<String, Boolean> failSuccessNumbersMap = new Map<String, Boolean>();
        showStatus = true;
        if(optOutPhonesWrapList.size() > 0){
            for(OptOutPhonesWrapper o : optOutPhonesWrapList) {
                o.message = '';
                o.status = '';
                if(o.optOut){
                    optOutMap.put(o.phoneNumber, o);
                }
            }
        }
        
        // First clear out message and status and then validate
        if(optoutResMap.isEmpty()){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'None of the phone numbers are selected'));
            showStatus = false;
            return;
        }
        // Added by VS to do a callout
        failSuccessNumbersMap = ADTEWCConsentAPI.updateADTEWCConsentAPI(optOutMap, 'updateConsents', 1, acc);
        
        if(!optOutMap.isEmpty() && !failSuccessNumbersMap.isEmpty()){
            for(OptOutPhonesWrapper o : optOutPhonesWrapList) {
                if(failSuccessNumbersMap.containsKey(o.phoneNumber)) {
                    if(failSuccessNumbersMap.get(o.phoneNumber)){
                        o.message = 'Successfully Opted out';
                        o.status = 'success';
                    }else {
                        o.message = 'Error: Cannot Opt Out. Please try again.';
                        o.status = 'error';
                    }
                } 
            }
        }
    }
    
    //this function might be used in future to show the more details of the particular checkbox phone number clicked and it display the datatable in the below screen
    public void showDetails() {
        String phoneNo = Apexpages.currentPage().getParameters().get('phoneNo');
        String index = Apexpages.currentPage().getParameters().get('index');
        String checked = Apexpages.currentPage().getParameters().get('checked');
        Boolean singlePhoneNumber = true;
        for(Integer i=0 ; i< optOutPhonesWrapList.size() ; i++){
            if(phoneNo.toLowerCase() == 'all') {
                optoutResMap.put(String.valueOf(i+1),optOutPhonesWrapList[i].phoneNumber);
                singlePhoneNumber = false;
            }else if(phoneNo.toLowerCase() == 'none') {
                optoutResMap.clear();
                singlePhoneNumber = false;
                break;
            }
        }
        
        if(singlePhoneNumber){
            if(String.isNotBlank(checked) && checked.containsIgnoreCase('true')){
                optoutResMap.put(index,phoneNo);
            }else if(optoutResMap.containsKey(index)){
                optoutResMap.remove(index);
            }
        }
    }
    
    // Class to hold phoneNumber and its related Checkbox value
    public class OptOutPhonesWrapper {
        public String phoneNumber {get; set;}
        public String status {get; set;}
        public String fieldLabel {get; set;}
        public String Message{get;set;}
        public Boolean optOut {get; set;}
        public Id accid {get; set;}
        public OptOutPhonesWrapper(String phoneNumber, Boolean optOut, String fieldLabel, String msg, Id accId) {
            this.phoneNumber = phoneNumber;
            this.optOut = optOut;
            this.fieldLabel = fieldLabel;
            this.message = msg;
            this.accId = accId;
        }
    }
}