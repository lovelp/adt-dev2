global class CallSendtoDialer{
    webService static Id sendtoDialer(String campaignId) {
        DialerListBatchProcessor dlbp=new DialerListBatchProcessor(campaignId);
        CampaignDefaults__c cd = CampaignDefaults__c.getorgDefaults();
        database.executebatch(dlbp,integer.valueof(cd.Dialer_List_Batch_Size__c));
        return null;
    }
}