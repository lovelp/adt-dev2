@isTest
public class ResendFailedRequestsToTelemarTest{


    public static void createTestData(){
        
        
        
        
        //Custom settings
        ResaleGlobalVariables__c rgv=new ResaleGlobalVariables__c();
        rgv.name='AsyncAccountsBatchSize';
        rgv.value__c='5';
        insert rgv;
        
        rgv=new ResaleGlobalVariables__c();
        rgv.name='FilterRequestQueErrorstoReprocess';
        rgv.value__c='Scheduled Backup time;Parser was expecting element';
        insert rgv;
        
        rgv=new ResaleGlobalVariables__c();
        rgv.name='RequestQueueItemstoProcess';
        rgv.value__c='3000';
        insert rgv;
        
        rgv=new ResaleGlobalVariables__c();
        rgv.name='RQProcessingEmailRecepients';
        rgv.value__c='test@test.com';
        insert rgv;
        
        rgv=new ResaleGlobalVariables__c();
        rgv.name='TransactionTypesToReprocess';
        rgv.value__c='SetAccountDispositionOnly;SetAccountAccount;SetAccountAccount-DSC';
        insert rgv;
        
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.TelemarEndpoint__c='https://wsbeta.adt.com/salesmobilitytest8/sm/ExternalInbound/Services';
        insert is;

        
        //request queues
        RequestQueue__c rq=new RequestQueue__c();
        rq.ServiceTransactionType__c='setAppointmentNew-NSC';
        rq.RequestStatus__c='Async-Error';
        rq.ResponseTelemarNumber__c='';
        rq.ServiceMessage__c='{"TransactionType_type_info":["TransactionType","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"TransactionType":"New-NSC","TelemarAccountNumber_type_info":["TelemarAccountNumber","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"TelemarAccountNumber":"70000009","Promotion_type_info":["Promotion","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"Promotion":null,"PreSale_type_info":["PreSale","http://www.adt.com/SalesMobility/","PreSale_element","0","1","false"],"PreSale":{"State_type_info":["State","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"State":"FL","ReferredBy_type_info":["ReferredBy","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"ReferredBy":null,"QueriedSource_type_info":["QueriedSource","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"QueriedSource":"OTHER","PostalCode_type_info":["PostalCode","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"PostalCode":"334314438","PhoneNumber4_type_info":["PhoneNumber4","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"PhoneNumber4":null,"PhoneNumber3_type_info":["PhoneNumber3","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"PhoneNumber3":null,"PhoneNumber2_type_info":["PhoneNumber2","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"PhoneNumber2":null,"PhoneNumber1_type_info":["PhoneNumber1","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"PhoneNumber1":"(786) 445-6515","LastName_type_info":["LastName","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"LastName":"Test","FirstName_type_info":["FirstName","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"FirstName":"Autotelnum","field_order_type_info":["FirstName","LastName","BusinessName","AddressLine1","AddressLine2","AddressLine3","AddressLine4","City","State","PostalCode","CountryCode","PhoneNumber1","PhoneNumber2","PhoneNumber3","PhoneNumber4","Email","Comments","QueriedSource","ReferredBy","Channel"],"Email_type_info":["Email","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"Email":null,"CountryCode_type_info":["CountryCode","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"CountryCode":"US","Comments_type_info":["Comments","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"Comments":null,"City_type_info":["City","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"City":"Boca Raton","Channel_type_info":["Channel","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"Channel":"Resi Direct Sales","BusinessName_type_info":["BusinessName","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"BusinessName":null,"apex_schema_type_info":["http://www.adt.com/SalesMobility/","false","false"],"AddressLine4_type_info":["AddressLine4","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"AddressLine4":null,"AddressLine3_type_info":["AddressLine3","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"AddressLine3":null,"AddressLine2_type_info":["AddressLine2","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"AddressLine2":null,"AddressLine1_type_info":["AddressLine1","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"AddressLine1":"1501 NW 51st St"},"MessageID_type_info":["MessageID","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"MessageID":"SF-1472585585311-00530000007dy7mAAA","Lsrc_type_info":["Lsrc","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"Lsrc":null,"LeadSource_type_info":["LeadSource","http://www.adt.com/SalesMobility/","LeadSource_element","1","1","false"],"LeadSource":"User Entered","HREmployeeID_type_info":["HREmployeeID","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"HREmployeeID":"133244","field_order_type_info":["MessageID","TransactionType","TelemarAccountNumber","HREmployeeID","LeadSource","PreSale","Appointment","Dnis","Promotion","Lsrc"],"Dnis_type_info":["Dnis","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"Dnis":"8777597304","Appointment_type_info":["Appointment","http://www.adt.com/SalesMobility/","Appointment_element","1","1","false"],"Appointment":{"TaskCode_type_info":["TaskCode","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"TaskCode":"","OriginalTelemarScheduleID_type_info":["OriginalTelemarScheduleID","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"OriginalTelemarScheduleID":"","Notes_type_info":["Notes","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"Notes":"","HREmployeeID_type_info":["HREmployeeID","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"HREmployeeID":"133244","field_order_type_info":["AppointmentStartDateTime","AppointmentOffsetToGMT","Duration","TaskCode","Notes","OriginalTelemarScheduleID","HREmployeeID"],"Duration_type_info":["Duration","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"Duration":"30","AppointmentStartDateTime_type_info":["AppointmentStartDateTime","http://www.w3.org/2001/XMLSchema","dateTime","1","1","false"],"AppointmentStartDateTime":"2016-08-30T19:33:05.295Z","AppointmentOffsetToGMT_type_info":["AppointmentOffsetToGMT","http://www.w3.org/2001/XMLSchema","string","1","1","false"],"AppointmentOffsetToGMT":"-0400","apex_schema_type_info":["http://www.adt.com/SalesMobility/","false","false"]},"apex_schema_type_info":["http://www.adt.com/SalesMobility/","false","false"]}';
        insert rq;
        
        rq=new RequestQueue__c();
        rq.ServiceTransactionType__c='setAccountDispositionOnly';
        rq.RequestStatus__c='Error';
        rq.ResponseTelemarNumber__c='';
        rq.ServiceMessage__c='Web service callout failed: Unexpected element. Parser was expecting element';
        insert rq;
        
        rq=new RequestQueue__c();
        rq.ServiceTransactionType__c='setAccountAccount';
        rq.RequestStatus__c='Error';
        rq.ResponseTelemarNumber__c='';
        rq.ServiceMessage__c='Web service callout failed: Unexpected element. Parser was expecting element';
        insert rq;
        
    }
    
    public static testMethod void TestResendFailedAccountsToTelemar(){
        

        createTestData();

        test.startTest();
            Test.setMock(WebServiceMock.class, new RequestQueueWebServiceMockImpl());
            ResendFailedAccountsToTelemar  p = new ResendFailedAccountsToTelemar ();
            String sch = '0 17 11 26 10 ? 2022';
            system.schedule('Accounts', sch, p);

        test.stopTest();
    }


}