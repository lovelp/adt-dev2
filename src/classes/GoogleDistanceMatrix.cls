/************************************* MODIFICATION LOG ********************************************************************************************
* GoogleDistanceMatrix
* 
* DESCRIPTION : Invokes the Google Distance Matrix API using an HTTPRequest, processes the JSON response, 
*               creates GoogleDistanceMatrixResults structure and handles any errors during processing.  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera              2/3/2016				- Original Version
*
*													  
*													
*/


public without sharing class GoogleDistanceMatrix {

	private static final String googleBaseURL = 'https://maps.googleapis.com/maps/api/distancematrix/';
	private static final String googleOutput = 'json';
	private static final String googleClient = 'gme-adt';
	private static final String googleChannel = 'Resale';
	private static final String googlePlaceSeparator = '|';
	private static String DistanceMatrixOriginDestinationURL;
	private static List<DistanceMatrixPlace> PlaceOriginList;
	private static List<DistanceMatrixPlace> PlaceDestinationList;
	
	public class GoogleDistanceMatrixException extends Exception{}
	
	//-------------------------------------------------------------------------------------------
	//	Wrapper class to identify a place of origin/destination when requesting a distance matrix
	//-------------------------------------------------------------------------------------------
	public class DistanceMatrixPlace {
		private decimal lat;
		private decimal lng;
		private String addr;
		public String plUID; //	This place's ID	 
		public String tUID;  // Traveler ID
		public String getPlace(){
			String resVal = '';
			if( lat != null &&  lng != null){
				resVal = String.valueOf(lat) + ',' + String.valueOf(lng);
			}
			else {
				resVal = EncodingUtil.urlEncode(addr,'UTF-8');
			}
			return resVal;
		}
		public DistanceMatrixPlace(Address__c addrParam){
			lat = addrParam.Latitude__c;
			lng = addrParam.Longitude__c;
			addr = addrParam.StandardizedAddress__c;
			plUID = addrParam.Id;
		}
		public DistanceMatrixPlace(String addrParam){
			addr = addrParam;
			// our address needs to match the original place for origin/destination enconded using the same crypto algorithm
			plUID = EncodeValue(addrParam);
		}
		public DistanceMatrixPlace(String tId, Address__c addrParam){
			lat = addrParam.Latitude__c;
			lng = addrParam.Longitude__c;
			addr = addrParam.StandardizedAddress__c;
			plUID = addrParam.Id;
			// Traveler Id
			tUID = tId;
		}
	}
	
	//-------------------------------------------------------------------------------------------
	//	Apex classes used on deserialize of the JSON response from google
	//-------------------------------------------------------------------------------------------
	public class Duration {
	    public integer value { get; set; }
	    public string text { get; set; }
	}
	
	public class Distance {
	    public integer value { get; set; }
	    public string text { get; set; }
	}
	
	public class Element {
	    public string status { get; set; }
	    public Duration duration { get; set; }
	    public Distance distance { get; set; }
	}
	
	public class Row {
	    public List<Element> elements { get; set; }
	}
	
	// JSON wrapper returned by Google's Distance Matrix
	public class DistanceMatrixResponse {
	    public string status { get; set; }
	    public List<string> origin_addresses { get; set; }
	    public List<string> destination_addresses { get; set; }
	    public List<Row> rows { get; set; }
	}
	
	public class DistanceMatrixResult {
		public DistanceMatrixPlace origin;
		public DistanceMatrixPlace destination;
		public Element TravelInformation;
	}
	
	/**
	 *	Static initialization block to make sure all resources will be available when requested from static calls to this class
	 *	accross same execution context.
	 */
	static{
		PlaceOriginList = new List<DistanceMatrixPlace>();
		PlaceDestinationList = new List<DistanceMatrixPlace>();
	}
	
	public static Boolean isDistanceMatrixAvailable(){
		return !( PlaceOriginList == null || PlaceDestinationList == null || PlaceOriginList.isEmpty() || PlaceDestinationList.isEmpty() );
	}
	
	public static void clearPlaces(){
		if ( PlaceOriginList != null ) PlaceOriginList.clear();
		if ( PlaceDestinationList != null ) PlaceDestinationList.clear();
	}
	
	public static List<DistanceMatrixResult> getDistanceMatrix(){
		HttpRequest req = new HttpRequest();
		req.setEndpoint(getDistanceMatrixURL());
     	req.setMethod('GET');	
     	System.debug('Google Distance Matrix Request……: ' + req);
     	Http http = new Http();
	    HTTPResponse response = http.send(req);
	    String ResponseBody = response.getBody();
        if (!(  response.getStatusCode() == 200 || response.getStatusCode() == 201 || response.getStatusCode() == 202 )) {
            System.debug('[GoogleDistanceMatrix]::[getDistanceMatrix] Error Response Status:'+ response.getStatus() + ' Code:' + response.getStatusCode());
            throw new IntegrationException(' StatusCode='+response.getStatusCode()+' Status='+response.getStatus() + ' Message='+ResponseBody);        
        }	    
	    system.debug( ResponseBody );
	    return parseJSONResponse( ResponseBody );
	    //system.debug(GGCAddress);
	}
	
    public static List<DistanceMatrixResult> getDistanceMatrix_retask(){
		HttpRequest req = new HttpRequest();
		req.setEndpoint(getDistanceMatrixURL());
     	req.setMethod('GET');	
     	System.debug('Google Distance Matrix Request……: ' + req);
     	Http http = new Http();
	    HTTPResponse response = http.send(req);
	    String ResponseBody = response.getBody();
        if (!(  response.getStatusCode() == 200 || response.getStatusCode() == 201 || response.getStatusCode() == 202 )) {
            System.debug('[GoogleDistanceMatrix]::[getDistanceMatrix] Error Response Status:'+ response.getStatus() + ' Code:' + response.getStatusCode());
            throw new IntegrationException(' StatusCode='+response.getStatusCode()+' Status='+response.getStatus() + ' Message='+ResponseBody);        
        }	    
	    system.debug( ResponseBody );
	    return parseJSONResponse_retask( ResponseBody );
	    //system.debug(GGCAddress);
	}
	
	private static List<DistanceMatrixResult> parseJSONResponse_retask(String JSONResponse){
		List<DistanceMatrixResult> resVal = new List<DistanceMatrixResult>();
		
		// Parse JSON response
		system.debug('parsing...'+JSONResponse);		
		DistanceMatrixResponse  gDistanceMatrixResponse = (DistanceMatrixResponse) JSON.deserialize(JSONResponse, DistanceMatrixResponse.class);
		
		for(Integer i = 0; i< PlaceOriginList.size(); i++ ){
			DistanceMatrixResult dmRes = new DistanceMatrixResult(); 
			dmRes.origin = PlaceOriginList[i];
			for(Integer j = 0; j < PlaceDestinationList.size(); j++){
			    DistanceMatrixResult dmRes1 = new DistanceMatrixResult();
			    dmRes1.origin = PlaceOriginList[i];
				dmRes1.destination = PlaceDestinationList[j];
				dmRes1.TravelInformation = gDistanceMatrixResponse.rows[i].elements[j];
				resVal.add( dmRes1);
				system.debug('list'+resVal);
			}
			//system.debug('@$$ '+resVal);
		}
		return resVal;		
	}
	
	/**
	 *	Sets the array of places used as origins by the distance matrix api, accepts string types.
	 *	@mehod setDistanceMatrixOrigins
	 *	@param List <String>	Array of places in the form of string addresses (similar to normalized address value on the address object)
	 */
	public static void setDistanceMatrixOrigins(List <String> addrList){
		for( String addr :addrList ){
			PlaceOriginList.add( new DistanceMatrixPlace(addr) );
		}
	}
	
	public static void setDistanceMatrixOrigin( String TravelerId, Address__c addrParam){
		PlaceOriginList.add( new DistanceMatrixPlace( TravelerId, addrParam ) );
	}
	
	/**
	 *	Sets the array of places used as origins by the distance matrix api, accepts Address__c custom object types.
	 *	@mehod setDistanceMatrixOrigins
	 *	@param List <Address__c>	Array of places in the form of custom object address records
	 */
	public static void setDistanceMatrixOrigins(List <Address__c> addrList){
		for( Address__c addr :addrList ){
			PlaceOriginList.add( new DistanceMatrixPlace(addr) );
		}
	}

	public static void setDistanceMatrixDestinations(List <String> addrList){
		for( String addr :addrList ){
			PlaceDestinationList.add( new DistanceMatrixPlace(addr) );
		}
	}
	
	public static void setDistanceMatrixDestination( String TravelerId, Address__c addrParam){
		PlaceDestinationList.add( new DistanceMatrixPlace(TravelerId, addrParam) );
	}
	
	public static void setDistanceMatrixDestinations(List <Address__c> addrList){
		for( Address__c addr :addrList ){
			PlaceDestinationList.add( new DistanceMatrixPlace(addr) );
		}
	}
	
	private static String EncodeValue(String cryptStr){
		String resVal = '';
		resVal = EncodingUtil.urlEncode(resVal, 'UTF-8');
		Blob decodedKey = EncodingUtil.base64Decode( ResaleGlobalVariables__c.getInstance('GoogleGeoCodeDecodeKey').value__c );
		Blob mac = Crypto.generateMac('HMacSHA1',  Blob.valueOf(resVal), EncodingUtil.base64Decode('rE5EevLmL7uzpwPOkxpw1RcZCFk='));
		resVal = EncodingUtil.base64Encode(mac).replace('+', '-').replace('/', '_');
		return resVal;
	}
	
	private static String getDistanceMatrixURL(){
		// Require at least an array of size 1 for each list of origins and destinations
		if( PlaceOriginList == null || PlaceDestinationList == null || PlaceOriginList.isEmpty() || PlaceDestinationList.isEmpty() ){
			throw new GoogleDistanceMatrixException('Origins and Destinations are required in order to request a distance Matrix');
		}
		String returnURL = '';
		getDistanceMatrixOriginDestinationURL(PlaceOriginList, PlaceDestinationList);
		String signature = getGoogleSignature(PlaceOriginList, PlaceDestinationList); 
		returnURL = googleBaseURL + googleOutput + '?' + DistanceMatrixOriginDestinationURL + '&client=' + googleClient + '&channel=' + googleChannel + '&signature=' + signature;
		return returnURL;
	}
	
	private static String getDistanceMatrixOriginDestinationURL(List<DistanceMatrixPlace> origins, List<DistanceMatrixPlace> destinations){
		// Origins
		String resVal = 'origins=';
		for( DistanceMatrixPlace dbPl : origins){
			resVal += dbPl.getPlace()  + googlePlaceSeparator;
		}
		// Strip last separator
		resVal = resVal.substringBeforeLast(googlePlaceSeparator);
		// Destinations
		resVal += '&destinations=';
		for( DistanceMatrixPlace dbPl : destinations){
			resVal += dbPl.getPlace()  + googlePlaceSeparator;
		}
		// Strip last separator
		resVal = resVal.substringBeforeLast(googlePlaceSeparator);
		// Additional parameters
		resVal += '&units=imperial';
		DistanceMatrixOriginDestinationURL = resVal;
		return DistanceMatrixOriginDestinationURL;
	}
	
	private static String getGoogleSignature(List<DistanceMatrixPlace> origins, List<DistanceMatrixPlace> destinations){
		String BaseURL = '/maps/api/distancematrix/';
		String returnURL = BaseURL + googleOutput + '?' + DistanceMatrixOriginDestinationURL + '&client=' + googleClient + '&channel=' + googleChannel;
		String urlEncodedURL = EncodingUtil.urlEncode(returnURL, 'UTF-8');
		String algorithmName = 'HMacSHA1';
		//Blob decodedKey = EncodingUtil.base64Decode('rE5EevLmL7uzpwPOkxpw1RcZCFk=');
		Blob decodedKey = EncodingUtil.base64Decode( ResaleGlobalVariables__c.getInstance('GoogleGeoCodeDecodeKey').value__c );
		Blob mac = Crypto.generateMac(algorithmName,  Blob.valueOf(returnURL), EncodingUtil.base64Decode('rE5EevLmL7uzpwPOkxpw1RcZCFk='));
		String macUrl = EncodingUtil.base64Encode(mac);//EncodingUtil.urlEncode(EncodingUtil.base64Encode(mac), 'UTF-8');		
		string returnVal = macUrl.replace('+', '-').replace('/', '_');
		return String.valueOf(returnVal); 
	}
	
	private static List<DistanceMatrixResult> parseJSONResponse(String JSONResponse){
		List<DistanceMatrixResult> resVal = new List<DistanceMatrixResult>();
		
		// Parse JSON response
		system.debug('parsing...'+JSONResponse);		
		DistanceMatrixResponse  gDistanceMatrixResponse = (DistanceMatrixResponse) JSON.deserialize(JSONResponse, DistanceMatrixResponse.class);
		
		for(Integer i = 0; i< PlaceOriginList.size(); i++ ){
			DistanceMatrixResult dmRes = new DistanceMatrixResult(); 
			dmRes.origin = PlaceOriginList[i];
			for(Integer j = 0; j < PlaceDestinationList.size(); j++){
				dmRes.destination = PlaceDestinationList[j];
				dmRes.TravelInformation = gDistanceMatrixResponse.rows[i].elements[j];
				system.debug('@$$ '+dmRes);
				resVal.add( dmRes );
			}
			//system.debug('@$$ '+resVal);
		}
		return resVal;		
	}

}