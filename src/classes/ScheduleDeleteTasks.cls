global class ScheduleDeleteTasks implements Schedulable {
	global void execute(SchedulableContext SC) {
        Database.executeBatch(new DeleteTasks(),200);
    }
}