@isTest
global class ADTWebleadAPITest {
    @isTest
    static void testADTEBRAPI(){
        
        ResaleGlobalVariables__c rgvVar=new ResaleGlobalVariables__c();
        rgvVar.name='DataRecastDisableAccountTrigger';
        rgvVar.value__c='true';
        insert rgvVar;
        Test.setMock(HttpCalloutMock.class, new ADTWEBLEADSMockTest());
        
        Account acc = TestHelperClass.createAccountData();
        acc.TelemarAccountNumber__c='1234';
        acc.Phone='1234567890';
        acc.PhoneNumber2__c='12345678901';
        acc.PhoneNumber3__c='1234567890';
        acc.PhoneNumber4__c='1234567890';
        update acc;
        AuditLog__c aLog = new AuditLog__c();
        aLog.name = 'Test Audit Log';
        aLog.Account__c = acc.id;
        aLog.FormName__c = 'Test Form Name';
        aLog.Email__c = 'test@tcs.com';
        alog.PrimaryPhone__c = '1478523698';
        aLog.Type__c = 'Contact';
        aLog.LineOfBusiness__c = '1100 - Residential';
        insert aLog;
        ADTEBRAPI.sendToEBR(acc.Id,aLog.Id);
        list<AuditLog__c> aloglist = new list<AuditLog__c>();
        aloglist.add(aLog);
        list<Account> accnlist = new list<Account>();
        accnlist.add(acc);
        ADTEBRAPI.sendToEBR(aloglist,accnlist);
        
        Test.starttest();
        ADTEBRAPI.doEBRCallOut([select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c from RequestQueue__c limit 1]);
        
        Test.stoptest();
    }
    
    static testmethod void AdtOneClickAPITTest(){
        ResaleGlobalVariables__c rgvVar=new ResaleGlobalVariables__c();
        rgvVar.name='DataRecastDisableAccountTrigger';
        rgvVar.value__c='true';
        insert rgvVar;
        Test.setMock(HttpCalloutMock.class, new ADTWEBLEADSMockTest());  
        
        Account acc=TestHelperClass.createAccountData();
        acc.TelemarAccountNumber__c='1234';
        acc.Phone='1234567890';
        acc.PhoneNumber2__c='12345678901';
        acc.PhoneNumber3__c='1234567890';
        acc.PhoneNumber4__c='1234567890';
        update acc;
        AuditLog__c aLog = new AuditLog__c();
        aLog.name = 'Test Audit Log';
        aLog.Account__c = acc.id;
        aLog.FormName__c = 'Test Form Name';
        aLog.Email__c = 'test@tcs.com';
        alog.PrimaryPhone__c = '1478523698';
        aLog.Type__c = 'Contact';
        aLog.LineOfBusiness__c = '1100 - Residential';
        insert aLog;
        ADTOneClickAPI.createOneClickReqQueue(acc.Id,aLog.Id);
        Test.starttest();
        ADTOneClickAPI.doOneClickCallOut([select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c from RequestQueue__c limit 1]);
        
        Test.stoptest();
    }
    
     static testmethod void ADTCheetahMailAPITest(){
        ResaleGlobalVariables__c rgvVar=new ResaleGlobalVariables__c();
        rgvVar.name='DataRecastDisableAccountTrigger';
        rgvVar.value__c='true';
        insert rgvVar;
        Test.setMock(HttpCalloutMock.class, new ADTWEBLEADSMockTest());  
        //RequestQueue__c insertReqQue = new RequestQueue__c();
        //insert insertReqQue;
        
        Account acc=TestHelperClass.createAccountData();
        acc.TelemarAccountNumber__c='1234';
        acc.Phone='1234567890';
        acc.PhoneNumber2__c='12345678901';
        acc.PhoneNumber3__c='1234567890';
        acc.PhoneNumber4__c='1234567890';
        update acc;
        AuditLog__c aLog = new AuditLog__c();
        aLog.name = 'Test Audit Log';
        aLog.Account__c = acc.id;
        aLog.FormName__c = 'Test Form Name';
        aLog.Email__c = 'test@tcs.com';
        alog.PrimaryPhone__c = '1478523698';
        aLog.Type__c = 'Contact';
        aLog.LineOfBusiness__c = '1100 - Residential';
        insert aLog;
        ADTCheetahMailAPI cheetah = new ADTCheetahMailAPI();
        cheetah.sendToACKEmail(aLog);
        list<AuditLog__c> aloglist = new list<AuditLog__c>();
        aloglist.add(aLog);
        ADTCheetahMailAPI.sendToACKEmail(aloglist);
        Test.starttest();
        ADTCheetahMailAPI.doACKEmailCallOut([select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c from RequestQueue__c limit 1]);
        
        List<RequestQueue__c> rqlist = new List<RequestQueue__c>();
        RequestQueue__c rq = new RequestQueue__c();
            rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rq.ServiceTransactionType__c = 'sendEBRRequest';
         rq.ServiceMessage__c = '{"test":"test"}';
         rqList.add(rq);
         
        RequestQueue__c rq1 = new RequestQueue__c();
            rq1.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rq1.ServiceTransactionType__c = 'sendACKEmail'; 
         rq1.ServiceMessage__c = '{"test":"test"}';
         rqList.add(rq1);
         
         // generic rq
            RequestQueue__c rq2 = new RequestQueue__c();
            rq2.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rq2.ServiceTransactionType__c = 'sendToOneClick';
         rq2.ServiceMessage__c = '{"test":"test"}';
         rqList.add(rq2);
         
         RequestQueue__c rq3 = new RequestQueue__c();
            rq3.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rq3.ServiceTransactionType__c = 'sendEWCRequest';
         rq3.ServiceMessage__c = '{"test":"test"}';
         rqList.add(rq3);
         RequestQueue__c rq4 = new RequestQueue__c();
            rq4.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rq4.ServiceTransactionType__c = 'sendText';
         rq4.ServiceMessage__c = '{"test":"test"}';
         rqList.add(rq4);
        insert rqList; 
        ADTWebLeadsAPI.processWebleadsRequestQueue('EBR');
        ADTWebLeadsAPI.processWebleadsRequestQueue('OneClick');
        ADTWebLeadsAPI.processWebleadsRequestQueue('ACK');
        ADTWebLeadsAPI.processWebleadsRequestQueue('sendEWCRequest');
        ADTWebLeadsAPI.processWebleadsRequestQueue('sendText');
        
        
     
        Test.stoptest();
    }
    static testmethod void ADTConfiguratorCheetahMailAPITest(){
    	list<Equifax__c> elist = new list<Equifax__c>();
        elist.add(new Equifax__c(Name='Employee Condition Code', value__c='CME3'));
        elist.add(new Equifax__c(Name='Employee Risk Grade', value__c='U'));
        elist.add(new Equifax__c(Name='Default Condition Code', value__c='  CAE1'));
        elist.add(new Equifax__c(Name='Default Risk Grade', value__c='W'));
        insert elist;       
        
        list<ResaleGlobalVariables__c> resale = new list<ResaleGlobalVariables__c>();
        ResaleGlobalVariables__c rgvVar=new ResaleGlobalVariables__c();
        rgvVar.name='DataRecastDisableAccountTrigger';
        rgvVar.value__c='true';
        resale.add(rgvVar);
        resale.add(new ResaleGlobalVariables__c(name='MMBAccountNameLength', value__c='100'));
        resale.add(new ResaleGlobalVariables__c(name='MMBNameLength', value__c='100'));
        insert resale;
        
        Lead_Convert__c lc = new Lead_Convert__c();
        lc.Name = 'SGL';
        lc.Description__c = 'Default';
        lc.SourceNo__c = '022';
        insert lc;
        String uniqueId = '123';
        String friendlyId = '123';
        String reqBody = '{"data": {' + 
				        +'"formName": "default",'
				        +'"promotionCode": "WEB0009573",'
				        +'"messageDivision": "resi",'
				        +'"messageType": "L",'
				        +'"emailSubject": "Home Security Review DNIS:WEB0009573",'
				        +'"firstName": "P",'
				        +'"lastName": "Test",'
				        +'"stateCode": "FL",'
				        +'"countryCode": "US",'
				        +'"zipCode": "33496",'
				        +'"primaryPhoneNumber": "5615555555",'
				        +'"emailAddress": "test@adt.com",'
				        +'"sourceIP": "10.43.36.39",'
				        +'"createDateTime": 1571251260404,'
				        +'"udf": [ { "tag": "Request_Message_Format","value": "RQ20051015" },' +
				            '{"tag": "Account_Type","value": "CCC" },'+
				            '{"tag": "Request_Custom_7", "value": "https://adt-dev-63.adobecqms.net/protect/me" },'+
				            '{"tag": "Request_Custom_8", "value": "default-pc-email" },'+
				            '{"tag": "Request_Custom_9", "value": "15712512603701660515249" },'+
				            '{"tag": "Request_Custom_10", "value": "10.43.36.39" },'+
				            '{"tag": "Request_Custom_13", "value": ' + uniqueId + '},'+
				            '{"tag": "Request_Custom_14", "value": ' + friendlyId + ' },'+
				            '{"tag": "Request_Custom_15", "value": "TemplateID:email-pc-option-2,Protect:Pet,HeroImage:H-3,KeyProduct1:P-1,KeyProduct2:P-2,GroupProduct1:P-3,GroupProduct2:P-4,GroupProduct3:P-5,GroupProduct4:P-6,TollFree:8002382727,CallNowButtonFlag:true,PersonalCode:fe59b8"},'+
				            '{"tag": "Call_Comment_01", "value": "Call Rep:" }' + ']}}';
        Test.setMock(HttpCalloutMock.class, new ADTCheetahMailMockTest());  
        
        Test.starttest();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri ='https://cs13.salesforce.com/services/apexrest/createWebLead';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(reqBody);
        
        RestContext.request = request;
        RestContext.response = response;
        ADTWebLeadsAPI.dopostmethod();
        list<AuditLog__c> auditLogObj = [SELECT Id FROM AuditLog__c WHERE Request_Custom_13__c = :uniqueId order by createddate desc limit 1];
        
        system.assertEquals(auditLogObj.isEmpty(), false);
        system.assertEquals(auditLogObj.size(), 1);
        
        list<RequestQueue__c> rqList = [SELECT Id, ServiceTransactionType__c, RequestStatus__c, MessageID__c, counter__c 
        									FROM RequestQueue__c 
        									WHERE AuditLog__c = :auditLogObj.get(0).Id
        									AND ServiceTransactionType__c = 'sendACKEmail'];
        system.assertEquals(rqList.isEmpty(), false);
        system.assertEquals(rqList.size(), 1);
        system.assertEquals(rqList.get(0).ServiceTransactionType__c, 'sendACKEmail');
        system.assertEquals(rqList.get(0).RequestStatus__c, 'Ready');
        system.assertEquals(rqList.get(0).counter__c, 0);
        
        ADTWebLeadsAPI.processWebleadsRequestQueue('ACK');
        
        rqList = [SELECT Id, ServiceTransactionType__c, RequestStatus__c, MessageID__c, counter__c 
        									FROM RequestQueue__c 
        									WHERE AuditLog__c = :auditLogObj.get(0).Id];
        system.assertEquals(rqList.isEmpty(), false);
        system.assertEquals(rqList.size(), 3);
        
        list<CustomerInterestConfig__c> questionList = new list<CustomerInterestConfig__c>();
        questionList.add(new CustomerInterestConfig__c(UniqueVisitorNumber__c = uniqueId, Type__c = 'Product', FriendlyId__c = friendlyId));
        insert questionList;
        
        request.requestUri ='https://cs13.salesforce.com/services/apexrest/createWebLead';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(reqBody);
        
        RestContext.request = request;
        RestContext.response = response;
        ADTWebLeadsAPI.dopostmethod();
        
        //delete auditLogObj;
        auditLogObj = [SELECT Id FROM AuditLog__c WHERE Request_Custom_13__c = :uniqueId order by createddate desc limit 1];
        
        system.assertEquals(auditLogObj.isEmpty(), false);
        system.assertEquals(auditLogObj.size(), 1);
        
        rqList = [SELECT Id, ServiceTransactionType__c, RequestStatus__c, MessageID__c, counter__c 
        									FROM RequestQueue__c 
        									WHERE AuditLog__c = :auditLogObj.get(0).Id
        									AND ServiceTransactionType__c = 'sendPCEmail'];
        system.assertEquals(rqList.isEmpty(), false);
        system.assertEquals(rqList.size(), 1);
        system.assertEquals(rqList.get(0).ServiceTransactionType__c, 'sendPCEmail');
        system.assertEquals(rqList.get(0).RequestStatus__c, 'Ready');
        system.assertEquals(rqList.get(0).counter__c, 0);
        
        ADTWebLeadsAPI.processWebleadsRequestQueue('sendPCEmail');
        rqList = [SELECT Id, ServiceTransactionType__c, RequestStatus__c, MessageID__c, counter__c 
        									FROM RequestQueue__c 
        									WHERE AuditLog__c = :auditLogObj.get(0).Id];
        system.assertEquals(rqList.isEmpty(), false);
        system.assertEquals(rqList.size(), 3);
        Test.stoptest();
    }
    
	global class ADTCheetahMailMockTest implements HttpCalloutMock {
	    global HTTPResponse respond(HTTPRequest request) {
	        // Create a fake response
	        HttpResponse response = new HttpResponse();
	        response.setHeader('Content-Type', 'application/json');
	        //response.setBody('');
	        response.setStatusCode(200);
	        return response; 
	    }
	}
}