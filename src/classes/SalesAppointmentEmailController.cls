/************************************* MODIFICATION LOG ********************************************************************************************
* SalesAppointmentEmailController
*
* DESCRIPTION : Used as the controller on the component generating HTML content for an email alert related to an upcoming appointment
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera      5/26/2016     - Original Version
* Magdiel Herrera      1/5/2017      - Added Express activation conditions for dynamic content
*                          
*/

public without sharing class SalesAppointmentEmailController {
    
    public Id eId {get;set;}
    public Boolean isReminder {get;set;}
    public Boolean isReschedule {get;set;}
    public Boolean renderRescheduleDetails {get;set;}
    public Boolean isInHouseExpressActivation {get;set;}
    public Event e {get;set;}
    public Event eR {get;set;}
    public Account a {get;set;}
    public Address__c addr {get;set;}
    public User recipient {get;set;}
    
    public String StartDateTime {get;set;}
    public String StartDateTimeReschedule {get;set;}
    public String AccountURL {get;set;}
    public String AppointmentURL {get;set;}
    public String AppointmentURLReschedule {get;set;}
    public String EmailDescription {get;set;}
    
    public SalesAppointmentEmailController(){}
    
    private DateTime formatCustomerDateTime(Event ev){
        Timezone CustomerTimeZone = TimeZone.getTimeZone( ev.TimeZoneId__c );
        Timezone currentTimeZone = TimeZone.getTimeZone( UserInfo.getTimeZone().getID() );
        DateTime st=ev.startDateTime;
        Integer offsetHourValueCustomer = ((CustomerTimeZone.getOffset(st))/1000/60/60)*(-1);//offset in hours to GMT
        Integer offsetHourValuePhoneSalesRep = ((currentTimeZone .getOffset(st))/1000/60/60)*(-1);//offset in hours to GMT
        st = st.addHours(offsetHourValuePhoneSalesRep -offsetHourValueCustomer );
        return st;
    }
    
    public String getComponentData(){
        String compRes = '';
        isReschedule = false;
        renderRescheduleDetails = false;
        isInHouseExpressActivation = false;
        DateTime st;
        // If the event id is set
        if( !String.isEmpty( eId ) ){
            // This event
            e = [SELECT Subject, WhatId, WhoId, StartDateTime, EndDateTime, DurationInMinutes, Description, Status__c,TimeZoneId__c,ownerId, ScheduleID__c, Reschedule__c, Reschedule_FromID__c, Reschedule_To__c, Reschedule_ToID__c, OwnerWhenCanceled__c, Quote_Id__c ,TaskCode__c FROM Event WHERE Id = :eId];
            
            // This is an InHouse Express Activation appointment if this field has a value on the event
            if( !String.isBlank(e.Quote_Id__c) ){
                isInHouseExpressActivation = true;
            }
            
            AppointmentURL = URL.getSalesforceBaseUrl().toExternalForm().replace('-api', '') + '/' + e.Id;
            StartDateTime = e.StartDateTime.format('MMMM dd, YYYY') +' at ' +e.StartDateTime.format('h:mm a');
            if( NSCHelper.isAccountID( e.WhatId ) ){
                a = [SELECT Name, Phone, Email__c, TelemarAccountNumber__c,
                            AddressID__r.Street2__c, AddressID__r.StreetName__c, AddressID__r.PostalCodeAddOn__c, AddressID__r.County__c, AddressID__r.PostalCode__c, AddressID__r.Street__c, AddressID__r.State__c, AddressID__r.StandardizedAddress__c, AddressID__r.OriginalAddress__c, AddressID__r.Longitude__c, AddressID__r.Latitude__c, AddressID__r.City__c, AddressID__r.Name, AddressID__c
                     FROM Account WHERE Id = :e.WhatId];
                AccountURL = URL.getSalesforceBaseUrl().toExternalForm().replace('-api', '') + '/' + a.Id;
                if( a.AddressID__c != null ){
                    addr = (Address__c)a.AddressID__r;
                }
                
                st = formatCustomerDateTime(e);
                StartDateTime = st.format('MMMM dd, YYYY') +' at ' +st.format('h:mm a');
            }
            if( !String.isBlank( e.WhoId ) ){
                recipient = [SELECT Name, Phone, Email FROM User WHERE Id = :e.OwnerId];
            }       
        }
        if( isReminder != null && isReminder ){
            if (e.TaskCode__c==System.Label.Relo_Appt_Task_Code)
                compRes = 'Relocation Certification Appointment Reminder';
            else
                compRes = 'Sales Appointment Reminder';
            EmailDescription = 'This is a courtesy reminder of your upcoming appointment.';
            if( isInHouseExpressActivation ){
                compRes = 'Express Activation Reminder';
            }
        }
        else if ( e.Status__c == EventManager.STATUS_CANCELED ){
            // Check if this appointment was cancelled because of a reschedule by checking at a reason code for it
            if( !String.isBlank(e.Reschedule_ToID__c) && !String.isBlank(e.OwnerWhenCanceled__c) ){
                // Read new appointment information
                er = [SELECT Subject, WhatId, WhoId, StartDateTime, EndDateTime, DurationInMinutes, Description, Status__c,TimeZoneId__c,ownerId, owner.Name, ScheduleID__c, Reschedule__c, Reschedule_FromID__c, Reschedule_To__c, Reschedule_ToID__c FROM EVENT WHERE Id = :e.Reschedule_ToID__c];
                isReschedule = true;
                renderRescheduleDetails = e.OwnerWhenCanceled__c.equalsIgnoreCase(er.owner.Name);
                st = formatCustomerDateTime(er);
                StartDateTimeReschedule = st.format('MMMM dd, YYYY') +' at ' +st.format('h:mm a');
                AppointmentURLReschedule = URL.getSalesforceBaseUrl().toExternalForm().replace('-api', '') + '/' + er.Id;
                if (e.TaskCode__c==System.Label.Relo_Appt_Task_Code)
                 compRes = 'Relocation Certification Appointment Rescheduled';
                else
                 compRes = 'Sales Appointment Rescheduled';
                if( isInHouseExpressActivation ){
                    compRes = 'Express Activation Rescheduled';
                }
                EmailDescription = 'Your upcoming appointment has been rescheduled. Please review your calendar.';
            }
            else{
                 if (e.TaskCode__c==System.Label.Relo_Appt_Task_Code)
                   compRes = 'Relocation Certification Appointment Cancelled';   
                 else
                   compRes = 'Sales Appointment Cancelled';
                
                if( isInHouseExpressActivation ){
                    compRes = 'Express Activation Cancelled';
                }
                EmailDescription = 'Your upcoming appointment has been cancelled. Please review your calendar.';
            }
        }
        else{
            if (e.TaskCode__c==System.Label.Relo_Appt_Task_Code){
             compRes = 'Relocation Certification Appointment Confirmation';  
             EmailDescription = 'You have a new upcoming Relocation Certification appointment.';  
            }
            else{
             compRes = 'Sales Appointment Confirmation';
            EmailDescription = 'You have a new upcoming sales appointment.';
            }
            if( isInHouseExpressActivation ){
                compRes = 'Express Activation Appointment Confirmation';
                EmailDescription = 'You have a new upcoming in-house express activation appointment.';
            }
        }
        return compRes;
    }
    
}