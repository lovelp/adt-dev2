@isTest
private class OptOutContactsAppplicationControllerTest {
    
    @testSetup
    private static void testSetup() {
     
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.name = 'DataRecastDisableAccountTrigger';
        rgv.value__c = 'FALSE';
        insert rgv;
        
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '22102';
        addr.OriginalAddress__c = '8952 BROOK RD, MCLEAN, VA 22102';
        insert addr;
        
        Account acc=TestHelperClass.createAccountData(addr,null,false);
        acc.ShippingStreet = '8952 Brook Rd';
        acc.ShippingStreet2__c = '';
        acc.ShippingCity = 'McLean';
        acc.ShippingState = 'VA';
        acc.ShippingPostalCode = '221o2';
        acc.MMBCustomerNumber__c='401093713';
        acc.MMB_Past_Due_Balance__c=740.00;
        acc.MMBSiteNumber__c='47453518';
        acc.MMB_Multisite__c=false;
        acc.Phone='7324317789';
        acc.AddressID__c=addr.id;
        acc.TelemarAccountNumber__c='39463559';
        acc.Profile_YearsInResidence__c='1';
        acc.recordtypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('ADT NA Resale').getRecordTypeId();
        acc.PhoneNumber4__c = '39463559';
        acc.PhoneNumber3__c = '39463560';
        acc.PhoneNumber2__c = '39463561';
        insert acc;
    }
    
    @isTest
    private static void testOptOut() {
        Account acc = [Select Id,PhoneNumber4__c,TelemarAccountNumber__c,MMBCustomerNumber__c,DNIS__c From Account Limit 1];
        
        PageReference pageRef = Page.OptOutContactsApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.currentPage().getParameters().put('phoneNo',acc.PhoneNumber4__c);
        ApexPages.currentPage().getParameters().put('checked','True');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        
        OptOutContactsAppplicationController ctrl = new OptOutContactsAppplicationController(sc);
        
        ctrl.optoutResMap = new Map<String, String>{'phonenumber1' => '1'};
        
        Test.startTest();
        ctrl.showDetails();
        ctrl.acc = acc;
        ctrl.optOutPhonesWrapList[0].phoneNumber = acc.PhoneNumber4__c;
        ctrl.optOutPhonesWrapList[0].optOut = true;
        ctrl.getOptOutMap();
        Test.stopTest();
    }
    
    @isTest
    private static void testOptOutCheckedFalse() {
        Account acc = [Select Id,PhoneNumber3__c From Account Limit 1];
        
        PageReference pageRef = Page.OptOutContactsApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.currentPage().getParameters().put('phoneNo',acc.PhoneNumber3__c);
        ApexPages.currentPage().getParameters().put('checked','False');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        
        OptOutContactsAppplicationController ctrl = new OptOutContactsAppplicationController(sc);
        
        Test.startTest();
        ctrl.showDetails();
        Test.stopTest();
    }
    
    @isTest
    private static void testOptOutCheckedAll() {
        Account acc = [Select Id,PhoneNumber3__c From Account Limit 1];
        
        PageReference pageRef = Page.OptOutContactsApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.currentPage().getParameters().put('phoneNo','All');
        ApexPages.currentPage().getParameters().put('checked','True');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        
        OptOutContactsAppplicationController ctrl = new OptOutContactsAppplicationController(sc);
        
        Test.startTest();
        ctrl.showDetails();
        Test.stopTest();
    }
    
    @isTest
    private static void testOptOutCheckedNone() {
        Account acc = [Select Id,PhoneNumber3__c From Account Limit 1];
        
        PageReference pageRef = Page.OptOutContactsApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.currentPage().getParameters().put('phoneNo','None');
        ApexPages.currentPage().getParameters().put('checked','True');
         ApexPages.currentPage().getParameters().put('index','0');

        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        
        OptOutContactsAppplicationController ctrl = new OptOutContactsAppplicationController(sc);
        
        Test.startTest();
        ctrl.showDetails();
        Test.stopTest();
    }
    
    @isTest
    private static void testOptOutResMap() {
        Account acc = [Select Id,PhoneNumber3__c From Account Limit 1];
        
        PageReference pageRef = Page.OptOutContactsApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        //ApexPages.currentPage().getParameters().put('phoneNo','None');
        //ApexPages.currentPage().getParameters().put('checked','True');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        
        OptOutContactsAppplicationController ctrl = new OptOutContactsAppplicationController(sc);
        
        Test.startTest();
        ctrl.getOptOutMap();
        Test.stopTest();
    }
}