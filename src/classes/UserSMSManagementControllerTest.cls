@isTest
public class UserSMSManagementControllerTest {
	public static Event eve;
     public static Account a;
    
    static void createTestData(){
        TestHelperClass.createReferenceDataForTestClasses();
        
        List<user> users=new List<user>();
        
        Profile p = [select id from profile where name='System Administrator'];
        integer i= 23547875;
        User integrationUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='TestIntegration', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingsms'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(integrationUser);
        
        i= 7987340;
        User NewRehashUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='NewRehash', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='unitTestSalesRepsms@testorg.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(NewRehashUser);
        i= 2343579;
        User ResaleAdmin=new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='ADMIN',firstname='TestGlobal', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingsms'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(ResaleAdmin);
        
        INSERT users;
        
       a=testHelperClass.createAccountData();
        
        
        eve=testHelperClass.createEvent(a,testHelperClass.createUser(32426402));
        eve.TimeZoneId__c ='America/Los_Angeles';
        contact c=new contact();
        c.firstname='test';
        c.lastname='test';
        c.accountid=a.id;
        insert c;
        eve.whoid=c.id;
        update eve;
  
    }
    
    static testmethod void testUserSMSManagementController(){
        createTestData();
        User u;
        User rep;
        User current = [select Id from User where Id=:UserInfo.getUserId()];
       	User testadmin = [select id from user where profile.name = 'System Administrator' and IsActive = true limit 1];
        System.runAs(testadmin){
        u = TestHelperClass.createSalesRepUser(); 
        }
 
        test.startTest();
        System.runAs(current){
        
        // Create a page Reference
            PageReference pRef = Page.UserSMSManagement;
            Test.setCurrentPage(pRef);
            // Instantiate the Controller
            ApexPages.StandardController sc = new ApexPages.StandardController(u);
            UserSMSManagementController  testCls = new UserSMSManagementController(sc);
            testCls.SMSOption = false;
            testCls.saveSMSOption();
            testCls.saveSMSOption();
            //EmailMessageUtilities.NotifyUserBySMS( u.Id, true );
        test.stopTest();
        }
           
    }
     
    
    static testmethod void testUserSMSManagementController2(){
        createTestData();
        User u;
        User rep;
        User current = [select Id from User where Id=:UserInfo.getUserId()];
       	User testadmin = [select id from user where profile.name = 'System Administrator' and IsActive = true limit 1];
        System.runAs(testadmin){
        u = TestHelperClass.createSalesRepUser(); 
        }
 
        test.startTest();
        System.runAs(current){
        
        // Create a page Reference
            PageReference pRef = Page.UserSMSManagement;
            Test.setCurrentPage(pRef);
            // Instantiate the Controller
            ApexPages.StandardController sc = new ApexPages.StandardController(u);
            UserSMSManagementController  testCls = new UserSMSManagementController(sc);
        	
            u.SMS_Enabled__c = false;
            u.MobilePhone = '4085073115';
            update u;
            testCls.SMSOption = true;
            testCls.saveSMSOption();
            testCls.SMSOption = true;
            u.MobilePhone = '';
            update u;
            testCls.saveSMSOption();
        test.stopTest();
        }
           
    }
}