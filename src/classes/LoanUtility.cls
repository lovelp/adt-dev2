/************************************* MODIFICATION LOG ********************************************************************************************
* 
* Name: LoanUtility
*
* DESCRIPTION :
* Utility class for Loan Application
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                        DATE                        Ticket              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari               06/07/2019                   HRM-9918          FlexFI National - Ability to vie Loan info
* Srinivas Yarramsetti           06/12/2019                   HRM-9918          Added loan query logic in this.
********************************************************************************************************************************************************/
public class LoanUtility {
    public static list<CustomerLoan__c> queryLoans(set<String> dataSet){
        list<CustomerLoan__c> loans = [SELECT Id, Name, Active__c, AmountBilled__c, AmountFinanced__c, Balance__c, CustomerNumber__c, InstallmentsBilled__c, Last_Refreshed_on__c,PaymentURL__c,
                                       Loan_Type__c, NoteNumber__c, QuoteOrderNumber__c, QuoteJobNumber__c, Selected_to_Pay_at_Sale__c, SiteNumber__c, SiteStatus__c, Status__c, TotalInstallments__c,
                                       createddate, UnbilledInstallments__c, CFGLoanAcceptanceDate__c
                                       FROM CustomerLoan__c 
                                       WHERE CustomerNumber__c IN:dataSet AND Active__c = true LIMIT 50000];
        return loans;                       
    }
    
    public static list<CustomerLoan__c> processCFGCustLoans(String custNo){
        list<LoanApplication__c> loans = [SELECT Id, AmountFinanced__c, BillingAddress__c, QuoteId__c, LoanNumber__c, InitialPaymentAmt__c, installmentAmt__c, RequestedAmount__c,
                                          Term__c, Site__c, Customer__c,MMBJobNumber__c, LoanAcceptRequestDate__c
                                          FROM LoanApplication__c WHERE Customer__c = :custNo AND LoanAcceptRequestDate__c != null 
                                          AND (CFG_Loan_Status__c = 'Active' OR CFG_Loan_Status__c ='') LIMIT 100];
        list<CustomerLoan__c> loanList = new list<CustomerLoan__c>();
        for(LoanApplication__c loanApp : loans){
            CustomerLoan__c loan = new CustomerLoan__c();
            loan.Active__c = true;
            loan.AmountFinanced__c = loanApp.AmountFinanced__c;
            loan.TotalInstallments__c = loanApp.Term__c;
            //loan.AmountBilled__c = loanApp.RequestedAmount__c;
            loan.Loan_Type__c = 'CFG Loan';
            loan.CustomerNumber__c = loanApp.Customer__c;
            loan.SiteNumber__c = loanApp.Site__c;
            loan.InstallmentsBilled__c = 0;
            loan.Last_Refreshed_on__c = system.now();
            loan.QuoteOrderNumber__c = loanApp.QuoteId__c;
            loan.QuoteJobNumber__c = loanApp.MMBJobNumber__c;
            loan.CFGLoanAcceptanceDate__c = loanApp.LoanAcceptRequestDate__c;
            loanList.add(loan);
        }
        if(loanList.size()>0){
            return loanList;
        } 
        return null;
    }
    
    @future
    public static void customerLnRefFut(String custNo){
        LoanUtility.customerLoanRefresh(custNo, null);
    }
    
    //Method to refresh the customer loans.
    public static void customerLoanRefresh(String custNo, Map<String, String> siteStatusMap){
        system.debug('Loan refresh called'+custNo);
        list<CustomerLoan__c> newCustLnInsert = new list<CustomerLoan__c>();
        list<CustomerLoan__c> custLoanToDeleteCFG = new list<CustomerLoan__c>();
        list<CustomerLoan__c> custLoanToDeleteRIC = new list<CustomerLoan__c>();
        List<ADTLog__c> adtLogList = new List<ADTLog__c>();
        //ADT RIC loans from MMB using notes api
        if(String.isNotBlank(custNo)){
            try{
                newCustLnInsert = ADTNotereceivableAPI.getNotes(custNo);
            }
            catch(Exception ex){
                ADTLog__c logObj = new ADTLog__c(name='Customer Number '+custNo);
                logObj.Custom_App__c =  ADTApplicationMonitor.CUSTOM_APP.FLEXFI.name();
                logObj.Date_Time__c = DateTime.now();
                logObj.Message__c = 'ADT NotesreceivableAPI';
                logObj.Message_Description__c = ex.getMessage();
                adtLogList.add(logObj);
            }
        }
        System.debug('The adtLogList is'+adtLogList);
        //If list is null
        if(newCustLnInsert == null){
            newCustLnInsert = new list<CustomerLoan__c>();
        }
        //RIC Loans End.
        list<CustomerLoan__c> custLoans = queryLoans(new set<String>{custNo});
        //CFG Loans only
        list<CustomerLoan__c> cfgtoInsert = processCFGCustLoans(custNo);
        if(cfgtoInsert != null && cfgtoInsert.size()> 0)
            newCustLnInsert.addAll(cfgtoInsert);
        //End CFG loans 
        //All loans for delete
        //if(newCustLnInsert.size()>0){
        for(CustomerLoan__c csl:custLoans){
            if(csl.Loan_Type__c == 'CFG Loan'){
                custLoanToDeleteCFG.add(csl);
            }
            if(csl.Loan_Type__c == 'ADT RIC'){
                custLoanToDeleteRIC.add(csl);
            }
        }
        //}
        set<String> siteNos = new set<String>();
        List<MMBCustomerSiteSearchApi.SiteInfoResponse> siteResponseslist = new List<MMBCustomerSiteSearchApi.SiteInfoResponse>();
        //Take all site nos from fresh data
        if(newCustLnInsert != null && newCustLnInsert.size()>0){
            for(CustomerLoan__c ncl: newCustLnInsert){
                if(String.isNotBlank(ncl.SiteNumber__c)){
                    system.debug('Doing site lookup for: '+ncl.SiteNumber__c);
                    list<MMBCustomerSiteSearchApi.SiteInfoResponse> siteresponstemp = new List<MMBCustomerSiteSearchApi.SiteInfoResponse>();
                    siteresponstemp = MMBLookupSiteInformation(ncl.SiteNumber__c);
                    if(siteresponstemp != null){
                        siteResponseslist.addAll(siteresponstemp);
                    }
                }
            }
        }
        //If sitemap is null intilaize it
        if(siteStatusMap == null){
            siteStatusMap = new Map<String, String>();
        }
        //Create a map for unique site and staus
        if(siteResponseslist != null && siteResponseslist.size()>0){
            for(MMBCustomerSiteSearchApi.SiteInfoResponse st: siteResponseslist){
                siteStatusMap.put(st.SiteNo, st.ServiceActive);
            }
        }
        //Site Status if active or inactive
        if(newCustLnInsert != null && newCustLnInsert.size()>0){
            if(String.isNotBlank(custNo) && siteStatusMap != null && siteStatusMap.keyset().size()>0){
                list<CustomerLoan__c> custLoansSiteStatus = queryLoans(new set<String>{custNo});
                for(CustomerLoan__c cus: newCustLnInsert){
                    if(siteStatusMap.get(cus.SiteNumber__c) == 'IN' || siteStatusMap.get(cus.SiteNumber__c) == 'OUT')
                        cus.SiteStatus__c = siteStatusMap.get(cus.SiteNumber__c); 
                    else
                        cus.SiteStatus__c = 'Error/Retry';
                }
                //update custLoansSiteStatus;
            }
        }
        if(newCustLnInsert.size()>0){
            insert newCustLnInsert; 
        }
        list<CustomerLoan__c> culnsToDelete = new list<CustomerLoan__c>();
        
        if(custLoanToDeleteCFG.size()>0){
            culnsToDelete.addAll(custLoanToDeleteCFG);
        }
        if(custLoanToDeleteRIC.size()>0){
            culnsToDelete.addAll(custLoanToDeleteRIC);
        }
        if(culnsToDelete.size()>0){
            //Database.delete(custLoanToDeleteCFG);
            Database.delete(culnsToDelete);
        }
        if(adtLogList.size()>0){
            insert adtLogList;
        }
    }
    
    public static Set<String> getLoansForInactiveSites(Account a, list<CustomerLoan__c> customerLoanList){
        try{
            //get all customer loan records based on the customer number
            //List<CustomerLoan__c> customerLoanList = new List<CustomerLoan__c>();//List of customer loans
            Set<String> quoteUniqueSet = new Set<String>();
            //Query to get all customer Loan records for a customer number on the account
            
            if(a!= Null && String.isNotBlank(a.MMBCustomerNumber__c) && customerLoanList==null){
                customerLoanList = new list<CustomerLoan__c>();
                customerLoanList = [SELECT CustomerNumber__c, SiteNumber__c, Loan_Type__c, SiteStatus__c, QuoteOrderNumber__c FROM CustomerLoan__c Where CustomerNumber__c =: a.MMBCustomerNumber__c];
            }
           
            if(customerLoanList != null){
                for(CustomerLoan__c cLRecord : customerLoanList){
                    if(cLRecord.SiteStatus__c != null && cLRecord.SiteStatus__c.equalsIgnoreCase('OUT') && String.isNotBlank(cLRecord.QuoteOrderNumber__c) && !quoteUniqueSet.contains(cLRecord.QuoteOrderNumber__c) && String.isNotBlank(a.MMBSiteNumber__c) && !cLRecord.SiteNumber__c.equalsIgnoreCase(a.MMBSiteNumber__c)){
                         quoteUniqueSet.add(cLRecord.QuoteOrderNumber__c);
                    }
                }
            }
            return quoteUniqueSet;
        }catch(exception ex){
            return new Set<String>();
        }
        
    }
    
    public static Set<String> getActiveLoansForAccountSite(Account a){
        //get all customer loan records based on the customer number
        List<CustomerLoan__c> customerLoanList = new List<CustomerLoan__c>();//List of customer loans
        Map<String,List<String>> siteCustomerMap = new Map<String,List<String>>();//Map of site Number and List of Quotes for that site
        Map<String,String> siteStatusMap   = new Map<String,String>();//Map of site number and site status
        Set<String> quoteSet = new Set<String>();// Set for Unique quote values
        try{
            //Query to get all customer Loan records for a customer number on the account
            if(a!= Null && String.isNotBlank(a.MMBCustomerNumber__c)){
                customerLoanList = [SELECT CustomerNumber__c, SiteNumber__c, Loan_Type__c, SiteStatus__c, QuoteOrderNumber__c FROM CustomerLoan__c Where CustomerNumber__c =: a.MMBCustomerNumber__c];
            }
            for(CustomerLoan__c customerLoanRecord : customerLoanList){
                //populate map for site number and all quotes related to it.
                if(siteCustomerMap.containsKey(customerLoanRecord.SiteNumber__c)) {
                    List<String> quoteList = siteCustomerMap.get(customerLoanRecord.SiteNumber__c);
                    quoteList.add(customerLoanRecord.QuoteOrderNumber__c);
                    siteCustomerMap.put(customerLoanRecord.SiteNumber__c, quoteList);
                } else {
                    siteCustomerMap.put(customerLoanRecord.SiteNumber__c, new List<String> { customerLoanRecord.QuoteOrderNumber__c });
                }
                if(!siteStatusMap.containsKey(customerLoanRecord.SiteNumber__c)){
                    siteStatusMap.put(customerLoanRecord.SiteNumber__c,customerLoanRecord.SiteStatus__c);
                }
            }
            //Create a Set for the unique values of quotes for a site on account. 
            if(String.isNotBlank(a.MMBSiteNumber__c) && siteStatusMap.containsKey(a.MMBSiteNumber__c) && siteCustomerMap.containsKey(a.MMBSiteNumber__c)){
                quoteSet.addAll(siteCustomerMap.get(a.MMBSiteNumber__c));
            }
            return quoteSet;
        }catch(exception ex){
            return new Set<String>();// Set for Unique quote values
        }
    }
    
    public static String limitOnLoanApplication(Account a){
        try{
            String statusFlag = '';
            Set<String> activeLoanSet = LoanUtility.getActiveLoansForAccountSite(a);
            Set<String> inActiveSiteLoanSet = LoanUtility.getLoansForInactiveSites(a,null);
            if(activeLoanSet.size() >= 2){
                statusFlag =(FlexFiMessaging__c.getinstance('loanLimitOnCurrentSiteMessage') != null && String.isNotBlank(FlexFiMessaging__c.getinstance('loanLimitOnCurrentSiteMessage').ErrorMessage__c)) ? '<br/>'+FlexFiMessaging__c.getinstance('loanLimitOnCurrentSiteMessage').ErrorMessage__c +'<br/>' :'<br/>'+'Cannot create loan Application. 2 active loans on this site exist.' ;
            }
            if(inActiveSiteLoanSet.size() > 2){
                statusFlag +=(FlexFiMessaging__c.getinstance('inactiveLoanLimitOnSiteMessage') != null && String.isNotBlank(FlexFiMessaging__c.getinstance('inactiveLoanLimitOnSiteMessage').ErrorMessage__c)) ? '<br/>'+FlexFiMessaging__c.getinstance('inactiveLoanLimitOnSiteMessage').ErrorMessage__c +'<br/>' :'<br/>'+'Cannot do loan Application. More than 2 inactive loans exist for this customer number.';
            }
            return statusFlag;
        }catch(exception ex){
            return '';
        }
    }
    
    public static List<MMBCustomerSiteSearchApi.SiteInfoResponse> MMBLookupSiteInformation (string siteId){
        try{
            // create Site info Request
            MMBCustomerSiteSearchApi.SiteInfoRequest siteInfoReq = new MMBCustomerSiteSearchApi.SiteInfoRequest();
            siteInfoReq.FirstName   = '';
            siteInfoReq.LastName    = '';
            siteInfoReq.Address1    = '';
            siteInfoReq.City        = '';
            siteInfoReq.State       = '';
            siteInfoReq.ZipCode     = '';
            //String prefixVal = PartnerAPIMessaging__c.getInstance('SitePrefixes').Error_Message__c;
            siteInfoReq.SiteNo      = siteId.containsIgnoreCase('_') ? siteId.substringAfter('_') : siteId;
            system.debug('@@siteInfoReq: '+siteInfoReq);
            MMBCustomerSiteSearchApi.LookupSiteResponse siteResponse = MMBCustomerSiteSearchApi.LookupSiteForPartner(siteInfoReq);
            system.debug('@@siteResponse: '+siteResponse);
            List<MMBCustomerSiteSearchApi.SiteInfoResponse> selectedResultSiteList = new List<MMBCustomerSiteSearchApi.SiteInfoResponse>();
            // if Success Response
            if (siteResponse !=null && siteResponse.ResultSummaryMessage.equalsIgnoreCase('Success')){
                for (MMBCustomerSiteSearchApi.SiteInfoResponse sResponse : siteResponse.siteInfo){
                    if(sResponse.ServiceActive == 'IN'){
                        selectedResultSiteList.add(sResponse);
                        return selectedResultSiteList;
                    }
                    selectedResultSiteList.add(sResponse);
                }                    
            }
            return selectedResultSiteList;
        }
        catch(Exception ex){
            system.debug('@@Exception Occured: '+ex.getMessage());
            ADTApplicationMonitor.log(ex, 'ADTPartnerMMBLookupSite', 'MMBLookupSiteInformation', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            return null;
        }
    }
    
}