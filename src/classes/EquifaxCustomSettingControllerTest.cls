/**
 Description- This test class used for EquifaxCustomSettingController.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class EquifaxCustomSettingControllerTest {
    
     static testMethod void test1() {
     
         List<EquifaxCreditScoreMapping__c> equifaxList= new List<EquifaxCreditScoreMapping__c>();
         equifaxList.add(new EquifaxCreditScoreMapping__c (name = '300:325',MappedCreditScore__c = 'A'));
         equifaxList.add(new EquifaxCreditScoreMapping__c (name = '376:400',MappedCreditScore__c = 'D'));

         test.startTest();
         insert equifaxList;
         
         EquifaxCreditScoreMapping__c checkCM= [select id from EquifaxCreditScoreMapping__c where id=: equifaxList[0].Id];
         system.assertEquals(checkCM.Id, equifaxList[0].Id);
         
         EquifaxCreditScoreMapping__c equ= new EquifaxCreditScoreMapping__c();
         
         ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(equ);
         
         EquifaxCustomSettingController eq= new EquifaxCustomSettingController(sc1);
         eq.getEquifaxMapping();
         
         test.stopTest();
     }
}