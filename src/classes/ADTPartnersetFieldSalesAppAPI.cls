/************************************* MODIFICATION LOG ********************************************************************************************
* ADTPartnersetFieldSalesAppAPI
*
* DESCRIPTION : Supporting class for ADTPartnersetFieldSalesAppAPI. Process the incoming JSON request and process response with creditscore.
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*     SY              9/26/2017      - Original Version
* 
*                          
*/

@RestResource(urlMapping='/setFieldSalesAppointment/*')
global without sharing class ADTPartnersetFieldSalesAppAPI{

    @HttpPost
    global static void dopostmethod(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String jsonCustRequest = RestContext.request.requestBody.toString();
        system.debug('Request Details'+jsonCustRequest);
        res.addHeader('Content-Type', 'application/json');
        if(String.isBlank(req.requestBody.toString())){
            res = errorResponse(400,'Request body is blank.');
        }
        else{
            ADTPartnersetFieldSalesAppController adtPartnerFSApp = new ADTPartnersetFieldSalesAppController();
            ADTPartnersetFieldSalesAppController.setAppntResponse createResponse = adtPartnerFSApp.parseRequest(req.requestBody.toString());
            res.responseBody = Blob.valueOf(JSON.serialize(createResponse,true));
            res.statusCode = adtPartnerFSApp.statusCode;
        }
    }
    
    //Handle error responses here
    global static RestResponse errorResponse(Integer statusCode, String reason){
        RestResponse res = RestContext.response;
        res.statusCode = statusCode;
        String errorMessage = PartnerAPIMessaging__c.getinstance(String.valueOf(statusCode)).Error_Message__c;
        res.responseBody = Blob.valueOf(JSON.serialize(errorMessage, true));
        ADTApplicationMonitor.log ('ADTPartner Invalid Request Error', reason, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        return res;
    }

}