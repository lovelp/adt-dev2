/************************************* MODIFICATION LOG ********************************************************************************************
* ADTGoMapController
* 
DESCRIPTION : Obtains all dynamic data required by the ADTGo Map Display
*             It is a Controller in the SFDC MVC architecture. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Mike Tuckman    				8/15/13				- Original Version
*
*													
*/


global with sharing class ADTGoRest {
	
	private static String canopyURL; // = 'https://sales-admin.adtcanopyportal.com/api/person/attributes/';
    public static String debugResponse;
    public static ADTGoResponse canopyResponse;
    
    public class RestException extends Exception {}
	 
	public class UserResults {
		public Boolean status;
		public String firstname;
		public String lastname;
		public String email;
        public String phone;
		public Device dev;
		public String carrier;
		public Location loc;
		public Battery batt;
		public Datetime updated;
        public String message;
        public Decimal demoseq;
        public ADTGoPhyAttr attr;
	}
	
	public class Device {
		public String type;
		public String id;
	}
	
	public class Battery {
		public Decimal level;
		public String state;
	}
	
	public class Location {
		public Decimal lat;
		public Decimal lon;
		// public Address address;
		public Decimal speed;
		public String direction;
		public Integer offset;
        public String accuracy;
	}
	
/*	public class Address { // Not currently used
		public String addr;
		public String city;
		public String state;
		public String zip;
		
		public override String toString(){
			return addr + ' ' + city + ', ' + 'state' + ' ' + zip;
		}
	}*/

    public class ADTGoResponse {
		public ADTGoPhyAttr physicalAttributes;
		public List<ADTGoLocations> locations;
		public ADTGoContact contact;
	}
    
	public class ADTGoPhyAttr {
		public String hairColor;
		public String weightInLbs;
		public String heightInInches;
		public String gender;
		public String race;
	}

    global class ADTGoLocations implements Comparable {
		public String id;
		public String eventId;
		public String accuracy;
        public String speed;
		public String heading;
		public String location;
		//public DateTime datetime;
		public DateTime created;
        
        public ADTGoLocations(String i, String e, String a, String s, String h, String l, DateTime c) {
            id = i;
            eventId = e;
            accuracy = a;
            speed=  s;
            heading = h;
            location = l;
            created = c;
		}
        
        global Integer compareTo(Object compareTo) {
            ADTGoLocations compareLoc = (ADTGoLocations)compareTo;
        	if (created == compareLoc.created) return 0;
        	if (created > compareLoc.created) return 1;
        	return -1;       
    	}
    }
    
	public class ADTGoContact {
		public String firstName;
		public String lastName;
		public String mobilePhoneNumber;
	}
    
	public static UserResults getUser (String phone, Decimal demoSeq){
		
		String resJson;
		resJson = '{"status":false,"user":{"mobile":"9999999999","first_name":"Default","last_name":"Data","latitude":28.4186,"longitude":-81.5811,"email":"Michael@Tuckman.us","updated_at":"2017-10-28T21:39:47Z","devices":[{"token":"b71f1875-2c69-4dd9-9ecc-759288c789a8","token_type":"android"},{"token":"0050cf67-6921-4cd2-9edb-5c120f9d7752","token_type":"android"}]}}';

		UserResults results = parseUserResults(resJson);
        results.demoseq = 0;
        
        if (phone == '9999999997') {
            system.debug(demoSeq);
            //try{
            	ADTGoDemoLocation__c demoLoc = [select Latitude__c, Longitude__c, Message__c, Sequence__c from ADTGoDemoLocation__c where Sequence__c > :demoSeq and Active__c = true order by Sequence__c limit 1];
                if (demoLoc != null) {
                    results.loc.lat = demoLoc.Latitude__c;
                    results.loc.lon = demoLoc.Longitude__c;
                    results.message = demoLoc.Message__c;
                    results.demoseq = demoLoc.Sequence__c;
                }
            //}
            /*catch(Exception e){
            	ADTGoDemoLocation__c demoLoc = [select Latitude__c, Longitude__c, Message__c, Sequence__c from ADTGoDemoLocation__c where Active__c = true order by Sequence__c limit 1];
                if (demoLoc != null) {
                    results.loc.lat = demoLoc.Latitude__c;
                    results.loc.lon = demoLoc.Longitude__c;
                    results.message = demoLoc.Message__c;
                    results.demoseq = demoLoc.Sequence__c;
                }
            }*/
        }
		//results.updated = datetime.now();
        results.updated = Datetime.newInstance(datetime.now().date(),datetime.now().time());
		System.debug('Results' + results);		
		
		return results;
	}

    public static UserResults getUserCanopy (String eventID){
		
		HttpRequest req = new HttpRequest();

		canopyURL = getURL();

		String url = canopyURL + '/api/person/attributes/' + eventID.trim();
		req.setEndpoint(url);
		req.setMethod('GET');
		System.debug('Url...: '+ url);

		String auth = setAuthentication();
		req.setHeader('Authorization', auth);
		req.setHeader('Accept', '*/*');
		req.setHeader('Accept-Encoding', 'gzip,deflate,sdch');

		System.debug('Request...: ' + req);
		Http http = new Http();
		Httpresponse res = new Httpresponse();
		If (!Test.isRunningTest()) {
			res = http.send(req);
		}
		else {
			res = new Httpresponse();
			res.setStatusCode(200);
			StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ADTGoResponse1' LIMIT 1];
			res.setBody(sr.Body.toString());
		}
		System.debug('Response...: ' + res.getBody());
		String resJson;
		if (res.getStatusCode() == 200){
			resJson = res.getBody();
		}
		else {
            throw new RestException(res.getStatusCode() + '-' + res.getBody());
			resJson = '{ "physicalAttributes": { "hairColor": "", "weightInLbs": "1", "heightInInches": "1", "gender": "DEFAULT", "race": "DEFAULT" }, "locations": [ { "id": "97fc21ca-3e73-4cd6-b0f0-99f736fc59ce", "eventId": "b1e77644-8b34-468b-99b3-b39712a1e9cc", "accuracy": "10", "speed": null, "heading": null, "location": "26.3948860438786,-80.1180510689319", "datetime": "2017-12-13T16:12:29.000Z", "created": "2017-12-13T16:12:29.699Z" } ], "contact": { "firstName": "William", "lastName": "Jackson", "mobilePhoneNumber": "5617151754" }}';
		}
        
        debugResponse = res.getStatusCode() + ' ' + res.getBody();

		UserResults results = parseCanopyUserResults(resJson);
		
		System.debug('Results' + results);		
		
		return results;
	}
    
    public static String setDisposition (String eventID, String code, String message){
		
		HttpRequest req = new HttpRequest();

		canopyURL = getURL();

		String url = canopyURL + '/api/sos/disposition/' + eventID.trim();
		req.setEndpoint(url);
		req.setMethod('POST');
		System.debug('Url...: '+ url);

		String auth = setAuthentication();
		req.setHeader('Authorization', auth);
		req.setHeader('Accept', '*/*');
		req.setHeader('Accept-Encoding', 'gzip,deflate,sdch');
		req.setHeader('Content-Type', 'application/json');
        
        String body = '{"code": "' + code + '", "message": "' + message + '"}';
        req.setBody(body);

		System.debug('Request...: ' + req);
		System.debug('Request...: ' + req.getBody());
		Http http = new Http();
		
		If (Test.isRunningTest()) {
	   		return '';
	    	}
		Httpresponse res = http.send(req);
        System.debug('Status...: ' + res.getStatusCode());
		System.debug('Response...: ' + res.getBody());
        System.debug('Response...: ' + res);
		if (res.getStatusCode() == 200){
			return '';
		}
		else {
			return (res.getStatusCode() + ':' + res.getBody());
		}
	}
	
	private static String getURL (){
		String url;
        
        if (ADTGoSetting.getSetting('CanopySystem').equalsIgnoreCase('Canopy')) {
        	url = ADTGoSetting.getSetting('CanopyURL');
        }
		
        else if (ADTGoSetting.getSetting('CanopySystem').equalsIgnoreCase('ADT')) {
        	url = ADTGoSetting.getSetting('ADTUrl') + '/canopyadmin';
        }
		
		return url;
		
	}
	
	private static String setAuthentication (){
		String user;
        String pass;
        
        if (ADTGoSetting.getSetting('CanopySystem').equalsIgnoreCase('Canopy')) {
        	user = ADTGoSetting.getSetting('CanopyUser');
			pass = ADTGoSetting.getSetting('CanopyPassword');
        }
		
        else if (ADTGoSetting.getSetting('CanopySystem').equalsIgnoreCase('ADT')) {
        	user = ADTGoSetting.getSetting('ADTUser');
			pass = ADTGoSetting.getSetting('ADTPassword');
        }
		
		return setAuthentication(user, pass);
		
	}
	
	private static String setAuthentication (String user, String pass){
		
		Blob headerValue = Blob.valueOf(user + ':' + pass);
		String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
		
		return authorizationHeader;
		
	}

 /**
{
    "physicalAttributes": {
        "hairColor": "",
        "weightInLbs": "1",
        "heightInInches": "1",
        "gender": "FEMALE",
        "race": "AMERICAN_INDIAN_ALASKAN_NATIVE"
    },
    "locations": [
        {
            "id": "97fc21ca-3e73-4cd6-b0f0-99f736fc59ce",
            "eventId": "b1e77644-8b34-468b-99b3-b39712a1e9cc",
            "accuracy": "10",
            "speed": null,
            "heading": null,
            "location": "26.3948860438786,-80.1180510689319",
            "datetime": "2017-12-13T16:12:29.000Z",
            "created": "2017-12-13T16:12:29.699Z"
        },
        {
            "id": "b8b887c8-7784-4224-8b20-9a7eb709ccf0",
            "eventId": "b1e77644-8b34-468b-99b3-b39712a1e9cc",
            "accuracy": "10",
            "speed": null,
            "heading": null,
            "location": "26.3948573358603,26.3948573358603",
            "datetime": "2017-12-13T16:17:08.000Z",
            "created": "2017-12-13T16:17:09.206Z"
        }
    ],
    "contact": {
        "firstName": "William",
        "lastName": "Jackson",
        "mobilePhoneNumber": "5617151754"
    }
}
 */
    
	private static UserResults parseCanopyUserResults(String resJson){

        Type resultType = Type.forName('ADTGoRest.ADTGoResponse');
		//ADTGoResponse canopyResponse = (ADTGoResponse)JSON.createParser(resJson).readValueAs(resultType);
		canopyResponse = (ADTGoResponse)JSON.createParser(resJson).readValueAs(resultType);
        
		UserResults results = new UserResults();
		results.loc = new Location();
		results.batt = new Battery();
		results.dev = new Device();
		
		results.firstname = canopyResponse.contact.firstName;
		results.lastname = canopyResponse.contact.lastName;
		results.phone = canopyResponse.contact.mobilePhoneNumber;
        
        DateTime latestDT;
        
        canopyResponse.locations.sort();
        
        for (ADTGoLocations l : canopyResponse.locations){
            if (latestDT == null || latestDT < l.created) {
                latestDT = l.created;
                results.loc.lat = decimal.valueOf(l.location.split(',')[0]);
                results.loc.lat.setScale(5);
                results.loc.lon = decimal.valueOf(l.location.split(',')[1]);
                results.loc.lon.setScale(5);
                if (l.speed <> null) results.loc.speed = decimal.valueOf(l.speed);
                if (l.heading <> null) results.loc.direction = l.heading;
                if (l.accuracy <> null) results.loc.accuracy = l.accuracy;
                results.updated = l.created;
            }
        }
        
        results.attr = canopyResponse.physicalAttributes;
        
		return results;
		
	}

    private static UserResults parseUserResults(String resJson){

		UserResults results = new UserResults();
		results.loc = new Location();
		results.batt = new Battery();
		results.dev = new Device();
		
		Jsonparser jp = JSON.createParser(resJson);
		while (jp.nextValue() != null){
			if (jp.getCurrentName() == 'status'){
				results.status = jp.getBooleanValue();
				if (!results.status){
					//break;
				}
			}
			else if (jp.getCurrentName() == 'first_name'){
				results.firstname = jp.getText();
			}
			else if (jp.getCurrentName() == 'last_name'){
				results.lastname = jp.getText();
			}
			else if (jp.getCurrentName() == 'email'){
				results.email = jp.getText();
			}
			else if (jp.getCurrentName() == 'latitude'){
				try {
					results.loc.lat = jp.getDecimalValue();
				} catch (Exception e) {
					results.loc.lat = decimal.valueOf(jp.getText());
				}
				results.loc.lat.setScale(5);
			}
			else if (jp.getCurrentName() == 'longitude'){
				try {
					results.loc.lon = jp.getDecimalValue();
				} catch (Exception e) {
					results.loc.lon = decimal.valueOf(jp.getText());
				}
				results.loc.lon.setScale(5);
			}
			else if (jp.getCurrentName() == 'updated_at'){
				results.updated = jp.getDateTimeValue();
			}
			/*
			else if (jp.getCurrentName() == 'speed'){
				results.loc.speed = jp.getDecimalValue();
				results.loc.lon.setScale(1);
			}
			else if (jp.getCurrentName() == 'direction'){
				results.loc.direction = jp.getText();
			}
			else if (jp.getCurrentName() == 'level'){
				results.batt.level = jp.getDecimalValue();
				results.loc.lon.setScale(1);
			}
			else if (jp.getCurrentName() == 'state'){
				results.batt.state = jp.getText();
			}
			else if (jp.getCurrentName() == 'offset'){
				results.loc.offset = jp.getIntegerValue();
			}
			else if (jp.getCurrentName() == 'last_device_type'){
				results.dev.type = jp.getText();
			}
			else if (jp.getCurrentName() == 'last_device_id'){
				results.dev.id = jp.getText();
			}
			else if (jp.getCurrentName() == 'carrier'){
				results.carrier = jp.getText();
			}
			*/
		}		
		
		return results;
		
	}
}