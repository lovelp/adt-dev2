global class ResendFailedRequestsToTelemar implements Schedulable {
   global void execute(SchedulableContext sc) {
      Date startDate = Date.today()-1;
      List<String> transactionTypestoReprocess=ResaleGlobalVariables__c.getValues('TransactionTypesToReprocess').value__c.split(';');
      List<RequestQueue__c> rQueues=[SELECT id,RequestStatus__c,ErrorDetail__c  
                                   FROM RequestQueue__c 
                                   WHERE createdDate>=:startDate  and 
                                         ServiceTransactionType__c IN:transactionTypestoReprocess and
                                         RequestStatus__c ='Error'];
    
      
      List<requestQueue__c> rqList=new List<RequestQueue__c>();
      integer count=0;
      for(RequestQueue__c rq:rQueues){
          
          for(String err:ResaleGlobalVariables__c.getValues('FilterRequestQueErrorstoReprocess').value__c.split(';')){
              if(rq.errordetail__c!=null && rq.errorDetail__c.contains(err)){
                  rq.RequestStatus__c='Ready';
                  rqList.add(rq);
                  count++;
                  break;   
              }
          }
          
          if(count>=Integer.valueof(ResaleGlobalVariables__c.getValues('RequestQueueItemstoProcess').value__c.trim())){
              break;
          }
          
          
      }
      system.debug(rQlist.size());
      Update rQlist;
      
      /*send an email*/
      //Number of totalErrorCount
      //rQlist.size()
      Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        //can be configured in custom settings
        message.toAddresses = ResaleGlobalVariables__c.getValues('RQProcessingEmailRecepients').value__c.split(';');
        message.plainTextBody = 'Total Number of request queue items: '+rQueues.size()+'\n';
        message.plainTextBody = message.plainTextBody+'Number of request queue items processed: '+rQlist.size()+'\n';
        message.subject= 'Failed Request queues to Telemar';
        Messaging.SingleEmailMessage[] messages = 
            new List<Messaging.SingleEmailMessage> {message};
                 Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: '
                  + results[0].errors[0].message);
        }
        
       if (sc != null)    
           system.abortJob(sc.getTriggerId()); 
   } 
}