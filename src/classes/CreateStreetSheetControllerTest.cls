@isTest
private class CreateStreetSheetControllerTest {
	
	static testMethod void testSaveNoSelection() {
		
		Test.startTest();
		
		
		List<Account> accountList = new List<Account>();
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(accountList);
		
		CreateStreetSheetController cssc = new CreateStreetSheetController(ssc);
		
		PageReference ref = cssc.save();
		
		System.assert(ref.getUrl().contains('ANS'), 'Expect to be navigated to error page with error ANS');
		
		Test.stopTest();
	}
	

	static testMethod void testSaveSelectedAccount() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		
		Account a;
		List<Account> accountList;
		System.runAs(salesRep) {
			a = TestHelperClass.createAccountData();
			accountList = [SELECT id, name FROM Account LIMIT 20];
		}
	
		Test.startTest();
		
		System.runAs(salesRep) {
		
			ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(accountList);
		
			CreateStreetSheetController cssc = new CreateStreetSheetController(ssc);
			
			cssc.selectedAccounts = accountList;
		
			PageReference ref = cssc.save();
		
			System.assert(ref != null, 'Expect to be navigated to Street Sheet View page');
		}
		Test.stopTest();
	}


	static testMethod void testSaveNoLeadSelection() {
		
		Test.startTest();
		
		
		List<Lead> leads = new List<Lead>();
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(leads);
		
		CreateStreetSheetLeadsController cssc = new CreateStreetSheetLeadsController(ssc);
		
		PageReference ref = cssc.createStreetSheet();
		
		System.assert(ref.getUrl().contains('LNS'), 'Expect to be navigated to error page with error LNS');
		
		Test.stopTest();
	}

	static testMethod void testSaveSelectedLead() {
		
		User salesRep;
		Lead l;
		User current = [Select Id from User where Id = :UserInfo.getUserID()];
		system.runas(current) {
			salesRep = TestHelperClass.createSalesRepUser();
			l = TestHelperClass.createLead(salesRep);
		}
	
		Test.startTest();
		
		System.runAs(salesRep) {
		
			ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new list<Lead>{l});
			ssc.setSelected(new list<Lead>{l});
		
			CreateStreetSheetLeadsController cssc = new CreateStreetSheetLeadsController(ssc);
		
			PageReference ref = cssc.createStreetSheet();
		
			System.assert(!ref.getUrl().contains('ShowError'), 'Expect to be navigated to street sheet page');
		}
		Test.stopTest();
	}
}