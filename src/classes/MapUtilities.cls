/************************************* MODIFICATION LOG ********************************************************************************************
* MapUtilities
*
* DESCRIPTION : Defines utility methods relating to points on a map represented by latitude and longitude coordinates.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2012			- Original Version
*
*													
*/

public with sharing class MapUtilities {

    //------------------------------------------------------------------------------------------------------------------------------
    //    Returns the bounding box. 
    //    Lat MIN, Lon Min, Lat Max, Lon Max.
    //    If there's an "error" return a non-valid bounding box (min > max)
    //------------------------------------------------------------------------------------------------------------------------------
    public static List<Decimal> getBoundingBox(MapItem i, Decimal d)
    {
        List<Decimal> latlons = new List<Decimal>();
        try
        {
            latlons.add( calcMinLat(i,d) );// Lat Min
            latlons.add( calcMinLon(i,d) );// Lon Min
            latlons.add( calcMaxLat(i,d) );// Lat Max
            latlons.add( calcMaxLon(i,d) );// Lon Max
        }
        catch(Exception e)
        {
            latlons.add( 1 );// Lat Min
            latlons.add( 1 );// Lon Min
            latlons.add( 0 );// Lat Max
            latlons.add( 0 );// Lon Max
        }
        return latlons; 
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    //    I will take a starting point and a distance and return the string Conditional statement.
    //    I will help you find all the Whatevers within the boundingBox. 
    //------------------------------------------------------------------------------------------------------------------------------
    public static String getBoundingCondition(MapItem startingPoint, Decimal d)
    {	
    	String condition;
    	
        List<Decimal> latLons = getBoundingBox(startingPoint,d);
    	System.debug('getBoundingCondition-----------d: ' + d);
    	System.debug('getBoundingCondition-----------latLons: ' + latLons);
    	
	    condition = ' ((AddressID__r.Latitude__c >= '+latLons[0] +' and AddressID__r.Longitude__c >= '+latLons[1]+
	                             ' and AddressID__r.Latitude__c <= '+latLons[2] +' and AddressID__r.Longitude__c <= '+latLons[3]+'))';

        return condition;
    }
    
    
    public static Decimal calcMaxLat(MapItem i, Decimal distance){
    	System.debug(i);
        Decimal lat = i.rLat;
        Decimal lon = i.rLon;
        
        Decimal maxLat = (distance + (69.1)* lat)/69.1;
        return Math.abs(maxLat);
    }

    public static Decimal calcMinLat(MapItem i, Decimal distance){
        Decimal max = calcMaxLat(i,distance);
        Decimal diff = max - i.rLat;
        
        return i.rLat-diff;
    }
    
    public static Decimal calcMaxLon(MapItem i, Decimal distance){
        Decimal lat = i.rLat;
        Decimal lon = i.rLon;
        Decimal L = Math.cos(lat/57.3);
        
        Decimal max = (distance + (L * 69.1*lon))/(L*69.1);
        return max; 
    }

    public static Decimal calcMinLon(MapItem i, Decimal distance){
        Decimal max = calcMaxLon(i,distance);
        Decimal diff = max - i.rLon;
        
        return i.rLon-diff;
    }
    
 	public static decimal getDistance(MapItem source, MapItem target){
 		
    	if (source.rLat !=null&&target.rLat !=null&& source.rLon !=null&& target.rLon !=null ) {
    		return getDistance(source.rLat, source.rLon, target.rLat, target.rLon);
    	}
    	else {
    		return 0;
    	}	
    } 
    
    public static decimal getDistance(Decimal point1Lat, Decimal point1Long, Decimal point2Lat, Decimal point2Long){
 		
    	
    	decimal distance;
    	decimal r=3963.1;//miles
    	decimal toRad=57.29577951;
    //	decimal kLat=point1Lat/toRad;
    //	decimal kLon=point1Long/toRad;
    //	decimal aLat=point2Lat/toRad;
    //	decimal aLon=point2Long/toRad;
    //	decimal dLat=(kLat-aLat);
    //	decimal dLon=(kLon-aLon);
    	
    	try {
 		//	decimal a = Math.sin(kLat)*math.sin(aLat)+math.cos(kLat)*math.cos(aLat)*math.cos(kLon-aLon);
 			decimal a = Math.sin(point1Lat/toRad)*math.sin(point2Lat/toRad)+math.cos(point1Lat/toRad)*math.cos(point2Lat/toRad)*math.cos(point1Long/toRad-point2Long/toRad);
    		decimal c = Math.acos(a);
    		distance = ( r* c).setScale(2);
    	} catch (Exception e) {
    		distance = 0;
    	}	
    	
		return distance;
    		
    }      


}