@isTest
public class ADTPartnerSiteCustomerLookupTest{
    
    public static opportunity opp;
    public static List<ProfileQuestion__c> pq;
    public static List<ProfileQuestionAnswer__c> pa;
    public static Call_Data__c cd;
    public static Affiliate__c aff;
    
    public static void createTestData (User u){   
        Equifax__c eq = new Equifax__c();
        eq.name = 'Order Types For Credit Check';
        eq.value__c = 'N1;R1';
        insert eq;
        
        ProposalOnlyStates__c Pos = new ProposalOnlyStates__c();
        Pos.name = 'States';
        Pos.States__c = 'IL,LA,MD,MA,MI,MS,SC,TN,TX,UT,VA,NV';
        insert Pos;
        
        // create accounts
        Account a1 = new Account ();
        a1 = TestHelperClass.createAccountData();
        a1.Business_Id__c = '1100 - Residential';
        update a1;
        
        //Opportunity opp;
        opp = New Opportunity();
        opp.AccountId = a1.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp;
        
        // create Lead
        Lead ld = new Lead ();
        ld = TestHelperClass.createLead (u, true, '');
        
        // create call data record
        cd = new Call_Data__c (name = 'TestCallData', Lead__c = ld.id, Contact_Type__c = 'Inbound Call');
        insert cd;
        
        // create Affiliate
        aff = new Affiliate__c (name = 'TestAffiliate', Active__c = true, Affiliate_Description__c = 'Test Affiliate');
        insert aff;
        
        
        // create Profile Question List
        pq = new List<ProfileQuestion__c> ();
        pq.add(new ProfileQuestion__c (Active__c = true, name= 'What is the square footage of your home?', Type__c= 'Text', LineOfBusiness__c = 'Small Business', OrderType__c='New; New Multi Site; Relo; Resale; Reinstatement; Resale Relocation; Resale Multi Site; Addon ' ));
        pq.add(new ProfileQuestion__c (Active__c = true, name= 'Does your existing system work?', Type__c= 'Radio', LineOfBusiness__c = 'Small Business', OrderType__c='New; New Multi Site; Relo; Resale; Reinstatement; Resale Relocation; Resale Multi Site; Addon ' ));
        pq.add(new ProfileQuestion__c (Active__c = true, name= 'How many users?', Type__c= 'Picklist' , DropDownValues__c = '0-2,2-4,4+', LineOfBusiness__c = 'Small Business', OrderType__c='New; New Multi Site; Relo; Resale; Reinstatement; Resale Relocation; Resale Multi Site; Addon'));
        insert pq;
        
        // create Profile Question Answer List
        pa = new List <ProfileQuestionAnswer__c> ();
        pa.add(new ProfileQuestionAnswer__c (Account__c= a1.id, Answer__c = '3000', ProfileQuestion__c = pq[0].id ));
        pa.add(new ProfileQuestionAnswer__c (Account__c= a1.id, Answer__c = 'Yes', ProfileQuestion__c = pq[1].id ));
        insert pa;
    }
    
    public static void createCustomSettingData (user u){   
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.MMBSearchUsername__c = 'salesforceclup';
        is.MMBSearchPassword__c = 'abcd1234';
        is.Equifax_EndPoint__c = 'https://dev2.api.adt.com/adtfico';
        is.Equifax_Username__c = 'ibmcpqoms';
        is.Equifax_Password__c = 'abcd1234';
        is.MMBSearchCalloutTimeout__c = 60000;
        is.MMBSearchEndpoint__c = 'https://dev.api.adt.com/adtclup';
        insert is;
        
        List<Equifax_Conditions__c> eqi = Equifax_Conditions__c.getall().values();
        
        list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
        rgvs.add(new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(name='IntegrationUser', value__c='Integration User'));
        rgvs.add(new ResaleGlobalVariables__c(name='RIFAccountOwnerAlias', value__c='ADT RIF'));
        rgvs.add(new ResaleGlobalVariables__c(name='DataConversion', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(name='AccountUpdateAPI_AllowedOrderTypes', value__c='N1;N4;R1;R3;A1'));
        rgvs.add(new ResaleGlobalVariables__c(name='NewRehashHOARecordOwner', value__c= u.username));
        rgvs.add(new ResaleGlobalVariables__c(Name='GlobalResaleAdmin', value__c='Testing'));
        rgvs.add(new ResaleGlobalVariables__c(Name='Alternate Pricing Model Order Types', value__c='N1;R1'));
        rgvs.add(new ResaleGlobalVariables__c(Name='P1RententionPhNumber', value__c='1800-000-0000'));
        rgvs.add(new ResaleGlobalVariables__c(Name='P1HOAPhNumber', value__c='1800-000-0000'));
        rgvs.add(new ResaleGlobalVariables__c(Name='HOATeamPhone', value__c='1800-000-0000'));
        rgvs.add(new ResaleGlobalVariables__c(name='RvServiceabilityOrderTypes', value__c='N1;N2;N3;N4;A1;R1;R2;R3;R4'));
        insert rgvs;
        
        List<Equifax__c> efx = new List<Equifax__c> ();
        efx.add(new Equifax__c (name= 'Default Condition Code' , value__c= 'CAE1'));
        efx.add(new Equifax__c (name= 'Default Risk Grade' , value__c= 'W'));
        insert efx;
        
        list<PartnerAPIMessaging__c> partapilist = new list<PartnerAPIMessaging__c>();
        partapilist.add(new PartnerAPIMessaging__c(name='400',API_Name__c = 'invalid request'));
        partapilist.add(new PartnerAPIMessaging__c(name='404',API_Name__c= 'Opty not found'));
        partapilist.add(new PartnerAPIMessaging__c(name='500',API_Name__c= 'Something went wrong while processing the request. Please try again later.'));
        partapilist.add(new PartnerAPIMessaging__c(name='OptyLeadConvert',API_Name__c= 'Opportunity Id not found. Lead associated to call ID is converted to Account ID'));
        partapilist.add(new PartnerAPIMessaging__c(name='ResaleEligibleSite',API_Name__c= 'SiteCustomerLookupAPI'));
        partapilist.add(new PartnerAPIMessaging__c(name='SiteBillingPrefixInformix',API_Name__c= 'SiteBillingPrefixInformix'));
        partapilist.add(new PartnerAPIMessaging__c(name='BlockedSiteCustomerTypeId',API_Name__c= 'BlockedSiteCustomerTypeId'));
        partapilist.add(new PartnerAPIMessaging__c(name='CustomerNull',API_Name__c= 'CustomerNull'));
        partapilist.add(new PartnerAPIMessaging__c(name='MustSetFieldSalesAppointment',API_Name__c= 'MustSetFieldSalesAppointment'));
        //partapilist.add(new PartnerAPIMessaging__c(name='CustomerNull',API_Name__c= 'CustomerNull'));
        //partapilist.add(new PartnerAPIMessaging__c(name='CustomerNull',API_Name__c= 'CustomerNull'));
        
        insert partapilist;
        
        list<PartnerSettings__c> partset = new list<PartnerSettings__c>();
        partset.add(new PartnerSettings__c(ValidUSStateCodes__c='AL;AK;AZ;AR;CA;CO;CT;DE;DC;FL;GA;HI;ID;IL;IN;IA;KS;KY;LA;ME;MD;MA;MI;MN;MS;MO;MT;NE;NV;NH;NJ;NM;NY;NC;ND;OH;OK;OR;PA;PR;RI;SC;SD;TN;TX;UT;VT;VA;VI;WA;WV;WI;WY',AccountUpdateAllowedOrderTypes__c='R1;R2;A1;R3;R4;N1;N2;N3;N4;R4'));
      
        insert partset;
        
        List<ResaleGlobalVariables__c> rgv=new list<ResaleGlobalVariables__c>();
        rgv.add(new ResaleGlobalVariables__c(name='MMBNameLength',value__c ='28'));
        rgv.add(new ResaleGlobalVariables__c(name='MMBAccountNameLength',value__c ='30'));

        insert rgv;
        
        PartnerConfiguration__c partnerConfig = new PartnerConfiguration__c();
        partnerConfig.PartnerMessage__c = 'Transfer to ADT';
        partnerConfig.AllowedOrderType__c = 'N1;R1';
        partnerConfig.PartnerID__c = 'Partner001';
        partnerConfig.SiteCustomerLookupAPI__c = 'Loan Application;Serviceability;Credit Rating;Opportunity Id;MMB Lookup';
        insert partnerConfig;
        
        Equifax_Conditions__c ec = new Equifax_Conditions__c();
        ec.Agent_Message__c = 'test \n';
        ec.name = 'CAE1';
        ec.EasyPay_Required__c = true;
        ec.Payment_Frequency__c = 'A';
        ec.Deposit_Required_Percentage__c = 100;
        insert ec;
    }
    
    // Positive scenario
    public static testMethod void TestDoPostMethod(){
        User u = TestHelperClass.createUser(1001);
        System.runAs(u){
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);
            
            // Set the request body
        	String sBody = '{ "partnerId": "Partner001","callId": "' +cd.id+ '" ,"opportunityID": "' + opp.id+ '","partnerRepName": "John Smith"}';
            String rBody = '{"opportunityID": "0061300001O8exI","message": "Update accepted"}';
            String SiteIds = '46121245';
            system.debug('@@sBody: '+sBody);        
            
            // Set the Mock class for MMB Lookup
            Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteResponseGenerator());   
            
            RestRequest req = new RestRequest();
            //HttpResponse res = new HttpResponse(); 
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            Test.startTest();
            ADTPartnerSiteCustomerLookupAPI.doPost();
            
            HttpResponse response = new HttpResponse();
            String responseString=siteInformationResponse();
            system.debug(responseString);
            response.setBody(responseString);
            response.setStatusCode(200);
            response.setHeader('Content-Type', 'application/xml');
            Test.stopTest();
        }
    }
     private static Call_Data__c createcallData(){
        Call_Data__c cData = new Call_Data__c();
        cData.name = 'test call';
        insert cData;
        return cData;
    }
    
    //create new lead
    public static Lead createNewLead(){
        Postal_Codes__c pc = new Postal_Codes__c ();
        pc.Name = '32212';
        pc.BusinessID__c ='1100';
        insert pc;
        
        Lead l = new Lead();
        l.FirstName = 'John';
        l.LastName = 'Smith';
        l.Phone = '904- 999- 1234';
        l.Channel__c = 'Resi Direct Sales';
        l.Business_Id__c ='1100 - Residential';
        l.SiteStreet__c = '101 Main Street';
        l.SiteStreet2__c = 'Unit 3';
        l.SiteStreetName__c = 'First Street';
        l.SiteStreetNumber__c = '101';
        l.SiteCity__c = 'Jacksonville';
        l.SiteStateProvince__c = 'FL';
        l.SiteCounty__c = 'Duval';
        l.SiteCountryCode__c = 'US';
        l.IncomingLatitude__c = 30.332184;
        l.IncomingLongitude__c = -81.60647;
        l.SitePostalCode__c = '32212';
        l.SitePostalCodeAddOn__c = '1234';
        l.SiteValidated__c = True;
        l.SiteValidationMsg__c= 'test';
        l.PostalCodeID__c = pc.id;
        insert l;
        return l;
    }
    
    public static Address__c createAddr(){
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '101 Main Street';
        addr.City__c = 'Jacksonville';
        addr.State__c = 'FL';
        addr.PostalCode__c = '32212';
        addr.OriginalAddress__c = '101 MAIN STREET+JACKSONVILLE+FL+32212';
        
        insert addr;
        return addr;
    }
    
    
    public static Account CreateAccount(Address__c addr){
        Account acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'test';
        acct.FirstName__c = 'testfirst';
        acct.LastName__c ='testlast';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.PostalCodeID__c = TestHelperClass.inferPostalCodeID(addr.PostalCode__c,'1100');
        acct.DispositionCode__c = 'test';
        acct.ShippingPostalCode = '32212';
        acct.Data_Source__c = 'TEST';
        
        acct.Phone = '904-999-1234';
        acct.ShippingPostalCode = '22102';
        acct.EquifaxApprovalType__c = 'CAE1';
        insert acct;
              
        /*acct.Phone = '904-999-1234';
        acct.ShippingPostalCode = '22102';
        acct.EquifaxApprovalType__c = 'CAE1';
        update acct;*/
        return acct;
    }
    
    public static Opportunity createOpp(account acct){
        Opportunity opp = New Opportunity();
        opp.AccountId = acct.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp;
        return opp;
    }
     public static void createTestData(){
        list<PartnerAPIMessaging__c> apiMsglist = new list<PartnerAPIMessaging__c>();
        apiMsglist.add(new PartnerAPIMessaging__c(name='400', API_Name__c = 'All APIs', Error_Message__c = 'Invalid request'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='404', API_Name__c = 'All APIs', Error_Message__c = 'Opportunity not found'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='500', API_Name__c = 'All APIs', Error_Message__c = 'Something went wrong while processing the request. Please try again later.'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='AccountNewSale', API_Name__c = 'SiteCustomerLookupAPI', Error_Message__c = 'This Customer Account is not available for replacetype and will be booked as a New quote - New Sale'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='AddOnNoActiveContract', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Customer does not have any active contract for Add On')); 
        apiMsglist.add(new PartnerAPIMessaging__c(name='    BlockedSiteCustomerTypeId', API_Name__c = 'SiteCustomerLookupAPI', Error_Message__c = 'Site or Customer Type not allowed for sale'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='CellGuardNotAllowed', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'DLL Appointment is not allowed for this Radio'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='ConversionAllowed', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Conversion allowed for this customer site selection'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='CustomerNull', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Customer information missing, please retry.'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='ResaleEligibleSite', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Site eligible for Resale'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='HOA', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Must Set Field Sales Appointment'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='MustSetFieldSalesAppointment', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Must Set Field Sales Appointment'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='P1-CHS', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'P1-CHS is not allowed'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='P1MMBNotAllowedForSelectBoth', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = '   Select both is not allowed for P1MMB customers'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='PanelExistsButZonesNotAllowed', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Phone Activation is not Allowed.Panel Exists but Zone and Cell guard is not present.'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='SiteBillingPrefixInformix', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'INF'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='SiteBillingPrefixMMB', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'MMB'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='SiteBillingPrefixP1MMB', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'P1'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='TexasSmokeDectorMessage', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Phone activation is not allowed. You have Fire Smoke Detector Installed. We will send a technician to activate your System.'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='TransferP1Site', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'P1 customer/site please transfer call to NSC'));    
        apiMsglist.add(new PartnerAPIMessaging__c(name='TexasZoneCommentMessage', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Must set Field Sales Appt'));  
        apiMsglist.add(new PartnerAPIMessaging__c(name='ZoneCommentMessage', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Must set Field Sales Appt'));  
         
        insert apiMsglist;
       
        Equifax__c eq1 = new Equifax__c();
        eq1.name = 'Default Risk Grade';
        eq1.value__c = 'W';
        insert eq1;
        Equifax__c eq2 = new Equifax__c();
        eq2.name = 'Default Condition Code';
        eq2.value__c = 'CAE1';
        insert eq2;
        
        list<ResaleGlobalVariables__c> rgvList = new list<ResaleGlobalVariables__c>();
        rgvList.add(new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger', value__c='FALSE'));
        rgvList.add(new ResaleGlobalVariables__c(name='IntegrationUser', value__c='Integration User'));
        rgvList.add(new ResaleGlobalVariables__c(name='RIFAccountOwnerAlias', value__c='ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c(name='DataConversion', value__c='FALSE'));
        User u = TestHelperClass.createAdminUser();
        rgvList.add(new ResaleGlobalVariables__c(name='NewRehashHOARecordOwner', value__c=u.username));
        rgvList.add(new ResaleGlobalVariables__c(name='RvServiceabilityOrderTypes', value__c='N1;N2;N3;N4;A1;R1;R2;R3;R4'));
        rgvList.add(new ResaleGlobalVariables__c(name='DaysToPopulateDNISOnAccount', value__c='1'));
        rgvList.add(new ResaleGlobalVariables__c(name='GlobalResaleAdmin',value__c = 'Global Admin'));
        insert rgvList;
                    
        PartnerSettings__c ps = new PartnerSettings__c();
        ps.accountLookupLimit__c = 5;
        ps.AllowedCusTypeId__c = 'SMB,RES,I,A';
        ps.AllowedSiteTypeId__c = 'B,C,N,Q,R,S,W';
        ps.AccountUpdateAllowedOrderTypes__c = 'N4;R3;A1';
        ps.MMB_Search_Results_Limit__c  = 5;
        ps.NewSaleBasedOnSiteTypeId__c = 'W';
        ps.SiteBillingSystems__c = 'INF, Informix Billed, P1MMB, MMB, MMB-Residential, MMB-Small Business';
        ps.ValidUSStateCodes__c = 'AL;AK;AZ;AR;CA;CO;CT;DE;DC;FL;GA;HI;ID;IL;IN;IA;KS;KY;LA;ME;MD;MA;MI;MN;MS;MO;MT;NE;NV;NH;NJ;NM;NY;NC;ND;OH;OK;OR;PA;PR;RI;SC;SD;TN;TX;UT;VT;VA;VI;WA;WV;WI;WY';
        insert ps;
                  
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.MMBSearchUsername__c = 'salesforceclup';
        is.MMBSearchPassword__c = 'abcd1234';
        is.Equifax_EndPoint__c = 'https://dev2.api.adt.com/adtfico';
        is.Equifax_Username__c = 'ibmcpqoms';
        is.Equifax_Password__c = 'abcd1234';
        is.MMBSearchCalloutTimeout__c = 60000;
        is.MMBSearchEndpoint__c = 'https://dev.api.adt.com/adtclup';
        insert is;
                       
        Equifax_Conditions__c ec = new Equifax_Conditions__c();
        ec.Agent_Message__c = 'test \n';
        ec.name = 'CAE1';
        ec.EasyPay_Required__c = true;
        ec.Payment_Frequency__c = 'A';
        ec.Deposit_Required_Percentage__c = 100;
        insert ec;
        
        PartnerConfiguration__c partnerConfig = new PartnerConfiguration__c();
        partnerConfig.PartnerMessage__c = 'Transfer to ADT';
        partnerConfig.AllowedOrderType__c = 'N1;R1';
        partnerConfig.PartnerID__c = 'RV';
        insert partnerConfig;
    }
     public static testMethod void SiteCustomerLookupAPI(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u = TestHelperClass.createUser(3333);
        System.runAs(u) {    
               
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            Opportunity opp = createOpp(acct);
            call_data__c cd = createcallData();
            cd.lead__c = le.id;
            update cd;
                
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('Test');//'{"partnerId" : "RV" ,"callId" : "' + cd.id+ '" ,"opportunityId" :"' + opp.id+ '" ,"partnerRepName" : "John Smith"}') ;
                           
            RestContext.request = req;
            RestContext.response = res;
            
            Test.starttest();
            // Using MMBCustomerSiteResponseGenerator
            Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteResponseGenerator());   
            Test.stoptest();
            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    
    static String siteInformationResponse(){
    	string xmlBodyStr='{  "siteCustomerLookupResponse": {    "opportunityId": "0061300001O8exI",    "creditRating": {      "dateRetrieved": "2017-08-01",      "riskGrade": A",      "approvalType": "APPROV",      "paymentFrequency": "Annual",      "depositPercent": 25,      "multiPayAllowed": true,      "multiPayTerms": [        [          "3",          "24",          "36"        ]      ],      "easyPayRequired": true,      "message": "Standard Pricing Market: Approved, No Payment Requirements $$$Pricing Pilot Market: Refer to Pilot Job Aid for Terms and Payment Options"    },    "sites": [      {        "id": 630199231,        "status": "InService",        "name": "Smiths Feed Supplies",        "addrLine1": "101 Main Street",        "addrLine2": "Apt 101",        "city": "Jacksonville",        "state": "FL",        "postalCode": "32212",        "postalCodeAddOn": "1234",        "siteType": "RESI - Residential",        "siteFlags": [          [            "HOA",            "BHT",           "CHS"          ]        ],        "systemNumber": 31903910,        "csNumber": "U93831873",        "pulseService": "PULSE_3 - Remote with Video",        "systemType": "FSVF",        "panelType": "Safewatch 3000 (V20P)",        "lifeSafetyFlags": [          [            "Smoke",            "CODetection",            "CellBackup"          ]        ],        "servicePlan": "1019CR",        "zones": [          {            "id": "01",            "description": "BA-WINDOW(S)",            "comment": "MASTER BR"          },          {            "id": "22",            "description": "BA-BURG-DOOR",            "comment": "FRONT DOOR"          },          {            "id": "22",            "description": "FA-FIRE-SMOKE DET",            "comment": "LIV RM"          }       ],        "radio": "3G4000RF-ADTUSA",        "customer": {          "id": 68624817,          "status": "Active",          "name": {            "first": "John",            "last": "Smith",            "company": "Smiths Feed Supplies"          },          "addrLine1": "101 Main Street",          "addrLine2": "Suite 100",          "city": "Jacksonville",          "state": "FL",          "postalCode": "32212",          "postalCodeAddOn": "1234",          "billingSystem": "MMB",          "customerType": "MMB-Residential",          "pastDueDays": 0,          "contract": {            "number": 9481949,            "status": "Active",            "startDate": "2017-01-01",            "expirationDate": "2019-01-01",            "disco": {              "date": "2016-12-01",              "reason": "SVC Never Effective/ Never Inst",              "non-pay": 0            },            "pastDue": {              "status": "61-90 Days Past Due",              "balance": 101.98            },            "currentBalance": 0          }        },        "selectionRules": [          {            "ruleType": "siteSelection",            "allowed": true,            "message": "Site eligible for Resale",            "orderType": "R1"          },          {            "ruleType": "customerSiteSelection",            "allowed": false,            "message": "Paste Due Balance - Not eligible - Transfer",            "orderType": "A1"          },          {            "ruleType": "customerSelection",            "allowed": false,            "message": "Not eligible for selection",            "orderType": null          },          {            "ruleType": "phoneActivation",            "allowed": false,            "message": "Cellguard Radio needs to be replaced",            "orderType": null          }        ]      }    ],    "orderServiceability": [      {        "orderType": "N1",        "description": "New Customer and Site",        "orderAllowed": true,        "orderMessage": null,        "appointmentAllowed": true,        "appointmentMessage": null      },      {        "orderType": "N2",        "description": "Relocation New Site",        "orderAllowed": false,        "orderMessage": null,        "appointmentAllowed": true,        "appointmentMessage": "Transfer to ADT"      },      {        "orderType": "N3",        "description": "MultiSite",        "orderAllowed": false,        "orderMessage": null,        "appointmentAllowed": true,        "appointmentMessage": "Transfer to ADT"      },      {        "orderType": "N4",        "description": "Conversion",        "orderAllowed": false,        "orderMessage": null,        "appointmentAllowed": true,        "appointmentMessage": "Transfer to ADT"      },      {        "orderType": "A1",        "description": "Add-On",        "orderAllowed": false,        "orderMessage": null,        "appointmentAllowed": false,        "appointmentMessage": "Add On Orders are not allowed in this area - Out Of Market"      },      {        "orderType": "R1",        "description": "Resale",        "orderAllowed": true,        "orderMessage": null,        "appointmentAllowed": true,        "appointmentMessage": null      },      {        "orderType": "R2",        "description": "Relocation Resale",        "orderAllowed": false,        "orderMessage": null,        "appointmentAllowed": true,        "appointmentMessage": "Transfer to ADT"      },      {        "orderType": "R3",        "description": "Reinstatement",        "orderAllowed": false,        "orderMessage": null,        "appointmentAllowed": true,        "appointmentMessage": "Transfer to ADT"      },      {        "orderType": "R4",        "description": "Reinstatement Resale",        "orderAllowed": false,        "orderMessage": null,        "appointmentAllowed": true,        "appointmentMessage": "Transfer to ADT"      }    ]  }}';
        return xmlBodyStr;
    }
}