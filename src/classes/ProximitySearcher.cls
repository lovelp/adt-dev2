/************************************* MODIFICATION LOG ********************************************************************************************
* ProximitySearcher
*
* DESCRIPTION : An abstract class defining the base algorithm for finding nearby accounts and leads for the Locator and Cloverleaf tools.
*               Use of this class is intended to shield controller classes from details of the search algorithm
*               Subclasses should override findNearbyAccounts and/or findNearbyLeads as appropriate
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            3/23/2012           - Original Version
*
*                                                   
*/

public abstract class ProximitySearcher
{   
    protected String whereString;         
    
    protected final String NEARBY_ACCOUNT_BASE_QUERY = 'Select  OwnerId, Id, Name, Latitude__c,Longitude__c, Phone, Type, ProcessingType__c, Channel__c,'+
                                         ' InService__c, NewMover__c, UnassignedLead__c, NewMoverType__c, Data_Source__c, TenureDate__c, ANSCSold__c, SystemProfileCode__c, ServiceProfileCode__c, DisconnectReason__c, DisconnectDate__c, '+
                                         '  DispositionDate__c, DispositionCode__c, DispositionDetail__c, SiteStreet__c, SiteCity__c, SalesAppointmentDate__c, ScheduledInstallDate__c, DateAssigned__c, '+
                                         ' SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, Owner.Name, RecordTypeId, AccountStatus__c, LastActivityDate__c'+
                                         ' from Account  ';
                                         
    protected final String NEARBY_LEAD_BASE_QUERY = 'Select  OwnerId, Id, Name, Company, Latitude__c,Longitude__c, Phone, Channel__c, '+
                                         ' NewMoverType__c, NewMover__c, UnassignedLead__c, LeadSource, DisconnectDate__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, DateAssigned__c,'+
                                         '  SiteStreet__c, SiteCity__c, SiteStateProvince__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, Type__c, Owner.Name '+
                                         ' from Lead  ';                              
    
    public abstract String getName();
    public String SearchType{get;set;}

    public String expandButtonText{get;set;}
    public Integer publicnumberOfProspects;
    public Decimal nextDistance = 0;
    public virtual list<MapItem> findNearby(Decimal latitude, Decimal longitude, Decimal distance, User currUser, Boolean isSalesRep, Boolean isManager, Boolean isCloverLeaf)
    {   
        list<MapItem> mapItemList = new list<MapItem>();
        Integer numberOfProspects = 0;
        
        
        ProximitySearchSettings__c pss = ProximitySearchSettings__c.getInstance(currUser.ProfileId);
        // retrieve all accounts that are within a latitude and longitude bounding box calculated from the initial distance of 1 mile
        String boundingCondition;
        String endLimit;
        list<Account> accountList;
        list<Lead> leadsList;
        // create a MapItem from the lat and long
        MapItem startingPoint = new MapItem(latitude, longitude);
        system.debug('Search type is '+searchType);
        if(String.isBlank(SearchType)){       
            if(isCloverLeaf)
            {               
                pss.MaximumItems__c = pss.CloverleafMaxItemLimit__c;            
                pss.InitialDistance__c = pss.CloverleafDistance__c;             
            }
            
            System.debug('pss....: '+ pss);
                    
            
            
            boundingCondition = MapUtilities.getBoundingCondition(startingPoint, pss.InitialDistance__c);
            endLimit = String.valueOf(Math.roundToLong(pss.FirstRowLimit__c));
            
            accountList = findNearbyAccounts(boundingCondition, endLimit, currUser, isSalesRep, isManager);
            leadsList = findNearbyLeads(boundingCondition, endLimit, currUser, isSalesRep, isManager);
            
            if(accountList != null )
            {               
                numberOfProspects += accountList.size();        
            }
            
            if(leadsList != null)       
            {
                numberOfProspects += leadsList.size();  
            }       
            /*
            Commented by Shiva Pochamalla for reducing lead locater processing time*/
            if(!isCloverLeaf)
            {      
                system.debug('Num Prospects '+numberOfProspects );     
                system.debug('Max Items '+pss.MaximumItems__c );   
                // If searching in the 1 mile bounding box did not return the maximum number of accounts            
                if (numberOfProspects < pss.MaximumItems__c) 
                {     
                    
                    //setbutton and number of prospects
                    expandButtonText='First Density Increment';
                    publicnumberOfProspects=numberofProspects;
                    /*
                              
                    // determine how big the next bounding box should be
                    if (numberOfProspects > pss.FirstItemThreshold__c) 
                    {
                        // Have more than half the accounts needed, so accounts are quite dense at this location
                        // Increase the size of the bounding box by 1.5 times the initial size
                        nextDistance = pss.InitialDistance__c * pss.FirstHighDensityIncrement__c;
                    } 
                    else
                    {
                        // Have less than half the accounts needed, so account density is lower here
                        // Increase the size of the bounding box by 2 times the initial size
                        nextDistance = pss.InitialDistance__c * pss.FirstLowDensityIncrement__c;
                    }
                    
                    mapItemList.clear();
                    accountList.clear();
                    leadsList.clear();
                    numberOfProspects = 0;
                    
                    // retrieve all accounts that are within a latitude and longitude bounding box calculated from next distance
                    boundingCondition = MapUtilities.getBoundingCondition(startingPoint, nextDistance);
                    endLimit = String.valueOf(Math.roundToLong(pss.SecondRowLimit__c));
                    
                    accountList = findNearbyAccounts(boundingCondition, endLimit, currUser, isSalesRep, isManager);
                    leadsList = findNearbyLeads(boundingCondition, endLimit, currUser, isSalesRep, isManager);
                                
                    if(accountList != null )
                    {               
                        numberOfProspects += accountList.size();        
                    }
                    
                    if(leadsList != null)       
                    {
                        numberOfProspects += leadsList.size();  
                    }  
                    */                 
                }
                
                
                /*
                // If searching in the second bounding box did not return the maximum number of accounts and 
                // the supplied distance parameter would provide a third bigger bounding box, try one more search
                if (numberOfProspects < pss.MaximumItems__c && distance > nextDistance) 
                {               
                    // determine how big the next bounding box should be
                    if(numberOfProspects > pss.SecondItemThreshold__c)
                    {
                        // Have some of the accounts needed, so account density suggests 
                        // the maximum bounding box may not be needed
                        // Increase the size of the bounding box by an increment of the initial  size
                        nextDistance = nextDistance * pss.SecondHighDensityIncrement__c;
                    }
                    else 
                    {
                        // Below the threshold so search in a box defined by the supplied maximum
                        nextDistance = distance;
                    }   
                                    
                    mapItemList.clear();
                    accountList.clear();
                    leadsList.clear();
                    numberOfProspects = 0;
                    
                    // retrieve all accounts that are within a latitude and longitude bounding box calculated from next distance
                    boundingCondition = MapUtilities.getBoundingCondition(startingPoint, nextDistance);
                    endLimit = String.valueOf(Math.roundToLong(pss.ThirdRowLimit__c));
                                
                    accountList = findNearbyAccounts(boundingCondition, endLimit, currUser, isSalesRep, isManager);
                    leadsList = findNearbyLeads(boundingCondition, endLimit, currUser, isSalesRep, isManager);
                    
                    if(accountList != null )
                    {               
                        numberOfProspects += accountList.size();        
                    }
                    
                    if(leadsList != null)       
                    {
                        numberOfProspects += leadsList.size();  
                    }       
                } 
                */              
            }
             
        
        }else if(SearchType=='First Density Increment'){
                if (publicnumberOfProspects > pss.FirstItemThreshold__c) 
                {
                    // Have more than half the accounts needed, so accounts are quite dense at this location
                    // Increase the size of the bounding box by 1.5 times the initial size
                    nextDistance = pss.InitialDistance__c * pss.FirstHighDensityIncrement__c;
                } 
                else
                {
                    // Have less than half the accounts needed, so account density is lower here
                    // Increase the size of the bounding box by 2 times the initial size
                    nextDistance = pss.InitialDistance__c * pss.FirstLowDensityIncrement__c;
                }
                
                // retrieve all accounts that are within a latitude and longitude bounding box calculated from next distance
                boundingCondition = MapUtilities.getBoundingCondition(startingPoint, nextDistance);
                endLimit = String.valueOf(Math.roundToLong(pss.SecondRowLimit__c));
                
                accountList = findNearbyAccounts(boundingCondition, endLimit, currUser, isSalesRep, isManager);
                leadsList = findNearbyLeads(boundingCondition, endLimit, currUser, isSalesRep, isManager);
                            
                if(accountList != null )
                {               
                    numberOfProspects += accountList.size();        
                }
                
                if(leadsList != null)       
                {
                    numberOfProspects += leadsList.size();  
                }   
                publicNumberofProspects=numberofProspects;
                
                system.debug('Num Prospects Expand1 '+publicNumberOfProspects );     
                system.debug('Max Items Expand1 '+pss.MaximumItems__c ); 
                system.debug('Distance '+distance); 
                system.debug('Next distance '+nextDistance);
                if (publicNumberOfProspects < pss.MaximumItems__c && distance > nextDistance) 
                { 
                    

                    expandButtonText='Second Density Increment';
                
                }else
                    expandButtonText='';
            }else{
                  system.debug('Second increment '+expandButtonText);
                  system.debug('Number of prospects '+publicNumberOfProspects );
                  system.debug('Second item threshold '+pss.SecondItemThreshold__c );
                  system.debug('Second high density increment '+pss.SecondHighDensityIncrement__c);
                  system.debug('Next distance '+nextDistance);
                  system.debug('Distance '+distance);
                // determine how big the next bounding box should be
                    if(publicNumberOfProspects > pss.SecondItemThreshold__c)
                    {
                        // Have some of the accounts needed, so account density suggests 
                        // the maximum bounding box may not be needed
                        // Increase the size of the bounding box by an increment of the initial  size
                        nextDistance = nextDistance * pss.SecondHighDensityIncrement__c;
                    }
                    else 
                    {
                        // Below the threshold so search in a box defined by the supplied maximum
                        nextDistance = distance;
                    }   
                                    
                    
                    
                    // retrieve all accounts that are within a latitude and longitude bounding box calculated from next distance
                    boundingCondition = MapUtilities.getBoundingCondition(startingPoint, nextDistance);
                    endLimit = String.valueOf(Math.roundToLong(pss.ThirdRowLimit__c));
                                
                    accountList = findNearbyAccounts(boundingCondition, endLimit, currUser, isSalesRep, isManager);
                    leadsList = findNearbyLeads(boundingCondition, endLimit, currUser, isSalesRep, isManager);
                    system.debug('Account list is '+accountList);
                    if(accountList != null )
                    {               
                        numberOfProspects += accountList.size();        
                    }
                    
                    if(leadsList != null)       
                    {
                        numberOfProspects += leadsList.size();  
                    } 
                    
                    publicNumberofProspects=numberofProspects;
                    expandButtonText='';
            }
        
        // determine account result threshold's based on the user's business unit
        Decimal outOfServicePercent = 100;
        Decimal revenueInForcePercent = 100;
        Map<String, AccountResultThreshold__c> thresholdMap = AccountResultThreshold__c.getAll();
        AccountResultThreshold__c threshold = thresholdMap.get(currUser.Business_Unit__c);
        if (threshold != null) {
            outOfServicePercent = threshold.OutOfServicePercent__c;
            revenueInForcePercent = threshold.RevenueInForcePercent__c; 
        }
    

        
        DistanceMap dMap = new DistanceMap(accountList,leadsList, distance ,startingPoint); 
        mapItemList = dMap.getProspectsList(pss.MaximumItems__c, outOfServicePercent, revenueInForcePercent);   
        system.debug('Number of prospects-'+numberofProspects); 
        system.debug('Map Item List-'+mapItemList.size());                              
        return mapItemList;
    }   
    
    protected virtual list<Lead> findNearbyLeads(String boundingCondition, String endLimit, User currUser, Boolean isSalesRep, Boolean isManager)
    {
        list<Lead> leadList = new list<Lead>();
        
        return leadList;
    }   
    
    protected virtual list<Account> findNearbyAccounts(String boundingCondition, String endLimit, User currUser, Boolean isSalesRep, Boolean isManager)
    {   
        list<Account> accountList = new list<Account>();       
        
        return accountList;
    }   
    
     
}