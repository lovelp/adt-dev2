@RestResource (urlMapping = '/setInstallAppointment/*')

global class ADTPartnerSetDLLAppointmentAPI {
    @HttpPost
    global static void doPost ()
    {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        
        ADTPartnerSetDLLAppointmentController setDLLAppointmentController = new ADTPartnerSetDLLAppointmentController ();
        
        system.debug('@@request: '+request.requestBody.toString());
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(setDLLAppointmentController.parseJSONRequest (request.requestBody.toString())));
        
        response.statusCode = setDLLAppointmentController.statusCode;
    }

}