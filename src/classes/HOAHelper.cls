/************************************* MODIFICATION LOG ********************************************************************************************
* HOAHelper
*
* DESCRIPTION : Helper class for processing information.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel herrera             	09/24/2014			- Original Version
*
*													
*/

public class HOAHelper {
	
	private static Boolean HOATriggerFired = true; 
	
	public static Boolean runHOATrigger(){
		if ( HOATriggerFired ){
			HOATriggerFired = false;
			return true;
		}
		else{
			return HOATriggerFired;
		}
	}
	
	public enum HOASource {LEAD, ACCOUNT} 
	
	public static Boolean isHOARecord(Account a){
		try{
			return isHOARecord(a.Channel__c); 
		}
		catch(Exception err){
			a.addError(err.getMessage());
		}
		return false;
	}
			
	public static Boolean isHOARecord(Lead l){
		try{
			return isHOARecord(l.Channel__c); 
		}
		catch(Exception err){
			l.addError(err.getMessage());
		}
		return false;
	}	
	
	private static Boolean isHOARecord(String HOAChannel){
		return ( !Utilities.isEmptyOrNull(HOAChannel) && HOAChannel == Channels.TYPE_HOADIRECTSALES );
	}
	
	public static Boolean isHOAMaster(Lead l){
		return isHOAMaster(l.MMBMasterCustomerNumber__c, l.HOA_Type__c, l.MMBCustomerNumber__c, l.MMBSiteNumber__c, l.Channel__c);
	}
	public static Boolean isHOAMaster(Account a){
		return isHOAMaster(a.MMBMasterCustomerNumber__c, a.HOA_Type__c, a.MMBCustomerNumber__c, a.MMBSiteNumber__c, a.Channel__c);
	}	
	private static Boolean isHOAMaster(String MMBMasterCustomerNumber, String HOA_Type, String MMBCustomerNumber, String MMBSiteNumber, String Channel){
		if( !Utilities.isEmptyOrNull(HOA_Type) && HOA_Type != 'Association') return false;
		return ( MMBMasterCustomerNumber == MMBCustomerNumber && !Utilities.isEmptyOrNull(MMBCustomerNumber));
	}
	
	public static Boolean isHOACustomerSite(Account a){
		return isHOACustomerSite(a.MMBMasterCustomerNumber__c, a.HOA_Type__c, a.MMBCustomerNumber__c, a.Channel__c);
	}
	public static Boolean isHOACustomerSite(Lead l){
		return isHOACustomerSite(l.MMBMasterCustomerNumber__c, l.HOA_Type__c, l.MMBCustomerNumber__c, l.Channel__c);
	}
	private static Boolean isHOACustomerSite(String MMBMasterCustomerNumber, String HOA_Type, String MMBCustomerNumber, String Channel){
		if( !Utilities.isEmptyOrNull(HOA_Type) && HOA_Type != 'Homeowner/Site') return false;
		return ( !Utilities.isEmptyOrNull(MMBMasterCustomerNumber) && MMBMasterCustomerNumber != MMBCustomerNumber && Channel == Channels.TYPE_HOADIRECTSALES);
	}
	
	public static void setHOAOwnership(Lead l){
        l.OwnerId = Utilities.getHOARehashOwner();		
	} 
	
	/**
	 *	Process a lead conversion of HOA records
	 *	@param List<Lead> List of leads to convert
	 */
	public static void ProcessHOALeadConversion(List<Lead> AssociationSiteList){
		LeadConversion lc = new LeadConversion(AssociationSiteList, true, null);
		lc.convertLead(); 
	}
	
	/**
	 *	Process a list of hoa master records and re-assign child list of sites if any
	 *	@param List<Account>	List of association accounts 
	 */
	public static void ProcessHOAMasterInfo(List<Account> AssociationSiteList){
		List<Account> SiteByAssociation = new List<Account>();
		Map<String, String> HOAParentByCustomerNo = new Map<String, String>();
		for(Account a: AssociationSiteList){
			String mhoa = a.MMBCustomerNumber__c;
			if(!Utilities.isEmptyOrNull(mhoa)){
				HOAParentByCustomerNo.put(mhoa, a.Id);
			}
		}
		
		for(Account a: [SELECT HOA_Association__c FROM Account WHERE MMBMasterCustomerNumber__c IN :HOAParentByCustomerNo.keySet()]){
			a.HOA_Association__c = HOAParentByCustomerNo.get( a.MMBMasterCustomerNumber__c );
			SiteByAssociation.add(a);							
		}
		
		if( SiteByAssociation != null && !SiteByAssociation.isEmpty() ){
			update SiteByAssociation;
		}
	}
	
	public static void ProcessHOASiteInfo(List<Account> AssociationSiteList){
		Map<String, List<Account>> SiteByAssociationMap = new Map<String, List<Account>>();
		
		// Group all sites by Master Association record 
		for(Account a: AssociationSiteList){
			String mhoa = a.MMBMasterCustomerNumber__c;
			if(!Utilities.isEmptyOrNull(mhoa)){
				List<Account> tempList = new List<Account>();
				if( SiteByAssociationMap.containsKey(mhoa) ){
					tempList = SiteByAssociationMap.get(mhoa);
				}
				tempList.add(a);
				SiteByAssociationMap.put(mhoa, tempList);
			}
		}
		
		// Link every site to his master record
		for(Account assocA: [SELECT Id, Name, MMBCustomerNumber__c FROM Account WHERE MMBCustomerNumber__c <> null AND MMBCustomerNumber__c IN :SiteByAssociationMap.keySet() ]){
			for(Account siteA: SiteByAssociationMap.get(assocA.MMBCustomerNumber__c)){
				siteA.HOA_Association__c = assocA.Id;
			}
			List<Account> tempList = SiteByAssociationMap.remove(assocA.MMBCustomerNumber__c);
		}
		
		// Not erroring out, uncomment if it becomes a requirement
		// Error out every site included here which couldn't locate a master record
		//for(String k: SiteByAssociationMap.keySet()){
		//	for(Account SiteObj :SiteByAssociationMap.get(k)){
		//		SiteObj.addError('Home Owner Association site could not locate Master record.');
		//	}
		//}
		
	}

	public static Account readAccountHOADetail( String Id ){
		return [ SELECT Name, FirstName__c, LastName__c, AddressID__c, Longitude__c, Latitude__c, MMBCustomerNumber__c, MMBSiteNumber__c, MMB_TownID__c, ResaleRegion__c, ResaleDistrict__c, ResaleTown__c,
						TelemarAccountNumber__c, SiteStreet__c, SiteCity__c, SitePostalCode__c, SiteCounty__c, SiteCrossStreet__c, SiteSubdivision__c, SiteCountryCode__c,
						Phone, PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email__c, Spouse__c, PromotionCode__c, PromotionDescription__c, TelemarLeadSource__c, GenericMedia__c, QueriedSource__c,
						Affiliation__c, IncomingLatitude__c, Channel__c, AccountStatus__c
				 FROM Account 
				 WHERE Id = :Id ];
	} 
	
	public static Lead readLeadHOADetail( String Id ){
		return [ SELECT Name, FirstName, LastName, AddressID__c, Longitude__c, Latitude__c, MMBCustomerNumber__c, MMBSiteNumber__c, Town__c, Region__c, District__c, 
						TelemarAccountNumber__c, SiteStreet__c, SiteCity__c, SitePostalCode__c, SiteCounty__c, SiteCrossStreet__c, SiteSubdivision__c, SiteCountryCode__c,
						Phone, PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email, Spouse__c, PromotionCode__c, PromotionDescription__c, TelemarLeadSource__c, GenericMedia__c, QueriedSource__c,
						Affiliation__c, IncomingLatitude__c, Channel__c, AccountStatus__c, SiteStateProvince__c, IncomingCINotes__c, SourceAccountCreateDate__c, SourceCriteriaID__c,
						SourceCriteriaDescription__c, SourceExtractDate__c, ExpirationDate__c, Type__c, LeadSource        
				 FROM Lead 
				 WHERE Id = :Id ];
	}
	 
	
	

}