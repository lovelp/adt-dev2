/**
 Description- This test class used for CallidusCallController.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class CallidusCallControllerTest{
    
    static testMethod void pageInitTest() {
    
        test.startTest();
        CallidusCallController controller = new CallidusCallController();
        PageReference result= controller.init();
        system.assertEquals(null, result);
        test.stopTest();

    }
}