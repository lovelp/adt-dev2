/************************************* MODIFICATION LOG ********************************************************************************************
* 
* LoanProgramMessageJSON
*
* DESCRIPTION :
* Generates the JSON String which contains the following objects data:
*	 1. Loan Decision Request Message
*	 2. Accept Loan Request Message
*	 3. Update App Request Message
*	 4. Loan Decision Response Message
*	 5. Accept Loan Response Message
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER										 DATE						 Ticket				 REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* 
* Jitendra Kothari						11/16/2018				HRM-8524		Generate Loan Decision Request Message
* Siju Varghese                         12/05/2018           
*/

public class LoanProgramMessageJSON {
	//public final static String DATE_FORMAT = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
	public final static String DATE_FORMAT = 'yyyy-MM-dd HH:mm:ss';
	//Loan Decision Request Message Wrapper
	public class LoanDecisionRequestMessage{
		public CitizensLoanDecisionRequest citizensLoanDecisionRequest{get;set;}
	}
	
	public class CitizensLoanDecisionRequest {
		public String requestId;
		public String requestDttm;
		public String sourceChannel;
		public String loanType;
		public String storeId;
		public String IPaddress;
		public String transactionType;
		public Integer retryCounter;
		public Boolean eDocSignConsent;
		public Boolean termsAndConditionsConsent;
		public String termsAndConditionsConsentDttm;
		public String termsAndConditionsDocumentVersion;
		public String employeeId;
		public String loanAppId;
		public String clientDef1;
		public String clientDef2;
		public String clientDef3;
		public String clientDef4;
		public String clientDef5;
		public String originalLoanNbr;
		//public String autoBookFlg;
		public boolean autoBookFlg;
		public CustomerInformation customerInformation;
		public OrderInformation orderInformation;
		public CitizensLoanDecisionRequest(){
			//requestDttm = system.now().format(DATE_FORMAT);
			requestDttm = system.now().formatGmt(DATE_FORMAT);
		//	sourceChannel = ZootApiConfig__c.getValues('Loan Decision Req').sourceChannel__c;//'ONLINE';
		//	transactionType = ZootApiConfig__c.getValues('Loan Decision Req').TransactionType__c;//'SALE';
		//	retryCounter = integer.valueOf(ZootApiConfig__c.getValues('Loan Decision Req').RetryCounter__c);
		//	autoBookFlg = ZootApiConfig__c.getValues('Loan Decision Req').AutoBookFlg__c;
		}
	}
	
	public class AddressInformation {
		public String addressLine1;
		public String addressLine2;
		public String city;
		public String stateCd;
		public String postalCd;
		public String countryCd;
		public AddressInformation(Address__c add){
			addressLine1 = add.Street__c;
			addressLine2 = add.Street2__c;
			city = add.City__c;
			stateCd = add.State__c;
			postalCd = add.PostalCode__c;
			countryCd = add.CountryCode__c;
		}
		public AddressInformation(){
		}

	}

	public class PersonalInformation {
		public String firstNm;
		public String lastNm;
		public String birthdate;
		public String taxId;
		public String emailAddress;
		public String homePhoneNbr;
		public String cellPhoneNbr;
		public Integer annualIncome;
		public PersonalInformation(Account acc){
			firstNm = acc.FirstName__c;
			lastNm = acc.LastName__c;
			birthdate = acc.DOB_encrypted__c;
			emailAddress = acc.Email__c;
		}
		public PersonalInformation(){
		}
	}

	public class CustomerInformation {
		public PersonalInformation personalInformation;
		public PaymentInformation paymentInformation;
		public AddressInformation billingAddress;
		public AddressInformation shippingAddress;
		public AddressInformation previousAddress;
	}

	public class OrderDetails {
		public String productId;
		public String productDesc;
		public String partNumber;
		public Integer quantity;
		public Decimal unitPrice;
	}

	public class OrderInformation {
		public String orderNbr;
		public String orderDttm;
		public String financePlanType;
		public Integer term;
		public Decimal interestRate;
		public Decimal amtFinanced;
		public Decimal salesTaxAmt;
		public Decimal initialPaymentAmt;
		public Decimal installmentAmt;
		public Decimal purchasePriceAmt;
		public Decimal shippingAmt;
		public Decimal otherFeeAmt;
		public List<OrderDetails> orderDetails;
	}

	public class PaymentInformation {
		public String profileId;
		public String ccCVV;
	}
	
	//Loan Decision Response Message Elements
	public class CitizensLoanDecisionResponse_Z {
		public CitizensLoanDecisionResponse citizensLoanDecisionResponse;
	}
	
	public class CitizensLoanDecisionResponse {
		public String responseId;
		public String responseDttm;
		public String requestId;
		public String loanAppId;
		public String orderNbr;
		public String loanNbr;
		public String clientDef1;
		public String clientDef2;
		public String clientDef3;
		public String clientDef4;
		public String clientDef5;
		//public Integer creditScore;
		public String creditScore;
		public DecisionInformation decisionInformation;
		public LoanDetails loanDetails;	
	}
	
	public class DecisionInformation {
		public String decisionStatus;
		public String loanStatus;
		public String decisionReasonCd;
		public String decisionReasonDesc;
		public Boolean retryAllowed;
	}

	public class LoanDetails {
		public String financePlanType;
		public Integer term;
		public Decimal interestRate;
		public Decimal amtFinanced;
		public Decimal salesTaxAmt;
		public Decimal initialPaymentAmt;
		public Decimal installmentAmt;
		public Decimal purchasePriceAmt;
		public Decimal shippingAmt;
		public Decimal otherFeeAmt;
	}
	
	//Accept Loan Request Message
	public class AcceptLoanRequestMessage{
		public CitizensLoanAcceptRequest citizensLoanAcceptRequest{get;set;}
	}
	
	public class CustomerApplicationValidation {
		public String firstNm;
		public String lastNm;
		public String birthdate;
		public String taxId;
		public CustomerApplicationValidation(Account acc){
			firstNm = acc.FirstName__c;
			lastNm = acc.LastName__c;
			birthdate = acc.DOB_encrypted__c;
			//taxId = acc.Profile_TaxID__c;
		}
		public CustomerApplicationValidation(){
		}    
	}

	public class CitizensLoanAcceptRequest {
		public String requestId;
		public String requestDttm;
		public Integer retryCounter;
		public String loanAppId;
		public Boolean eDocSignConsent;
		public Boolean termsAndConditionsConsent;
		public String termsAndConditionsConsentDttm;
		public String termsAndConditionsDocumentVersion;
		public String clientDef1;
		public String clientDef2;
		public String clientDef3;
		public String clientDef4;
		public String clientDef5;
		public CustomerApplicationValidation customerApplicationValidation;
		public CitizensLoanAcceptRequest(){
			//requestDttm = system.now().format(DATE_FORMAT);
			requestDttm = system.now().formatGmt(DATE_FORMAT);
			//retryCounter = integer.valueOf(ZootApiConfig__c.getValues('Accept Loan Req').RetryCounter__c);
			/*if(ZootApiConfig__c.getValues('Loan Decision Req').eDocSignConsent__c == TRUE){
				eDocSignConsent = ZootApiConfig__c.getValues('Loan Decision Req').eDocSignConsent__c;
			}*/
		}
	}
	
	//Loan Accept Response Message
	public class CitizensLoanAcceptResponseMessage {
		public CitizensLoanAcceptResponse citizensLoanAcceptResponse;
	}

	public class CitizensLoanAcceptResponse {
		public String responseId;
		public String responseDttm;
		public String requestId;
		public String loanAppId;
		public String clientDef1;
		public String clientDef2;
		public String clientDef3;
		public String clientDef4;
		public String clientDef5;
		public String loanNbr;
		public DecisionInformation decisionInformation;
		public LoanDetails loanDetails;
	}
	
	//Update App Request Message
	public class UpdateAppRequestMessage{
		public CitizensUpdateAppRequest citizensUpdateAppRequest{get;set;}
	}

	public class CitizensUpdateAppRequest {
		public String requestId;
		public String requestDttm;
		public Integer retryCounter;
		public String loanAppId;
		public String clientDef1;
		public String clientDef2;
		public String clientDef3;
		public String clientDef4;
		public String clientDef5;
		public CustomerApplicationValidation customerApplicationValidation;
		public OrderInformation orderInformation;
		public CitizensUpdateAppRequest(){
			//requestDttm = system.now().format(DATE_FORMAT);
			requestDttm = system.now().formatGmt(DATE_FORMAT);
			//retryCounter = 1;
			//retryCounter = integer.valueOf(ZootApiConfig__c.getValues('Update App Req').RetryCounter__c);
			
		}
	}
	
	//Update App Response Message
	
	public class CitizensUpdateAppResponse_Z {
		public CitizensUpdateAppResponse citizensUpdateAppResponse;
	}
	
	public class CitizensUpdateAppResponse {
		public String responseId;
		public String responseDttm;
		public String requestId;
		public String loanAppId;
		public String clientDef1;
		public String clientDef2;
		public String clientDef3;
		public String clientDef4;
		public String clientDef5;
		public String orderNbr;
		public DecisionInformation decisionInformation;
		public LoanDetails loanDetails;
	}
	
	public class Errors {
		public String status;
		public String errorCode;
		public String errorMessage;
	}
	
	public class ErrorWrapper {
		public List<Errors> errors;
	}
	
	
	
	//Cancel App Request Message Elements
	/*public class CancelAppRequestMessage{
		public CitizensCancelAppRequest citizensCancelAppRequest;
	}

	public class CitizensCancelAppRequest {
		public String requestId;
		public String requestDttm;
		public Integer retryCounter;
		public String loanAppId;
		public String orderNbr;
		public String cancelReasonCd;
		public CustomerApplicationValidation customerApplicationValidation;
		public CitizensCancelAppRequest(){
			requestDttm = system.now().format(DATE_FORMAT);
			retryCounter = 3;
		}
	}
	
	//Cancel App Request Message
	public class CitizensCancelAppResponse_Z {
		public CitizensCancelAppResponse citizensCancelAppResponse;
	}
	
	public class CitizensCancelAppResponse {
		public String responseId;
		public String responseDttm;
		public String requestId;
		public String loanAppId;
		public String orderNbr;
		public DecisionInformation decisionInformation;
	}*/
}