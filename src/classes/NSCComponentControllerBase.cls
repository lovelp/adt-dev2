public virtual class NSCComponentControllerBase {

  public NSCPageControllerBase pageController { get; 
    set {
      if (value != null) {
		pageController = value;
		pageController.setComponentController(this);
      }
    }
  }
  
}