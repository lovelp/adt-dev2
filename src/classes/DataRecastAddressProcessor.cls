/************************************* MODIFICATION LOG ********************************************************************************************
* DataRecastAddressProcessor
*
* DESCRIPTION : Update addresses from phase 1
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* SahilGrover				  4/2/2012				- Original Version
*
*													
*/

global class DataRecastAddressProcessor implements Database.Batchable<sObject>, Database.Stateful {

	public String query = 'Select ID, CountryCode__c ' +
						  ' from Address__c ' +
						  ' Where ( ' +
						  ' CountryCode__c = null ' +
						  ') ';

	global Database.QueryLocator start( Database.Batchablecontext bc ) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext bc, List<sObject> scope) {
		DataRecastHelper.processAddresses((List<Address__c>)scope);
		update scope;
	}
	
	global void finish(Database.BatchableContext bc) {
		
	}

}