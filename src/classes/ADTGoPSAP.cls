/************************************* MODIFICATION LOG ********************************************************************************************
* ADTGoMapController
* 
DESCRIPTION : Obtains all dynamic data required by the ADTGo Map Display
*             It is a Controller in the SFDC MVC architecture. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Mike Tuckman    				8/15/13				- Original Version
*
*													
*/


public with sharing class ADTGoPSAP {
	
	private static ADTGoGeoCode.ADTGoGeoCodeRevResults revResults;
    
	public class PSAPResults {
        public String PSAPID;
        public String FCCID;
        public String PSAPType;
        public String CountyName;
        public String CountyFIPS;
        public String Agency;
        public String CoverageArea;
        public String CoverageExceptions;
        public String CoverageComments;
        public String PSAPComments;
        public String OperatorPhone;
        public String ContactPrefix;
        public String ContactFirstName;
        public String ContactLastName;
        public String ContactTitle;
        public String ContactPhone;
        public String ContactFax;
        public String ContactEmail;
        public String ContactComments;
        public String MailingStreet;
        public String MailingCity;
        public String MailingState;
        public String MailingZip;
        public String SitePhone;
        public String SiteFax;
        public String SiteStreet;
        public String SiteCity;
        public String SiteState;
        public String SiteZip;
        public String Latitude;
        public String Longitude;
        public String PolicePhone;
        public String FirePhone;
        public String EMSPhone;
        }
    
	public static PSAPResults getPSAP (Decimal lat, Decimal lon){
        
        HttpRequest req = new HttpRequest();

		String url = ADTGoSetting.getSetting('ADTUrl') + '/ahj/getahhjlookup';
		req.setEndpoint(url);
		req.setMethod('POST');

        String jsonReq = '{"getAHJlookup": {"AddressLine1":"","AddressLine2":"","City":"","StateProvince":"","PostalCode":"","Latitude":"' + lat.toPlainString() + '" , "Longitude":"' + lon.toPlainString() + '"} }';
		system.debug('**jsonReq: ' + jsonReq);
        system.debug('URL: ' + url);
		String auth = setAuthentication();
		req.setHeader('Authorization', auth);
		req.setHeader('Content-Type', 'application/json');
		req.setBody(jsonReq);
		System.debug('Request...:' + req.getBody());
		Http http = new Http();
		Httpresponse res = new Httpresponse();
        If (!Test.isRunningTest()) {
			res = http.send(req);
		}
		else {
			res = new Httpresponse();
			res.setStatusCode(200);
			StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ADTGoResponse4' LIMIT 1];
			res.setBody(sr.Body.toString());
		}
		system.debug(res);
        system.debug(res.getBody());
		PSAPResults resPSAP = parseResults(res);
		
		System.debug('Results' + resPSAP);		
		
		return resPSAP;
	}
	/*
	public static PSAPResults getPSAP (ADTGoGeoCode.ADTGoGeoCodeRevResults revResults){
                
        HttpRequest req = new HttpRequest();

		String url = ADTGoSetting.getSetting('ADTUrl') + '/ahj/getahhjlookup';
		req.setEndpoint(url);
		req.setMethod('POST');

        String jsonReq = '{"getAHJlookup": {"AddressLine1":"' + revResults.streetNumber + ' ' + revResults.streetName +
            '", "City":"' + revResults.city + '", "StateProvince":"' + revResults.state + '", "PostalCode":"' + revResults.zipcode + '"} }';
        
		String auth = setAuthentication();
		req.setHeader('Authorization', auth);
		req.setHeader('Content-Type', 'application/json');
		req.setBody(jsonReq);
		System.debug('Request...:' + req.getBody());
		Http http = new Http();
		Httpresponse res = http.send(req);
        System.debug('Response...:' + res.getBody());
		
		PSAPResults resPSAP = parseResults(res);
		
		System.debug('Results' + resPSAP);		
		
		return resPSAP;
	}
	*/
	
	private static String setAuthentication (){
		
		String user = ADTGoSetting.getSetting('ADTUser');
		String pass = ADTGoSetting.getSetting('ADTPassword');
		system.debug('AHJUser:' + user + ' ADTPassword:' + pass);
		return setAuthentication(user, pass);
		
	}
	
	private static String setAuthentication (String user, String pass){
		
		Blob headerValue = Blob.valueOf(user + ':' + pass);
		String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
		
		return authorizationHeader;
		
	}
	
	private static PSAPResults parseResults(Httpresponse res){

		PSAPResults psap = new PSAPResults();
        
        String responseBody = res.getBody();

        Jsonparser jp = JSON.createParser(responseBody);
		String tempValue;
		String tempValueShort;
		while (jp.nextValue() != null){

            if (jp.getCurrentName() == 'PSAPID') psap.PSAPID = jp.getText();
            if (jp.getCurrentName() == 'PsapFccId') psap.FCCID = jp.getText();
            if (jp.getCurrentName() == 'PSAPType') psap.PSAPType = jp.getText();
            if (jp.getCurrentName() == 'PsapCountyName') psap.CountyName = jp.getText();
            if (jp.getCurrentName() == 'PsapCountyFIPS') psap.CountyFIPS = jp.getText();
            if (jp.getCurrentName() == 'PsapAgency') psap.Agency = jp.getText();
            if (jp.getCurrentName() == 'PsapCoverageArea') psap.CoverageArea = jp.getText();
            if (jp.getCurrentName() == 'PsapCoverageExceptions') psap.CoverageExceptions = jp.getText();
            if (jp.getCurrentName() == 'PsapCoverageComments') psap.CoverageComments = jp.getText();
            if (jp.getCurrentName() == 'PSAPComments') psap.PSAPComments = jp.getText();
            if (jp.getCurrentName() == 'PsapOperatorPhone') psap.OperatorPhone = jp.getText();
            if (jp.getCurrentName() == 'PsapContactPrefix') psap.ContactPrefix = jp.getText();
            if (jp.getCurrentName() == 'PsapContactFirstName') psap.ContactFirstName = jp.getText();
            if (jp.getCurrentName() == 'PsapContactLastName') psap.ContactLastName = jp.getText();
            if (jp.getCurrentName() == 'PsapContactTitle') psap.ContactTitle = jp.getText();
            if (jp.getCurrentName() == 'PsapContactPhone') psap.ContactPhone = jp.getText();
            if (jp.getCurrentName() == 'PsapContactFax') psap.ContactFax = jp.getText();
            if (jp.getCurrentName() == 'PsapContactEmail') psap.ContactEmail = jp.getText();
            if (jp.getCurrentName() == 'PsapContactComments') psap.ContactComments = jp.getText();
            if (jp.getCurrentName() == 'PsapMailingStreet') psap.MailingStreet = jp.getText();
            if (jp.getCurrentName() == 'PsapMailingCity') psap.MailingCity = jp.getText();
            if (jp.getCurrentName() == 'PsapMailingState') psap.MailingState = jp.getText();
            if (jp.getCurrentName() == 'PsapMailingZip') psap.MailingZip = jp.getText();
            if (jp.getCurrentName() == 'PsapSitePhone') psap.SitePhone = jp.getText();
            if (jp.getCurrentName() == 'PsapSiteFax') psap.SiteFax = jp.getText();
            if (jp.getCurrentName() == 'PsapSiteStreet') psap.SiteStreet = jp.getText();
            if (jp.getCurrentName() == 'PsapSiteCity') psap.SiteCity = jp.getText();
            if (jp.getCurrentName() == 'PsapSiteState') psap.SiteState = jp.getText();
            if (jp.getCurrentName() == 'PsapSiteZip') psap.SiteZip = jp.getText();
            if (jp.getCurrentName() == 'Latitude') psap.Latitude = jp.getText();
            if (jp.getCurrentName() == 'Longitude') psap.Longitude = jp.getText();
            if (jp.getCurrentName() == 'PoliceAlarmPhone') psap.PolicePhone = jp.getText();
            if (jp.getCurrentName() == 'FireAlarmPhone') psap.FirePhone = jp.getText();
            if (jp.getCurrentName() == 'EmsAlarmPhone') psap.EMSPhone = jp.getText();
        }
				
		return psap;
		
	}	

}