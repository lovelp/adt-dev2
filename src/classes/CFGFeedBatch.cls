/************************************* MODIFICATION LOG ********************************************************************************************
* 
* Name: CFGFeedBatch
*
* DESCRIPTION :
* This batch class will process feed
* 1.  Loan Application
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                        DATE                        Ticket              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari               06/07/2019              	  HRM-9790	        FlexFI National - Ability to vie Loan info
* 
********************************************************************************************************************************************************/

global class CFGFeedBatch implements Database.Batchable<SObject>, Database.stateful{
	@TestVisible private Integer totalRecsproc = 0;
    @TestVisible private Integer totalFailedRecs = 0;
    global Database.QueryLocator start(Database.BatchableContext bc) {
    	String queryString = 'Select Id, LoanApplicationNumber__c, FirstName__c, LastName__c, Order_Number__c, Previous_Status__c,Status__c ' + 
    						 'From Loan_Close_Feed__c Where LoanApplicationNumber__c != null AND Partner__c = \'CFG\' ' +
            				 'AND ProcessingStatus__c =\'Not Started\' ' +
            				 ' order by LoanApplicationNumber__c asc, createddate desc';
    	return Database.getQueryLocator(queryString);
    }
    global void execute(Database.BatchableContext bc, List<Loan_Close_Feed__c> records){
    	try{
    		//If we expects only one record for loan account
    		map<String, Loan_Close_Feed__c> loanIdToCFG = new map<String, Loan_Close_Feed__c>();
            list<Loan_Close_Feed__c> feedList = new list<Loan_Close_Feed__c>();
    		for(Loan_Close_Feed__c record : records){
    			loanIdToCFG.put(record.LoanApplicationNumber__c, record);
    		}
    		map<Id, LoanApplication__c> loanApps = new map<Id, LoanApplication__c>([Select Id, LoanNumber__c, CFG_Loan_Status__c, CFG_Status_Update_Timestamp__c 
    												From LoanApplication__c Where LoanNumber__c in :loanIdToCFG.keyset()]);
    		system.debug('loanApps'+loanApps);
            if(!loanApps.isEmpty()){ 
                for(LoanApplication__c loanApp : loanApps.values()){
                    if(loanIdToCFG.keyset().contains(loanApp.LoanNumber__c)){
                        loanApp.CFG_Loan_Status__c = loanIdToCFG.get(loanApp.LoanNumber__c).Status__c;
                        loanApp.CFG_Status_Update_Timestamp__c = system.now();
                    }
                }
                Database.SaveResult[] resultList = Database.update(loanApps.values(), false);
                //list<Loan_Close_Feed__c> cfgForDelete = new list<Loan_Close_Feed__c>();
                for(Integer i = 0; i < resultList.size(); i++){
                    String key = loanApps.get(resultList[i].getId()).LoanNumber__c;
                    Loan_Close_Feed__c feed = loanIdToCFG.get(key);
                    loanIdToCFG.remove(key);
                    if(resultList[i].isSuccess()){
                        //cfgForDelete.add(feed);
                        feed.ProcessingStatus__c = 'Completed';
                        //feedList.add(feed);
                        totalRecsproc++;
                    }else{
                        feed.ProcessingStatus__c = 'Error';
                        Database.Error[] errors = resultList[i].getErrors();
                        if(!errors.isEmpty()){
                            feed.ProcessingError__c = errors[0].getStatusCode() + ': ' + errors[0].getMessage();
                        }else{
                            feed.ProcessingError__c = 'Error While updating Loan Application Record Status.';
                        }
                        //feedList.add(feed);
                        totalFailedRecs++;
                    }
                    feedList.add(feed);
                }
            }
    		/*if(!cfgForDelete.isEmpty()){
                Database.DeleteResult[] dllist = Database.delete(cfgForDelete, false);
           	}*/
            for(Loan_Close_Feed__c feed : loanIdToCFG.values()){
                feed.ProcessingStatus__c = 'Error';
                feed.ProcessingError__c = 'No matching Loan Application number is found for this Loan Application Number.';
                feedList.add(feed);
                totalFailedRecs++;
            }
            if(!feedList.isEmpty()){
            	update feedList;
            }
    	}catch(Exception ae){
            ADTApplicationMonitor.log ('Bulk load job failure', ae.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);
            totalFailedRecs += records.size();
    	}
    }
    global void finish(Database.BatchableContext bc){
    	// execute any post-processing operations
        //Send an email to the Manager if it cannot be assigned to Sales Rep
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(userinfo.getUserId());
        mail.setSubject('CFG Feed load Batch Process Complete');
        mail.setPlainTextBody('The batch processing has been completed. Total records proccessed: '+totalRecsproc+'. Total records failed: '+totalFailedRecs);
        mail.setSaveAsActivity(false);
        Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }
}