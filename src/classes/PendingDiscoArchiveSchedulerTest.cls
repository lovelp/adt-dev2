/**
 Description- This test class used for PendingDiscoArchiveScheduler.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class PendingDiscoArchiveSchedulerTest {
    
    static testMethod void test1() {
       
        PendingDiscoArchiveScheduler es= new PendingDiscoArchiveScheduler ();
        String chron = '0 0 23 * * ?';
        
        Test.startTest();
        System.schedule('Test PendingDiscoArchiveScheduler', chron, es);
        Test.stopTest();  
        
    }
}