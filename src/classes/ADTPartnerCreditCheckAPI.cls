/************************************* MODIFICATION LOG ********************************************************************************************
* ADTPartnerCreditCheckAPI - getPartnerCredit
*
* DESCRIPTION : Rest URI to recieve the json from DP and repond back with Credit score
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*     SY              9/26/2017      - Original Version
* 
*                          
*/


@RestResource(urlMapping='/checkCredit/*')
global with sharing class ADTPartnerCreditCheckAPI{

    @HttpPost
    global static void dopostmethod(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        ADTPartnerCreditCheckAPIController apcreditCheck = new ADTPartnerCreditCheckAPIController();
        ADTPartnerCreditCheckAPIController.responseWrapper resObj = apcreditCheck.parseDPRequest(req.requestBody.toString());
        res.addHeader('Content-Type', 'application/json');
        if(String.isBlank(req.requestBody.toString())){
            res.statusCode = 400;
            ADTPartnerCreditCheckAPIController.creditRating cr = new ADTPartnerCreditCheckAPIController.creditRating();
            cr.Message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
            resObj.creditRating = cr;
            res.responseBody = Blob.valueOf(JSON.serialize(resObj, true));
        }
        else{
            Map<String, Object> reqMap = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());
 
            set<String> requiredParamSet = new set<String>{'opportunityId','partnerId','partnerRepName','callId'};
            Boolean reqNotPresent = false;
            for(String str:requiredParamSet){
                if(reqMap.get(str) == null || reqMap.get(str) == '' ){
                    reqNotPresent = true;
                }
            }
            //Best valid case
            if(reqMap.size()>0 && !reqNotPresent){
                res.statusCode = apcreditCheck.statusCode;
                res.responseBody = Blob.valueOf(JSON.serialize(resObj, true));
            }
            else{
                res.statusCode = 400;
                ADTPartnerCreditCheckAPIController.creditRating cr = new ADTPartnerCreditCheckAPIController.creditRating();
                cr.Message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                resObj.creditRating = cr;
                res.responseBody = Blob.valueOf(JSON.serialize(resObj, true));
            }
        }
        
    }
    
}