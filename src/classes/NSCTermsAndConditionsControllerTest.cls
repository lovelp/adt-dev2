@isTest
private class NSCTermsAndConditionsControllerTest {
    private static testMethod void test() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
            
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'TRUE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        insert rgvList;
        
        List<FlexFiMessaging__c> flexiFiMessaging = new List<FlexFiMessaging__c>();
        FlexFiMessaging__c msg = new FlexFiMessaging__c(Name = 'LoanUpdateFailed', ErrorMessage__c = 'Application Update Failed');
        flexiFiMessaging.add(msg);
        
        FlexFiMessaging__c mess400 = new FlexFiMessaging__c();
        mess400.name = '400';
        mess400.ErrorMessage__c = 'Invalid input or Bad request';
        mess400.ErrorCode__c = '000';
        flexiFiMessaging.add(mess400);
        
        
        FlexFiMessaging__c mess404 = new FlexFiMessaging__c();
        mess404.name = '404 - Account';
        mess404.ErrorMessage__c = 'No Account found';
        mess400.ErrorCode__c = '001';
        flexiFiMessaging.add(mess404);
        
        FlexFiMessaging__c ssn404 = new FlexFiMessaging__c();
        ssn404.name = '404 - SSN';
        ssn404.ErrorMessage__c = 'No Account found with the corresponding SSN/DOB';
        ssn404.ErrorCode__c = '002';
        flexiFiMessaging.add(ssn404);
        
        FlexFiMessaging__c mess500 = new FlexFiMessaging__c();
        mess500.name = '500';
        mess500.ErrorMessage__c = 'Something went wrong while processing the request. Please try again later.';
        ssn404.ErrorCode__c = '003';
        flexiFiMessaging.add(mess500);
        insert flexiFiMessaging;
        
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.name = '22102';
        pc.BusinessID__c = '1100';
        insert pc;
        
        Account a = TestHelperClass.createAccountData();
        a.Business_Id__c = '1100 - Residential';
        a.Channel__c = 'Resi Direct Sales';
        a.MMBOrderType__c = 'R1';
        a.PostalCodeId__c = pc.Id;
        a.MMBCustomerNumber__c = '300010540';
        a.MMBSiteNumber__c = '48857037';
        a.Equifax_Last_Check_DateTime__c = Datetime.now();
        a.EquifaxApprovalType__c = 'APPROV';
        a.TelemarAccountNumber__c = '12345';
        a.EquifaxRiskGrade__c = 'A';
        a.Email__c = 'test@test.com';
        update a;
        
        Equifax_Conditions__c cs = new Equifax_Conditions__c();
        cs.Name = 'APPROV';
        cs.Three_Pay_Allowed__c= true;
        insert cs;
        
        List<PartnerAPIMessaging__c> partnerAPIMessagingData = new List<PartnerAPIMessaging__c>();
        PartnerAPIMessaging__c pm = new PartnerAPIMessaging__c();
        pm.Name = '400';
        pm.Error_Message__c = 'Invalid input or Bad request';
        partnerAPIMessagingData.add(pm);
        
        PartnerAPIMessaging__c pm500 = new PartnerAPIMessaging__c();
        pm500.Name = '500';
        pm500.Error_Message__c = 'Something went wrong while processing the request. Please try again later.';
        partnerAPIMessagingData.add(pm500);
        
        PartnerAPIMessaging__c pm404 = new PartnerAPIMessaging__c();
        pm404.Name = '404';
        pm404.Error_Message__c = 'requestID not found.';
        partnerAPIMessagingData.add(pm404);
        insert partnerAPIMessagingData;
        
        LoanApplication__c lApp = new LoanApplication__c();
        lApp.Account__c = a.id;
        lApp.MMBJobNumber__c = '1';
        lApp.LastZootUpdatedDateTime__c = datetime.now();
        lApp.SubmittedDateTime__c = datetime.now();
        lApp.DecisionStatus__c = 'APPROVED';
        lApp.SSN__c = '123456789';
        lApp.DOB__c = '01/18/90';
        lApp.SubmittedDateTime__c = Datetime.now();
        lApp.LastZootUpdatedDateTime__c = Datetime.now();
        insert lApp;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(lApp);
        NSCTermsAndConditionsController controller = new NSCTermsAndConditionsController(sc);
    }

}