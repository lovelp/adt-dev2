/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RedirectControllerTest {

    static testMethod void testAdminEventRedirect() {
        Event e;
        User u;
        Account a;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
        	u = TestHelperClass.createSalesRepUser();
        	e = TestHelperClass.createEvent(a, u, false);
        	e.RecordTypeId = Utilities.getRecordType( RecordTypeName.ADMIN_TIME , 'Event').Id;
        	insert e;
        }
        
        PageReference ref = Page.EventRedirect;
        ref.getParameters().put('id', e.Id);
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(e);
        
        PageReference redref;
        
        test.startTest();
        	RedirectController rc = new RedirectController(sc);
        	redref = rc.redirectEvent();
        test.stopTest();
        
        system.assert(redref.getUrl().toLowerCase().contains('manageevents'));
    }
    
    static testMethod void testSFOnlyRedirect() {
        Event e;
        User u;
        Account a;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
        	u = TestHelperClass.createSalesRepUser();
        	e = TestHelperClass.createEvent(a, u, false);
        	e.RecordTypeId = Utilities.getRecordType( RecordTypeName.SALESFORCE_ONLY_EVENT , 'Event').Id;
        	insert e;
        }
        
        PageReference ref = Page.EventRedirect;
        ref.getParameters().put('id', e.Id);
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(e);
        
        PageReference redref;
        
        test.startTest();
        	RedirectController rc = new RedirectController(sc);
        	redref = rc.redirectEvent();
        test.stopTest();
        
        String prefix = Schema.Sobjecttype.Event.getKeyPrefix();
        String nooverride = redref.getParameters().get('nooverride');
        system.assertEquals('1', nooverride);
        system.assert( ('/' + e.Id).toLowerCase().startsWith(sc.view().getUrl().toLowerCase()) );
    }
    
    static testMethod void testEventEditRedirect() {
        Event e;
        User u;
        Account a;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
        	u = TestHelperClass.createSalesRepUser();
        	e = TestHelperClass.createEvent(a, u, false);
        	e.RecordTypeId = Utilities.getRecordType( RecordTypeName.ADMIN_TIME , 'Event').Id;
        }
        
        PageReference ref = Page.EventRedirect;
        ref.getParameters().put('id', e.Id);
        ref.getParameters().put('RecordTypeName', RecordTypeName.ADMIN_TIME);
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(e);
        
        PageReference redref;
        
        test.startTest();
        	RedirectController rc = new RedirectController(sc);
        	redref = rc.redirectEventEdit();
        test.stopTest();
        
        system.assertEquals('edit', redref.getParameters().get('mode'));
    }
    
    static testMethod void testEventDeleteError() {
    	
    	PageReference ref;
    	test.startTest();
    		RedirectController rc = new RedirectController(null);
    		ref = rc.eventDeleteError();
    	test.stopTest();
    	
    	system.assert(ref.getUrl().contains('ShowError?Error=SG_EVENT_DEL'));
    }
    
    static testMethod void testAutoSFEvent() {
        User u;
        Account a;
        Event e;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
        	u = TestHelperClass.createSalesRepUser();
        	e = TestHelperClass.createEvent(a, u, false);
        	e.RecordTypeId = null;
        }
        
        String prefix = Schema.Sobjecttype.Event.getKeyPrefix();
        PageReference ref;
        
        test.startTest();
        	ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(e);
        	RedirectController rc = new RedirectController(sc);
        	ref = rc.redirectEvent();
        test.stopTest();
        
        system.assert(ref.getURL().contains('/' + prefix + '/e'));
    }
    
    static testMethod void testredirectCase(){
    	User u;
        Account a;
        Case newCase;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        RecordType callidusRT = [SELECT Id, Name FROM RecordType WHERE Name = 'Callidus' limit 1];
        RecordType feedbackRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'NSC_FeedBack_Install' limit 1];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();        	
        	newCase = TestHelperClass.createCase(a.Id,feedbackRT.Id,current.Id);
        	insert newCase;
        }
        String prefix = Schema.Sobjecttype.Event.getKeyPrefix();
        PageReference ref;
        test.startTest();
        	ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(newCase);
        	RedirectController rc = new RedirectController(sc);
        	ref = rc.redirectCase();
        test.stopTest();
        //system.assert(ref.getURL().contains('/' + prefix + '/e'));
        
    }
    
    static testMethod void testredirectCase1(){
    	User u;
        Account a;
        Case newCase;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        RecordType callidusRT = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Case' and Name = 'Callidus' limit 1];
        RecordType feedbackRT = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Case' and DeveloperName = 'NSC_FeedBack_Install' limit 1];
        RecordType hermesRt = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Case' and DeveloperName = 'Quoting' limit 1];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();        	
        	newCase = TestHelperClass.createCase(a.Id,hermesRt.Id,current.Id);
        	insert newCase;
        }
        String prefix = Schema.Sobjecttype.Event.getKeyPrefix();
        PageReference ref;
        test.startTest();
        	ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(newCase);
        	RedirectController rc = new RedirectController(sc);
        	ref = rc.redirectCase();
        test.stopTest();
        //system.assert(ref.getURL().contains('/' + prefix + '/e'));
        
    }
    /*static testmethod void testredirectcase2(){
        Event e;
        User u;
        Account a;
        Lead l;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
        	u = TestHelperClass.createSalesRepUser();
        	e = TestHelperClass.createEvent(a, u, false);
            l=TestHelperClass.createLead(u);
        	e.RecordTypeId = Utilities.getRecordType( RecordTypeName.ADMIN_TIME , 'Event').Id;
        	insert e;
        }
        
        
    }*/
}