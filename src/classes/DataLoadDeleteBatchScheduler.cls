global class DataLoadDeleteBatchScheduler implements Schedulable {
	global void execute(SchedulableContext SC) {
        Database.executeBatch(new DataloadDeleteBatch(),200);
    }
}