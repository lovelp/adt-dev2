@istest
public class ADTGoGeoCodeTest {
    
    static testMethod void ADTGoGeoCodeRevResultsTest(){
ADTGoGeoCode ADTGoGeoCodeInstance=new ADTGoGeoCode ();
ADTGoGeoCode.ADTGoGeoCodeRevResults ADTGoGeoCodeRevResultsSampleInstance=new ADTGoGeoCode.ADTGoGeoCodeRevResults();
ADTGoGeoCode.ADTGoTimeZoneResults ADTGoTimeZoneResultsSampleInstance=new ADTGoGeoCode.ADTGoTimeZoneResults ();


  test.startTest();
 StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('ADTGoGeoCodeCalloutMock');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        ADTGoGeoCode.ADTGoGeoCodeRevResults ADTGoGeoCodeRevResultsInstance= ADTGoGeoCode.GeoCodeLatLon(1.0,1.0);
  test.stopTest();
}

    static testMethod void ADTGoTimeZoneResultsTest(){


  test.startTest();
 StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('ADTGoGeoCodeCalloutMock');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        ADTGoGeoCode.ADTGoTimeZoneResults ADTGoTimeZoneResultsInstance=ADTGoGeoCode.TimeZoneLatLon(1.0,1.0);
  test.stopTest();
}
}