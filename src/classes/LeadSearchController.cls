/**
 *Abhinav Pandey        03/11/2019    - USA country code fix for existing address.    
 **/
public with sharing class LeadSearchController {
    @AuraEnabled
    public static String createLead(Lead leadSearch,String street1, String street2, String city, String state, String postalCode,Boolean isOverRide){
        String sucessFlag = '';        
        try{
            if(leadSearch != null){
                MelissaDataAPI.MelissaDataResults melissaResponse = new MelissaDataAPI.MelissaDataResults();
                //check if address has lat, lon, postal code.
                String uniqueOriginalAddress =street1+'+';
                uniqueOriginalAddress += String.isnotBlank(street2) ? street2+'+'+city+'+'+state+'+'+postalCode :city+'+'+state+'+'+postalCode;
                List<Address__c> addrResponseList = [SELECT Id,OriginalAddress__c,Validated__c ,Street__c,Street2__c,StreetNumber__c,StreetName__c,CHS__c,
                                                     City__c,County__c,State__c,PostalCode__c,PostalCodeAddOn__c,CountryCode__c,
                                                     Latitude__c,Longitude__c FROM Address__c WHERE OriginalAddress__c =: uniqueOriginalAddress];
                //if no address exists
                if(addrResponseList.size() == 0){
                    //melissa call
                    melissaResponse = MelissaDataAPI.validateAddr(street1, street2 !=null ? street2: '', city, state,postalCode);
                    if(melissaResponse != null){
                        if(!isOverride){
                            for (String code : melissaResponse.ResultCodes){
                                if (code.containsIgnoreCase('AE')){
                                    // return back with error
                                    return 'Error:'+code;
                                }
                            }
                        }
                        uniqueOriginalAddress ='';
                        uniqueOriginalAddress = melissaResponse.Addr1+'+';
                        String address2 = melissaResponse.Addr2;
                        if (String.isNotBlank(melissaResponse.Suite)){
                            address2 = melissaResponse.Suite;
                        }
                        uniqueOriginalAddress +=String.isnotBlank(address2) ? address2 + '+' +melissaResponse.City+ '+' +melissaResponse.State+'+'+melissaResponse.Zip : melissaResponse.City+ '+'+ melissaResponse.State+ '+'+ melissaResponse.Zip;
                        //Query the address
                        addrResponseList = [SELECT Id,OriginalAddress__c,Street__c,Validated__c ,Street2__c,StreetNumber__c,StreetName__c,CHS__c,
                                            City__c,County__c,State__c,PostalCode__c,PostalCodeAddOn__c,CountryCode__c,
                                            Latitude__c,Longitude__c FROM Address__c WHERE OriginalAddress__c =: uniqueOriginalAddress];
                        if(addrResponseList.size() == 0){
                            //Create Lead and Address with melissa data
                            createLeadAdress(leadSearch,melissaResponse,street1,street2,city,state,postalCode,isOverRide);
                            sucessFlag = 'true';
                        }else{                           
                            //address exists after melissa validation so assign address addrResponseList[0] to the new lead
                            if(String.isBlank(addrResponseList[0].PostalCode__c)||String.isBlank(addrResponseList[0].PostalCodeAddOn__c) ||addrResponseList[0].Latitude__c==NULL||addrResponseList[0].Longitude__c==NULL|| String.isBlank(addrResponseList[0].City__c) || String.isBlank(addrResponseList[0].State__c)|| String.isBlank(addrResponseList[0].CountryCode__c)|| String.isBlank(addrResponseList[0].County__c) ||  (String.isNotBlank(addrResponseList[0].CountryCode__c) && addrResponseList[0].CountryCode__c.equalsIgnoreCase('USA'))){
                                    melissaResponse = checkForMelissaData(addrResponseList[0]);
                                    addrResponseList[0] = updateInvalidAddress(melissaResponse,addrResponseList[0]);
                            } 
                            createLead(leadSearch,addrResponseList[0]);
                            sucessFlag = 'true';
                        }
                    }
                }
                
                //address exists in the system
                else{
                    //assign address addrResponseList[0] to the new lead
                    if(String.isBlank(addrResponseList[0].PostalCode__c)||String.isBlank(addrResponseList[0].PostalCodeAddOn__c) ||addrResponseList[0].Latitude__c==NULL||addrResponseList[0].Longitude__c==NULL|| String.isBlank(addrResponseList[0].City__c) || String.isBlank(addrResponseList[0].State__c)|| String.isBlank(addrResponseList[0].CountryCode__c)|| String.isBlank(addrResponseList[0].County__c) ||  (String.isNotBlank(addrResponseList[0].CountryCode__c) && addrResponseList[0].CountryCode__c.equalsIgnoreCase('USA'))){
                      melissaResponse = checkForMelissaData(addrResponseList[0]);
                      addrResponseList[0] = updateInvalidAddress(melissaResponse,addrResponseList[0]);
                    } 
                    createLead(leadSearch,addrResponseList[0]);
                    sucessFlag = 'true';
                }
            }
        }catch(exception ex){ 
            sucessFlag = ex.getMessage();      
            System.debug('error in lead saving');
            ADTApplicationMonitor.log(ex, 'LeadSearchController', 'createLead');
        }        
        return sucessFlag;
    }
    
    public static pageReference createLead(Lead leadObj,Address__c addr){
        // Set Acc
        String bId;
        Boolean error = false;
        List<RecordType> leadRecordTypeList = [select Id,Name from RecordType where sObjectType='Lead'];
        leadObj.AddressID__c = addr.id;
        leadObj.SiteCity__c = addr.City__c;
        leadObj.SiteStateProvince__c =addr.State__c;
        leadObj.SitePostalCode__c = addr.PostalCode__c;
        leadObj.SitePostalCodeAddOn__c = addr.PostalCodeAddOn__c;
        leadObj.SiteStreet__c = addr.Street__c;
        leadObj.SiteCountryCode__c = addr.CountryCode__c;
        leadObj.SiteCounty__c = String.isnotBlank(addr.County__c) ? addr.County__c : '';
        leadObj.SiteStreet2__c = String.isnotBlank(addr.Street2__c) ? addr.Street2__c : '';
        leadObj.SiteStreetNumber__c = String.isNotBlank(addr.StreetNumber__c) ? addr.StreetNumber__c : '';
        leadObj.SiteStreetName__c   = String.isNotBlank(addr.StreetName__c) ? addr.StreetName__c : '';
        String zip = addr.PostalCode__c;
        //Custom home sales logic  if address exists and is a custom home sales address
        if(leadObj.Business_Id__c.contains('1100') && addr.CHS__c != null && addr.CHS__c == true){
            leadObj.Channel__c = Channels.TYPE_CUSTOMHOMESALES;
            leadObj.Business_Id__c = Channels.TYPE_RESIDENTIAL;
        }
        //HRM-10450 Biz Id changes start
        bId = Channels.getFormatedBusinessId(leadObj.Business_Id__c,Channels.BIZID_OUTPUT.NUM);
        for(RecordType rt:leadRecordTypeList){
            if(((leadObj.Business_Id__c.contains('1100') || leadObj.Business_Id__c.contains('1200')) && rt.Name.contains('User')) || (leadObj.Business_Id__c.contains('1300') && rt.Name.contains('Commercial'))){
                leadObj.RecordTypeId = rt.id;
                break;
            }
        }
        for(Postal_Codes__c pList : [Select id,Name from Postal_Codes__c where BusinessId__c =: bId AND Name =: zip limit 1]){
            leadObj.PostalCodeID__c = pList.id;
        }
        //HRM-10450 Biz Id changes End
        leadObj.LeadSource ='Commercial';
        insert leadObj;
        if(leadObj != null){
            PageReference retURL = new PageReference('/'+leadObj.id);
            aura.redirect(retURL);
        }
        return null;
    }
        
    public static void createLeadAdress(Lead l ,MelissaDataAPI.MelissaDataResults mdr,String street1, String street2, String city, String state, String postalCode,Boolean isOverRide){
        Boolean error = false;
        Boolean didOverride = isOverRide;
        Address__c addr = new Address__c();
        // Set Addr
        addr.Street__c = mdr != null ? mdr.Addr1 : street1;
        String address2 = mdr!=null? mdr.Addr2:street2 ;
        if (mdr!=null && String.isNotBlank(mdr.Suite)){
            address2 = mdr.Suite;
        }
        addr.Street2__c = String.isNotBlank(address2) ? address2 : String.isNotBlank(street2)? street2 :'';
        addr.StreetNumber__c = mdr != null ? mdr.StreetNumber: '';
        addr.StreetName__c = mdr != null ? mdr.StreetName : '';
        addr.City__c = mdr != null ? mdr.City :String.isNotBlank(city) ? city : '';
        addr.County__c = mdr != null ? mdr.County :'';
        addr.State__c = mdr != null ?  mdr.State :String.isNotBlank(state) ? state : '';
        addr.PostalCode__c = (mdr != null && String.isNotBlank(mdr.Zip))? mdr.Zip : String.isNotBlank(postalCode) ? postalCode : '';
        addr.PostalCodeAddOn__c = mdr != null ? mdr.Plus4 :'';
        addr.CountryCode__c = (mdr != null && String.isNotBlank(mdr.CountryCode)) ? mdr.CountryCode : '';
        addr.Latitude__c = mdr != null ? decimal.valueOf(mdr.Lat) : 0.00;
        addr.Longitude__c = mdr != null ? decimal.valueOf(mdr.Lon) : 0.00;
        addr.OriginalAddress__c = mdr != null ? mdr.Addr1+'+':street1+'+';
        if(mdr != null){
            addr.Validated__c = true;
            String errorFlag = '';
            String melissaAddress2 = mdr.Addr2;
            if (String.isNotBlank(mdr.Suite)){
                melissaAddress2 = mdr.Suite;
            }
            addr.OriginalAddress__c += String.isnotBlank(melissaAddress2) ? melissaAddress2 + '+' +mdr.City+ '+' +mdr.State+'+'+mdr.Zip : mdr.City+ '+'+ mdr.State+ '+'+ mdr.Zip;
            for (String code : mdr.ResultCodes){
                if (code.containsIgnoreCase('AE')){
                    errorFlag = 'Error:'+code;
                    break;
                }
            }     
        }else {
            addr.OriginalAddress__c += String.isnotBlank(street2) ? street2 + '+' +city+ '+' +state+'+'+postalCode : city+ '+'+ state+ '+'+ postalCode;
        }
        addr.StandardizedAddress__c = mdr != null ? mdr.Addr1+',' :street1 + ',';
        if(mdr != null){
            String meliAddress2 = mdr.Addr2;
            if (String.isNotBlank(mdr.Suite)){
                meliAddress2 = mdr.Suite;
            }
            addr.StandardizedAddress__c += String.isnotBlank(meliAddress2) ?meliAddress2 + ',' +mdr.City+ ',' +mdr.State+','+mdr.Zip : mdr.City+ ','+ mdr.State+ ','+ mdr.Zip;
        }else {
            addr.StandardizedAddress__c += String.isnotBlank(street2) ? street2 + ',' +city+ ',' +state+','+postalCode: city+ ','+ state+ ','+ postalCode;
        }
        //override codes
        if (error && didOverride) {
            addr.ValidationMsg__c = 'User Override';
            for (String code: mdr.ResultCodes) {
                addr.ValidationMsg__c += ':' + code.substring(0,4);
            }
        }
        addr.StandardizedAddress__c = addr.StandardizedAddress__c.toUpperCase();
        addr.OriginalAddress__c = addr.OriginalAddress__c.toUpperCase();         
        insert addr;
        createLead(l,addr);
    }
    @AuraEnabled
    public static String searchLead(Lead leadSearch,String street1, String street2, String city, String state, String country, String postalCode){
        System.debug('Inside searchlead');
        searchLeadWrapper lWrap = new searchLeadWrapper();
        try{
            if(leadSearch != null){
                String uniqueOriginalAddress =street1+'+';
                MelissaDataAPI.MelissaDataResults melissaResponse = new MelissaDataAPI.MelissaDataResults();
                melissaResponse = MelissaDataAPI.validateAddr(street1, street2 !=null ? street2: '', city, state,postalCode);
                //Melissa results on lead and user entered address on parameters
                if(melissaResponse != null){
                    leadSearch.SiteStreet__c = String.isNotBlank(melissaResponse.Addr1) ? melissaResponse.Addr1 : '';
                    leadSearch.SiteStreet2__c = String.isNotBlank(melissaResponse.Addr2) ? melissaResponse.Addr2 : ''; 
                    if (String.isNotBlank(melissaResponse.Suite)){
                        leadSearch.SiteStreet2__c = melissaResponse.Suite;
                    }                    
                    leadSearch.SiteCity__c = String.isNotBlank(melissaResponse.City) ? melissaResponse.City : '';
                    leadSearch.SiteStateProvince__c = String.isNotBlank(melissaResponse.State) ? melissaResponse.State : '';
                    leadSearch.SitePostalCode__c = String.isNotBlank(melissaResponse.Zip) ? melissaResponse.Zip : '';
                }
                lWrap.leadSearch = leadSearch;
                lWrap.street1 = street1;
                lWrap.street2 = street2;
                lWrap.city = city;
                lWrap.state = state;
                lWrap.country = country;
                lWrap.postalCode = postalCode;
            }
        }catch(exception ex){
            ADTApplicationMonitor.log(ex, 'LeadSearchController', 'searchLead');
        }
        System.debug('Inside searchlead wrao'+lWrap);
        return Json.serialize(lWrap);
    }
    public class searchLeadWrapper{
        public lead leadSearch;
        public string street1;
        public string street2;
        public string city;
        public string state;
        public string country;
        public string postalCode;
    }
    @AuraEnabled
    public static List<String> getPickListValuesList(String objectType, String selectedField){
        List<string> pickListValues = PickListController.getPickListValuesIntoList(objectType,selectedField);
        return pickListValues;
    }
    
    @AuraEnabled
    public static User getUser(){
        User u = [Select Id,Business_Unit__c, Business_Unit_Calculated__c  from User where Id = :Userinfo.getUserId()];
        return u;
    }
    
    @AuraEnabled
    public static Lead getExistingLead(String leadIdValue){
        try{
            System.debug('###'+leadIdValue);
            if(String.isNotBlank(leadIdValue)){
                return [SELECT id,FirstName,LastName,channel__c,Business_Id__c,Company,Phone,Email,SiteStreet__c,SiteCity__c,SiteCountryCode__c,SiteStateProvince__c,SitePostalCode__c,SiteStreet2__c FROM Lead Where Id=: leadIdValue];
            }else {
                return null;
            }
        }catch(exception ex){
            System.debug('exception in getting lead'+ex);
            return null;
        }    
    }
    public static Address__c createAdress(lead l,MelissaDataAPI.MelissaDataResults mdr,Boolean isOverRide){
        Boolean error = false;
        Boolean didOverride = isOverRide;
        Address__c addr = new Address__c();
        // Set Addr
        addr.Street__c = mdr != null ? mdr.Addr1 :l.SiteStreet__c;
        String address2 = mdr!=null? mdr.Addr2:l.SiteStreet2__c ;
        if (mdr!=null && String.isNotBlank(mdr.Suite)){
            address2 = mdr.Suite;
        }
        addr.Street2__c = String.isNotBlank(address2) ? address2 : String.isNotBlank(l.SiteStreet2__c)? l.SiteStreet2__c :'';
        addr.StreetNumber__c = mdr != null ? mdr.StreetNumber: '';
        addr.StreetName__c = mdr != null ? mdr.StreetName : '';
        addr.City__c = mdr != null ? mdr.City :String.isNotBlank(l.SiteCity__c) ? l.SiteCity__c : '';
        addr.County__c = mdr != null ? mdr.County :'';
        addr.State__c = mdr != null ?  mdr.State :String.isNotBlank(l.SiteStateProvince__c) ? l.SiteStateProvince__c : '';
        addr.PostalCode__c = (mdr != null && String.isNotBlank(mdr.Zip))? mdr.Zip : String.isNotBlank(l.SitePostalCode__c) ? l.SitePostalCode__c : '';
        addr.PostalCodeAddOn__c = mdr != null ? mdr.Plus4 :'';
        addr.CountryCode__c = (mdr != null && String.isNotBlank(mdr.CountryCode)) ? mdr.CountryCode : '';
        addr.Latitude__c = mdr != null ? decimal.valueOf(mdr.Lat) : 0.00;
        addr.Longitude__c = mdr != null ? decimal.valueOf(mdr.Lon) : 0.00;
        addr.OriginalAddress__c = mdr != null ? mdr.Addr1+'+':l.SiteStreet__c+'+';
        if(mdr != null){
            addr.Validated__c = true;
            String errorFlag = '';
            String melissaAddress2 = mdr.Addr2;
            if (String.isNotBlank(mdr.Suite)){
                melissaAddress2 = mdr.Suite;
            }
            addr.OriginalAddress__c += String.isnotBlank(melissaAddress2) ? melissaAddress2 + '+' +mdr.City+ '+' +mdr.State+'+'+mdr.Zip : mdr.City+ '+'+ mdr.State+ '+'+ mdr.Zip;
            for (String code : mdr.ResultCodes){
                if (code.containsIgnoreCase('AE')){
                    errorFlag = 'Error:'+code;
                    break;
                }
            }
        }else {
            addr.OriginalAddress__c += String.isnotBlank(l.SiteStreet2__c) ? l.SiteStreet2__c + '+' +l.SiteCity__c+ '+' +l.SiteStateProvince__c+'+'+l.SitePostalCode__c : l.SiteCity__c+ '+'+ l.SiteStateProvince__c+ '+'+ l.SitePostalCode__c;
        }
        addr.StandardizedAddress__c = mdr != null ? mdr.Addr1+',' :l.SiteStreet__c + ',';
        if(mdr != null){
            String meliAddress2 = mdr.Addr2;
            if (String.isNotBlank(mdr.Suite)){
                meliAddress2 = mdr.Suite;
            }
            addr.StandardizedAddress__c += String.isnotBlank(meliAddress2) ?meliAddress2 + ',' +mdr.City+ ',' +mdr.State+','+mdr.Zip : mdr.City+ ','+ mdr.State+ ','+ mdr.Zip;
        }else {
            addr.StandardizedAddress__c += String.isnotBlank(l.SiteStreet2__c) ? l.SiteStreet2__c + '+' +l.SiteCity__c+ '+' +l.SiteStateProvince__c+'+'+l.SitePostalCode__c : l.SiteCity__c+ '+'+ l.SiteStateProvince__c+ '+'+ l.SitePostalCode__c;
        }
        //override codes
        if (error && didOverride) {
            addr.ValidationMsg__c = 'User Override';
            for (String code: mdr.ResultCodes) {
                addr.ValidationMsg__c += ':' + code.substring(0,4);
            }
        }
        addr.StandardizedAddress__c = addr.StandardizedAddress__c.toUpperCase();
        addr.OriginalAddress__c = addr.OriginalAddress__c.toUpperCase();         
        insert addr;
        return addr;
    }
    public static void updateLead(Lead l,Address__c addr){
        l.AddressID__c = addr.id;
        l.SiteStreet__c = addr.Street__c;
        l.SiteStreet2__c = String.isNotBlank(addr.Street2__c) ? addr.Street2__c : '';
        l.SiteCity__c = addr.City__c;
        l.SiteStateProvince__c = addr.State__c;
        l.SiteCountryCode__c = addr.CountryCode__c;
        l.SitePostalCode__c = addr.PostalCode__c;
        l.SiteValidated__c = addr.Validated__c;
        update l;        
    }
    @AuraEnabled
    public static String doLeadUpdate(Lead leadToUpdate,Boolean isOverRide){
        System.debug('### lead to update is'+leadToUpdate);
        String successFlag = '';
        Address__c addrObj = new Address__c();
        MelissaDataAPI.MelissaDataResults melissaResponse = new MelissaDataAPI.MelissaDataResults();
        try{
           if(leadToUpdate != null){
                String uniqueOriginalAddress =leadToUpdate.SiteStreet__c+'+';
                uniqueOriginalAddress += String.isnotBlank(leadToUpdate.SiteStreet2__c) ? leadToUpdate.SiteStreet2__c+'+'+leadToUpdate.SiteCity__c+'+'+leadToUpdate.SiteStateProvince__c+'+'+leadToUpdate.SitePostalCode__c :leadToUpdate.SiteCity__c+'+'+leadToUpdate.SiteStateProvince__c+'+'+leadToUpdate.SitePostalCode__c;
                List<Address__c> addrResponseList = [SELECT Id,OriginalAddress__c,Validated__c ,Street__c,Street2__c,StreetNumber__c,StreetName__c,CHS__c,
                                                     City__c,County__c,State__c,PostalCode__c,PostalCodeAddOn__c,CountryCode__c,
                                                     Latitude__c,Longitude__c FROM Address__c WHERE OriginalAddress__c =: uniqueOriginalAddress];
               if(addrResponseList.size() == 0){
                   System.debug('### address does not exists');
                   //melissa call                    
                  melissaResponse = MelissaDataAPI.validateAddr(leadToUpdate.SiteStreet__c, String.isNotBlank(leadToUpdate.SiteStreet2__c)? leadToUpdate.SiteStreet2__c: '', leadToUpdate.SiteCity__c, leadToUpdate.SiteStateProvince__c,leadToUpdate.SitePostalCode__c);
                    if(melissaResponse != null){
                        if(!isOverride){
                            for (String code : melissaResponse.ResultCodes){
                                if (code.containsIgnoreCase('AE')){
                                    // return back with error
                                    return 'Error:'+code;
                                }
                            }
                        }
                        uniqueOriginalAddress ='';
                        uniqueOriginalAddress = melissaResponse.Addr1+'+';
                        String address2 = melissaResponse.Addr2;
                        if (String.isNotBlank(melissaResponse.Suite)){
                            address2 = melissaResponse.Suite;
                        }
                        uniqueOriginalAddress +=String.isnotBlank(address2) ? address2 + '+' +melissaResponse.City+ '+' +melissaResponse.State+'+'+melissaResponse.Zip : melissaResponse.City+ '+'+ melissaResponse.State+ '+'+ melissaResponse.Zip;
                        //Query the address
                        addrResponseList = [SELECT Id,OriginalAddress__c,Validated__c ,Street__c,Street2__c,StreetNumber__c,StreetName__c,CHS__c,
                                            City__c,County__c,State__c,PostalCode__c,PostalCodeAddOn__c,CountryCode__c,
                                            Latitude__c,Longitude__c FROM Address__c WHERE OriginalAddress__c =: uniqueOriginalAddress];
                        if(addrResponseList.size() == 0){
                            //Create Lead and Address with melissa data
                            addrObj = createAdress(leadToUpdate,melissaResponse,isOverRide);
                            System.debug('### address does not exists after melissa'+addrObj);
                            //update lead with this address details
                            updateLead(leadToUpdate,addrObj);
                            successFlag = 'true';
                        }else{      
                            //address exists after melissa validation so assign address addrResponseList[0] to the lead
                            if(String.isBlank(addrResponseList[0].PostalCode__c)||String.isBlank(addrResponseList[0].PostalCodeAddOn__c) ||addrResponseList[0].Latitude__c==NULL||addrResponseList[0].Longitude__c==NULL|| String.isBlank(addrResponseList[0].City__c) || String.isBlank(addrResponseList[0].State__c)|| String.isBlank(addrResponseList[0].CountryCode__c)|| String.isBlank(addrResponseList[0].County__c)){
                                    melissaResponse = checkForMelissaData(addrResponseList[0]);
                                    addrResponseList[0] = updateInvalidAddress(melissaResponse,addrResponseList[0]);
                                System.debug('### address  exists and no melissa needed'+addrResponseList[0]);
                            } 
                            
                            updateLead(leadToUpdate,addrResponseList[0]);   
                            successFlag = 'true';        
                        }
                    }
                }
                else{
                   //address exists in the system
                   if(String.isBlank(addrResponseList[0].PostalCode__c)||String.isBlank(addrResponseList[0].PostalCodeAddOn__c) ||addrResponseList[0].Latitude__c==NULL||addrResponseList[0].Longitude__c==NULL|| String.isBlank(addrResponseList[0].City__c) || String.isBlank(addrResponseList[0].State__c)|| String.isBlank(addrResponseList[0].CountryCode__c)|| String.isBlank(addrResponseList[0].County__c)){
                                    melissaResponse = checkForMelissaData(addrResponseList[0]);
                                    addrResponseList[0] = updateInvalidAddress(melissaResponse,addrResponseList[0]);
                   } 
                   updateLead(leadToUpdate,addrResponseList[0]);  
                   successFlag = 'true';
                }
                if(String.isNotBlank(successFlag) && successFlag.containsIgnoreCase('true')){
                   PageReference retURL = new PageReference('/'+leadToUpdate.id);
                   aura.redirect(retURL);
                }                
            }
            return successFlag;   
        }catch(exception ex){
            System.debug('### ex'+ex);
            ADTApplicationMonitor.log(ex, 'LeadSearchController', 'updateLead'+leadToUpdate.Id);
            return ex.getMessage();
        }        
    }
    
    // melissa data check
    public static MelissaDataAPI.MelissaDataResults checkForMelissaData(Address__c addr){
        MelissaDataAPI.MelissaDataResults melissaResponse = new MelissaDataAPI.MelissaDataResults();
        //check if reqAddr is null
        if(addr != null){
            melissaResponse = MelissaDataAPI.validateAddr(addr.Street__c, String.isNotBlank(addr.Street2__c)? addr.Street2__c : '', addr.City__c, addr.State__c,addr.PostalCode__c);
        }
        return melissaResponse;
    }
    
    public static Address__c updateInvalidAddress(MelissaDataAPI.MelissaDataResults mdr, Address__c addr){
        Boolean error = false;
        Boolean didOverride = false;
        if(mdr != null){
            addr.PostalCode__c = mdr.Zip;
            addr.City__c = mdr.City;
            addr.State__c = mdr.State;
            addr.CountryCode__c =mdr.CountryCode;
            addr.County__c =mdr.County;
            addr.Latitude__c=Decimal.valueof(mdr.Lat);
            addr.Longitude__c=Decimal.Valueof(mdr.Lon);
            addr.PostalCodeAddOn__c=mdr.Plus4;
            addr.Validated__c = true;
            for (String code : mdr.ResultCodes){
                if (code.startsWith('AE')){
                    error = true;
                    didOverride = true;
                }
            }
            if (error && didOverride) {
                addr.ValidationMsg__c = 'User Override';
                for (String code: mdr.ResultCodes) {
                    addr.ValidationMsg__c += ':' + code.substring(0,4);
                }
            }
            update addr;
        }
        return addr;
    }  
}