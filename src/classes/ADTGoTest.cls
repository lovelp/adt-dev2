/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=false)

private class ADTGoTest {

    static testMethod void goBadID() {
        createTestData();
        ApexPages.PageReference refbad = new PageReference('/apex/ADTGoMap');
    		test.setCurrentPageReference(refbad);
    		ADTGoMapController adtgo = new ADTGoMapController();
    		            	
    		Test.startTest();
    		adtgo.init();
    		Test.stopTest();
        
    }

    static testMethod void goDemo() {
        createTestData();
        ADTGoDemoLocation__c location = new ADTGoDemoLocation__c(Active__c=true, Sequence__c=1, Latitude__c=100.0, Longitude__c=100.0, Message__c='Test Message');
        insert location;
        
        ApexPages.PageReference refdemo = new PageReference('/apex/ADTGoMap?id=9999999997&debug');
    		test.setCurrentPageReference(refdemo);
    		ADTGoMapController adtgo = new ADTGoMapController();
    		
    		Test.startTest();
    		adtgo.init();
    		adtgo.updatePage();
    		Test.stopTest();
        
    }

    static testMethod void goInvalidID() {
        
        ApexPages.PageReference refdemo = new PageReference('/apex/ADTGoMap?id=f189cc00-1e31-4862--1329d1cebf05&debug');
    		test.setCurrentPageReference(refdemo);
    		ADTGoMapController adtgo = new ADTGoMapController();
    		

    		Test.startTest();
    		adtgo.init();
    		Test.stopTest();
        
    }

    static testMethod void goID() {
    	createTestData();
        ApexPages.PageReference refdemo = new PageReference('/apex/ADTGoMap?id=f189cc00-1e31-4862-9457-1329d1cebf05');
    		test.setCurrentPageReference(refdemo);
    		ADTGoMapController adtgo = new ADTGoMapController();
    		
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        multimock.setStaticResource(
            'https://admin.adtcanopyportal.com/api/person/attributes/f189cc00-1e31-4862-9457-1329d1cebf05', 'ADTGoResponse1');
        multimock.setStaticResource(
            'https://maps.googleapis.com/maps/api/geocode/json?client=gme-adt&sensor=true&channel=ADTGo&latlng=26.3950181,-80.1180629&signature=4prlfPxl_4qElwv5g5QG7BuqsMI=', 'ADTGoResponse2');
        multimock.setStaticResource(
			'https://admin.adtcanopyportal.com/api/sos/disposition/f189cc00-1e31-4862-9457-1329d1cebf05', 'ADTGoResponse3');
        multimock.setStaticResource(
			'https://dev.api.adt.com/ahj/getahhjlookup', 'ADTGoResponse4');
        multimock.setStatusCode(200);
        multimock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, multimock);    	

    		Test.startTest();
    		adtgo.init();
    		adtgo.mLat = 0;
    		adtgo.mLon = 0;
    		adtgo.updatePage();
    		adtgo.mapDispo = '';
    		adtgo.closeCall();
    		adtgo.mapDispo = 'MEDICAL';
    		adtgo.closeCall();
    		adtgo.mapDispo = 'ALL_CLEAR';
    		adtgo.closeCall();
    		Test.stopTest();
        
    }
    
    private static void createTestData(){
    	list<ADTGo_Settings__c> ls = new list<ADTGo_Settings__c>();
    	ls.add(new ADTGo_Settings__c(Name='EventLogging', Setting__c = 'true'));
    	ls.add(new ADTGo_Settings__c(Name='Demographics', Setting__c = 'true'));
    	ls.add(new ADTGo_Settings__c(Name='CanopyUser', Setting__c = 'true'));
    	ls.add(new ADTGo_Settings__c(Name='CanopyURL', Setting__c = 'true'));
    	ls.add(new ADTGo_Settings__c(Name='CanopySystem', Setting__c = 'ADT'));
    	ls.add(new ADTGo_Settings__c(Name='CanopyPassword', Setting__c = 'true'));
    	ls.add(new ADTGo_Settings__c(Name='AHJUser', Setting__c = 'true'));
    	ls.add(new ADTGo_Settings__c(Name='AHJUrl', Setting__c = 'https://api.adt.com'));
    	ls.add(new ADTGo_Settings__c(Name='AHJPassword', Setting__c = 'true'));
    	ls.add(new ADTGo_Settings__c(Name='ADTUser', Setting__c = 'true'));
    	ls.add(new ADTGo_Settings__c(Name='ADTUrl', Setting__c = 'https://api.adt.com'));
    	ls.add(new ADTGo_Settings__c(Name='ADTPassword', Setting__c = 'true'));
    	
    	insert ls;
    	
    	list<ADTGo_Dispos__c> ds = new list<ADTGo_Dispos__c>();
    	ds.add(new ADTGo_Dispos__c(Name='ALL_CLEAR', DispoDisplay__c = 'All clear, no assistance needed', StopUpdates__c = true));
    	ds.add(new ADTGo_Dispos__c(Name='CUSTOMER_NO_ANSWER_POLICE', DispoDisplay__c = 'Unable to contact customer – police advised', StopUpdates__c = false));
    	ds.add(new ADTGo_Dispos__c(Name='MEDICAL', DispoDisplay__c = 'Medical advised - customer requested assistance', StopUpdates__c = false));
    	ds.add(new ADTGo_Dispos__c(Name='POLICE', DispoDisplay__c = 'Police advised - customer requested assistance', StopUpdates__c = false));
    	insert ds;
    	
    }

}