@isTest class OppCPQIntegrationDataGeneratorTest {
    static final String TEST_NAME = 'TestName';
    static final String STAGE_NAME = 'Proposal/Price Quote';
    static OpportunityCPQIntegrationDataGenerator.IntegrationDataSource dataSource;

    static void setup() {
        Account account = new Account(
            Name = TEST_NAME
        );
        account.FirstName__c='testFirstName';
    	account.LastName__c='testLastName';
    	account.Email__c = 'abc@gmail.com';
    	account.DOB_encrypted__c='11/11/1973';
        account.Income__c = '1000';
        account.Phone = '14785894';
        account.Business_Id__c = '1100 - Residential';
        insert account;
        
        Opportunity opportunity = new Opportunity(
            Name = TEST_NAME,
            AccountId = account.Id,
            RecordTypeId = Utilities.optyRecordTypeId(account),
            StageName = STAGE_NAME,
            Probability = 90,
            CloseDate = System.today()
        );
        insert opportunity;
        
        list<FlexFiMessaging__c> msgs = new list<FlexFiMessaging__c>();
        FlexFiMessaging__c msg = new FlexFiMessaging__c(Name = 'NoCreditCheck', ErrorMessage__c = 'No Credit Check Found');
        msgs.add(msg);
        FlexFiMessaging__c msg1 = new FlexFiMessaging__c(Name = 'ApmError', ErrorMessage__c = 'ApmError');
        msgs.add(msg1);
        FlexFiMessaging__c msg2 = new FlexFiMessaging__c(Name = 'AddressNotFoundError', ErrorMessage__c = 'AddressNotFoundError');
        msgs.add(msg2);
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'ZipCodeNotFoundError', ErrorMessage__c = 'ZipCodeNotFoundError');
        msgs.add(msg3);
        FlexFiMessaging__c msg4 = new FlexFiMessaging__c(Name = 'StreetNotFoundError', ErrorMessage__c = 'StreetNotFoundError');
        msgs.add(msg4);
        
        insert msgs;
        
        LoanApplication__c LoanApplication = new LoanApplication__c();
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c =DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.PreviousAddressLine1__c ='Prev Add Line 1';
        LoanApplication.PreviousAddressLine2__c ='Prev Add Line 2';
        LoanApplication.PreviousAddressPostalCd__c ='27518';
        LoanApplication.PreviousAddressStateCd__c ='NC';
        LoanApplication.PreviousAddressCountryCd__c='US';
       // LoanApplication.eConsentAccepted__c=true;
        LoanApplication.PreviousAddressLine1__c = '8952 Brook Rd';
        LoanApplication.PreviousAddressCity__c = 'McLean';
        LoanApplication.PreviousAddressStateCd__c = 'VA';
        LoanApplication.PreviousAddressPostalCd__c = '221o2';
        LoanApplication.PreviousAddressCountryCd__c = 'US';
        LoanApplication.eConsentAcceptedDateTime__c = system.now();
    	LoanApplication.Account__c = account.id;
    	LoanApplication.PaymentechProfileId__c = '123';
        LoanApplication.FirstName__c='testFirstName';
        LoanApplication.LastName__c='testLastName';
        LoanApplication.DOB__c='11/11/1973';
        LoanApplication.SSN__c='123456789';
        LoanApplication.AnnualIncome__c=1000;
        LoanApplication.CreditCardName__c = 'Test CC';
    	LoanApplication.CreditCardNumber__c = '1234567812345678';
    	LoanApplication.CreditCardExpirationDate__c = '11/11/2022';
        insert LoanApplication;
        
        String qOrderType = null;
        
       	dataSource = 
            new OpportunityCPQIntegrationDataGenerator.IntegrationDataSource(
        		opportunity.Id,
                qOrderType
            );
    }
    
    @isTest static void testGetCPQIntegrationJSON() {
        setup();
        
      	Test.startTest();
        	OpportunityCPQIntegrationDataGenerator.getCPQIntegrationJSON(dataSource);
        Test.stopTest();
        
        //System.assertEquals();
    }
}