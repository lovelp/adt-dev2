/************************************* MODIFICATION LOG ********************************************************************************************
* *
* DESCRIPTION : This Apex Class is been used as a controller for Visual Force Page Called ChatterFileSecurityControl
*				Handle logic related to customized security on files posted on chatter
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera				10/17/2013			- Original Version
*													
*/



global with sharing class ChatterFileSecurityControlCtrl {
        
        public String 		selectedKeyword 	{ get; set; }
		public String 		selectedFileType	{ get; set; }
		public Set<String> 	keywordSet			{ get; set; }
		public Set<String> 	fTypeSet			{ get; set; }
				
		// User wrapper
		global class userInfo {
			public User uObj;
			public String Name;
			public String email;
			public String Id;
			public Boolean isExcluded;
			public userInfo(User uObj, Boolean isEx){
				uObj 	= uObj;
				Name 	= uObj.Name;
				email	= uObj.email;
				Id		= uObj.Id;
				isExcluded = isEx; 
			}
		}
		
		// License wrapper
		global class userLicenseWrapper {
			public String Id;
			public String Name;
			public Boolean isSet;
			public userLicenseWrapper(String pId, String pName, Boolean pisSet){
				Id = pId;
				Name = pName;
				isSet = pisSet; 
			}
		}
		
        public List<SelectOption> fileTypeGroup {
        	get{
        		List<SelectOption> resultList = new List<SelectOption>();
        		fTypeSet = new Set<String>();
        		String fTypeStr = '';
        		try{
        			fTypeStr = ChatterSecurity__c.getInstance().file_type__c;
        		}
        		catch(Exception err){}        		
        		resultList.add(new SelectOption('','')); 
        		if(!Utilities.isEmptyOrNull(fTypeStr)){
        			for(String s: fTypeStr.split(',')){
        				resultList.add(new SelectOption(s, '*.'+s));
        				fTypeSet.add(s); 
        			}
        		}
        		return resultList; 
        	}
        }

        /**
         *	@Constructor
         */
        public ChatterFileSecurityControlCtrl() { }
        
        /**
         *	Saves selected users to be included in exclusion list. This action is called from the UI using a saving button linked to an action function.
         *	@method saveSuperUserSelection
         *	@return PageReference
         */
        public PageReference saveSuperUserSelection(){
        	String uIdSelection = ApexPages.currentPage().getParameters().get('uIdSelection');
        	ChatterUtilities.setChatterFileSuperUser(uIdSelection);
        	return null;
        }
        
        /**
         *	Add a file type to settings
         *	@method addFileType
         *	@return PageReference
         */
        public PageReference addFileType(){
        	String fTypeStr = ApexPages.currentPage().getParameters().get('fTypeParam');
        	Boolean settingsCreated = false;
        	if(!Utilities.isEmptyOrNull(fTypeStr))
        	{
        		// Remove all special characters entered by the user
        		fTypeStr = fTypeStr.replaceAll('[^a-zA-Z0-9]+','');
        		
        		ChatterSecurity__c sObj;
        		try{
        			sObj = [SELECT file_type__c FROM ChatterSecurity__c];
        		}
        		catch(exception err){}
        		if (sObj==null){
        			sObj = new ChatterSecurity__c(file_type__c = fTypeStr);
        			insert sObj;
        			settingsCreated = true; 
        		}
        		else if(!Utilities.isEmptyOrNull(sObj.file_type__c))
	        		sObj.file_type__c += ','+fTypeStr;
	        	else	
	        		sObj.file_type__c = fTypeStr;
	        	
	        	if(!settingsCreated)	
	        		update sObj;
        			 	        	
        	} 
        	return null;
        }
        
        /**
         *	Removes a file type from settings action received from client
         *	@method removeFileType
         *	@return PageReference
         */
        public PageReference removeFileType(){
        	String fTypeStr = ApexPages.currentPage().getParameters().get('fTypeParam');
        	if(!Utilities.isEmptyOrNull(fTypeStr))
        	{
	        	String fTypeListStr = '';
	        	fTypeSet.remove(fTypeStr);
	        	for(String s: fTypeSet){
	        		fTypeListStr += s + ',';
	        	}
	        	if(!Utilities.isEmptyOrNull(fTypeListStr)){
	        		fTypeListStr = fTypeListStr.substring(0, fTypeListStr.length()-1);
	        	}
	        	ChatterSecurity__c sObj = [SELECT file_type__c FROM ChatterSecurity__c];
	        	sObj.file_type__c =  fTypeListStr;
	        	update sObj; 
        	}
        	return null;
        } 
        
        /**
         *	Recovers a list of users to show on the selector of powerusers adding a flag for those already included
         *	@method getUserList
         *	@return List<userInfo> List of users
         */  
	    @RemoteAction
	    global static List<userInfo> getUserList() {
			Map<String, userInfo> userMap = new Map<String, userInfo>();
			 
			// List all active users					
			for(User u: [SELECT Id, Name, email 
						 FROM User 
						 WHERE IsActive = true
						 Order By email asc]){
				userInfo uObj = new userInfo(u, false); 						   	  	
				userMap.put(u.Id, uObj);
			}
			
			// Mark already included on group users 
			for(GroupMember g: [SELECT UserOrGroupId
								FROM GroupMember 
								WHERE Group.DeveloperName = :ChatterSecurity__c.getInstance().Chatter_Super_User_Group__c
									  AND UserOrGroupId IN :userMap.keySet()]){
				userInfo uObj = userMap.get(g.UserOrGroupId);
				uObj.isExcluded = true; 
				userMap.put(g.UserOrGroupId, uObj);
			}
			
			return userMap.values();															    
	    }
	    
	    /**
	     *	Recover licences from salesforce for this application to use
	     *	@method getUserLicenses
	     *	@return List<userLicenseWrapper> Lists of salesforce licences containing a flag showing which is enabled for this app
	     */
	    @RemoteAction
	    global static List<userLicenseWrapper> getUserLicenses(){
	    	List<userLicenseWrapper> resList = new List<userLicenseWrapper>();

			// Read existing configuration
	    	Set<String> licenseEnabledSet = ChatterUtilities.ChatterEnhancedLicenses(); 

			// Build license list			
	    	for(UserLicense usrLObj: [SELECT Id, Name, LicenseDefinitionKey FROM UserLicense Order By Name asc]){
	    		resList.add(new userLicenseWrapper(usrLObj.Id, usrLObj.Name, licenseEnabledSet.contains(usrLObj.Name)));
	    	}
	    	
	    	return resList;
	    }
	    
	    /**
	     *	Takes current license configuration and saves changes submitted by user
	     *	@method setUserLicense
	     *	@return Boolean Value not currently used
	     */
	    @RemoteAction
	    global static Boolean setUserLicense(Boolean isSet, String LicenseName){
	    	Set<String> licenseSet = new Set<String>();
	    	try{
		    	if(!Utilities.isEmptyOrNull(LicenseName))
	        	{
			    	ChatterSecurity__c sObj = [SELECT License_Enabled__c FROM ChatterSecurity__c];
			    	
			    	// Evalute if license is being removed or added
			    	if(!Utilities.isEmptyOrNull(sObj.License_Enabled__c)){
				    	for(String lStr: sObj.License_Enabled__c.split(',')){
				    		if(lStr == LicenseName && isSet==true)
				    			licenseSet.add(lStr);
				    		else if (lStr != LicenseName)
				    			licenseSet.add(lStr);	
				    	}
			    	}
			    	if(!licenseSet.contains(LicenseName) && isSet==true){
			    		licenseSet.add(LicenseName);
			    	}
			    	
			    	// Save result process back to settings
			    	sObj.License_Enabled__c = ''; // reset settings			    	
		        	for(String s: licenseSet){
		        		sObj.License_Enabled__c += s + ',';
		        	}
	    			if(!licenseSet.isEmpty())
		        		sObj.License_Enabled__c = sObj.License_Enabled__c.substring(0, sObj.License_Enabled__c.length()-1); //remove additional comma
		        	update sObj; 
			    	
	        	}
	    	}
	    	catch(Exception err){
	    		system.debug('\n @@ ERROR '+err.getMessage()+' \n '+err.getStackTraceString()+'\n');
	    	}
			return true;	    	
	    }

         
}