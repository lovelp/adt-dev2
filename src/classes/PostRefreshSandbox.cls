global class PostRefreshSandbox implements SandboxPostCopy{
	global void runApexClass(SandboxContext context) {
        System.debug('Org ID: ' + context.organizationId());
        System.debug('Sandbox ID: ' + context.sandboxId());
        System.debug('Sandbox Name: ' + context.sandboxName());
    	Database.executeBatch(new BatchCommercialMsgInsert(), 1);
        // Insert logic here to prepare the sandbox for use.
    }
}