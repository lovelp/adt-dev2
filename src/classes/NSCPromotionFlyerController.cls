public without sharing class NSCPromotionFlyerController {
    public String FlyerId{get;set;}
    public String PromoId{get;set;}
    public String promoDesc{get;set;}
    public String message{get;set;}

    public NSCPromotionFlyerController(){
        
        
        message = System.currentPagereference().getParameters().get('flyerid');
        FlyerId=URL.getSalesforceBaseUrl().toExternalForm()+'/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId='+System.currentPagereference().getParameters().get('flyerid');
        system.debug('Url is ...'+FlyerId);
        PromoId = System.currentPagereference().getParameters().get('promotionId');
        
        Promotion__c Promotion=[Select Description__c From Promotion__c Where id=:PromoId];
        promoDesc=Promotion.Description__c;
    }
}