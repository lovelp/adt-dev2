/************************************* MODIFICATION LOG ********************************************************************************************
* LocationDataMessageProcessor
*
* DESCRIPTION : An abstract class defining an interface that each type of location tracking message must support.  
*               A processor must be able to validate its input, construct the required LocationDataMessageParameters object, 
*               and interpret results for presentation to the user.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					10/14/2011				- Original Version
*
*													
*/
public abstract class LocationDataMessageProcessor {
	
	public static final String NONE = 'NONE';
	protected List<LocationDataMessageProcessor.LocationData> locationDataList;
	protected String mapData;
	protected Date dateParameter;
	protected Map<Id, String> userNameMap;
	
	public abstract String getName();
	
	public virtual Boolean isValidInput(String userId, String userIdForTeam, String searchDate) {
		
		// assume valid input unless...
		Boolean validInput = true;
		if (userId == NONE ||userId==null) {
			// determine otherwise
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'A User must be selected.'));
			validInput = false;
		}
		
		if (validInput) {
			if (searchDate == null || searchDate.length() == 0){
	 			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'A date must be entered.'));
				validInput = false; 		
	 		}
	 		else {
	 			// accommodates a format of mm/dd/yyyy, m/d/yyyy, mm/dd/yy, or m/d/yy
    			List<String> parts = searchDate.split('/', 3);
    			String year = parts[2];
    			if (parts[2].length() == 2) {
    				year = '20' + year;
    			}
    			dateParameter = Date.newInstance(Integer.valueOf(year),
    											Integer.valueOf(parts[0]),
    											Integer.valueOf(parts[1]));								
	 			
	 			if (dateParameter.addDays(250) < System.today() || dateParameter > System.today()) {
	 				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'A valid date within the past 250 days must be entered.'));
	 				validInput=false;
	 			}
	 		}		
		}		
		return validInput;
	}
	
	public virtual LocationDataMessageParameters buildParameters(String userId, String userIdForTeam, String searchDate) {
		
		// Use userId and userIdForTeam to build a List of user ids and set the team indicator
		List<Id> userIdList = new List<Id>();
		Boolean isForATeam = false;
		if (userId != null && userId.length() > 0 && userId != LocationDataMessageProcessor.NONE) {
			userIdList.add(userId);
		}	
		else {
			if (userIdForTeam != null && userIdForTeam.length() > 0 && userIdForTeam != LocationDataMessageProcessor.NONE) {
				Set<Id> managerSet = new Set<Id>();
				managerSet.add(userIdForTeam);
				Map<Id, List<Id>> managerToTeamMembersMap = Utilities.getManagersAndSubordinates(managerSet);
				userIdList = managerToTeamMembersMap.get(userIdForTeam);
				isForATeam = true;
			}
		}	
		
		// Use the Date value created from the String searchDate
		// Default to historical data
		return new LocationDataMessageParameters(dateParameter, userIdList, userIdForTeam, true);
		
	}
	
	public virtual Boolean interpretResults(LocationDataMessageParameters parameters, List<LocationItem> itemsList) {
		
		Boolean readyForDisplayOnMap = true;			
		if (itemsList.isEmpty()){
			//No data points available for the user
			inferUserNames(parameters.userIdList);
		   	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No location history available for ' + getUserName(parameters.userIdList[0]) + ' on ' + parameters.searchDate.format() + '.'));
			readyForDisplayOnMap = false;
		}
		return readyForDisplayOnMap;
	}
	
	
	public virtual List<LocationDataMessageProcessor.LocationData> getLocationDataList(List<LocationItem> itemsList) {
		
		if (locationDataList == null) {
			prepareForMapping(itemsList);
		}
		
		return locationDataList; 
		
	}
	
	public virtual String getMapData(List<LocationItem> itemsList) {
		
		if (mapData == null) {
			prepareForMapping(itemsList);
		}
		
		return mapData; 
	}
	
	protected void inferUserNames(List<Id> userIdList) {
		
		userNameMap = new Map<Id, String>();
		User[] userArray = [select id, name from User where id in:userIdList];
		for (User u: userArray) {
			userNameMap.put(u.id, u.name);
		}
	}
	
	protected String getUserName(Id userId) {
		
		return userNameMap.get(userId);
	}
	
	public virtual void prepareForMapping(List<LocationItem> itemsList) {
		mapData='';
    	decimal lat ;
    	decimal longX;
    	locationDataList = new List <LocationDataMessageProcessor.LocationData>();
	
		integer m=1;
	  	integer j=itemsList.size();
		integer count=1;

		for (integer n=0; n<j;n++){
			LocationItem  l=itemsList[n];
			lat = decimal.valueOf(l.pLatitude);
    		longX= decimal.valueOf(l.pLongitude);
                                    
        	LocationDataMessageProcessor.LocationData  tab =new LocationDataMessageProcessor.LocationData();
	    	tab.knockList = new List <LocationDataMessageProcessor.LocationItemPair>();
	    	boolean isValid=true;
	 	
	    	if (isValid) {
	    		//bMapValid=true;
	    		tab.count=count;
	    		tab.BreadCrumb=l;
     			locationDataList.add(tab);
     			count++;
     			system.debug(count);
      	 		mapData += 'l$' + lat + '$' +
                                        longX +'$'+l.rUser.Name+'$'+l.sRecordId+'$'+l.sRecordText +'$'+l.sMapLinkText+'$'+l.pDateTime.format() +'$'+l.primaryColor+'$'+count+'@';
	    	}
			System.debug(locationDataList);
		}	
    }
    
    public class LocationData {
	
		public LocationItem BreadCrumb {get;set;}
		public List <LocationItemPair> knockList {get;set;}
		public integer count {get;set;}
		 
		public LocationData (){
			this.breadCrumb = new LocationItem();
			knockList=new List <LocationItemPair>();
		}
	}
    
    
    public class LocationItemPair {

		public LocationItem knockLoc {get;set;}
		public LocationItem accountLoc {get;set;}
		public decimal proxDiff {get;set;}
		public string sProxDiff {get;set;}
		
		public LocationItemPair(LocationItem k,LocationItem a ){
			this.knockLoc=k;
			this.accountLoc=a;
			this.proxDiff=getDistance();
			this.sProxDiff=this.proxDiff+' mi';
		}

	
		private decimal getDistance() {
    		if (accountLoc.pLatitude!=null && knockLoc.pLatitude!=null && knockLoc.pLongitude!=null && accountLoc.pLongitude!=null){
    			decimal distance;
    			double r=3963.1;//miles
    			double toRad=57.29577951;
    			double kLat=double.valueOf(knockLoc.pLatitude)/toRad;
    			double kLon=double.valueOf(knockLoc.pLongitude)/toRad;
    			double aLat=double.valueOf(accountLoc.pLatitude)/toRad;
    			double aLon=double.valueOf(accountLoc.pLongitude)/toRad;
    			double dLat=(kLat-aLat);
    			double dLon=(kLon-aLon);
    	
 				decimal a =math.sin(kLat)*math.sin(aLat)+math.cos(kLat)*math.cos(aLat)*math.cos(kLon-aLon);
    			decimal c=math.acos(a);
    			distance=  ( r* c).setScale(2);
				return distance;
    		}
    		else {
    			return 0;
    		}	
    	}
	
	}
	
}