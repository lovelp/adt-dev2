public class CommentConfiguration {
    
    public String commentLength {get;set;}
    public String initialRowData{get;set;}
    public String dialerRecordId{get;set;}
    public Integer maxRowLength  {get;set;}
     
    public class CommentResponse{
        public String name;
        public String value;
        public String length;
        CommentResponse(){
         name ='';
         value ='';
         length = '';
        }
    }
    public CommentConfiguration(ApexPages.StandardController controller) {
        dialerRecordId = System.currentPagereference().getParameters().get('id');
        CampaignDefaults__c cd = CampaignDefaults__c.getInstance();
        commentLength = String.valueOf(Integer.valueOf(cd.commentLength__c));
        maxRowLength =  Integer.valueOf(cd.maxRowLength__c);
        this.buildInitialRow();       
    }
    
    @RemoteAction
    public static Map<String,String> buildCommentFields(){
            Map<String,String> objectField = new Map<String,String>();
            Schema.DescribeFieldResult fieldResultList = Dialer_List_Output_Control__c.Comment_Fields__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResultList.getPicklistValues();
            for(Schema.PicklistEntry p : ple){
                objectField.put(p.getLabel(), p.getValue());
            }
               
        return objectField;
    }
    
    @RemoteAction
    public static String backToDialer(String dialerRecordId){
        if(dialerRecordId != null){
            String baseUrlCampaign = URL.getSalesforceBaseUrl().toExternalForm();
            baseUrlCampaign = baseUrlCampaign+'/'+dialerRecordId;
            return baseUrlCampaign;
        }
        else {
            return null;
        }
    }
    
    @RemoteAction
    public static String saveToDialer(String dialerRecordId,String commentData,String commentJsonToAppend){
        System.debug('### final comment data is'+commentData);
        String updateStringWithOutComments = '[';
        List<Dialer_List_Output_Control__c> dialerRecord = [SELECT File_Output_Record_Layout__c,CommentFieldMapping__c FROM Dialer_List_Output_Control__c WHERE Id =:dialerRecordId LIMIT 1];
        if(dialerRecordId != null){
            System.debug('### data from the record is'+dialerRecord[0].File_Output_Record_Layout__c);
            //update json with new comments field
            JSONParser parser = JSON.createParser(dialerRecord[0].File_Output_Record_Layout__c);
            while(parser.nextToken() != null){
                if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'name')){
                    parser.nextToken();
                    if(!parser.getText().containsIgnoreCase('comment') && !parser.getText().containsIgnoreCase('campaign')){
                        updateStringWithOutComments += '{"name":"'+parser.getText()+'",';
                        parser.nextToken();
                        if(parser.getText() == 'length'){
                            parser.nextToken();
                            updateStringWithOutComments += '"length":"'+parser.getText()+'"},';
                        }
                    }
                    else if(parser.getText().containsIgnoreCase('campaign')){
                        updateStringWithOutComments += '{"name":"'+parser.getText()+'",';
                        parser.nextToken();
                        if(parser.getText() == 'length'){
                            parser.nextToken();
                            updateStringWithOutComments += '"length":"'+parser.getText()+'"}';
                        }                        
                    }
                }
            }
            
            dialerRecord[0].CommentFieldMapping__c = commentData;
            commentJsonToAppend = commentJsonToAppend.replace('[','');
            commentJsonToAppend = commentJsonToAppend.replace(']','');
            if(String.isBlank(commentJsonToAppend)){
              updateStringWithOutComments +=commentJsonToAppend+']';
            }
            else{
              updateStringWithOutComments +=','+commentJsonToAppend+']';
            }
            
            dialerRecord[0].File_Output_Record_Layout__c =updateStringWithOutComments;
            update dialerRecord[0];
            System.debug('### updated String without comments is'+updateStringWithOutComments);
            String baseUrlCampaign = URL.getSalesforceBaseUrl().toExternalForm();
            baseUrlCampaign = baseUrlCampaign+'/'+dialerRecordId;
            return baseUrlCampaign;
        }
        else {
            return null;
        }
    }
    
    public void buildInitialRow(){
      List<Dialer_List_Output_Control__c> dialerRecord = new List<Dialer_List_Output_Control__c>();
      dialerRecord = [SELECT CommentFieldMapping__c FROM Dialer_List_Output_Control__c WHERE Id =:dialerRecordId LIMIT 1];
      if(dialerRecord[0] != null && String.isNotBlank(dialerRecord[0].CommentFieldMapping__c)){
        System.debug('### before serialize'+dialerRecord[0].CommentFieldMapping__c);
        initialRowData = dialerRecord[0].CommentFieldMapping__c;
        System.debug('### after serialize'+initialRowData);
      }
      else{
        List<CommentResponse> commentResponseList=new List<CommentResponse>();
          CommentResponse commentResponseObject = new CommentResponse();
          commentResponseObject.name = 'Comment1';        
          commentResponseObject.value = '';        
          commentResponseObject.length = commentLength; 
          commentResponseList.add(commentResponseObject);
          initialRowData = JSON.serialize(commentResponseList);
      }
        
    }

}