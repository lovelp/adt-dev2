/************************************* MODIFICATION LOG ********************************************************************************************
* ChatterUtilities
*
* DESCRIPTION : Utility methods for working with chatter posts
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  2/10/2012               - Original Version
*
*                                                   
*/

public with sharing class ChatterUtilities {
    
    public static final String LINK_POST = 'LinkPost';
    public static final String TEXT_POST = 'TextPost';
    public static Boolean chatterFilesEvaluated = false; 
    
    /**
        chatterPost create a feed item of type "Text Post"
        
        @param  parentId    Parent ID of the feed item
        
        @param  bodytext    Text on the body of the post
        
        @param  insertRecord    True to insert record into database
        
        @return Returns FeedItem object record generated based on parameters
    */
    public static FeedItem chatterPost(
        Id parentId,
        String bodytext,
        Boolean insertRecord
    )
    {
        return chatterPost(parentId, bodytext, insertRecord, null);
    }
    
    
    /**
        chatterPost create a feed item of type "Link Post"
        
        @param  parentId    Parent ID of the feed item
        
        @param  bodytext    Text on the body of the post
        
        @param  insertRecord    True to insert record into database
        
        @param  linkurl URL of the chatter post link
        
        @return Returns FeedItem object record generated based on parameters
    */
    public static FeedItem chatterPost(
        Id parentId,
        String bodytext,
        Boolean insertRecord,
        String linkurl
    )
    {
        
        FeedItem fi = new FeedItem();
        Boolean isLimitedUser = false;
        String userprefix = Schema.Sobjecttype.User.getKeyPrefix();
        if (((String)parentId).startsWith(userprefix)) {
            User u = [Select Profile.Name from User where Id = :parentId];
            isLimitedUser = ProfileHelper.isLimitedAccessUser(u.Profile.Name);
        }
        
        // Check the supplied parent Id since there's no need to create a chatter post
        // for the global unassigned user or limited user
        if (parentId != Utilities.getGlobalUnassignedUser() && !isLimitedUser) {
        
            if (linkurl != null) 
            {
                fi.Type = LINK_POST;
                fi.LinkUrl = linkurl;
                fi.Title = 'Click for details.';
            }
            else 
            {
                fi.Type = TEXT_POST;
            }
            fi.parentId = parentId;
            fi.Body = bodytext;
            if (insertRecord) {
                insert fi;
            }
        }   
        return fi;
    }
    
    public static Boolean contains(List<String> sList, String value, Boolean ignoreCase) {
        if (sList != null && value != null) {
            for (String s : sList) {
                if (ignoreCase) {
                    if (s.equalsIgnoreCase(value)) {
                        return true;
                    }
                } else {
                    if (s.equals(value)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     *	Deletes document records.
     *	@param Set<String>	Ids for the documents to delete
     *  @return void
     */
    @future
    public static void DeleteContentDocuments(Set<String> cDocIDs){
    	List<ContentDocument> cDocumentList = new List<ContentDocument>([SELECT Id FROM ContentDocument WHERE Id IN :cDocIDs]);
    	delete cDocumentList;
    }
    
    /**
     *	Read configuration for current user and decide if his license is controlled by enhanced rules
     *	@method	isChatterEnhancedSecurity
     *	@return Boolean	If true then apply enhanced rules, bypass app otherwise
     */
    public static Boolean isChatterEnhancedSecurity(){
		// Read existing configuration
    	Set<String> licenseEnabledSet = ChatterUtilities.ChatterEnhancedLicenses(); 
    	User uObj = [SELECT Profile.Name, Profile.UserLicense.Name, Profile.UserLicenseId FROM User WHERE ID = :UserInfo.getUserId()];    	
    	return licenseEnabledSet.contains(uObj.Profile.UserLicense.Name);    	
    }     
    
    public static Boolean setChatterFileSuperUser(String uIdSelection){
    	try{
	    	if(!Utilities.isEmptyOrNull(uIdSelection))
	    	{
	    		Set<String> uIds = new Set<String>(uIdSelection.split(','));
	    		
	    		// Clear any existent configuration it will be overwritten by this new selection
	    		List<GroupMember> uMemberList = new List<GroupMember>([SELECT Id
																       FROM GroupMember 
																       WHERE Group.DeveloperName = :ChatterSecurity__c.getInstance().Chatter_Super_User_Group__c]);
				if(!uMemberList.isEmpty()){
					delete uMemberList;
				}
				
				// Read group
				Group UserGroupObj = [SELECT Id FROM Group WHERE DeveloperName = :ChatterSecurity__c.getInstance().Chatter_Super_User_Group__c limit 1];
				
				// Add members 
				List<GroupMember> newMembers = new List<GroupMember>();				
				for(String uId: uIds){
					GroupMember gMemberObj = new GroupMember(UserOrGroupId=uId, GroupId=UserGroupObj.Id);
					newMembers.add(gMemberObj); 
				}
				if(!newMembers.isEmpty())				
					insert newMembers; 
				
	    	}
	    	else{
	    		return false;
	    	}
    	}
    	catch(Exception err){
    		system.debug('[ChatterUtilities]::[setChatterFileSuperUser] \nERROR:'+err.getMessage()+' \nSTACK:'+err.getStackTraceString());
    		return false;
    	}
    	return true;
    } 
    
    /**
     *	Read configuration for enabled licenses
     *	@method ChatterEnhancedLicenses
     *	@return Set containing enabled licenses for this app
     */
    public static Set<String> ChatterEnhancedLicenses(){
		// Read existing configuration
	    String licenseEnabledStr = ChatterSecurity__c.getInstance().License_Enabled__c;
    	Set<String> licenseEnabledSet = new Set<String>(); 
    	if(!Utilities.isEmptyOrNull(licenseEnabledStr)){
    		for(String Str: licenseEnabledStr.split(',')){
    			licenseEnabledSet.add(Str);
    		}
    	}
    	return licenseEnabledSet;
    } 
    
	/**
	 *	Evaluates if the file can be uploaded or not depending on configuration settings
	 *	@param 	fType	Type of file
	 *	@param 	uId		User id uploading the file
	 *	@return	Boolean	The user can continue with the file upload or not
	 */
	public static Boolean isFileUploadValid(String fType, String uId){

        system.debug('\n fType='+fType+' uId='+uId+' \n');
        
		
        // Load file type exceptions from settings, these files do not require approval 
        List<String> FileTypeExceptions = new List<String>();
        FileTypeExceptions.addAll(FileTypeExceptions());
    	
        system.debug('\n EVAL='+(contains(FileTypeExceptions, fType, true) || isSuperUser(uId))+' \n');    	
    	
		return (contains(FileTypeExceptions, fType, true) || isSuperUser(uId));	 
	}

	public static Set<String> FileTypeExceptions(){
		Set<String> res = new Set<String>(); 
		String fTypeStr = ChatterSecurity__c.getInstance().file_type__c;
		if(!Utilities.isEmptyOrNull(fTypeStr)){
			for(String s: fTypeStr.split(',')){
				res.add(s); 
			}
		}
		return res;
	}

	public static Boolean isSuperUser(String userID){
		Boolean res = false;
		try{
			GroupMember gMember = [ SELECT Id, UserOrGroupId, Group.Name 
									FROM GroupMember 
									WHERE UserOrGroupId = :userID 
										AND Group.DeveloperName = :ChatterSecurity__c.getInstance().Chatter_Super_User_Group__c 
									limit 1];
			if(gMember!=null)
				res = true;
		}
		catch(Exception err){
			// user does not belong to this group, flag was already initialize with false no further action required
		}
		return res;
	}	
	
    /**
     *  Evaluates security parameters for file sharing on chatter posts
     *  @method EvalFileContent
     *  @param ContentVersionList   List of version content for files
     *  @return void
     */
    public static void EvalFileContent(Map<String,FeedItem >  feedItemObjMap, Set<String> RelatedRecordIdSet){
        
        // Evaluate file types and continue or abort for each feed 
        for(FeedItem feedItemObj: feedItemObjMap.values()){
        	
        	// Abort file upload and notify user this action is not permitted
        	if(!isFileUploadValid(feedItemObj.ContentType, UserInfo.getUserID())){
        		feedItemObj.addError('File upload restricted. Operation aborted.');
        	}
        	
        	// Continue with file upload only if the file type is not restricted or is a superuser        	
        	
        }
        
    }
	

	/**
	 *	Evaluates security parameters for file sharing on chatter comments to posts. Behavior is almost identical to overloaded signature.
	 *	@param feedCommentObjMap	Map containing all feed comments to process
	 *	@param RelatedRecordIdSet	All related files
	 *	@return void
	 */
    public static void EvalFileContent( Map<String,FeedComment> feedCommentObjMap, Set<String> RelatedRecordIdSet ){
		
		// Load additional information about the files not available on a feed comment
		Map<String, ContentVersion> relatedFileMap = 
			new Map<String, ContentVersion>([SELECT Id, FileType, Title 
											 FROM ContentVersion 
											 WHERE ID IN :RelatedRecordIdSet 
											  	AND IsLatest = true]); 
		
        // Create review records for each feed to review 
        for(FeedComment feedPostObj: feedCommentObjMap.values()){
        	ContentVersion cVerObj = relatedFileMap.get(feedPostObj.RelatedRecordId);
        	// Abort file upload and notify user this action is not permitted
        	if(!isFileUploadValid(cVerObj.FileType, UserInfo.getUserID())){
        		feedPostObj.addError('File upload restricted. Operation aborted.');
        	}

        	// Continue with file upload only if the file type is not restricted or is a superuser        	
        	
        }
        
    }
    
}