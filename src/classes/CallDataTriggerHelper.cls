public class CallDataTriggerHelper {
    
    public static void ProcessAccountDNISUpdate(List<Call_Data__c> cd){
        List<String> acc = new List<String>();
        List<String> dnis = new List<String>();
        List<Account> acclist = new List<Account>(); 
        Map<id,Account> accMap ;
        Map<id,DNIS__c> dnisMap;
        List<Call_Data__c> calldata = new List<Call_Data__c>();
        Map<id,Call_Data__c> callDataAccMap;
        System.debug('The call data is in trigger'+cd);
        for(Call_Data__c c: cd){
            //Capturing the call data ids that has the account and DNIS attached to it
            system.debug('Call data'+c);
            if(c.Account__c!=null && c.DNIS__c!=null){
                acc.add(c.Account__c);
                dnis.add(c.DNIS__c);
            }
        }
        System.debug('The call data is'+acc+' '+dnis);
        Date dt;
        if(ResaleGlobalVariables__c.getinstance('DaysToPopulateDNISOnAccount') != null && String.isNOTBlank(ResaleGlobalVariables__c.getinstance('DaysToPopulateDNISOnAccount').value__c)){
            dt = Date.today()-Integer.valueof(ResaleGlobalVariables__c.getinstance('DaysToPopulateDNISOnAccount').value__c);   
        }
        accMap = new Map<id,Account>([Select id,Name,DNIS__c,LastActivityDate__c,DNIS_Modifier_Name__c,DNIS_Modified_Date__c,(select id,createdDate,LastModifiedDate,Contact_Type__c,ownerid from CallData__r where CreatedDate <:dt order by createdDate asc NULLS LAST LIMIT 1) from Account where id in:acc]);
        dnisMap = new Map<id,DNIS__c>([Select id,Name from DNIS__c where id in:dnis]);
        callData = [Select id,name,CreatedDate,Account__c,ownerid,LastModifiedDate,DNIS__c,DNIS__r.Name from Call_Data__c where Account__c IN:acc and DNIS__c!=null order by CreatedDate desc NULLS LAST];
        callDataAccMap = new Map<id,Call_Data__c>();
        for(Call_Data__c cdata:callData){
            if(cdata.Account__c!=null){
                callDataAccMap.put(cdata.Account__c,cdata);
            }   
        }
        for(Call_Data__c c:cd){
            Integer diffDates;
            Integer dnisDiffDates;
            system.debug('Entered second for');
            if(!accMap.isempty() && String.IsnotBlank(c.Account__c)&& c.AccountLastActivityDate__c!=null && c.DNIS__c!=null){
                diffDates=c.AccountLastActivityDate__c.daysBetween(Date.today());
                if(accMap.get(c.Account__c).DNIS_Modified_Date__c!=null){
                    system.debug('Dnis Date Value'+ accMap.get(c.Account__c).DNIS_Modified_Date__c.Date());
                    dnisDiffDates = accMap.get(c.Account__c).DNIS_Modified_Date__c.Date().daysBetween(Date.today());   
                }
                System.Debug('Diff Dates'+diffDates);
                System.debug('Dnis Dates Dif'+dnisDiffDates+'Date.today()'+Date.today());
                //Update DNIS with recent call data if last activity date is greater than 90 days from today and DNIS modified date is null or To check last activity date and DNIS modified date is greater than 90 days from today
                if((diffDates > Integer.valueOf(ResaleGlobalVariables__c.getinstance('DaysToPopulateDNISOnAccount').value__c) && accMap.get(c.Account__c).DNIS_Modified_Date__c == null) || 
                   (dnisDiffDates!=null && diffDates > Integer.valueOf(ResaleGlobalVariables__c.getinstance('DaysToPopulateDNISOnAccount').value__c) && dnisDiffDates > Integer.valueOf(ResaleGlobalVariables__c.getinstance('DaysToPopulateDNISOnAccount').value__c)&& accMap.get(c.Account__c).DNIS_Modified_Date__c!=null)){
                       System.debug('Entered first if condition');
                       Account acct = accMap.get(c.Account__c);
                       acct.DNIS__c = dnisMap.get(c.DNIS__c).Name;
                       acct.DNIS_Modifier_Name__c = c.OwnerId;
                       acct.DNIS_Modified_Date__c = c.CreatedDate;
                       acclist.add(acct);
                   }//Populate DNIS based on the oldest call data within 90 days if Account had activity within 90 days but DNIS modified date or DNIS is null 
                else if(accMap.get(c.Account__c).DNIS_Modified_Date__c == null ||  accMap.get(c.Account__c).DNIS__c == null || accMap.get(c.Account__c).DNIS_Modifier_Name__c == null){
                    System.debug('Entered third if condition');
                    Account acct = accMap.get(c.Account__c);
                    System.debug('Call data DNIS'+acct.DNIS__c);
                    if(callDataAccMap.get(c.Account__c)!=null){
                        acct.DNIS__c = callDataAccMap.get(c.Account__c).DNIS__r.Name;
                        acct.DNIS_Modifier_Name__c = callDataAccMap.get(c.Account__c).ownerid;
                        acct.DNIS_Modified_Date__c = callDataAccMap.get(c.Account__c).CreatedDate;
                    }
                    acclist.add(acct);
                }
                system.debug('DNIS'+c.Account__r.DNIS__c+'ModifierName'+c.Account__r.DNIS_Modifier_Name__c+'DateTime'+c.Account__r.DNIS_Modified_Date__c);
            }
        }
        update acclist;
    }
}