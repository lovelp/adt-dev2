/************************************* MODIFICATION LOG ************************************************************
* ADTPartnerAccountLookupApi
*
* DESCRIPTION : Defines web services to enable SFDC to Partner integration
*               Rest API webservice
*-------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*-------------------------------------------------------------------------------------------------------------------
* Abhinav Pandey   09/21/2016    - Original Version
* Abhinav Pandey    06/29/2018    - Added ECOM Partner functionality
*
*/
@RestResource(urlMapping='/accountLookup/*')
global class ADTPartnerAccountLookupApi {
    
    @HttpPost
    global static void doPost() {
        String jsonCustRequest = RestContext.request.requestBody.toString();
        //String jsonCustRequest = httpRequest.getBody();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Boolean isRequiredFlag = true;
        //ADTPartnerAccountLookupSchema.AccountLookUpRequest custReq;
        System.debug('### initial req'+jsonCustRequest);
        Set<String> requiredFieldsStringSet = new Set<String>{'partnerId','partnerRepName','name','address','phones'};
        ADTPartnerAccountLookupSchema.AccountLookUpRequest custReq;
           ADTPartnerAccountLookupController controller = new ADTPartnerAccountLookupController();
        String custResponse;
        //response variable for returning the response 
        //check for apex class attribute match and error out if any other attribute is passed in json
        ADTPartnerAccountLookupSchema.AccountLookUpResponse clr = new ADTPartnerAccountLookupSchema.AccountLookUpResponse();
        try{
          custReq = (ADTPartnerAccountLookupSchema.AccountLookUpRequest)JSON.deserializeStrict(jsonCustRequest,ADTPartnerAccountLookupSchema.AccountLookUpRequest.class);
        }
        catch(exception e){
            isRequiredFlag = false;
            res.statusCode = 400;
            clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
        }
        
        // check for required attribute in json and also empty request
        try{
          if(isRequiredFlag){
             Map<String, Object> resMap = (Map<String, Object>)JSON.deserializeUntyped(jsonCustRequest);
             System.debug('### map for request is'+resMap);
             if(resMap.size() > 0){
                //check for invalid request
                for(String k : requiredFieldsStringSet){
                    if(!resMap.containsKey(k)|| resMap.get(k) == null || resMap.get(k)== ''){
                        isRequiredFlag = false;
                        res.statusCode = 400;
                        clr.errorStatusCode = 400;
                        clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                    }
                }
             }
             else {
                isRequiredFlag = false;
                res.statusCode = 400;
                clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
             }
          } 
        }
        catch(exception e){
               isRequiredFlag = false;
                res.statusCode = 400;
                clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
        }
        
        
        try{
            if(isRequiredFlag){
                Integer addrLengthMMB = Integer.valueOf(ResaleGlobalVariables__c.getinstance('MMBAddressLength').Value__c);
                Integer nameLenghtMMB = Integer.valueOf(ResaleGlobalVariables__c.getinstance('MMBNameLength').Value__c);
                Integer nameLenghtMMBWithSpace = Integer.valueOf(ResaleGlobalVariables__c.getinstance('MMBAccountNameLength').Value__c);
                Integer street1Length = custReq.address.addrLine1.length();
                Integer street2Length = String.isNotBlank(custReq.address.addrLine2) ? custReq.address.addrLine2.length() : 0;
                Integer fNameLength = String.isNotBlank(custReq.name.first) ? custReq.name.first.length() : 0;
                Integer lNameLength = String.isNotBlank(custReq.name.last) ? custReq.name.last.length() : 0;
                Integer companyNameLength = String.isNotBlank(custReq.name.company) ? custReq.name.company.length() : 0;
                if(street1Length > addrLengthMMB || street2Length > addrLengthMMB || (fNameLength + lNameLength) > nameLenghtMMB || (fNameLength + lNameLength + 1) > nameLenghtMMBWithSpace || companyNameLength > nameLenghtMMBWithSpace){
                    isRequiredFlag = false;
                    res.statusCode = 400;
                    clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                    isRequiredFlag = false;
                    ADTApplicationMonitor.log ('ADTPartner error Name,Company or Address greater than required limit.',PartnerAPIMessaging__c.getinstance('400').Error_Message__c+' and request is'+jsonCustRequest, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                }
                if(!custReq.address.postalCode.isNumeric()){
                    isRequiredFlag = false;
                    res.statusCode = 400;
                    isRequiredFlag = false;
                    clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                     ADTApplicationMonitor.log ('ADTPartner error postal code is not numeric',PartnerAPIMessaging__c.getinstance('400').Error_Message__c+' and request is'+jsonCustRequest, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                }
                if(!ADTPartnerAccountLookupController.validatePhoneFormat(custReq.phones.phone)){
                    isRequiredFlag = false;
                    res.statusCode = 400;
                    isRequiredFlag = false;
                    clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                }
            }
        }
        catch(exception e){
                isRequiredFlag = false;
                res.statusCode = 400;
                clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                ADTApplicationMonitor.log(e, 'ADTPartnerAccountLookup', 'Invalid request exceeding length'+' and request is'+jsonCustRequest, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        }
        
        
        //Get Partner User Parameters 
        try{
            ADTPartnerAccountLookupController.partnerList = ADTPartnerAccountLookupController.getPartnerName(custReq.partnerId);
            if(ADTPartnerAccountLookupController.partnerList.size() > 0 && ADTPartnerAccountLookupController.partnerList[0] != null  && String.isNotBlank(ADTPartnerAccountLookupController.partnerList[0].AccountLookUp__c) && String.isNotBlank(ADTPartnerAccountLookupController.partnerList[0].IntegrationUser__c)){
                List<String> listAllowed = ADTPartnerAccountLookupController.partnerList[0].AccountLookUp__c.split(';');
                ADTPartnerAccountLookupController.idSetForPartner.add(ADTPartnerAccountLookupController.partnerList[0].IntegrationUser__r.Id);
                ADTPartnerAccountLookupController.allowedFeaturesSet.addAll(listAllowed);
            }   
            else {
                clr.accounts = null;
                clr.locationFlags = null;
                clr.permits = null; 
                clr.fees = null;
                clr.alerts = null;
                res.statusCode = 500;
                isRequiredFlag = false;
                clr.message = PartnerAPIMessaging__c.getinstance('invalidPartnerId').Error_Message__c;
            }       
        }catch(exception e){
            clr.accounts = null;
            clr.locationFlags = null;
            clr.permits = null;
            clr.fees = null;
            clr.alerts = null;
            res.statusCode = 500;
            isRequiredFlag = false;
            clr.message = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
            ADTApplicationMonitor.log(e, 'AccountAPI', 'accountlookup'+' and request is'+jsonCustRequest, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        }
        
        //if no error in request run the api logic
        //1. Get the call Id
        try{
            if(String.isBlank(ADTPartnerAccountLookupController.errorMessageValue) && isRequiredFlag){
                String nondigits = '[^0-9]';      
                String digitRepeat = '([1-9])\\1{5}';
                custReq.phones.phone = custReq.phones.phone.replaceAll(nondigits,'');
                if(ADTPartnerAccountLookupController.allowedFeaturesSet.contains('Remove Phone')){
                    if(custReq.phones.phone.length() == 11 && custReq.phones.phone.subString(0,1) == '1' && custReq.phones.phone.subString(1).equals('9999999999')){
                        custReq.phones.phone='';
                    }
                    else if(custReq.phones.phone.length() == 10 && custReq.phones.phone.equals('9999999999')){
                        custReq.phones.phone='';
                    }
                }
                if(ADTPartnerAccountLookupController.allowedFeaturesSet.contains('Call Id')){
                    clr.callId = controller.getCallId(custReq);
                    if(clr.callId == null){
                        clr.opportunityId=null;
                        clr.callId = null;
                        clr.accounts = null;
                        clr.locationFlags = null;
                        clr.permits = null;
                        clr.fees = null;
                        clr.alerts = null;
                        res.statusCode = 500;
                        isRequiredFlag = false;
                        clr.message = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
                        String messErr = 'Partner API: accountLookup \n PartnerID: '+custReq.partnerID+' \n PartnerRepName: '+custReq.PartnerRepName+'\n';
                        ADTApplicationMonitor.log ('ADTPartner error while getting call ID',messErr+PartnerAPIMessaging__c.getinstance('500').Error_Message__c+' and request is'+jsonCustRequest, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                    }
                }
                if(ADTPartnerAccountLookupController.allowedFeaturesSet.contains('opportunity Id')){
                    clr.opportunityId = controller.getOppId();
                    if(clr.opportunityId == null){
                        clr.opportunityId=null;
                        clr.callId = null;
                        clr.accounts = null;
                        clr.locationFlags = null;
                        clr.permits = null;
                        clr.fees = null;
                        clr.alerts = null;
                        res.statusCode = 500;
                        isRequiredFlag = false;
                        clr.message = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
                        String messErr = 'Partner API: accountLookup \n PartnerID: '+custReq.partnerID+' \n PartnerRepName: '+custReq.PartnerRepName+'\n';
                        ADTApplicationMonitor.log ('ADTPartner error while getting Opportunity ID',messErr+PartnerAPIMessaging__c.getinstance('500').Error_Message__c+' and request is'+jsonCustRequest, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                    }
                }
            }
        }catch(Exception e){
            clr.accounts = null;
            clr.locationFlags = null;
            clr.permits = null;
            clr.fees = null;
            clr.alerts = null;
            res.statusCode = 500;
            isRequiredFlag = false;
            clr.message = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
            ADTApplicationMonitor.log(e, 'AccountAPI', 'accountlookup'+' and request is'+jsonCustRequest, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        }
      
        //3. Get the other parameters
        try{
            if(String.isBlank(ADTPartnerAccountLookupController.errorMessageValue) && isRequiredFlag){
                //get account list
                if(ADTPartnerAccountLookupController.allowedFeaturesSet.contains('Account List')){
                    clr.accounts = controller.getAccountList(custReq);  
                }
                //get location flags
                if(ADTPartnerAccountLookupController.allowedFeaturesSet.contains('Location Flags')){
                    clr.locationFlags = controller.getLocationFlags();  
                }
                //get permit list
                if(ADTPartnerAccountLookupController.allowedFeaturesSet.contains('Permits')){
                    clr.permits = controller.getPermitList();   
                }
                //get Fees
                if(ADTPartnerAccountLookupController.allowedFeaturesSet.contains('Trip Fee')){
                    clr.fees = controller.getTripFeeList(); 
                }
                //get alerts
                if(ADTPartnerAccountLookupController.allowedFeaturesSet.contains('Alerts')){
                    clr.alerts = controller.getAlertsForRequest(custReq);   
                }
                //get Leads List
                //if(ADTPartnerAccountLookupController.allowedFeaturesSet.contains('Lead List')){
                    //clr.leads = controller.getLeadsList(custReq);   
                //}
                

            }
        }catch(Exception e){
            clr.accounts = null;
            clr.locationFlags = null;
            clr.permits = null;
            clr.fees = null;
            clr.alerts = null;
            System.debug(e.getStackTraceString());
            res.statusCode = 500;
            isRequiredFlag = false;
            clr.message = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
        }
         
        // 6.error message returned from controller logic
        if(String.isNotBlank(ADTPartnerAccountLookupController.errorMessageValue)){
            clr.message =ADTPartnerAccountLookupController.errorMessageValue;
            res.statusCode = 404;
        } 
        
        res.addHeader('Content-Type', 'application/json');
        res.responseBody = Blob.valueOf(JSON.serialize(clr,true));
        String JsonResp = JSON.serialize(clr); 
        system.debug('json resp to rv:'+JsonResp);
    }
}