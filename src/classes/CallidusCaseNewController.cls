/************************************* MODIFICATION LOG ********************************************************************************************
* CallidusCaseNewController
*
* DESCRIPTION : Handle initial information case form submission.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                                             DATE                                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera                               11/30/2014                               - Added controlling logic for case
*                                                                                                       
*/

global with sharing class CallidusCaseNewController {	
	
	public Account 	acc 			{get;set;}
	public Case 	caseObj 		{get;set;}
	public String 	def_account_id 	{get;set;}
	public String 	retUrl 			{get;set;}
	public Boolean 	initSuccess 	{get;set;}
	public String 	rtCallidusId 	{get;set;}
	public Boolean  renderClosedCases {get;set;}
	public Boolean  renderOpenCases {get;set;}
	
	ApexPages.StandardController stdController;
	
	private Boolean validatePageInit(){
		Boolean resVal = true;
		// validate account was sent to this page
		if(Utilities.isEmptyOrNull(def_account_id)){
			//resVal = false;
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.WARNING, 'You\'re starting a case without an account. Some tracking might not be available.') );            			
		}
		else{
			try{
				acc = [SELECT name, MMBCustomerNumber__c FROM Account WHERE Id = :def_account_id];
				caseObj.MMB_Customer__c = acc.MMBCustomerNumber__c;
			}
			catch(Exception err){
            	ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find account. Either does not exist or you do not have access privileges to this information. Id ['+def_account_id+']') );
            	resVal = false;            			
			}
		}
		return resVal;
	}
	
	public CallidusCaseNewController(){
		PageInit();
	}
	
	public void PageInit(){
		renderClosedCases = true;
		renderOpenCases = true;
    	def_account_id = ApexPages.currentPage().getParameters().get('def_account_id');
    	retUrl = ApexPages.currentPage().getParameters().get('retUrl');
    	rtCallidusId = ApexPages.currentPage().getParameters().get('RecordType');
    	caseObj = new Case( Compensation_Plan__c = '', Commission_Reason_Code__c='', Subject='', Description='');		
    	initSuccess = validatePageInit();
	}
	
	public PageReference UpdateCaseSubject(){
		if(!Utilities.isEmptyOrNull(caseObj.Compensation_Plan__c)){
			caseObj.Subject = caseObj.Compensation_Plan__c;
		}
		if(!Utilities.isEmptyOrNull(caseObj.Commission_Reason_Code__c)){
			caseObj.Subject += '|' + caseObj.Commission_Reason_Code__c;
		}
		return null;
	}
	
	public PageReference save(){
		
		// Validate this case is not duplicated while still open
		List<Case> openCaseList = new List<Case>();
		if(acc != null){
			openCaseList = [SELECT CaseNumber FROM Case WHERE RecordType.Name = 'Callidus' AND Status<> 'Closed' AND Commission_User__c = :UserInfo.getUserId() AND AccountId = :acc.Id AND Compensation_Plan__c = :caseObj.Compensation_Plan__c AND Commission_Reason_Code__c = :caseObj.Commission_Reason_Code__c];
		}
		else{
			openCaseList = [SELECT CaseNumber FROM Case WHERE RecordType.Name = 'Callidus' AND Status<> 'Closed' AND Commission_User__c = :UserInfo.getUserId() AND Compensation_Plan__c = :caseObj.Compensation_Plan__c AND Commission_Reason_Code__c = :caseObj.Commission_Reason_Code__c];
		}
		if( !openCaseList.isEmpty() ){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Our records indicate you have a case open for this Compensation Plan <strong>'+caseObj.Compensation_Plan__c+'</strong> and Commission Reason Code <strong>'+caseObj.Commission_Reason_Code__c+'</strong>.') );
            return null;
		}
		
		// Default case subject
		UpdateCaseSubject();
		
		// Continue to save case
		try{
			// Case defaults
			caseObj.Reason = 'New problem';	
			if(acc != null )	
				caseObj.AccountId = acc.Id;	
			caseObj.Type = 'Callidus';
			caseObj.RecordTypeId = rtCallidusId;
			caseObj.Commission_User__c = UserInfo.getUserId();
			// Assign to callidus queue
			try{
				Group gQueue = [SELECT Type, Name, DeveloperName FROM Group WHERE Type = 'Queue' AND (Name = 'Callidus' OR DeveloperName='Callidus') limit 1];
				caseObj.OwnerId = gQueue.Id; 
			}
			catch(Exception err){ 
            	ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable assign case to Queue. ' + err.getMessage() ) );
			}
			
			// Save case details
			insert caseObj;
			// Redirect user after saving case
			if( acc!= null )
        		return new PageReference('/'+acc.Id);
        	else
        		return new PageReference('/500/o');
		}
		catch(Exception err){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable save Case. ' + err.getMessage() ) );
            return null;	
		}
		return null;
	}
	
	/**
	 *	Check some record consistencies before continue with saving without affecting our viewstate and let the user know
	 *	
	 *	@method	checkCustomerNoCase
	 *	
	 *	@param	String	MMB Customer Number to check
	 *
	 *	@param	String	Compensation Plan
	 *
	 *	@param	String	Reason Code
	 *
	 *	@return	Boolean	True if it passes validation, False otherwise
	 *
	 */
	@RemoteAction 
	global static Boolean validateCaseInfo(String MMBCustomerNo, String compPlan, String reasonCode){
		// Check for exceptions on this information
		List<Case> submittedCaseList = new List<Case>([SELECT CaseNumber FROM Case WHERE MMB_Customer__c = :MMBCustomerNo AND Compensation_Plan__c = :compPlan AND Commission_Reason_Code__c = :reasonCode AND Status = 'Closed' AND Commission_User__c = :UserInfo.getUserId()]);
		return submittedCaseList.isEmpty();
	}

	/**
	 *	Read closed cases for execution context user 
	 *	
	 *	@method	getUserCallidusCases
	 *	
	 *	@param	String		Case status to filter
	 *
	 *	@return	List<Case>	List of cases found for this user with the specified status
	 *
	 */
	@RemoteAction 
	global static List<Case> getUserCallidusCases(String aId){
		List<Case> resList;
		// Check for exceptions on this information
		if( Utilities.isEmptyOrNull(aId) ) {
			resList = new List<Case>([SELECT CaseNumber, Status, Commission_Error_Final_Disposition__c, Expected_Commission_Amount__c, Actual_Adjustment_Amount__c, Commission_Reason_Code__c, Compensation_Plan__c 
									  FROM Case 
									  WHERE Commission_User__c = :UserInfo.getUserId()]);
		}
		else {
			resList = new List<Case>([SELECT CaseNumber, Status, Commission_Error_Final_Disposition__c, Expected_Commission_Amount__c, Actual_Adjustment_Amount__c, Commission_Reason_Code__c, Compensation_Plan__c 
									  FROM Case 
									  WHERE Commission_User__c = :UserInfo.getUserId()
									  	AND AccountId = :aId]);
		}							  
		return resList;
	}
	
}