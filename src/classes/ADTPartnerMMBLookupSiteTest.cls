@IsTest
public class ADTPartnerMMBLookupSiteTest {
    
    @IsTest
    public static void testADTPartnerMMB(){
        String siteID = '48886213';
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'HOATeamPhone',value__c = '123457'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'P1HOAPhNumber',value__c = '2'));
        insert rgvList;

        ResaleGlobalVariables__c resaleGlobalVar=new ResaleGlobalVariables__c(); //Custom Setting for Contact Fields
        resaleGlobalVar.Name='P1RententionPhNumber';//Static record 1 of custom setting
        resaleGlobalVar.Value__c='1-877-776-1911';
        insert resaleGlobalVar;
        List<MMBCustomerSiteSearchApi.SiteInfoResponse> siteInformationList = new List<MMBCustomerSiteSearchApi.SiteInfoResponse>();
        MMBCustomerSiteSearchApi.SiteInfoResponse siteInfo = new MMBCustomerSiteSearchApi.SiteInfoResponse();
        siteInformationList.add(siteInfo);
        MMBCustomerSiteSearchApi.CustomerInfoResponse CustomerInformation = new MMBCustomerSiteSearchApi.CustomerInfoResponse();
        ADTPartnerMMBLookupSite adtPartnermmb = new ADTPartnerMMBLookupSite(); 
        //adtPartnermmb.HOAalert = false;
        string str = adtPartnermmb.P1TransferCallPhoneNumber;
        str = adtPartnermmb.P1HOAPhoneNumber;
        str = adtPartnermmb.HOATeamCallPhoneNumber;
        adtPartnermmb.MMBLookupSiteInformation(siteID);
        adtPartnermmb.MMBLookupCustomerInformation(siteInformationList);
        
    }
    
}