/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : DeleteTasktest is a test class for Deletetask batch class
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Giribabu Geda      05/08/2018      - Origininal Version
*
*                           
*/
@isTest
private class DeleteTasktest {

    Static testmethod void testDeleteTasks(){
    	insert (new BatchGlobalVariables__c(Name='DaysOverDuefortaskdeletion', value__c='14'));
    	insert (new BatchGlobalVariables__c(Name='Statusesfortaskdeletion', value__c='\'Completed\',\'Not Started\''));
    	insert (new BatchGlobalVariables__c(Name='GlobalAdminId', value__c='00530000005dBvw'));
    	List<Task> lstTask = new list<Task>();
    	for(integer i=0;i<200;i++){
	    	Task tsk =new Task();    	
	    	tsk.Subject 		='Follow Up'+i;
	    	tsk.activitydate	=System.today() -20;
	    	tsk.status			='Not Started';
	    	tsk.Priority		='Normal';
	    	lstTask.add(tsk);
    	}
    	if(lstTask !=null && lstTask.size()>0){
    		insert lstTask;
    	}
    	
    	Test.startTest();
    		Database.executeBatch(New DeleteTasks());
    	Test.stopTest();
    	List<Task> lstTaskAfterdelete = [Select id from Task];
    	system.assertEquals(0,lstTaskAfterdelete.size());
    	
    }
    
    Static testmethod void testscheduleDeleteTasks(){
    	Test.startTest();
		    insert (new BatchGlobalVariables__c(Name='DaysOverDuefortaskdeletion', value__c='14'));
    		insert (new BatchGlobalVariables__c(Name='Statusesfortaskdeletion', value__c='\'Completed\',\'Not Started\''));
    		insert (new BatchGlobalVariables__c(Name='GlobalAdminId', value__c='00530000005dBvw'));
		    ScheduleDeleteTasks sh1 = new ScheduleDeleteTasks();
		
			String sch = '0 0 23 * * ?'; 
			system.schedule('Test Delete Schedule', sch, sh1); 
		Test.stopTest(); 
	
	}  
}