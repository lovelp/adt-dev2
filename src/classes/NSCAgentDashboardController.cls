public without sharing class NSCAgentDashboardController {
    
    public Call_Data__c calldata{get;set;}
    public integer callsanswered{get;set;}
    public integer callsans{get;set;}
    public integer outboundCallanswered{get;set;}
    public List<Call_Data__c> recentCalls{get;set;} 
    public Boolean isInConsole{get;set;}
    public String agentdashboardtTitle{
        get{
            return 'As of '+ DateTime.now().format();
        }
    }
    public String todaydate{
        get{
            return Date.today().format();
        }
    }
    public String Yesterdaydate{
        get{
            return Date.today().addDays(-1).format();
        }
    }
    public String accPrefix{
        get{
            return Schema.getGlobalDescribe().get('Account').getDescribe().getKeyPrefix();
        }
    }
    //Constructor
    public NSCAgentDashboardController(){
        getinfo();
    } 
    
    public PageReference rerenderCallInfo(){
        isInConsole = true;
        return null;
    }
    
    public PageReference getinfo(){
        Integer today=[SELECT Count() FROM Call_Data__c WHERE Received_By__c=:userinfo.getuserid() and Contact_Type__c ='Inbound' and createddate=today];
        callsanswered=today;
        
        Integer yesterday=[SELECT Count() FROM Call_Data__c WHERE Received_By__c=:userinfo.getuserid() and Contact_Type__c ='Inbound' and createddate=yesterday];
        callsans=yesterday;
        
        Integer outboundcalls=[SELECT Count() FROM Call_Data__c WHERE Received_By__c=:userinfo.getuserid() and Contact_Type__c ='Outbound' and createddate=today];
        outboundCallanswered=outboundcalls;
        
        Set<Id> callDataIds = new Set<Id>();
        for (RecentlyViewed recentItms : [SELECT Id FROM RecentlyViewed WHERE Type IN ('Call_Data__c') ORDER BY LastViewedDate DESC Limit 15]){
            callDataIds.add(recentItms.Id);
        }

        recentCalls = new List<Call_Data__c>([SELECT Id, Name, DNIS__c, DNIS__r.name, ANI__c, Account__c, Account__r.Name, Account__r.Phone, Lead__c, Lead__r.Name, Lead__r.Phone, Call_Disposition__r.name, CreatedDate FROM Call_Data__c WHERE Id IN :callDataIds ORDER BY CreatedDate DESC]);
        return null;
    }
}