@RestResource(urlMapping='/updateOpportunity/*')
global with sharing class UpdateOpportunityAgreementAPI{
    @HttpPost
    global static void dopostmethod(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        System.debug('The req is'+req.requestBody.toString());
        if(String.isBlank(req.requestBody.toString())){
            res.statuscode = 400;
            res.responseBody = serAllErrors(CommercialMessages__c.getInstance('Emptyrequest').Message__c, 400, null);
        }
        else{
            SalesAgreementSchema.SalesAgreementLookupResponse saleAgreeReq = new SalesAgreementSchema.SalesAgreementLookupResponse();
            saleAgreeReq = (SalesAgreementSchema.SalesAgreementLookupResponse) System.JSON.deserialize(req.requestBody.toString(), SalesAgreementSchema.SalesAgreementLookupResponse.class);
            System.debug('The req is'+saleAgreeReq);
            list<Opportunity> opp = [SELECT Id,AccountId FROM Opportunity WHERE Id =:saleAgreeReq.sfdcOpportunityID];
            //Prepare response on success
            if(opp.size()>0){
                try{
                    opp[0].LookupSalesAgreementResponseJSON__c = req.requestBody.toString();
                    SalesAgreementAPI.populateOppFromResponse(saleAgreeReq, opp[0]);
                    res.statuscode = 200;
                    res.responseBody = createSuccess(opp[0].Id);
                }catch(Exception ae){
                    res.statuscode = 500;
                    res.responseBody = serAllErrors(CommercialMessages__c.getInstance('500').Message__c, 500, opp[0].Id);
                      ADTApplicationMonitor.log ('Sales pilot request', ae.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
                }
            }
            else if(String.isBlank(saleAgreeReq.sfdcOpportunityID)){
                res.statuscode = 400;
                res.responseBody = serAllErrors(CommercialMessages__c.getInstance('OppIdRequired').Message__c, 400, null);
            }
            else{
                res.statuscode = 404;
                res.responseBody = serAllErrors(CommercialMessages__c.getInstance('OptyNotFound').Message__c, 404, saleAgreeReq.sfdcOpportunityID);
            }
        }
        
    }
    public static Blob serAllErrors(String errorMessage, Integer StatusCode, String optyId){
        ErrorJSON adtError = new ErrorJSON();
        errors err = new errors();
        err.status = String.valueOf(StatusCode);
        err.errorMessage = errorMessage+' Opporunity Id: '+optyId;
        err.errorCode = '-1';//Default code if Sfdc has error
        list<errors> errlist = new list<errors>();
        errlist.add(err);
        adtError.errors = errlist;
        String errString = JSON.serialize(adtError, true);
        return Blob.valueOf(errString);
    }
    
    public static Blob createSuccess(String optyId){
        SuccessJson sjson = new SuccessJson();
        sjson.OpportunityId = optyId;
        sjson.message = 'Update Successful.';
        String jsonStr = JSON.serialize(sjson);
        return Blob.valueOf(jsonStr);
    }
    
    public class SuccessJson{
        public String OpportunityId;
        public String message;
    }
    public class ErrorJSON{
        public list<errors> errors;
    }
    public class errors{
        public string status;
        public string errorCode;
        public string errorMessage;
    } 
}