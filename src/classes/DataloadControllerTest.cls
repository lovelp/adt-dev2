@isTest 
public class DataloadControllerTest{
    public static DataloadTemp__c dlAcc,dlAcc1,dlLead;
    public static final Integer totalCount = 200;
    public static void createTestData(){
        DataloadTemp__c dlAcc,dlLead;
        dlAcc=new DataloadTemp__c();
        //dl.Name='20180627-0000000119';
        dlAcc.Type__c='Account';
        //dl.DataSource__c='Bulk Import';
        dlAcc.GroupId__c='SYARR-20180627-12';
        dlAcc.BusinessId__c='1100 - Residential';
        dlAcc.MmbSiteNo__c='46995701';
        dlAcc.AddressLine1__c='abc';
        dlAcc.AddressLine2__c='xyz';
        dlAcc.SitePostalCode__c='33029';
        dlAcc.SiteLastName__c='DemoLastName';
        dlAcc.Processed__c=false;
        insert dlAcc;
         
        dlAcc1=new DataloadTemp__c();
        //dl.Name='20180627-0000000119';
        dlAcc1.Type__c='Account';
        //dl.DataSource__c='Bulk Import';
        dlAcc1.GroupId__c='SYARR-20180627-2';
        dlAcc1.BusinessId__c='1200 - Small Business';
        dlAcc1.MmbSiteNo__c='46995701';
        dlAcc1.AddressLine1__c='abc2';
        dlAcc1.AddressLine2__c='xyz';
        dlAcc1.SitePostalCode__c='400-064';
        dlAcc1.SiteLastName__c='DemoLastName2';
        
        dlAcc1.Processed__c=false;
        insert dlAcc1;
        
        Affiliate__c aff = new Affiliate__c(Name = 'Test123');
        insert aff;

        IntegrationSettings__c intSet=new IntegrationSettings__c(Telemar_Number_Nextup__c=1,SetupOwnerId=UserInfo.getOrganizationId());
        insert intSet;
        
        // IntegrationSettings__c intSet=new IntegrationSettings__c(Telemar_Number_Nextup__c=1,SetupOwnerId=UserInfo.getOrganizationId());
        //insert intSet;
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.Name = '99999';
        pc.BusinessID__c = '1100';
        insert pc;
        

        userrole ur=[select id,developername from userrole where id=:userinfo.getUserRoleId()];
        
        list<ResaleGlobalVariables__c> globalVars = new list<ResaleGlobalVariables__c>();
        
        ResaleGlobalVariables__c rsaleGlobal = new ResaleGlobalVariables__c();
        rsaleGlobal.Name= 'DataRecastDisableAccountTrigger';
        rsaleGlobal.Value__c = 'FALSE';  
        globalVars.add(rsaleGlobal);
        
        ResaleGlobalVariables__c rsaleGlobal1 = new ResaleGlobalVariables__c();
        rsaleGlobal1.Name= 'IntegrationUser';
        rsaleGlobal1.Value__c = userInfo.getName();  
        globalVars.add(rsaleGlobal1);
        
        ResaleGlobalVariables__c rsaleGlobal2 = new ResaleGlobalVariables__c();
        rsaleGlobal2.Name= 'RIFAccountOwnerAlias';
        rsaleGlobal2.Value__c = userinfo.getFirstName().substring(0,1)+userinfo.getLastName().substring(0,4);  
        globalVars.add( rsaleGlobal2);
        
        ResaleGlobalVariables__c rsaleGlobal3 = new ResaleGlobalVariables__c();
        rsaleGlobal3.Name= 'DataConversion';
        rsaleGlobal3.Value__c = 'test';
        globalVars.add( rsaleGlobal3);
        
        ResaleGlobalVariables__c rsaleGlobal4 = new ResaleGlobalVariables__c();
        rsaleGlobal4.Name= 'GlobalResaleAdmin';
        rsaleGlobal4.Value__c = userInfo.getName();
        globalVars.add( rsaleGlobal4);
        
        ResaleGlobalVariables__c rsaleGlobal5 = new ResaleGlobalVariables__c();
        rsaleGlobal5.Name= 'DevconPhoneSaleRole';
        rsaleGlobal5.Value__c = ur.DeveloperName;
        globalVars.add( rsaleGlobal5);
        
        ResaleGlobalVariables__c rsaleGlobal6 = new ResaleGlobalVariables__c();
        rsaleGlobal6.Name= 'NewRehashHOARecordOwner';
        rsaleGlobal6.Value__c = userInfo.getUserName();
        globalVars.add( rsaleGlobal6);
        
        insert globalVars;
                 
        AlphaConversion__c ac=new AlphaConversion__c();
        ac.name='test';
        ac.AlphaCharValue__c='0';
        insert ac;
        
        ErrorMessages__c errmsg = new ErrorMessages__c();
        errmsg.Name= 'Missing_Address';
        errmsg.message__c = 'Fail';
        insert errmsg;
        
    }
    
    /*private static void createLeadTestData(){
        dlAcc1=new DataloadTemp__c();
        //dl.Name='20180627-0000000119';
        dlAcc1.Type__c='Lead';
        //dl.DataSource__c='Bulk Import';
        dlAcc1.GroupId__c='SYARR-20180627-1';
        dlAcc1.BusinessId__c='1200 - Small Business';
        dlAcc1.MmbSiteNo__c='46995701';
        dlAcc1.AddressLine1__c='abc1';
        dlAcc1.AddressLine2__c='xyz1';
        dlAcc1.SitePostalCode__c='400-064';
        dlAcc1.SiteLastName__c='DemoLastName1';
       
        dlAcc1.Processed__c=false;
        insert dlAcc1;
    }*/
    
    static testmethod void createTestBatchforAccount(){
        createTestData();
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        
        List<DataloadTemp__c> dltemp = new List<DataloadTemp__c>();
        List<DataloadTemp__c> dltemp1 = new List<DataloadTemp__c>();
        for(integer i=4;i<totalCount;i++){
            DataloadTemp__c dlAcc = new DataloadTemp__c();
            dlAcc=new DataloadTemp__c();
            dlAcc.Type__c='Account';
            dlAcc.BusinessId__c='1100 - Residential';
            dlAcc.MmbSiteNo__c='46995701';
            dlAcc.AddressLine1__c='4201 abc';
            dlAcc.AddressLine2__c='xyz';
            dlAcc.SitePostalCode__c='99999';
            dlAcc.SiteLastName__c='DemoLastName'+i;
            dlAcc.Processed__c=false;
            dlAcc.CreatedDate = System.now()- 34;
            dlAcc.SiteCity__c='Los Angeles';
            dlAcc.SiteState__c='California';
            dlAcc.AffiliateName__c = 'Test123';
            dltemp.add(dlAcc);
        }
        
        if(dltemp!=null && dltemp.size()>0)
        insert dltemp;
        
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(dlTemp);
        DataloadController dlc=new DataloadController(stdSetController);
        DataloadController.dataloadWrapper dlWrap =new DataloadController.dataloadWrapper('abc',0);
        list<SelectOption> selOptList=dlc.getgroupIdOption();
        dlc.selectedGroups='SYARR-20180627-1';
        dlc.jsonString='';
        dlc.processBatch();
        DataLoadBatch dlBatch = New DataLoadBatch('SYARR-20180627-1');
        QL = dlBatch.start(BC);
        dlBatch.execute(BC, dltemp);
        dlBatch.finish(BC);      
    }


    static testmethod void createTestBatchforLead(){
        createTestData();
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        
        // IntegrationSettings__c intSet=new IntegrationSettings__c(Telemar_Number_Nextup__c=1,SetupOwnerId=UserInfo.getOrganizationId());
        //insert intSet;
        /*Postal_Codes__c pc = new Postal_Codes__c();
        pc.Name = '99999';
        pc.BusinessID__c = '1100';
        insert pc;
        

        userrole ur = [select id,developername from userrole where id=:userinfo.getUserRoleId()];
        
        ResaleGlobalVariables__c rsaleGlobal = new ResaleGlobalVariables__c();
        rsaleGlobal.Name= 'DataRecastDisableAccountTrigger';
        rsaleGlobal.Value__c = 'FALSE';  
        insert rsaleGlobal;
        
     
        
            //User u=TestHelperClass.createUser(0);
        //System.debug('name'+u.name+u.FirstName+u.LastName);
        ResaleGlobalVariables__c rsaleGlobal1 = new ResaleGlobalVariables__c();
        rsaleGlobal1.Name= 'IntegrationUser';
        rsaleGlobal1.Value__c = userInfo.getName();  
        insert rsaleGlobal1;
        
        ResaleGlobalVariables__c rsaleGlobal2 = new ResaleGlobalVariables__c();
        rsaleGlobal2.Name= 'RIFAccountOwnerAlias';
            rsaleGlobal2.Value__c = userinfo.getFirstName().substring(0,1)+userinfo.getLastName().substring(0,4);  
        insert rsaleGlobal2;
            
        ResaleGlobalVariables__c rsaleGlobal3 = new ResaleGlobalVariables__c();
        rsaleGlobal3.Name= 'DataConversion';
        rsaleGlobal3.Value__c = 'test';
        insert rsaleGlobal3;
        
        ResaleGlobalVariables__c rsaleGlobal4 = new ResaleGlobalVariables__c();
        rsaleGlobal4.Name= 'GlobalResaleAdmin';
        rsaleGlobal4.Value__c = userInfo.getName();
        insert rsaleGlobal4;
         
        ResaleGlobalVariables__c rsaleGlobal6 = new ResaleGlobalVariables__c();
        rsaleGlobal6.Name= 'GlobalNSCAdmin';
        rsaleGlobal6.Value__c = userInfo.getName();
        insert rsaleGlobal6;
        
        
        ResaleGlobalVariables__c rsaleGlobal5 = new ResaleGlobalVariables__c();
        rsaleGlobal5.Name= 'DevconPhoneSaleRole';
        rsaleGlobal5.Value__c = ur.DeveloperName;
        insert rsaleGlobal5;
        
         ErrorMessages__c errmsg = new ErrorMessages__c();
        errmsg.Name= 'Missing_Address';
        errmsg.message__c = 'Fail';
        insert errmsg;
        
            
        
                     
        AlphaConversion__c ac=new AlphaConversion__c();
        ac.name='test';
        ac.AlphaCharValue__c='0';
        insert ac;*/
        
            List<DataloadTemp__c> dltemp = new List<DataloadTemp__c>();
        List<DataloadTemp__c> dltemp1 = new List<DataloadTemp__c>();
        for(integer i=0;i<totalCount;i++){
            DataloadTemp__c dlAcc = new DataloadTemp__c();
            dlAcc=new DataloadTemp__c();
            dlAcc.Type__c='Lead';
            dlAcc.BusinessId__c='1100 - Residential';
            dlAcc.MmbSiteNo__c='46995701';
            dlAcc.AddressLine1__c='4201 abc';
            dlAcc.AddressLine2__c='xyz';
            dlAcc.SitePostalCode__c='99999';
            dlAcc.SiteLastName__c='DemoLastName'+i;
            dlAcc.Processed__c=false;
            dlAcc.CreatedDate = System.now()- 34;
            
           
            dltemp.add(dlAcc);
           
        }
        
        if(dltemp!=null && dltemp.size()>0)
            insert dltemp;
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(dlTemp);
        DataloadController dlc=new DataloadController(stdSetController);
        DataloadController.dataloadWrapper dlWrap =new DataloadController.dataloadWrapper('abc',0);
        list<SelectOption> selOptList=dlc.getgroupIdOption();
        dlc.selectedGroups='SYARR-20180627-1';
        dlc.jsonString='';
        dlc.processBatch();
        DataLoadBatch dlBatch = New DataLoadBatch('SYARR-20180627-1');
        QL = dlBatch.start(BC);
        dlBatch.execute(BC, dltemp);
        dlBatch.finish(BC);      
    }

    
}