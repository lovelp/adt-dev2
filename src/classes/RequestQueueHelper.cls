/************************************* MODIFICATION LOG ********************************************************************************************
* RequestQueueHelper
*
* DESCRIPTION : Responsible for removing unneeded records from the Request Queue object
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					3/5/2012				- Original Version
*
*													
*/


public with sharing class RequestQueueHelper {
	
	// Items in the Request Queue are retained for a maximum of 30 days after their creation
	private static final Integer DAYS_TO_RETAIN_ITEM = 30;
	
	// Finds and deletes Request Queue items that were created more than 30 days ago
	public static void deleteExpiredItems() {
		
		// Retrieve the custom settings
		IntegrationSettings__c settings = IntegrationSettings__c.getOrgDefaults();
        
        // Assume the default value defined here
        Integer daysToRetain = DAYS_TO_RETAIN_ITEM;
        // But use the value from the custom setting as an override
        if (settings.RequestQueueRetentionLimit__c > 0) {
        	daysToRetain = Integer.valueOf(settings.RequestQueueRetentionLimit__c);
        }
        
		Datetime rightNow = Datetime.now();
		Datetime deleteBeforeDatetime = rightNow.addDays(-1 * daysToRetain);
	
		
		// This query will be subject to the limit of 10,000 records returned from a SOQL statement.
		// Based on the size of the user base and the frequency of messages to be sent asynchronous, do not  
		// expect to approach this limit with an given week.  
		// If over time, the limit is reached, this method will need to refactored to process batches of records.
		RequestQueue__c[] requestQueueArray = [select ID from RequestQueue__c where RequestDate__c <= :deleteBeforeDatetime limit 10000];
		
		try {
			delete requestQueueArray;
		} catch (Exception e) {
			System.debug('Unable to delete expired RequestQueue items: ' + e.getMessage());
		}
		
	}

}