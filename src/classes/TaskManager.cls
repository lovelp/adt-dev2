/************************************* MODIFICATION LOG ********************************************************************************************
* TaskManager
*
* DESCRIPTION : Responsible for persisting Task data and specifically the Task created for a Sales Manager 
*               when a Sales Representative submits a Street Sheet for Direct Mail export.
*
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*             				 10/14/2011			- Original Version
*
*													
*/

public with sharing class TaskManager {
	
	
	private static final String EXPORT_TASK_DESCRIPTION = ' is requesting that a Prospect List be exported for Direct Mail.';
	private static final String NORMAL_PRIORITY = 'Normal';
	private static final String NOT_STARTED_STATUS = 'Not Started';
	private static final String EXPORT_TASK_SUBJECT = 'Direct Mail Export request from ';
	private static final Integer EXPORT_REMINDER_INCREMENT = 2;
	private static final Integer EXPORT_DUE_DATE_INCREMENT = 3;
	
	
	public static void createDirectMailExportTask(User creator, String ownerID, String streetSheetID) {
		
		System.debug('TaskManager: Street Sheet ID = ' + streetSheetID);
		Task exportTask = new Task( Description = creator.Name + EXPORT_TASK_DESCRIPTION,
									Priority = NORMAL_PRIORITY,
									Status = NOT_STARTED_STATUS,
									Subject = EXPORT_TASK_SUBJECT + creator.Name,
									IsReminderSet = true,
									ReminderDateTime = System.now() + EXPORT_REMINDER_INCREMENT,
									OwnerId = ownerID,
									ActivityDate = Date.today() + EXPORT_DUE_DATE_INCREMENT,
									//CreatedBy = creator,
									WhatId = streetSheetID);
		
		insert exportTask;
		
	}

}