/************************************* MODIFICATION LOG ********************************************************************************************
* GoogleGeoCode
* 
* DESCRIPTION : Invokes the Google Geocode API using an HTTPRequest, processes the xml response, 
*               creates GoogleGeoCodeResults structure and handles any errors during processing.  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*               				10/14/2011			- Original Version
*
*													  
*													
*/

public class GoogleGeoCode {
	
	private static final String googleBaseURL = 'https://maps.googleapis.com/maps/api/geocode/';
	private static final String googleOutput = 'xml';
	private static final String googleVersion = '3';
	private static final String googleClient = 'gme-adt';
	private static final String googleChannel = 'Resale';
	
	private static Boolean useADTLicense = true;
	
	public static GoogleGeoCodeResults GeoCodeAddress(String Address)
	{
		HttpRequest req = new HttpRequest();
		req.setEndpoint(getGoogleGeocodeURL(EncodingUtil.urlEncode(Address,'UTF-8')));
     	req.setMethod('GET');	
     	System.debug('Google GeoCode Request……: ' + req);
     	Http http = new Http();
	    HTTPResponse res = http.send(req);
	    //system.debug(res.getBody());
	    return parseXMLResponse(res.getBody());
	    //system.debug(GGCAddress);
	}
	
	private static String getGoogleGeocodeURL(String Address)
	{
		String returnURL = '';
		if(useADTLicense)
		{
			String signature = getGoogleSignature(Address); 
			returnURL = googleBaseURL + googleOutput + '?' + 'client=' + googleClient + '&sensor=true' + '&channel=' + googleChannel + '&address=' + Address + '&signature=' + signature;
		} 
		else
			returnURL = googleBaseURL + googleOutput + '?address=' + Address + '&sensor=true';
		return returnURL;
	}
	
	private static String getGoogleSignature(String Address)
	{
		String BaseURL = '/maps/api/geocode/';
		String returnURL = BaseURL + googleOutput + '?' + 'client=' + googleClient + '&sensor=true' + '&channel=' + googleChannel + '&address=' + Address;
		String urlEncodedURL = EncodingUtil.urlEncode(returnURL, 'UTF-8');
		String algorithmName = 'HMacSHA1';
		//Blob decodedKey = EncodingUtil.base64Decode('rE5EevLmL7uzpwPOkxpw1RcZCFk=');
		Blob decodedKey = EncodingUtil.base64Decode( ResaleGlobalVariables__c.getInstance('GoogleGeoCodeDecodeKey').value__c );
		String decodedKeyString = decodedKey.toString();
		system.debug('DECODED::' + decodedKeyString);
		Blob mac = Crypto.generateMac(algorithmName,  Blob.valueOf(returnURL), EncodingUtil.base64Decode('rE5EevLmL7uzpwPOkxpw1RcZCFk='));
		String macUrl = EncodingUtil.base64Encode(mac);//EncodingUtil.urlEncode(EncodingUtil.base64Encode(mac), 'UTF-8');		
		string returnVal = macUrl.replace('+', '-').replace('/', '_');
		return String.valueOf(returnVal); 
	}
	
	public static GoogleGeoCodeResults parseXMLResponse(String responseBody)
	{
		GoogleGeoCodeResults result = new GoogleGeoCodeResults();
		XmlStreamReader reader = new XmlStreamReader(responseBody);	
		result.ExactMatch = 'true';	
		while(reader.hasNext())
		{
			if (reader.getEventType() == XmlTag.START_ELEMENT) {
				if(reader.getLocalName() == 'status')
				{
					reader.next();
			        if (reader.getEventType() == XmlTag.CHARACTERS) {
			           	result.Status = reader.getText();
			        }
				}
				else if (reader.getLocalName() == 'result')
				{
					reader.next();
			        if (reader.getEventType() == XmlTag.CHARACTERS) {
			           	parseResultNodes(reader, result);	           	
			        }					
				}
			}
			reader.next();
		}
		system.debug(result);
		return result;
	}
	
	private static GoogleGeoCodeResults parseResultNodes(XmlStreamReader reader, GoogleGeoCodeResults result)
	{
		while(reader.hasNext())
		{
			if (reader.getEventType() == XmlTag.START_ELEMENT) {
				if(reader.getLocalName() == 'type')
				{
					reader.next();
			        if (reader.getEventType() == XmlTag.CHARACTERS) {
			           result.AddressType = reader.getText();		           
			        }
				}
				else if(reader.getLocalName() == 'address_component')
				{
					while(reader.hasNext())
					{
						if(reader.getEventType() == XmlTag.END_ELEMENT && reader.getLocalName() == 'address_component')
							break;
						reader.next();
					}
				}
				else if (reader.getLocalName() == 'formatted_address')
				{
					reader.next();
			        if (reader.getEventType() == XmlTag.CHARACTERS) {
			           	result.FormattedAddress = reader.getText();			           	
			        }					
				}
				else if (reader.getLocalName() == 'geometry')
				{
					Boolean breakfromthiscode = false;
					while(reader.hasNext())
					{
						if(reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == 'location')
						{
							while(reader.hasNext())
							{
								if(reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == 'lat')
								{
									reader.next();
									result.AddressLat = reader.getText();
								}
								else if(reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == 'lng')
								{
									reader.next();
									result.AddressLon = reader.getText();
								}				
								if(reader.getEventType() == XmlTag.END_ELEMENT && reader.getLocalName() == 'location'){
									breakfromthiscode = true;
									break;	
								}				
								reader.next();																
							}		
						}
						if(breakfromthiscode)
							break;
						reader.next();
					}			
				}
				else if (reader.getLocalName() == 'partial_match')
				{
					system.debug('$I got here');
					reader.next();
			        if (reader.getEventType() == XmlTag.CHARACTERS) {
			           	result.ExactMatch = reader.getText() == 'true' ? 'false' : 'true';			           	
			        }	
				}								
			}
			reader.next();
		}		
		return result;
	}
	
}