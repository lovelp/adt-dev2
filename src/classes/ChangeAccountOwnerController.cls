/************************************* MODIFICATION LOG ********************************************************************************************
* ChangeAccountOwnerController
*
* DESCRIPTION : Users may initiate a change of ownership for account(s) from a list view.
*               This class supports the VisualForce page where the user selects the new owner.
*               It is a Controller Extension in the SFDC MVC architecture.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                 DATE                TICKET          REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover              10/14/2011                          - Original Version
* Siddarth Asokan           05/20/2019                          - Added ChangeOwnershipWithListOfAccounts method
* Siddarth Asokan           07/26/2019                          - Updated ChangeOwnershipWithListOfLeads method to support National
* Siddarth Asokan           08/19/2019          HRM-10846       - Updated assign to Manager method to update the child records
*/

public class ChangeAccountOwnerController {
    private class SubordinateUser implements Comparable {
        public String label;
        public string id;
        public SubordinateUser(String l, String uid){
            label = l;
            id = uid;
        }
        public Integer compareTo(Object compareTo) {
            SubordinateUser compareToEmp = (SubordinateUser)compareTo;
            if (label == compareToEmp.label){
                return 0;
            }
            if (label > compareToEmp.label){
                return 1;
            }
            return -1;
        }
    }
    
    private list<Account> selectedAccounts {get; set;}
    private map<Id, User> allAvailableSalesRepsForManager = new map<Id, User>();
    public list<Account> selectedAccountDetails {get; set;}
    public Integer NumberOfRecords {get; set;}
    public map<Boolean,List<Account>> mapAccount{get;set;}
    public static map<Id,String> accountIdOwnerMap{get;set;}
    public Boolean isAccountAssignmentAllowed{get;set;}
    //selected salesrep for assignment
    String selectedSalesRep = null;
    //getter for salesrep selected
    public String getSelectedSalesRep(){
        return selectedSalesRep;
    }
    //setter for salesrep selected
    public void setSelectedSalesRep(String ssr){ 
        this.selectedSalesRep = ssr; 
    }
    
    //Constructor
    public ChangeAccountOwnerController(ApexPages.StandardSetController controller){
        //Check if Account Assignment is allowed
        isAccountAssignmentAllowed = false;
        String allowedProfiles = label.AllowedProfilesToRunAssignments;
        for(user loggedInUser : [Select Id, Profile.Name from User Where Id =: UserInfo.getUserId()]){
            if(String.isNotBlank(allowedProfiles) && allowedProfiles.contains(loggedInUser.Profile.Name)){
                isAccountAssignmentAllowed = true;
            }
        }
        selectedAccounts = controller.getSelected();
        NumberOfRecords = selectedAccounts.size();
        if(NumberOfRecords > 0){
            BuildListOfAvailableReps();
            BuildListOfAccountsSelected();
        }
    }
    
    public Integer getListSizeForFalse(){
        return (mapAccount != null && mapAccount.size() > 0)? mapAccount.get(false).size() : 0;
    }
    
    public Integer getListSizeForTrue(){
        return (mapAccount != null && mapAccount.size() > 0)? mapAccount.get(true).size() : 0;
    }
    
    public pageReference Validate(){
        if(selectedAccounts.size() == 0){
            return new pageReference('/apex/ShowError?Error=ANS');
        }else if (ProfileHelper.isSalesRep()){
            return new pageReference('/apex/ShowError?Error=SCA');
        }
        return null;
    }
    
    private void BuildListOfAvailableReps(){
        //get the user id and the related towns that the user/manager is assigned to
        Id currentUserRoleId = UserInfo.getUserRoleId();
        List<Id> matrixedRoles = new List<Id>();
        matrixedRoles = Utilities.getMatrixedRolesForManager(currentUserRoleId);
        List<Id> allRoles = new List<Id>();
        allRoles.add(currentUserRoleId);
        if(matrixedRoles != null){
            allRoles.addAll(matrixedRoles);
        }
        //get all subordinate roles for the user
        List<UserRole> subRoles = [select id, name from UserRole where parentRoleId =: allRoles];
        List<id> allSubordinateRoleIds = new List<Id>();
        for(UserRole ur: subRoles){
            allSubordinateRoleIds.add(ur.id);
        }

        //get all salesreps for the manager
        List<User> allSubordinateUsers = [select id, name, IsActive from User where UserRoleId in : allSubordinateRoleIds];

        // check if our current user is enabled to quick see deep into the hierarchy any number of levels
        Set<Id> deepViewUsers  = Utilities.getUnlimitedHierarchyViewGroupUsers();
        if(deepViewUsers != null && !deepViewUsers.isEmpty() && deepViewUsers.contains(UserInfo.getUserId())){
            List<User> tmpL = RoleUtils.getAllSubordinates( UserInfo.getUserId() );
            if(tmpL != null && !tmpL.isEmpty()){
                allSubordinateUsers.addAll( tmpL );
            }
        }

        for(User u : allSubordinateUsers){
            allAvailableSalesRepsForManager.put(u.id, u);
        }
    }
    
    public List<SelectOption> getallSalesreps(){
        List<SelectOption> salesReps = new List<SelectOption>(); 
        SelectOption anOption;
        User u;
        String label;
        List<SubordinateUser> subUList = new List<SubordinateUser>(); 
        for(Id uid : allAvailableSalesRepsForManager.KeySet()){
            u = allAvailableSalesRepsForManager.get(uid);
            if(u.IsActive){
                subUList.add( new SubordinateUser( u.Name, u.id ) );
            }
        }
        if(subUList !=null && !subUList.isEmpty()){
            subUList.sort();
            for(SubordinateUser subU: subUList){
                salesReps.add( new SelectOption(subU.id, subU.label ) );
            }
        }
        label = 'Me (' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ')';
        anOption = new SelectOption(UserInfo.getUserId(), label);
        salesReps.add(anOption);
        return salesReps;
    }   
    
    private void BuildListOfAccountsSelected(){
        Set<id> accountIds = new Set<id>();
        selectedAccountDetails = new List<Account>(); 
        for(Account acct : selectedAccounts){
            accountIds.add(acct.id);
        }
        
        for(List<Account> allQueriedAccounts : [select id, Name, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, Owner.Name from Account where Id in : accountIds]){
            for(Account queriedAccount : allQueriedAccounts){
                selectedAccountDetails.add(queriedAccount);
            }
        }
    }
    
    public pageReference Save(){
        List<Account> updatedAccounts = new List<Account>();
        if(this.selectedSalesRep == null || this.selectedSalesRep == ''){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a salesrep to change the ownership.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        Set<String> rectypes = new Set<String>{RecordTypeDevName.COMPANY_GENERATED_APPOINTMENT, RecordTypeDevName.SELF_GENERATED_APPOINTMENT};
        
        List<Event> events = new List<Event>([
            Select Id from Event
            where WhatId IN :selectedAccountDetails
                and (StartDateTime > :DateTime.now() or EndDateTime > :DateTime.now())
                and Status__c <> :EventManager.STATUS_CANCELED
                and RecordType.DeveloperName IN :rectypes
            limit 1000
        ]);
        
        if (events.size() > 0) {
            Utilities.addError('One or more of the selected accounts have upcoming appointments.  You cannot change ownership of these accounts.');
            return null;
        }

        for(Account acct : selectedAccountDetails){
            acct.OwnerId = this.selectedSalesRep;
            acct.ManuallyAssigned__c = true;
            acct.AssignedBy__c = UserInfo.getUserId();
            acct.DateAssigned__c = DateTime.now();
            acct.PreviousOwner__c = null;
            acct.NewLead__c = true;
            acct.UnassignedLead__c = this.selectedSalesRep == UserInfo.getUserId()?true:false;
            updatedAccounts.add(acct);
        }
        update updatedAccounts;

        return new pageReference('/' + schema.Sobjecttype.Account.getKeyPrefix() );
    }
    
    // Call this method on page load
    public void onLoad(){
        if(isAccountAssignmentAllowed){
            // Only call the method if account assignment is allowed
            mapAccount = changeOwnershipWithListOfAccounts(selectedAccounts,true,true,true);
        }
    }

    // Returns a map of success accounts & failed accounts
    public static map<Boolean,list<Account>> changeOwnershipWithListOfAccounts(List<Account> accounts){
        return changeOwnershipWithListOfAccounts(accounts,false,false,false);
    }

    // Returns a map of success accounts & failed accounts
    public static map<Boolean,list<Account>> changeOwnershipWithListOfAccounts(List<Account> accounts, Boolean doUpdate){
        return changeOwnershipWithListOfAccounts(accounts,doUpdate,false,false);
    }

    // Returns a map of success accounts & failed accounts
    public static map<Boolean,list<Account>> changeOwnershipWithListOfAccounts(List<Account> accounts, Boolean doUpdate, Boolean doUpdateChildRecords, Boolean needChangedOwner){
        accountIdOwnerMap = new map<Id,String>();
        list<Account> accountList = new list<Account>();
        list<Contact> contactList = new list<Contact>();
        list<Opportunity> oppList = new list<Opportunity>();
        map<Boolean,list<Account>> returnMap = new map <Boolean,list<Account>>();
        list<Account> successAccounts = new list<Account>();
        list<Account> errorAccounts = new list<Account>();
        list<Account> sameOwnerAccounts = new list<Account>();
        // HRM-10369 National Owner Assignment
        String defaultNationalOwnerId;
        if(ResaleGlobalVariables__c.getinstance('DefaultNationalOwnerUsername') != null){
            String defaultNationalUsername = ResaleGlobalVariables__c.getinstance('DefaultNationalOwnerUsername').value__c;
            for(User nationalOwner: [Select Id from User where UserName =: defaultNationalUsername limit 1]){
                defaultNationalOwnerId = nationalOwner.Id;
            }
        }
        if(doUpdate){
            // If the method is called from the UI
            set<Id> accountIdSet = new set<Id>();
            for(Account a : accounts){
                // For all other owner assignments
                accountIdSet.add(a.id);
            }
            accountList = [SELECT Id,name,Phone,Email__c,AssignOwner__c,StandardizedAddress__c,Business_Id__c,Channel__c,Affiliation__c,RecordType.name,OwnerId,Owner.Name,postalcodeId__c,ProcessingType__c,QueriedSource__c,TelemarLeadSource__c,GenericMedia__c FROM Account WHERE Id IN: accountIdSet];
        }else{
            // If the method is called from the trigger
            map<String,String> usernameOwnerIdMap = new map<String,String>();
            set<String> usernameSet = new set<String>();
            for(Account a: accounts){
                // Get the owner username if provided at the time of insert and and put it in set
                if(String.isNotBlank(a.OwnerUsername__c)){
                    usernameSet.add(a.OwnerUsername__c);
                }
            }
            
            if(usernameSet.size() > 0){
                // Get the active owner based on the set of usernames
                for(User u: [Select Id,UserName from User where Username IN :usernameSet And IsActive = true]){
                    usernameOwnerIdMap.put(u.Username,u.Id);
                }
            }
            
            for(Account finalAccount : accounts){
                if(String.isNotBlank(finalAccount.OwnerUsername__c) && usernameOwnerIdMap.size() > 0 && usernameOwnerIdMap.containsKey(finalAccount.OwnerUsername__c)){
                    // If the Owner linked to the username is still active, use it
                    finalAccount.OwnerId = usernameOwnerIdMap.get(finalAccount.OwnerUsername__c);
                }else if(String.isNotBlank(finalAccount.Business_Id__c) && finalAccount.Business_Id__c.contains('1300') && String.isNotBlank(finalAccount.channel__c) && finalAccount.channel__c == Channels.TYPE_NATIONAL && String.isNotBlank(defaultNationalOwnerId) && finalAccount.OwnerId != defaultNationalOwnerId){
                    // HRM-10369 Assign the National Owner
                    // Assigning the default National Owner here since we do not need to run the below code
                    finalAccount.OwnerId = defaultNationalOwnerId;
                }else{
                    // If Owner usename is not provided or if the user is deactivated
                    accountList.add(finalAccount);
                }
            }
        }
        if(accountList.size() > 0){
            map<Id,String> newOwnerIdMap = new map<Id,String>();
            map<Id,String> successOwnerIdMap = new map<Id,String>();
            list<Id> postalCodeIds = new list<Id>();
            map<String, String> pcOwners = new map<String, String>();
            set<String> allTowns = new set<String>();
            map<Id, String> pcTownsMap = new map<Id, String>();
            map<String, String> mtOwners = new map<String, String>();
            list<Id> ownerIdList = new list<Id> ();
            map <Id,String> ownerIdUSAAMap = new map <Id,String>();
            String globalUnassingnedUser = Utilities.getGlobalUnassignedUser();
            String devconInsideSalesUser = Utilities.getDevconPhoneSaleUser();
            String globalNSCAdmin = Utilities.getGlobalNSCUser(); // User for leadsharing for nscaccount
            Id resaleRecTypeId = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account').Id;
            //get all postal codes from accounts + postal code and channels
            for(Account acct : accountList){
                if(acct.postalcodeId__c != null){
                    postalCodeIds.add(acct.postalcodeId__c);
                }
            }
            if(postalCodeIds.size() > 0){
                //get all towns related to the postal codes
                for(Postal_Codes__c pcs : [Select Id, TownId__c from Postal_Codes__c where Id in : postalCodeIds]){
                    allTowns.add(pcs.TownId__c);
                    pcTownsMap.put(pcs.id, pcs.TownId__c);
                }
                //check of the postal codes are assigned
                for(TerritoryAssignment__c ta : [Select Id, PostalCodeId__c, OwnerId, TerritoryType__c from TerritoryAssignment__c where PostalCodeId__c in : postalCodeIds AND Owner.isActive = True]){
                    pcOwners.put(ta.PostalCodeId__c + ';' + ta.TerritoryType__c, ta.OwnerId);
                    ownerIdList.add(ta.OwnerId);
                }
                // Manager Towns
                for(ManagerTown__c mt : [Select id, ManagerId__c, TownId__c, BusinessId__c, Type__c from ManagerTown__c where TownId__c in : allTowns]){
                    for(String mtts : mt.Type__c.split(';')){
                        mtOwners.put(mt.TownId__c + ';' + mt.BusinessId__c + ';' + mtts, mt.ManagerId__c);
                    }
                }
                // USAA
                for (user owners : [Select Id, qualification__c from user where Id in : ownerIdList And IsActive = true]){
                    ownerIdUSAAMap.put(owners.Id,owners.qualification__c);
                }
            }
            
            for(Account acct : accountList){
                system.debug('Account Name: '+acct.Name);
                String oldAccountOwner = acct.OwnerId;
                //when accounts are phone sales (processing type = NSC), then account channel needs to be transformed to a phone sale channel
                String accountChannel = acct.Channel__c;
                String bussId = '';
                if(String.isNotBlank(acct.Business_Id__c)){
                    bussId = Channels.getFormatedBusinessId(acct.Business_Id__c,Channels.BIZID_OUTPUT.NUM);
                }      
                System.debug('Account Bussiness ID: '+bussId);
                if(String.isNotBlank(acct.Business_Id__c) && acct.Business_Id__c.contains('1300') &&  String.isNotBlank(acct.channel__c) && acct.channel__c == Channels.TYPE_NATIONAL && String.isNotBlank(defaultNationalOwnerId)){
                    // HRM-10369 Assign the National Owner
                    acct.OwnerId = defaultNationalOwnerId;
                    acct.DateAssigned__c = DateTime.now();
                    acct.UnassignedLead__c = true;
                    acct.NewLead__c = true; 
                }else if(acct.ProcessingType__c == IntegrationConstants.PROCESSING_TYPE_NSC){
                    accountChannel = Channels.getPhoneSaleChannel(acct.Channel__c, true);
                    if(pcOwners.get(acct.PostalCodeId__c + ';' + accountChannel) != null){
                        String ownerId = pcOwners.get(acct.PostalCodeId__c + ';' + accountChannel);
                        acct.DateAssigned__c = DateTime.now();
                        acct.UnassignedLead__c = false;
                        acct.NewLead__c = true;
                    
                        if(acct.Affiliation__c == IntegrationConstants.AFFILIATION_USAA && ownerIdUSAAMap.size() > 0 && ownerIdUSAAMap.get(ownerId) == IntegrationConstants.AFFILIATION_USAA){
                            acct.OwnerId = ownerId;
                            system.debug('USAA Owner: '+acct.OwnerId);
                        }else if ((acct.QueriedSource__c!=null && acct.QueriedSource__c.contains('DEVCON')) ||
                                (acct.TelemarLeadSource__c!=null && acct.TelemarLeadSource__c.contains('DEVCON')) ||
                                (acct.GenericMedia__c!=null && acct.GenericMedia__c.contains('DEVCON'))){
                            acct.OwnerId = devconInsideSalesUser;
                            System.debug('Devcon Owner1: '+acct.OwnerId);
                        }else if (mtOwners.get(pcTownsMap.get(acct.PostalCodeId__c) + ';' + bussId + ';' + acct.Channel__c) != null){
                            acct.OwnerId = mtOwners.get(pcTownsMap.get(acct.PostalCodeId__c) + ';' + bussId + ';' + acct.Channel__c);
                            System.debug('mtOwners1: '+acct.OwnerId);
                        }else{
                            acct.OwnerId = globalUnassingnedUser;
                            System.debug('Global Unassigned User1: '+acct.OwnerId);
                        }
                    }else if ((acct.QueriedSource__c != null && acct.QueriedSource__c.contains('DEVCON')) ||
                                (acct.TelemarLeadSource__c != null && acct.TelemarLeadSource__c.contains('DEVCON')) ||
                                (acct.GenericMedia__c != null && acct.GenericMedia__c.contains('DEVCON'))){
                        acct.OwnerId = devconInsideSalesUser;
                        System.debug('Devcon Owner2: '+acct.OwnerId);
                    }else if (mtOwners.get(pcTownsMap.get(acct.PostalCodeId__c) + ';' + bussId + ';' + acct.Channel__c) != null){
                        acct.OwnerId = mtOwners.get(pcTownsMap.get(acct.PostalCodeId__c) + ';' + bussId + ';' + acct.Channel__c);
                        acct.DateAssigned__c = DateTime.now();
                        acct.UnassignedLead__c = true;
                        acct.NewLead__c = true;  
                        System.debug('mtOwners2: '+acct.OwnerId);
                        System.debug('PC TownMap2: '+pcTownsMap.get(acct.PostalCodeId__c) + 'Bussiness ID;' + bussId + 'Channel;' + acct.Channel__c);
                    }else{
                        acct.OwnerId = globalUnassingnedUser;
                        acct.DateAssigned__c = DateTime.now();
                        acct.UnassignedLead__c = true;
                        acct.NewLead__c = true;
                        System.debug('Global Unassigned User2: '+acct.OwnerId);
                    }
                }else if((String.isNotBlank(acct.Business_Id__c) && acct.Business_Id__c.contains('1300')) || (acct.ProcessingType__c == IntegrationConstants.PROCESSING_TYPE_NSCLeadSharing && acct.Lead_Origin__c == 'Matrix Rehash')){                
                    system.debug('1300 Change account logic');
                    if(pcOwners.get(acct.PostalCodeId__c + ';' + acct.Channel__c) != null){
                        acct.OwnerId = pcOwners.get(acct.PostalCodeId__c + ';' + acct.Channel__c);
                        acct.Rep_User__c = pcOwners.get(acct.PostalCodeId__c + ';' + acct.Channel__c);
                        acct.UnassignedLead__c = false;
                        System.debug('pcOwners3: '+acct.OwnerId);
                    }else if (mtOwners.get(pcTownsMap.get(acct.PostalCodeId__c) + ';' + bussId + ';' + acct.Channel__c) != null){
                        acct.OwnerId = mtOwners.get(pcTownsMap.get(acct.PostalCodeId__c) + ';' + bussId + ';' + acct.Channel__c);
                        acct.UnassignedLead__c = true;
                        acct.Contact_Owner__c = true;
                        System.debug('mtOwners3: '+acct.OwnerId);
                    }else{
                        acct.OwnerId = String.isNotBlank(acct.Business_Id__c) && acct.Business_Id__c.contains('1300')? globalUnassingnedUser : globalNSCAdmin;
                        acct.UnassignedLead__c = true;
                        System.debug('globalNSCAdmin User3: '+acct.OwnerId);
                    }
                    acct.DateAssigned__c = DateTime.now();
                    acct.NewLead__c = true;
                }else if(acct.RecordTypeId == resaleRecTypeId){
                    if(pcOwners.get(acct.PostalCodeId__c + ';' + acct.Channel__c) != null){
                        acct.OwnerId = pcOwners.get(acct.PostalCodeId__c + ';' + acct.Channel__c);
                        acct.UnassignedLead__c = false;
                        System.debug('pcOwners4: '+acct.OwnerId);
                    }else if (mtOwners.get(pcTownsMap.get(acct.PostalCodeId__c) + ';' + bussId + ';' + acct.Channel__c) != null){
                        acct.OwnerId = mtOwners.get(pcTownsMap.get(acct.PostalCodeId__c) + ';' + bussId + ';' + acct.Channel__c);
                        acct.UnassignedLead__c = true;
                        System.debug('mtOwners4: '+acct.OwnerId);
                    }else{
                        acct.OwnerId = globalUnassingnedUser;
                        acct.UnassignedLead__c = true;
                        System.debug('globalUnassingnedUser4: '+acct.OwnerId);
                    }
                    acct.DateAssigned__c = DateTime.now();
                    acct.NewLead__c = true;
                }
                newOwnerIdMap.put(acct.Id,acct.OwnerId);
                // Create a map of success and failures to display in UI
                if(acct.OwnerId == globalUnassingnedUser){
                    // If owner is global admin error it out and not do a update if called from UI
                    errorAccounts.add(acct);
                }else if(acct.OwnerId == oldAccountOwner){
                    // Add to the final map to show in UI but not do an update since owner is the same
                    sameOwnerAccounts.add(acct);
                }else{
                    successOwnerIdMap.put(acct.Id,acct.OwnerId);
                    successAccounts.add(acct);
                }
                // Reset values after assignment
                acct.AssignOwner__c = false;
                acct.OwnerUsername__c = null;
            }
            
            if(doUpdate){
                // Only process the below for manual updates
                if(successAccounts.size() > 0){
                    // Update the success records
                    database.update(successAccounts,false);
                }
                if(needChangedOwner && newOwnerIdMap.size() > 0){
                    //Adding New Owner in a map to display in UI
                    map<Id,String> ownerIdNameMap = new map<Id,String>();
                    for(User u: [Select Id, name From user Where Id IN: newOwnerIdMap.values()]){
                        ownerIdNameMap.put(u.Id,u.Name);
                    }
                    for(String accId: newOwnerIdMap.keySet()){
                        accountIdOwnerMap.put(accId,ownerIdNameMap.get(newOwnerIdMap.get(accId)));
                    }
                }
            }

            if(doUpdateChildRecords && successOwnerIdMap != null && successOwnerIdMap.size() > 0){
                // HRM-10846 - Update child contact records
                list<contact> contactsToUpdate = new list<contact>();
                for(Contact c : [Select Id,OwnerId,AccountId from contact where AccountId IN: successOwnerIdMap.keyset()]){
                    c.OwnerId = successOwnerIdMap.get(c.AccountId);
                    contactsToUpdate.add(c);
                }
                if(contactsToUpdate.size() > 0){
                    database.update(contactsToUpdate,false);
                }
                // HRM-10846 - Update child opportunity records
                list<Opportunity> oppToUpdate = new list<Opportunity>();
                for(Opportunity o : [Select Id,OwnerId,AccountId from Opportunity where AccountId IN: successOwnerIdMap.keyset()]){
                    o.OwnerId = successOwnerIdMap.get(o.AccountId);
                    oppToUpdate.add(o);
                }
                if(oppToUpdate.size() > 0){
                    database.update(oppToUpdate,false);
                }
            }
        }
        // Before returning the final map add the same owner leads to the success list
        successAccounts.addAll(sameOwnerAccounts);
        returnMap.put(false,errorAccounts);
        returnMap.put(true,successAccounts);
        system.debug('Final Map: '+ returnMap);
        return returnMap;
    }
}