global class CTGetSFUserName {

    webService static String getSFUserName() {
    	String uName = '';    	
        User u = [select Id, username, FederationIdentifier from User where Id = :UserInfo.getUserId()];
        System.debug('User Id: ' + UserInfo.getUserId() + '; UserName: ' + u.Username + '; FederationIdentifier: ' + u.FederationIdentifier);
        if( !String.isBlank(u.FederationIdentifier) ){
        	uName = u.FederationIdentifier.split('@')[0].toLowerCase();
        }
        return uName;
    }
}