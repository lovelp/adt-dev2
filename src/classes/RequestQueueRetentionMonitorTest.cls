/**
 Description- This test class used for RequestQueueRetentionMonitor.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class RequestQueueRetentionMonitorTest{

    public static void createCustomSettingData ()
    {   
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.MMBSearchUsername__c = 'salesforceclup';
        is.MMBSearchPassword__c = 'abcd1234';
        is.Equifax_EndPoint__c = 'https://dev2.api.adt.com/adtfico';
        is.Equifax_Username__c = 'ibmcpqoms';
        is.Equifax_Password__c = 'abcd1234';
        is.MMBSearchCalloutTimeout__c = 60000;
        is.MMBSearchEndpoint__c = 'https://dev.api.adt.com/adtclup';
        insert is;
    }
        
    static testMethod void executeMethodTest() {
        
        createCustomSettingData();
        
        RequestQueueRetentionMonitor rm= new RequestQueueRetentionMonitor();
        
        String chron = '0 0 23 * * ?';
        
        Test.startTest();
        System.schedule('Test Schedule', chron, rm);
        Test.stopTest();
        system.assert(true); 
    }
}