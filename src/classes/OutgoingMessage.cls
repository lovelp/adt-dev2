/************************************* MODIFICATION LOG ********************************************************************************************
* OutgoingMessage
*
* DESCRIPTION : Abstract parent class that establishes similar behavior for web services calls for the Telemar integration.
*               subclass will provide overrides for the creation of the web service request message and the processing of 
*               the web service response message. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					1/19/2012				- Original Version
*
*											
*/

public with sharing abstract class OutgoingMessage {
	
	public String Status {get;set;}
	public String ServiceMessage {get;set;}
	public String TransactionType {get;set;}
	public String ServiceName {get;set;}
	public String ServiceTransactionType
	{
		get {
			return this.ServiceName + this.TransactionType;
		}
		set;
	}
		
	public String MessageID {get {return mid;} }
	public String MessageStatus {get {return mstatus;} }
	public String Error {get {return err;} }
	public String TelemarScheduleID {get {return sid;} }
	public String EventID {get; set;}
	
	public static final Integer ADDRESS_LINE_SIZE = 30;
	
	protected String mid;
	protected String mstatus;
	protected String err;
	protected String sid;

	
	public OutgoingMessage() {}
	
	public void process()
	{
		processResponse();
	}
	
	public abstract String createRequest();
	
	public abstract void processResponse();
	
	protected String getMessageId(String uid) {
		Long timeAsLong = system.now().getTime();
		mid = IntegrationConstants.MESSAGE_ID_PREFIX + timeAsLong + '-' + uid;
		return mid;
	}
	
	protected String[] getAddressLines(String siteStreet) {
		String[] alines = new String[4];
		
		if (siteStreet == null || siteStreet.length() == 0) return alines;
		
		for (Integer i=0, j=0;
			 i<ADDRESS_LINE_SIZE*4 && i<siteStreet.length();
			 i += ADDRESS_LINE_SIZE) {
			
			// in case site street is less than 120 chars
			Integer strlen = ADDRESS_LINE_SIZE;
			if ( i + ADDRESS_LINE_SIZE >= siteStreet.length()) {
				strlen = siteStreet.length() - i;
			}
			alines[j] = TextUtilities.removeDiacriticalMarks(siteStreet.substring(i, i+strlen));
			j++;
		}
		return alines;
	}
	
	protected String truncatePostalCode(String postalCode, String postalCodeAddOn) {
		String pc = postalCode + (postalCodeAddOn == null ? '' : postalCodeAddOn);
		return truncateString(pc,9);
	}
	
	protected String truncatePhone(String phone) {
		return truncateString(phone, 17);
	}
	
	protected String truncateBusinessName(String businessName) {
		return truncateString(businessName, 30);
	}
	
	protected String truncateLastName(String lastName) {
		return truncateString(lastName, 16);
	}
	
	protected String truncateFirstName(String firstName) {
		return truncateString(firstName, 10);
	}
	
	protected String truncateSiteCity(String siteCity) {
		return truncateString(siteCity, 24);
	}
	
	protected String truncateTextArea(String aTextArea, Integer maxSize) 
	{
		aTextArea = aTextArea == null ? '' : aTextArea.replaceAll('[\n\r]+',' ');
		return truncateString(aTextArea, maxSize);
	}
	
	protected String truncateString(String str, Integer maxlength) {
		if (str == null) str = '';
		if (str.length() > maxlength) {
			return str.substring(0, maxlength);
		}
		return TextUtilities.removeDiacriticalMarks(str);
	}
	
}