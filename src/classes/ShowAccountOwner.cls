/************************************* MODIFICATION LOG ********************************************************************************************
* ShowAccountOwner
*
* DESCRIPTION : Supports the display of the owner of an account
*               It is a Controller Extension in the SFDC MVC architecture.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2011			- Original Version
*
*													
*/

public with sharing class ShowAccountOwner {
	
	public string OwnerName {get; set;}

	public ShowAccountOwner(ApexPages.StandardController controller)
	{
		Id accountid = controller.getId();
		Account acctInfo = [select ownerid from account where id = : accountId limit 1];
		
		User owner = [select name from User where id = : acctInfo.OwnerId Limit 1];
		OwnerName = owner.Name;
	}	
	
}