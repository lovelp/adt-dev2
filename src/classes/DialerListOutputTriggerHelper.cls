public class DialerListOutputTriggerHelper{
    public static void processbeforeInsert(List<Dialer_List_Output_Control__c > newDialerList){
        for( Dialer_List_Output_Control__c dloc:newDialerList ){
            dloc.File_Output_Record_Layout__c = Label.DialerListOutputRecordLayout;
        }
    }
}