global class CustomIterableClass implements Iterator<SObject>{ 
   
   public List<SObject> sobjs {get; set;} 
   Integer i {get; set;} 
    public CustomIterableClass(){ 
        try{
            sobjs = new List<SObject>();
            for(Archival_Data__c aData : [Select Id,Object_API_Name__c,Criteria__c from Archival_Data__c where Active__c = true limit 25]){
                //String dateStr = Datetime.now().addDays(-Integer.valueOf(aData.Date_Range__c)).format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');
                //String query = 'Select Id from '+aData.Object_API_Name__c+' Where CreatedDate > '+dateStr;
                String query = 'Select Id from '+aData.Object_API_Name__c+' Where '+aData.Criteria__c+' LIMIT 49999';
                List<Sobject> sobj = database.query(query);
                sobjs.addall(sobj);
            }
            System.debug('The sobjs is'+sobjs);
        }
        catch(exception e){
            ADTApplicationMonitor.log(e,null);
        }
        i = 0;
   }
   
   global boolean hasNext(){
       if(i >= sobjs.size())
           return false;
       else
           return true;
   }
    
   global SObject next(){
       if(i == 6){ i++; return null;}
       i=i+1;
       return sobjs[i-1];
   }
}