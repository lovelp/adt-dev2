/************************************* MODIFICATION LOG ********************************************************************************************
 *
 * DESCRIPTION : Controller for the credit score page
 *
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                TICKET         REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Magdiel Herrera               03/22/2016                         - Original Version
 * Siddarth Asokan               12/09/2016                         - Added the GetConsumerScoreInformation
 * Siddarth Asokan               06/27/2017                         - Populated JSON for field sales after credit check HRM 4893
 * Snehal Choudhary              07/06/2017          HRM 5400       - Added by TCS 
 * Srinivas Yarramsetti          12/26/2018                         - Added by Srini for soft and hard credit request.
 * Nishanth Mandala              08/21/2019          HRM 10862      - Handling partial Equifax Response
 * Siddarth Asokan               10/10/2019          HRM-10880      - FlexFi - Mizuho: Field and Phone disclosures
 * Abhinav Pandey                11/03/2019          HRM-10881      - Flex Fi  Changes
 */
 
public without sharing class NSCCreditScoreCheckController {
    
    public class NSCCreditScoreCheckControllerException extends Exception {}
    public enum CREDIT_CONDITION { DEFAULT_CODE, ON_FAILURE }
    
    public Account a {get;set;}
    public String FirstName {get;set;}
    public String LastName {get;set;}
    public String agentType {get;set;} 
    public String requireSelectConfirmMsg {get;set;}
    public Equifax_Conditions__c EquifaxCondition {get;set;}
    public String EquifaxConditionJSON {
        get{
            String resVal = '';
            if( EquifaxCondition != null ){
                resVal = JSON.serialize( EquifaxCondition );
            }
            return resVal;
        }
    }
    public Boolean renderMainFrm {
        get {
            return renderMainFrm && CreditCheckAttempts > 0;
        }
        set;
    }
    public Boolean tncSwitch {get;set;}
    public Boolean isNoHit {get;set;}
    public Boolean isCreditCheckError {get;set;}
    public Integer isCreditCheckErrorCount {get;set;}
    public Boolean isAcceptConditionSuccess {get;set;}
    public Integer CreditCheckAttempts {get;set;}
    public ADTFICOAPI.doConsumerScoreInfoResp ConsumerCheckResp {get;set;} 
    
    public Boolean renderPhoneView {
        get{
            system.debug('Phone Render view');
            return String.isNotBlank( agentType ) && agentType == 'PHONE';
        }
    }
    public Boolean renderFieldView {
        get{
            return String.isNotBlank( agentType ) && agentType == 'FIELD';
        }
    }
    
    // Credit Information
    private String Equifax_Credit_Score {get;set;}
    private String EquifaxApprovalType {get;set;}
    private String EquifaxRiskGrade {get;set;}
    private String EquifaxRiskGradeDisplayValue {get;set;}
    private Boolean blockCreditCheck {get;set;}
    
    // SSN      
    public String ssnNumber3 {get;set;}
    private String ssn3 {get;set;} // persist the form entry on this value in case of provided on the first request
    public String ssnNumber2 {get;set;}
    private String ssn2 {get;set;} 
    public String ssnNumber4 {get;set;}
    
    // DOB
    public String dobMonth {get;set;}
    public String dobDayOfMonth {get;set;}
    public String dobYear {get;set;}
    
    // Address
    public String ccStreet {get;set;}
    public String ccStreet2 {get;set;}
    public String ccCity {get;set;}
    public String ccState {get;set;}
    public String ccPostalCode {get;set;}
    public String ccCounty {get;set;}
    
    // Previous Address
    public String ccPrevStreet {get;set;}
    public String ccPrevStreet2 {get;set;}
    public String ccPrevCity {get;set;}
    public String ccPrevState {get;set;}
    public String ccPrevPostalCode {get;set;}
    public String ccPrevCounty {get;set;}
    //HRM-8447 - Added for soft to hard credit check
    public Boolean doSoftCheck {get;set;}
    public Boolean allowPageMessage;

    // Terms & Conditions HRM-10880
    public String termsAndConditions {get; set;}
    public Boolean tncAccepted { get; set;}
    public Boolean showTnCCheckBox { get; set;}
    public Boolean showTnCOnce {get; set;}

    //Constructor to invoke from other class.
    public NSCCreditScoreCheckController(String accID){
        allowPageMessage = false;
        this.CreditCheckAttempts = Integer.valueOf( Equifax__c.getinstance('CreditCheckAttempts').value__c );
        this.isCreditCheckError = false;
        this.isCreditCheckErrorCount = 0;
        this.renderMainFrm = true;
        this.isAcceptConditionSuccess = false;
        this.isNoHit = false;
        this.Equifax_Credit_Score = '0';
        this.ssnNumber3 = '';
        this.ssn3 = '';
        this.ssnNumber2 = '';
        this.ssn2 = '';
        this.ssnNumber4 = '';
        this.ccPrevStreet = '';
        this.ccPrevStreet2 = '';
        this.ccPrevCity = '';
        this.ccPrevState = '';
        this.ccPrevPostalCode = '';
        this.ccPrevCounty = '';
        this.doSoftCheck = false;
        
        a = [SELECT Name, FirstName__c, LastName__c, Email__c,DOB_encrypted__c, Customer_Refused_to_provide_DOB__c, Channel__c, Business_Id__c, 
             TelemarAccountNumber__c, Equifax_Last_Check_DateTime__c, Equifax_Credit_Score__c, EquifaxApprovalType__c, EquifaxRiskGrade__c, Equifax_Last_Four__c, 
             Equifax_Full_SSN_Used__c, AddressId__c, SiteStreet__c, SiteStreet2__c, SiteCity__c, SiteState__c, AddressID__r.State__c, SitePostalCode__c, SiteCounty__c, 
             ResaleTownNumber__c, Profile_YearsInResidence__c, Profile_BuildingType__c, Profile_RentOwn__c, TelemarLeadSource__c, QueriedSource__c, GenericMedia__c, MMBOrderType__c, 
             PostalCodeID__c, PreviousCity__c, PreviousState__c, PreviousStreet__c, PreviousStreet2__c, PreviousZip__c
             FROM Account WHERE Id = :accID];
        // Check if we can run another credit check on this customer
        Integer minDaysAllowed = Integer.valueOf( Equifax__c.getinstance('Days to allow additional check').value__c );
        FirstName = a.FirstName__c;
        LastName = a.LastName__c;
        // current address
        ccStreet = a.SiteStreet__c;
        ccStreet2 = a.SiteStreet2__c;
        ccCity = a.SiteCity__c;
        ccState = a.SiteState__c;
        ccPostalCode = a.SitePostalCode__c;
        ccCounty = a.SiteCounty__c;
        ssnNumber4 = a.Equifax_Last_Four__c;
        if(String.isNotBlank(a.PreviousStreet__c)){
            ccPrevStreet = String.isNotBlank(a.PreviousStreet__c)?a.PreviousStreet__c:'';
            ccPrevStreet2 = String.isNotBlank(a.PreviousStreet2__c)?a.PreviousStreet2__c:'';
            ccPrevCity = String.isNotBlank(a.PreviousCity__c)?a.PreviousCity__c:'';
            ccPrevState = String.isNotBlank(a.PreviousState__c)?a.PreviousState__c:'';
            ccPrevPostalCode = String.isNotBlank(a.PreviousZip__c)?a.PreviousZip__c:'';
        }
        if(a.DOB_encrypted__c != null){
            // Added by TCS -- HRM 5400
            String[] dobEnclist = a.DOB_encrypted__c.split('/') ;
            if(dobEnclist.size() == 3){
                dobDayOfMonth = String.valueOf(dobEnclist[1]);
                dobYear = String.valueOf(dobEnclist[2]);
                Integer tempDobMonth = Integer.valueOf(dobEnclist[0]);
                dobMonth = String.valueOf(tempDobMonth);
            }
        }
        system.debug('Date: '+ dobDayOfMonth + ' Month: '+ dobMonth + ' Year: '+ dobYear);
    }
    
    public NSCCreditScoreCheckController(){
        String accID = ApexPages.currentPage().getParameters().get('aId');
        agentType = ApexPages.currentPage().getParameters().get('agentType');
        allowPageMessage = true;
        showTnCOnce = true;
        showTnCCheckBox = false;
        tncAccepted = false;
        this.requireSelectConfirmMsg = 'We\'re searching for conditions depending on your credit information.';
        this.CreditCheckAttempts = Integer.valueOf( Equifax__c.getinstance('CreditCheckAttempts').value__c );
        this.isCreditCheckError = false;
        this.isCreditCheckErrorCount = 0;
        this.renderMainFrm = true;
        this.isAcceptConditionSuccess = false;
        this.isNoHit = false;
        this.Equifax_Credit_Score = '0';
        this.ssnNumber3 = '';
        this.ssn3 = '';
        this.ssnNumber2 = '';
        this.ssn2 = '';
        this.ssnNumber4 = '';
        this.ccPrevStreet = '';
        this.ccPrevStreet2 = '';
        this.ccPrevCity = '';
        this.ccPrevState = '';
        this.ccPrevPostalCode = '';
        this.ccPrevCounty = '';
        this.doSoftCheck = false;
        
        if(String.isNotBlank(accID) ){
            a = [SELECT Name, FirstName__c, LastName__c, Email__c,DOB_encrypted__c, Customer_Refused_to_provide_DOB__c, Channel__c, Business_Id__c, TelemarAccountNumber__c, 
                 Equifax_Last_Check_DateTime__c, Equifax_Credit_Score__c, EquifaxApprovalType__c, EquifaxRiskGrade__c, Equifax_Last_Four__c, Equifax_Full_SSN_Used__c, AddressId__c, 
                 SiteStreet__c, SiteStreet2__c, SiteCity__c, SiteState__c, AddressID__r.State__c, SitePostalCode__c, SiteCounty__c, ResaleTownNumber__c, Profile_YearsInResidence__c, 
                 Profile_BuildingType__c, Profile_RentOwn__c, TelemarLeadSource__c, QueriedSource__c, GenericMedia__c, MMBOrderType__c, PostalCodeID__c, PreviousCity__c, 
                 PreviousState__c, PreviousStreet__c, PreviousStreet2__c, PreviousZip__c
                 FROM Account WHERE Id = :accID];
            // Check if we can run another credit check on this customer
            Integer minDaysAllowed = Integer.valueOf( Equifax__c.getinstance('Days to allow additional check').value__c );
            FirstName = a.FirstName__c;
            LastName = a.LastName__c;
            
            // current address
            ccStreet = a.SiteStreet__c;
            ccStreet2 = a.SiteStreet2__c;
            ccCity = a.SiteCity__c;
            ccState = a.SiteState__c;
            ccPostalCode = a.SitePostalCode__c;
            ccCounty = a.SiteCounty__c;
            
            //Error handling in UI
            String errorMsg = '';
            if(String.isBlank(a.FirstName__c)){
                errorMsg += 'First name is required<br/>';
            }
            if(String.isBlank(a.TelemarAccountNumber__c)){
                errorMsg += 'Telemar number is required<br/>';
            }
            if(String.isBlank(a.SiteStreet__c) || String.isBlank(a.SiteCity__c) || String.isBlank(a.SiteState__c) || String.isBlank(a.SitePostalCode__c)){
                errorMsg += 'Some address fields are missing, Street, City, State, Postal code are required to run credit check';
            }
            
            // If any one of the above fields are missing show error in UI
            if(allowPageMessage && String.isNotBlank(errorMsg)){
                this.renderMainFrm = false;
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fix the below errors before proceeding to credit check.<br/>' + errorMsg));
            }
            // Added by TCS -- HRM 5400
            if(a.DOB_encrypted__c != null){
                String[] dobEnclist = a.DOB_encrypted__c.split('/') ;
                if(dobEnclist.size() == 3){
                    dobDayOfMonth = String.valueOf(dobEnclist[1]);
                    dobYear = String.valueOf(dobEnclist[2]);
                    Integer tempDobMonth = Integer.valueOf(dobEnclist[0]);
                    dobMonth = String.valueOf(tempDobMonth);
                }
            }
            
            //Control the soft check for APM Postal codes HRM-10880
            //HRM-10881 Flex Fi passing emoty string for apm determination based on quote order type
            if(String.isBlank(errorMsg) && Utilities.checkForAlternatePricingModel(a,'')){
                doSoftCheck = true;
                //HRM-10880 Flex fi Disclosures
                tncSwitch = FlexFiConfig__c.getinstance('CFG Disabled') != null && String.isNotBlank(FlexFiConfig__c.getinstance('CFG Disabled').value__c) && FlexFiConfig__c.getinstance('CFG Disabled').value__c.containsIgnoreCase('true');
                if(tncSwitch){
                    String staticResTnc = '';
                    // To query the name of the static resource for terms and conditions
                    if(String.isNotBlank(agentType) && agentType.equalsIgnoreCase('FIELD')){
                        if(String.isNotBlank(a.Email__c)){
                            // Show terms and conditions accept check box for field sales only if the email is not blank
                            showTnCCheckBox = true;
                        }
                        staticResTnc = 'CCFieldEConsent';
                    }else{
                        staticResTnc = 'CCPhoneEConsent';
                    }
                    StaticResource sr = [select Name,Body from StaticResource where Name =: staticResTnc];
                    if(sr != null){
                        // If we find the static resource
                        termsAndConditions = sr.Body.toString();
                        if(String.isNotBlank(agentType) && agentType.equalsIgnoreCase('FIELD') && String.isNotBlank(termsAndConditions) && termsAndConditions.contains('{EMAIL}')){
                            // Find and replace the account email in the disclosure
                            if(String.isNotBlank(a.Email__c)){
                                // Populate the account email in the disclosure
                                termsAndConditions =  termsAndConditions.replace('{EMAIL}',a.Email__c);
                            }else{
                                // Remove the email paragraph
                                termsAndConditions =  termsAndConditions.replace(termsAndConditions.substringBetween('{EMAILPARA}'),'');
                            }
                            termsAndConditions =  termsAndConditions.replace('{EMAILPARA}','');
                        }
                    }
                }
            }
        }else{
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Could not locate account id.' ));
        }
    }
    
    /**
     *  Action used from the page to submit a credit check
     *  @method GetConsumerScoreInformation
     */
    public PageReference GetConsumerScoreInformation(){
        try{
            system.debug('Final Date: '+ dobDayOfMonth + ' Month: '+ dobMonth + ' Year: '+ dobYear);
            // Count attempt of Credit Score
            this.CreditCheckAttempts -= 1;            
            this.isCreditCheckError = false;
            ADTFICOGateway.CCInfo cInf = new ADTFICOGateway.CCInfo();
            // Current Addr
            cInf.address1 = ccStreet;
            cInf.address2 = (String.isBlank( ccStreet2 ))?'':ccStreet2;
            cInf.city = ccCity;
            cInf.state = ccState;
            cInf.zip = ccPostalCode;
            // Previous Addr
            cInf.prev_address1 = ccPrevStreet;
            cInf.prev_address2 = ccPrevStreet2;
            cInf.prev_city = ccPrevCity;
            cInf.prev_state = ccPrevState;
            cInf.prev_zip = ccPrevPostalCode;
            if(String.isNotBlank(ssn2) && String.isNotBlank(ssn3) ){
                cInf.ssnNo = ssn3 + ssn2 + ssnNumber4;
            }
            cInf.ssn4No = ssnNumber4;
            cInf.dob = Date.newInstance( Integer.valueOf( dobYear ), Integer.valueOf( dobMonth ), Integer.valueOf( dobDayOfMonth ));
            if(doSoftCheck){
                cInf.organizationCode = 'adtprequal';
            }
            ADTFICOGateway ficoScoringSrv = new ADTFICOGateway(a, cInf);
            // Credit Check
            ConsumerCheckResp = ficoScoringSrv.ConsumerScoreInfo();
            // Query the latest equifax log and populate the eConsent Information
            showTnCOnce = false;
            if(showTnCCheckBox){
                // Only update the equifax log Econsent values if the checkbox was presented
                list<Equifax_Log__c> eLogList = [Select Id, AccountID__c From Equifax_Log__c Where AccountID__c =: a.Id And CreatedDate = TODAY Order By CreatedDate Desc Limit 1];
                if(eLogList != null && eLogList.size() > 0){
                    Equifax_Log__c eLog = eLogList[0];
                    eLog.IsEConsentAccepted__c = tncAccepted;
                    eLog.EConsentUpdatedDate__c = System.Now();
                    eLog.EConsentUpdatedBy__c = UserInfo.getUserId();
                    update eLog;
                }
            }
            
            if(ConsumerCheckResp != null){
                String noHitRiskGrade = '';
                String noHitRiskGradeDisplayValue = '';
                String noHitApprovalType = '';
                this.isNoHit = false;
                this.Equifax_Credit_Score = ConsumerCheckResp.ccIcCreditScore;
                if(String.isNotBlank(ConsumerCheckResp.ccIcHitCode)){ 
                    for(Equifax_Mapping__c eMap: [Select HitCode__c, HitCodeResponse__c, NoHit__c, RiskGrade__c, RiskGradeDisplayValue__c, ApprovalType__c, CreditScoreStartValue__c, CreditScoreEndValue__c from Equifax_Mapping__c Where HitCode__c =: ConsumerCheckResp.ccIcHitCode]){
                        if(ConsumerCheckResp.ccIcHitCode == '1' || ConsumerCheckResp.ccIcHitCode == '6'){
                            if((String.isNotBlank(ConsumerCheckResp.ccIcCreditScore) && String.isNotBlank(eMap.CreditScoreStartValue__c) && String.isNotBlank(eMap.CreditScoreEndValue__c) && Integer.valueOf(ConsumerCheckResp.ccIcCreditScore) >= Integer.valueOf(eMap.CreditScoreStartValue__c) && Integer.valueOf (ConsumerCheckResp.ccIcCreditScore) <= Integer.valueOf(eMap.CreditScoreEndValue__c)) || (String.isNotBlank(ConsumerCheckResp.ccIcRejectCode) && ConsumerCheckResp.ccIcRejectCode == eMap.HitCodeResponse__c)){
                                this.EquifaxApprovalType = eMap.ApprovalType__c;
                                this.EquifaxRiskGrade = eMap.RiskGrade__c;
                                if(String.isNotBlank(eMap.RiskGradeDisplayValue__c)){
                                    this.EquifaxRiskGradeDisplayValue = eMap.RiskGradeDisplayValue__c;
                                }
                            }
                        }else{
                            if(eMap.NoHit__c){
                                this.isNoHit = true;
                                noHitRiskGrade = eMap.RiskGrade__c;
                                noHitRiskGradeDisplayValue = eMap.RiskGradeDisplayValue__c;
                                noHitApprovalType = eMap.ApprovalType__c;
                            }
                            this.EquifaxApprovalType = eMap.ApprovalType__c;
                            this.EquifaxRiskGrade = eMap.RiskGrade__c;
                            if(String.isNotBlank(eMap.RiskGradeDisplayValue__c)){
                                this.EquifaxRiskGradeDisplayValue = eMap.RiskGradeDisplayValue__c;
                            }
                        }
                    }               
                }
                if(isNoHit){
                    //Set the No Hit Values
                    this.EquifaxApprovalType = noHitApprovalType;
                    this.EquifaxRiskGrade = noHitRiskGrade;
                    this.EquifaxRiskGradeDisplayValue = noHitRiskGradeDisplayValue;
                    this.blockCreditCheck = true;
                    this.acceptCreditCondition();
                    if (CreditCheckAttempts > 0){
                        this.requireSelectConfirmMsg = 'Unable to find customer.';
                        if(allowPageMessage)
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.WARNING, 'Customer not found. Please confirm your search criteria and try again. You have <strong>'+CreditCheckAttempts+'</strong> attempts left.' ));
                    }else{
                        doSoftCheck = false;
                        // If reached maximum number of tries and still no hit throw error message
                        this.requireSelectConfirmMsg = Equifax_Conditions__c.getinstance( EquifaxApprovalType ).Customer_Message__c;
                        if(allowPageMessage)
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.WARNING, 'Maximum number of attempts reached. <br><strong>'+requireSelectConfirmMsg+'</strong><br/><strong>Risk Grade: </strong>' + EquifaxRiskGradeDisplayValue));
                        return null;
                    }
                }else {
                    // For all other HIT scenarios
                    doSoftCheck = false;
                    if(ConsumerCheckResp.ErrorMessage == 'Hit Code Missing') {
                        // HRM 10862 - Handling partial Equifax Response
                        this.isCreditCheckError = true;
                        this.isCreditCheckErrorCount += 1;
                        this.blockCreditCheck = true;
                        setDefaultCondition(CREDIT_CONDITION.ON_FAILURE);
                        this.acceptCreditCondition();
                        
                        if(allowPageMessage){                     
                            if(this.CreditCheckAttempts == 0 || this.isCreditCheckErrorCount >= this.CreditCheckAttempts){
                                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.WARNING, 'Maximum number of attempts reached. <br><strong>'+requireSelectConfirmMsg+'</strong>' ));
                            }else {
                                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Could not complete credit check.<br/><strong style="color:red;"></strong><br/>' ));
                            }
                        }
                        // Clearing out the confirm message which gets set in the setDefaultCondition method
                        this.requireSelectConfirmMsg = '';
                    }else{
                        // If we get a proper response
                        List<Equifax_Conditions__c> equifaxList = [SELECT Name, Payment_Frequency__c, Three_Pay_Allowed__c, EasyPay_Required__c, Deposit_Required_Percentage__c, Customer_Message__c, Agent_Quoting_Message__c,Agent_Message__c FROM Equifax_Conditions__c WHERE Name = :EquifaxApprovalType];

                        if(equifaxList != null && !equifaxList.isEmpty()){
                            this.EquifaxCondition = equifaxList[0];
                            this.blockCreditCheck = true;
                            this.acceptCreditCondition();
                            String customerMessage = '<div class="confirmMsgHeader">Congratulations !!</div><div class="confirmMsgContent">' + EquifaxCondition.Customer_Message__c + '</div><strong>Risk Grade: </strong>' + EquifaxRiskGradeDisplayValue; 
                            this.requireSelectConfirmMsg = customerMessage; 
                            this.renderMainFrm = false;
                            if(allowPageMessage){
                                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, customerMessage));
                            }
                        }
                    }
                }
            }           
        }catch(Exception err){
            this.isCreditCheckError = true;
            this.isCreditCheckErrorCount += 1;
            this.blockCreditCheck = true;
            setDefaultCondition(CREDIT_CONDITION.ON_FAILURE);
            this.acceptCreditCondition();
            if(allowPageMessage){    
                if (this.CreditCheckAttempts == 0 || this.isCreditCheckErrorCount >= this.CreditCheckAttempts){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.WARNING, 'Maximum number of attempts reached. <br><strong>'+requireSelectConfirmMsg+'</strong>' ));
                }else{
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Could not complete credit check.<br/><strong style="color:red;">'+err.getMessage()+'</strong><br/>'+err.getStackTraceString()));
                }
            }
        }
        return null;
    }

    @Testvisible
    private void setDefaultCondition (CREDIT_CONDITION cCond){
        if( cCond == CREDIT_CONDITION.DEFAULT_CODE ){
            EquifaxApprovalType = Equifax__c.getinstance('Default Condition Code').value__c;
            EquifaxRiskGrade = Equifax__c.getinstance('Default Risk Grade').value__c;
            EquifaxRiskGradeDisplayValue = Equifax__c.getinstance('Default Risk Grade Display Value').value__c;
        }else if ( cCond == CREDIT_CONDITION.ON_FAILURE ){
            EquifaxApprovalType = Equifax__c.getinstance('Failure Condition Code').value__c;
            EquifaxRiskGrade = Equifax__c.getinstance('Failure Risk Grade').value__c;
            EquifaxRiskGradeDisplayValue = Equifax__c.getinstance('Failure Risk Grade Display Value').value__c;
        }
        requireSelectConfirmMsg = Equifax_Conditions__c.getinstance( EquifaxApprovalType ).Customer_Message__c;
        Equifax_Credit_Score = '6.50';
    }

    @Testvisible
    private void acceptCreditCondition(){
        try{
            if ( this.blockCreditCheck ){
                this.a.Equifax_Last_Check_DateTime__c = DateTime.now();
            }
            this.a.Equifax_Credit_Score__c = this.Equifax_Credit_Score;
            this.a.EquifaxApprovalType__c = this.EquifaxApprovalType;
            this.a.EquifaxRiskGrade__c = this.EquifaxRiskGrade;
            if(String.isNotBlank(ssnNumber4)){
                this.a.Equifax_Last_Four__c = ssnNumber4;
            }
            //Added by TCS HRM 5400
            this.a.DOB_encrypted__c = dobMonth+'/'+dobDayOfMonth+'/'+dobYear;
            this.a.Customer_Refused_to_provide_DOB__c = false;
            //end
            if(String.isNotBlank(ssn2) && String.isNotBlank(ssn3)){
                this.a.Equifax_Full_SSN_Used__c = true; 
            }
            // HRM - 4893 Start by Siddarth Asokan
            if(renderFieldView){
                // Populate the JSON field on the account for field sales only
                CPQIntegrationDataGenerator.IntegrationDataSource iSource = new CPQIntegrationDataGenerator.IntegrationDataSource(this.a.Id);                
                iSource.equifaxApprovalType = this.a.EquifaxApprovalType__c;
                iSource.equifaxRiskGrade = this.a.EquifaxRiskGrade__c;
                this.a.CPQIntegrationData__c = CPQIntegrationDataGenerator.getCPQIntegrationJSON( iSource );
            }
            // HRM - 4893 End
            //Save prev address field on account
            if(String.isNotBlank(ccPrevStreet)){
                this.a.PreviousStreet__c = ccPrevStreet;
                if(String.isNotBlank(ccPrevStreet2)){
                    this.a.PreviousStreet2__c = ccPrevStreet2;
                }
                this.a.PreviousCity__c = ccPrevCity;
                this.a.PreviousState__c = ccPrevState;
                this.a.PreviousZip__c = ccPrevPostalCode;
            }else if(String.isNotBlank(this.a.PreviousStreet__c)){
                //Clearn prev address field on account if prev street isnot populated from page.
                this.a.PreviousStreet__c = '';
                this.a.PreviousStreet2__c = '';
                this.a.PreviousCity__c = '';
                this.a.PreviousState__c = '';
                this.a.PreviousZip__c = '';
            }
            update this.a;
            isAcceptConditionSuccess = true;
        }catch( Exception err){
            isAcceptConditionSuccess = false;
            if(allowPageMessage)
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Could not complete condition agreement.<br/><strong style="color:red;">'+err.getMessage()+'</strong><br/>'+err.getStackTraceString() ));
        }
    }
}