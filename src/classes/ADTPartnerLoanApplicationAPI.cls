/************************************* MODIFICATION LOG ************************************************************
* RVLoanApplicationAPI
*
* DESCRIPTION : Defines logic to handle the method calls from RV Loan Application API. 
*-------------------------------------------------------------------------------------------------------------------
* DEVELOPER            	                 DATE                    Ticket                REASON
*-------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari                      04/29/2019               HRM-9547	    	  Original Version
* Srinivas Yarramsetti                  06/06/2019               HRM-9547             Moderator

*/
@RestResource(urlMapping='/loanApplication/*')
global class ADTPartnerLoanApplicationAPI {
    @HttpPost
    global static void doPost() {
        ADTPartnerLoanAppAPISchema.LoanApplicationRequestMessage loanAppReq;
        try{
            system.debug('***** time when hit by API *******' + datetime.now());
            String jsonCustReq = RestContext.request.requestBody.toString();
            System.debug('### rquest for json is' + jsonCustReq);
            RestResponse res = RestContext.response;
            boolean isRequiredFlag = true;
            ADTPartnerLoanAppAPISchema.LoanApplicationResponseMessage loanAppRes = new ADTPartnerLoanAppAPISchema.LoanApplicationResponseMessage();
            loanAppRes.loanApplicationResponse = new ADTPartnerLoanAppAPISchema.LoanApplicationResponse();
            String loanAppErrorStr;
            try{
                loanAppReq = (ADTPartnerLoanAppAPISchema.LoanApplicationRequestMessage)JSON.deserializeStrict(jsonCustReq, ADTPartnerLoanAppAPISchema.LoanApplicationRequestMessage.class);       
                system.debug('Cust Req'+loanAppReq+'req params..'+RestContext.request.params);
            }catch(Exception e){
                isRequiredFlag = false;
                res.statusCode = 500;
                loanAppErrorStr = serAllErrors(PartnerAPIMessaging__c.getinstance('500').Error_Message__c, res.statusCode, loanAppReq.loanApplicationRequest.opportunityId);
                ADTApplicationMonitor.log ('Partner Loan Decision: Cust Req: '+ jsonCustReq, e.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
            }
            if(isRequiredFlag){
                String accId = null;
                // Get Account from Opportunity logic
                if(String.isNotBlank(loanAppReq.loanApplicationRequest.opportunityId)){
                    list<Opportunity> oppList = [Select Id, AccountId From Opportunity Where Id =: loanAppReq.loanApplicationRequest.opportunityId And AccountId != Null];
                    if(oppList != null && oppList.size() > 0){
                        accId = oppList.get(0).AccountId;
                    }
                }
                if(accId != null && String.isNotBlank(loanAppReq.loanApplicationRequest.callId)){
                    for(Call_Data__c cd : [SELECT Id, Account__c FROM Call_Data__c WHERE Id =:loanAppReq.loanApplicationRequest.callID AND Account__c !=: accId]){
                		accId = null;
                	}
                	
                    /*list<Call_Data__c> callList = [SELECT Id, Account__c FROM Call_Data__c WHERE Id =:loanAppReq.loanApplicationRequest.callID];
                    if(!callList.isEmpty() && callList.get(0).Account__c != null){
                       if(accId == callList.get(0).Account__c){}
                       else{
                           accId = null;
                       }
                    }*/
                }
                if(accId != null){
                    loanAppRes.loanApplicationResponse.opportunityId = loanAppReq.loanApplicationRequest.opportunityID;
                    
                    try{
                        loanAppRes.loanApplicationResponse.loanApplication = ADTPartnerLoanAppAPIHelper.getLoanApplication(loanAppReq, accId);
                        loanAppRes.loanApplicationResponse.message = ADTPartnerLoanAppAPIHelper.responseMessage;
                    }catch(IntegrationException e){
                        if(String.isNotBlank(e.getMessage())){
                            if(e.getMessage().contains('Missing values in request:')){
                                res.statusCode =  400;
                                loanAppErrorStr = serAllErrors(e.getMessage(), res.statusCode, loanAppReq.loanApplicationRequest.opportunityId);
                                
                            }else if(e.getMessage().contains('Data Error:')){
                                res.statusCode =  400;
                                loanAppErrorStr = serAllErrors(e.getMessage().remove('Data Error:').trim(), res.statusCode, loanAppReq.loanApplicationRequest.opportunityId);
                                
                            }else if(e.getMessage().contains('Callout to Zoot failed:')){
                                loanAppErrorStr = e.getMessage().remove('Callout to Zoot failed:').trim();
                                if(loanAppErrorStr.contains('errorMessage')){
                                    ADTPartnerLoanAppAPISchema.zootError zoorErrorslist = desZootError(loanAppErrorStr);
                                    res.statusCode =  Integer.valueOf(zoorErrorslist.errors[0].status);
                                }
                                else{
                                    res.statusCode =  500;
                                    loanAppErrorStr = serAllErrors(PartnerAPIMessaging__c.getinstance('500').Error_Message__c, res.statusCode, loanAppReq.loanApplicationRequest.opportunityId);
                                }
                            }
                            else{
                                res.statusCode =  500;
                                loanAppErrorStr = serAllErrors(PartnerAPIMessaging__c.getinstance('500').Error_Message__c, res.statusCode, loanAppReq.loanApplicationRequest.opportunityId);
                            }
                            loanAppRes.loanApplicationResponse.loanApplication = null;
                        }
                    }
                }else{
                    res.statusCode = 404;
                    loanAppErrorStr = serAllErrors(PartnerAPIMessaging__c.getinstance('404').Error_Message__c.replace('requestID','CallID or Opportunity ID'), res.statusCode, loanAppReq.loanApplicationRequest.opportunityId);
                    
                }
            }
            String custResponse = String.isNotBlank(loanAppErrorStr)?loanAppErrorStr:JSON.serialize(loanAppRes, true);
            system.debug('JSON Resp to RV:'+custResponse);
                    
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf(custResponse);
            
            system.debug('***** time when done on API *******' + datetime.now());
        }catch(Exception ex){
            ADTApplicationMonitor.log(ex, 'ADTPartnerLoanAppAPI',loanAppReq.loanApplicationRequest.opportunityId, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);   
        }
    }
    
    public static String serAllErrors(String errorMessage, Integer StatusCode, String optyId){
        ADTPartnerLoanAppAPISchema.zootError adtError = new ADTPartnerLoanAppAPISchema.zootError();
        ADTPartnerLoanAppAPISchema.errors err = new ADTPartnerLoanAppAPISchema.errors();
        err.status = String.valueOf(StatusCode);
        err.errorMessage = errorMessage+' Opporunity Id: '+optyId;
        err.errorCode = '-1';//Default code if Sfdc has error
        list<ADTPartnerLoanAppAPISchema.errors> errlist = new list<ADTPartnerLoanAppAPISchema.errors>();
        errlist.add(err);
        adtError.errors = errlist;
        String errString = JSON.serialize(adtError, true);
        return errString;
    }
    
    public static ADTPartnerLoanAppAPISchema.zootError desZootError(String errorStr){
        return (ADTPartnerLoanAppAPISchema.zootError)JSON.deserializeStrict(errorStr, ADTPartnerLoanAppAPISchema.zootError.class);
    }
}