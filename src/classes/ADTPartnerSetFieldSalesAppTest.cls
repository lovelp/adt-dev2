@isTest
public class ADTPartnerSetFieldSalesAppTest{
    public static Event evnt;
    public static Event evnt2;
    public static opportunity opp;
    public static Call_Data__c calldata;
    public static Account a2;
    public static opportunity opp2;
    public static Call_Data__c calldata2;
    public class NoDataFoundException extends Exception{}
    //Create NSC testuser
    private static User CreateNSCAgentUser(){
        Profile p1 = [select id from profile where name='ADT NSC Sales Representative'];
        User salesAgent = new User(alias = 'uatsr1', email='unitTestSalesAgent1@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p1.Id, EmployeeNumber__c = TestHelperClass.generateEmployeeNumber(),
                timezonesidkey='America/Los_Angeles', username='unitTestSalesAgent1@testorg.com',
                StartDateInCurrentJob__c = Date.today());
        insert salesAgent;
        return salesAgent;
    }
    
    //setup test user
     private static SchedUserTerr__c createSchedUserTerr(User u, Id stDefaultLocation){
        SchedUserTerr__c schObj  = new SchedUserTerr__c();
        schObj.User__c = u.Id;
        schObj.BusinessId__c = '1100';
        //schObj.Territory_Association_ID__c=ta.Id;
        schObj.Order_Types__c = 'New;Resale;Relocation;Add-On;Conversion;Custom Home';
        schObj.TMTownID__c = '623';
        schObj.TMSubTownID__c = '1';

        if( !String.isBlank(stDefaultLocation) ){
            schObj.Default_Starting_Address__c = stDefaultLocation;
        }
        
        // Sunday
        schObj.Sunday_Available_Start__c = 800;
        schObj.Sunday_Available_End__c = 2300;
        
        // Monday
        schObj.Monday_Available_Start__c = 800;
        schObj.Monday_Available_End__c = 2300;
        
        // Tuesday
        schObj.Tuesday_Available_Start__c = 800;
        schObj.Tuesday_Available_End__c = 2300;
        
        // Wednesday
        schObj.Wednesday_Available_Start__c = 800;
        schObj.Wednesday_Available_End__c = 2300;
        
        // Thursday
        schObj.Thursday_Available_Start__c = 800;
        schObj.Thursday_Available_End__c = 2300;
        
        // Friday
        schObj.Friday_Available_Start__c = 800;
        schObj.Friday_Available_End__c = 2300;
        
        // Saturday
        schObj.Saturday_Available_Start__c = 800;
        schObj.Saturday_Available_End__c = 2300;
        
        return schObj;
    }
    
    
     private static void testdataSetup(){
        //Do not run trigger while creating test data

        // Create User(s) Needed For Testing 
        Profile p1 = [select id from profile where name='ADT NA Sales Representative'];
		
		String empno = TestHelperClass.generateEmployeeNumber();
        empno = empno.substring(1);
        //testSalesRep1.EmployeeNumber__c = empno;
        //update testSalesRep1;
         
        User testSalesRep1 = new User(alias = 'utsr1', email='unitTestSalesRep1@testorg.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p1.Id, EmployeeNumber__c = empno,
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com',
                StartDateInCurrentJob__c = Date.today());
        insert testSalesRep1;
        
        User testSalesRep2 = new User(alias = 'utsr2', email='unitTestSalesRep2@testorg.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p1.Id, EmployeeNumber__c = '12354',
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep2@testorg.com',
                StartDateInCurrentJob__c = Date.today());
        insert testSalesRep2;
        
        User testSalesRep3 = new User(alias = 'utsr3', email='unitTestSalesRep3@testorg.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p1.Id, EmployeeNumber__c = TestHelperClass.generateEmployeeNumber(),
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep3@testorg.com',
                StartDateInCurrentJob__c = Date.today());
        insert testSalesRep3;

        User testSalesAgentRep = CreateNSCAgentUser();
        User testadmin = TestHelperClass.createAdminUser();
        
        system.runas(testSalesRep1){
                    
        //Custom setting record for error message.
        list<ErrorMessages__c> emlist = new list<ErrorMessages__c>();
        emlist.add(new ErrorMessages__c(name='EXCEPTION', Message__c='An exception occurred. Please contact ADT support Team'));
        emlist.add(new ErrorMessages__c(name='OPPORTUNITY_NOT_FOUND', Message__c='Opportunity not found'));
        insert emlist;
        List<ResaleGlobalVariables__c> rgv1=new list<ResaleGlobalVariables__c>();
        rgv1.add(new ResaleGlobalVariables__c(name='MMBNameLength',value__c ='28'));
        rgv1.add(new ResaleGlobalVariables__c(name='MMBAccountNameLength',value__c ='30'));
        rgv1.add(new ResaleGlobalVariables__c(name='SameDayAppointmentOffset',value__c ='15'));
        rgv1.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'TRUE'));
        /*rgv1.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgv1.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgv1.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgv1.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));*/
        rgv1.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgv1.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgv1.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgv1.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgv1.add(new ResaleGlobalVariables__c (name = 'DaysToPopulateDNISOnAccount',value__c = '2'));
        insert rgv1;
       
        list<PartnerAPIMessaging__c> partapilist = new list<PartnerAPIMessaging__c>();
        partapilist.add(new PartnerAPIMessaging__c(name='400',Error_Message__c = 'invalid request'));
        partapilist.add(new PartnerAPIMessaging__c(name='404',Error_Message__c= 'Opty not found'));
        partapilist.add(new PartnerAPIMessaging__c(name='500',Error_Message__c= 'Something went wrong while processing the request. Please try again later.'));
        partapilist.add(new PartnerAPIMessaging__c(name='AppointmentSuccessful', Error_Message__c='Set Appointment is successful'));
        partapilist.add(new PartnerAPIMessaging__c(name='AlternateSlotsFound', Error_Message__c='Unable to book an appointment for requested slot.'));
        partapilist.add(new PartnerAPIMessaging__c(name='AlternateSlotsNotFound', Error_Message__c='Unable to book an appointment for requested slot. No alternate slots found.'));
        partapilist.add(new PartnerAPIMessaging__c(name='AppntDateTimeError', Error_Message__c='AppntDateTimeError'));
        partapilist.add(new PartnerAPIMessaging__c(name='ExistingSalesAppnt', Error_Message__c='ExistingSalesAppnt'));
        partapilist.add(new PartnerAPIMessaging__c(name='ActiveAppntsNotFound', Error_Message__c='ActiveAppntsNotFound'));
        partapilist.add(new PartnerAPIMessaging__c(name='dllTechError', Error_Message__c='dllTechError'));
        partapilist.add(new PartnerAPIMessaging__c(name='rescheduleSalesAppnt', Error_Message__c='rescheduleSalesAppnt'));
        partapilist.add(new PartnerAPIMessaging__c(name='ReschalternateSlotsFound', Error_Message__c='ReschalternateSlotsFound'));
        partapilist.add(new PartnerAPIMessaging__c(name='ReschalternateSlotsNotFound', Error_Message__c='ReschalternateSlotsNotFound'));

        insert partapilist;
            
        EventDefaults__c ed = new EventDefaults__c();
        ed.DurationMinutes__c = 120;
        ed.IsReminderSet__c = true;
        ed.ReminderMinutesBeforeEvent__c = 15;
        ed.StartDateTimeOffsetMinutes__c = 1;
        insert ed;
            
            
        SchedDefaults__c scdef = new SchedDefaults__c();
        scdef.name = 'New';
        scdef.Busi_Task__c = 'SBV';
        scdef.Code__c = 'N1,N3';
        scdef.Duration__c = 2;
        scdef.Resi_Task__c = 'RHS';
        insert scdef;
        Address__c addr1 = new Address__c();
        addr1.Latitude__c = 26.39412020000000;
        addr1.Longitude__c = -80.11689480000000;
        addr1.Street__c = '1501 Yamato Rd';
        addr1.City__c = 'Boca Raton';
        addr1.State__c = 'FL';
        addr1.PostalCode__c = '33431';
        addr1.OriginalAddress__c = '1501 Yamato Rd, Boca Raton, FL 33431';
        addr1.StandardizedAddress__c = '1501 Yamato Rd, Boca Raton, FL 33431';
        insert addr1;
        //Create test data
        //Account a1 = TestHelperClass.createAccountData(addr1);
        
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.BusinessID__c = '1100';
        pc.Name = '221o2';
        pc.TMTownID__c = '623';
        pc.TMSubTownID__c = '1';
        insert pc;
        
        //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.925924;
        addr.Longitude__c = -77.229562;
        addr.Street__c = '1645 International Dr';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.OriginalAddress__c = '1645 International Dr, McLean, VA 221o2';
        
        insert addr;
        
        
        Account a1 = new Account(Name = 'Test Account');
        a1.AddressID__c = addr.Id;
        //a1.Channel__c = 'Custom Home Sales';
        a1.Business_Id__c = '1100';
        a1.MMBOrderType__c = 'N1';
        a1.LeadStatus__c = 'Active';
        a1.UnassignedLead__c = false;
        a1.InService__c = false;
        a1.PostalCodeID__c = pc.Id;
        a1.ShippingPostalCode = '221o2';
        
        insert a1;
        
            
        //Opportunity data
        opp = New Opportunity(Name = 'TestOpp1', StageName = 'Prospecting');
        opp.CloseDate = Date.today();
        opp.AccountId = a1.Id;
        insert opp;
        //Call data
        calldata = new Call_data__c();
        calldata.Account__c = a1.Id;
        calldata.Name = 'Test Call data';
        insert calldata;
            
        a2 = new Account();
        a2.AddressID__c = addr.Id;
        a2.Name = 'Test Account new '; 
        a2.MMBOrderType__c = 'N1';
        a2.Business_Id__c = '1100';
        a2.LeadStatus__c = 'Active';
        a2.UnassignedLead__c = false;
        a2.InService__c = false;
        a2.PostalCodeID__c = pc.Id;
        a2.ShippingPostalCode = '221o2';
        //a2.Channel__c = 'Custom Home Sales';
        
        insert a2;
        
        opp2 = New Opportunity();
        opp2.Name = 'TestOpp1New';
        opp2.StageName = 'Prospecting';
        opp2.CloseDate = Date.today();
        opp2.AccountId = a2.Id;
        insert opp2;
        
        calldata2 = new Call_data__c();
        calldata2.Account__c = a2.Id;
        calldata2.Name = 'Test Call data New';
        insert calldata2;
            
        evnt= new Event();
        evnt.whatId= a2.id;
        evnt.Status__c= EventManager.STATUS_SCHEDULED;
        evnt.StartDateTime=DateTime.Now().AddDays(1);
        evnt.DurationInMinutes= 120;
        evnt.ShowAs = EventManager.SHOW_AS_BUSY;
        evnt.Subject = 'AppointmentTest';
        evnt.EndDateTime = evnt.StartDateTime.addHours(2);
        insert evnt;
        
       
                    //
        // Create Events
        Timezone CustomerTimeZone = UserInfo.getTimeZone();

        // Creates Scheduled User Territories
        SchedUserTerr__c schUserTerr1 = createSchedUserTerr(testSalesRep1, addr.Id);
        insert schUserTerr1;
        SchedUserTerr__c schUserTerr2 = createSchedUserTerr(testSalesRep2, addr.Id);
        insert schUserTerr2;
        SchedUserTerr__c schUserTerr3 = createSchedUserTerr(testSalesRep3, addr.Id);
        insert schUserTerr3;
        SchedUserTerr__c schUserTerr4 = createSchedUserTerr(testSalesAgentRep, addr.Id);
        insert schUserTerr4;
        }   
        
         User user=[select id from User where id=: userinfo.getUserId()];
         system.runAs(user){
            IntegrationSettings__c is =new IntegrationSettings__c();
            is.TelemarCalloutsEnabled__c = true;
            is.TelemarCalloutsAsync__c = true;
            insert is;
         }
    }
    // Positive scenario
    static testMethod void TestDoPostMethod1(){
        testdataSetup();
        Date currentDate = System.today().addDays(1);
        String appntDate = String.valueOf(currentDate).substring(0,10);
        //system.debug('currentDate##'+appntDate);
        // Set the request body
        String sBody = '{ "partnerID": "Partner001","callID": "' +calldata.id+ '", "opportunityID": "' +opp.id+ '",'+  
                       +'"partnerRepName": "John Smith", "appointmentRequest": {"orderType": "N1","appointmentdate": "'+appntDate+'","appointmenttime": "08:00"}}';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/setFieldSalesAppointment';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(sBody);
        
        RestContext.request = req;
        RestContext.response= res;
        User user=[select id from User where id=: userinfo.getUserId()];
        system.runAs(user){
            
        ADTPartnersetFieldSalesAppAPI.dopostmethod();
        }
    }
    //appointmentrequest is empty
    static testMethod void TestDoPostMethod1a(){
        testdataSetup();
        //system.debug('currentDate##'+appntDate);
        // Set the request body
        String sBody = '{ "partnerID": "Partner001","callID": "' +calldata.id+ '", "opportunityID": "' +opp.id+ '",'+  
                       +'"partnerRepName": "John Smith", "appointmentRequest": {"orderType": " ","appointmentdate": " ","appointmenttime": " "}}';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/setFieldSalesAppointment';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(sBody);
        
        RestContext.request = req;
        RestContext.response= res;
        User user=[select id from User where id=: userinfo.getUserId()];
        system.runAs(user){
            
        ADTPartnersetFieldSalesAppAPI.dopostmethod();
        }
    }
    static testMethod void TestDoPostMethod2(){
        testdataSetup();
        User u = TestHelperClass.createUser(10);
        System.runAs(u) {
           String sBody = '{}';
           RestRequest req = new RestRequest();
           RestResponse res = new RestResponse();
           req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/setFieldSalesAppointment';
           req.httpMethod = 'POST';//HTTP Request Type
           req.requestBody = Blob.valueof(sBody) ;
           system.debug('###'+req);    
           RestContext.request = req;
           RestContext.response= res;
           User user=[select id from User where id=: userinfo.getUserId()];
           system.runAs(user){
           try{
            ADTPartnersetFieldSalesAppAPI.dopostmethod();
            PartnerAPIMessaging__c APIMsg = new PartnerAPIMessaging__c(Name = '201', Error_Message__c = 'TEST');
            insert APIMsg;
            ADTPartnersetFieldSalesAppAPI.errorResponse(201, 'success');
           }
            catch(Exception e){}
           }
       }
    }
    
    // Positive scenario
    static testMethod void TestDoPostMethod3(){
        testdataSetup();
        Date currentDate = System.today().addDays(1);
        // Set the request body
        String sBody = '{ "partnerID": "Partner001", "opportunityID": "' +opp.id+ '",'+  
                       +'"partnerRepName": "John Smith", "appointmentRequest": {"orderType": "N1","appointmentdate": "'+currentDate+'","appointmenttime": "08:00"}}';
                               RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/setFieldSalesAppointment';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(sBody);
        
        RestContext.request = req;
        RestContext.response= res;
        
        User user=[select id from User where id=: userinfo.getUserId()];
        system.runAs(user){
            ADTPartnersetFieldSalesAppAPI.dopostmethod();
        }
    }
    
        // Positive scenario
    static testMethod void TestDoPostMethod4(){
       
        testdataSetup();
        Date currentDate = System.today().addDays(1);
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/setFieldSalesAppointment';
        req.httpMethod = 'POST';//HTTP Request Type
        RestContext.request = req;
        RestContext.response= res;
        
        // Set the request body
        String sBody = '{ "partnerID": "Partner001",    "callID": "' +calldata.id+ '",    "opportunityID": "006c000000H2qx3",'+  
                       +'"partnerRepName": "John Smith", "appointmentRequest": {"orderType": "N1","appointmentdate": "'+currentDate+'","appointmenttime": "08:00"}}';
        req.requestBody = Blob.valueof(sBody);
        
        
        User user=[select id from User where id=: userinfo.getUserId()];
        system.runAs(user){
             try{
            	ADTPartnersetFieldSalesAppAPI.dopostmethod();
        	} catch(Exception ex){ 
        	}
        	String callId = Call_Data__c.sobjecttype.getDescribe().getKeyPrefix() + 'c000000H2qx3';
        	sBody = '{ "partnerID": "Partner001",    "callID": "' + callId + '",    "opportunityID": "' +opp.id+ '",'+   
                       +'"partnerRepName": "John Smith", "appointmentRequest": {"orderType": "N1","appointmentdate": "'+currentDate+'","appointmenttime": "08:00"}}';
        	req.requestBody = Blob.valueof(sBody);
        	try{
            	ADTPartnersetFieldSalesAppAPI.dopostmethod();
        	} catch(Exception ex){ 
        	}
        	sBody = '{ "partnerID": "Partner001",    "callID": "' + calldata2.id + '",    "opportunityID": "' +opp.id+ '",'+   
                       +'"partnerRepName": "John Smith", "appointmentRequest": {"orderType": "N1","appointmentdate": "'+currentDate+'","appointmenttime": "08:00"}}';
        	req.requestBody = Blob.valueof(sBody);
        	try{
            	ADTPartnersetFieldSalesAppAPI.dopostmethod();
        	} catch(Exception ex){ 
        	}
        	sBody = '{ "partnerID": "Partner001",    "callID": "' + calldata.id + '",    "opportunityID": "' +opp.id+ '",'+   
                       +'"partnerRepName": "John Smith"}';
        	req.requestBody = Blob.valueof(sBody);
        	try{
            	ADTPartnersetFieldSalesAppAPI.dopostmethod();
        	} catch(Exception ex){ 
        	}
        	sBody = '{ "partnerID": "Partner001",    "callID": "' + calldata.id + '",    "opportunityID": "' +opp.id+ '",'+   
                       +'"partnerRepName": "John Smith", "appointmentRequest": {"orderType": "N1","appointmentdate": "'+currentDate+'","appointmenttime": "08:45"}}';
        	req.requestBody = Blob.valueof(sBody);
        	try{
            	ADTPartnersetFieldSalesAppAPI.dopostmethod();
        	} catch(Exception ex){ 
        	}
        	sBody = '{ "partnerID": "Partner001", "callID": "' + calldata2.id + '", "opportunityID": "' + opp2.Id + '",'+  
            //+'"partnerRepName": "John Smith", "existingId":"'+evnt.id+'"}';//
            +'"partnerRepName": "John Smith", "appointmentRequest": {"orderType": "N1","appointmentDate": "' + currentDate + '","appointmenttime": "08:00"}}';
        	req.requestBody = Blob.valueof(sBody);
        	try{
            	ADTPartnersetFieldSalesAppAPI.dopostmethod();
        	} catch(Exception ex){ 
        	}
        }
    }
    
            // Positive scenario
    static testMethod void TestDoPostMethod5(){
        try{
            system.debug('hiiiii inside TestDoPostMethod5 ');
        testdataSetup();
        
        Date currentDate = System.today().addDays(1);
        // Set the request body
        String sBody = '{ "partnerID": "Partner001",    "callID": "' +calldata.id+ '",    "opportunityID": "'+opp.Id+'",'+  
            +'"partnerRepName": "John Smith","appointmentRequest": {"appointmentdate": "'+currentDate+'","appointmenttime": "08:00"}, "existingId":"'+evnt.id+'"}';
                     //  +'"partnerRepName": "John Smith", "appointmentRequest": {"orderType": "N1","appointmentdate": "'+currentDate+'","appointmenttime": "08:00"}}';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/setFieldSalesAppointment';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(sBody);
        
        RestContext.request = req;
        RestContext.response= res;
        
        ADTPartnersetFieldSalesAppAPI.dopostmethod();
        throw new NoDataFoundException('Call ID not found');
        }
        catch(Exception e){
            
        }
    }
    
    static testMethod void TestDoPostMethod6(){
        try{
          
        testdataSetup();
        Test.startTest();
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new GoogleDistanceMatrixApiRespGenerator());
        Date currentDate = System.today().addDays(1);
        // Set the request body
        String sBody = '{ "partnerID": "Partner001", "callID": "' + calldata2.id + '", "opportunityID": "' + opp2.Id + '",'+  
            //+'"partnerRepName": "John Smith", "existingId":"'+evnt.id+'"}';//
            +'"partnerRepName": "John Smith", "existingId":"' + evnt.id + '", "appointmentRequest": {"orderType": "N1","appointmentDate": "' + currentDate + '","appointmentTime": "08:00"}}';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/setFieldSalesAppointment';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(sBody);
        
        RestContext.request = req;
        RestContext.response= res;
        
        ADTPartnersetFieldSalesAppAPI.dopostmethod();
        User user=[select id from User where id=: userinfo.getUserId()];
        system.runAs(user){
            Account acc2= [select id from Account where id=: a2.id limit 1];
            acc2.MMBOrderType__c='';
            update acc2;
              
        ADTPartnersetFieldSalesAppAPI.dopostmethod();    
       	Test.stopTest();
        }
        }
        catch(Exception e){
            
        }
    }
    
    static testMethod void testFmethods(){
        testdataSetup();
        ADTPartnersetFieldSalesAppController adtpartObj = new ADTPartnersetFieldSalesAppController();
        adtpartObj.formatPhoneNumber('5615621234');
        adtpartObj.formatPhoneNumber('+15615621234');
        adtpartObj.isSameDayAppointment(System.today());
        adtpartObj.TimeDiffMinutes(Time.newInstance(13, 30, 2, 20),Time.newInstance(18, 30, 2, 20));
        //adtpartObj.setEventToCanceled();
    }
    static String setFieldSalesResponse()
    {string xmlBodyStr='{{  " {    "opportunityID": "0061300001O8exI",    "setAppointment": {      "success": true,      "message": "Unable to set appointment - alternate time slots returned",      "id": "00U1B00000iNBKCUA4",      "startDateTime": "2017-10-24T08:00:00-0400",      "endDateTime": "2017-10-24T12:00:00-0400",      "salesRep": {        "name": "Jone Jones",        "email": "jjones@adt.com",        "phone": "904-111-2323"      }    },    "alternateSlots": [      {        "appointmentAvailableDate": "2017-10-21",        "appointmentAvailableTimes": [          [            "08:00",            "09:00",            "10:00"          ]        ]      }    ]  }}';
        return xmlBodyStr; 
    }
    
    static testmethod void testme(){
        ADTPartnerAppointmentSchema.AppointmentRequest appnteq = new ADTPartnerAppointmentSchema.AppointmentRequest();
        appnteq.startingDate = '2017-10-24T08:00:00-0400';
    }
   
    // No customer Id - Select Site
 
}