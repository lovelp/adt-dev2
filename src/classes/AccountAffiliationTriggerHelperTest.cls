/**
    * This class contains unit tests for validating the behavior of Apex classes
    * and triggers.
    *
    * Unit tests are class methods that verify whether a particular piece
    * of code is working properly. Unit test methods take no arguments,
    * commit no data to the database, and are flagged with the testMethod
    * keyword in the method definition.
    *
    * All test methods in an organization are executed whenever Apex code is deployed
    * to a production organization to confirm correctness, ensure code
    * coverage, and prevent regressions. All Apex classes are
    * required to have at least 75% code coverage in order to be deployed
    * to a production organization. In addition, all triggers must have some code coverage.
    * 
    * The @isTest class annotation indicates this class only contains test
    * methods. Classes defined with the @isTest annotation do not count against
    * the organization size limit for all Apex scripts.
    *
    * See the Apex Language Reference for more information about Testing and Code Coverage.
    */

@isTest
public class AccountAffiliationTriggerHelperTest {
    
    public static Account acc;
    public static Account acc1;
    
    static testmethod void testprocessAccountUpdatesOnInsert(){
        
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;
    
        System.runAs(current) { 
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            update u;
        
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
            insert addr;
        
            acc = TestHelperClass.createAccountData(addr, true);
            acc.FirstName__c = 'TestFName';
            acc.LastName__c = 'TestLName';
            acc.Name = 'TestFName TestLName';
            acc.OwnerId = u.Id;
            update acc;
        
            aff = new Affiliate__c();
            aff.Affiliate_Description__c = 'USAA Test';
            aff.Relevance__c = 1;
            aff.Name = 'USAA';
            aff.Member_Required__c = true;
            aff.Member_validated_Field_Sales__c = true;
            aff.Builder__c = true;
            aff.Active__c = true;
            insert aff;
                
            Account_Affiliation__c accaff = new Account_Affiliation__c();
            accaff.Affiliate__c = aff.id;
            accaff.Active__c = true;
            accaff.Member__c = acc.id;
            accaff.MemberNumber__c = '';
            accaff.Military_Rank__c = 'CAPTAIN';
            insert accaff;
            
            List<Account> accUpdate = new List<Account>([Select Id, ExtraSkills__c From Account where Id =:acc.Id]);
            system.assertEquals(Channels.TERRITORYTYPE_BUILDER, accUpdate[0].ExtraSkills__c);
            
        	//accaff.Active__c = false;
            //update accaff;
        
        }
    }
    
    static testmethod void testprocessAccountUpdatesOnInsert2(){
        
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;
    
        System.runAs(current) { 
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            update u;
        
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
            insert addr;
        
            acc = TestHelperClass.createAccountData(addr, true);
            acc.FirstName__c = 'TestFName';
            acc.LastName__c = 'TestLName';
            acc.Name = 'TestFName TestLName';
            acc.OwnerId = u.Id;
            acc.ExtraSkills__c = 'Spanish';
            update acc;
        
            aff = new Affiliate__c();
            aff.Affiliate_Description__c = 'USAA Test';
            aff.Relevance__c = 1;
            aff.Name = 'USAA';
            aff.Member_Required__c = true;
            aff.Member_validated_Field_Sales__c = true;
            aff.Builder__c = true;
            aff.Active__c = true;
            insert aff;
                
            Account_Affiliation__c accaff = new Account_Affiliation__c();
            accaff.Affiliate__c = aff.id;
            accaff.Active__c = true;
            accaff.Member__c = acc.id;
            accaff.MemberNumber__c = '';
            accaff.Military_Rank__c = 'CAPTAIN';
            insert accaff;
            
            List<Account> accUpdate = new List<Account>([Select Id, ExtraSkills__c From Account where Id =:acc.Id]);
            system.assertEquals('Builder;Spanish', accUpdate[0].ExtraSkills__c);
            
        }
    }
    
    static testmethod void testprocessAccountUpdatesOnUpdate(){
        
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;
    
        System.runAs(current) { 
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            update u;
        
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
            insert addr;
        
            acc = TestHelperClass.createAccountData(addr, true);
            acc.FirstName__c = 'TestFName';
            acc.LastName__c = 'TestLName';
            acc.Name = 'TestFName TestLName';
            acc.OwnerId = u.Id;
            acc.ExtraSkills__c = 'Spanish';
            update acc;
        
            aff = new Affiliate__c();
            aff.Affiliate_Description__c = 'USAA Test';
            aff.Relevance__c = 1;
            aff.Name = 'USAA';
            aff.Member_Required__c = true;
            aff.Member_validated_Field_Sales__c = true;
            aff.Builder__c = true;
            aff.Active__c = true;
            insert aff;
                
            Account_Affiliation__c accaff = new Account_Affiliation__c();
            accaff.Affiliate__c = aff.id;
            accaff.Active__c = true;
            accaff.Member__c = acc.id;
            accaff.MemberNumber__c = '';
            accaff.Military_Rank__c = 'CAPTAIN';
            insert accaff;
            
            List<Account> accUpdate = new List<Account>([Select Id, ExtraSkills__c From Account where Id =:acc.Id]);
            system.assertEquals('Builder;Spanish', accUpdate[0].ExtraSkills__c);
            
            accaff.Active__c = false;
            update accaff;
            
            List<Account> accUpdateList = new List<Account>([Select Id, ExtraSkills__c From Account where Id =:acc.Id]);
            system.assertEquals('Spanish', accUpdateList[0].ExtraSkills__c);
            
        }
    }
    
    
    static testmethod void testprocessAccountUpdatesOnUpdate2(){
        
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;
    
        System.runAs(current) { 
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            update u;
        
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
            insert addr;
        
            acc = TestHelperClass.createAccountData(addr, true);
            acc.FirstName__c = 'TestFName';
            acc.LastName__c = 'TestLName';
            acc.Name = 'TestFName TestLName';
            acc.OwnerId = u.Id;
            acc.ExtraSkills__c = 'Spanish';
            update acc;
            
            acc1 = TestHelperClass.createAccountData(addr, true);
        
            aff = new Affiliate__c();
            aff.Affiliate_Description__c = 'USAA Test';
            aff.Relevance__c = 1;
            aff.Name = 'USAA';
            aff.Member_Required__c = true;
            aff.Member_validated_Field_Sales__c = true;
            aff.Builder__c = true;
            aff.Active__c = true;
            insert aff;
                
            List<Account_Affiliation__c> accaffList = new List<Account_Affiliation__c>();
            accaffList.add(new Account_Affiliation__c(Affiliate__c = aff.id, Active__c = true, Member__c = acc.id, MemberNumber__c = '', Military_Rank__c = 'CAPTAIN'));
            accaffList.add(new Account_Affiliation__c(Affiliate__c = aff.id, Active__c = false, Member__c = acc.id, MemberNumber__c = '', Military_Rank__c = 'CAPTAIN'));
            accaffList.add(new Account_Affiliation__c(Affiliate__c = aff.id, Active__c = false, Member__c = acc1.id, MemberNumber__c = '', Military_Rank__c = 'CAPTAIN'));
            insert accaffList;
            
            List<Account> accUpdate = new List<Account>([Select Id, ExtraSkills__c From Account where Id =:acc.Id]);
            system.assertEquals('Builder;Spanish', accUpdate[0].ExtraSkills__c);
            
            accUpdate[0].ExtraSkills__c = '';
            acc1.ExtraSkills__c = 'Spanish';
            accUpdate.add(acc1);
            update accUpdate;
            
            accaffList[0].Active__c = false;
			accaffList[1].Active__c = true;
            accaffList[2].Active__c = true;
            update accaffList;
            
            List<Account> accUpdateList = new List<Account>([Select Id, ExtraSkills__c From Account where Id =:acc.Id]);
            system.assertEquals('Builder', accUpdateList[0].ExtraSkills__c);
            
        }
    }
    
    static testmethod void testprocessAccountUpdatesOnDelete(){
        
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;
    
        System.runAs(current) { 
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            update u;
        
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
            insert addr;
        
            acc = TestHelperClass.createAccountData(addr, true);
            acc.FirstName__c = 'TestFName';
            acc.LastName__c = 'TestLName';
            acc.Name = 'TestFName TestLName';
            acc.OwnerId = u.Id;
            acc.ExtraSkills__c = 'Spanish';
            update acc;
        
            aff = new Affiliate__c();
            aff.Affiliate_Description__c = 'USAA Test';
            aff.Relevance__c = 1;
            aff.Name = 'USAA';
            aff.Member_Required__c = true;
            aff.Member_validated_Field_Sales__c = true;
            aff.Builder__c = true;
            aff.Active__c = true;
            insert aff;
                
            Account_Affiliation__c accaff = new Account_Affiliation__c();
            accaff.Affiliate__c = aff.id;
            accaff.Active__c = true;
            accaff.Member__c = acc.id;
            accaff.MemberNumber__c = '';
            accaff.Military_Rank__c = 'CAPTAIN';
            insert accaff;
            
            List<Account> accUpdate = new List<Account>([Select Id, ExtraSkills__c From Account where Id =:acc.Id]);
            system.assertEquals('Builder;Spanish', accUpdate[0].ExtraSkills__c);
            
            delete accaff;
            
            List<Account> accUpdateList = new List<Account>([Select Id, ExtraSkills__c From Account where Id =:acc.Id]);
            system.assertEquals('Spanish', accUpdateList[0].ExtraSkills__c);
            
        }
    }
    
    static testmethod void testprocessAccountUpdatesOnDelete2(){
        
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;
    
        System.runAs(current) { 
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            update u;
        
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
            insert addr;
        
            acc = TestHelperClass.createAccountData(addr, true);
            acc.FirstName__c = 'TestFName';
            acc.LastName__c = 'TestLName';
            acc.Name = 'TestFName TestLName';
            acc.OwnerId = u.Id;
            acc.ExtraSkills__c = 'Spanish';
            update acc;
            
            acc1 = TestHelperClass.createAccountData(addr, true);
        
            aff = new Affiliate__c();
            aff.Affiliate_Description__c = 'USAA Test';
            aff.Relevance__c = 1;
            aff.Name = 'USAA';
            aff.Member_Required__c = true;
            aff.Member_validated_Field_Sales__c = true;
            aff.Builder__c = true;
            aff.Active__c = true;
            insert aff;
                
            List<Account_Affiliation__c> accaffList = new List<Account_Affiliation__c>();
            accaffList.add(new Account_Affiliation__c(Affiliate__c = aff.id, Active__c = true, Member__c = acc.id, MemberNumber__c = '', Military_Rank__c = 'CAPTAIN'));
            accaffList.add(new Account_Affiliation__c(Affiliate__c = aff.id, Active__c = false, Member__c = acc.id, MemberNumber__c = '', Military_Rank__c = 'CAPTAIN'));
            accaffList.add(new Account_Affiliation__c(Affiliate__c = aff.id, Active__c = false, Member__c = acc1.id, MemberNumber__c = '', Military_Rank__c = 'CAPTAIN'));
            insert accaffList;
            
            List<Account> accUpdate = new List<Account>([Select Id, ExtraSkills__c From Account where Id =:acc.Id]);
            system.assertEquals('Builder;Spanish', accUpdate[0].ExtraSkills__c);
            
            acc.ExtraSkills__c = 'Spanish';
            update acc;
                                   
            delete accaffList[1];
            
            List<Account> accUpdateList = new List<Account>([Select Id, ExtraSkills__c From Account where Id =:acc.Id]);
            system.assertEquals('Builder;Spanish', accUpdateList[0].ExtraSkills__c);
            
        }
    }


}