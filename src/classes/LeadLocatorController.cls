/************************************* MODIFICATION LOG ********************************************************************************************
* LeadLocatorController
* 
DESCRIPTION : Obtains all dynamic data required by the Locator and Cloverleaf tools
*             It is a Controller in the SFDC MVC architecture. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                                             DATE                              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*                                               10/14/2011                      - Original Version
*
* Shivkant Vadlamani                    03/23/2012                      - Incorporated Phase 2 changes including support for Cloverleaf tool 
*                                                                                                         
*                                                                                                       
*/

public class LeadLocatorController
{ 
    // contains MapItems for display on the map 
    public List<MapItem> locations {get;set;}
    // a delimited list of accounts for mapping
    public String mapDataPointsStr {get; set;}
    // delimiter that divides accounts to be mapped; also used to delimit disposition updates from the page
    private static final String POINT_DELIMITER = '~~~~';
    // delimiter that divides an account id and a disposition code
    private static final String DATA_DELIMITER = '#';
    
    // corresponds to the starting point of the search; can be an account, a user entered address, or the user's current location
    public MapItem startingPoint {get;set;}
    
    // contains the user entered address
    public String addressEntered {get; set;}

    // indicates whether account filters should be rendered on the page
    public Boolean mDisplayAccountSelectLists {get;set;}
    
    // correspond to filters on the page
    public Integer mMaxLeads {get;set;}
    public String distance {get;set;}
   
    // used when creating a street sheet; page populates this with a comma delimited list of account ids
    public String ssiIds{get;set;}
    
    // used to display any errors to the user
    public String errorMessage{get;set;}
    
    // SFDC profile of the current user
    public String mCurrentUserProfile {get;set;}
    
    // indicates if a Back link should be present on the page
    public Boolean  mBShowBackLink {get;set;}
    
    // holds the account Id supplied as a query string parameter
    public String mAid {get;set;}
    // holds the user entered address supplied as a query string parameter
    public String mAddr {get;set;}
    // holds the latitude and longitude supplied as a query string parameter
    public String mLat {get;set;}
    public String mLon {get;set;}
    
    // Context User's data
    public User currUser {get;set;}
    
    // Context Usre's Managers
    public set<String> MgrNames {get;set;}
    
    // Creating variables locally to avoid multiple calls to ProfileHelper.
    public Boolean isSalesRep;
    public Boolean isManager;
    public Boolean isSalesExec;
    public Boolean isSysAdmin;
    public Boolean isCloverLeaf = false;
    public Boolean isError {get;set;}
    
    // Default values in the filter
    public String InitialDistanceValue {get;set;}
        public String InitialLeadNumsValue {get;set;}
        public String InitialDataSourcesValue {get;set;}
        public String InitialDispositionValue {get;set;}
        public String InitialRepAssignedValue {get;set;}
        public String InitialDiscoReasonValue {get;set;}
        public String InitialNewMoverValue {get;set;}
        public Boolean InitialActiveSiteValue {get;set;}
        public Boolean InitialAvailForAssignmentValue {get;set;}
        
        public id SelectedPLValue {get; set;}
    
    private ProximitySearchSettings__c pss;
   
    //------------------------------------------------------------------------------------------------------------------------------
    //init()
    //------------------------------------------------------------------------------------------------------------------------------
    public PageReference init()
    {           
        // look for query string parameters
        mAid = ApexPages.currentPage().getParameters().get('aid');        
        mAddr = ApexPages.currentPage().getParameters().get('addr');
        mLat = ApexPages.currentPage().getParameters().get('lat'); 
        mLon = ApexPages.currentPage().getParameters().get('lon');         
        isError = false;
        
        String clf =  ApexPages.currentPage().getParameters().get('clf');
        if(clf != null && clf != '')
        {
                isCloverLeaf = clf.equals('true');      
        }   
        
        // Fetch context user's information
        currUser = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                           FROM User
                                           WHERE Id = :UserInfo.getUserId()];
        
        // Get ProximitySearchSettings for context user
        pss = ProximitySearchSettings__c.getInstance(currUser.ProfileId);
        
        //Hide "Back To.." link if user navigates to LL from a tab, or user enters an address       
        if(mAid == '' || mAid == null) {
            mBShowBackLink = false;
        }
        else {
            mBShowBackLink = true;
        }
              
                // initialize lists that will contain search results
        locations = new List<MapItem>();
        
        // get the current user's profile
        String profileId = UserInfo.getProfileId();
        Profile prf = [select Id, Name from Profile where Id=:profileId];
        mCurrentUserProfile = prf.Name;
        
        isSalesRep = ProfileHelper.isSalesRep(mCurrentUserProfile);
        isManager = ProfileHelper.isManager(mCurrentUserProfile);
                isSalesExec = ProfileHelper.isSalesExec(mCurrentUserProfile);
        isSysAdmin = ProfileHelper.isSysAdmin(mCurrentUserProfile);
        
        // default will be no account level filters
        mDisplayAccountSelectLists = false; 
      
        if (isManager || isSalesExec || isSysAdmin) 
        {
            // such filters will be available for managers and system admins
            mDisplayAccountSelectLists = true;
        }
        
        //Setting default filter values
        if(mDisplayAccountSelectLists)
        {
                InitialDistanceValue = '10';
                        InitialLeadNumsValue = '100';
        }
        else
        {
                InitialDistanceValue = '5';
                        InitialLeadNumsValue = '50';
        }
                InitialDataSourcesValue = 'All';
                InitialDispositionValue = 'All';
                InitialRepAssignedValue = 'All';
                InitialDiscoReasonValue = 'All';
                InitialNewMoverValue = 'All';
                InitialActiveSiteValue = true;
                InitialAvailForAssignmentValue = true;
        
        // a maximum of 100 leads
        mMaxLeads = 100;
        
        // set distance filter based on the user's profile      
        
        if(isCloverLeaf)
                {
                        distance = '1';
                }
                else
                {
                        if (isManager || isSalesExec || isSysAdmin) 
                {
                    distance = '10';
                }    
                else 
                {
                    distance = '5';
                }
                }
        
        if(isSalesRep && currUser.UserRoleId != null)
        {
                MgrNames = new set<String>();             
                for(User usr : Utilities.getManagersForRole(currUser.UserRoleId))
                {
                        MgrNames.add(usr.Name); 
                }               
        }
        
        // determine the starting point for the search
        setStartingPoint();
        // then search for nearby accounts
        searchNearby();
        
        if(!isCloverLeaf && locations != null && locations.size() > 0)
        {               
                //Create a LeadLocator metrics record right after the call to find nearby accounts 
            LeadLocatorMetrics__c llMetrics = new LeadLocatorMetrics__c();
            llMetrics.LeadLocatorInvokedBy__c = UserInfo.getUserId();
            llMetrics.NumberOfLeadsReturned__c = locations.size();
            insert llMetrics;                     
        }
        else if(isCloverLeaf)
        {   
                PageReference pageRef;             
                if(locations != null && locations.size() > 0)
                {
                        Integer countOfCriteria=0;              
                        for(MapItem item : locations)
                        {                               
                                if(countOfCriteria < pss.CloverleafMaxItemLimit__c - 1)
                                {
                                        if (countOfCriteria!=0) 
                                                {
                                                        ssiIds += ',';
                                                }       
                                                if(ssiIds != null && ssiIds != '')
                                                {
                                                        ssiIds += item.rId;     
                                                }       
                                                else
                                                {
                                                        ssiIds = item.rId;
                                                }       
                                }                                                               
                                        countOfCriteria++;                                      
                        }
                        if(!ssiIds.contains(','))
                        {
                                ssiIds += ',';
                        }       
                        pageRef = SaveStreetSheet();                    
                }
                else
                {
                        isError = true;
                        pageRef = null;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'There are no Prospects within 1 mile radius from the Account address, please launch the Locator tool.');
                        ApexPages.addMessage(myMsg);                            
                }
                return pageRef;                 
        }
        
        return null;
    } 
    
    // Back To Account
    
    public PageReference backToAccount()
    {           
        PageReference pageRef = new PageReference('/'+mAid); 
        return pageRef;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    // prepareMapData()
    //------------------------------------------------------------------------------------------------------------------------------
    public void prepareMapData() {  
        mapDataPointsStr = '';
                for(MapItem mi : locations)
        {
                mapDataPointsStr += mi.asString();
                mapDataPointsStr += POINT_DELIMITER;       
        }
        
       System.debug('prepareMapData-----------mapDataPointsStr:' + mapDataPointsStr);
        
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    //setStartingPoint()
    //------------------------------------------------------------------------------------------------------------------------------
    public void setStartingPoint()
    { 
        if (mAid != 'null' && mAid != null)
        {
                String acctprefix = Schema.Sobjecttype.Account.getKeyPrefix();
                        String leadprefix = Schema.Sobjecttype.Lead.getKeyPrefix();
                        Account a;
                        Lead l;
                        
                        if (mAid.toLowerCase().startsWith(acctprefix.toLowerCase()))
                        {
                                // the start point is the location of the identified account
                                a = SearchManager.getAccount(mAid); 
                        }                                               
                        else if(mAid.toLowerCase().startsWith(leadprefix.toLowerCase()))
                        {
                                // the start point is the location of the identified account
                        l = SearchManager.getLead(mAid); 
                        }
                    
                if((a == null && l == null) || (a != null && (a.Latitude__c == null || a.Longitude__c == null || (a.Latitude__c == 0 && a.Longitude__c == 0))) || (l != null && (l.Latitude__c == null || l.Longitude__c == null || (l.Latitude__c == 0 && l.Longitude__c == 0))))
                {
                        GoogleGeoCodeResults result;
                        if(a != null && (a.Latitude__c == null || a.Longitude__c == null || (a.Latitude__c == 0 && a.Longitude__c == 0)))
                        {
                                result = GoogleGeoCode.GeoCodeAddress(a.SiteStreet__c + ' ,' + a.SiteCity__c + ' ,' + a.SiteCountryCode__c + ' ,' + a.SitePostalCode__c);       
                        }
                        else if(l != null && (l.Latitude__c == null || l.Longitude__c == null || (l.Latitude__c == 0 && l.Longitude__c == 0)))
                        {
                                result = GoogleGeoCode.GeoCodeAddress(l.SiteStreet__c + ' ,' + l.SiteCity__c + ' ,' + l.SiteCountryCode__c + ' ,' + l.SitePostalCode__c);       
                        }
                        
                        if(result!= null && result.AddressLat != null && result.AddressLon != null)
                        {
                                startingPoint = new MapItem(Decimal.valueOf(result.AddressLat), Decimal.valueOf(result.AddressLon));
                        }
                        else
                        {
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'This record cannot be geo-coded within the locator function, the system has logged this error and will report it to the appropriate personnel. Please utilize the following process to continue; enter the address without the street number within the search field.');
                        ApexPages.addMessage(myMsg);
                        startingPoint = null;
                        }
                
            } 
            else 
            {
                if(a != null)
                {
                        startingPoint = new MapItem(a);
                }  
                else if( l != null)
                {
                        startingPoint = new MapItem(l);
                }               
                //mark the starting point so Map can display it as a separate colored marker 
                startingPoint.rIsStartingPoint = true;                  
                }       
        }
   
        else if (mLat != 'null' && mLat != null && mLon != 'null' && mLon != null) {
                startingPoint = new MapItem(Decimal.valueOf(mLat), Decimal.valueOf(mLon));
                startingPoint.rAddress = mAddr;
                        
        }         
        else
        {
                startingPoint = null;
        }
            
    }
    

        //------------------------------------------------------------------------------------------------------------------------------
    // searchNearby()
    //------------------------------------------------------------------------------------------------------------------------------  
    public ProximitySearcher searcher{get;set;}

    public void searchNearby()
    {
        if (startingPoint!=null)
        {
            try
            {   
                if(searcher==null)
                    searcher = ProximitySearcherFactory.getSearcher(isSalesRep, isManager);
                
                locations = searcher.findNearby(startingPoint.rLat, startingPoint.rLon, Decimal.valueOf(distance), currUser, isSalesRep, isManager, isCloverLeaf); 

                
                Integer indexOfRefAccount;
                                
                                for(Integer i = 0; i < locations.size(); i++)
                                {
                                        if(isCloverLeaf && mAid != null && mAid != '' && locations[i].rId.contains(mAid))
                                        {
                                                indexOfRefAccount = i;
                                        }                               
                                }               
                                
                                if(isCloverLeaf && indexOfRefAccount != null)
                                {
                                        locations.remove(indexOfRefAccount);
                                }
                                if(isCloverLeaf && locations.size() > pss.CloverleafMaxItemLimit__c - 1)
                                {
                                        list<MapItem> tempLocationList = new list<MapItem>();
                                        for(Integer i = 0; i < pss.CloverleafMaxItemLimit__c - 1; i++)
                                        {
                                                tempLocationList.add(locations[i]);
                                        }
                                        locations.clear();
                                        locations.addAll(tempLocationList);
                                }
                                                        
                // call method to populate mapDataPointsStr which is used for the map
                prepareMapData();
                    
            }
            catch(Exception e){
                System.debug('searchNearby--------Exception occurred: ' + e.getMessage() + e.getStackTraceString());
                errorMessage = 'The system was unable to process your request.  Please share the following information with your system administrator: ' + e.getMessage() + e.getStackTraceString();
            }
      }     
      
      
    }     
    
    //------------------------------------------------------------------------------------------------------------------------------
    //getPLOptions()
    //------------------------------------------------------------------------------------------------------------------------------
    public List<SelectOption> getPLOptions()
    {
                List<SelectOption> retOpt = new List<SelectOption>();
                //list<StreetSheet__c> ss = new list<StreetSheet__c>();
                for(StreetSheet__c ss : [Select id, name from StreetSheet__c where OwnerId =: UserInfo.getUserId()])
                {
                        retOpt.add(new SelectOption(ss.id, ss.name));
                }
                return retOpt;
    }    
    
    
    //------------------------------------------------------------------------------------------------------------------------------
    //getDataSourceOptions()
    //------------------------------------------------------------------------------------------------------------------------------
    public List<SelectOption> getDataSourceOptions()
    {
                return PicklistHelper.getDataSourceValues(); 
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    //getDispositionOptions()
    //------------------------------------------------------------------------------------------------------------------------------
    public List<SelectOption> getDispositionOptions()
    {
        return PicklistHelper.getDispositionCodeValues();  
            
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------
    //getRepAssignedOptions()
    //------------------------------------------------------------------------------------------------------------------------------
    public List<SelectOption> getRepAssignedOptions()
    {
        Map<Id, User> AllAvailableSalesRepsForManager = new Map<Id, User>();
        
        //get the user id and the related towns that the user/manager is assigned to
        Id currentUserRoleId = UserInfo.getUserRoleId();
        
        List<Id> matrixedRoles = new List<Id>();
        matrixedRoles = Utilities.getMatrixedRolesForManager(currentUserRoleId);
        List<Id> allRoles = new List<Id>();
        allRoles.add(currentUserRoleId);
        if(matrixedRoles != null)
            allRoles.addAll(matrixedRoles);
        
        //get all subordinate roles for the user
        List<UserRole> subRoles = [select id, name from UserRole where parentRoleId =: allRoles];
        List<id> allSubordinateRoleIds = new List<Id>();
        for(UserRole ur: subRoles)
        {
            allSubordinateRoleIds.add(ur.id);
        }

        //get all salesreps for the manager
        List<User> allSubordinateUsers = [select id, name, IsActive from User where UserRoleId in : allSubordinateRoleIds];
        for(User u : allSubordinateUsers)
        {
            AllAvailableSalesRepsForManager.put(u.id, u);
        }


        List<SelectOption> SalesReps = new List<SelectOption>(); 
        SalesReps.add(new SelectOption('All','All'));
        
        SelectOption anOption;
        User u;
        String Label;
        for(Id uid : AllAvailableSalesRepsForManager.KeySet())
        {
            u = AllAvailableSalesRepsForManager.get(uid);
            if(u.IsActive)
            {
                Label = u.Name;
                anOption = new SelectOption(u.id, Label);
                SalesReps.add(anOption);
            }
        } 
        return SalesReps;
         
    }
    
 
    //------------------------------------------------------------------------------------------------------------------------------
    //setDiscoReasonOptions()
    //------------------------------------------------------------------------------------------------------------------------------
    public List<SelectOption> getDiscoReasonOptions()
    {
        return PicklistHelper.getDiscoReasonValues();  
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    //setNewMoverOptions()
    //------------------------------------------------------------------------------------------------------------------------------
    public List<SelectOption> getNewMoverOptions()
    {
        return PicklistHelper.getNewMoverTypeValues();   
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    //goBackToRecord()
    //------------------------------------------------------------------------------------------------------------------------------
    public PageReference goBackToRecord(){
        return new PageReference('/'+startingPoint.rId);   
    }
   
    
    //------------------------------------------------------------------------------------------------------------------------------
    //    Creates drop down of Distances 
    //------------------------------------------------------------------------------------------------------------------------------
    public List<SelectOption> getDistances()
    { 
        List<SelectOption> distances;
        
        if (isManager || isSalesExec || isSysAdmin)
        {
            distances = new List<SelectOption>{new SelectOption('1','1'),
                        new SelectOption('2','2'),new SelectOption('3','3'), new SelectOption('4','4'),
                    new SelectOption('5','5'),new SelectOption('10','10')
            }; 
        }
        else
        {
            distances = new List<SelectOption>{new SelectOption('1','1'),
                    new SelectOption('2','2'),new SelectOption('3','3'), new SelectOption('4','4'),
                    new SelectOption('5','5')
            }; 
        }
        
        return distances;
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------
    //Get the options to populate the number of leads selectList
    //------------------------------------------------------------------------------------------------------------------------------
    public List<SelectOption> getLeadNums()
    { 
        List<SelectOption> numLeads;
        
        if (isManager || isSalesExec || isSysAdmin)
        {
                 numLeads = new List<SelectOption>{
                                                                                        new SelectOption('10','10'), new SelectOption('25','25'),
                                                                                new SelectOption('50','50'),new SelectOption('75','75'), new SelectOption('100','100')
                                                                                   };   
        }
        else
        {
                numLeads = new List<SelectOption>{
                                                                                        new SelectOption('10','10'), new SelectOption('25','25'),
                                                                                new SelectOption('50','50')};           
        }        
      return numLeads;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    //SaveStreetSheet()
    //------------------------------------------------------------------------------------------------------------------------------
    public PageReference SaveStreetSheet()
    {
        
        Set<String> streetSheetItemsSet = new Set <String>();
                
                //parse the string of comma separated Street Sheet Items
        List<String> tempString = ssiIds.split(',');  
        for(string t : tempString){
            if(t!=null && t.length()>=15 && t.length()<=18){
                streetSheetItemsSet.add(t);
            }
        }
        System.debug(ssiIds);
                System.debug(streetSheetItemsSet);
        String streetSheetId = null;
        try 
        {
                processProspectsOwnership(streetSheetItemsSet);
                
                if(isCloverLeaf)
                {
                        String streetSheetName = 'Nearby ( ' + SearchManager.getAccount(mAid).Name + ' ) - ' + System.now();
                        streetSheetId = StreetSheetManager.create(streetSheetName, streetSheetItemsSet);
                }
                else
                {
                        streetSheetId = StreetSheetManager.create(streetSheetItemsSet); 
                }                               
        }
        catch (Exception e) 
        {
                System.debug('Exception creating a Street Sheet: ' + e.getMessage());
                return new PageReference('/apex/ShowError?Error=DATAERROR');
        }
        PageReference pr = new PageReference ('/'+ streetSheetId);
        pr.setRedirect(true);
        return pr;
        
    }  
    
    //------------------------------------------------------------------------------------------------------------------------------
    //SaveToExistingStreetSheet()
    //------------------------------------------------------------------------------------------------------------------------------
    public PageReference SaveToExistingStreetSheet()
    {
                Set<String> streetSheetItemsSet = new Set <String>();
                
                //parse the string of comma separated Street Sheet Items
        List<String> tempString = ssiIds.split(',');  
        for(string t : tempString){
            if(t!=null && t.length()>=15 && t.length()<=18){
                streetSheetItemsSet.add(t);
            }
        }
        try 
        {
                processProspectsOwnership(streetSheetItemsSet);
                StreetSheetManager.addItems(SelectedPLValue, streetSheetItemsSet);                      
        }
        catch (Exception e) 
        {
                System.debug('Exception creating a Street Sheet: ' + e.getMessage());
                return new PageReference('/apex/ShowError?Error=DATAERROR');
        }
        PageReference pr = new PageReference ('/'+ SelectedPLValue);
        pr.setRedirect(true);
        return pr;
    }    
    
    //------------------------------------------------------------------------------------------------------------------------------
    //processAddressEntered()
    //------------------------------------------------------------------------------------------------------------------------------
    
    public PageReference processAddressEntered()
    {
        PageReference pr = new PageReference ('/apex/LeadLocatorPreProcessor?addr='+ addressEntered);
        return pr;
    }
    
    // processProspectsOwnership
    public void processProspectsOwnership(Set<String> itemIdSet)
    {
        String acctprefix = Schema.Sobjecttype.Account.getKeyPrefix();
                String leadprefix = Schema.Sobjecttype.Lead.getKeyPrefix();
                set<Id> accountIds = new set<Id>();
                set<Id> leadIds = new set<Id>();
                list<Account> accountsForOwnershipChange = new list<Account>();
                list<Lead> leadsForOwnershipChange = new list<Lead>();              
                                
                for(String itemId : itemIdSet)
                {       
                        if (itemId.toLowerCase().startsWith(acctprefix.toLowerCase()))
                                accountIds.add(itemId);
                        else if(itemId.toLowerCase().startsWith(leadprefix.toLowerCase()))
                                leadIds.add(itemId);                            
            }
            
            // Reset the fields on an Account where the current user is not the owner.
            if(!accountIds.isEmpty())
            {
                for(Account a : [SELECT Id, Name, OwnerId, DateAssigned__c, 
                                                                PreviousOwner__c, NewLead__c, UnassignedLead__c
                                                                        FROM Account
                                                                        WHERE Id IN :accountIds AND OwnerId !=: currUser.Id])
                {
                        a.DateAssigned__c = System.now();
                        a.NewLead__c = true;                    
                        a.PreviousOwner__c = a.OwnerId;
                        a.OwnerId = currUser.Id;
                        
                        if(isManager)
                        {
                                a.UnassignedLead__c = true;
                        }
                        else
                        {
                                a.UnassignedLead__c = false;
                        }               
                        accountsForOwnershipChange.add(a);      
                }                                                                        
                        update accountsForOwnershipChange;
            }
            
            // Reset the fields on a Lead where the current user is not the owner.
            if(!leadIds.isEmpty())
            {
                for(Lead l : [SELECT Id, Name, OwnerId, DateAssigned__c, 
                                                         PreviousOwner__c, NewLead__c, UnassignedLead__c
                                                         FROM Lead
                                                         WHERE Id IN :leadIds AND IsConverted = false AND OwnerId !=: currUser.Id])
                {
                        l.DateAssigned__c = System.now();                       
                        l.NewLead__c = true;
                        l.PreviousOwner__c = l.OwnerId;
                        l.OwnerId = currUser.Id;
                        
                        if(isManager)
                        {
                                l.UnassignedLead__c = true;
                        }
                        else
                        {
                                l.UnassignedLead__c = false;
                        }
                                leadsForOwnershipChange.add(l);
                }
                        update leadsForOwnershipChange;
            }
            
    }

}