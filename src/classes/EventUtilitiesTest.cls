/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EventUtilitiesTest {
    

    
    static testMethod void testCancelEventController() {
        Event e;
        User u;
        Account a;
        Event ecg;
        User current = [select Id from User where ID = :UserInfo.getUserId()];
        system.debug(UserInfo.getUiThemeDisplayed());
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            a.OwnerId = u.Id;
            update a;
            e = TestHelperClass.createEvent(a, u, false);
            ecg = TestHelperClass.createEvent(a, u, false);
            RecordType rt = Utilities.getRecordType( RecordTypeName.SALESFORCE_ONLY_EVENT, 'Event' );
            e.RecordTypeId = rt.Id;
            RecordType rtcg = Utilities.getRecordType( RecordTypeName.COMPANY_GENERATED_APPOINTMENT, 'Event' );
            ecg.RecordTypeId = rtcg.Id;
            ecg.Status__c = EventManager.STATUS_SCHEDULED;
            insert e;
            insert ecg;
            
        }
        
        Id eid = e.Id;
        
        test.setCurrentPage(Page.EventCancel);
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(e);
                EventCancelController ec = new EventCancelController(sc);
                ec.cancelEvent();
                
                sc = new ApexPages.Standardcontroller(ecg);
                ec = new EventCancelController(sc);
                ec.cancelEvent();
            }
        test.stopTest();
        
        List<Event> events = new List<Event>([select Id, Status__c from Event where Id = :eid or Id = :ecg.Id]);
        system.assertEquals(1, events.size());
        system.assertEquals( EventManager.STATUS_CANCELED, events[0].Status__c);
    } 
    static testMethod void testValidatePostalCode() {
        String us = '44131';
        String usaddon = '44131-2540';
        String ca = 'K1A 0B1';
        
        system.assert( EventUtilities.validatePostalCode(us) );
        system.assert( EventUtilities.validatePostalCode(usaddon) );
        system.assert( EventUtilities.validatePostalCode(ca) );
    }   
    static testMethod void testDeriveTimeZoneOffset() {
        
        String AmericaNewYork = 'America/New_York'; 
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveTimeZoneOffset(AmericaNewYork);
        System.assert(offset == '-0400' || offset == '-0500', 'Offset for New York should be -4 or -5');
        
        
        Test.stopTest();
        
        
    }
    
    
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ1() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'America/St_Johns');
        System.assert(offset == '-0230' || offset == '-0330', 'Offset for St Johns should not be: ' + offset);
        
        Test.stopTest();
        
        
    }
        
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ2() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'America/Halifax');
        System.assert(offset == '-0300' || offset == '-0400', 'Offset for Halifax should not be: ' + offset);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ3() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'America/New_York');
        System.assert(offset == '-0400' || offset == '-0500', 'Offset for New York should not be: ' + offset);
        
        Test.stopTest();
        
        
    }   
    
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ4() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'America/Indiana/Indianapolis');
        System.assert(offset == '-0400' || offset == '-0500', 'Offset for Indianapolis should not be: ' + offset);
        
        Test.stopTest();
        
        
    }   
    
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ5() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'America/Puerto_Rico');
        System.assertEquals(offset, '-0400', 'Offset for Puerto Rico should not be: ' + offset);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ6() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'America/Chicago');
        System.assert(offset == '-0500' || offset == '-0600', 'Offset for Chicago should not be: ' + offset);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ7() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'America/Denver');
        System.assert(offset == '-0600' || offset == '-0700', 'Offset for Denver should not be: ' + offset);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ8() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'America/Los_Angeles');
        System.assert(offset == '-0700' || offset == '-0800', 'Offset for Los Angeles should not be: ' + offset);
        
        Test.stopTest();
        
        
    }   
    
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ9() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'America/Phoenix');
        System.assertEquals(offset, '-0700', 'Offset for Puerto Rico should not be: ' + offset);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ10() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'America/Anchorage');
        System.assert(offset == '-0800' || offset == '-0900', 'Offset for Anchorage should not be: ' + offset);
        
        Test.stopTest();
        
        
    }   
    
    static testMethod void testDeriveUserTimeZoneOffsetForDateTZ11() {
        
        String offset;
        
        Test.startTest();
        
        offset = EventUtilities.deriveUserTimeZoneOffsetForDate(Datetime.now(), 'Pacific/Honolulu');
        System.assertEquals(offset, '-1000', 'Offset for Puerto Rico should not be: ' + offset);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testDST() {
        list<String> tzones = new list<String>(EventUtilities.NO_DST_TIMEZONE_SID_KEYS);
        Integer numzones = tzones.size();
        User[] users = new User[numzones];
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            
            for (Integer i=0; i<users.size(); i++) {
               // users[i] = TestHelperClass.createUser(i);
               users[i] = [Select Id from User where Id = :UserInfo.getUserId()];
                users[i].TimeZoneSidKey = tzones[i];
            }
            
           // update users;
        }                           
    
        for (User u : users) {
            system.runas(u) {
                DateTime d1 = DateTime.valueof('2012-5-1 12:12:12');
                DateTime d2 = DateTime.valueof('2012-12-1 12:12:12');
                Integer d1hourgmt = d1.hourGmt();
                Integer d1mingmt = d1.minuteGmt();
                Integer d2hourgmt = d2.hourGmt();
                Integer d2mingmt = d2.minuteGmt();
                system.assertNotEquals(d1hourgmt, d2hourgmt, 'The time zone ' + u.TimeZoneSidKey + ' observes DST.');
                system.assertEquals(d1mingmt, d1mingmt,  'The time zone ' + u.TimeZoneSidKey + ' observes DST.');
            }
        }
    }
    
    static testMethod void testShouldSetTaskCodeForAccount() {
        
        String offset;
        
        Test.startTest();
        
        System.assert(EventUtilities.shouldSetTaskCodeForAccount(IntegrationConstants.TASK_CODE_RIF_VISIT, IntegrationConstants.TYPE_RIF), 'Should set for a Visit to RIF Customer Site');
        System.assert(!EventUtilities.shouldSetTaskCodeForAccount(IntegrationConstants.TASK_CODE_RIF_VISIT, IntegrationConstants.TYPE_OTHER), 'Should not set for a Visit to Type Other');
        System.assert(EventUtilities.shouldSetTaskCodeForAccount('TSK', IntegrationConstants.TYPE_OTHER), 'Should set for a Telemar task for Type Other');
        System.assert(EventUtilities.shouldSetTaskCodeForAccount('TSK', IntegrationConstants.TYPE_RIF), 'Not expected to occur but should set for a Telemar task for RIF Customer Site');
        
        
        Test.stopTest();
        
        
    }
}