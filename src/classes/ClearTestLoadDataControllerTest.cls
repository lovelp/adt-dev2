@istest
public class ClearTestLoadDataControllerTest{

    @testSetup 
    static void setup() {
        List<Archival_Data__c> archDataList = new List<Archival_Data__c>();
        
        
        List<Disposition__c> dsList = new List<Disposition__c>();
        List<Inbound_Request__c> irList = new List<Inbound_Request__c>();
        
        for (Integer i=0;i<10;i++) {
            dsList.add(new Disposition__c(DispositionType__c= 'Quote Created' + i , DispositionEmployeeNumber__c = '143576'));
        }
        
        for (Integer i=0;i<10;i++) {
            irList.add(new Inbound_Request__c(Name ='Test Inbound' + i));
        }
        
        insert dsList;
        insert irList;  
        
        //Dispositions
        Archival_Data__c d1 = new Archival_Data__c();
        d1.Name = 'Dispositons';
        d1.Object_API_Name__c = 'Disposition__c';
        d1.Criteria__c = 'CreatedDate = today';
        d1.Active__c = true;
        
        //Inbound Requests
        Archival_Data__c d2 = new Archival_Data__c();
        d2.Name = 'Inbound Requests';
        d2.Object_API_Name__c = 'Inbound_Request__c';
        d2.Criteria__c = 'CreatedDate = today';
        d2.Active__c = true;
        
        archDataList.add(d1);
        archDataList.add(d2);
        
        insert archDataList;
    }
    
    static testmethod void testController() { 
        
        
        ClearTestLoadDataController ct = new ClearTestLoadDataController(null);
        
        //after the testing starts, assert records were created properly
        //System.assertEquals(10, [select count() from Inbound_Request__c]);
               
        Test.startTest();
        
        ct.callBatchClass();
        
        Test.stopTest();
        
        //after the testing stops, assert records were deleted properly
        //System.assertEquals(0, [select count() from Inbound_Request__c]);
    }


}