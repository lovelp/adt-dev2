public with sharing class EquifaxCustomSettingController {
    
    public EquifaxCustomSettingController(ApexPages.StandardController ctl){ }
    
    public List<EquifaxCreditScoreMapping__c> getEquifaxMapping() {
        List<EquifaxCreditScoreMapping__c> equiFaxMappingList = [SELECT name, MappedCreditScore__c FROM EquifaxCreditScoreMapping__c ORDER BY name ASC];
        return equiFaxMappingList;
    }
}