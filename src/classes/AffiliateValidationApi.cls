/************************************* MODIFICATION LOG ********************************************************************************************
* AffiliateValidationApi
*
* DESCRIPTION : Provides API to make a REST webservice call to USAA
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Leonard Alarcon               01/27/2016            USAA SUPPORT
*                                                   
*/
public class AffiliateValidationApi {        
    /********* (USAA) credentials ***********/
    private static String USAA_SERVICE_ENDPOINT {
        get{
            return IntegrationSettings__c.getinstance().USAA_Endpoint__c;
        }
    }
        
    private static Integer USAA_SERVICE_TIMEOUT {
        get{
            return IntegrationSettings__c.getinstance().USAA_Timeout__c.intValue();
        }
    }
        
    private static String USAA_SERVICE_USERNAME {
        get{
            return IntegrationSettings__c.getinstance().USAA_Username__c;
        }
    }
        
    private static String USAA_SERVICE_PASSWORD {
        get{
            return IntegrationSettings__c.getinstance().USAA_Password__c;
        }
    }
    /********* (USAA) credentials ***********/
    
    /**
    ** USAA Advance Search with Member Number
    **/
    public String ValidateUSAAMembership(String MemberId, String LastName){
        String encodedLName = EncodingUtil.urlEncode(LastName, 'UTF-8');
        return sendUSAARequest(USAA_SERVICE_ENDPOINT +'?membernumber='+ MemberId +'&lastname='+ encodedLName);
    }
    
    /**
    ** USAA Advance Search without Member Number
    **/
    public String ValidateUSAAMembershipAdvance(String FirstName, String LastName, String birthdate, String phonenumber, String city, String state, String zipcode, String email){
        
        String sEndpoint = '';
        String encodedLName = EncodingUtil.urlEncode(LastName, 'UTF-8');
        String encodedFName = EncodingUtil.urlEncode(FirstName, 'UTF-8');
        String encodedDob = EncodingUtil.urlEncode(birthdate, 'UTF-8');
        
        // First Name, Last Name & Dob is Mandatory
        sEndpoint = USAA_SERVICE_ENDPOINT + '?lastname='+ encodedLName +'&firstname='+ encodedFName +'&birthdate=' + encodedDob;
        
        if(!String.isBlank(phonenumber))
            sEndpoint = sEndpoint + '&phonenumber=' + EncodingUtil.urlEncode(phonenumber, 'UTF-8');
        
        if(!String.isBlank(city))
            sEndpoint = sEndpoint + '&city=' + EncodingUtil.urlEncode(city, 'UTF-8');
        
        if(!String.isBlank(state))
            sEndpoint = sEndpoint + '&state=' + EncodingUtil.urlEncode(state, 'UTF-8');
        
        if(!String.isBlank(zipcode))
            sEndpoint = sEndpoint + '&zipcode=' + EncodingUtil.urlEncode(zipcode, 'UTF-8');
        
        if(!String.isBlank(email))
            sEndpoint = sEndpoint + '&email=' + EncodingUtil.urlEncode(email, 'UTF-8');

        
  //ApexPages.Message errorMsg2 = new ApexPages.Message(ApexPages.Severity.INFO,'ENDPOINT = ' + sEndpoint);
  //ApexPages.addMessage(errorMsg2);
        
        return sendUSAARequest(sEndpoint);
    }
    
    private String sendUSAARequest(String EndpointUrl){
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        String sCredentials = USAA_SERVICE_USERNAME + ':' + USAA_SERVICE_PASSWORD;
        Integer Timeout = USAA_SERVICE_TIMEOUT;
        String sResponse = '';
        String sStatusCode = '';
        String sUserID = UserInfo.getUserId();
        
        request.setMethod('GET');
        request.setEndPoint(EndpointUrl);
        request.setTimeout(Timeout);
        
        Blob headerValue = Blob.valueOf(sCredentials);        
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Account-Id', sUserID);

        try{
            response = http.send(request);
            System.debug('USAA Server Response:' + response.getBody());
            System.debug('Response Status Code:' + response.getStatusCode());           

            JSONParser parser = JSON.createParser(response.getBody());           
            while (parser.nextToken() != null){
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'memberNumber')){
                    parser.nextToken();
                    sResponse = 'Success:' + parser.getText();
                }
                
                //********* Modified by Sangeetha Ashokan - 4/19/2017 to get 'Military Rank' from the response
                 if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'militaryRank')){
                    parser.nextToken();
                    sResponse += ':MilitaryRank:' +parser.getText();
                }
                
                //***********************************************************************************************

                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'status')){
                    parser.nextToken();
                    sStatusCode = parser.getText();
                }

                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'errorMessage')){
                    parser.nextToken();
                    sResponse = parser.getText();
                }                   
            }           
            
            String callBackData = '';
            if (sResponse == 'Success'){
                callBackData = 'Success';
            } 
            else{
                callBackData = sStatusCode+':'+sResponse;
            }
            return callBackData;
        }
        catch(Exception err){
            ADTApplicationMonitor.log(err, 'AffiliateValidateionApi', 'sendUSAARequest', ADTApplicationMonitor.CUSTOM_APP.MATRIX);
            system.debug('USAA error :' + response.toString());
            system.debug('Err: '+ err.getMessage());
            system.debug('Trace: '+ err.getStackTraceString());
        }                
        return null;
    }
}