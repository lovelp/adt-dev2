/************************************* MODIFICATION LOG ********************************************************************************************
 *
 * DESCRIPTION : ADTFICOGateway.cls has methods to communicate with the equifax api
 *
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                  REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Magdiel Herrera               03/22/2016          - Original Version
 * Siddarth Asokan               12/09/2016          - Added ConsumerScoreInfo
 * Srini                         12/26/2018          - Added Organization code for soft and hard credit
 */
 
public without sharing class ADTFICOGateway {
    
    //---------------------------------------------------------------------------------------
    //  Credit Check information used on Equifax requests
    //---------------------------------------------------------------------------------------
    public class CCInfo {
        public String address1;
        public String address2;
        public String city;
        public String state;
        public String zip;
        public String prev_address1;
        public String prev_address2;
        public String prev_city;
        public String prev_state;
        public String prev_zip;
        public String ssnNo;
        public String ssn4No;
        public Date DOB;        
        //Added by Srini for PartnerID, RepName for RV_PartnerCreditCheck - HRM 6019
        public String partnerId;
        public String repName;
        //Added by Srini for new parameter
        public String organizationCode;
    }
    
    private CCInfo CreditCheckInformation;
    private Account a;
    public Boolean isSoftCheck = false;
    public String errorMsg;
    
    /**
     *  @Constructor
     */ 
    public ADTFICOGateway(){}
    public ADTFICOGateway(Account acc, CCInfo cInf ){
        this.CreditCheckInformation = cInf;
        this.a = acc;
    }
    
    /**
     *  Set customer personal information to be used by Equifax
     *
     *  @method setCreditCheckInformation
     *  
     *  @param  cInf    Personal information for this customer encapsulated on CCInfo class
     *
     */
    public void setCreditCheckInformation( CCInfo cInf ){
        this.CreditCheckInformation = cInf;
    }
    
    /**
     *  Set customer salesforce account to be used by the request while populating values on this Gateway
     *
     *  @method setCreditCheckAccount
     *  
     *  @param  acc     Salesforce account used on this Gateway to populate request information
     *
     */
    public void setCreditCheckAccount(Account acc){
        this.a = acc;
    }
    
    public ADTFICOAPI.doConsumerScoreInfoResp ConsumerScoreInfo(){
        ADTFICOAPI.EquifaxAPISOAP FicoSrv = new ADTFICOAPI.EquifaxAPISOAP();
        ADTFICOAPI.doConsumerScoreInfoReq doConsumerScoreInfoReq = createConsumerScoreRequest();
        if(String.isNotBlank(errorMsg)){
       		// Write to ADT Logs
       		ADTApplicationMonitor.log ('Credit check failed', errorMsg, true, ADTApplicationMonitor.CUSTOM_APP.HERMES);
       		throw new CalloutException( errorMsg );
       	}else{
	        ADTFICOAPI.doConsumerScoreInfoResp CCRes = FicoSrv.doConsumerScoreInfo( doConsumerScoreInfoReq ); 
	        logEquifaxResponse( CCRes );
	        if( !CCRes.isSuccess ){
	            throw new IntegrationException( CCRes.ErrorMessage );
	        }
	        return CCRes;
        }
        return null;
    }
    
    private ADTFICOAPI.doConsumerScoreInfoReq createConsumerScoreRequest(){
        ADTFICOAPI.doConsumerScoreInfoReq reqRes = new ADTFICOAPI.doConsumerScoreInfoReq();
        // Error Handling
        errorMsg = '';
        if(String.isBlank(a.FirstName__c)){
       		errorMsg += 'First name is missing. ';
       	}
       	if(String.isBlank(a.TelemarAccountNumber__c)){
       		errorMsg += 'Telemar number is missing. ';
       	}
       	if(String.isBlank(CreditCheckInformation.address1)){
       		errorMsg += 'Street address is missing. ';
       	}
       	if(String.isBlank(CreditCheckInformation.city)){
       		errorMsg += 'City is missing. ';
       	}
       	if(String.isBlank(CreditCheckInformation.state)){
       		errorMsg += 'State is missing. ';
       	}
       	if(String.isBlank(CreditCheckInformation.zip)){
       		errorMsg += 'Postal Code is missing. ';
       	}
		if(String.isNotBlank(errorMsg)){
			errorMsg = 'Credit check failed for Account: '+ a.Id+ '. Errors are as follows: '+ errorMsg;
			return null;
		}
        // String messageId;
        reqRes.account = a.TelemarAccountNumber__c;
        reqRes.firstName = a.FirstName__c;
        reqRes.lastName = a.LastName__c;
        // Current Addr
        reqRes.address1 = CreditCheckInformation.address1;
        reqRes.address2 = CreditCheckInformation.address2;
        reqRes.city = CreditCheckInformation.city;
        reqRes.state = CreditCheckInformation.state;
        reqRes.zip = CreditCheckInformation.zip;
        // Previous Addr
        reqRes.priorAddress1 = CreditCheckInformation.prev_address1;
        reqRes.priorAddress2 = CreditCheckInformation.prev_address2;
        reqRes.priorCity = CreditCheckInformation.prev_city;
        reqRes.priorState = CreditCheckInformation.prev_state;
        reqRes.priorZip = CreditCheckInformation.prev_zip;
        reqRes.ssn4 = ( !String.isBlank( CreditCheckInformation.ssnNo ) )? CreditCheckInformation.ssnNo : CreditCheckInformation.ssn4No;
        // To throw fault error use the below dob
        //reqRes.dob = CreditCheckInformation.DOB.month() + ''+ CreditCheckInformation.DOB.day() + '' + CreditCheckInformation.DOB.year();
        // Using xsd date format [YYYY-MM-DD]
        dateTime dobDate = dateTime.newInstance(CreditCheckInformation.DOB.year(), CreditCheckInformation.DOB.month(), CreditCheckInformation.DOB.day());
        reqRes.dob = dobDate.format('yyyy-MM-dd');
        //Added by Srini for PartnerID, RepName for RV_PartnerCreditCheck - HRM 6019
        reqRes.partnerID = CreditCheckInformation.partnerID;
        reqRes.repName = CreditCheckInformation.repName;
        //Added by Srini for new requirement.
        if(String.isNotBlank(CreditCheckInformation.organizationCode)){
            reqRes.organizationCode = CreditCheckInformation.organizationCode;
            isSoftCheck = true;
        }
        
        System.debug('Date after XSD Date formating:'+reqRes.dob);
        system.debug('####'+reqRes);
        return reqRes;
    }
    
    private void logEquifaxResponse( ADTFICOAPI.doConsumerScoreInfoResp ConsumerRes){
        Equifax_Log__c eLogObj = new Equifax_Log__c();
        String logName;
        if(!String.isBlank(ConsumerRes.repName)){
            logName =  ConsumerRes.repName;
        }
        else if(!String.isBlank(ConsumerRes.partnerId)){
            logName = ConsumerRes.partnerId;
        }
        else{
            logName = UserInfo.getName();
         }
        eLogObj.Name = 'Credit_' + logName + '_' + DateTime.now().format();
        eLogObj.AccountID__c = a.Id;
        eLogObj.Address__c = a.AddressId__c;
        eLogObj.Partner_ID__c = ConsumerRes.partnerId;
        eLogObj.Partner_User__c = ConsumerRes.repName;
        eLogObj.Adverse_Action_Codes__c = '';
        for( String adverseCode: ConsumerRes.ccIcReasonForScore ){
            eLogObj.Adverse_Action_Codes__c += adverseCode + ';';
        }
        eLogObj.Adverse_Action_Codes__c = eLogObj.Adverse_Action_Codes__c.substringBeforeLast(';');
        eLogObj.TransactionID__c = ConsumerRes.ccIcTransactionId;
        eLogObj.Interaction_ID__c = ConsumerRes.ccIcInteractionId;
        if(String.isNotBlank(ConsumerRes.ccIcFileSinceDate))
            eLogObj.File_Since_Date__c = date.newInstance(Integer.ValueOf(ConsumerRes.ccIcFileSinceDate.substring(4,8)),Integer.ValueOf(ConsumerRes.ccIcFileSinceDate.substring(0,2)),Integer.ValueOf(ConsumerRes.ccIcFileSinceDate.substring(2,4)));
        if(String.isNotBlank(ConsumerRes.ccIcLastActivityDate))
            eLogObj.Last_Activity_Date__c = date.newInstance(Integer.ValueOf(ConsumerRes.ccIcLastActivityDate.substring(4,8)),Integer.ValueOf(ConsumerRes.ccIcLastActivityDate.substring(0,2)),Integer.ValueOf(ConsumerRes.ccIcLastActivityDate.substring(2,4)));        
        
        // Hit Code & Reject Code Logic
        if(String.isNotBlank(ConsumerRes.ccIcHitCode)){
            eLogObj.Hit_Code__c = ConsumerRes.ccIcHitCode;
            eLogObj.Hit_Code_Description__c = ConsumerRes.ccIcHitDesc;
            for(Equifax_Mapping__c eMap: [Select HitCode__c, HitCodeResponse__c, RiskGrade__c, ApprovalType__c, CreditScoreStartValue__c, CreditScoreEndValue__c from Equifax_Mapping__c Where HitCode__c =:ConsumerRes.ccIcHitCode]){
                if(eMap.HitCode__c == ConsumerRes.ccIcHitCode){
                    if(ConsumerRes.ccIcHitCode == '1' || ConsumerRes.ccIcHitCode == '6'){
                        if(String.isNotBlank(ConsumerRes.ccIcCreditScore) && String.isNotBlank(eMap.CreditScoreStartValue__c) && String.isNotBlank(eMap.CreditScoreEndValue__c) && Integer.valueOf(ConsumerRes.ccIcCreditScore) >= Integer.valueOf(eMap.CreditScoreStartValue__c) && Integer.valueOf (ConsumerRes.ccIcCreditScore) <= Integer.valueOf(eMap.CreditScoreEndValue__c)){
                            eLogObj.Approval_Type__c = eMap.ApprovalType__c;
                            eLogObj.ADT_Risk_Grade__c = eMap.RiskGrade__c;
                        }
                        else{
                            if(String.isNotBlank(ConsumerRes.ccIcRejectCode) && ConsumerRes.ccIcRejectCode == eMap.HitCodeResponse__c){
                                eLogObj.Reject_Code__c = ConsumerRes.ccIcRejectCode;
                                eLogObj.Reject_Code_Description__c = ConsumerRes.ccIcRejectDesc;
                                eLogObj.Approval_Type__c = eMap.ApprovalType__c;
                                eLogObj.ADT_Risk_Grade__c = eMap.RiskGrade__c;
                            }
                        }
                    }else{
                        eLogObj.Approval_Type__c = eMap.ApprovalType__c;
                        eLogObj.ADT_Risk_Grade__c = eMap.RiskGrade__c;
                    }
                }
            }
        }
        // For Failure we assign the defaults for failure risk grade & approval type
        if(!ConsumerRes.isSuccess){
            if(Equifax__c.getinstance('Failure Condition Code') != null)
                eLogObj.Approval_Type__c = Equifax__c.getinstance('Failure Condition Code').value__c;
            if(Equifax__c.getinstance('Failure Risk Grade') != null)
                eLogObj.ADT_Risk_Grade__c = Equifax__c.getinstance('Failure Risk Grade').value__c;  
        }
        
        if(String.isNotBlank(eLogObj.Approval_Type__c)){
            Equifax_Conditions__c equiCond = Equifax_Conditions__c.getValues(eLogObj.Approval_Type__c);
            eLogObj.Deposit_Percent__c = equiCond.Deposit_Required_Percentage__c;
            eLogObj.Three_pay_Indicator__c = equiCond.Three_Pay_Allowed__c;
            eLogObj.Easy_Pay_Indicator__c = equiCond.EasyPay_Required__c;
            eLogObj.Payment_Frequency__c = equiCond.Payment_Frequency__c;
        }
        if(String.isNotBlank(ConsumerRes.ccIcCreditScore)){
            for(EquifaxCreditScoreMapping__c equiMap: [Select Name, MappedCreditScore__c from EquifaxCreditScoreMapping__c]){
                if(Integer.valueOf(ConsumerRes.ccIcCreditScore) >= Integer.valueOf(equiMap.Name.substringBefore(':')) && Integer.valueOf(ConsumerRes.ccIcCreditScore) <= Integer.valueOf(equiMap.Name.substringAfter(':')))
                    eLogObj.Translated_credit_score__c = equiMap.MappedCreditScore__c;
            }
        }
        eLogObj.Date_Time__c = DateTime.now();
        eLogObj.Name__c = a.Name;
        eLogObj.Full_SSN_Used__c = !String.isBlank(CreditCheckInformation.ssnNo);
        eLogObj.Equifax_Last_Four__c = CreditCheckInformation.ssn4No;
        eLogObj.Type__c = 'Credit';
        eLogObj.User__c = UserInfo.getUserId();
        eLogObj.XML_Response__c = ConsumerRes.XML_RESPONSE;
        // Credit Score will not be stored in Equifax Logs
        //eLogObj.Credit_Score__c = ConsumerRes.ccIcCreditScore;
        if(String.isNotBlank(ConsumerRes.ccIcCreditScore))
            eLogObj.XML_Response__c = ConsumerRes.XML_RESPONSE.substring(0,ConsumerRes.XML_RESPONSE.indexOf(ConsumerRes.ccIcCreditScore)) + ConsumerRes.XML_RESPONSE.substring(ConsumerRes.XML_RESPONSE.indexOf(ConsumerRes.ccIcCreditScore)+3);
        else
            eLogObj.XML_Response__c = ConsumerRes.XML_RESPONSE;
        //Added by Srinbi - HRM - 6109
        eLogObj.Partner_ID__c = ConsumerRes.partnerId;
        eLogObj.Partner_User__c =  ConsumerRes.repName;
        eLogObj.IsSoftCheck__c = isSoftCheck;
        insert eLogObj;
    }
}