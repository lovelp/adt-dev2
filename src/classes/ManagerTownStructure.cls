/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : ManagerTownStructure.cls is a wrapper class to save and manupulate a list of manager-town relationship records. This is 
*				used on the managerTown visualforce page to load initial state, maintain intermediate state and to save the final state.
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli				02/05/2012			- Origininal Version
*
*													 
*/

public class ManagerTownStructure
{
	private final String BUSINESSID_RESIDENTIAL = '1100';
	private final String BUSINESSID_SMALLBUSINESS = '1200';
	private final String BUSINESSID_COMMERCIALBUSINESS = '1300';
	
	public Id ManagerTownId {get; set;}
	public String TownId {get; set;}
	public String TownName {get; set;}
	public String BusinessId {get; set;}
	public Boolean isManagerActive {get; set;}
	//variables to hold the checkbox values for each type
	public Boolean isResiDirect {get; set;}
	public Boolean isSBDirect {get; set;}
	public Boolean isResiResale {get; set;}
	public Boolean isSBResale {get; set;}
	public Boolean isCustomHomeSales {get; set;}
	public Boolean isHomeHealth {get; set;}
	//public Boolean isUSAAResiDirect {get; set;}
	//public Boolean isBuilder {get; set;}

	public Boolean isResiDirectPhoneSales {get; set;}
	public Boolean isSBDirectPhoneSales {get; set;}
	public Boolean isResiResalePhoneSales {get; set;}
	public Boolean isSBResalePhoneSales {get; set;}
	
	public Boolean isCBDirect  {get; set;}
	public Boolean isCBNational  {get; set;}

	//variables to indicate if a type should be shown
	public Boolean enableResiDirect {get; set;}
	public Boolean enableSBDirect {get; set;}
	public Boolean enableResiResale {get; set;}
	public Boolean enableSBResale {get; set;}
	public Boolean enableCustomHomeSales {get; set;}
	public Boolean enableHomeHealth {get; set;}
	//public Boolean enableUSAAResiDirect {get; set;}
	//public Boolean enableBuilder {get; set;}	

	public Boolean enableResiDirectPhoneSales {get; set;}
	public Boolean enableSBDirectPhoneSales {get; set;}
	public Boolean enableResiResalePhoneSales {get; set;}
	public Boolean enableSBResalePhoneSales {get; set;}
	//public Boolean enableBuilderPhoneSales {get; set;}
	
	public Boolean enableCBDirect  {get; set;}
	public Boolean enableCBNational  {get; set;}
	
	public ManagerTownStructure()
	{
		isResiDirect = false;
		isSBDirect = false;
		isResiResale = false;
		isSBResale = false;
		isCustomHomeSales = false;
		isResiDirectPhoneSales = false;
		isSBDirectPhoneSales = false;
		isResiResalePhoneSales = false;
		isSBResalePhoneSales = false;
		isHomeHealth = false;
		isCBDirect = false;
		isCBNational = false;
		//isUSAAResiDirect = false;
		//isBuilder = false;
		
		enableResiDirect = false;
		enableSBDirect = false;
		enableResiResale = false;
		enableSBResale = false;
		enableCustomHomeSales = false;
		enableResiDirectPhoneSales = false;
		enableSBDirectPhoneSales = false;
		enableResiResalePhoneSales = false;
		enableSBResalePhoneSales = false;
		enableHomeHealth = false;
		enableCBDirect = false;
		enableCBNational = false;
		//enableUSAAResiDirect = false;
		//enableBuilder = false;		
		
		isManagerActive = true;		
	}
	
	public void CheckAllAvailableTypes()
	{
		if(BusinessId == this.BUSINESSID_RESIDENTIAL)
		{
			isResiDirect = true;
			isResiResale = true;
			isCustomHomeSales = true;
			isResiDirectPhoneSales = true;
			isResiResalePhoneSales = true;
			isHomeHealth = true;
			//isUSAAResiDirect = true;
			//isBuilder = true;			
		}
		else if (BusinessId == BUSINESSID_SMALLBUSINESS)
		{
			isSBDirect = true;
			isSBResale = true;
			isSBDirectPhoneSales = true;
			isSBResalePhoneSales = true;					
		}else if (BusinessId == BUSINESSID_COMMERCIALBUSINESS)
		{
			isCBDirect = true;
			isCBNational = true;
			
			//isSBDirectPhoneSales = true;
			//isSBResalePhoneSales = true;					
		}
	}
	
	public boolean isValidMSI()
	{
		Boolean retVal = true;
		if(!isResiDirect && !isResiResale && !isCustomHomeSales && !isSBDirect && !isSBResale && !isHomeHealth && !isCBDirect){
			retVal = false;
		}
		return retVal;
	}
	
	public void enabletypes()
	{
		if(isManagerActive)
		{
			if(BusinessId == this.BUSINESSID_RESIDENTIAL)
			{
				enableResiDirect = true;
				enableResiResale = true;
				enableCustomHomeSales = true;
				enableResiDirectPhoneSales = true;
				enableResiResalePhoneSales = true;
				enableHomeHealth = true;
				//enableUSAAResiDirect = true;
				//enableBuilder = true;						
			}
			else if (BusinessId == BUSINESSID_SMALLBUSINESS)
			{
				enableSBDirect = true;
				enableSBResale = true;
				enableSBDirectPhoneSales = true;
				enableSBResalePhoneSales = true;					
			}else if (BusinessId == BUSINESSID_COMMERCIALBUSINESS)
			{
				enableCBDirect = true;
				enableCBNational = true;
			}
		}
		else
		{
			enableResiDirect = false;
			enableResiResale = false;
			enableCustomHomeSales = false;
			enableResiDirectPhoneSales = false;
			enableResiResalePhoneSales = false;	
			enableHomeHealth = false;
			//enableUSAAResiDirect = true;
			//enableBuilder = true;						
			enableSBDirect = false;
			enableSBResale = false;
			enableSBDirectPhoneSales = false;
			enableSBResalePhoneSales = false;
			enableCBDirect = false;
			enableCBNational = false;					
		}
	}		
}