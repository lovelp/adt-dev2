@isTest
private class DailyLocationMessageProcessorTest {
	
	private static final String USER = 'User';
	private static final String TEAM = 'Team';
	
	static testMethod void testIsValidInputReturnsFalse() {
		
		Datetime dateParam = Datetime.now();
		dateParam = dateParam.addDays(1);
		
		Test.startTest();
		
		DailyLocationMessageProcessor dlmp = new DailyLocationMessageProcessor();
		
		Boolean returnVal = dlmp.isValidInput(null, null, null);
		System.assert(!returnVal, '1. All null input should be invalid');
		System.assert(ApexPages.hasMessages(), '1. The page should have a message');
		
		returnVal = dlmp.isValidInput(LocationDataMessageProcessor.NONE, LocationDataMessageProcessor.NONE, null);
		System.assert(!returnVal, '2. None as all inputs should be invalid');
		System.assert(ApexPages.hasMessages(), '2. The page should have a message');
		
		returnVal = dlmp.isValidInput(USER, null, null);
		System.assert(!returnVal, '3. Null date should be invalid');
		System.assert(ApexPages.hasMessages(), '2. The page should have a message');
		
		returnVal = dlmp.isValidInput(USER, null, '');
		System.assert(!returnVal, '4. Empty string for date should be invalid');
		System.assert(ApexPages.hasMessages(), '4. The page should have a message');
		
		returnVal = dlmp.isValidInput(USER, null, '01/01/2000');
		System.assert(!returnVal, '5. Date more than 250 days ago should be invalid');
		System.assert(ApexPages.hasMessages(), '5. The page should have a message');
		
		returnVal = dlmp.isValidInput(USER, null, dateParam.format('MM/dd/yyyy'));
		System.assert(!returnVal, '6. Date in the future (' + dateParam.format('MM/dd/yyyy') + ') should be invalid');
		System.assert(ApexPages.hasMessages(), '6. The page should have a message');
		
		Test.stopTest();
		
	}
	
	
	static testMethod void testIsValidInputReturnsTrue() {
		
		Datetime dateParam = Datetime.now();
		
		Test.startTest();
		
		DailyLocationMessageProcessor dlmp = new DailyLocationMessageProcessor();
		
		Boolean returnVal = dlmp.isValidInput(USER, null, dateParam.format('MM/dd/yyyy'));
		System.assert(returnVal, '1. String for user and current date should be valid');
		System.assert(!ApexPages.hasMessages(), '1. The page should not have a message');
		
		returnVal = dlmp.isValidInput(USER, null, dateParam.format('M/d/yyyy'));
		System.assert(returnVal, '2. String for user and current date should be valid');
		System.assert(!ApexPages.hasMessages(), '2. The page should not have a message');
		
		returnVal = dlmp.isValidInput(USER, null, dateParam.format('M/d/yy'));
		System.assert(returnVal, '3. String for user and current date should be valid');
		System.assert(!ApexPages.hasMessages(), '3. The page should not have a message');
		
		returnVal = dlmp.isValidInput(USER, null, dateParam.format('MM/dd/yy'));
		System.assert(returnVal, '4. String for user and current date should be valid');
		System.assert(!ApexPages.hasMessages(), '4. The page should not have a message');
		
		Test.stopTest();
		
	}
	
	static testMethod void testBuildParameters() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		Datetime dateParam = Datetime.now();
		Date paramAsDate = Date.newInstance(dateParam.year(), dateParam.month(), dateParam.day());
		
		Test.startTest();
		
		DailyLocationMessageProcessor dlmp = new DailyLocationMessageProcessor();
		
		if (dlmp.isValidInput(salesRep.Id, null, dateParam.format('MM/dd/yyyy'))) {
			LocationDataMessageParameters ldmp = dlmp.buildParameters(salesRep.Id, null, dateParam.format('MM/dd/yyyy'));
			System.assert(ldmp.userIdList != null, 'Expect a non-null list of user ids');
			System.assertEquals(salesRep.Id, ldmp.userIdList[0], 'List should contain the supplied User');
			System.assertEquals(paramAsDate, ldmp.searchDate, 'Search date should be the supplied value');
			System.assert(ldmp.historicalDataRequired, 'Historical data is required');
			System.assert(!ldmp.isForATeam, 'This search should not be for a team');
			System.assert(ldmp.teamManagerId == null, 'There should be no team manager id');
		}
		else {
			System.assert(false, 'Input should be valid but was not');
		}
		
		
		Test.stopTest();
		
	}
	
	static testMethod void testInterpretResultsNoDataExists() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		Datetime dateParam = Datetime.now();
		
		DailyLocationMessageProcessor dlmp = new DailyLocationMessageProcessor();
		
		if (dlmp.isValidInput(salesRep.Id, null, dateParam.format('MM/dd/yyyy'))) {
			LocationDataMessageParameters ldmp = dlmp.buildParameters(salesRep.Id, null, dateParam.format('MM/dd/yyyy'));
		
			List<LocationItem> itemsList = new List<LocationItem>();
		
			Test.startTest();
		
			Boolean returnValue = dlmp.interpretResults(ldmp, itemsList);
			System.assert(!returnValue, 'Expect nothing to display on the map');
			System.assert(ApexPages.hasMessages(), 'Expect the page to have a message');
		}
		else {
			System.assert(false, 'Input should be valid but was not');
		}
		
		Test.stopTest();
		
	}
	
	static testMethod void testInterpretResultsDataExists() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		Datetime dateParam = Datetime.now();
		
		DailyLocationMessageProcessor dlmp = new DailyLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = dlmp.buildParameters(salesRep.Id, null, dateParam.format('MM/dd/yyyy'));
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		LocationItem li = new LocationItem();
		li.pDateTime = Datetime.now();
		li.rUser = salesRep;
		
		itemsList.add(li);
		
		Test.startTest();
		
		Boolean returnValue = dlmp.interpretResults(ldmp, itemsList);
		System.assert(returnValue, 'Expect to have data to display on the map');
		System.assert(!ApexPages.hasMessages(), 'Do not expect the page to have a message since breadcrumb is for time: ' + li.pDateTime);
		
		Test.stopTest();
		
	}

}