@isTest
public class AppointmentRetaskingTest{
    
    private static Event newEvent;
    private static User testadmin;
    private static User testSalesRep1;
    private static IntegrationSettings__c settings;
    private static void createApponitmentdata(){

        // Create User(s) Needed For Testing 
        Profile p1 = [select id from profile where name='ADT NA Sales Representative'];
        UserRole urExisting = [select id, OpportunityAccessForAccountOwner from UserRole where name like '%Rep' limit 1];
        testSalesRep1 = new User(alias = 'utsr1', email='unitTestSalesRep1@testorg.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p1.Id, userroleid = urExisting.Id, EmployeeNumber__c = TestHelperClass.generateEmployeeNumber(),
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com',
                StartDateInCurrentJob__c = Date.today());
        insert testSalesRep1;
        
        User testSalesRep2 = new User(alias = 'utsr2', email='unitTestSalesRep2@testorg.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p1.Id, userroleid = urExisting.Id, EmployeeNumber__c = TestHelperClass.generateEmployeeNumber(),
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep2@testorg.com',
                StartDateInCurrentJob__c = Date.today());
        insert testSalesRep2;
        
        User testSalesRep3 = new User(alias = 'utsr3', email='unitTestSalesRep3@testorg.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p1.Id, userroleid = urExisting.Id, EmployeeNumber__c = TestHelperClass.generateEmployeeNumber(),
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep3@testorg.com',
                StartDateInCurrentJob__c = Date.today());
        insert testSalesRep3;
        testadmin = TestHelperClass.createAdminUser();
        String empno = Math.random() + '';
        empno = empno.substring(0,9);
        testSalesRep1.EmployeeNumber__c = empno;
        update testSalesRep1;
        system.runas(testadmin){
	        list<ResaleGlobalVariables__c> reslist = new list<ResaleGlobalVariables__c>();
	        ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
	        res.name = 'DataRecastDisableAccountTrigger';
	        res.value__c = 'TRUE';
	        reslist.add(res);
	        ResaleGlobalVariables__c res1 = new ResaleGlobalVariables__c();
	        res1.name = 'GlobalResaleAdmin';
	        res1.value__c = 'Global Admin';
	        reslist.add(res1);
	        ResaleGlobalVariables__c res2 = new ResaleGlobalVariables__c();
	        res2.name = 'EventRecTypeNameAllowed';
	        res2.value__c = 'Company Generated Appointment,Field Sales Appointment';
	        reslist.add(res2);
	        ResaleGlobalVariables__c res3 = new ResaleGlobalVariables__c();
	        res3.name = 'DisqualifyEventTaskCode';
	        res3.value__c = 'TRV';
	        reslist.add(res3);
	        insert reslist;
	        SchedDefaults__c scde = new SchedDefaults__c();
	        scde.Name = 'scde';
	        scde.Busi_Task__c = 'SBV';
	        scde.Code__c = 'N1,N3';
	        scde.Duration__c = 2;
	        scde.Resi_Task__c = 'RHS';
	        insert scde;
	        Address__c addr1 = new Address__c();
	        addr1.Latitude__c = 26.39412020000000;
	        addr1.Longitude__c = -80.11689480000000;
	        addr1.Street__c = '1501 Yamato Rd';
	        addr1.City__c = 'Boca Raton';
	        addr1.State__c = 'FL';
	        addr1.PostalCode__c = '33431';
	        addr1.OriginalAddress__c = '1501 Yamato Rd, Boca Raton, FL 33431';
	        addr1.StandardizedAddress__c = '1501 Yamato Rd, Boca Raton, FL 33431';
	        insert addr1;
	        Postal_Codes__c psc = new Postal_Codes__c();
	        psc.Name = '33431';
	        psc.TMTownID__c = '623';
	        psc.TMSubTownID__c = '1';
	        insert psc;
	        Account a = TestHelperClass.createAccountData(addr1);
		        //a.Channel__c = 'Resi Direct Sales';
		        a.Channel__c = 'Custom Home Sales';
	        a.MMBOrderType__c = 'N1';
	        a.PostalCodeID__c = psc.Id;
		        a.AddressID__c = addr1.Id;
		        a.Type = IntegrationConstants.TYPE_RIF;
	        update a;
	        Account tstAcc = [SELECT Id, Name, AddressID__c, MMBOrderType__c, TelemarAccountNumber__c, SalesAppointmentDate__c, FirstName__c, 
	                              LastName__c, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, 
	                              Phone, Email__c, DispositionComments__c, ReferredBy__c, Channel__c, Business_ID__c, ProcessingType__c, 
	                              TaskCode__c, TransitionDate2__c, LeadExternalID__c, Data_Source__c, Type, AccountStatus__c, OutboundTelemarAccountNumber__c, 
	                              Affiliation__c, TMTownID__c, TMSubTownID__c FROM ACCOUNT WHERE id = :a.Id];
	                              system.debug('###Account'+tstAcc);
	        newEvent = new Event();
	        newEvent.ShowAs = EventManager.SHOW_AS_BUSY;
	        newEvent.Status__c= EventManager.STATUS_SCHEDULED;
	        newEvent.TaskCode__c = 'TSK';
	        newEvent.subject = 'Appt: TSK - Boca Raton, FL';
		        newEvent.StartDateTime = system.now().addminutes(15);
	        newEvent.EndDateTime = system.now().addminutes(150);
	        newEvent.ReminderDateTime = system.now().addMinutes(-1*15);//Default reminder
	        newEvent.OwnerId = testSalesRep1.Id;
	        newEvent.WhatId = a.Id;
	        newEvent.TimeZoneId__c = 'America/New_York';  //Srini
	        newEvent.Appointment_Type__c = 'New';
	        // Set appointment record type
	        RecordType rt = [select id, name, sobjecttype from RecordType where sobjectType = 'Event' AND Name = :RecordTypeName.SELF_GENERATED_APPOINTMENT];
	        newEvent.RecordTypeId = rt.Id;
	        insert newEvent;
	        SchedUserTerr__c schUserTerr1 = createSchedUserTerr(testSalesRep1, addr1.Id);
	        insert schUserTerr1;
	        SchedUserTerr__c schUserTerr2 = createSchedUserTerr(testSalesRep2, addr1.Id);
	        insert schUserTerr2;
	        SchedUserTerr__c schUserTerr3 = createSchedUserTerr(testSalesRep3, addr1.Id);
	        insert schUserTerr3;
			settings = new IntegrationSettings__c(TelemarCalloutsEnabled__c = true, TelemarCalloutsAsync__c = true);
	        insert settings;	        
        }
    }    
    private static Event createEvent(Id accId){
    	Event eve = new Event();
        eve.ShowAs = EventManager.SHOW_AS_BUSY;
        eve.Status__c= EventManager.STATUS_SCHEDULED;
        eve.TaskCode__c = 'TSK';
        eve.subject = 'Appt: TSK - Boca Raton, FL';
        eve.StartDateTime = system.now().addminutes(30);
        eve.EndDateTime = system.now().addminutes(150);
        eve.ReminderDateTime = system.now().addMinutes(15);//Default reminder
        eve.OwnerId = testSalesRep1.Id;
        eve.WhatId = accId;
        eve.TimeZoneId__c = 'America/New_York';  //Srini
        eve.Appointment_Type__c = 'New';
        // Set appointment record type
        RecordType rt = [select id, name, sobjecttype from RecordType where sobjectType = 'Event' AND Name = :RecordTypeName.SELF_GENERATED_APPOINTMENT];
        eve.RecordTypeId = rt.Id;
        insert eve;
        return eve;
    }
    private static SchedUserTerr__c createSchedUserTerr( User u, Id stDefaultLocation){
        SchedUserTerr__c schObj  = new SchedUserTerr__c();
        schObj.User__c = u.Id;
        schObj.BusinessId__c = '1100';
        //schObj.Territory_Association_ID__c=ta.Id;
        schObj.Order_Types__c = 'New;Resale;Relocation;Add-On;Conversion;Custom Home;Custom Home Sales';
        schObj.TMTownID__c = '623';
        schObj.TMSubTownID__c = '1';
        
        if( !String.isBlank(stDefaultLocation) ){
            schObj.Default_Starting_Address__c = stDefaultLocation;
        }
        
        // Sunday
        schObj.Sunday_Available_Start__c = 800;
        schObj.Sunday_Available_End__c = 2300;
        
        // Monday
        schObj.Monday_Available_Start__c = 800;
        schObj.Monday_Available_End__c = 2300;
        
        // Tuesday
        schObj.Tuesday_Available_Start__c = 800;
        schObj.Tuesday_Available_End__c = 2300;
        
        // Wednesday
        schObj.Wednesday_Available_Start__c = 800;
        schObj.Wednesday_Available_End__c = 2300;
        
        // Thursday
        schObj.Thursday_Available_Start__c = 800;
        schObj.Thursday_Available_End__c = 2300;
        
        // Friday
        schObj.Friday_Available_Start__c = 800;
        schObj.Friday_Available_End__c = 2300;
        
        // Saturday
        schObj.Saturday_Available_Start__c = 800;
        schObj.Saturday_Available_End__c = 2300;
        
        return schObj;

    }

    
    static testmethod void testmethod1(){
        createApponitmentdata();
        system.runas(testadmin){
            try{
            Test.setCurrentPageReference(Page.ManageEvents);
            ApexPages.Standardcontroller stdcon = new Apexpages.Standardcontroller(newEvent);
            AppointmentRetasking apnt = new AppointmentRetasking(stdcon);
            apnt.getTaskcodes();
            apnt.retaskCode = [SELECT Id FROM RetaskCode__mdt WHERE DeveloperName = 'ERR'].Id;
            apnt.retaskComments = 'Test data';
            apnt.allRepsAndSch();
            apnt.retaskCancel();
	            apnt.retaskCode = [SELECT Id FROM RetaskCode__mdt WHERE DeveloperName = 'RSL_UPD'].Id;
	            apnt.retaskComments = 'Test data';
	            apnt.allRepsAndSch();
				settings.TelemarCalloutsEnabled__c = false;
				update settings;
				AppointmentRetasking.sendCancelAppntToTelemar(newEvent.Id, newEvent.WhatId);
            }
            catch(Exception e){
                system.debug('ERR may not be available. Ignore');
            }
        }
    }
    
    static testmethod void testmethod2(){
        createApponitmentdata();
        RetaskCode__mdt rc = new RetaskCode__mdt(DeveloperName = 'RSL_NEW', Channel__c = 'ANY', Active__c = true, Action__c = 'NEW');
        rc.Duration__c = '0'; 
        rc.AppointmentType__c = 'Resale';
        rc.TaskCodeToUse__c = 'RSL';
        rc.NeedNotification__c = true;
        system.runas(testadmin){
            try{
            	Test.startTest();
            Test.setCurrentPageReference(Page.ManageEvents);
            ApexPages.Standardcontroller stdcon = new Apexpages.Standardcontroller(newEvent);
            AppointmentRetasking apnt = new AppointmentRetasking(stdcon);
            apnt.getTaskcodes();
		        apnt.custMetaMap.put('RSLNewTestCase', rc);
		        //apnt.getRepDetails();
		        //apnt.retaskCode = [SELECT Id FROM RetaskCode__mdt WHERE DeveloperName = 'RSL_NEW'].Id;
		        apnt.retaskCode = 'RSLNewTestCase';
            apnt.selRepId = testSalesRep1.Id;
            apnt.schDate = system.today().format();
            apnt.retaskTime = '11:00 AM';
            apnt.retaskComments = 'Test Data';
            apnt.allRepsAndSch();
            apnt.getRepDetails();
            apnt.saveEvent();
				apnt.goBack();
				apnt.updateAppnt();

				createEvent(newEvent.WhatId);
				apnt.allRepsAndSch();
				apnt.getRepDetails();
				Test.stopTest();
            }
            
            catch(Exception e){
                system.debug('RSL_NEW may not be available. Ignore');
            }
        }
    }
    
    static testMethod void testConvertoCustTimeZone() {
        RetaskingAppointmentProcessing objRetaskingAppointmentProcessing = new RetaskingAppointmentProcessing();
        
        objRetaskingAppointmentProcessing.googleTZResp = new GoogleMapsTimeZone.GoogleTZResponse();
        objRetaskingAppointmentProcessing.googleTZResp.dstOffset = 0;
        objRetaskingAppointmentProcessing.googleTZResp.rawOffset = -18000;
        objRetaskingAppointmentProcessing.googleTZResp.timeZoneId = 'America/New_York';
        objRetaskingAppointmentProcessing.googleTZResp.timeZoneName = 'Eastern Standard Time';
        objRetaskingAppointmentProcessing.googleTZResp.error_message = 'error';
        
        objRetaskingAppointmentProcessing.requestSchDate = Date.Today();
        
        system.debug(objRetaskingAppointmentProcessing.googleTZResp);
        objRetaskingAppointmentProcessing.convertoCustTimeZone(new List<Time>{DateTime.Now().Time(), DateTime.Now().Time()}, 'GMT');
    }
    
    static testMethod void testInitSalesUserAvailability() {
        //  Test data setup
        createApponitmentdata();
        
        Account acc = [Select Id From Account limit 1];
        
        RetaskingAppointmentProcessing objRetaskingAppointmentProcessing = new RetaskingAppointmentProcessing();
        
        objRetaskingAppointmentProcessing.accID = acc.Id;
        
        objRetaskingAppointmentProcessing.googleTZResp = new GoogleMapsTimeZone.GoogleTZResponse();
        objRetaskingAppointmentProcessing.googleTZResp.dstOffset = 0;
        objRetaskingAppointmentProcessing.googleTZResp.rawOffset = -18000;
        objRetaskingAppointmentProcessing.googleTZResp.timeZoneId = 'America/New_York';
        objRetaskingAppointmentProcessing.googleTZResp.timeZoneName = 'Eastern Standard Time';
        objRetaskingAppointmentProcessing.googleTZResp.error_message = 'error';
        
        objRetaskingAppointmentProcessing.requestSchDate = Date.Today();
        
        SchedUserTerr__c objSchedUserTerr = new SchedUserTerr__c();
        objSchedUserTerr.Sunday_Available_Start__c = 10;
        objSchedUserTerr.Sunday_Available_End__c = 12;
        objSchedUserTerr.Monday_Available_Start__c = 10;
        objSchedUserTerr.Monday_Available_End__c = 12;
        objSchedUserTerr.Tuesday_Available_Start__c = 10;
        objSchedUserTerr.Tuesday_Available_End__c = 12;
        objSchedUserTerr.Wednesday_Available_Start__c = 10;
        objSchedUserTerr.Wednesday_Available_End__c = 12;
        objSchedUserTerr.Thursday_Available_Start__c = 10;
        objSchedUserTerr.Thursday_Available_End__c = 12;
        objSchedUserTerr.Friday_Available_Start__c = 10;
        objSchedUserTerr.Friday_Available_End__c = 12;
        objSchedUserTerr.Saturday_Available_Start__c = 10;
        objSchedUserTerr.Saturday_Available_End__c = 12;
        objSchedUserTerr.Order_Types__c = 'TEST';
        
        
        
        objRetaskingAppointmentProcessing.initSalesUserAvailability();
        
        objRetaskingAppointmentProcessing.fieldAppType = 'TYPE';
        
        objRetaskingAppointmentProcessing.getAllEligibleReps();
        
        objRetaskingAppointmentProcessing.repsFromScheTerr = new Set<Id>{testSalesRep1.Id};
        
        objRetaskingAppointmentProcessing.currentEventsForRep();
        
        objRetaskingAppointmentProcessing.requestSchDate = Date.Today().addDays(1);
        objRetaskingAppointmentProcessing.indentifyDayReps(objSchedUserTerr);
        objRetaskingAppointmentProcessing.requestSchDate = Date.Today().addDays(2);
        objRetaskingAppointmentProcessing.indentifyDayReps(objSchedUserTerr);
        objRetaskingAppointmentProcessing.requestSchDate = Date.Today().addDays(3);
        objRetaskingAppointmentProcessing.indentifyDayReps(objSchedUserTerr);
        objRetaskingAppointmentProcessing.requestSchDate = Date.Today().addDays(4);
        objRetaskingAppointmentProcessing.indentifyDayReps(objSchedUserTerr);
        objRetaskingAppointmentProcessing.requestSchDate = Date.Today().addDays(5);
        objRetaskingAppointmentProcessing.indentifyDayReps(objSchedUserTerr);
        objRetaskingAppointmentProcessing.requestSchDate = Date.Today().addDays(6);
        objRetaskingAppointmentProcessing.indentifyDayReps(objSchedUserTerr);
        objRetaskingAppointmentProcessing.requestSchDate = Date.Today().addDays(7);
        objRetaskingAppointmentProcessing.indentifyDayReps(objSchedUserTerr);
    }
    
}