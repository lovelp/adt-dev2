@isTest
private class ManagerTownLeadRealignmentBatchTest {

    static testMethod void testManagerTownBatch() {
        
        User admin = TestHelperClass.createAdminUser();
        User salesRep = TestHelperClass.createSalesRepUserWithManager();
        TestHelperClass.createSalesRepUser(); 
        System.runAs(admin)
        {
            TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        String MTInterval = BatchState__c.getInstance('ManagerTownScheduledBatchInterval').value__c;
        
        UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
        User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId Limit 1];
        
        system.runAs(admin)
        {
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';    
            insert addr;        
            
            Account acct = TestHelperClass.createAccountData(addr);
            acct.Channel__c = 'Resi Direct Sales';
            acct.ProcessingType__c = IntegrationConstants.PROCESSING_TYPE_NSC;
            acct.Business_Id__c = '1100';
            acct.TelemarAccountNumber__c = '1_2_3_4';
            update acct;
            
            Account acct1 = TestHelperClass.createAccountData(addr);
            acct1.Channel__c = 'SB Resale';
            acct1.Business_Id__c = '1200';
            acct1.Data_Source__c = '1';
            acct1.LeadStatus__c = 'Active';
            acct1.UnassignedLead__c = false;
            acct1.InService__c = true;          
            update acct1;
            
            Lead led = TestHelperClass.createLead(admin, false);
            led.Channel__c = 'Resi Direct Sales';
            led.Business_Id__c = '1100 - Residential';
            led.NewMoverType__c = 'RL';
            led.LeadSource = 'BUDCO';
            insert led;
            
            Postal_Codes__c newPC1 = [select id, BusinessID__c, TownId__C from Postal_Codes__c where name = '221o2' and BusinessId__c = '1100'];
            ManagerTown__c existingMT = TestHelperClass.createManagerTown(ManagerUser, newPC1, 'Resi Direct Sales' + ';' + 'Resi Resale');  
            
            ManagerTownRealignmentQueue__c MQ = new ManagerTownRealignmentQueue__c(TownId__c = 'TestTNID', BusinessId__c = '1100');
            insert MQ;
            List<ManagerTownRealignmentQueue__c> MQr = new List<ManagerTownRealignmentQueue__c>();
            MQr.add(MQ);
            
            //create leads          
            Test.StartTest();
            
            String acctQry = 'Select id, OwnerId, DateAssigned__c, UnassignedLead__c from Account where ResaleTownNumber__c = ';
            acctQry += '\'' + 'TestTNID' + '\' ';
            acctQry += 'and Business_Id__c like \'%' + '1100' + '%\' ';
            acctQry += 'and Channel__c = \'' + 'Resi Direct Sales' + '\' ';
            //acctQry += 'and OwnerId = \'' + Utilities.getGlobalUnassignedUser() + '\' ';
            acctQry += 'and (ProcessingType__c = \'' + IntegrationConstants.PROCESSING_TYPE_NSC + '\' ';
            acctQry += 'or RecordType.DeveloperName = \'ADTNAResale\')';
            id acctBatchId = database.executeBatch(new ManagerTownLeadRealignmentBatch(acctQry, ManagerUser.id, true, MQr), 1000);
            
            String leadQry = 'Select  id, OwnerId, DateAssigned__c, UnassignedLead__c from Lead where TownNumber__c = ';
            leadQry += '\'' + 'TestTNID' + '\' ';
            leadQry += 'and Business_Id__c like \'%' + '1100' + '%\' ';
            leadQry += 'and Channel__c = \'' + 'Resi Direct Sales' + '\' ';
            //leadQry += 'and OwnerId = \'' + MQ[0].OldOwnerId__c + '\' ';
            leadQry += 'and LeadSource = \'BUDCO\' ';           
            id leadBatchId = database.executeBatch(new ManagerTownLeadRealignmentBatch(leadQry, ManagerUser.id, false, MQr), 1000);
            Test.StopTest(); 
           /*
            acct = [select id, ownerid from account where id = : acct.id];
            System.assertEquals(acct.OwnerId, ManagerUser.id);
            led = [select id, ownerid from lead where id = : led.id];
            System.assertEquals(led.OwnerId, ManagerUser.id);   
            */      
        }
        
    }
}