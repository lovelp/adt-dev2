@isTest
public without sharing class NSCLeadCaptureControllerTest {

    @TestSetup
    static void makeData(){
        TestHelperClass.createReferenceDataForTestClasses();
        TestHelperClass.createPostalCodes();
    }

    static testMethod void getAccPrefix_Returns_Data(){
        
        Test.startTest();
        NSCLeadCaptureController nscLeadcapture=new NSCLeadCaptureController();
        String result = nscLeadcapture.getAccPrefix;
        Test.stopTest();

        System.assertNotEquals(null, result, 'getAccPrefix should not return null.');
    }

    static testMethod void findDefaultSelectedPromotion_with_default(){
        List<Promotion__c> promos = new List<Promotion__c>();
        Promotion__c promo = new Promotion__c();
        promo.name = 'Promo Without Default Selected';
        promo.Default_Selected__c = false;
        promos.add(promo);

        promo = new Promotion__c();
        promo.name = 'Promo With Default Selected';
        promo.Default_Selected__c = true;
        promos.add(promo);

        Test.startTest();
        NSCLeadCaptureController nscLeadcapture=new NSCLeadCaptureController();
        String result = nscLeadcapture.findDefaultSelectedPromotion(promos);
        Test.stopTest();

        System.assertEquals('Promo With Default Selected', result);
    }


    static testMethod void findDefaultSelectedPromotion_without_default(){
        List<Promotion__c> promos = new List<Promotion__c>();
        Promotion__c promo = new Promotion__c();
        promo.name = 'Promo Without Default Selected';
        promo.Default_Selected__c = false;
        promos.add(promo);

        Test.startTest();
        NSCLeadCaptureController nscLeadcapture=new NSCLeadCaptureController();
        String result = nscLeadcapture.findDefaultSelectedPromotion(promos);
        Test.stopTest();

        System.assertEquals('', result);
    }
}