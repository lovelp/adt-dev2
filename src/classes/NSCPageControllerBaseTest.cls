/**
 Description- This test class used for NSCPageControllerBase and NSCComponentControllerBase.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class NSCPageControllerBaseTest {
    
     static testMethod void testMethod1() {
         
         NSCComponentControllerBase compBase= new NSCComponentControllerBase();
         NSCPageControllerBase nscBase= new NSCPageControllerBase();
         
         compBase.pageController= nscBase;
         
         nscBase.setComponentController(compBase);   
         nscBase.getComponentController();
         nscBase.getThis();
         
         
         }
}