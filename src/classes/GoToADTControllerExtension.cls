/************************************* MODIFICATION LOG ********************************************************************************************
* GoToADTControllerExtension
*
* DESCRIPTION : Serves as a class to be used for invocation of GOTOADT service  and handling the API response
* 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Parameswaran Iyer             7/30/2018         - Original Version

*/

public class GoToADTControllerExtension{

    public String jsonString{get;set;}
    @TestVisible
    private Account acc{get;set;}
    public static HttpResponse response{get;set;}
    public GoToADTControllerExtension() {}

    

    /**
     * This is future method to call external REST API
     * @Param jsonString,accId
     **/

    public PageReference doCallOut(String jsonString,Id accId){
        System.debug('jsonString---'+jsonString);
        sendCalloutREST(jsonString,accId);
        return null; 

    }

    /**
     * This method is used to make the callout and write to the disposition history object
     * @Param jsonString,accId
     **/

    @future(callout=true)
    public static void sendCalloutREST(String jsonString,Id accId){
        try{

            //jsonString = '{ "customerEmailAddress": "dptest1@email.com",   "productId":"adtgo",   "employeeId" : 432237,   "role": "P",   "requestApp": "YOURAPP"}';
            String endPointURL = IntegrationSettings__c.getInstance().GoToADT_Endpoint__c;
            String userName = IntegrationSettings__c.getInstance().GoToADT_Username__c;
            String password = IntegrationSettings__c.getInstance().GoToADT_Password__c;
            
            System.debug('@endpoint---'+endPointURL);
            // Specify the required user name and password to access the endpoint 
            // As well as the header and header information 
            Blob headerValue = Blob.valueOf(userName + ':' + password);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            Httprequest request = new HttpRequest();
            HttpResponse response = new HttpResponse();
            Http http = new Http();
            request.setHeader('Authorization', authorizationHeader);
            request.setHeader('Content-Type', 'application/json');
            request.setEndpoint(endPointURL);
            request.setTimeout(1000);
            request.setMethod('POST');
            request.setBody(jsonString);
            system.debug('RequestBody'+jsonString);
            response = http.send(request);
            Disposition__c dispRec=new Disposition__c();
            dispRec.DispositionType__c='System Generated';
            dispRec.DispositionDate__c=System.now();
            dispRec.DispositionDetail__c= 'ADTGo request Invite Database is sent for update';//'ADTGo Invite Database Updated';
            dispRec.AccountID__c=accId;
            dispRec.DispositionedBy__c=UserInfo.getUserId();
            dispRec.Comments__c= 'ADTGo invite database is sent successfully;';
            insert dispRec;
            System.debug('responseBody: '+response.getBody());
            if(response.getBody()!=null && response.getstatuscode()==200){
                //PostInviteItemWrapper pIIWrapInstance= (PostInviteItemWrapper)JSON.deserialize(response.getBody(), PostInviteItemWrapper.class);
                dispRec.DispositionDetail__c = 'ADTGo Invite Database Updated';
                dispRec.Comments__c= 'ADTGo invite database is sent; Updated successfully.';
                update dispRec;
            }
            else{
                // for any invalid input parameters, log a record into ADTLog
                dispRec.DispositionDetail__c = 'ADTGo invite database Update failed';
                dispRec.Comments__c= 'ADTGo invite database update failed. Please contact system administrator.';
                update dispRec;
                ADTApplicationMonitor.log ('GOTOADT Service Callout Error. Account Id: '+accId, response.getBody(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);

            }
        }catch(Exception e){
            System.debug('Error::'+e.getMessage());
        }
    }


    /**
     * Wrapper class to store request and response of PostInviteItem API
     *
     **/
    public class PostInviteItemRequestWrapper{

        public String customerEmailAddress{get;set;}
        public Integer employeeId{get;set;}
        public  String productId{get;set;}
        public String role{get;set;}
        public String requestApp{get;set;}   

        public PostInviteItemRequestWrapper(){}

        public PostInviteItemRequestWrapper(String customerEmailAddress,Integer employeeId,String productId,String role,String requestApp){
            this.customerEmailAddress=customerEmailAddress.toLowerCase();
            this.employeeId=employeeId;
            this.productId=productId.toUpperCase();
            this.role=role.toUpperCase();
            this.requestApp=requestApp.toUpperCase();
        }

    }
    
    /**
     * Wrapper class to store request and response of PostInviteItem API
     *
     **/
    public class PostInviteItemWrapper{

        public Integer inviteId{get;set;}
        public String customerEmailAddress{get;set;}
        public Integer employeeId{get;set;}
        public  String productId{get;set;}
        public String role{get;set;}
        public String timestamp{get;set;}
        public String requestApp{get;set;}   

        public PostInviteItemWrapper(){}

        public PostInviteItemWrapper(String customerEmailAddress,Integer employeeId,String productId,String role,String requestApp){
            this.customerEmailAddress=customerEmailAddress.toLowerCase();
            this.employeeId=employeeId;
            this.productId=productId.toUpperCase();
            this.role=role.toUpperCase();
            this.requestApp=requestApp.toUpperCase();
        }

    }
    

}