/************************************* MODIFICATION LOG ********************************************************************************************
* StreetSheetRedirect
*
* DESCRIPTION : Used by the StreetSheetRedirect VisualForce page to actually redirect the User to the Street Sheet Lead search screen.  
*               It is a Controller Extension in the SFDC MVC architecture.
*
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2011			- Original Version
*
*													
*/

public with sharing class StreetSheetRedirect {
	
	public StreetSheetRedirect(ApexPages.StandardController stdController) {
      
    }

	public pageReference redirect (){
		PageReference pr = new PageReference ('/apex/ManageStreetSheet');
		pr.setRedirect(true);
		return pr;
	}
	
}