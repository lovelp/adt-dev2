@RestResource(urlmapping='/installDashboard/*')
global class ADTPartnerGetInstallAppointmentAPI 
{
    @HttpPost
    global static void doPost()
    {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        
        ADTPartnerGetInstallApptController getInstallAppointmentController = new ADTPartnerGetInstallApptController ();
        
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(getInstallAppointmentController.parseJSONRequest(request.requestBody.toString())));
        system.debug('Response'+response.responseBody);
        response.statusCode = getInstallAppointmentController.statusCode;
    }
}