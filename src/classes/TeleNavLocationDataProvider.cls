/************************************* MODIFICATION LOG ********************************************************************************************
* TeleNavLocationDataProvider
*
* DESCRIPTION : Extends LocationDataProvider and provides a TeleNav Track specific implementation of the interface.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER					 DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner				 10/14/2011			- Original Version
*
* SHIVKANT VADLAMANI     	 4/5/2012			- Added GPS Appointment Tracking using stop reports 													
*/

public class TeleNavLocationDataProvider extends LocationDataProvider {

	public static string teleNavTestResponse='<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">   <soapenv:Body>      <ns1:queryLatestGpsMeasurementResponse soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:ns1=\"urn:tns\">         <queryLatestGpsMeasurementReturn soapenc:arrayType=\"ns2:TdxGps[2]\" xsi:type=\"soapenc:Array\" xmlns:ns2=\"http://www.telenavtrack.net/schema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">            <queryLatestGpsMeasurementReturn xsi:type=\"ns2:TdxGps\">               <ptn xsi:type=\"xsd:string\">3036382096</ptn>               <time xsi:type=\"xsd:long\">1280623350000</time>               <heading xsi:type=\"xsd:double\">148.0</heading>               <speed xsi:type=\"xsd:double\">110.0</speed>               <mileage xsi:type=\"xsd:double\">0.0</mileage>               <address xsi:type=\"ns2:TdxAddress\">                 <lat xsi:type=\"xsd:double\">40.162</lat>                  <lon xsi:type=\"xsd:double\">-75.3002</lon>                  <street xsi:type=\"xsd:string\">Near I-476 S\NE Extension Penn Tpke</street>                  <city xsi:type=\"xsd:string\">Blue Bell</city>                  <state xsi:type=\"xsd:string\">PA</state>                  <zipCode xsi:type=\"xsd:string\">19422</zipCode>                  <country xsi:type=\"xsd:string\">US</country>                  <layerId xsi:type=\"xsd:long\" xsi:nil=\"true\"/>               </address>            </queryLatestGpsMeasurementReturn>            <queryLatestGpsMeasurementReturn xsi:type=\"ns2:TdxGps\">               <ptn xsi:type=\"xsd:string\">3036382096</ptn>               <time xsi:type=\"xsd:long\">1280622450000</time>              <heading xsi:type=\"xsd:double\">173.0</heading>               <speed xsi:type=\"xsd:double\">126.0</speed>               <mileage xsi:type=\"xsd:double\">0.0</mileage>               <address xsi:type=\"ns2:TdxAddress\">                  <lat xsi:type=\"xsd:double\">40.4071</lat>                  <lon xsi:type=\"xsd:double\">-75.4131</lon>                  <street xsi:type=\"xsd:string\">Near I-476 S\NE Extension Penn Tpke</street>                  <city xsi:type=\"xsd:string\">Quakertown</city>                  <state xsi:type=\"xsd:string\">PA</state>                  <zipCode xsi:type=\"xsd:string\">18951</zipCode>                  <country xsi:type=\"xsd:string\">US</country>                  <layerId xsi:type=\"xsd:long\" xsi:nil=\"true\"/>               </address>            </queryLatestGpsMeasurementReturn>         </queryLatestGpsMeasurementReturn>      </ns1:queryLatestGpsMeasurementResponse>   </soapenv:Body></soapenv:Envelope>';
	public static Dom.Document testDoc = new Dom.Document();
	public static boolean isTest =true;
	public static string gpsHistoryString =	'<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:tns\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soapenv:Header/> <soapenv:Body><urn:getGpses soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> <wsAdmin xsi:type=\"sch:TdxAdmin\" xmlns:sch=\"http://www.telenavtrack.net/schema\"><email xsi:type="xsd:string">&userName</email><password xsi:type=\"xsd:string\">&pswd</password></wsAdmin><ptns xsi:type=\"sch:StringArray\" soapenc:arrayType=\"xsd:string[]\" xmlns:sch=\"http://www.telenavtrack.net/schema\">&phoneItems</ptns> <startTime xsi:type=\"xsd:long\">&start</startTime> <endTime xsi:type=\"xsd:long\">&end</endTime>  </urn:getGpses>   </soapenv:Body></soapenv:Envelope>';
	public static string sQueryLatestGPS='<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:tns\">   <soapenv:Header/>   <soapenv:Body>      <urn:queryLatestGpsMeasurement soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">         <wsAdmin xsi:type=\"sch:TdxAdmin\" xmlns:sch=\"http://www.telenavtrack.net/schema\">            <email xsi:type=\"xsd:string\">&userName</email>            <password xsi:type=\"xsd:string\">&pswd</password>         </wsAdmin>         <ptn xsi:type=\"xsd:string\">&phoneNum</ptn>      </urn:queryLatestGpsMeasurement>   </soapenv:Body></soapenv:Envelope>';
	public static string sPtnItem='<item xsi:type=\"xsd:string\">&phoneNum</item>';
	private static boolean isHistorical =false;
	private static boolean isTeam =false;
	private static Map <String, User> userMap =new Map <String, User>();
	private static Map <String, LocationItem> latestLocMap = new Map <String, LocationItem>();
	public static string getStopReportString  = '<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" > <soapenv:Body>  <ns1:getStopReport soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:ns1=\"urn:tns\">   <wsAdmin href=\"#id0\"/>   <ptn xsi:type=\"xsd:string\">&devicenum</ptn>   <stopTime href=\"#id1\"/>   <stopRadius href=\"#id2\"/>   <startTime href=\"#id3\"/>   <endTime href=\"#id4\"/>  </ns1:getStopReport>  <multiRef id=\"id4\" soapenc:root=\"0\" soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xsi:type=\"xsd:long\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">&end</multiRef><multiRef id=\"id1\" soapenc:root=\"0\" soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xsi:type=\"xsd:int\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">&stoptime</multiRef>  <multiRef id=\"id0\" soapenc:root=\"0\" soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xsi:type=\"ns2:TdxAdmin\" xmlns:ns2=\"http://www.telenavtrack.net/schema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">   <email xsi:type=\"xsd:string\">&userName</email>   <password xsi:type=\"xsd:string\">&pswd</password>  </multiRef>  <multiRef id=\"id2\" soapenc:root=\"0\" soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xsi:type=\"xsd:int\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">&stopradius</multiRef>  <multiRef id=\"id3\" soapenc:root=\"0\" soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xsi:type=\"xsd:long\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">&start</multiRef></soapenv:Body></soapenv:Envelope>';
	public static string teleNavTestStopReportResponse = '<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"> <soapenv:Body>  <ns1:getStopReportResponse soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:ns1=\"urn:tns\">   <getStopReportReturn soapenc:arrayType=\"ns2:TdxGPSPair[1]\" xsi:type=\"soapenc:Array\" xmlns:ns2=\"http://www.telenavtrack.net/schema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">    <getStopReportReturn xsi:type=\"ns2:TdxGPSPair\">     <startGPS xsi:type=\"ns2:TdxGps\">      <ptn xsi:type=\"xsd:string\">5550000014</ptn>      <time xsi:type=\"xsd:long\">1335279600000</time>      <heading xsi:type=\"xsd:double\">0.0</heading>      <speed xsi:type=\"xsd:double\">0.0</speed>      <mileage xsi:type=\"xsd:double\">0.0</mileage>      <address xsi:type=\"ns2:TdxAddress\">       <lat xsi:type=\"xsd:double\">37.3731</lat>       <lon xsi:type=\"xsd:double\">-121.999</lon>       <street xsi:type=\"xsd:string\">Near 1142 KIFER RD and SEMICONDUCTOR DR</street>       <city xsi:type=\"xsd:string\">SANTA CLARA</city>       <state xsi:type=\"xsd:string\">CA</state>       <zipCode xsi:type=\"xsd:string\">94086</zipCode>       <country xsi:type=\"xsd:string\">US</country>       <layerId xsi:type=\"xsd:long\" xsi:nil=\"true\"/>      </address>     </startGPS>     <endGPS xsi:type=\"ns2:TdxGps\">      <ptn xsi:type=\"xsd:string\">5550000014</ptn>      <time xsi:type=\"xsd:long\">1335283200000</time>      <heading xsi:type=\"xsd:double\">0.0</heading>      <speed xsi:type=\"xsd:double\">0.0</speed>      <mileage xsi:type=\"xsd:double\">0.0</mileage>      <address xsi:type=\"ns2:TdxAddress\">       <lat xsi:type=\"xsd:double\">37.37319</lat>       <lon xsi:type=\"xsd:double\">-121.999</lon>       <street xsi:type=\"xsd:string\">Near 1142 KIFER RD and SEMICONDUCTOR DR</street>       <city xsi:type=\"xsd:string\">SANTA CLARA</city>       <state xsi:type=\"xsd:string\">CA</state>       <zipCode xsi:type=\"xsd:string\">94086</zipCode>       <country xsi:type=\"xsd:string\">US</country>       <layerId xsi:type=\"xsd:long\" xsi:nil=\"true\"/>      </address>     </endGPS>    </getStopReportReturn>   </getStopReportReturn>  </ns1:getStopReportResponse> </soapenv:Body></soapenv:Envelope>';
	public static list<LocationItem> startGPSLocationItemList = new list<LocationItem>();
	public static list<LocationItem> endGPSLocationItemList = new list<LocationItem>();
	public static String sUserName;
	public static String sPassword;
	public static String sEndPoint; 
	 
	public TeleNavLocationDataProvider()
	{
		userMap = new Map <String, User>();
		
		IntegrationSettings__c settings = IntegrationSettings__c.getOrgDefaults();		
		
		sUserName = settings.TeleNavTrackUserName__c;
		sPassword = settings.TeleNavTrackPassword__c;	 
		sEndPoint = settings.TeleNavTrackEndpoint__c;	
	}

	public override List<LocationItem> getLocationItems(LocationDataMessageParameters parameters) {
		
		httpRequest req = TeleNavLocationDataProvider.basicAuthCallout(parameters.searchDate, parameters.userIdList, 
																			parameters.historicalDataRequired);
		Dom.Document  domDoc = TeleNavLocationDataProvider.invokeCallout(req);
		 			
		List<LocationItem> locationItemList = TeleNavLocationDataProvider.handleResponse(domDoc);
		
		return locationItemList;
	}	
	
	public override  map<String,List<LocationItem>> getStopLocations(Datetime startDT, Datetime endDT, String DevicePhoneNumber)
	{
		map<String,List<LocationItem>> stopInfoMap = new map<String,List<LocationItem>>();
		
		httpRequest req = TeleNavLocationDataProvider.basicAuthCalloutForStopReport(startDT, endDT, DevicePhoneNumber);
		
		Dom.Document  domDoc = TeleNavLocationDataProvider.invokeCallout(req);		
			
		stopInfoMap = TeleNavLocationDataProvider.handleResponseForStopReport(domDoc);
				
		return stopInfoMap;
	}	
	
	public static Dom.Document invokeCallout( HttpRequest req)
	{		
     	Http h = new Http();
    	HttpResponse res;
    	Dom.Document doc = null;
    	Boolean calloutSuccessful = false;
    	Integer attemptCount=0;
        //Invoke Web Service, retrying as necessary
    	while(!calloutSuccessful && attemptCount < 10)
    	{
        	try
            {
                attemptCount++;
                res = h.send(req);
                doc = res.getBodyDocument();
                calloutSuccessful = true;
            }
            catch(CalloutException e)
            {
                calloutSuccessful = false;
            }
    	}    	
	    return doc;     	
	}

	public static List <LocationItem> handleResponse( Dom.Document doc)
	{
		List <LocationItem> locationHistoryList  = new List <LocationItem> ();
        for(dom.XmlNode parent : doc.getRootElement().getChildElements()) {      
        	for(dom.XmlNode node : parent.getChildElements()) {         	
        		for(dom.XmlNode children : node.getChildElements()) {         			 
        				for(dom.XmlNode gpsReturn2 : children.getChildElements()) { 
        					 LocationItem thisLoc = new LocationItem();
        					 string namespace = gpsReturn2.getNamespace();
        					 if(gpsReturn2.getChildElement('address',namespace)!=null){
        					 	 thisLoc.pLatitude=gpsReturn2.getChildElement('address',namespace).getChildElement('lat',namespace).getText();
        					  	thisLoc.pLongitude=gpsReturn2.getChildElement('address',namespace).getChildElement('lon',namespace).getText();
        					  	thisLoc.reverseAddress = gpsReturn2.getChildElement('address',namespace).getChildElement('street',namespace).getText();
        					 // Long l = long.valueOf(pString)
							  	thisLoc.pDateTime=DateTime.newInstance(long.valueOf(gpsReturn2.getChildElement('time',namespace).getText()));
							  	thisLoc.telenavDate=gpsReturn2.getChildElement('time',namespace).getText();
							  	thisLoc.pTime=thisLoc.pDateTime.format('h:mm a'); 
							 	thisLoc.rUser= userMap.get(gpsReturn2.getChildElement('ptn',namespace).getText());
							 	thisLoc.key=thisLoc.pDateTime+'-'+thisLoc.pLatitude+'-'+thisLoc.pLongitude;
      							thisLoc.gMapsLink= '<a href="#" onclick="javascript:window.open(\'http://maps.google.com/?q='+thisLoc.pLatitude +','+thisLoc.pLongitude+'\')">'+thisLoc.reverseAddress+'</a>';
      							thisLoc.sMapLinkText='\'http://maps.google.com/?q='+thisLoc.platitude +','+thisLoc.plongitude+'\'';
      							thisLoc.formattedDate = thisLoc.pDateTime.format();
      							thisLoc.pPhoneNum =gpsReturn2.getChildElement('ptn',namespace).getText();
      							thisLoc.key=thisLoc.telenavDate+'-'+thisLoc.pPhoneNum;      							
							//	thisLoc.rUser=userMap.get(children.getChildElement('addressableId', null).getText());
        						if (!isTeam) {
        							locationHistoryList.add(thisLoc);
        						}	
        						else {
        							LocationItem storedLoc;
        							if (latestLocMap.get( thisLoc.rUser.id)!=null){
        								storedLoc=latestLocMap.get( thisLoc.rUser.id);
        								if (storedLoc.pDateTime< thisLoc.pDateTime){
        									latestLocMap.remove( thisLoc.rUser.id);
        									latestLocMap.put( thisLoc.rUser.id, thisLoc);
        								}
        							}
        							else {
        								latestLocMap.put( thisLoc.rUser.id, thisLoc);
        							}
        						}
        						if (!isHistorical) break;
        					 }
        				}
        				if (isTeam) {
        					locationHistoryList=latestLocMap.values();
        				}	
        		}
        	}
        }
       
        return locationHistoryList;
	}
	
	
	public static HttpRequest   basicAuthCallout(Date d, List<Id> UserIds, Boolean historical){
		string sStartUnix, sEndUnix, sPhoneNumber, sRequestString;
		string [] sPhoneArray = new String[]{};
		//if (userIds.size()>1) isTeam=true;
        HttpRequest req = createHttpRequest();     	
				
		for (User u: [select id, DevicePhoneNumber__c, name from User where id in:UserIds]){
	 		if (u.DevicePhoneNumber__c != null){
	 	 		userMap.put(u.DevicePhoneNumber__c, u);
	 	 		sPhoneArray.add(u.DevicePhoneNumber__c);
	 	 		
	 		}
	 	}
		if (sPhoneArray.size()>1)  {
			isTeam=true;
		}	
		else {
			isTeam=false;
		}	

		if (historical) {
			isHistorical=true;
			sPhoneNumber='';
			Date tom = d.addDays(1);
			if (isTeam){
				d=d.addDays(-6);
			}
			DateTime startDT = DateTime.newInstance(d.year(), d.month(), d.day(), 0, 0, 0);
			DateTime endDT = DateTime.newInstance(tom.year(), tom.month(), tom.day(), 0, 0, 0);			
			
			long startUnix = startDT.getTime();
			sStartUnix = string.valueOf(startUnix);
			long endUnix = endDT.getTime();
			sEndUnix = string.valueOf(endUnix);
			sRequestString=TeleNavLocationDataProvider.gpsHistoryString;
			sRequestString=sRequestString.replace('&userName', sUserName);
			sRequestString=sRequestString.replace('&pswd', sPassword);
			for (String s: sPhoneArray){
				sPhoneNumber+=sPtnItem;
				sPhoneNumber=sPhoneNumber.replace('&phoneNum', s);
			}
			sRequestString=sRequestString.replace('&phoneItems', sPhoneNumber);
			sRequestString=sRequestString.replace('&start', sStartUnix);
			sRequestString=sRequestString.replace('&end', sEndUnix);

		}
		else {
			sRequestString=TeleNavLocationDataProvider.sQueryLatestGPS;
			sRequestString=sRequestString.replace('&userName', sUserName);
			sRequestString=sRequestString.replace('&pswd', sPassword);
			if (!sPhoneArray.isEmpty()) {
				sRequestString=sRequestString.replace('&phoneNum', sPhoneArray[0]);
			}	
		}
		
	  	req.setBody(sRequestString);
    	req.setEndpoint(sEndPoint);
      	
      	return req;
	}
	
	public static map<String,List<LocationItem>> handleResponseForStopReport( Dom.Document doc)
	{		
		Map<String,List<LocationItem>> stopInfoMap = new Map<String,List<LocationItem>>();
		List <LocationItem> locationHistoryList  = new List <LocationItem> ();
        for(dom.XmlNode parent : doc.getRootElement().getChildElements()) 
        {      
        	for(dom.XmlNode node : parent.getChildElements()) 
        	{ 
        		for(dom.XmlNode children : node.getChildElements()) 
        		{ 
    				for(dom.XmlNode stopRepRet : children.getChildElements()) 
    				{
	    				string namespace = stopRepRet.getNamespace();	
	    				Dom.XmlNode startGPSNode = stopRepRet.getChildElement('startGPS',namespace);
	    				Dom.XmlNode endGPSNode = stopRepRet.getChildElement('endGPS',namespace);		
						if(startGPSNode != null)
						{
							LocationItem thisLoc = new LocationItem();
							Dom.XmlNode addressNode = startGPSNode.getChildElement('address',namespace);
							if(addressNode != null)
							{
								thisLoc.pLatitude = addressNode.getChildElement('lat',namespace).getText();
	    					  	thisLoc.pLongitude = addressNode.getChildElement('lon',namespace).getText();
	    					}    					 
						  	thisLoc.pDateTime=DateTime.newInstance(long.valueOf( startGPSNode.getChildElement('time',namespace).getText()));
						  									
							startGPSLocationItemList.add(thisLoc);   												 	
						}							
						
						if(endGPSNode != null)   
						{
							LocationItem thisLoc = new LocationItem();
							Dom.XmlNode addressNode = endGPSNode.getChildElement('address',namespace);
							if(addressNode != null)
							{
								thisLoc.pLatitude = addressNode.getChildElement('lat',namespace).getText();
	    					  	thisLoc.pLongitude = addressNode.getChildElement('lon',namespace).getText();
	    					}    					 
						  	thisLoc.pDateTime=DateTime.newInstance(long.valueOf( endGPSNode.getChildElement('time',namespace).getText()));
						  	
							endGPSLocationItemList.add(thisLoc);   
						} 							
    				}        					
        		}
        	}
        }	
        
        if(startGPSLocationItemList != null && startGPSLocationItemList.size() > 0)
        {
        	stopInfoMap.put(LocationDataProvider.START_GPS, startGPSLocationItemList);
        }
       
        if(endGPSLocationItemList != null && endGPSLocationItemList.size() > 0)
        {
        	stopInfoMap.put(LocationDataProvider.END_GPS, endGPSLocationItemList);
        }
        
        return stopInfoMap;   
	}
	
	public static HttpRequest basicAuthCalloutForStopReport(Datetime startDT, Datetime endDT, String DevicePhoneNumber)
	{		
		HttpRequest req = createHttpRequest();    	
		 	
	 	long startUnix = startDT.getTime();			 
		long endUnix = endDT.getTime();		
		String sRequestString=TeleNavLocationDataProvider.getStopReportString;
					
		sRequestString=sRequestString.replace('&userName', sUserName);
		sRequestString=sRequestString.replace('&pswd', sPassword);		
		sRequestString=sRequestString.replace('&devicenum', DevicePhoneNumber);
		sRequestString=sRequestString.replace('&start', string.valueOf(startUnix));
		sRequestString=sRequestString.replace('&end', string.valueOf(endUnix));
		sRequestString=sRequestString.replace('&stopradius', ResaleGlobalVariables__c.getinstance('AcceptableRadiusForTelenavStopReport').value__c);
		sRequestString=sRequestString.replace('&stoptime', ResaleGlobalVariables__c.getinstance('stopTimeForTelenavStopReport').value__c);			
			
		req.setBody(sRequestString);    	
		req.setEndpoint(sEndPoint);
		
      	return req;
	}
	
	public static HttpRequest createHttpRequest()
	{
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setHeader('content-type','text/xml;charset=UTF-8');
		req.setHeader('Host','integration.telenav.com');
		req.setHeader('SOAPAction','');      
		req.setTimeout(60000);
		
		return req;
	}
}