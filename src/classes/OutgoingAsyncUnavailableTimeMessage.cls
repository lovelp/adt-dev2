/************************************* MODIFICATION LOG ********************************************************************************************
* OutgoingAsyncUnavailableTimeMessage
*
* DESCRIPTION : Asynchronous Admin time message for Telemar Interface
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					2/14/2012				- Original Version
*
*													
*/

public with sharing class OutgoingAsyncUnavailableTimeMessage extends OutgoingUnavailableTimeMessage {
	

	public override void processResponse()
	{
		try {
			super.processResponse();
			
			updateEventWithScheduleID();
			
			// Let a CalloutException bubble up to the calling program (RequestQueueBatchProcessor)
			// so the associated RequestQueue record stays as status Ready and can be picked up for processing again later
				
		} catch (IntegrationException ie) {
			// An IntegrationException means Telemar could not record the unavailable time.
			
			// Retrieve the event
			e = EventManager.getEvent(EventID);
			
			// Need to inform the manager who tried to create the unavailable time	
			Id managerid;
			// default to the eastern time zone but derive for the recipient of the chatter post
			String timeZoneSidKey = EventUtilities.TIME_ZONE_NEW_YORK;
			User uprof = [Select Id,ManagerId,Profile.Name,UserRoleId,Name, TimeZoneSidKey from User where Id = :e.OwnerId];
			if (ProfileHelper.isManager(uprof.Profile.Name))
			{
				managerid = u.Id;
				timeZoneSidKey = u.TimeZoneSidKey;
			}
			else
			{
				List<User> mgrs = Utilities.getManagersForRole(uprof.UserRoleId);
				if (mgrs.size() > 0) {
					managerid = mgrs[0].Id;
					timeZoneSidKey = mgrs[0].TimeZoneSidKey;
				}	
			}
			
			// Format date and times according to the manager's time zone
			String startTime = e.StartDateTime.format('h:mm a', timeZoneSidKey);
			String endTime = e.EndDateTime.format('h:mm a', timeZoneSidKey);
			String eventDate = e.StartDateTime.format('M/d/yyyy', timeZoneSidKey);
			
			// Create the Chatter notification 
			String bodytext = 'The time ' + startTime + ' - ' + endTime + ' on ' + eventDate + ' could not be blocked as unavailable for ' + uprof.Name + ' in Telemar.';
			//ChatterUtilities.chatterPost(managerId, bodytext, true);
			EmailMessageUtilities.SendEmailNotification(managerId, 'Unavailable Time could not be blocked for' + uprof.Name, bodytext, null, true);
			
			// Then delete the event
			EventManager.deleteEvent(e);
		}	
	
	}
	
	private void updateEventWithScheduleID() {
	
		try {
			// Need to retrieve the event
			e = EventManager.getEvent(EventID); 
		
			if (sid != null)
			{
				e.ScheduleID__c = sid;
				EventManager.updateEvent(e);
			}
		} catch (Exception ex)	{
			
			//attempt chatter notification
			try
			{
				Id managerid;
				User uprof = [Select Id,ManagerId,Profile.Name,UserRoleId from User where Id = :e.OwnerId];
				if (ProfileHelper.isManager(uprof.Profile.Name))
				{
					managerid = u.Id;
				}
				else
				{
					List<User> mgrs = Utilities.getManagersForRole(uprof.UserRoleId);
					if (mgrs.size() > 0) {
						managerid = mgrs[0].Id;
					}	
				}
				
				String bodytext = 'A system failure occurred while synchronizing unavailable time (Id: ' + e.Id + ' ) with Telemar.  Please contact support.';
				//ChatterUtilities.chatterPost(managerId, bodytext, true);
				EmailMessageUtilities.SendEmailNotification(managerId, 'Unable to synchronize unavailable time', bodytext, null, true);
				
			} catch (Exception chatterEx) {
				System.debug('Unable to send Chatter notification: ' + chatterEx.getMessage() + ' ' + ex.getStackTraceString());
			}
			
		}
	}	
			

}