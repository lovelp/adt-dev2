/*
* Description: Apex controller for reprocessing webleads.
*
* Created By               Date           Change log
* -------------------------------------------------------
* Srinivas Yarramsetti     11/02/2018     Intial version
*/

public class WebleadsReprocessingController{
    public String selectedGroup{get;set;}
    public String jsonString{get;set;}
    public String errType{get;set;}

    public WebleadsReprocessingController(ApexPages.StandardSetController controller){
        
    }
    
    //Prepare the select options
    public list<SelectOption> getgroupIdOption(){
        list<SelectOption> options = new list<SelectOption>();
        Map<String, Integer> reprocessMap = new Map<String, Integer>();
        Map<String, set<Id>> selOptMap = new Map<String, set<Id>>();
        Map<String, set<Id>> tempDMap = new Map<String, set<Id>>();
        for(AuditLog__c al:[SELECT id, Error__c, ZipCode__c, PromotionCode__c, CreatedDate FROM AuditLog__c WHERE Status__c = 'Form Error' ORDER BY CreatedDate DESC LIMIT 50000]){
            for(String ls:al.Error__c.split(';')){
                //Group all postal code issues.
                if(ls.trim() == 'POSTALCODE_ERROR'){
                    if(selOptMap.containskey('POSTALCODE_ERROR: '+al.ZipCode__c)){
                        selOptMap.get('POSTALCODE_ERROR: '+al.ZipCode__c).add(al.id);
                    }
                    else{
                        set<Id> idlist = new set<Id>();
                        idlist.add(al.Id);
                        selOptMap.put('POSTALCODE_ERROR: '+al.ZipCode__c,idlist);
                     }
                }
                if(ls.trim() == 'DNIS_ERROR'){
                    if(selOptMap.containskey('DNIS_ERROR: '+al.PromotionCode__c)){
                        selOptMap.get('DNIS_ERROR: '+al.PromotionCode__c).add(al.id);
                    }
                    else{
                        set<Id> idlist = new set<Id>();
                        idlist.add(al.Id);
                        //logic to bring up the DNIS before postalCode
                        tempDMap.clear();
                        tempDMap.put('DNIS_ERROR: '+al.PromotionCode__c,idlist);//put new DNIS error in the map.
                        tempDMap.putAll(selOptMap);//then add all the old dnis.
                        selOptMap.clear();//Clear the existing map.
                        selOptMap.putAll(tempDMap);//Add all the temp data.
                    }
                }
            }
        }
        system.debug('##'+selOptMap);
        if(selOptMap.size() > 0){
            for(String st: selOptMap.keyset()){
                String str ='';
                Integer count = 0;
                for(Id au: selOptMap.get(st)){
                    if(String.isBlank(str)){
                        str = au;
                        count++;
                    }
                    else{
                        str = str+','+au;
                        count++;
                    }
                }
                if(options.size()<1000)
                    options.add(new selectoption(str,st));
                reprocessMap.put(str,count);
                
            }
        }
        jsonString = JSON.serialize(reprocessMap);
        system.debug('##'+jsonString);
        return options;
    }
    
    //Process batch job here
    public pageReference processBatch(){
        String selGroup = selectedGroup;
        system.debug('Selected Err'+errType);
        WebLeadsReprocessBatch myBatchObject = new WebLeadsReprocessBatch(selGroup,errType); 
        Id batchId = Database.executeBatch(myBatchObject,100);
        Schema.Describesobjectresult res = Schema.Sobjecttype.AuditLog__c;
        PageReference pageRef = new PageReference('/'+res.getKeyPrefix()+'/o' ); 
        pageRef.setRedirect(true);
        return pageRef; 
    }
}