/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : TermsandConditionsController is a controller class for TermsandConditions.page, which redirects from the Terms & Conditions tab to the edit or new page.
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Erin McGee      07/08/2013    - Original Version
*
*                           
*/
public with sharing class TermsandConditionsController
{
    public TermsandConditionsController() 
    {
        
    }

    public PageReference redirect()
    {
        List<Terms_and_Condition__c> tcs= [Select Id From Terms_and_Condition__c];

        if(tcs.isEmpty())
        {
            return new PageReference('/'+Schema.SObjectType.Terms_and_Condition__c.getKeyPrefix()+'/e');
        }
        else
        {
            return new PageReference('/'+tcs.get(0).Id+'/e');
        }
    }
}