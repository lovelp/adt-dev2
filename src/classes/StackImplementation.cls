public with sharing class StackImplementation {
   private List<String> stringList;
   
   public StackImplementation() {
    stringList = new List<String>();
   }
   
   public void push(String str) {
    stringList.add(str);
   }
   
   public String pop(){
    if(!isEmpty()){
        return stringList.remove(size()-1);
    }
    else {
        return null;
    }
   }
   
   public boolean isEmpty(){
    return (stringList.size() == 0);
   }
   
   public String peek(){
    if(!isEmpty()){
        return (stringList.get(size()-1));
    }
    else {
        return null;
    }
   }
      
   public Integer size() {
    return stringList.size();
   }
   
   public void printStack(){
       
       for(String s:StringList){}
           //system.debug(s);
       
   }
   
    
}