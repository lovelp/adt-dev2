/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : Test class for QuoteJobTriggerHandler
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari             03/06/2019          - Origininal Version (HRM-9453 - eContract Signed notification to Submitted by Rep and his Manager)
*                                                
*/
@isTest
private class QuoteJobTriggerHandlerTest {

    static testMethod void testCreateQuoteJob() {
        User salesRep = TestHelperClass.createSalesRepUser();
		Account a1;
		Account a2;
		Opportunity opp;
		scpq__SciQuote__c sq;
		Quote_Job__c qj;
		System.runAs(salesRep) {
            list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
            rgvs.add(new ResaleGlobalVariables__c(Name='DataRecastDisableAccountTrigger', value__c='FALSE'));
            rgvs.add(new ResaleGlobalVariables__c(Name='IntegrationUser', value__c='Integration User'));
            rgvs.add(new ResaleGlobalVariables__c(Name='RIFAccountOwnerAlias', value__c='ADT RIF'));
            rgvs.add(new ResaleGlobalVariables__c(Name='DataConversion', value__c='FALSE'));
            rgvs.add(new ResaleGlobalVariables__c(Name='ContractSignedNotificationPackages', value__c=' IDPackage , IDPackage1, IDPackage2, '));
            insert rgvs;
            
			// create accounts
			a1 = TestHelperClass.createAccountData();
			opp = New Opportunity();
			opp.AccountId = a1.Id;
			opp.Name = 'TestOpp1';
			opp.StageName = 'Prospecting';
			opp.CloseDate = Date.today();
			insert opp;
		}
    	
		Test.startTest();
		
		System.runAs(salesRep) {
			sq = New scpq__SciQuote__c();
			sq.Name = 'TestQuote1';
			sq.scpq__OpportunityId__c = opp.Id;
			sq.scpq__OrderHeaderKey__c = 'whoknows';
			sq.scpq__SciLastModifiedDate__c = Date.today();
			sq.scpq__Status__c = 'Created';
			sq.Activity_Data__c = '<?xml version="1.0" encoding="UTF-8"?><Activity>    <DOAApproval>        <Job ApprovalComment="Test DOA TEST" ApproverLevel="Director"            Family="Home Security" HoldRequired="true"            InstallPercent="45.00" MonitorPercent="0.00" QSPPercent="0.00">            <Package Name="[L1 001] - Traditional"/>        </Job>    </DOAApproval>   <DepositWaived>        <Job ApprovalComment="Test DOA TEST" ApproverLevel="Director"            Family="Home Security" HoldRequired="true"            InstallPercent="45.00" MonitorPercent="0.00" QSPPercent="0.00">            <Package Name="[L1 001] - Traditional"/>        </Job>    </DepositWaived>   <EmailActivity>        <Job ApprovalComment="Test DOA TEST" ApproverLevel="Director"            Family="Home Security" HoldRequired="true"            InstallPercent="45.00" MonitorPercent="0.00" QSPPercent="0.00">            <Package Name="[L1 001] - Traditional"/>        </Job>    </EmailActivity></Activity>';
			insert sq;
			/*sq.scpq__Status__c = 'Paid';
			update sq;*/
			qj = new Quote_Job__c();
            qj.Job_Status__c = 'submitted';
            qj.ParentQuote__c = sq.id;
            qj.PackageID__c = 'IDPackage';
            insert qj; 
		}
		scpq__SciQuote__c sq2 = [select Id, Name, Contains_Cyber_Orders__c from scpq__SciQuote__c where id = :sq.id];
		
		//System.assert(sq2.Contains_Cyber_Orders__c != null, 'Account ID should not be null');
		System.assertEquals(true, sq2.Contains_Cyber_Orders__c, 'Contains Cyber Orders should be true');
		
		Test.stopTest();

    }
}