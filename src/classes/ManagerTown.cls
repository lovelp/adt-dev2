/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : ManagerTown.cls is a controller for ManagerTowns visualforce page. enables load, edit and save for manager-towns
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli				10/15/2012			- Origininal Version
*
* Sunil Addepalli				02/01/2012			- Modified to accomodate channel based assignments
*													- Modified UI and related changes
*													 
*/

public with sharing class ManagerTown {
	
	//indicates step 1 of the 2 step process
	public Boolean isStep1 {get; set;}
	//indicates step 2 of the 2 step process
	public Boolean isStep2 {get; set;}
	//manager id selected from step 1
	public String selectedManagerId {get; set;}
	//All available managers to select a manager from (shown in step 1)
	public List<SelectOption> managerSelectOptions {get; set;}
	//List of Manager town structure objects (details of all the towns and options selected for a manger)
	public List<ManagerTownStructure> mts {get;set;}
	//manager towns before editing starts
	public List<ManagerTown__c> originalManagerTowns {get; set;}
	public List<ManagerTown__c> originalManagerTownsForPostProcessing {get; set;}
	//all towns deleted from a manager
	public String removedMT {get; set;}
	//selected area (used when selecting additional towns to add to a manager)
	public String selectedArea {get; set;}
	//indicates that user is in step 2 but needs towns list to be shown
	public Boolean refreshedForAddingTowns {get; set;}
	//List of all the new towns to be added to the manager town struct list
	String[] selectedTownsToAddToManager = new String[]{};
	//all available towns list used for creating multiselect checkboxes
	public List<SelectOption> availableTowns {get; set;}
	//sets value to true for an inactive manager
	public Boolean isManagerActive {get; set;}
	//page title - changes based on manager selected
	public String pageTitle {get; set;}
	//a dummy variable for manager search text box
	public String managerSearch {get; set;}
	//Phase 2 setup - This variable is used to enable MT for inactive managers
	public Boolean inactiveManagerOverride {get; set;}
	
	private final String BUSINESSID_RESIDENTIAL = '1100';
	private final String BUSINESSID_SMALLBUSINESS = '1200';
	private final String BUSINESSID_CommercialBUSINESS = '1300';
	public ManagerTown()
	{
		if(ResaleGlobalVariables__c.getInstance('EnableManagerTownForInactiveManagers').value__c == 'TRUE')
		{
			inactiveManagerOverride = true;
		}
		else
		{
			inactiveManagerOverride = false;
		}
		if(selectedManagerId == null || selectedManagerId == '')
		{
			pageTitle = 'Manager Town';
			isStep2 = false;
			isStep1 = true;	
			isManagerActive = true;		
			List<User> allManagers = new List<User>();
			//allManagers = ProfileHelper.getAllActiveManagers();
			allManagers = ProfileHelper.getAllManagers();
			managerSelectOptions = new List<SelectOption>();
			String inactiveString = ' (Inactive)';
			for(User mgr : allManagers)
			{
				if(mgr.IsActive){
					managerSelectOptions.add(new SelectOption(mgr.id, mgr.Name));
				}
				else {
					managerSelectOptions.add(new SelectOption(mgr.id, mgr.Name + inactiveString));
				}
			}
		}
	} 
	
	public pageReference next()
	{
		if(selectedManagerId == null || selectedManagerId == '')
		{
			pageTitle = 'Manager Town';
			isStep2 = false;
			isStep1 = true;
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMessages__c.getinstance('MT_SM').Message__c);
			ApexPages.addMessage(myMsg);
		}
		else
		{
			isStep2 = true;
			isStep1 = false;
			User Mgr = [select id, Name, isActive from User where id = : selectedManagerId];
			if(Mgr.isActive) {
				isManagerActive = true;
				pageTitle = 'Towns for ' + Mgr.Name;
			} else {
				if(inactiveManagerOverride)
				{
					isManagerActive = true;
				}
				else
				{
					isManagerActive = false;
				}
				pageTitle = 'Towns for ' + Mgr.Name + ' (Inactive)';
			}
			BuildExistingManagerTowns();
		}
		return null;
	}
	
	public pageReference save()
	{
		Boolean hasError = false;
		for(ManagerTownStructure msi : mts)
		{
			if(!msi.isValidMSI()){
				system.debug('######');
				hasError = true;
			}
		}
		system.debug(hasError);
		if(hasError)
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMessages__c.getinstance('MT_IMT').Message__c);
			ApexPages.addMessage(myMsg);
		}
		else
		{
			List<ManagerTown__c> allMgrTownsToSave = new List<ManagerTown__c>();
			List<ManagerTown__c> DeleteMTs = new List<ManagerTown__c>();
			Map<id, ManagerTown__c> existingMTMap = new Map<Id, ManagerTown__c>();
			ManagerTown__c indivMT;
			Set<Id> allMTownIds = new Set<Id>();
			for(ManagerTownStructure msi : mts)
			{
				if(msi.ManagerTownId != null)
					allMTownIds.add(msi.ManagerTownId);
			}		
			for(ManagerTown__c mt : originalManagerTowns)
			{
				existingMTMap.put(mt.id, mt);
				if(!allMTownIds.contains(mt.id))
					DeleteMTs.add(mt);
			}
	
			
			for(ManagerTownStructure msi : mts)
			{
				indivMT = new ManagerTown__c();
				if(msi.ManagerTownId != null && existingMTMap.get(msi.ManagerTownId) != null)
					indivMT = existingMTMap.get(msi.ManagerTownId);
				indivMT.ManagerId__c = selectedManagerId;
				indivMT.BusinessID__c = msi.BusinessId;
				indivMT.TownID__c = msi.TownId;
				indivMT.Type__c = '';
				if(msi.isResiDirect)
				{
					if(indivMT.Type__c == '')
						indivMT.Type__c += Channels.TYPE_RESIDIRECT;
					else
						indivMT.Type__c += ';' + Channels.TYPE_RESIDIRECT;
				}
				if(msi.isResiResale)
				{
					if(indivMT.Type__c == '')
						indivMT.Type__c += Channels.TYPE_RESIRESALE;
					else
						indivMT.Type__c += ';' + Channels.TYPE_RESIRESALE;
				}
				if(msi.isSBDirect)
				{
					if(indivMT.Type__c == '')
						indivMT.Type__c += Channels.TYPE_SBDIRECT;
					else
						indivMT.Type__c += ';' + Channels.TYPE_SBDIRECT;
				}			
				if(msi.isSBResale)
				{
					if(indivMT.Type__c == '')
						indivMT.Type__c += Channels.TYPE_SBRESALE;
					else
						indivMT.Type__c += ';' + Channels.TYPE_SBRESALE;
				}
				if(msi.isCustomHomeSales)
				{
					if(indivMT.Type__c == '')
						indivMT.Type__c += Channels.TYPE_CUSTOMHOMESALES;
					else
						indivMT.Type__c += ';' + Channels.TYPE_CUSTOMHOMESALES;
				}
				if(msi.isHomeHealth)
				{
					if(indivMT.Type__c == '')
						indivMT.Type__c += Channels.TYPE_HOMEHEALTH;
					else
						indivMT.Type__c += ';' + Channels.TYPE_HOMEHEALTH;
				}				
				if(msi.isCBDirect)
				{
					if(indivMT.Type__c == '')
						indivMT.Type__c += Channels.TYPE_BUSINESSDIRECTSALES;
					else
						indivMT.Type__c += ';' + Channels.TYPE_BUSINESSDIRECTSALES;
				}		
				if(msi.isCBNational)
				{
					if(indivMT.Type__c == '')
						indivMT.Type__c += Channels.TYPE_NATIONAL;
					else
						indivMT.Type__c += ';' + Channels.TYPE_NATIONAL;
				}							
				allMgrTownsToSave.add(indivMT);									
			}
			upsert allMgrTownsToSave;
			delete DeleteMTs;
			
			ProcessAllAccounts();
			
			PageReference pref = ApexPages.currentPage();
			pref.setRedirect(true);
			return pref;
		}
		return null;
	}
	
	public void ProcessAllAccounts()
	{
 		List<ManagerTown__c> newManagerTowns = [Select Id, ManagerId__c, BusinessId__c, TownId__c, Type__c From ManagerTown__c where ManagerId__c = : selectedManagerId];
 
 		Map<String, ManagerTown__c> newMTsMap = new Map<String, ManagerTown__c>();
 		Map<String, ManagerTown__c> oldMTsMap = new Map<String, ManagerTown__c>();

 		set<String> addedTMTypes = new Set<String>();
 		set<String> deletedTMTypes = new Set<String>();
 		
 		Set<String> oldTypes1;
 		Set<String> newTypes1;
 		Set<String> oldTypes2;
 		Set<String> newTypes2;
 		ManagerTown__c tempMT;
 		
 		for(ManagerTown__c nmt : newManagerTowns)
 		{
			newMTsMap.put(nmt.TownId__c + '-' + nmt.BusinessId__c, nmt);
 		}
 		for(ManagerTown__c omt : originalManagerTownsForPostProcessing)
 		{
			oldMTsMap.put(omt.TownId__c + '-' + omt.BusinessId__c, omt);
 		}
 		//find all the new manager towns added in the process
 		for(ManagerTown__c nmt : newManagerTowns)
 		{
 			oldTypes1 = new Set<String>();
 			newTypes1 = new Set<String>();
 			oldTypes2 = new Set<String>();
 			newTypes2 = new Set<String>();
 			tempMT = new  ManagerTown__c();
 			if(oldMTsMap.get(nmt.townId__c + '-' + nmt.BusinessId__c) == null)
 			{
	 			for(string typ : nmt.Type__c.split(';'))
				{
					addedTMTypes.add(nmt.townId__c + ';' + nmt.BusinessId__c + ';' + typ);
				}
 			}
 			else //if types are changed on existing towns
 			{
 				tempMT = oldMTsMap.get(nmt.townId__c + '-' + nmt.BusinessId__c);
 				for(String oldTyp : tempMT.Type__c.split(';'))
 				{
 					oldTypes1.add(oldTyp);
 					oldTypes2.add(oldTyp);
 				}
 				for(String newTyp : nmt.Type__c.split(';'))
 				{
 					newTypes1.add(newTyp);
 					newTypes2.add(newTyp);
 				}
 				oldTypes1.removeAll(newTypes1);
 				if(oldTypes1.size() > 0) //there were some removed types
 				{
 					for(String st : oldTypes1)
 						deletedTMTypes.add(nmt.townId__c + ';' + nmt.BusinessId__c + ';' + st);
 				}
 				newTypes2.removeAll(oldTypes2);
 				if(newTypes2.size() > 0)//there are some newly added types
 				{
 					for(String st : newTypes2)
 						addedTMTypes.add(nmt.townId__c + ';' + nmt.BusinessId__c + ';' + st);
 				}
 			}
 		}
 		//find all the old manager towns that were deleted
 		for(ManagerTown__c omt : originalManagerTownsForPostProcessing)
 		{
 			if(newMTsMap.get(omt.townId__c + '-' + omt.BusinessId__c) == null)
 			{
 				for(string typ : omt.Type__c.split(';'))
				{
 					deletedTMTypes.add(omt.townId__c + ';' + omt.BusinessId__c + ';' + typ);
				}
 			}
 		}
 		
 		//for all the added manager town types, create a record in the managertown queue.
 		ManagerTownRealignmentQueue__c mtq;
 		List<ManagerTownRealignmentQueue__c> mtqs = new List<ManagerTownRealignmentQueue__c>();
 		for(String addedTypes : addedTMTypes)
 		{
 			mtq = new ManagerTownRealignmentQueue__c();
 			mtq.BusinessId__c = addedTypes.split(';')[1];
 			mtq.TownId__c = addedTypes.split(';')[0];
 			mtq.Channel__c = addedTypes.split(';')[2];
 			mtq.OldOwnerId__c = Utilities.getGlobalUnassignedUser();
 			mtq.NewOwnerId__c = selectedManagerId;
 			mtqs.add(mtq);
 		}
 		
 		//for all the deleted manager towns types, check if another manager has the town + businessid + channel combination assigned.
 		for(String deletedTypes : deletedTMTypes)
 		{
 			//[TOTO] - Change this to make this one query for all
 			mtq = new ManagerTownRealignmentQueue__c();
 			mtq.BusinessId__c = deletedTypes.split(';')[1];
 			mtq.TownId__c = deletedTypes.split(';')[0];
 			mtq.Channel__c = deletedTypes.split(';')[2];
 			List<ManagerTown__c> MT = [Select id, ManagerID__c from ManagerTown__c where TownId__c = : mtq.TownId__c and BusinessId__c = : mtq.BusinessId__c and Type__c includes (: mtq.Channel__c)];
 			if(MT.size() > 0)
 			{
 				mtq.OldOwnerId__c = selectedManagerId;
 				mtq.NewOwnerId__c = MT[0].ManagerId__c;
 			}
 			else
 			{
	 			mtq.OldOwnerId__c = selectedManagerId;
	 			mtq.NewOwnerId__c = Utilities.getGlobalUnassignedUser();			
 			}
 			mtqs.add(mtq);
 		}
 		insert mtqs;
	}
	
	public pageReference startover()
	{
		PageReference pref = ApexPages.currentPage();
		pref.setRedirect(true);
		return pref;
	}
	
	public pageReference retrieveTowns()
	{
		if(selectedArea == null)
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMessages__c.getinstance('MT_SA').Message__c);
			ApexPages.addMessage(myMsg);		
		}
		else
		{
			refreshedForAddingTowns = true;
			availableTowns = new List<SelectOption>();
			String areaid = selectedArea.split(' - ')[0];
			String areaname = selectedArea.split(' - ')[1].replace('(', '=').replace(')', '').split(' =')[0];
			String businessId = selectedArea.split(' - ')[1].replace('(', '=').replace(')', '').split(' =')[1];
			Map<String, String> TownMap = new Map<String, String>();
			for (List<Postal_Codes__c> zips: [Select Id, Name, TownId__c, Town__c, BusinessId__c, TownUniqueId__c From Postal_Codes__c where Area__c = :areaname and AreaId__c = : areaid and BusinessId__c = : businessId]) {
		        for(Postal_Codes__c z : zips) {
					TownMap.put(z.TownUniqueId__c, 'Town Id:' + z.TownId__c + '; Town Name:' + z.Town__c + '; BusinessId: ' + z.BusinessId__c);
		        }
		    }			
			for (String tinfo : TownMap.keySet()){
            	availableTowns.add(new SelectOption(tinfo, TownMap.get(tinfo)));
        	}		    
		}
		return null;
	}
	
	public pageReference addSelectedTowns()
	{
		if(selectedTownsToAddToManager.size() == 0)
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMessages__c.getinstance('MT_ST').Message__c);
			ApexPages.addMessage(myMsg);
		}
		else
		{
			Boolean townExists = false;
			set<String> currentTowns = new set<String>();
			for(ManagerTownStructure mt : mts)
			{
				currentTowns.add(mt.TownId + '-' + mt.BusinessId);
			}
			for(String selectedTown : selectedTownsToAddToManager)
			{
				if(currentTowns.contains(selectedTown))
					townExists = true;
			}
			if(townExists)
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMessages__c.getinstance('MT_TAS').Message__c);
				ApexPages.addMessage(myMsg);				
			}
			else
			{
				for(String selectedTown : selectedTownsToAddToManager)
				{
					ManagerTownStructure msi = new ManagerTownStructure();
					msi.BusinessId = selectedTown.split('-')[1];
					msi.TownId = selectedTown.split('-')[0];
					msi.EnableTypes();
					msi.CheckAllAvailableTypes();
					mts.add(msi);
				}
				refreshedForAddingTowns = false;
				selectedTownsToAddToManager = new String[]{};
			}
		}
		return null;
	}
	
	public String[] getselectedTownsToAddToManager() {
        return this.selectedTownsToAddToManager;
    }	
	
    public void setselectedTownsToAddToManager(String[] selectedTs) {
        this.selectedTownsToAddToManager = selectedTs;
    }	
	
	public pageReference deleteTown()
	{
		Integer index = -1;
		Integer deletedIndex = 0;
		for(ManagerTownStructure mti : mts)
		{
			index++;
			if(mti.TownId + ';' + mti.BusinessId == removedMT)
				deletedIndex = index;
		}
		mts.remove(deletedindex);
		return null;
	}
	
    public List<SelectOption> getAreaItems() 
    {
        List<SelectOption> options = new List<SelectOption>();
    	// Get all the areas from the 99999 postal code
		Postal_Codes__c pc = [select id, All_Areas__c from Postal_Codes__c where Name = '99999'];
    	String allAreas = pc.All_Areas__c;
    	if(allAreas != null && allAreas != '')
    	{
	    	List<String> parts = allAreas.split('~');
	        for(String part : parts)
	        {
	        	options.add(new SelectOption(part, part));
	        }
    	}
    		
        return options;
    }	
	
	private void BuildExistingManagerTowns()
	{
		originalManagerTowns = [Select Id, ManagerId__c, BusinessId__c, TownId__c, Type__c From ManagerTown__c where ManagerId__c = : selectedManagerId];
		originalManagerTownsForPostProcessing = originalManagerTowns.deepClone();
		mts = new List<ManagerTownStructure>();
		ManagerTownStructure mti;
		String TypesSelected = '';
		List<String> TypesSelectedList;
		Set<String> TypeSelectedSet;
		for(ManagerTown__c MT : originalManagerTowns)
		{
			TypesSelectedList = new List<String>();
			TypeSelectedSet = new Set<String>();
			mti = new ManagerTownStructure();
			mti.TownId = MT.TownId__c;
			mti.BusinessId = MT.BusinessId__c;
			mti.isManagerActive = isManagerActive;
			TypesSelected = MT.Type__c;
			mti.ManagerTownId = MT.id;
			if(TypesSelected.length() != 0)
			{
				TypesSelectedList = TypesSelected.split(';');
				for(String tsl : TypesSelectedList)
					TypeSelectedSet.add(tsl);
				system.debug('###########' + TypeSelectedSet);
				if(TypeSelectedSet.contains(Channels.TYPE_RESIDIRECT))
					mti.isResiDirect = true;
				if(TypeSelectedSet.contains(Channels.TYPE_SBDIRECT))
					mti.isSBDirect = true;
				if(TypeSelectedSet.contains(Channels.TYPE_RESIRESALE))
					mti.isResiResale = true;
				if(TypeSelectedSet.contains(Channels.TYPE_SBRESALE))
					mti.isSBResale = true;
				if(TypeSelectedSet.contains(Channels.TYPE_CUSTOMHOMESALES))
					mti.isCustomHomeSales = true;
				if(TypeSelectedSet.contains(Channels.TYPE_RESIDIRECTPHONESALES))
					mti.isResiDirectPhoneSales = true;
				if(TypeSelectedSet.contains(Channels.TYPE_SBDIRECTPHONESALES))
					mti.isSBDirectPhoneSales = true;
				if(TypeSelectedSet.contains(Channels.TYPE_RESIRESALEPHONESALES))
					mti.isResiResalePhoneSales = true;
				if(TypeSelectedSet.contains(Channels.TYPE_SBRESALEPHONESALES))
					mti.isSBResalePhoneSales = true;
				if(TypeSelectedSet.contains(Channels.TYPE_HOMEHEALTH))
					mti.isHomeHealth = true;					
				if(TypeSelectedSet.contains(Channels.TYPE_BUSINESSDIRECTSALES))
					mti.isCBDirect = true;
				if(TypeSelectedSet.contains(Channels.TYPE_NATIONAL))	
					mti.isCBNational = true;				
			}
			mti.enabletypes();
			mts.add(mti);
			TypesSelected = '';
		}
	}
}