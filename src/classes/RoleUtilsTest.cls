@isTest
private class RoleUtilsTest {
    
    static testMethod void getParentNodeOfUserTest() {
        
        User SalesRep;
        
        // Init management hierarchy
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        test.startTest();
        system.runas(current) {
            SalesRep = TestHelperClass.createExecWithTeam();
            SalesRep.Business_Unit__c = 'Resi Direct Sales';
            update SalesRep;
        }
        
        RoleUtils.RoleNodeWrapper n = RoleUtils.getParentNodeOfUser( SalesRep.Id );
        
        RoleUtils.RoleNodeWrapper n1 = RoleUtils.getRootNodeWithFullTree();
        
        String s1 = RoleUtils.getTreeJSON( SalesRep.Id, -1 );
        test.stopTest();
        
    }
    
    static testMethod void testRoleUtils(){
        Boolean b1,b2,b3;
        String str1,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12;
        
        /*RoleUtils.HierarchyLevelSMB ruhlsmb=new RoleUtils.HierarchyLevelSMB();
        b1=ruhlsmb.isDirectAssigneeLevel('', true);
        b2=ruhlsmb.isLevelInBranch('');
        b3=ruhlsmb.isTopHierarchyLevel('');
        str1=ruhlsmb.getLastApprover();
        str2=ruhlsmb.getNextLowerApprover('Direct Reports to CEO');
        str3=ruhlsmb.getNextUpperApprover('Direct Reports to CEO');
        str4=ruhlsmb.getNextLowerApprover('Vice President');
        str5=ruhlsmb.getNextUpperApprover('Vice President');
        str6=ruhlsmb.getNextLowerApprover('Regional Sales Director');
        str7=ruhlsmb.getNextUpperApprover('Regional Sales Director');
        str8=ruhlsmb.getNextLowerApprover('Area Sales Manager');
        str9=ruhlsmb.getNextUpperApprover('Area Sales Manager');
        str10=ruhlsmb.getNextLowerApprover('Sales Manager');
        str11=ruhlsmb.getNextUpperApprover('Sales Manager');
        
        RoleUtils.HierarchyLevelResi ruhlresi=new RoleUtils.HierarchyLevelResi();
        b1=ruhlresi.isDirectAssigneeLevel('', true);
        b2=ruhlresi.isLevelInBranch('');
        b3=ruhlresi.isTopHierarchyLevel('');
        str1=ruhlresi.getLastApprover();
        str2=ruhlresi.getNextLowerApprover('Regional General Manager');
        str3=ruhlresi.getNextUpperApprover('Regional General Manager');
        
        RoleUtils.HierarchyLevelNSC ruhlnsc=new RoleUtils.HierarchyLevelNSC();
        b1=ruhlnsc.isDirectAssigneeLevel('', true);
        b2=ruhlnsc.isLevelInBranch('');
        b3=ruhlnsc.isTopHierarchyLevel('');
        str1=ruhlnsc.getLastApprover();
        str2=ruhlnsc.getNextLowerApprover('Agent');
        str3=ruhlnsc.getNextUpperApprover('Agent');
        str4=ruhlnsc.getNextLowerApprover('Team Manager');
        str5=ruhlnsc.getNextUpperApprover('Team Manager');
        str6=ruhlnsc.getNextLowerApprover('Unit Manager');
        str7=ruhlnsc.getNextUpperApprover('Unit Manager');
        str8=ruhlnsc.getNextLowerApprover('Director');
        str9=ruhlnsc.getNextUpperApprover('Director');*/
        
        
        Map<string,user> listOfApprovers = new Map<string,user>();
        // User u = [Select Id,Rep_Team__c from User where Id = :UserInfo.getUserId()];
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'ADT NSC Sales Representative' LIMIT 1];
        User usr = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileId.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='UtilityTestUser2@testorg.com',EmployeeNumber__c='999111');
        insert usr;
        System.runAs(usr){
            Profile pro = [SELECT Id FROM Profile WHERE Name = 'ADT NSC Sales Representative' LIMIT 1];
            User usrS = new User(Alias = 'standt', Email='standarduser21@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = pro.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='UtilityTestUser21@testorg.com',EmployeeNumber__c='9999111');
            insert usrS;
            Test.startTest();
            RoleUtils.HierarchyLevelGlobal ruhlglobal=new RoleUtils.HierarchyLevelGlobal();
            b1=ruhlglobal.isDirectAssigneeLevel('', true);
            b2=ruhlglobal.isLevelInBranch('');
            b3=ruhlglobal.isTopHierarchyLevel('');
            str6=ruhlglobal.getNextLowerApprover('Regional Vice President');
            str7=ruhlglobal.getNextUpperApprover('Regional Vice President');
            str8=ruhlglobal.getNextLowerApprover('Area General Manager');
            str9=ruhlglobal.getNextUpperApprover('Area General Manager');
            str10=ruhlglobal.getNextLowerApprover('Sales Manager');
            str11=ruhlglobal.getNextUpperApprover('Sales Manager');
            str12 = ruhlglobal.getLowerApprovalLevels('Sales Manager');
            listOfApprovers.put(usrS.id,usrS);
            DOA_Request__c doaReq = new DOA_Request__c();
            doaReq.name = 'Test';
            doaReq.Sales_Rep__c = usrS.id;
            doaReq.DOAApproverLevel__c = 'Director';
            //insert doaReq;
            List<DOA_Request__c> doaReqList = new List<DOA_Request__c>();
            doaReqList.add(doaReq);
            //insert doaReqList;
            ruhlglobal.sendEmailNotificationToApprovers(listOfApprovers,doaReqList);
            Test.stopTest();
        }
        /*RoleUtils.HierarchyLevelCRD ruhlcrd=new RoleUtils.HierarchyLevelCRD();
        b1=ruhlcrd.isDirectAssigneeLevel('', true);
        b2=ruhlcrd.isLevelInBranch('');
        b3=ruhlcrd.isTopHierarchyLevel('');
        str1=ruhlcrd.getLastApprover();
        str2=ruhlcrd.getNextLowerApprover('CRD Agent');
        str3=ruhlcrd.getNextUpperApprover('CRD Agent');
        str4=ruhlcrd.getNextLowerApprover('CRD Team Manager');
        str5=ruhlcrd.getNextUpperApprover('CRD Team Manager');
        str8=ruhlcrd.getNextLowerApprover('CRD Unit Manager');
        str9=ruhlcrd.getNextUpperApprover('CRD Unit Manager');
        str10=ruhlcrd.getNextLowerApprover('CRD Sr Vice President');
        str11=ruhlcrd.getNextUpperApprover('CRD Sr Vice President');*/

        
        User u=testHelperClass.createUser(234343);
    }
    
    static testMethod void testHierarchyLevelMatrix(){
        User u = [Select Id,Rep_Team__c from User where Id = :UserInfo.getUserId()];
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'ADT NSC Sales Representative' LIMIT 1];
        User usr = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileId.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='UtilityTestUser2@testorg.com',EmployeeNumber__c='999111');
        insert usr;
        System.runAs(usr){
            Test.startTest();
            RoleUtils.HierarchyLevelMatrix hLMatrix = new RoleUtils.HierarchyLevelMatrix(usr);
            hlMatrix.getFirstApprover();
             hlMatrix.getLastApprover();
            hlMatrix.isDirectAssigneeLevel('Agent',true);
            hlMatrix.isTopHierarchyLevel('Agent');
            hlMatrix.isLevelInBranch('Agent');
            hlMatrix.getNextLowerApprover('Unit Manager');
            hlMatrix.getNextUpperApprover('Unit Manager');
            hlMatrix.getNextLowerApprover('Area General Manager');
            hlMatrix.getNextUpperApprover('Area General Manager');
            Test.stopTest();
        }
    }
    static testMethod void testHierarchyLevel(){
    //RoleUtils.getUserAsigneeHierarchy('Resi');
         User u = [Select Id,Rep_Team__c from User where Id = :UserInfo.getUserId()];
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'ADT NSC Sales Representative' LIMIT 1];
        User usr = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileId.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='UtilityTestUser2@testorg.com',EmployeeNumber__c='999111');
        insert usr;
        System.runAs(usr){
            Test.startTest();
            RoleUtils.getUpperApproverLevel('Vice_President');
            //RoleUtils.getUserAsigneeHierarchy(RoleUtils.HierarchyBusinessUnit.Resi);
            
            //RoleUtils.HierarchyLevel hL = new RoleUtils.HierarchyLevel();
            Test.stopTest();
        }
    }
    static testMethod void testHierarchyLevelAssignee(){
        //RoleUtils.getUserAsigneeHierarchy('Resi');
        User u = [Select Id,Rep_Team__c from User where Id = :UserInfo.getUserId()];
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'ADT NSC Sales Representative' LIMIT 1];
        User usr = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileId.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='UtilityTestUser2@testorg.com',EmployeeNumber__c='999111');
        insert usr;
        System.runAs(usr){
            RoleUtils.getUserAsigneeHierarchy(RoleUtils.HierarchyBusinessUnit.Resi);
        }
    }
     static testMethod void testHierarchyApprover(){
        //RoleUtils.getUserAsigneeHierarchy('Resi');
        User u = [Select Id,Rep_Team__c from User where Id = :UserInfo.getUserId()];
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'ADT NSC Sales Representative' LIMIT 1];
        User usr = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileId.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='UtilityTestUser2@testorg.com',EmployeeNumber__c='999111');
        insert usr;
        System.runAs(usr){
            RoleUtils.getRepApproverUserByLevel(usr.Id,RoleUtils.HierarchyBusinessUnit.Resi);
        }
    }
    static testMethod void testHierarchyNextAssignee(){
        //RoleUtils.getUserAsigneeHierarchy('Resi');
        UserRole uR = [select id,name from userRole where Name = 'ADT Business'];
        User u = [Select Id,Rep_Team__c from User where Id = :UserInfo.getUserId()];
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'ADT NSC Sales Representative' LIMIT 1];
        User usr = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileId.Id,  UserRoleId = uR.Id,
                            TimeZoneSidKey='America/Los_Angeles', UserName='UtilityTestUser2@testorg.com',EmployeeNumber__c='999111');
        insert usr;
        System.runAs(usr){
            Test.startTest();
            RoleUtils.getNextAvailableAsignee(usr.id);
            Map<string,user> listOfApprovers = new Map<string,user>();
            listOfApprovers.put(u.id,usr);
            RoleUtils.userByManagerLevelMap = listOfApprovers;
            Map <Id, UserRole> roleUsers = new Map <Id, UserRole>();
            roleUsers = new Map<Id, UserRole>([SELECT Id, Name, parentRoleId, 
                                                (SELECT id, name, UserRoleId, Approver_Level__c, email, IsActive, DelegatedEmail__c, ManagerId, Rep_Team__c
                                                FROM users WHERE IsActive = true ) FROM UserRole ORDER BY parentRoleId]);
            
            RoleUtils.roleUsersMap = roleUsers;
            Map <Id, List<Id>> parentChildRole = new Map <Id, List<Id>>();
            List<id> userId = new List<id>();
            userId.add(usr.id);
            parentChildRole.put(usr.id,userId);
            RoleUtils.usersByRoleMap = parentChildRole; 
            Test.stopTest();
        }
    }
    static testMethod void testHierarchySubOrdinates(){
        //RoleUtils.getUserAsigneeHierarchy('Resi');
        UserRole uR = [select id,name from userRole where Name = 'ADT Business'];
        User u = [Select Id,Rep_Team__c from User where Id = :UserInfo.getUserId()];
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'ADT NSC Sales Representative' LIMIT 1];
        User usr = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileId.Id, UserRoleId = uR.Id,
                            TimeZoneSidKey='America/Los_Angeles', UserName='UtilityTestUser2@testorg.com',EmployeeNumber__c='999111');
        insert usr;
        System.runAs(usr){
            Test.startTest();
            RoleUtils.getAllSubordinates(usr.Id);
            Test.stopTest();
        }
    }
    
}