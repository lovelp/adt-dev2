public without sharing class NSCReloController extends NSCPageControllerBase {
	
	private Enum RELO_STEP {ADDR, DISCO}
	private RELO_STEP currentReloStep;
	
	public Account acc {get;set;}
    public MMBLookupComponentController mmbLookupController { get; set; }
    public Map<Integer, Boolean> StepsVisitedMap {get;set;}
    public Boolean focusMainScreen {get;set;}
    public Boolean renderStep1 {
    	get {
    		return currentReloStep == RELO_STEP.ADDR;
    	}
    }
    public Boolean renderStep2 {
    	get {
    		return  currentReloStep == RELO_STEP.DISCO;
    	}
    }
	
    public Boolean renderMMBLookupValidation {
        get;
        set {
            // Whenever the MMB lookup is set to be rendered the filter is set on the component's controller instance
            if( value && acc != null ){
                if( Utilities.isEmptyOrNull(acc.SiteStreet__c) || Utilities.isEmptyOrNull(acc.SiteCity__c) ||  Utilities.isEmptyOrNull(acc.SiteState__c) || Utilities.isEmptyOrNull(acc.SiteState__c) ){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Cannot lookup a customer with missing site information.'));
                    renderMMBLookupValidation = false;
                }
                else{
                    mmbLookupController.fname = fname;
                    mmbLookupController.lname = lname;
                    mmbLookupController.Addr1 = Addr1;
                    mmbLookupController.Phone = Phone;
                    mmbLookupController.SiteNo = SiteNo;
                    mmbLookupController.CustNo = CustNo;
                    mmbLookupController.City = City;
                    mmbLookupController.State = State;
                    mmbLookupController.Zip = Zip;
                }
            }
            renderMMBLookupValidation = value;
        }
    } 
	
	//---------------------------------------------------------------------------------------------------
	//	Inline form values 
	//---------------------------------------------------------------------------------------------------
	public String fname {
		get;
		set{
			fname = ( !Utilities.isEmptyOrNull(value) )?value.toUpperCase():value;
		}
	}
	public String lname {
		get;
		set{
			lname = ( !Utilities.isEmptyOrNull(value) )?value.toUpperCase():value;
		}
	}
	public String Addr1 {
		get;
		set{
			Addr1 = ( !Utilities.isEmptyOrNull(value) )?value.toUpperCase():value;
		}
	}
	public String Phone {
		get;
		set{
			Phone = ( !Utilities.isEmptyOrNull(value) )?value.toUpperCase():value;
		}
	}
	public String SiteNo {
		get;
		set{
			SiteNo = ( !Utilities.isEmptyOrNull(value) )?value.toUpperCase():value;
		}
	}
	public String CustNo {
		get;
		set;
	}
	public String City {
		get;
		set{
			City = ( !Utilities.isEmptyOrNull(value) )?value.toUpperCase():value;
		}
	}
	public String State {
		get;
		set{
			State = ( !Utilities.isEmptyOrNull(value) )?value.toUpperCase():value;
		}
	}
	public String Zip {
		get;
		set;
	}
	//---------------------------------------------------------------------------------------------------
	//	End Form 
	//---------------------------------------------------------------------------------------------------
	
	public NSCReloController(){
		String accID = ApexPages.currentPage().getParameters().get('id');
		renderMMBLookupValidation = false;
		focusMainScreen = false;
		// these form values need to be initialized for xml
		SiteNo		= '';
		CustNo		= '';
		fname		= '';
		lname		= '';
		Phone		= '';	
		Addr1		= '';	
		Phone		= '';	
		Zip			= '';	
		State		= '';	
		City		= '';
		StepsVisitedMap = new Map<Integer, Boolean>{1=>true,2=>false};
		currentReloStep = RELO_STEP.ADDR;
		if( !utilities.isEmptyOrNull(accID) ){
			acc = NSCHelper.readAccountInfo(accID);
		}
	}

    /**
     *  Sets the component controller instance used for MMB loolkup
     *  @param NSCComponentControllerBase   Component controller instance received as a base class 
     */
    public override void setComponentController(NSCComponentControllerBase compController) {
        mmbLookupController = (MMBLookupComponentController)compController;
    }

    public override NSCComponentControllerBase getComponentController() {
        return mmbLookupController;
    }
	
	public PageReference submitSiteSearchForm(){
		if(  Utilities.isEmptyOrNull(Addr1) || Utilities.isEmptyOrNull(City) ||  Utilities.isEmptyOrNull(Zip) || Utilities.isEmptyOrNull(State) ){
        	ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Cannot lookup a customer with missing site information.'));
        	return null;
		}
		renderMMBLookupValidation = true;
		return null;
	}
	
	public PageReference loadAddrView(){
		currentReloStep = RELO_STEP.ADDR;
		renderMMBLookupValidation = true;
		return null;
	}
	
	public PageReference loadDisconnectView(){
		currentReloStep = RELO_STEP.DISCO;
		StepsVisitedMap.put(2, true);
		renderMMBLookupValidation = false;
		return null;
	}
	
	public PageReference loadConfirmView(){
		try{
			update acc;
			focusMainScreen = true;
		}
		catch(Exception err){
			focusMainScreen = false;
        	ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to proceed. ' +err.getMessage()));
        	return null;
		}
		return null;		
	}
	
	
}