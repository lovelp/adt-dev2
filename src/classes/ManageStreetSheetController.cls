/************************************* MODIFICATION LOG ********************************************************************************************
* ManageStreetSheetController
*
* DESCRIPTION : Used by the ManageStreetSheet VisualForce page to obtain all dynamic data for display.  
*               It is a Controller in the SFDC MVC architecture.
*               
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2012			- Original Version
*
*													
*/

global with sharing class ManageStreetSheetController {

	
	//List for the Street Sheet Items
	public List <SearchItem> ssiDetails {get; set;} 
	
	//Filtered list based on the list size variable	
	public List <SearchItem> itemsToAddList {get; set;} 
	
	// Keeps track of accounts already on Street Sheet so user can't add dups 
	public Set <String> acctIDs = new Set <String>(); 	
	
	// Lists to hold picklist values for the page
	public List<SelectOption> resultSizeValues {get; set;}
	public List<SelectOption> dataSourceValues {get; set;}
	public List<SelectOption> leadTypeValues {get; set;}
	public List<SelectOption> siteStateValues {get; set;}
	public List<SelectOption> discoReasonValues {get; set;}
	public List<SelectOption> dispoStatusValues {get;set;}
	
	// Values of user entered search criteria
	public String[] selectDataSource {get; set;} 
    public String[] selectLeadType {get; set;} 
    public Boolean selectNewMover {get; set;} 
    public Boolean selectNewLead {get;set;}        
    public String[] selectDiscoReason {get;set;} 
    public Date selectDateAssignedBefore {get;set;}
    public Date selectDateAssignedAfter {get;set;}
    public Boolean selectPhoneAvailable {get;set;}
    public String selectSiteStreet {get;set;}
    public String selectSiteCity {get;set;}
    public String[] selectSiteState {get;set;}
    public String selectSitePostalCode {get;set;}
    public Date selectDiscoDateBefore {get;set;}
    public Date selectDiscoDateAfter {get;set;}
    public Boolean selectActive {get;set;}
    public String selectedQuerySize {get;set;}
    
    //phase 2
	public Date selectNewMoverDateBefore {get;set;}				//Account.NewMoverDate__c and Lead.NewMoverDate__c
	public Date selectNewMoverDateAfter {get;set;}				//Account.NewMoverDate__c and Lead.NewMoverDate__c
	public Date selectSalesAppointmentDateBefore {get;set;}		//Account.SalesAppointmentDate__c
	public Date selectSalesAppointmentDateAfter {get;set;}		//Account.SalesAppointmentDate__c
	public Boolean selectPhoneSale {get;set;}					//Account.ProcessingType__c == "NSC"
	public String[] selectDispositionCode {get;set;}			//Account.DispositionCode__c or Lead.DispositionCode__c
	public Boolean selectManagerAssigned {get;set;}
    
    public Boolean selectAllCheckbox {get; set;}
    
    // The ID and Name of the Street Sheet
    public String sStreetSheetID {get;set;}  
 	public String sStreetSheetName {get;set;}
 	
 	// Holds a message summarizing the search results 
 	public String searchSummaryMessage {get;set;}
 	
 	// Allows the button text to show one value for a create action and another for a update action
 	public string sbuttonText {get;set;} 
 	
 	// Indicates whether to display the page block for creating a new Street Sheet
 	public Boolean showCreateBlock {get; set;}
 	
 	// Indicates whether to display the page block to gather user input for searching
 	public Boolean bInputCriteria {get;set;}
 	
 	// Indicates whether to display the page block with existing Street Sheet Items
 	public Boolean showStreetSheetItemBlock {get; set;}
    
    // default query row limit
    private static final Integer DEFAULT_QUERY_ROW_LIMIT = 100;
	 	
	public ManageStreetSheetController(){
		init();
    }
        
	public void init(){
		
		sStreetSheetID = null;
		sStreetSheetName = null;
		
		ssiDetails = new List  <SearchItem>();
		itemsToAddList = new List <SearchItem>();	
		
		clear();
	    
	    // Obtain values for picklists
		resultSizeValues = PicklistHelper.getResultSizeValues();
		
		dataSourceValues = PicklistHelper.getDataSourceValues();
		for (SelectOption so : PicklistHelper.getLeadDataSourceValues() ) {
			if ( !Utilities.contains(dataSourceValues, so.getValue()) ) {
				dataSourceValues.add(so);
			}
		}
		
		leadTypeValues = PicklistHelper.getAccountTypeValues();
		for (SelectOption so : PicklistHelper.getLeadTypeValues() ) {
			if ( !Utilities.contains(leadTypeValues, so.getValue()) ) {
				leadTypeValues.add(so);
			}
		}
		
      	discoReasonValues = PicklistHelper.getDiscoReasonValues();
      	siteStateValues = PicklistHelper.getSiteStateValues(); 
      	
      	dispoStatusValues = PicklistHelper.getDispositionCodeValues();
		for (SelectOption so : PicklistHelper.getLeadDispositionCodeValues() ) {
			if ( !Utilities.contains(dispoStatusValues, so.getValue()) ) {
				dispoStatusValues.add(so);
			}
		}
      	
      	// assume that the page block for creating a Street Sheet will not be displayed
      	showCreateBlock = false;
      	// assume that the page block for entering search criteria will be displayed
      	bInputCriteria = true;
      	// assume that the page block for Street Sheet Items will not be displayed
      	showStreetSheetItemBlock = false;
      	
		//For Existing Street Sheets
		if (ApexPages.currentPage().getParameters().get('id')!= null) {
			sStreetSheetId=ApexPages.currentPage().getParameters().get('id');
			try {
				sStreetSheetName = [select name from StreetSheet__c where id=:sStreetSheetID].name;
			} catch (exception e){
				system.debug(e.getMessage());
			}	
			// the button should indicate that the action will be adding items
			sbuttonText='Add Items'; 
			// get the items for the Street Sheet
			getStreetSheetItems();
		}
		//For New StreetSheets
		if (sStreetSheetId==null){
			// will need to display the page block for entering the name of the Street Sheet
			showCreateBlock=true;
			// set the appropriate button text
			sbuttonText='Add Items + Create Prospect List';
		}
		
	}
  	
	public pageReference backToSearchCriteria (){
      	bInputCriteria=true; //Flag to control visibility on input Criteria
      	
    	itemsToAddList.clear();// Clear subset of account results based on select critiera
    	
    	//Get items for this Street Sheet
       	ssiDetails= SearchManager.findStreetSheetItems(sStreetSheetID); 
       	 
       	//If there are not any items, the Street Sheet is new so display the name field
      	if (ssiDetails.isEmpty()) {
      		showStreetSheetItemBlock = true; 
      	}	     
      	return null;
    }
    
	public PageReference cancelMSS(){
		
		// return to the Street Sheet Tab
		return new PageReference('/'+Schema.SObjectType.StreetSheet__c.getKeyPrefix());
	}
	
	public PageReference clear() {
		
		// initialize to no search criteria
		selectDataSource = new String[]{};
		selectLeadType = new String[]{};
		selectNewMover = false;
		selectNewLead = false;
		selectDateAssignedBefore = null;
		selectDateAssignedAfter = null;
		selectDiscoReason = new String[]{};
		selectSiteStreet = null;
    	selectSiteCity = null;
    	selectSiteState = new String[]{};
    	selectSitePostalCode = null;
    	selectDiscoDateBefore = null;
    	selectDiscoDateAfter = null;
    	selectPhoneAvailable = false;
    	selectActive = false;
		selectedQuerySize = String.valueOf(DEFAULT_QUERY_ROW_LIMIT);
		selectAllCheckbox = false;
		
		//phase 2
		selectNewMoverDateBefore = null;
		selectNewMoverDateAfter = null;
		selectSalesAppointmentDateBefore = null;
		selectSalesAppointmentDateAfter = null;
		selectPhoneSale = false;
		selectDispositionCode = null;
		selectManagerAssigned = false;
		
		return null;
	}
	
	public PageReference changeQueryLimit(){//Filter results based on select criteria
   				
   		return searchLeads();
   			
   	}
	
	
	// Retrieve Street Sheet Items
	public PageReference getStreetSheetItems(){

    		ssiDetails.clear();
    		acctIDs.clear();

	    	ssiDetails = SearchManager.findStreetSheetItems(sStreetSheetID);
	    	
	    	if (ssiDetails.isEmpty()) {
	    		showStreetSheetItemBlock = false;
	    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,  'There are no items for the selected Prospect List.'));
	    	}
	    	else {
	    		showStreetSheetItemBlock = true;
	    		// Add each Street Sheet Item ID to a set
	     		for (SearchItem ssi: ssiDetails){
	     			if ( ssi.ssItem.AccountID__c != null )
	    				acctIDs.add(ssi.ssItem.AccountID__c);
	    			else
	    				acctIDs.add(ssi.ssItem.LeadID__c);
	     		}	
	    	}
	    	return null;
    }
    
    // Search for Leads based on user entered search criteria
    public PageReference searchLeads(){
    	
    	if (!validateSelection()) return null;
    	    	
    	AccountSearchParameters parameters = buildParameters();
    	
    	List<SearchItem> acctsToAdd = SearchManager.findActiveAccounts(parameters);
    	List<SearchItem> leadsToAdd = new List<SearchItem>();
    	if (!parameters.hasAccountSpecificCriteria()) { 
	    	leadsToAdd = SearchManager.findLeads(parameters);
	    	SearchManager.redistributeResults(acctsToAdd, leadsToAdd, parameters.queryRowLimit);
    	}
    	
    	//itemsToAddList = SearchManager.findActiveAccounts(parameters);
    	itemsToAddList = new List<SearchItem>();
    	itemsToAddList.addAll(acctsToAdd);
    	itemsToAddList.addAll(leadsToAdd);
    	
        getMessage(); 
    	
    	return null;
    }
    
    //can't find anywhere where user selection is being validated so I'm going to create this method with assumption that no selection validation was required in phase 1
    private Boolean validateSelection() {
    	Boolean valid = true;
    	
    	Map<String, ErrorMessages__c> errormap = ErrorMessages__c.getAll();
    	
    	if (Utilities.contains(selectDataSource, 'BUDCO') && Utilities.contains(selectLeadType, 'Discontinuance')) {
    		valid = false;
    		ApexPages.Message m = new ApexPages.Message( ApexPages.Severity.ERROR,
    			errormap.get('MSS_budco_disc').Message__c );
    		ApexPages.addMessage(m);
    	}
    	
    	if (Utilities.contains(selectDataSource, 'User Entered') && 
    		( Utilities.contains(selectLeadType, 'Discontinuance') || Utilities.contains(selectLeadType, 'Pending Discontinuance') )
    	) {
    		valid = false;
    		ApexPages.Message m = new ApexPages.Message( ApexPages.Severity.ERROR,
    			errormap.get('MSS_userentered_disc').Message__c );
    		ApexPages.addMessage(m);
    	}
    	
    	if (selectPhoneSale != null && selectPhoneSale && 
    		(
    			Utilities.contains(selectDataSource, 'ADMIN') ||
    			Utilities.contains(selectDataSource, 'BUDCO') ||
    			Utilities.contains(selectDataSource, 'Broadview') ||
    			Utilities.contains(selectDataSource, 'Salesgenie.com')
    		)
    	)
    	{
    		valid = false;
    		ApexPages.Message m = new ApexPages.Message( ApexPages.Severity.ERROR,
    			errormap.get('MSS_phonesale_badsource').Message__c );
    		ApexPages.addMessage(m);
    	}
    	
    	/*//check string dates
    	if (
    		!Utilities.checkDateFormat(selectDateAssignedAfter) ||
    		!Utilities.checkDateFormat(selectDateAssignedBefore) ||
    		!Utilities.checkDateFormat(selectDiscoDateAfter) ||
    		!Utilities.checkDateFormat(selectDiscoDateBefore)
    	) {
    		valid = false;
    		ApexPages.Message m = new ApexPages.Message( ApexPages.Severity.ERROR,
    			'Invalid Input: Dates must be formatted as "MM/DD/YYYY".' );
    		ApexPages.addMessage(m);
    	}*/
    	
    	//check date ranges
    	if ( 
    		!Utilities.checkDateOrder( selectDateAssignedAfter , selectDateAssignedBefore) || 
    		!Utilities.checkDateOrder( selectDiscoDateAfter , selectDiscoDateBefore) || 
    		!Utilities.checkDateOrder( selectNewMoverDateAfter , selectNewMoverDateBefore) ||
    		!Utilities.checkDateOrder( selectSalesAppointmentDateAfter , selectSalesAppointmentDateBefore)
    	) {
    		valid = false;
    		ApexPages.Message m = new ApexPages.Message( ApexPages.Severity.ERROR,
    			errormap.get('MSS_daterange').Message__c );
    		ApexPages.addMessage(m);
    	}
    	
    	return valid;	
    }
    
    private AccountSearchParameters buildParameters() {
    	
    	AccountSearchParameters parameters = new AccountSearchParameters();
    	
    	parameters.dataSource = cleanseStringArray(selectDataSource);
    	parameters.leadType = cleanseStringArray(selectLeadType);
    	parameters.newMoverIndicator = selectNewMover;
    	parameters.nonWorkedLeadIndicator = selectNewLead;       
    	parameters.discoReason = cleanseStringArray(selectDiscoReason);
    	
    	/*parameters.dateAssignedAfter = null;
    	if (selectDateAssignedAfter != null && selectDateAssignedAfter.length() > 0) {
    		
    		List<String> parts = selectDateAssignedAfter.split('/', 3);
    		String year = parts[2];
    		if (parts[2].length() == 2) {
    			year = '20' + year;
    		}
    		parameters.dateAssignedAfter = Datetime.newInstance(Integer.valueOf(year),
    													Integer.valueOf(parts[0]),
    													Integer.valueOf(parts[1]));
    		
    		
    	}
    	parameters.dateAssignedBefore = null;
    	if (selectDateAssignedBefore != null && selectDateAssignedBefore.length() > 0) {
    		
    		List<String> parts = selectDateAssignedBefore.split('/', 3);
    		String year = parts[2];
    		if (parts[2].length() == 2) {
    			year = '20' + year;
    		}
    		parameters.dateAssignedBefore = Datetime.newInstance(Integer.valueOf(year),
    													Integer.valueOf(parts[0]),
    													Integer.valueOf(parts[1]));
    		
    	}*/
    	
    	parameters.dateAssignedAfter = Utilities.toDateTime(selectDateAssignedAfter);
    	parameters.dateAssignedBefore = Utilities.toDateTime(selectDateAssignedBefore);
    	 	
    	parameters.phoneAvailableIndicator = selectPhoneAvailable;
    	parameters.siteStreet = String.escapeSingleQuotes(selectSiteStreet);
    	parameters.siteCity = String.escapeSingleQuotes(selectSiteCity);
    	parameters.siteState = cleanseStringArray(selectSiteState);
    	
    	// check for Canadian postal code
    	if (selectSitePostalCode.contains(' ')) {
    		String[] pcodes = selectSitePostalCode.split(' ');
    		parameters.sitePostalCode = pcodes[0];
    		parameters.sitePostalCodeAddOn = pcodes[1];
    	}
    	else {
    		parameters.sitePostalCode = String.escapeSingleQuotes(selectSitePostalCode);
    	}
   
    	/*parameters.discoDateAfter = null;
    	if (selectDiscoDateAfter != null && selectDiscoDateAfter.length() > 0) {
    		List<String> parts = selectDiscoDateAfter.split('/', 3);
    		String year = parts[2];
    		if (parts[2].length() == 2) {
    			year = '20' + year;
    		}
    		parameters.discoDateAfter = Date.newInstance(Integer.valueOf(year),
    													Integer.valueOf(parts[0]),
    													Integer.valueOf(parts[1]));
    		
    	}
    	parameters.discoDateBefore = null;
    	if (selectDiscoDateBefore != null && selectDiscoDateBefore.length() > 0) {
    		
    		List<String> parts = selectDiscoDateBefore.split('/', 3);
    		String year = parts[2];
    		if (parts[2].length() == 2) {
    			year = '20' + year;
    		}
    		parameters.discoDateBefore = Date.newInstance(Integer.valueOf(year),
    													Integer.valueOf(parts[0]),
    													Integer.valueOf(parts[1]));
    		
    	}*/
    	
    	parameters.discoDateAfter = selectDiscoDateAfter;
    	parameters.discoDateBefore = selectDiscoDateBefore;
    	
    	parameters.activeSiteIndicator = selectActive;
    	parameters.queryRowLimit = Integer.valueOf(String.escapeSingleQuotes(selectedQuerySize));
    	parameters.accountIdSet = acctIDs;
    	
    	//phase 2 parameters
    	parameters.dispositionCode = cleanseStringArray(selectDispositionCode);
    	//going to assume the date code checking year is unnecessary since salesforce throws an error if date is formatted incorrectly
    	parameters.newMoverDateAfter = selectNewMoverDateAfter;
    	parameters.newMoverDateBefore = selectNewMoverDateBefore;
    	parameters.salesAppointmentDateAfter = Utilities.toDateTime(selectSalesAppointmentDateAfter);
    	parameters.salesAppointmentDateBefore = Utilities.toDateTime(selectSalesAppointmentDateBefore);
    	parameters.phoneSale = selectPhoneSale;
    	parameters.manuallyAssigned = selectManagerAssigned;
    	
    	return parameters;
    	
    }
    
  	
  	public PageReference getMessage(){
		ApexPages.getMessages().clear();
		
		if(itemsToAddList.size() != 0){
			
         	searchSummaryMessage = 'Displaying ' + itemsToAddList.size().format();
         	if (itemsToAddList.size() == 1) {
         		searchSummaryMessage += ' match.';
         	}
         	else {
         		searchSummaryMessage += ' matches.';
         	}	
       	    
           	// do not display the page block for user input search criteria
           	bInputCriteria=false;
          	// do not display the page block for Street Sheet Items
          	showStreetSheetItemBlock=false;
        }
        else {
         	ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Info, 'Your search returned 0 matches.'));
           	// display the page block for user input search criteria
           	bInputCriteria=true;
        }
		return null;
	} 
    
 
    // Creates a Street Sheet and Street Sheet Items
	public PageReference addStreetSheet() {
		
        boolean bNoErrors = true;
        boolean bAtLeastOneLeadSelected = false;        
        
        // When creating a street sheet or adding to an existing one, need to have selected at least one lead
        for(SearchItem a: itemsToAddList){
        	if(a.selected){
        		bAtLeastOneLeadSelected = true;
        		break;
        	}
        }
                
        // When creating a street sheet, there should not yet be a Street Sheet ID
        if(sStreetSheetID == null || sStreetSheetID.length()==0){
        		
        	// Need a name for the street sheet	
        	if(sStreetSheetName.length() > 0){
        		
        		// Insert a record only if a lead has been selected
        		if (bAtLeastOneLeadSelected) {	        	
	        		try {
	        			sStreetSheetID = StreetSheetManager.create(sStreetSheetName, buildItemSet());
	        		}
	        		catch (exception e){
	        			bNoErrors = false;
	        			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Unable to create Prospect List: ' + e.getMessage()));		
	    	    	}
        		}
        	}
        	// Return an error if the street sheet does not have a name
        	else {
        		bNoErrors = false;
        		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,  'You must enter a name when creating a new Prospect List.'));
        	}	
        }
        // Otherwise, just need to add items to the existing Street Sheet
        else {
        	// Insert a record only if a lead has been selected
        	if (bAtLeastOneLeadSelected) {
	        	try {
	        		StreetSheetManager.addItems(sStreetSheetID, buildItemSet());
	        	}
	        	catch (exception e){
	        		bNoErrors = false;
	        		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Unable to create Prospect List: ' + e.getMessage()));		
	    	    }
        	}
        	
        }
        // If appropriate, create error message about selecting at least one lead 
        if (!bAtLeastOneLeadSelected) {
        	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'You must select at least one item.'));
        }	
       
        // Determine where to navigate
    	if(!bAtLeastOneLeadSelected ||!bNoErrors){
    		return null;
    	}
    	else {
    		PageReference streetSheetDetailPage = new PageReference('/'+ sStreetSheetID);
	   		streetSheetDetailPage.setRedirect(true);
			return streetSheetDetailPage;
    	}
        
	}   
	
	private Set<String> buildItemSet() {
		
		Set<String> itemIdSet = new Set<String>();
		for(SearchItem cAcct : itemsToAddList){
	    	if(cAcct.selected){
	    		// contruct set of items being added
	    		if (cAcct.acct != null)
	    			itemIdSet.add(cAcct.acct.id);
	    		else
	    			itemIdSet.add(cAcct.lead.id);
	    		// then add to the set of existing items
	    		if (cAcct.acct != null)
	       			acctIds.add(cAcct.acct.id);
	       		else
	       			acctIds.add(cAcct.lead.id);
	        }
	    }
		return itemIdSet;
	}
	
	private String[] cleanseStringArray(String[] thisArray) {
		
		if (thisArray != null) {
			String[] cleanArray = new String[thisArray.size()];
			Integer counter = 0;
			for (String s : thisArray) {
				cleanArray[counter] = String.escapeSingleQuotes(s);
				counter++;
			}
		
			return cleanArray;
		} else {
			return thisArray;
		}
		
	}

}