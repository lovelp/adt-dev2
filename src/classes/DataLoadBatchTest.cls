@isTest
private class DataLoadBatchTest {
  
 static testmethod void testdataloadBatch() 
  {
    
    ResaleGlobalVariables__c rgvVar=new ResaleGlobalVariables__c();
        rgvVar.name='DataRecastDisableLeadTrigger';
        rgvVar.value__c='true';
        insert rgvVar; 
    ResaleGlobalVariables__c rgvVar1=new ResaleGlobalVariables__c();
        rgvVar1.name='DataRecastDisableAccountTrigger';
        rgvVar1.value__c='true';
        insert rgvVar1;

     ResaleGlobalVariables__c rgvVar2=new ResaleGlobalVariables__c(Name='IntegrationUser', value__c='Integration User');
     insert rgvVar2;
     
     ResaleGlobalVariables__c rgvVar3=new ResaleGlobalVariables__c(Name='NewRehashHOARecordOwner', value__c=Userinfo.getUserName());
     insert rgvVar3;

     Account acc=TestHelperClass.createAccountData();
     Profile p1 = [select id from profile where name='ADT NSC Sales Representative'];
     User u = new User(alias = 'uatsr1', email='unitTestSalesAgent1@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p1.Id, EmployeeNumber__c = TestHelperClass.generateEmployeeNumber(),
                timezonesidkey='America/Los_Angeles', username='unitTestSalesAgent1@testorg.com',
                StartDateInCurrentJob__c = Date.today());
      insert u;  
    
    Lead led = TestHelperClass.createLead(u);
    
    List<Lead> leadlist = new List<Lead>();
    leadlist.add(led);
    List<Account> acclist = new List<Account>();
    acclist.add(acc);
    
    Affiliate__c aff= new Affiliate__c();
    aff.name = 'TESTAFF';
    aff.Active__c = true; 
    insert aff;
    
    DataLoadMapping__mdt dlmf = [SELECT AccountMapField__c, Active__c, DataLoadMapField__c, LeadMapField__c FROM DataLoadMapping__mdt WHERE Active__c = true LIMIT 1];
        
    DataloadTemp__c dlt = new DataloadTemp__c();
    dlt.UniqueId__c = 'test';
    dlt.Type__c = 'Account'; 
    dlt.GroupId__c = 'GroupId__c';
    dlt.Processed__c = false;
    dlt.AddressLine1__c = 'testadd';
    dlt.SiteLastName__c = 'test';
    dlt.SitePostalCode__c = '798080';
    insert dlt; 
    
    List<DataloadTemp__c> dltlist = new List<DataloadTemp__c>();
    dltlist.add(dlt);
    
    Test.startTest();
    String queryString = 'SELECT Id,DataLoadMapField__c,UniqueId__c,Type__c,GroupId__c from DataloadTemp__c WHERE GroupId__c IN :selectedGroupIds AND Processed__c = false';
    DataLoadBatch dlb = new DataLoadBatch(queryString);
    Database.executeBatch(dlb);
    //dlb.accStandRecordType();
    //dlb.setupAccntAff(acclist);
    Test.stopTest();
  }
  
  }