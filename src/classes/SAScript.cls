public with sharing class SAScript {
    /*public static void scriptExecute(){
        list<SystemDesign__c> existingDesignList = [SELECT Id, Opportunity__r.Id, Opportunity__r.SalesAgreementId__c, Opportunity__r.AccountId 
                                                FROM SystemDesign__c WHERE Opportunity__c != null limit 70];
        if(existingDesignList.isEmpty()) return;
        map<String, Opportunity> saToOpp = new map<String, Opportunity>();
        list<SalesAgreementSchema.SalesAgreementLookupResponse> resps = new list<SalesAgreementSchema.SalesAgreementLookupResponse>();
        for(SystemDesign__c sys : existingDesignList){
            Opportunity opp = sys.Opportunity__r;
            if(String.isNotBlank(opp.SalesAgreementId__c)){
                saToOpp.put(opp.SalesAgreementId__c, opp);
            }
        }
        
        for(Opportunity opp : saToOpp.values()){
            SalesAgreementSchema.SalesAgreementLookupRequest saReq = SalesAgreementAPI.getLookupRequest(opp);
            SalesAgreementSchema.SalesAgreementLookupResp resp = SalesAgreementAPI.lookupSalesAgreement(null, saReq);
            if(resp.status == true){
                resps.add(resp.result);
            }
        }
        List<Opportunity> oppUpdateList = new List<Opportunity>();
        list<SystemDesign__c> designList = new list<SystemDesign__c>();
        list<Account> accList = new list<Account>();
        list<Site__c> siteList = new list<Site__c>();
        set<String> siteIds = new set<String>();
        set<String> designIds = new set<String>();
        for(SalesAgreementSchema.SalesAgreementLookupResponse response : resps){
            Opportunity opp = saToOpp.get(response.salesAgreementID);
            
            SalesAgreementAPI.populateOpp(response, opp);
            oppUpdateList.add(opp);
            Account acc = SalesAgreementAPI.updateAccountFields(response, opp, 'SCRIPT');
            accList.add(acc);
            if(response.sites != null && !response.sites.isEmpty()){
                for(SalesAgreementSchema.Sites sys : response.sites){
                    Site__c site = SalesAgreementAPI.populateSite(sys, opp.Id);
                    siteList.add(site);
                    siteIds.add(sys.siteID);
                }
                //list<SystemDesign__c> designList = new list<SystemDesign__c>();
                for(SalesAgreementSchema.Sites sys : response.sites){
                    if(sys.systemDesigns != null && !sys.systemDesigns.isEmpty()){
                        for(SalesAgreementSchema.SystemDesigns sysDes : sys.systemDesigns){
                            SystemDesign__c design = SalesAgreementAPI.populateSystemDesign(sysDes);
                            design.SiteId__c = sys.siteID;
                            designList.add(design);
                            designIds.add(sysDes.systemDesignId);
                        }
                    }
                }
            }
        }
        if(!siteList.isEmpty()){
            upsert siteList SiteID__c;
        }
        map<String, Id> SiteIdToSite = new map<String, Id>();
        for(Site__c site : siteList){
            SiteIdToSite.put(site.SiteID__c, site.Id);
        }
        for(SystemDesign__c design : designList){
            design.Site__c = SiteIdToSite.get(design.SiteID__c);
        }
        if(!designList.isEmpty()){
            upsert designList SystemDesignId__c;
        }
        if(!accList.isEmpty()){
            update accList;
        }

        if(!oppUpdateList.isEmpty()){
            update oppUpdateList;
        }
        //delete existingDesignList;
        delete [SELECT Id, Opportunity__r.Id, Opportunity__r.SalesAgreementId__c, Opportunity__r.AccountId 
                                                FROM SystemDesign__c WHERE Opportunity__c != null AND Id IN :existingDesignList limit 70];
    }*/
}