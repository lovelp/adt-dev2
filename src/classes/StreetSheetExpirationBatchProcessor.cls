/************************************* MODIFICATION LOG ********************************************************************************************
* StreetSheetExpirationBatchProcessor
*
* DESCRIPTION : Delete expired street sheets
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* SahilGrover				  4/19/2012				- Original Version
*
*													
*/

global class StreetSheetExpirationBatchProcessor implements Database.Batchable<sObject>, Database.Stateful {
	
	public String query = 'Select ID from StreetSheet__c where LastModifiedDate < LAST_N_DAYS:' +
						StreetSheetRetentionLimit__c.getAll().values()[0].Name +
						'';
	
	global Database.QueryLocator start( Database.Batchablecontext bc ) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext bc, List<sObject> scope) {
		delete scope;
	}
	
	global void finish(Database.BatchableContext bc) {
		
	}
}