/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : LeadTriggerHelperTest is a test class for LeadTriggerHelper.cls
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            02/28/2012          - Origininal Version
* Jaydip Bhattacharya           06/24/2013          - Modified for Sprint#12, to populate Lead Type as Pre Mover for BUDCO leads
* Jaydip Bhattacharya           08/05/2013          - Modified to populate new bulder type PN instead of PM
* Leonard Alarcon               01/09/2014          - Modified testProcessLeadsBeforeInsert(...) added NewMoverType = "AP"
*/
@isTest
private class LeadTriggerHelperTest 
{


    static testMethod void testProcessLeadsBeforeInsert()
    {
        
        TestHelperClass.createrehashOwnerfortestclasses();
        user adminuser = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        Test.startTest();   
        System.runAs(adminuser){
            TestHelperClass.createReferenceDataForTestClasses();
            TestHelperClass.createAlphaconversionsettingvalues();
            LIST<Lead> lstLds=new LIST <LEAD>();            
            
            ResaleGlobalVariables__c rgv = [Select id,Name,Value__c from ResaleGlobalVariables__c where name='NewRehashHOARecordOwner'];
            rgv.Value__c ='unitTestrehashRep1@testorg.com';
            update rgv;
            
            Lead L1 = new Lead();
            String Id = TestHelperClass.inferPostalCodeID('22102', '1200');
            L1 =TestHelperClass.createLead(adminuser,false,'BUDCO');            
            L1.NewMoverType__c ='SN';  
            L1.Business_Id__c = '1100 - Residential';    
            lstLds.add(L1);
            
            
            
            Lead L2 = new Lead();
            String Id1 = TestHelperClass.inferPostalCodeID('28205', '1100');
            L2 =TestHelperClass.createLead(adminuser,false,'BUDCO');                
            L2.DispositionCode__c = 'Phone - No Answer';
            //L1.Channel__c = 'Resi Direct Sales';
            L2.SiteStreet__c = '1320 Brook Rd';
            L2.SiteCity__c = 'CHARLOTTE';      
            L2.SiteStateProvince__c = 'NC';
            L2.SiteCountryCode__c = 'US';
            L2.SitePostalCode__c = '28205';
            L2.PostalCodeID__c = Id1;
            L2.NewMoverType__c='PN';
            lstLds.add(L2);
            
            Lead L3 = new Lead();
            String Id3 = TestHelperClass.inferPostalCodeID('28205', '1100');
            L3 =TestHelperClass.createLead(adminuser,false,'BUDCO');
            L3.SiteStreet__c = '1320 Brook Rd';
            L3.SiteCity__c = 'CHARLOTTE';      
            L3.SiteStateProvince__c = 'NC';
            L3.SiteCountryCode__c = 'US';
            L3.SitePostalCode__c = '28205';
            L3.PostalCodeID__c = Id;
            L3.NewMoverType__c='AP';
            lstLds.add(L3);
            
            insert lstLds;
            L1 = [SELECT Id,Name, AddressID__c, PostalCodeID__c, Channel__c, Business_Id__c, NewMoverType__c FROM Lead WHERE Id = :lstLds[0].Id];
            
            System.debug('L1.....:'+L1);
            System.assert(L1.AddressID__c != null, 'The Address Lookup on Lead should not be Null');
            System.assert(L1.PostalCodeID__c != null, 'The Postal Code Lookup on Lead should not be Null');
            System.assert(L1.Channel__c != null, 'The Channel on Lead should not be Null');
            System.assert(L1.Business_Id__c != null, 'The Business Id on Lead should not be Null');
            L2 = [SELECT Id,Name,Type__c  FROM Lead WHERE Id = :lstLds[1].Id];
            System.assertEquals('Pre Mover', L2.Type__c);
            
            system.debug('*** TEST 1 ==> '+L3);
            L3 = [SELECT Id,Name,Type__c  FROM Lead WHERE Id = :L3.Id];
            system.debug('*** TEST 2 ==> '+L3);
            System.assertEquals('ANI',L3.Type__c);
        }
        Test.stopTest();
    }

    static testMethod void testProcessLeadsBeforeInsertWithCINotes()
    {
        TestHelperClass.createrehashOwnerfortestclasses();
        user adminuser = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        Test.startTest();   
        System.runAs(adminuser){
            TestHelperClass.createReferenceDataForTestClasses();
            LIST<Lead> lstLds=new LIST <LEAD>();            
            
            ResaleGlobalVariables__c rgv = [Select id,Name,Value__c from ResaleGlobalVariables__c where name='NewRehashHOARecordOwner'];
            rgv.Value__c ='unitTestrehashRep1@testorg.com';
            update rgv;
            
            Lead L1 = new Lead();
            String Id = TestHelperClass.inferPostalCodeID('22102', '1100');
            L1 =TestHelperClass.createLead(adminuser,false,'TelemarRehash');
            L1.LastName = 'TestLastName';
            L1.Company = 'TestCompany';
            L1.LeadSource = 'TelemarRehash';
            L1.SGCOM3__SG_infoGroupId__c = L1.LeadSource == 'Salesgenie.com' ? '12345' : null;
            L1.Channel__c = 'Resi Direct Sales';
            L1.SiteStreet__c = '8952 Brook Rd';
            L1.SiteCity__c = 'McLean';      
            L1.SiteStateProvince__c = 'VA';
            L1.SiteCountryCode__c = 'US';
            L1.SitePostalCode__c = '22102';
            
            String dispDelimiter = '~';
            String dataDelimiter = '|';
            String note1 = 'Customer prefers morning appointments';
            String date1 = '08/28/2012';
            L1.IncomingCINotes__c = dispDelimiter + note1 + dataDelimiter + date1;
            
            insert L1;
            
            L1 = [SELECT Id,Name, DispositionCode__c, DispositionDetail__c, DispositionComments__c, DispositionDate__c FROM Lead WHERE Id = :L1.Id];
         
            System.assertEquals(IntegrationConstants.DEFAULT_NSC_DISPOSITION, L1.DispositionCode__c);
            System.assertEquals(IntegrationConstants.DISPOSITION_DETAIL_CI_NOTE, L1.DispositionDetail__c);
            System.assertEquals(note1, L1.DispositionComments__c);
            System.assertEquals(Date.parse(date1), L1.DispositionDate__c.date());
        }
        
        Test.stopTest();
    }
    
    static testMethod void testProcessLeadsAfterInsert()
    {
        TestHelperClass.createrehashOwnerfortestclasses();
        user adminuser = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        Test.startTest();
        System.runAs(adminuser) { 
            TestHelperClass.createReferenceDataForTestClasses();
            
            ResaleGlobalVariables__c rgv = [Select id,Name,Value__c from ResaleGlobalVariables__c where name='NewRehashHOARecordOwner'];
            rgv.Value__c ='unitTestrehashRep1@testorg.com';
            update rgv;
            
            //create address for creating accounts
            Address__c addr = new Address__c();
            addr.Latitude__c = 29.91760000000000;
            addr.Longitude__c = -90.17870000000000;
            addr.Street__c = '56528 E Crystal Ct';
            addr.City__c = 'Westwego';
            addr.State__c = 'LA';
            addr.PostalCode__c = '70094';
            addr.OriginalAddress__c = '56528 E CRYSTAL CT+WESTWEGO+LA+70094';
             
            insert addr;
       
            Lead L1 = new Lead();
            String Id = TestHelperClass.inferPostalCodeID('70094', '1100');
            L1.LastName = 'TestLastName';
            L1.Company = 'TestCompany';
            L1.OwnerId = adminuser.Id;
            L1.LeadSource = 'TelemarRehash';
            L1.DispositionCode__c = 'Phone - No Answer';
            L1.Channel__c = 'Resi Direct Sales';
            L1.SiteStreet__c = '56528 E Crystal Ct';
            L1.SiteCity__c = 'Westwego';
            L1.SiteStateProvince__c = 'LA';
            L1.SiteCountryCode__c = 'US';
            L1.SitePostalCode__c = '70094';
            L1.AddressID__c = addr.Id;
            String dispDelimiter = '~';
            String dataDelimiter = '|';
            String note1 = 'Customer prefers morning appointments';
            String date1 = '08/28/2012';
            L1.IncomingCINotes__c = dispDelimiter + note1 + dataDelimiter + date1;
            
            insert L1;
            
            Disposition__c d = [SELECT DispositionType__c, DispositionDetail__c, Comments__c, DispositionDate__c FROM Disposition__c WHERE LeadID__c = :L1.Id];
            
            System.assert(d != null, 'Should be a Disposition record');
            System.assertEquals(IntegrationConstants.DEFAULT_NSC_DISPOSITION, d.DispositionType__c);
            System.assertEquals(IntegrationConstants.DISPOSITION_DETAIL_CI_NOTE, d.DispositionDetail__c);
            System.assertEquals(note1, d.Comments__c);
            System.assertEquals(Date.parse(date1), d.DispositionDate__c.date());
        }
        Test.stopTest();
    }
    
    static testMethod void testProcessLeadsBeforeInsert_SalesGHomeHealth()
    {
        TestHelperClass.createrehashOwnerfortestclasses();
        User admin = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        User u;
        System.runAs(admin) {
            u = TestHelperClass.createSalesRepUser();
            u.Business_Unit__c = Channels.TYPE_HOMEHEALTH;
            update u; 
            TestHelperClass.createReferenceDataForTestClasses();
            String Id = TestHelperClass.inferPostalCodeID('22102', '1100');
            Id = TestHelperClass.inferPostalCodeID('22102', '1200');
            
            ResaleGlobalVariables__c rgv = [Select id,Name,Value__c from ResaleGlobalVariables__c where name='NewRehashHOARecordOwner'];
            rgv.Value__c ='unitTestrehashRep1@testorg.com';
            update rgv;
            
        }
        
        Lead L1 = new Lead();
        
        Test.startTest();
            System.runAs(u) {   
            
            String Id = TestHelperClass.inferPostalCodeID('22102', '1100');
            Id = TestHelperClass.inferPostalCodeID('22102', '1200'); 
            L1.LastName = 'TestLastName';
            L1.Company = 'TestCompany';
            L1.OwnerId = UserInfo.getUserId();
            L1.LeadSource = 'Salesgenie.com';
            L1.SGCOM3__SG_infoGroupId__c = '12345';
            L1.SGCOM3__SG_InfoGroupDatabase__c = 'NewBusiness';
            L1.DispositionCode__c = 'Phone - No Answer';
            L1.SiteStreet__c = '8952 Brook Rd';
            L1.SiteCity__c =  'McLean';      
            L1.SiteStateProvince__c = 'VA';
            L1.SiteCountryCode__c = 'US';
            L1.SitePostalCode__c = '22102';
            L1.Business_Id__c = '1100 - Residential'; 
            L1.PostalCodeID__c = Id;
            insert L1;
        }    
        L1 = [SELECT Id,Name, AddressID__c, PostalCodeID__c, Channel__c, Business_Id__c FROM Lead WHERE Id = :L1.Id];
        
        System.debug('L1.....:'+L1);
        System.assert(L1.AddressID__c != null, 'The Address Lookup on Lead should not be Null');
        System.assert(L1.PostalCodeID__c != null, 'The Postal Code Lookup on Lead should not be Null');
        System.assertEquals(Channels.TYPE_HOMEHEALTH, L1.Channel__c,'The Channel on Lead should not be HomeHealth');
        System.assert(L1.Business_Id__c != null, 'The Business Id on Lead should not be Null');

        Test.stopTest();
    }
    
 static testMethod void testProcessLeadsBeforeInsert_SalesCanadaBusiness()
    {
        TestHelperClass.createrehashOwnerfortestclasses();
        User admin = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        Test.startTest();
            
        System.runAs(admin) {
            TestHelperClass.createReferenceDataForTestClasses();
            String Id = TestHelperClass.inferPostalCodeID('22102', '1100');
            Id = TestHelperClass.inferPostalCodeID('22102', '1200');
            
            ResaleGlobalVariables__c rgv = [Select id,Name,Value__c from ResaleGlobalVariables__c where name='NewRehashHOARecordOwner'];
            rgv.Value__c ='unitTestrehashRep1@testorg.com';
            update rgv;
            
            Lead L1 = new Lead();
            
            L1.LastName = 'TestLastName';
            L1.Company = 'TestCompany';
            L1.OwnerId = UserInfo.getUserId();
            L1.LeadSource = 'Salesgenie.com';
            L1.SGCOM3__SG_infoGroupId__c = '12345';
            L1.SGCOM3__SG_InfoGroupDatabase__c = 'CanadaBusiness';
            L1.DispositionCode__c = 'Phone - No Answer';
            L1.SiteStreet__c = '8952 Brook Rd';
            L1.SiteCity__c =  'McLean';      
            L1.SiteStateProvince__c = 'VA';
            L1.SiteCountryCode__c = 'US';
            L1.SitePostalCode__c = '22102';
            L1.Business_Id__c = '1100 - Residential'; 
            L1.PostalCodeID__c = Id;
            insert L1;
           
            L1 = [SELECT Id,Name, AddressID__c, PostalCodeID__c, Channel__c, Business_Id__c FROM Lead WHERE Id = :L1.Id];
            
            System.debug('L1.....:'+L1);
            System.assert(L1.AddressID__c != null, 'The Address Lookup on Lead should not be Null');
            System.assert(L1.PostalCodeID__c != null, 'The Postal Code Lookup on Lead should not be Null');
            System.assertEquals(Channels.TYPE_SBDIRECT, L1.Channel__c,'The Channel on Lead should not be SB_DIRECT');
            System.assert(L1.Business_Id__c != null, 'The Business Id on Lead should not be Null');
        }
        Test.stopTest();
    }
 
 static testMethod void testProcessLeadsBeforeInsert_Consumer()
    {
        TestHelperClass.createrehashOwnerfortestclasses();
        TestHelperClass.createReferenceUserDataForTestClasses();        
        User admin = TestHelperClass.createAdminUser();
        Test.startTest();
            System.runAs(admin) {
                TestHelperClass.createReferenceDataForTestClasses();
                
                ResaleGlobalVariables__c rgv = [Select id,Name,Value__c from ResaleGlobalVariables__c where name='NewRehashHOARecordOwner'];
                rgv.Value__c ='unitTestrehashRep1@testorg.com';
                update rgv;
            
                Lead L1 = new Lead();
                
                //u = TestHelperClass.createSalesRepUser();
                String Id = TestHelperClass.inferPostalCodeID('22102', '1100');
                L1 =TestHelperClass.createLead(admin,false,null);
                
                L1.LastName = 'TestLastName';
                L1.Company = 'TestCompany';
                L1.LeadSource = 'Salesgenie.com';
                L1.SGCOM3__SG_infoGroupId__c = '12345';
                L1.SGCOM3__SG_InfoGroupDatabase__c = 'Consumer';
                L1.DispositionCode__c = 'Phone - No Answer';
                L1.SiteStreet__c = '8952 Brook Rd';
                L1.SiteCity__c =  'McLean';      
                L1.SiteStateProvince__c = 'VA';
                L1.SiteCountryCode__c = 'US';
                L1.SitePostalCode__c = '22102';
                L1.Business_Id__c = '1100 - Residential';  
                L1.PostalCodeID__c = Id;
                insert L1;
              
                L1 = [SELECT Id,Name, AddressID__c, PostalCodeID__c, Channel__c, Business_Id__c FROM Lead WHERE Id = :L1.Id];
                
                System.debug('L1.....:'+L1);
                System.assert(L1.AddressID__c != null, 'The Address Lookup on Lead should not be Null');
                System.assert(L1.PostalCodeID__c != null, 'The Postal Code Lookup on Lead should not be Null');
                System.assertEquals(Channels.TYPE_RESIDIRECT, L1.Channel__c,'The Channel on Lead should not be TYPE_RESIDIRECT');
                System.assert(L1.Business_Id__c != null, 'The Business Id on Lead should not be Null');
            } 
        Test.stopTest();
    }  
    static testMethod void testProcessLeadsBeforeInsert_NewMover()
    {
        TestHelperClass.createrehashOwnerfortestclasses();
        User admin = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        Test.startTest();
        System.runAs(admin) {
            String Id = TestHelperClass.inferPostalCodeID('22102', '1100');
            TestHelperClass.createReferenceDataForTestClasses();
            
            ResaleGlobalVariables__c rgv = [Select id,Name,Value__c from ResaleGlobalVariables__c where name='NewRehashHOARecordOwner'];
            rgv.Value__c ='unitTestrehashRep1@testorg.com';
            update rgv;
            
            Lead L1 = new Lead();
            
            
            L1.LastName = 'TestLastName';
            L1.Company = 'TestCompany';
            L1.OwnerId = admin.Id;
            L1.LeadSource = 'Salesgenie.com';
            L1.SGCOM3__SG_infoGroupId__c = '12345';
            L1.SGCOM3__SG_InfoGroupDatabase__c = 'NewMover';
            L1.DispositionCode__c = 'Phone - No Answer';
            L1.SiteStreet__c = '8952 Brook Rd';
            L1.SiteCity__c =  'McLean';      
            L1.SiteStateProvince__c = 'VA';
            L1.SiteCountryCode__c = 'US';
            L1.SitePostalCode__c = '22102';
            L1.Business_Id__c = '1100 - Residential'; 
            L1.PostalCodeID__c = Id;
            insert L1;
            L1 = [SELECT Id,Name, AddressID__c, PostalCodeID__c, Channel__c, Business_Id__c FROM Lead WHERE Id = :L1.Id];
            
            System.debug('L1.....:'+L1);
            System.assert(L1.AddressID__c != null, 'The Address Lookup on Lead should not be Null');
            System.assert(L1.PostalCodeID__c != null, 'The Postal Code Lookup on Lead should not be Null');
            System.assertEquals(Channels.TYPE_RESIDIRECT, L1.Channel__c,'The Channel on Lead should not be TYPE_RESIDIRECT');
            System.assert(L1.Business_Id__c != null, 'The Business Id on Lead should not be Null');
        }
        Test.stopTest();
    }    
    
    static testMethod void testProcessLeadsBeforeInsertWithDKL()
    {
        TestHelperClass.createrehashOwnerfortestclasses();
        user adminuser = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        Test.startTest();   
        System.runAs(adminuser){
            TestHelperClass.createReferenceDataForTestClasses();
            LIST<Lead> lstLds=new LIST <LEAD>();            
            
            ResaleGlobalVariables__c rgv = [Select id,Name,Value__c from ResaleGlobalVariables__c where name='NewRehashHOARecordOwner'];
            rgv.Value__c ='unitTestrehashRep1@testorg.com';
            update rgv;
            
            Lead L1 = new Lead();
            String Id = TestHelperClass.inferPostalCodeID('22102', '1100');
            L1.LastName = 'TestLastName';
            L1.Company = 'TestCompany';
            L1.OwnerId = UserInfo.getUserId();
            L1.LeadSource = 'TelemarRehash';
            L1.SGCOM3__SG_infoGroupId__c = L1.LeadSource == 'Salesgenie.com' ? '12345' : null;
            L1.Channel__c = 'Resi Direct Sales';
            L1.SiteStreet__c = '8952 Brook Rd';
            L1.SiteCity__c = 'McLean';      
            L1.SiteStateProvince__c = 'VA';
            L1.SiteCountryCode__c = 'US';
            L1.SitePostalCode__c = '22102';
            L1.SourceCriteriaDescription__c = 'DKL - Abc123';
            L1.Phone = '999-123-1234';
            L1.PhoneNumber2__c = '999-234-2345';
            L1.DoNotCall = false;
            
            insert L1;
            
            L1 = [SELECT Id,Name, DoNotCall, Phone, PhoneNumber2__c FROM Lead WHERE Id = :L1.Id];
         
            System.assertEquals(IntegrationConstants.DNC, L1.Phone);
            System.assertEquals(IntegrationConstants.DNC, L1.PhoneNumber2__c);
            System.assert(L1.DoNotCall, 'Do Not Call should be True');
        }
        Test.stopTest();
    }

    static testMethod void testProcessLeadsAfterUpdateWithDKL()
    {
        TestHelperClass.createrehashOwnerfortestclasses();
        user adminuser = TestHelperClass.createAdminUser();
        List <user> u2 = [select id from user where Profile.Name ='ADT Commercial Representative' limit 2];
        TestHelperClass.createReferenceUserDataForTestClasses();
        Test.startTest();   
        System.runAs(adminuser){
            TestHelperClass.createReferenceDataForTestClasses();
            
            ResaleGlobalVariables__c rgv = [Select id,Name,Value__c from ResaleGlobalVariables__c where name='NewRehashHOARecordOwner'];
            rgv.Value__c ='unitTestrehashRep1@testorg.com';
            update rgv;
            
            LIST<Lead> lstLds=new LIST <LEAD>();            
            //create address for creating accounts
            Address__c addr = new Address__c();
            addr.Latitude__c = 29.91760000000000;
            addr.Longitude__c = -90.17870000000000;
            addr.Street__c = '56528 E Crystal Ct';
            addr.City__c = 'Westwego';
            addr.State__c = 'LA';
            addr.PostalCode__c = '70094';
            addr.OriginalAddress__c = '56528 E CRYSTAL CT+WESTWEGO+LA+70094';
           
            Lead L1 = new Lead();
            String Id = TestHelperClass.inferPostalCodeID('70094', '1100');
            L1.LastName = 'TestLastName';
            L1.Company = 'TestCompany';
            L1.OwnerId = UserInfo.getUserId();
            L1.LeadSource = 'TelemarRehash';
            L1.SGCOM3__SG_infoGroupId__c = L1.LeadSource == 'Salesgenie.com' ? '12345' : null;
            L1.Channel__c = 'Resi Direct Sales';
            L1.SiteStreet__c = '56528 E Crystal Ct';
            L1.SiteCity__c = 'Westwego';
            L1.SiteStateProvince__c = 'LA';
            L1.SiteCountryCode__c = 'US';
            L1.SitePostalCode__c = '70094';
            L1.AddressID__c = addr.Id;
            L1.Phone = '999-123-1234';
            L1.PhoneNumber2__c = '999-234-2345';
            L1.DoNotCall = false;
            
            insert L1;
            
            L1 = [SELECT Id,Name, DoNotCall, Phone, PhoneNumber2__c FROM Lead WHERE Id = :L1.Id];
            
            L1.Phone = '904-123-1234';
            update L1;
         
            L1 = [SELECT Id,Name, DoNotCall, Phone, PhoneNumber2__c FROM Lead WHERE Id = :L1.Id];
            L1.OwnerId = u2[0].id;
            update L1;
            
            System.assertEquals('904-123-1234', L1.Phone);
            System.assert(!L1.DoNotCall, 'Do Not Call should be False');
        }
        Test.stopTest();
    }
        
      
}