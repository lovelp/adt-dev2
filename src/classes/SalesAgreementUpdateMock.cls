/************************************* MODIFICATION LOG ********************************************************************************************
 * SalesAgreementUpdateMock
 *
 * DESCRIPTION : Mock response for Sales Agreement Update Request API
 *  1. Create
 *               
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                        DATE              TICKET            REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Jitendra Kothari                 05/25/2019        HRM-9710          Original
 */
@isTest
global class SalesAgreementUpdateMock implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://example.com/example/test', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status" : true, "result" : "", "message":"", "statusCode": "0"}');
        res.setStatusCode(200);
        return res;
    }
}