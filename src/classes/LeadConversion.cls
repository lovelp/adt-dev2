/************************************* MODIFICATION LOG ********************************************************************************************
* LeadConversion
*
* DESCRIPTION : Class to perform Lead Conversions
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  2/8/2012                - Original Version
* Magdiel Herrera               10/3/2014               - Refactor class to support bulk processing of lead conversion
* Mounika Anna      			06/19/2018    			- HRM-7272 DNIS Changes for Field Flow
* Giribabu Geda					06/27/2018				- HRM-7313 Amplifinity feeding wrong leadsource to MMB                                                 
*/

public without sharing class LeadConversion {
    
    /***********************************************************************************************
     *
     *      PUBLIC CLASS DEFINITIONS
     *      ========================
     *
     ***********************************************************************************************/
    
    public class LeadConversionException extends exception {}
    
    //---------------------------------------------------------------------------------------------------
    //  Id of account that was created/updated
    //---------------------------------------------------------------------------------------------------
    public Id AccountId {
        get {
            return aid;
        }
    }
                        
    //---------------------------------------------------------------------------------------------------
    //  This will be created only if the custom setting in ResaleGlobalVariables is set to true
    //---------------------------------------------------------------------------------------------------
    public Id ContactId {
        get {
            return cid;
        }
    }
                        
    //---------------------------------------------------------------------------------------------------
    //  List of errors on failure
    //---------------------------------------------------------------------------------------------------
    public list<Database.Error> errors {
        get{
            return err;
        }
    }   
    
    //---------------------------------------------------------------------------------------------------
    //  Should convert lead suppress opportunity creation? default = true
    //---------------------------------------------------------------------------------------------------
    public Boolean doNotCreateOpportunity { get; set; }     
    
    
    /***********************************************************************************************
     *
     *      PRIVATE CLASS DEFINITIONS
     *      =========================
     *
     ***********************************************************************************************/
     
    //---------------------------------------------------------------------------------------------------
    //  Inner class used for encapsulating information persisted from Lead to Accounts created as 
    //  as part of the conversion process
    //---------------------------------------------------------------------------------------------------
    private class LeadConvertFieldMap {
        public String lEmail;
        public String lCompetitor;
        public Boolean lDoNotCall;
        public String lType;
        public String lLeadSource;
        public Boolean lNewMover;
        public String lSICCode;
        public String lFirstName;
        public String lLastName;    
        public String lNMStreet;
        public String lNMCity;
        public String lNMState;
        public String lNMPostalCode;
        public Date lCreateDate;
        public Date sourceSystemCreateDate;     
        public String MMBCustomerNumber; 
        public String MMBSiteNumber; 
        public String MMBMasterCustomerNumber;
        public String PropertyManagementCompany;
    }
    
    //---------------------------------------------------------------------------------------------------
    //  Id of lead to be converted
    //---------------------------------------------------------------------------------------------------
    private Id lid;
                            
    //---------------------------------------------------------------------------------------------------
    //  Id of account that was created/updated
    //---------------------------------------------------------------------------------------------------
    private Id aid;
                            
    //---------------------------------------------------------------------------------------------------
    //  Id of contact that was created/updated
    //---------------------------------------------------------------------------------------------------
    private Id cid;                     
    
    //---------------------------------------------------------------------------------------------------
    //  List of errors on failure
    //---------------------------------------------------------------------------------------------------
    private list<Database.Error> err;   

    //---------------------------------------------------------------------------------------------------
    //  Telemar account number
    //---------------------------------------------------------------------------------------------------
    private String tan;
                        
    //---------------------------------------------------------------------------------------------------
    //  List of Leads to convert using bulk
    //---------------------------------------------------------------------------------------------------
    private List<Lead> lBulkConvert;
         
    //---------------------------------------------------------------------------------------------------
    //  Map containing existing account Ids by Telemar Number converted
    //---------------------------------------------------------------------------------------------------
    private Map<String, String> telemarAccountNumMap;   
    
    
    /***********************************************************************************************
     *
     *      CONSTRUCTORS
     *      ============
     *
     ***********************************************************************************************/
     
    /**
     *  @Constructor
     *
     *  @param Id       leadId              Lead Id to be converted
     *
     */
    public LeadConversion(Id leadId)
    {
        this(leadId, true);
    }
    
    /**
     *  @Constructor
     *
     *  @param Id       leadId              Lead Id to be converted
     *
     *  @param Boolean  doNotCreateOpp      Overwrite instance default with the option to create or not an opportunity during conversion
     *
     */
    public LeadConversion(Id leadId, Boolean doNotCreateOpp)
    {
        this(leadId, doNotCreateOpp, null);
    }
    
    /**
     *  @Constructor
     *
     *  @param Id       leadId              Lead Id to be converted
     *
     *  @param String   telemarAccountNum   Telemar Number for Lead to convert
     *
     */
    public LeadConversion(Id leadId, String telemarAccountNum)
    {
        this(leadId, true, telemarAccountNum);
    }
    
    /**
     *  @Constructor
     *
     *  @param List<Lead>           leadToConvertList   List of leads to convert
     *
     *  @param Boolean              doNotCreateOpp      Flag indicating if opportunities need to be added as part of the conversion process
     *
     *  @param Map<String, String>  telemarAccountNum   Map containing Lead Ids to convert by Telemar Number
     *
     */
    public LeadConversion(List<Lead> leadToConvertList, Boolean doNotCreateOpp, Map<String, String> telemarAccountNum){
        LeadConversionInit(leadToConvertList, doNotCreateOpp, telemarAccountNum);
    }
    
    /**
     *  @Constructor
     *  
     *  @param  leadId                  id of lead to be converted
     *  
     *  @param  doNotCreateOpp          Suppress opportunity creation
     *  
     *  @param  telemarAccountNumber    Telemar account number of the account to be updated
     */
    public LeadConversion(Id leadId, Boolean doNotCreateOpp, String telemarAccountNum)
    {
        lid = leadId;
        //get all fields that cannot be set with lead mapping
        List<Lead> tempL = new List<Lead>([ SELECT id, Email, Competitor__c, DoNotCall, Type__c, LeadSource, NewMover__c, SICCode__c, 
                                              FirstName, LastName, NewMoverOldSiteStreet__c, NewMoverOldSiteCity__c, NewMoverOldSiteState__c, 
                                              NewMoverOldSitePostalCode__c, CreatedDate, SourceAccountCreateDate__c, MMBCustomerNumber__c, 
                                              MMBSiteNumber__c, MMBMasterCustomerNumber__c,PropertyManagementCompany__c  
                                        FROM Lead 
                                        WHERE id =: lid ]);
        Map<String, String> tempM = new Map<String, String>();
        if( !Utilities.isEmptyOrNull(telemarAccountNum) ) 
            tempM.put( telemarAccountNum, tempL[0].Id);
        LeadConversionInit( tempL, doNotCreateOpp, tempM);                              
    }
    
    /**
     *  Instance initialization called from overloaded constructors
     *
     *  @param List<Lead>           leadToConvertList   List of leads to convert
     *
     *  @param Boolean              doNotCreateOpp      Flag indicating if opportunities need to be added as part of the conversion process
     *
     *  @param Map<String, String>  telemarAccountNum   Map containing Lead Ids to convert by Telemar Number
     *
     */
    private void LeadConversionInit(List<Lead> leadToConvertList, Boolean doNotCreateOpp, Map<String, String> telemarAccountNum){
        doNotCreateOpportunity = doNotCreateOpp;
        err = new list<Database.Error>();
        lBulkConvert = new List<Lead>();

        if( telemarAccountNum != null && !telemarAccountNum.isEmpty() ){
            List<String> telAccNoList = new List<String>();
            telAccNoList.addAll( telemarAccountNum.keySet() );          
            tan = telAccNoList[0];
            try
            {
                telemarAccountNumMap = new Map<String, String>();
                //for now assume that telemar account number is a unique value
                for(Account a: [SELECT Id FROM Account WHERE TelemarAccountNumber__c IN :telemarAccountNum.keySet()]){
                    
                    // Map containing account ids by lead id having a particular telemar number
                    telemarAccountNumMap.put( telemarAccountNum.get( a.TelemarAccountNumber__c ),  a.Id);
                    
                    // when called from all implementations using single conversions this will contain the account id 
                    aid = a.Id;
                }
            }
            catch (Exception ex) {}
        }
        
        if( leadToConvertList != null && !leadToConvertList.isEmpty())
            lBulkConvert.addAll( leadToConvertList );
    }
    
    /**
     *  Runs conversion logic from initialized list of leads
     *
     *  @return Boolean Returns true in case all processing for the lead conversion did not produced any errors
     *
     */
    public Boolean convertLead()
    {
        if( lBulkConvert == null || lBulkConvert.isEmpty() ){
            throw new LeadConversionException('No leads to convert.');
        }
        
        Map<String, LeadConvertFieldMap> lConvertValMap     = new Map<String, LeadConvertFieldMap>();
        Map<String, LeadConvertFieldMap> lConvertedValMap   = new Map<String, LeadConvertFieldMap>();
        List<Database.LeadConvert>       lcList             = new List<Database.LeadConvert>();
        
        Map<String, String> convertedLeadIds = new Map<String, String>();       
        List<Id> lContact               = new List<Id>(); 
        Boolean TransactSuccess         = true;
        
        //get lead converted status options (setting to Qualified)
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true Limit 1];
         
        //check if contact removal is required
        ResaleGlobalVariables__c rgv = ResaleGlobalVariables__c.getValues('CreateContactOnLeadConversion');
        Boolean createcontact = rgv.Value__c.equalsIgnoreCase('true');
        
        // Save lead field values to map into converted account
        for(Lead l: lBulkConvert){
            
            // Save field mapping values to be persisted over to account
            LeadConvertFieldMap lConvertVal     = new LeadConvertFieldMap(); 
            lConvertVal.lEmail                  = l.Email;
            lConvertVal.lCompetitor             = l.Competitor__c;
            lConvertVal.lDoNotCall              = l.DoNotCall;
            lConvertVal.lType                   = l.Type__c;
            lConvertVal.lLeadSource             = l.LeadSource;
            lConvertVal.lNewMover               = l.NewMover__c;
            lConvertVal.lSICCode                = l.SICCode__c;
            lConvertVal.lFirstName              = l.FirstName;
            lConvertVal.lLastName               = l.LastName;   
            lConvertVal.lNMStreet               = l.NewMoverOldSiteStreet__c;
            lConvertVal.lNMCity                 = l.NewMoverOldSiteCity__c;
            lConvertVal.lNMState                = l.NewMoverOldSiteState__c;
            lConvertVal.lNMPostalCode           = l.NewMoverOldSitePostalCode__c;
            lConvertVal.lCreateDate             = l.CreatedDate.date();
            lConvertVal.sourceSystemCreateDate  = l.SourceAccountCreateDate__c;
            lConvertVal.MMBCustomerNumber       = l.MMBCustomerNumber__c; 
            lConvertVal.MMBSiteNumber           = l.MMBSiteNumber__c; 
            lConvertVal.MMBMasterCustomerNumber = l.MMBMasterCustomerNumber__c;
            lConvertVal.PropertyManagementCompany = l.PropertyManagementCompany__c;
            
            // Create a lead conversion record referencing our lead to convert
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(l.id); 
            lc.setDoNotCreateOpportunity(doNotCreateOpportunity);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            if( telemarAccountNumMap != null && !telemarAccountNumMap.isEmpty() && telemarAccountNumMap.containsKey(l.id) ){
                lc.setAccountId( telemarAccountNumMap.get(l.id) );
            }
            lcList.add( lc );
            
            lConvertValMap.put(l.Id, lConvertVal);
        }
        
        // Bulk processing of lead conversion
        for(Database.LeadConvertResult lcr: Database.convertLead(lcList, doNotCreateOpportunity)){
            if( lcr.isSuccess() ){
                Id convLId = lcr.getLeadId();
                Id convAId = lcr.getAccountId();
                
                //set account id
                aid = convAId;
                
                lContact.add( lcr.getContactId() );
                
                // set default account values
                lConvertedValMap.put( convAId, lConvertValMap.get( convLId ) );
                
                // Save Id of converted lead
                convertedLeadIds.put( convLId,  convAId);   
            }
            else {
                err.addAll( lcr.getErrors() );
                TransactSuccess = false;
                //?.addError('Failed to convert Lead');
                ADTApplicationMonitor.log('LeadConversion',String.valueOf(err[0]),true,ADTApplicationMonitor.CUSTOM_APP.COMMERCIAL);
            }
            
        }
        
        // If conversions were successfull
        if( lConvertedValMap!= null && !lConvertedValMap.isEmpty() ){
            
            setDefaultAccountValues( lConvertedValMap );            
            
            // update related street sheet items
            updateStreetSheets( convertedLeadIds );
                
            //move disposition history
            moveDispositionHistory( convertedLeadIds );
            //moving call data to account on conversion-- Mounika Anna -- RV Account Lookup API
            moveCallDataRecords(convertedLeadIds);
            
                
            if (!createcontact && lContact != null && !lContact.isEmpty() )
                removeContact( lContact );
            else if( lContact != null && !lContact.isEmpty() )
                cid = lContact[0];
        }
        
        return TransactSuccess;
    }
    
    private void removeContact(List<Id> contactIdList){
        try
        {
            List<Contact> cList = [SELECT Id FROM Contact WHERE Id IN :contactIdList];
            delete cList;
        } catch (Exception ex) {}       
    } 
    
    public static void moveDispositionHistory(String rehashLeadId, String accountId){
        moveDispositionHistory( new Map<String, String>{rehashLeadId => accountId} );
    }
    
    /**
     *  Moves disposition object records from a lead to an account
     *
     *  @method moveDispositionHistory  
     *  
     *  @param  Map<Id, Id> The Id of the account to relate disposition records to from the lead the disposition records are related to 
     */
    public static void moveDispositionHistory( Map<String, String> convertedLeadIds )
    {
        List<disposition__c> disps = new List<disposition__c>();
        for (disposition__c d : [select Id, LeadId__c, AccountId__c from disposition__c where LeadId__c IN :convertedLeadIds.keySet()])
        {
            d.AccountId__c = convertedLeadIds.get( d.LeadId__c );
            d.LeadId__c = null;
            disps.add(d);
        }
        
        if( !disps.isEmpty() )
            update disps;
    }
    
      
    //Moving call Data records to account -- Mounika Anna
    public static void moveCallDataRecords( Map<String, String> convertedLeadIds ){
    List<Call_Data__c> cd = new List<Call_Data__c>();
        for(Call_Data__c c:[Select id,Lead__c,Account__c from Call_Data__c where Lead__c IN : convertedLeadIds.keySet()]){
            c.Account__c = convertedLeadIds.get(c.Lead__c);
            c.Lead__c = null;
            cd.add(c);
        }
        if(!cd.isEmpty())
            update cd;
    }
    
    private void setDefaultAccountValues( Map<String, LeadConvertFieldMap> lConvertFieldValMap) {
        if (aid == null) return;
        
        List<Account> aList = new List<Account>();
        //HRM-7313 start
        list<String> listpartnerDnis = new List<String>(); 
        Map<String,DNIS__c> dnisMap = new Map<String,DNIS__c>();
        //Map<String,String> partnerSourcingMdtMap = new Map<String,String>();
        
        for(Partner_Sourcing__mdt partnersourcingMDT: [Select DeveloperName,LeadSource__c,PartnerDNIS__c from Partner_Sourcing__mdt]){
            //add the lead source and partner DNIS to the map
            //partnerSourcingMdtMap.put(partnersourcingMDT.LeadSource__c,partnersourcingMDT.PartnerDNIS__c);
            if(String.isNotBlank(partnersourcingMDT.LeadSource__c))
        		listpartnerDnis.add(partnersourcingMDT.PartnerDNIS__c);
        }
        if(listpartnerDnis!=null && listpartnerDnis.size() > 0){
            List<DNIS__c> lstDnis = [Select id,Name,Tel_Lead_Source__c,Tel_Lead_Source__r.Name,Tel_Lead_Source__r.SourceNo__c from DNIS__c where Name IN : listpartnerDnis];
            if(lstDnis!=null && lstDnis.size() > 0){
            	for(DNIS__c dnis : lstDnis){
            		dnisMap.put(dnis.Name,dnis);
            	}
            }
        }
        
        User Loggedinuser = [Select id,Rep_Team__c from User where id =: UserInfo.getUserID()];
        //HRM-7313 --end
        for( Account a : [SELECT TransitionDate1__c,CreatedDate, TransitionDate2__c, TelemarLeadSource__c, Email__c, Competitor__c, DoNotCall__c, Type, Data_Source__c, 
                            NewMover__c, Sic, FirstName__c, LastName__c, LeadStatus__c, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                            MMBSiteNumber__c,  MMBMasterCustomerNumber__c, MMBCustomerNumber__c,DNIS__c,DNIS_Modified_Date__c,DNIS_Modifier_Name__c,lastmodifieddate 
                          FROM Account 
                          WHERE Id = :lConvertFieldValMap.keySet()]){
            
            // Get field values to persist from lead for this account
            LeadConvertFieldMap lConvertFieldVal = lConvertFieldValMap.get(a.Id);
                            
            a.TransitionDate1__c = lConvertFieldVal.lCreateDate;
            a.TransitionDate2__c = system.today();          
            a.ProcessingType__c = IntegrationConstants.PROCESSING_TYPE_USER;
            // unless a source system create date has been provided
            if ( lConvertFieldVal.sourceSystemCreateDate != null ) {
                Date comparisonDate = Date.today();
                comparisonDate = comparisonDate.addMonths(-6);
                // then if source system create date is less than 6 months ago
                if ( lConvertFieldVal.sourceSystemCreateDate > comparisonDate) {
                    // processing type is field
                    a.ProcessingType__c = IntegrationConstants.PROCESSING_TYPE_FIELD;
                } 
            }   
            a.Email__c          = lConvertFieldVal.lEmail;
            a.Competitor__c     = lConvertFieldVal.lCompetitor;
            a.DoNotCall__c      = lConvertFieldVal.lDoNotCall;
            a.Type              = lConvertFieldVal.lType;
            a.Data_Source__c    = lConvertFieldVal.lLeadSource;
            a.NewMover__c       = lConvertFieldVal.lNewMover;
            a.sic               = lConvertFieldVal.lSICCode;
            a.FirstName__c      = lConvertFieldVal.lFirstName;
            a.LastName__c       = lConvertFieldVal.lLastName;
            a.LeadStatus__c     = 'Active';
            a.BillingStreet     = lConvertFieldVal.lNMStreet;
            a.BillingPostalCode = lConvertFieldVal.lNMPostalCode;
            a.BillingState      = lConvertFieldVal.lNMState;
            a.BillingCity       = lConvertFieldVal.lNMCity;
            a.MMBSiteNumber__c  = lConvertFieldVal.MMBSiteNumber; 
            a.MMBMasterCustomerNumber__c = lConvertFieldVal.MMBMasterCustomerNumber;
            a.MMBCustomerNumber__c       = lConvertFieldVal.MMBCustomerNumber;
            //Assign Property mamangement company id to parent Account id on account on lead conversion
            if(String.isNotBlank(lConvertFieldVal.PropertyManagementCompany)){
            	a.Parentid = lConvertFieldVal.PropertyManagementCompany; 
            }
            //HRM-7272 DNIS Changes for Field Flow --- Mounika Anna
            // HRM-7313 Amplifinity Changes --- Giribabu geda
            if(String.isnotblank(Loggedinuser.Rep_Team__c) && Loggedinuser.Rep_Team__c.containsIgnoreCase('FIELDSALES')){
                LeadHelper.setDnis(a, lConvertFieldVal.lLeadSource,Loggedinuser,dnisMap);  
            }
            
            aList.add(a);
        }
        
        if( !aList.isEmpty() )
            update aList;
    }
    
    private void updateStreetSheets( Map<String, String> convertedLeadIds) {
        if ( convertedLeadIds == null || convertedLeadIds.isEmpty() == null ) return;
        List<StreetSheetItem__c> ssitems = new List<StreetSheetItem__c>(); 
        for (StreetSheetItem__c ssi : [ SELECT LeadId__c, AccountId__c FROM StreetSheetItem__c WHERE LeadId__c IN :convertedLeadIds.keySet() ]) {
            ssi.AccountID__c = convertedLeadIds.get( ssi.LeadId__c );
            ssi.LeadId__c = null;
            ssitems.add(ssi);
        }
        
        if( !ssitems.isEmpty())
            update ssitems;
    }
}