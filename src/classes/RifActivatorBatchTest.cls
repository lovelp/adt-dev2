/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RifActivatorBatchTest {
	
	static testMethod void testExecuteArchivedInService() {
        
        User admin1 = TestHelperClass.createUser(100);
        User admin2 = TestHelperClass.createUser(200);
        System.runAs(admin1) {        
        	TestHelperClass.createReferenceDataForTestClasses();
        	TestHelperClass.createReferenceUserDataForTestClasses();
        }
        
        
        Account a;
        
		System.runAs(admin1) {
			a = TestHelperClass.createAccountData();
        	a.Type = IntegrationConstants.TYPE_RIF;
        	a.LeadStatus__c = 'Archived';
        	a.ArchivedReason__c = 'In Service';
        	a.RecordTypeId = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account').Id;
        	update a;
		}
        
        Test.startTest();
        
        System.runAs(admin2) {
        	RifActivatorBatch rab = new RifActivatorBatch();
        	rab.query = 'select id, Type, LeadStatus__c, RecordTypeId from Account where id =\'' + a.Id + '\'';
       		Database.executeBatch(rab);
        }	
            
        Test.stopTest();
        
        Account aAfter = [select id, Type, LeadStatus__c from Account where id = : a.Id];
        
        System.assert(aAfter != null, 'Account should exist');
        System.assertEquals(IntegrationConstants.TYPE_RIF, aAfter.Type);
        System.assertEquals('Active', aAfter.LeadStatus__c);
    }
    
    static testMethod void testExecuteArchivedOutOfService() {
        
        User admin1 = TestHelperClass.createUser(100);
        User admin2 = TestHelperClass.createUser(200);
        System.runAs(admin1) {        
        	TestHelperClass.createReferenceDataForTestClasses();
        	TestHelperClass.createReferenceUserDataForTestClasses();
        }
        
        
        Account a;
        
		System.runAs(admin1) {
			a = TestHelperClass.createAccountData();
        	a.Type = IntegrationConstants.TYPE_RIF;
        	a.LeadStatus__c = 'Archived';
        	a.ArchivedReason__c = IntegrationConstants.ARCHIVED_REASON_OUT_OF_SERVICE;
        	a.RecordTypeId = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account').Id;
        	update a;
		}
        
        Test.startTest();
        
        System.runAs(admin2) {
        	RifActivatorBatch rab = new RifActivatorBatch();
       		Database.executeBatch(rab);
        }	
            
        Test.stopTest();
        
        Account aAfter = [select id, Type, LeadStatus__c from Account where id = : a.Id];
        
        System.assert(aAfter != null, 'Account should exist');
        System.assertEquals(IntegrationConstants.TYPE_RIF, aAfter.Type);
        System.assertEquals('Archived', aAfter.LeadStatus__c);
    }
    
    static testMethod void testExecuteStandardAccount() {
        
        User admin1 = TestHelperClass.createUser(100);
        User admin2 = TestHelperClass.createUser(200);
        System.runAs(admin1) {        
        	TestHelperClass.createReferenceDataForTestClasses();
        	TestHelperClass.createReferenceUserDataForTestClasses();
        }
        
        
        Account a;
        
		System.runAs(admin1) {
			a = TestHelperClass.createAccountData();
			// would expect the type of a standard account to be Other
			// but use Customer Site for the purposes of this test
			// in case there is some unexpected path that results in a Standard Account
			// of Type Customer Site
        	a.Type = IntegrationConstants.TYPE_RIF;
        	a.LeadStatus__c = 'Archived';
        	a.ArchivedReason__c = IntegrationConstants.ARCHIVED_REASON_OUT_OF_SERVICE;
        	a.RecordTypeId = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account').Id;
        	update a;
		}
        
        Test.startTest();
        
        System.runAs(admin2) {
        	RifActivatorBatch rab = new RifActivatorBatch();
       		Database.executeBatch(rab);
        }	
            
        Test.stopTest();
        
        Account aAfter = [select id, Type, LeadStatus__c from Account where id = : a.Id];
        
        System.assert(aAfter != null, 'Account should exist');
        System.assertEquals(IntegrationConstants.TYPE_RIF, aAfter.Type);
        System.assertEquals('Archived', aAfter.LeadStatus__c);
    }
    
    static testMethod void testExecuteResaleAccount() {
        
        User admin1 = TestHelperClass.createUser(100);
        User admin2 = TestHelperClass.createUser(200);
        System.runAs(admin1) {        
        	TestHelperClass.createReferenceDataForTestClasses();
        	TestHelperClass.createReferenceUserDataForTestClasses();
        }
        
        
        Account a;
        
		System.runAs(admin1) {
			a = TestHelperClass.createAccountData();
			// would expect the type of a resale account to be Pending Discontinuance or Discontinuance
			// but use Customer Site for the purposes of this test
			// in case there is some unexpected path that results in a Standard Account
			// of Type Customer Site
        	a.Type = IntegrationConstants.TYPE_RIF;
        	a.LeadStatus__c = 'Archived';
        	a.ArchivedReason__c = IntegrationConstants.ARCHIVED_REASON_OUT_OF_SERVICE;
        	a.RecordTypeId = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account').Id;
        	update a;
		}
        
        Test.startTest();
        
        System.runAs(admin2) {
        	RifActivatorBatch rab = new RifActivatorBatch();
       		Database.executeBatch(rab);
        }	
            
        Test.stopTest();
        
        Account aAfter = [select id, Type, LeadStatus__c from Account where id = : a.Id];
        
        System.assert(aAfter != null, 'Account should exist');
        System.assertEquals(IntegrationConstants.TYPE_RIF, aAfter.Type);
        System.assertEquals('Archived', aAfter.LeadStatus__c);
    }
    

    
    static testMethod void testValidateSOQL() {
        
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        	TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        
        Test.startTest();
        
        String query;
        System.runAs(admin) {
        	RifActivatorBatch rab = new RifActivatorBatch();
       		query = rab.query;
        }	
        
        try
        {
            List<Account> acctList = new List<Account>();
            acctList =  database.Query(query);            
        }
        catch(Exception e)
        {
            System.assert(false, 'An exception should not have occurred.  There is likely a problem with the SOQL');        
        }
            
        Test.stopTest();
        
        
    }
    
    

}