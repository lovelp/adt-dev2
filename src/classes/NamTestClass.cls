@isTest
public class NamTestClass {
   static testMethod void testAccountInsertionEmailtest(){
        List <user> u = [select id from user where Profile.Name ='ADT Commercial Representative' limit 2];
        TestHelperClass.inferPostalCodeID('221O2', '1200');
        Account a1 = new Account();
        a1.Data_Source__c = 'ADMIN';
        a1.Business_Id__c = '1300';
        a1.DisconnectReason__c = '';
        a1.Type = 'Resale';
        a1.ResoldType__c = '';
        a1.ShippingStreet = '8250 Jones Branch Dr';
        a1.ShippingCity = 'McLean';
        a1.ShippingState = 'VA';
        a1.ShippingPostalCode = '221O2';
        a1.Name = TestHelperClass.ACCOUNT_NAME;
        
        try {
            insert a1;
        } catch (Exception e) {
            System.assert(false, 'Exception while setting up data: ' + e.getMessage() + ' ' + e.getStackTraceString());
        }
        
        Account fullAcct = [select id, Data_Source__c,OwnerId, Business_Id__c, DisconnectReason__c, Type, ResoldType__c, Name, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode from Account where Id = :a1.Id];
        
        fullAcct.OwnerId = u[1].id;
        
        Test.startTest();
        
        update fullAcct;
        Test.stopTest(); 
    }
    
   /* @isTest static  void testProcessLeadsBeforeInsert()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1];
        LIST<Lead> lstLds=new LIST <LEAD>(); 
        userrole r = [select id,name,developername from userrole where DeveloperName = 'Atlantic_Comm_Sales_AR102_SM1_Rep'  limit 1];
        User u = new user();
        u.LastName = 'Test Code';
        u.Email = 'test@test123.com';
        u.Alias = 'Tcode';
        u.Username = 'test1234444@test123.com';
        u.CommunityNickname = 'test12';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'GMT';
        u.ProfileID = '00e90000000oyi5';
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.UserRoleId = r.id;
        u.ProfileId = p.Id;
        insert u;
        System.runas(u){
            Lead L2 = new Lead();
            //L2.name = 'Robert';
            L2.Business_Id__c = '1300 - Commercial';
            L2.Channel__c= 'Business Direct Sales';
            L2.DispositionCode__c = 'Phone - No Answer';
            L2.SiteStreet__c = '1320 Brook Rd';
            L2.SiteCity__c = 'CHARLOTTE';      
            L2.SiteStateProvince__c = 'NC';
            L2.SiteCountryCode__c = 'US';
            L2.SitePostalCode__c = '28205';
            L2.NewMoverType__c='PN';
            insert L2;
        }
        //LeadTriggerHelper.sendEmailonCreation(lstLds,newLeadMap);
    }
    */
    /*
    @isTest static void sendLeadAssignmentEmailTest(){
        // insert a lead with required user role
        //get user details 
        Test.startTest();   
        List <user> u = [select id from user where Profile.Name ='ADT Commercial Representative' limit 2];
        //user u1 = [select id from user where Profile.Name ='ADT Commercial Representative' limit 2];
        Lead L2 = new Lead();
        //L2.name = 'Robert';
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.name = '33431';
        pc.BusinessID__c = '1300';
        pc.APM_PostalCode__c = true;
        insert pc;
        L2.Company = 'TestCompany';
        L2.DispositionCode__c = 'Phone - No Answer';
        L2.AddressID__c = null;
        L2.Business_Id__c = null;
        L2.NewMoverDate__c = Date.today().addDays(-30);
        L2.Business_Id__c = '1300 - Commercial';
        L2.Channel__c= 'Business Direct Sales';
        L2.DispositionCode__c = 'Phone - No Answer';
        L2.SiteStreet__c = '1501 Yamato Road';
        L2.SiteCity__c = 'Boca Raton';      
        L2.SiteStateProvince__c = 'FL';
        L2.SiteCountryCode__c = 'US';
        L2.SitePostalCode__c = '33431';
        L2.NewMoverType__c='PN';
        L2.PostalCodeID__c = pc.id;
        L2.OwnerId = u[0].id;
        insert L2;
        // check the emails sent in this transaction
        integer invocations = limits.getEmailInvocations();
        system.assertequals(1,invocations);
        L2.OwnerId = u[1].id;
        update L2;
        Test.stopTest();
    }
    */
    
    
    
    
    
}