public without sharing class NSCTimerController {
	
    public String CalldataId {get;set;}
    public String calltime {get;set;}
    public Call_Data__c calldata {get;set;}
    public List<NSCTimecolorpicker__c> listA {get;set;}
    public String temp {get;set;}
    public static String CallDataPrefix {get;set;}
    
    public NSCTimerController(){
        CallDataPrefix = Call_Data__c.sobjecttype.getDescribe().getKeyPrefix();
        calltime = '0';
        listA = [SELECT name, Color__c FROM NSCTimecolorpicker__c];
        temp = Json.serialize(listA);
    }
    
    public pageReference Savetime(){
        system.debug('call time val is '+calltime);
        system.debug('Calldata val is '+CalldataId);
    	try{
	        if( !String.isBlank( CalldataId ) ){
	            if(callData == null){
	                calldata = [SELECT id,Call_Time__c FROM Call_Data__c WHERE id = :CalldataId];
	            }
	            calldata.Call_Time__c = integer.ValueOf(calltime);
	            update calldata;
	        }
    	}
    	catch(Exception err){
    		ADTApplicationMonitor.log(err, 'NSCTimerController', 'Savetime', ADTApplicationMonitor.CUSTOM_APP.MATRIX);
    	}
        return null;
    }
    
}