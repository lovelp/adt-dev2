@isTest
private class StreetSheetTabControllerTest {
	
	static testMethod void testRedirect() {
		
		
		
		Test.startTest();
		
		StreetSheetTabController sstc = new StreetSheetTabController();

		PageReference newRef = sstc.redirect();
		System.assert(newRef != null, 'Expect to be navigated to the All list view for Street Sheets');
		
		Test.stopTest();
		
		
	}

}