/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CallidusCaseNewControllerTest {

    static testMethod void PageLoadWithoutAccountTest() {
        PageReference pRef = Page.CallidusCaseNew;
        Test.setCurrentPage(pRef);
        CallidusCaseNewController ctrlObj = new CallidusCaseNewController();
        
        // Account cases
        List<Case> cList1 = CallidusCaseNewController.getUserCallidusCases( '' );
    }
    
    static testMethod void PageLoadWithAccountTest() {
        PageReference pRef = Page.CallidusCaseNew;       
    	User SalesRep;
		Account a1;
		Case CommissionCase;
		String MMBCustomerNo = '1234562';
    	test.startTest();
		System.runAs(TestHelperClass.createAdminUser()) {
			TestHelperClass.createReferenceDataForTestClasses();
			TestHelperClass.createReferenceUserDataForTestClasses();
		}

    	// Init management hierarchy
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {    
           
    		SalesRep = TestHelperClass.createExecWithTeam();

			// Assigns permission sets to the appropriate users and grant access to callidus cases
			PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'Commission_Error_Permission'];
			PermissionSetAssignment PSAssignment = new PermissionSetAssignment();
			PSAssignment.AssigneeId = SalesRep.id;
			PSAssignment.PermissionSetId = ps.ID;
			insert PSAssignment; 
            
        
        }
		
		SalesRep.Business_Unit__c = 'SB Direct Sales';
		update SalesRep; 
		
        System.runAs(SalesRep) {
			a1 = TestHelperClass.createAccountData();
        }        
        
        Test.setCurrentPage(pRef);
        pRef.getParameters().put('def_account_id', a1.Id);
        CallidusCaseNewController ctrlObj = new CallidusCaseNewController();
        
        // Case Subject
        ctrlObj.caseObj.Compensation_Plan__c = 'L1';
        ctrlObj.caseObj.Commission_Reason_Code__c = '1A';
        PageReference pref1 = ctrlObj.UpdateCaseSubject();
        
        // Validate info
        Boolean b1 = CallidusCaseNewController.validateCaseInfo( MMBCustomerNo , ctrlObj.caseObj.Compensation_Plan__c, ctrlObj.caseObj.Commission_Reason_Code__c);
        
        // Account cases
        List<Case> cList1 = CallidusCaseNewController.getUserCallidusCases( a1.Id );
        
        // Save Case
        PageReference pref2 = ctrlObj.save();
        test.stopTest();
        
    }
    
}