/************************************* MODIFICATION LOG ********************************************************************************************
* 
* Name: CFGFeedBatchTest
*
* DESCRIPTION :
* Test Coverage for CFGFeedBatch class
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                        DATE                        Ticket              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari               06/08/2019                   HRM-9790          FlexFI National - Ability to vie Loan info
* 
********************************************************************************************************************************************************/
@isTest
private class CFGFeedBatchTest {

    static LoanApplication__c lApp;
    static Account acct;
    static testMethod void testCFGBatch() {
        createTestData();
        Test.startTest();
        list<Loan_Close_Feed__c> cfgList = [Select Id, LoanApplicationNumber__c, Previous_Status__c,Status__c, CreatedDate 
                                                From Loan_Close_Feed__c Where LoanApplicationNumber__c != null
                                                AND Partner__c = 'CFG' 
                                                order by LoanApplicationNumber__c asc, createddate desc];
        system.assertEquals(cfgList.isEmpty(), false);
        system.debug('$$$' + cfgList);
        lApp = [Select Id, CFG_Loan_Status__c, CFG_Status_Update_Timestamp__c From LoanApplication__c where Id = :lApp.Id limit 1];
        system.assertEquals('Active', lApp.CFG_Loan_Status__c);
        system.assert(lApp.CFG_Status_Update_Timestamp__c == null);
        Database.executeBatch(new CFGFeedBatch());
        Test.stopTest();
        cfgList = [Select Id, LoanApplicationNumber__c, FirstName__c, LastName__c, Order_Number__c, Previous_Status__c,Status__c
                            , ProcessingStatus__c, ProcessingError__c
                                        From Loan_Close_Feed__c Where LoanApplicationNumber__c != null
                                        order by ProcessingStatus__c asc];
        system.assertEquals(false, cfgList.isEmpty());
        system.assertEquals(2, cfgList.size());
        //system.assertEquals('Completed', cfgList.get(0).ProcessingStatus__c);
        system.assertEquals('Error', cfgList.get(1).ProcessingStatus__c);
        system.assertEquals('No matching Loan Application number is found for this Loan Application Number.', cfgList.get(1).ProcessingError__c);
        
        lApp = [Select Id, CFG_Loan_Status__c, CFG_Status_Update_Timestamp__c From LoanApplication__c where Id = :lApp.Id limit 1];
        system.assert(lApp.CFG_Status_Update_Timestamp__c != null);
        system.assertEquals('Closed', lApp.CFG_Loan_Status__c);
    }
    public static void createTestData(){
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootLoanDecisionEndPoint__C = 'testurl';
        insert is;

        list<FlexFiConfig__c> flexConfigs = new list<FlexFiConfig__c>();
        FlexFiConfig__c fc1 = new FlexFiConfig__c(value__c='test');
        fc1.name='LoanType';
        flexConfigs.add(fc1);
        
        FlexFiConfig__c fc2 = new FlexFiConfig__c(value__c='test');
        fc2.name='TransactionType';
        flexConfigs.add(fc2);
        
        FlexFiConfig__c fc3 = new FlexFiConfig__c(value__c='test');
        fc3.name='autoBookFlg';
        flexConfigs.add(fc3);
        
        insert flexConfigs;
        
        map<String, String> EquifaxSettings = new map<String, String>
                        {
                          'Order Types For Credit Check'=>'N1;R1'
                        , 'Order Types For Credit Check - Renters'=>'N1;R1'
                        , 'Failure Risk Grade'=>'X'
                        , 'Failure Condition Code'=>'APPROV'
                        , 'Default Risk Grade'=>'W'
                        , 'Default Condition Code'=>'CAE1'
                        , 'Employee Risk Grade'=>'U'
                        , 'Employee Condition Code'=>'CME3'     
                        , 'Days to be considered out of date'=>'90'
                        , 'Days to allow additional check'=>'1'};
        List<Equifax__c> EquifaxSettingList = new List<Equifax__c>(); 
        for( String sN: EquifaxSettings.keySet()){
            Equifax__c eS = new Equifax__c( name = sN );
            eS.value__c = EquifaxSettings.get(sN);
            EquifaxSettingList.add( eS );
        }
        insert EquifaxSettingList;
        //Create Account
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '4';
        insert resale;
        ResaleGlobalVariables__c InteUser = new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User');
        Insert     InteUser;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name='RIFAccountOwnerAlias'; 
        rgv.value__c='ADT RIF';
        insert rgv;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        acct = new Account(Name = 'Unit Test Account 2');
        acct.AddressID__c = addr.Id;
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';
        acct.Email__c = 'abc@gmail.com';
        insert acct;
        
        lApp = new LoanApplication__c();
        lApp.Account__c = acct.id;
        lApp.BillingAddress__c =addr.id;
        lApp.ShippingAddress__c =addr.id;
        //lApp.TAndCUpdatedDate__c =DateTime.parse('12/05/2018 11:46 AM');
        lApp.PreviousAddressLine1__c ='Prev Add Line 1';
        lApp.PreviousAddressLine2__c ='Prev Add Line 2';
        lApp.PreviousAddressPostalCd__c ='27518';
        lApp.PreviousAddressStateCd__c ='NC';
        lApp.PreviousAddressCountryCd__c='USA';
        lApp.LoanNumber__c = '6058300920000010';
        lApp.CFG_Loan_Status__c = 'Active';
        insert lApp;
        
        Loan_Close_Feed__c fd = new Loan_Close_Feed__c();
        fd.LoanApplicationNumber__c = '123454';
        fd.Status__c = 'Writeoff';
        fd.FirstName__c = acct.FirstName__c;
        fd.LastName__c = acct.LastName__c;
        fd.Order_Number__c = '10001000002985901';
        fd.Previous_Status__c = 'Active';
        fd.Partner__c = 'CFG';
        insert fd;
        
        Loan_Close_Feed__c fd1 = new Loan_Close_Feed__c();
        fd1.LoanApplicationNumber__c = lApp.LoanNumber__c;
        fd1.Status__c = 'Closed';
        fd1.FirstName__c = acct.FirstName__c;
        fd1.LastName__c = acct.LastName__c;
        fd1.Order_Number__c = '10001000002985900';
        fd1.Previous_Status__c = 'Writeoff';
        fd1.Partner__c = 'CFG';
        insert fd1;
        
    }
}