/************************************* MODIFICATION LOG ********************************************************************************************
* 
* CPQIntegrationDataGenerator
*
* DESCRIPTION :
* Generates the JSON String which contains the following objects data:
* - Permits
* - Trip Fees
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE             Ticket         REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan               01/26/2016                  - Original Version
* Magdiel Herrera               03/01/2016                  - Added flag to support Pulse enabled sites to CPQ
* Mounika Anna                  02/26/2018      HRM-6275    - Added a condition to check the profile type of the rep user and send 900001 as the HRID for OSC agents
* Prashant Pandey               04/23/2018      HRM-6915    - Added Street 3
* Mounika Anna                  06/29/2018      HRM-7481    - CPQ integration Data fix for Informix billing system 
* Mounika Anna                  08/21/2018      HRM-7386    - Monitoring Tech
* Siddarth Asokan               09/25/2018      HRM-8017    - Renter Credit Rule changes & refactoring credit check
* Abhinav Pandey                10/29/2018      HRM-7489    - Simple Sell Changes
* Siju Varghese                 10/29/2018      HRM-8401    - Added TownName in JSON
* Giribabu Geda                 12/13/2018      HRM-8738    - Added Loan Application card details
* Siddarth Asokan               03/05/2019      HRM-9288    - Added separate wrapper for Simple Sell & added allowed flag
* Jitendra Kothari              03/22/2019      HRM-9627    - Added Credit info override
* Siddarth Asokan               04/08/2019      HRM-9405    - Added system type as part of SMB Cyber changes
* Srinivas Y                    09/19/2019      HRM-10745   - Add customerstartdate and sitestartdate.
* Abhinav Pandey                11/03/2019      HRM-10881   - Flex Fi Changes
*/
public class CPQIntegrationDataGenerator {
    public class IntegrationDataSource {
        public Account      a                   { get; set; }
        public Trip_Fee__c  fee                 { get; set; }
        public Boolean      useContextUser      { get; set; }
        public String       equifaxApprovalType { get; set; }
        public String       equifaxRiskGrade    { get; set; }
        public IntegrationDataSource(){
            // default use account owner
            this.useContextUser = false;
        }
        public IntegrationDataSource( Id accId ){
            this();
            //HRM-6915 added new field in the query sitestreet3
            //HRM- 7386 -- Mounika Anna
            this.a = [SELECT Name, Channel__c, MMB_Past_Due_Balance__c, SiteState__c, SiteStreet3__c, Business_Id__c, PreviousBillingCustomerNumber__c, 
                      MMBBillingSystem__c, MMBSystemType__c, AddressID__c, AddressID__r.PostalCode__c, ResaleTownNumber__c, Profile_RentOwn__c,
                      EquifaxRiskGrade__c, EquifaxApprovalType__c, Equifax_Last_Check_DateTime__c, MMBOrderType__c, MMB_Relo_Billing_System__c,
                      ADTEmployee__c,CLV__c,Churn_Value__c,PostalCodeID__c,VIPDepositWaiver__c,PostalCodeID__r.Town_Lookup__r.name, MMBCustomerStartDate__c, 
                      MMBSiteStartDate__c,MMBSystemStartDate__c FROM Account WHERE Id = :accId];
        }
    }
        
    private class CPQData {
        // Main Class which needs to be serialized
        public list<permitData> permits;
        public SimpleSellData simpleSell; // HRM-9288
        public tripFeeData ADTFee;
        public Boolean ADTP2Enabled;
        //public Boolean InformixCTCFlag;
        public billingInfo billingInfoData;
        public SystemInfo systemInfoData; // HRM-9405
        public userData user;
        public Tenure Tenure;
        public EquifaxData CreditScore;
        public Boolean LicenseRequired;
        public Integer ADTEmployee;
        public String CLV = '';
        public String Churn = '';
        public String APM; // added by Srini for 6109
        public string SiteStreet3; // added by TCS for HRM 6915
        public Boolean vipDepositWaiver; // HRM-7386 -- Mounika Anna
        public String townName;// HRM-8401 -- Siju
        public LoanApplicationDetails loanApplicationDetails; //HRM-8738 - Loan Application details wrapperlist 
    }
    
    //----------------------------------------------------------------------------------------
    //  Credit Check wrapper
    //----------------------------------------------------------------------------------------
    private class EquifaxData {
        public String RiskGrade;
        public String ApprovalType;
        public String PaymentFreq;
        public String DepositPercent;
        public String ThreePay;
        public String EasyPay;
    }
    
    //----------------------------------------------------------------------------------------
    //  User Information wrapper
    //----------------------------------------------------------------------------------------
    private class userData {
        // For Current Logged in User
        public String Name;
        public String HRID;
        public String MobilePhone;
        public String ManagerName;
        public String ManagerEmail;
    }
    
    //----------------------------------------------------------------------------------------
    //  Billing Information  wrapper
    //----------------------------------------------------------------------------------------
    private class billingInfo {
        public string PastDueAmount;
        public string PrevCustNum;      
    }
    
    //----------------------------------------------------------------------------------------
    //  System Information  wrapper
    //----------------------------------------------------------------------------------------
    private class SystemInfo {
        public string SystemType;
    }
    
    //----------------------------------------------------------------------------------------
    //  Permit wrapper
    //----------------------------------------------------------------------------------------
    private class permitData {
        public String PermitType;
        public String PermitAmount;
        public String MMBBillCode;
    }
    
    //----------------------------------------------------------------------------------------
    //  Simple Sell Products Wrapper
    //----------------------------------------------------------------------------------------
    private class SimpleSellData{
        public string Allowed;
        public String SimpleSellProductNotes; //HRM 7489
        public list<SimpleSellProducts> SimpleSellProductDetails; //HRM 7489
    }
    private class SimpleSellProducts {
        public String pName;
        public String pQuantity;
    }
    
    //----------------------------------------------------------------------------------------
    //  Trip Fee wrapper
    //----------------------------------------------------------------------------------------
    private class tripFeeData {
        public String FeeType;
        public String MinLimit;
        public String MaxLimit;
    }
    
    //----------------------------------------------------------------------------------------
    //  Loan Application Wrapper
    //----------------------------------------------------------------------------------------
    private class LoanApplicationDetails {
        public String PayName;
        public String PayCustRef;
        public String PayType;
        public String PayLast4;
        public String PayExpiry;
        public String ThirdPartyCustNbr;
        public String Finance;
    }
    
    //Tenure
    private class Tenure{
        public String SiteStartDate;
        public String CustomerStartDate;
        public String SystemStartDate;
     }

    
    /**
     *  Returns an object in JSON format with information needed by CPQ integration
     *
     *  @method getCPQIntegrationJSON
     *
     *  @param  IntegrationDataSource   
     *          - Source data used for creating this JSON object
     *
     *  @return String
     *          - JSON object encapsulating CPQ information
     *
     */
    public static string getCPQIntegrationJSON( IntegrationDataSource iSource ){
        CPQData mainData = new CPQData();
        Account a = iSource.a;
        Trip_Fee__c fee = iSource.fee;
        
        //Handling 13 states
        ProposalOnlyStates__c proposalstates = ProposalOnlyStates__c.getValues('States');
        list<String> statesList = proposalstates.states__c.split(',');
        set<String> statesSet = new set<String>(statesList);
        mainData.LicenseRequired = (statesSet.contains(iSource.a.SiteState__c))? true : false;
        
        //Shiva Pochamalla
        mainData.ADTEmployee = (a.ADTEmployee__c)? 1 : 0;
        
        //mounika anna
        //CLV Value
        if(String.isNotBlank(a.CLV__c)){
            mainData.CLV = a.CLV__c;
        }
        
        //Churn
        if(a.Churn_Value__c != null){
            mainData.Churn = a.Churn_Value__c;
        }
        
        // HRM-7386 --- Mounika Anna
        mainData.vipDepositWaiver = a.VIPDepositWaiver__c;
        
        //HRM-8401 - Siju
        mainData.townName = (String.isNotBlank(a.PostalCodeID__r.Town_Lookup__c) && String.isNotBlank(a.PostalCodeID__r.Town_Lookup__r.name))? a.PostalCodeID__r.Town_Lookup__r.name : '';
            
        //Added by TCS for 6915
        if(a.SiteStreet3__c != null){
            mainData.SiteStreet3 = a.SiteStreet3__c.replaceAll(' ','_');
        }
        
        // User Details
        userData usrData = new userData();
        Account repUserAcc = [SELECT Rep_User__c,Rep_User__r.Profile_Type__c, Rep_Phone__c, Rep_Name__c, Rep_Manager_Phone__c, Rep_Manager_Name__c, Rep_Manager_Email__c, Rep_License__c, Rep_HRID__c,MMBOrderType__c,ADTEmployee__c FROM Account WHERE ID = :a.Id];
        //HRM-6275 Added a condition to check the profile type of the rep user and send 900001 as the HRID
        User loggedInUser = [select id,Profile_Type__c,SimpleSell__c from User where id=:Userinfo.getUserId()];
        usrData.Name = repUserAcc.Rep_Name__c;
        // Made changes by Mounika Anna for --- HRM-6275
        if(loggedInUser!=null && loggedInUser.Profile_Type__c!=null && loggedInUser.Profile_Type__c.containsIgnoreCase('OSC')){
            usrData.HRID = ResaleGlobalVariables__c.getinstance('OSCHRID').value__c;
        }else{
            usrData.HRID = (String.isNotBlank(repUserAcc.Rep_HRID__c))? repUserAcc.Rep_HRID__c:null;
        }
        //end of HRM-6275 Changes.
        usrData.MobilePhone = '';
        if(String.isNotBlank(repUserAcc.Rep_Phone__c) ){
            string nondigits = '[^0-9]';      
            // remove all non numeric
            usrData.MobilePhone = repUserAcc.Rep_Phone__c.replaceAll(nondigits,'');
        }
        usrData.ManagerName = (String.isNotBlank(repUserAcc.Rep_Manager_Name__c))? repUserAcc.Rep_Manager_Name__c: null;
        usrData.ManagerEmail = (String.isNotBlank(repUserAcc.Rep_Manager_Email__c))? repUserAcc.Rep_Manager_Email__c: null;
        mainData.user = usrData;
        
        //Tenure details HRM 10745
        if(a.MMBSiteStartDate__c != null || a.MMBCustomerStartDate__c !=null || a.MMBSystemStartDate__c !=null ){
            Tenure tenr = new Tenure();
            tenr.SiteStartDate = String.valueOf(a.MMBSiteStartDate__c);
            tenr.CustomerStartDate = String.valueOf(a.MMBCustomerStartDate__c);
            tenr.SystemStartDate = String.valueOf(a.MMBSystemStartDate__c);
            mainData.Tenure = tenr;
        }
        
        // Informix Details
        system.debug('####'+a.MMB_Past_Due_Balance__c);
        billingInfo iData = new billingInfo();
        if (a.MMB_Past_Due_Balance__c != Null){
            iData.PastDueAmount = String.valueOf(a.MMB_Past_Due_Balance__c.setScale(2));
        }
        if(String.isNotBlank(a.PreviousBillingCustomerNumber__c) && (((a.MMBOrderType__c == 'R3' || a.MMBOrderType__c == 'N2') && String.isNotBlank(a.MMB_Relo_Billing_System__c) && a.MMB_Relo_Billing_System__c.containsIgnoreCase('INF')) || (String.isNotBlank(a.MMBBillingSystem__c) && a.MMBBillingSystem__c.containsIgnoreCase('INF')))){
            //HRM-7481 CPQ integration Data fix for Informix billing system -- Mounika Anna
            iData.PrevCustNum = a.PreviousBillingCustomerNumber__c;
        }
        system.debug('####'+iData.PastDueAmount);
        if(String.isNotBlank(iData.PastDueAmount) || String.isNotBlank(iData.PrevCustNum)){
            system.debug('Hey I am here');
            mainData.billingInfoData = iData;
        }
        system.debug('####'+mainData.billingInfoData);
        // HRM-9405 System Info Details
        if(String.isNotBlank(a.MMBSystemType__c)){
            SystemInfo sysInfo = new SystemInfo();
            sysInfo.SystemType = a.MMBSystemType__c;
            mainData.systemInfoData = sysInfo;
        }
        
        // Permit Details
        list<permit__c> permitLst = new list<permit__c>();
        String BusinessID = '';      
        if (a.AddressID__c != null && String.isNotBlank(a.AddressID__r.PostalCode__c)){
            String LOB = 'Residential';
            // Line of Business Mapping
            if (String.isNotBlank(a.Channel__c)){
                // HRM-10450 Biz Id Change Start
                String bizId = Channels.getBusinessId(a.Channel__c);
                if(String.isNotBlank(bizId)){
                    BusinessID = Channels.getFormatedBusinessId(bizId,Channels.BIZID_OUTPUT.NUM);
                    LOB = Channels.getFormatedBusinessId(bizId,Channels.BIZID_OUTPUT.LONGSTR);
                }
                // HRM-10450 Biz Id Change End
            }
            permitLst = [Select Id, Permit_Type__c, Zip_Code__c, Payee_Name__c, MMB_Bill_Code__c, Permit_Amount__c from Permit__c where Zip_Code__c =: a.AddressID__r.PostalCode__c AND Line_of_Business__c =: LOB];
        }

        if (permitLst.size() > 0){
            List<permitData> permitDataLst = new List<permitData>();
            for(permit__c p : permitLst){
                if (p.Payee_Name__c == 'ADT'){
                    permitData permit = new permitData();
                    permit.PermitType = p.Permit_Type__c;
                    permit.MMBBillCode = p.MMB_Bill_Code__c;
                    permit.PermitAmount = String.valueOf(p.Permit_Amount__c);
                    permitDataLst.add(permit);
                }
            }
            mainData.permits = permitDataLst;
        }
        
        // HRM - 9288 Simple Sell
        simpleSellData simpleSellWrapper = new simpleSellData();
        // Based on the allowed ordertype for simple sell & the user configuration check for allowed simple sell
        Boolean isSimpleSellAllowed = Utilities.checkForSimpleSell(a,loggedInUser);
        if(isSimpleSellAllowed){
            // Populate the 
            simpleSellWrapper.allowed = 'Y';
            //Simple Sell Product catalog
            list<SimpleSellProducts> simpleSellProductList = new list<SimpleSellProducts>();
            if(a.id != null){
                for(ProductCatalog__c pCats : [SELECT id,ProductName__c, Product_Quantity__c, Product_Notes__c FROM ProductCatalog__c WHERE ProductAccount__c =: a.id]){
                    SimpleSellProducts simpleSellObj = new SimpleSellProducts();
                    simpleSellObj.pName = String.isNotBlank(pCats.ProductName__c)? pCats.ProductName__c.replaceAll('\\s+','_') : '';
                    simpleSellObj.pQuantity = String.isNotBlank(pCats.Product_Quantity__c)? pCats.Product_Quantity__c : '';
                    simpleSellWrapper.SimpleSellProductNotes = String.isNotBlank(pCats.Product_Notes__c)? pCats.Product_Notes__c.replaceAll('\\s+','_') : '';
                    simpleSellProductList.add(simpleSellObj);
                }
                if(simpleSellProductList.size() > 0){                 
                   simpleSellWrapper.SimpleSellProductDetails = simpleSellProductList; 
                }
            }
        }
        else{
            simpleSellWrapper.allowed = 'N';
        }
        mainData.simpleSell = simpleSellWrapper;
        
        // Trip Fee Details
        if (fee != null && BusinessID == '1100'){
            tripFeeData tripFee = new tripFeeData();
            if (fee.Maximum_Limit__c != null){
                tripFee.MaxLimit = String.valueOf(fee.Maximum_Limit__c.setScale(2));
            }
            if (fee.Minimum_Limit__c != null){
                tripFee.MinLimit = String.valueOf(fee.Minimum_Limit__c.setScale(2));
            }
            tripFee.FeeType = fee.Fee_Type__c;
            mainData.ADTFee = tripFee;
        }
        
        // Pulse enabled (disabled by default)
        mainData.ADTP2Enabled = false;
        if(String.isNotBlank( a.ResaleTownNumber__c ) && String.isNotBlank( BusinessID ) ){
            // Lookup this town and find if it is pulse enabled
            String key = a.ResaleTownNumber__c + '-' + BusinessID;
            for(Town__c tObj: [SELECT Name, P2_Enabled__c FROM Town__c WHERE P2_Enabled__c = true AND Town_ID__c = :Key] ){          
                // We found at least one town matching this criteria
                mainData.ADTP2Enabled = true;
                break;
            }
        }
        
        // Credit Score
        if(String.isNotBlank(iSource.equifaxApprovalType) && String.isNotBlank(iSource.equifaxRiskGrade)){
            a.EquifaxApprovalType__c = iSource.equifaxApprovalType;
            a.EquifaxRiskGrade__c = iSource.equifaxRiskGrade;
        }
        //HRM-10881 Flex Fi Flag 
         if((FlexFiConfig__c.getinstance('CFG Disabled') != null && String.isNotBlank(FlexFiConfig__c.getinstance('CFG Disabled').Value__c)) && FlexFiConfig__c.getinstance('CFG Disabled').value__c.equalsIgnoreCase('false')){       
            if(String.isNotBlank(a.Business_Id__c) && a.Business_Id__c.contains('1100')){
                EquifaxData ccInf = new EquifaxData();
                
                if(Utilities.checkIfCreditCheckIsObsolete(a)){
                    // HRM-6905 - Assigning Defaults if credit is expired
                    Account tempAccount = Utilities.setCreditDefaults(a);
                    ccInf.RiskGrade = tempAccount.EquifaxRiskGrade__c;
                    ccInf.ApprovalType = tempAccount.EquifaxApprovalType__c;
                }else{
                    //Added by Jitendra
                    //Check if Profile, Order type is not blank and Not ADT Employee
                    if(String.isNotBlank(a.Profile_RentOwn__c) && String.isNotBlank(a.MMBOrderType__c) && !a.ADTEmployee__c){
                        String allowedOrderTypeForCreditCheckForOwners = Equifax__c.getinstance('Order Types For Credit Check') != null? Equifax__c.getinstance('Order Types For Credit Check').value__c:'';
                        String allowedOrderTypeForCreditCheckForRenters = Equifax__c.getinstance('Order Types For Credit Check - Renters') != null? Equifax__c.getinstance('Order Types For Credit Check - Renters').value__c:'';
                        String flexFiRiskGrades = Equifax__c.getinstance('FlexFiApprovalRiskGrade') != null? Equifax__c.getinstance('FlexFiApprovalRiskGrade').value__c : '';
                        // Check for allowed order types for Owners & Renters, should not be in FlexFiRiskApprovedGrade
                        if(String.isNotBlank(a.EquifaxRiskGrade__c) && String.isNotBlank(flexFiRiskGrades) && !flexFiRiskGrades.contains(a.EquifaxRiskGrade__c) &&
                            Equifax__c.getinstance('Default Approved Condition Code') != Null &&
                            Equifax__c.getinstance('Default Approved Risk Grade') != Null &&
                            ((String.isNotBlank(allowedOrderTypeForCreditCheckForOwners) && a.Profile_RentOwn__c == 'O' && !allowedOrderTypeForCreditCheckForOwners.contains(a.MMBOrderType__c)) ||
                            (String.isNotBlank(allowedOrderTypeForCreditCheckForRenters) && a.Profile_RentOwn__c == 'R' && !allowedOrderTypeForCreditCheckForRenters.contains(a.MMBOrderType__c)))
                        ){
                            // Credit Check defaults in case of Q APPROV
                            ccInf.ApprovalType = Equifax__c.getinstance('Default Approved Condition Code').value__c != null ? Equifax__c.getinstance('Default Approved Condition Code').value__c:'';
                            ccInf.RiskGrade = Equifax__c.getinstance('Default Approved Risk Grade').value__c  != null ? Equifax__c.getinstance('Default Approved Risk Grade').value__c:'';
                        }
                    }
                    //Not a valid combination of Order type for Owners and renters.
                    if(String.isBlank(ccInf.RiskGrade) && String.isBlank(ccInf.ApprovalType)){
                        // Credit Check fields from account
                        ccInf.RiskGrade = String.isNotBlank(a.EquifaxRiskGrade__c) ? a.EquifaxRiskGrade__c:'';
                        ccInf.ApprovalType = String.isNotBlank(a.EquifaxApprovalType__c) ? a.EquifaxApprovalType__c:'';
                    }
                    //Added by Jitendra
                }
                
                if(String.isNotBlank(ccInf.ApprovalType) && Equifax_Conditions__c.getValues(ccInf.ApprovalType) != Null){
                    ccInf.PaymentFreq = Equifax_Conditions__c.getValues(ccInf.ApprovalType).Payment_Frequency__c;
                    ccInf.DepositPercent = String.ValueOf(Equifax_Conditions__c.getValues(ccInf.ApprovalType).Deposit_Required_Percentage__c);
                    ccInf.ThreePay = (Equifax_Conditions__c.getValues(ccInf.ApprovalType).Three_Pay_Allowed__c)?'Y':'N';   
                    ccInf.EasyPay = (Equifax_Conditions__c.getValues(ccInf.ApprovalType).EasyPay_Required__c)?'Y':'N';
                }
                if(a.ADTEmployee__c){
                    ccInf.EasyPay = 'Y';
                }
                if(String.isNotBlank(ccInf.RiskGrade) && String.isNotBlank(ccInf.ApprovalType)){
                    mainData.CreditScore = ccInf;
                }
            }
        }
        
        //HRM-10881 added flex fi flag
         if((FlexFiConfig__c.getinstance('CFG Disabled') != null && String.isNotBlank(FlexFiConfig__c.getinstance('CFG Disabled').Value__c)) && FlexFiConfig__c.getinstance('CFG Disabled').value__c.equalsIgnoreCase('false')){
            //HRM-8738 - Loan Application details PayName,PayCustRef,PayType,PayLast4,PayExpiry,ThirdPartyCustNbr
            list<LoanApplication__c> lstLoanApplication = new list<LoanApplication__c>();
            lstLoanApplication = [Select id,Name,CreditCardName__c,Account__c,Account__r.Equifax_Last_Check_DateTime__c,Account__r.EquifaxRiskGrade__c,CreditCardExpirationDate__c,DecisionStatus__c,CreditCardNumber__c,CardType__c,PaymentechProfileId__c,SubmittedDateTime__c from LoanApplication__c where Account__c=:a.Id and ActiveLoanApplication__c = true limit 1];
            if(lstLoanApplication != null && lstLoanApplication.size()>0){
                if(String.isNotBlank(lstLoanApplication[0].CreditCardName__c) || String.isNotBlank(lstLoanApplication[0].CreditCardExpirationDate__c) || String.isNotBlank(lstLoanApplication[0].CreditCardNumber__c) || String.isNotBlank(lstLoanApplication[0].CardType__c)){
                    LoanApplicationDetails details = new LoanApplicationDetails();
                    if(String.isNotBlank(lstLoanApplication[0].CreditCardName__c)){
                         details.PayName      = (lstLoanApplication[0].CreditCardName__c).replace(' ','_');
                    }
                    if(String.isNotBlank(lstLoanApplication[0].PaymentechProfileId__c)){
                        details.PayCustRef      = lstLoanApplication[0].PaymentechProfileId__c;
                    }
                    if(String.isNotBlank(lstLoanApplication[0].CardType__c)){
                        details.PayType         = lstLoanApplication[0].CardType__c;
                    }
                    if(String.isNotBlank(lstLoanApplication[0].CreditCardNumber__c)){
                        details.PayLast4        = lstLoanApplication[0].CreditCardNumber__c.right(4);
                    }
                    if(String.isNotBlank(lstLoanApplication[0].CreditCardExpirationDate__c)){
                        details.PayExpiry       = lstLoanApplication[0].CreditCardExpirationDate__c;
                    }
                    
                    String Eligibleriskgrade = Equifax__c.getinstance('FlexFiApprovalRiskGrade') != null ? Equifax__c.getinstance('FlexFiApprovalRiskGrade').value__c: '';
                    //Active credit check,Active loaAplication with Approved status
                    if(!Utilities.checkIfCreditCheckIsObsolete(a) && lstLoanApplication[0].SubmittedDateTime__c !=null && String.isNotBlank(Eligibleriskgrade) && Eligibleriskgrade.contains(lstLoanApplication[0].Account__r.EquifaxRiskGrade__c)){                  
                        if(lstLoanApplication[0].DecisionStatus__c == 'APPROVED' && FlexFiConfig__c.getinstance('ThirdPartyCustomerNumber') != null && String.isNotBlank(FlexFiConfig__c.getinstance('ThirdPartyCustomerNumber').Value__c)){            
                            details.ThirdPartyCustNbr = FlexFiConfig__c.getinstance('ThirdPartyCustomerNumber').Value__c;
                        }
                        details.Finance = 'Y';
                        //active credit check ,Active loan Application but Decision declined  
                    }
                    mainData.loanApplicationDetails = details;
                }
            }
         }   
        
        //HRM 6109 - Added by Srini - Added parameter for APM
        mainData.APM = Utilities.checkForAlternatePricingModel(a,'')?'Y':'N';//HRM-10881 Empty string for order type determination in APM.
        String jsonString = '';
        if (mainData != null){
            jsonString = convertDataToJSON(mainData).deleteWhitespace();
        }
        return stripJsonNulls(jsonString);
    }
    
    private static string convertDataToJSON(CPQData data){
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();  
            //Write APM Check APM logic - Added by Srini - 6109
            if(data.APM != null){
                gen.writeFieldName('APM');
                gen.writeObject(data.APM); 
            }
            // Pulse enabled
            if(data.ADTP2Enabled != null){
                if(data.ADTP2Enabled){
                    gen.writeStringField('ADTP2Enabled', 'Y');             
                }
                else{
                    gen.writeStringField('ADTP2Enabled', 'N');
                }
            }
            
            //Proposal Only State    
            gen.writeFieldName('LicenseRequired');
            gen.writeObject(data.LicenseRequired);
            //ADT Employee
            gen.writeFieldName('ADTEmployee');
            gen.writeObject(data.ADTEmployee);
            //CLV
            if(data.CLV != null){
                gen.writeFieldName('CLVScore');
                gen.writeObject(data.CLV);
            }
            //Chrun
            if(data.Churn != null){
                gen.writeFieldName('ChurnRisk');
                gen.writeObject(data.Churn);
            }
            //HRM-7386 - VIP Deposit Waiver - Mounika
            gen.writeFieldName('VIPDepositWaiver');
            gen.writeObject(data.vipDepositWaiver);
            
             //HRM-8401---Siju 
            gen.writeFieldName('TownName');
            gen.writeObject(data.townName); 
            
            //HRM 6915 - Address Line 3 - TCS
            if(data.SiteStreet3 != null){
                gen.writeFieldName('SiteStreet3');
                gen.writeObject(data.SiteStreet3);
            }
            // Customer Billing Information
            if (data.billingInfoData != Null){
                gen.writeFieldName('CustomerBillingInfo');
                gen.writeObject(data.billingInfoData);
            }
            // System Information
            if (data.systemInfoData != Null){
                gen.writeFieldName('SystemInfo');
                gen.writeObject(data.systemInfoData);
            }
            // Current Logged in User Details
            if (data.user != null){
                gen.writeFieldName('User');
                gen.writeObject(data.user);
            }
            //HRM-10745
            if(data.Tenure != null){
                gen.writeFieldName('Tenure');
                gen.writeObject(data.Tenure);
            }
            // Trip Fee
            if(data.ADTFee != null){
                gen.writeFieldName('ADTFee');
                gen.writeObject(data.ADTFee);
            }
            // Permit Details
            if(data.permits != null){
                if (data.permits.size() > 0){
                    gen.writeFieldName('Permits');
                    gen.writeStartArray();
                    for (permitData pd: data.permits){
                        gen.writeObject(pd);    
                    }
                    gen.writeEndArray();
                }                
            }
            //HRM-9288 Simple Sell Data
            if(data.simpleSell != null){
                gen.writeFieldName('SimpleSell');
                gen.writeObject(data.simpleSell);
            }
            
            //HRM-8738 Loan application details
            if(data.LoanApplicationDetails != null){
                gen.writeFieldName('LoanApplicationDetails');
                gen.writeObject(data.LoanApplicationDetails);
            }
 
            // Equifax Data
            if(data.CreditScore != null){
                gen.writeFieldName('EquifaxData');
                gen.writeObject(data.CreditScore);    
            } 
        gen.writeEndObject();        
        return gen.getAsString();
    }
    
    // Removes all nulls from JSON
    public static string stripJsonNulls(string JsonString) { 
        if(JsonString != null) { 
            JsonString = JsonString.replaceAll('\"[^\"]*\":null',''); //basic removeal of null values 
            JsonString = JsonString.replaceAll(',{2,}', ','); //remove duplicate/multiple commas 
            JsonString = JsonString.replace('{,', '{'); //prevent opening brace from having a comma after it 
            JsonString = JsonString.replace(',}', '}'); //prevent closing brace from having a comma before it 
            JsonString = JsonString.replace('[,', '['); //prevent opening bracket from having a comma after it 
            JsonString = JsonString.replace(',]', ']'); //prevent closing bracket from having a comma before it 
        } 
        return JsonString; 
    }
}