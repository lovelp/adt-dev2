/************************************* MODIFICATION LOG ********************************************************************************************
* NSCEntryController
*
* DESCRIPTION : CTI Agent connection class to Salesforce NSC Calldata
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE        Ticket       REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera     02/15/2012                - Original Version
* Mounika Anna        06/12/2018  HRM-7272     - Made modifications to remove the check for the DNIS Start Date and End Date  
* EJ Bantz            10/22/2019  HRM-11053    - Home health
*/

public without sharing class NSCEntryController {
    
    private Map<String, String> NSCLeadRedirectMap;
    @TestVisible private Boolean isTabActive;
    private static RecordType rt;
    private Call_Data__c cData; 
    private DNIS__c DNISobj;
    public Boolean isError {get;set;}
    public boolean enableContinueBtn {get;set;}
    public String errMsg {get;set;}
    public String DNIS {get;set;}
    public String ANI{get;set;}
    public Boolean isInConsole{get;set;}
    public String selectedCampaign{get;set;}
    public boolean isInboundcampaign{get;set;} 
    public Boolean section1{get;set;}
    public Boolean section2{get;set;}
    public List<Account> acc {get;set;}
    public String TelemarNumber{get;set;}   

    public Boolean isOutboundUser{
        get{
            User u = [select profile.name from User where id =: Userinfo.getUserid()];
            if(u.Profile.name.contains('Outbound')){
                return true;
            }
            else
                return false;     
        }
    }
    public String announcementTitle {
        get{
            return 'As of '+ DateTime.now().format();
        }
    }
    
    public String dnisID {
        get {
            if( DNISobj != null ){
                return DNISobj.Id;
            }
            return '';
        }
    }
    public String rtID {
        get {
            return rt.Id;
        }
    } 
    public String CallDataID {
        get {
            return cData.ID;
        }
    }
    
    public String CampaignID{get;set;}
    
    static {
        rt = Utilities.getRecordType(RecordTypeName.USER_ENTERED_LEAD, 'Lead');
    }
    
    public Boolean callServiceConsoleOverride {
        get{
            return isTabActive;
        }
    }
    
    public String selectedChannel{get;set;}
    
    
    //list of active campaigns-- Mounika anna
    public List<SelectOption> getCampaigns(){
        List<SelectOption> options = new List<SelectOption>();
        List<Campaign> campls = [Select name,DNIS__c from Campaign where IsActive = true AND Type!='Job'];
        options.add(new SelectOption('Outbound','Outbound'));
        options.add(new SelectOption('Inbound','Inbound'));
       /* for(Campaign c: campls){
            options.add(new SelectOption(c.name,c.name));

        }*/
        return options;
    }
    public void updateData() {

       //if selected campaign is Inbound 

         if(selectedCampaign == 'Inbound'){
                        section1 = true;
                        isInboundcampaign = true;
                        section2 = false;
                       
         }//if the selected campaign is outbound


         else if(selectedCampaign != 'Inbound') {
                        section1 = false;
                        section2 = true;   
        }
    }
        
    public NSCEntryController(){
        isTabActive = false;
        isError = false;
        isInConsole = false;
        if(isOutboundUser)
            section2 = true;
        else
            section1 = true;        
        isInboundcampaign = false;
        enableContinueBtn = true;
        NSCLeadRedirectMap = new Map<String, String>();

        errMsg = '';
        NSCLeadRedirectMap.putAll(ApexPages.currentPage().getParameters());
        if( !NSCLeadRedirectMap.containsKey('RecordType') ){
            NSCLeadRedirectMap.put('ent','Lead');
            NSCLeadRedirectMap.put('isdtp','vw');
            NSCLeadRedirectMap.put('isWsVw','true');
            NSCLeadRedirectMap.put('RecordType', rt.Id);
            NSCLeadRedirectMap.put('save_new','1');
            NSCLeadRedirectMap.put('scontrolCaching','1');
            NSCLeadRedirectMap.put('sfdc.override','1');
            //NSCLeadRedirectMap.put('nooverride', '1'); 
            //NSCLeadRedirectMap.put('ekp', '00Q'); 
            NSCLeadRedirectMap.put('save_new_url', '/00Q/e');           
            NSCLeadRedirectMap.put('retURL', '/00Q/o');
            isTabActive = true;
        }
        if( NSCLeadRedirectMap.containsKey('cdId') ){
            cData = [SELECT DNIS__c, Dial__c, Received_By__c FROM Call_Data__c WHERE Id = :NSCLeadRedirectMap.get('cdId') ];
            if( !Utilities.isEmptyOrNull(cData.DNIS__c) ){
                DNISobj = [SELECT Start_Date__c, End_Date__c FROM DNIS__c WHERE Id = :cData.DNIS__c];
            }
        }
        else {
            String cDataName = Userinfo.getName() + '_' + Datetime.now().format();
            cData = new Call_Data__c( Name = cDataName);
        }
        cData.Received_By__c = UserInfo.getUserId();
    }
    
    public PageReference verifyDNIS(){
        try{
            String regexNumeric = '[^a-zA-Z0-9]';
            String srchStr ;
            system.debug('Telemar number'+TelemarNumber);
            if(TelemarNumber!=null && TelemarNumber!= ''){
                system.debug('Telemar number'+TelemarNumber);
                cData.Telemar_Number__c = TelemarNumber;
                acc = new List<Account>([select id,Phone,TelemarAccountNumber__c,Last_Master_Campaign__c from Account where TelemarAccountNumber__c =: telemarnumber limit 1]);
                if(!acc.isEmpty() && acc[0].TelemarAccountNumber__c == TelemarNumber){
                    system.debug('acc[0].TelemarAccountNumber__c '+acc[0].TelemarAccountNumber__c );
                    system.debug('ANI'+ANI);
                    ANI = acc[0].Phone;
                    cData.Account__c =acc[0].id;
                    if(acc[0].Last_Master_Campaign__c!=null){
                        cData.Campaign_Type__c  =acc[0].Last_Master_Campaign__c;
                        DNIS = [Select DNIS__c,DNIS__r.Name from Campaign where id =: acc[0].Last_Master_Campaign__c limit 1].DNIS__r.Name;
                    }
                }else{
                    iserror = true;
                    system.debug('checking teh error is entered');
                    errmsg = 'No account exists with given telemar number';
                }                    
            }

            if(selectedCampaign != null && selectedCampaign != 'Inbound'){
                   cData.Contact_Type__c = 'Outbound';
            } else if(selectedCampaign != null && selectedCampaign == 'Inbound'){
                    cData.Contact_Type__c ='Inbound';
            }
            
            System.debug('SelectedCampaign'+selectedCampaign);
            system.debug('Dnis'+DNIS);
            if(DNIS!=null){
                srchStr = DNIS.replaceAll(regexNumeric, '').trim();
                Date tDate = Date.today();
                cData.Dial__c = srchStr;
                System.debug('search string'+srchStr);
                if(ANI!=null)
                    cData.ani__c=ANI;
                system.debug('selected channel is '+selectedChannel);
                system.debug('Dnis'+DNIS);
                //HRM-7272 -- Mounika Anna -- Removed the start and end date check for the Query 
                List<DNIS__c> dnisList =[SELECT id,Start_Date__c, End_Date__c FROM DNIS__c WHERE Name = :srchStr AND lineofbusiness__c=:selectedchannel ];
                    
                if( dnisList!=null && !dnisList.isEmpty() ){
                    // DNIS found populate required info if any
                    DNISobj = dnisList[0];                
                    cData.DNIS__c = DNISobj.Id;  

                    System.debug('Dnis list is not null');
                }
                else {
                    if(selectedchannel==null || selectedchannel==''){
                      //HRM-7272 -- Mounika Anna -- Removed the start and end date check for the Query 
                        dnisList=[SELECT id,Start_Date__c, End_Date__c FROM DNIS__c WHERE Name = :srchStr order by lineofbusiness__c];
                        System.debug('Dnislist'+dnisList);
                    }
                    if(dnisList!=null && !dnisList.isEmpty()){
                        DNISobj = dnisList[0];                
                        cData.DNIS__c = DNISobj.Id;
                    }else if(!isOutboundUser) {
                        isError = true;
                        errMsg = 'The TFN supplied does not match any active DNIS configured in the system.  To continue without a DNIS selected, click Continue.  Otherwise click Cancel.';   
                    }
                }
            }
            else if(isOutboundUser){
                    if(ANI != null)
                        cData.ani__c = ANI;                   
            }

            system.debug('enableContinueBtn:'+enableContinueBtn);
            upsert cData; 
        } 
        catch (Exception err){
            isError = true;
            errMsg = err.getMessage();
        }
        return null;
    } 
    
    public PageReference startScript(){
        PageReference createLeadPage;
        if( !isTabActive ){
            createLeadPage = new PageReference('/apex/NSCLeadCapture');
            createLeadPage.getParameters().putAll(NSCLeadRedirectMap); 
            createLeadPage.getParameters().put('dnis',DNIS);
            createLeadPage.getParameters().put('dnisID',dnisID);
            createLeadPage.getParameters().put('cdId',cData.Id);   
            return createLeadPage;
        }
        return null;
    }
    
    public PageReference rerenderCallInfo(){
        isInConsole = true;
        return null;
    }
    
    public List<SelectOption> getChannelOptions(){
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        options.add(new SelectOption('Residential','Residential'));
        options.add(new SelectOption('Small Business','Small Business'));
        options.add(new SelectOption('Home Health','Home Health')); // HRM-11053
        return options;
    }
    
}