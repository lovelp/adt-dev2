@isTest
private class SearchManagerTest {
    
    private static String VALID_ID = '001T000000qShRf';
   
    static testMethod void testFindActiveAccounts() {
        
        setup();
        // create AccountSearchParameters with all search criteria except the set of Account IDs
        // corresponds to an initial user search with all search criteria
        AccountSearchParameters p1 = buildParameters();
        
        User salesRep = TestHelperClass.createSalesRepUser();
        
        // create an Account
        Account a;
        System.runAs(salesRep) {
            a = TestHelperClass.createAccountData();
        }   
        // create a second AccountSearchParameters
        AccountSearchParameters p2 = buildParameters();
        // then add a set containing that Account's ID
        // corresponds to a "Add Leads" user search with all search criteria
        Set<String> s1 = new Set<String>();
        s1.add(a.Id);
        p2.accountIdSet = s1;
        
        Set<String> s0 = new Set<String>();
        
        Set<String> s2 = new Set<String>();
        s2.addAll(s1);
        s2.add(VALID_ID);
        
        Test.startTest();
        // execute with first set of parameters
        try {
            SearchManager.findActiveAccounts(p1);
        } catch (DmlException dml) {
            // an exception likely means malformed SOQL
            System.assert(false, 'This exception likely means a malformed SOQL statement when simulating an inital user search (null ID set): ' + dml.getMessage() + ' ' + dml.getStackTraceString());
        } catch (Exception e) {
            // some other exception
            System.assert(false, 'This exception occurred when simulating an inital user search (null ID set): ' + e.getMessage() + ' ' + e.getStackTraceString());
        }
        
        // switch from a null set to empty set
        p1.accountIdSet = s0;
        // execute again with first set of parameters
        try {
            SearchManager.findActiveAccounts(p1);
        } catch (DmlException dml) {
            // an exception likely means malformed SOQL
            System.assert(false, 'This exception likely means a malformed SOQL statement when simulating an inital user search (empty ID set): ' + dml.getMessage() + ' ' + dml.getStackTraceString());
        } catch (Exception e) {
            // some other exception
            System.assert(false, 'This exception occurred when simulating an inital user search: ' + e.getMessage() + ' ' + e.getStackTraceString());
        }
        
        // execute with second set of parameters
        try {
            SearchManager.findActiveAccounts(p2);
        } catch (DmlException dml) {
            // an exception likely means malformed SOQL
            System.assert(false, 'This exception likely means a malformed SOQL statement when simulating a *Add Leads* user search (1 ID set): ' + dml.getMessage() + ' ' + dml.getStackTraceString());
        } catch (Exception e) {
            // some other exception
            System.assert(false, 'This exception occurred when simulating a *Add Leads* user search (1 ID set): ' + e.getMessage() + ' ' + e.getStackTraceString());
        }
        
        // switch from set with 1 account to set with 2 accounts
        p2.accountIdSet = s2;
        
        // execute again with second set of parameters
        try {
            SearchManager.findActiveAccounts(p2);
        } catch (DmlException dml) {
            // an exception likely means malformed SOQL
            System.assert(false, 'This exception likely means a malformed SOQL statement when simulating a *Add Leads* user search (2 ID set): ' + dml.getMessage() + ' ' + dml.getStackTraceString());
        } catch (Exception e) {
            // some other exception
            System.assert(false, 'This exception occurred when simulating a *Add Leads* user search (2 ID set): ' + e.getMessage() + ' ' + e.getStackTraceString());
        }
        
        
        
        //TODO
        // test all possible combinations of search criteria to ensure no combination results in malformed SOQL
        
        //TODO
        // test that data matching search criteria is returned
        
        
        Test.stopTest();    
        
    }
    
    static testMethod void testGetAccounts() {
        
        setup();
        User salesRep = TestHelperClass.createSalesRepUser();
        
        // set with no Ids
        Set<String> s0 = new Set<String>();
        
        // create an Account
        Account a; 
        System.runAs(salesRep) {
            a = TestHelperClass.createAccountData();
        }
        
        // set with 1 matching Id
        Set<String> s1 = new Set<String>();
        s1.add(a.Id);
        
        // set with 2 Ids
        Set<String> s2 = new Set<String>();
        s2.addAll(s1);
        s2.add(VALID_ID);
        
        Test.startTest();
        System.runAs(salesRep) {
            try {
                List<SearchItem> siList = SearchManager.getAccounts(s0);
            } catch (DmlException dml) {
                // an exception likely means malformed SOQL
                System.assert(false, 'This exception likely means a malformed SOQL statement with empty set: ' + dml.getMessage() + ' ' + dml.getStackTraceString());
            } catch (Exception e) {
                // some other exception
                System.assert(false, 'This exception occurred with empty set: ' + e.getMessage() + ' ' + e.getStackTraceString());
            }
        
            try {
                List<SearchItem> siList = SearchManager.getAccounts(s1);
                System.assertEquals(1, siList.size(), 'Expect to retrieve one account');
            } catch (DmlException dml) {
                // an exception likely means malformed SOQL
                System.assert(false, 'This exception likely means a malformed SOQL statement with set with 1 Id: ' + dml.getMessage() + ' ' + dml.getStackTraceString());
            } catch (Exception e) {
                // some other exception
                System.assert(false, 'This exception occurred with set with 1 Id: ' + e.getMessage() + ' ' + e.getStackTraceString());
            }
        
            try {
                List<SearchItem> siList = SearchManager.getAccounts(s2);
                System.assertEquals(1, siList.size(), 'Expect to retrieve one account');
            } catch (DmlException dml) {
                // an exception likely means malformed SOQL
                System.assert(false, 'This exception likely means a malformed SOQL statement with set with 2 Ids: ' + dml.getMessage() + ' ' + dml.getStackTraceString());
            } catch (Exception e) {
                // some other exception
                System.assert(false, 'This exception occurred with set with 2 Ids: ' + e.getMessage() + ' ' + e.getStackTraceString());
            }
        
            
        }
        
        Test.stopTest();
        
    }
    
    static testMethod void testFindStreetSheetItems() {
    
        setup();
        Account a = TestHelperClass.createAccountData();
        
        StreetSheet__c ss = new StreetSheet__c();
        ss.Name = 'Unit Test Street Sheet';
        
        insert ss;
        
        StreetSheetItem__c ssi1 = new StreetSheetItem__c();
        ssi1.AccountID__c = a.Id;
        ssi1.StreetSheet__c = ss.Id;
        
        insert ssi1;
        
        StreetSheetItem__c ssi2 = new StreetSheetItem__c();
        ssi2.AccountID__c = a.Id;
        ssi2.StreetSheet__c = ss.Id;
        
        insert ssi2; 
        
        Test.startTest();
        
        try {
            List<SearchItem> siList = SearchManager.findStreetSheetItems(ss.Id);
            
            System.assert(siList != null, 'Expect a non-null list');
            System.assertEquals(2, siList.size(), 'Expect two items in the list');
            
        } catch (DmlException dml) {
            // an exception likely means malformed SOQL
            System.assert(false, 'This exception likely means a malformed SOQL statement: ' + dml.getMessage() + ' ' + dml.getStackTraceString());
        } catch (Exception e) {
            // some other exception
            System.assert(false, 'This exception occurred the valid Id: ' + e.getMessage() + ' ' + e.getStackTraceString());
        }
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testFindStreetSheetItemsForMapping() {
    
        setup();
        Account a = TestHelperClass.createAccountData();
        
        StreetSheet__c ss = new StreetSheet__c();
        ss.Name = 'Unit Test Street Sheet';
        
        insert ss;
        
        StreetSheetItem__c ssi1 = new StreetSheetItem__c();
        ssi1.AccountID__c = a.Id;
        ssi1.StreetSheet__c = ss.Id;
        
        insert ssi1;
        
        StreetSheetItem__c ssi2 = new StreetSheetItem__c();
        ssi2.AccountID__c = a.Id;
        ssi2.StreetSheet__c = ss.Id;
        
        insert ssi2; 
        
        Test.startTest();
        
        try {
            List<SearchItem> siList = SearchManager.findStreetSheetItems(ss.Id, 'SortCode__c');
            
            System.assert(siList != null, 'Expect a non-null list');
            System.assertEquals(2, siList.size(), 'Expect two items in the list');
            
        } catch (DmlException dml) {
            // an exception likely means malformed SOQL
            System.assert(false, 'This exception likely means a malformed SOQL statement: ' + dml.getMessage() + ' ' + dml.getStackTraceString());
        } catch (Exception e) {
            // some other exception
            System.assert(false, 'This exception occurred the valid Id: ' + e.getMessage() + ' ' + e.getStackTraceString());
        }
        
        Test.stopTest();
        
        
    }
    
    private static AccountSearchParameters buildParameters() {
        
        AccountSearchParameters parameters = new AccountSearchParameters();
        
        String[] DATASOURCE = new String[]{'ADMIN','Broadview'};
        parameters.dataSource = DATASOURCE;
        String[] LEADTYPE = new String[]{'Discontinuance', 'New Mover'};
        parameters.leadType = LEADTYPE;
        parameters.newMoverIndicator = true;
        parameters.nonWorkedLeadIndicator = true; 
        String[] DISCOREASON = new String[]{'Disco', 'Reason'};      
        parameters.discoReason = DISCOREASON;
        parameters.dateAssignedAfter = Datetime.now();
        parameters.dateAssignedBefore = Datetime.now();
        parameters.phoneAvailableIndicator = true;
        parameters.siteStreet = '8952 Brook Rd';
        parameters.siteCity = 'McLean';
        String[] STATE = new String[]{'DC', 'VA', 'MD'};
        parameters.siteState = STATE;
        parameters.sitePostalCode = '221o2';
        parameters.discoDateAfter = Date.today();
        parameters.discoDateBefore = Date.today();
        parameters.activeSiteIndicator = true;
        parameters.queryRowLimit = 100;
        parameters.accountIdSet = null;
        
        //phase 2
        parameters.manuallyAssigned = false;
        parameters.phoneSale = false;
        
        return parameters;
        
    }
    
    static testMethod void testGetAccount() {
        
        setup();
        Account inputAcct = TestHelperClass.createAccountData();
        
        Test.startTest();
        
        Account retrievedAcct = SearchManager.getAccount(inputAcct.Id);
        
        System.assert(retrievedAcct != null);
        System.assertEquals(inputAcct.Id, retrievedAcct.Id);
        System.assertEquals(inputAcct.Name, retrievedAcct.Name);
        //System.assertEquals(38.94686000000000, retrievedAcct.Latitude__c);
        //System.assertEquals(-77.25470100000000, retrievedAcct.Longitude__c);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testGetAccountByTelemarAccountNumberMatchTelemar() {
        
        setup();
        Account inputAcct = TestHelperClass.createAccountData();
        
        inputAcct.TelemarAccountNumber__c = '1234567890';
        update inputAcct; 
        
        Test.startTest();
        
        Account retrievedAcct = SearchManager.getAccountByTelemarAccountNumber('1234567890');
        
        System.assert(retrievedAcct != null);
        System.assertEquals(inputAcct.Id, retrievedAcct.Id);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testGetAccountByTelemarAccountNumberNoMatch() {
        
        setup();
        Account inputAcct = TestHelperClass.createAccountData();
        
        Test.startTest();
        
        Account retrievedAcct = SearchManager.getAccountByTelemarAccountNumber('1234567890');
        
        System.assert(retrievedAcct == null);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testGetAccountByTelemarAccountNumberNull() {
        
        setup();
        Account inputAcct = TestHelperClass.createAccountData();
        
        Test.startTest();
        
        Account retrievedAcct = SearchManager.getAccountByTelemarAccountNumber(null);
        
        System.assert(retrievedAcct == null);
        
        Test.stopTest();
        
        
    }

/* Disabling this test in association with a change to the implementation of the method
   such that there is no search against RifTelemarAccountNumber__c
       
    static testMethod void testGetAccountByTelemarAccountNumberMatchRifOnly() {
        
        setup();
        Account inputAcct = TestHelperClass.createAccountData();
        
        inputAcct.RifTelemarAccountNumber__c = '1234567890';
        update inputAcct; 
        
        Test.startTest();
        
        Account retrievedAcct = SearchManager.getAccountByTelemarAccountNumber('1234567890');
        
        System.assert(retrievedAcct != null);
        System.assertEquals(inputAcct.Id, retrievedAcct.Id);
        
        Test.stopTest();
        
        
    }
 */   
 
 /* Disabling this test in association with a change to the implementation of the method
   such that there is no search against RifTelemarAccountNumber__c
      
    static testMethod void testGetAccountByTelemarAccountNumberMatchRif() {
        
        setup();
        Account inputAcct1 = TestHelperClass.createAccountData();
        
        inputAcct1.RifTelemarAccountNumber__c = '1234567890';
        update inputAcct1; 
        
        Account inputAcct2 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
        
        inputAcct2.TelemarAccountNumber__c = '1234567890';
        update inputAcct2; 
        
        Test.startTest();
        
        Account retrievedAcct = SearchManager.getAccountByTelemarAccountNumber('1234567890');
        
        System.assert(retrievedAcct != null);
        System.assertEquals(inputAcct1.Id, retrievedAcct.Id);
        
        Test.stopTest();
        
        
    }
*/    
    
    static testMethod void testGetAccountByLeadExternalIDMatch() {
        
        setup();
        Account inputAcct = TestHelperClass.createAccountData();
        
        inputAcct.LeadExternalID__c = 'TEST-1234567890';
        update inputAcct; 
        
        Test.startTest();
        
        Account retrievedAcct = SearchManager.getAccountByLeadExternalID('TEST-1234567890');
        
        System.assert(retrievedAcct != null);
        System.assertEquals(inputAcct.Id, retrievedAcct.Id);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testGetAccountByLeadExternalIDNoMatch() {
        
        setup();
        Account inputAcct = TestHelperClass.createAccountData();
        
        Test.startTest();
        
        Account retrievedAcct = SearchManager.getAccountByLeadExternalID('1234567890');
        
        System.assert(retrievedAcct == null);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testGetAccountByLeadExternalIDNull() {
        
        setup();
        Account inputAcct = TestHelperClass.createAccountData();
        
        Test.startTest();
        
        Account retrievedAcct = SearchManager.getAccountByLeadExternalID(null);
        
        System.assert(retrievedAcct == null);
        
        Test.stopTest();
        
        
    }
    //COMMENTED OUT FOR DEPLOYMENT
    static testMethod void testLeadSearch()
    {
        setup();
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        Account a;
        Lead l;
        User u;
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
        }
        system.runas(u) {   
            a = TestHelperClass.createAccountData();
            a.OwnerId = u.Id;
            update a;
            l = TestHelperClass.createLead(u, false, 'BUDCO');
			l.NewMoverType__c = 'RL';
			insert l;
        }
        
        AccountSearchParameters parameters = new AccountSearchParameters();
        
        String[] DATASOURCE = new String[]{'BUDCO'};
        parameters.dataSource = DATASOURCE;
        String[] LEADTYPE = new String[]{'Other'};
        parameters.leadType = null;
        parameters.newMoverIndicator = false;
        parameters.nonWorkedLeadIndicator = false; 
        String[] DISCOREASON = new String[]{'All'};      
        parameters.discoReason = null;
        parameters.dateAssignedAfter = null;
        parameters.dateAssignedBefore = null;
        parameters.phoneAvailableIndicator = false;
        parameters.siteStreet = '8952 Brook Rd';
        parameters.siteCity = 'McLean';
        String[] STATE = new String[]{'DC', 'VA', 'MD'};
        parameters.siteState = STATE;
        parameters.sitePostalCode = '22102';
        parameters.discoDateAfter = null;
        parameters.discoDateBefore = null;
        parameters.activeSiteIndicator = false;
        parameters.queryRowLimit = 100;
        parameters.accountIdSet = null;
        //TODO: UPDATE LEAD BEING SEARCHED TO MATCH BUILT PARAMETERS AND ASSERT
        
        
        Test.startTest();
        List<SearchItem> results;
        system.runas(current) {
            results = SearchManager.findLeads(parameters);
        }
        //System.assert(results != null, 'results list should not be null');
        //System.assertEquals(1, results.size(), 'results list should contain one item'); 
        
        Test.stopTest();
    }
    
    static testMethod void testRedistributeLists() {
        
        setup();
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        Account a;
        Lead l;
        User u;
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            l = TestHelperClass.createLead(u);
        }
        
        Integer resLimit = 10;
        Integer dist = resLimit / 2;
        List<SearchItem> accounts = new List<SearchItem>();
        List<SearchItem> leads = new List<SearchItem>();
        
        test.startTest();
        
            //test both greater than limit
            for (Integer i = 0; i < dist+2; i++) {
                accounts.add(new SearchItem(a, false));
                leads.add(new SearchItem(l, false));
            }
            
            SearchManager.redistributeResults(accounts, leads, reslimit);
            system.assertEquals(dist, accounts.size());
            system.assertequals(dist, leads.size());
            
            //test for accounts greater than res limit
            accounts.clear();
            leads.clear();
            for (Integer i = 0; i < resLimit+1; i++) {
                accounts.add(new SearchItem(a, false));
            }
            for (Integer i = 0; i < dist-1; i++) {
                leads.add(new SearchItem(l, false));
            }
            
            SearchManager.redistributeResults(accounts, leads, reslimit);
            
            system.assertEquals(dist-1, leads.size());
            system.assertEquals(resLimit - leads.size(), accounts.size());
        
            //test for leads greater than res limit
            accounts.clear();
            leads.clear();
            for (Integer i = 0; i < resLimit+1; i++) {
                leads.add(new SearchItem(a, false));
            }
            for (Integer i = 0; i < dist-1; i++) {
                accounts.add(new SearchItem(l, false));
            }
            
            SearchManager.redistributeResults(accounts, leads, reslimit);
            
            system.assertEquals(dist-1, accounts.size());
            system.assertEquals(resLimit - accounts.size(), leads.size());
            
        test.stopTest();
        
    }
    
    //COMMENTED OUT FOR DEPLOYMENT (SLOW PROCESS)
    static testMethod void testSearchCriteria() {
        
        setup();
        Account a;
        Lead l;
        Address__c addr;
        User u;
        AccountSearchParameters prm = new AccountSearchParameters();
        prm.queryRowLimit = 10;
        prm.siteCity = 'testcity9999';
        prm.sitePostalCodeAddOn = '1234';
        prm.newMoverIndicator = false;
        prm.nonWorkedLeadIndicator = false; 
        prm.phoneAvailableIndicator = false;
        prm.activeSiteIndicator = false;
        prm.manuallyAssigned = false;
        prm.dispositionCode = new String[] {'Called (Resi)', 'Called'};
        User thisUser = [Select Id from User where Id = :UserInfo.getUserId()];
        
        system.runas(thisUser) {
            u = TestHelperClass.createSalesRepUser();
        }
        system.runas(u) {   
            a = TestHelperClass.createAccountData();
            a.NewMover__c = prm.newMoverIndicator;
            a.NewLead__c = prm.nonWorkedLeadIndicator;
            a.Phone = '1234567890';
            a.InService__c = prm.activeSiteIndicator;
            a.ManuallyAssigned__c = prm.manuallyAssigned;
            a.LeadStatus__c = 'Active';
            a.OwnerId = u.Id;
            a.DispositionCode__c = prm.dispositionCode[0];
            a.DispositionDetail__c = 'Called - ADT Customer';
            a.UnassignedLead__c = false;
            update a;
            addr = [select Id,City__c, PostalCodeAddOn__c from Address__c where Id = :a.AddressID__c];
            addr.City__c = prm.siteCity;
            addr.PostalCodeAddOn__c = prm.sitePostalCodeAddOn;
            update addr;
            l = TestHelperClass.createLead(u, false);
            l.NewMover__c = prm.newMoverIndicator;
            l.NewLead__c = prm.nonWorkedLeadIndicator;
            l.Phone = '1234567890';
            l.AddressId__c = addr.Id;
            l.DispositionCode__c = prm.dispositionCode[1];
            l.DispositionDetail__c = 'Called - ADT Customer';
            insert l;
        }
        
        list<SearchItem> items;
        system.runas(u) {
            items = SearchManager.findActiveAccounts(prm);
            items.addAll( SearchManager.findLeads(prm));
        }   
        
        system.assert(items.size() > 0);
        
        Boolean foundAccount = false;
        Boolean foundLead = true;
        
        for (SearchItem si : items) {
            if (si.acct != null && si.acct.Id == a.Id) {
                foundAccount = true;
            }
            if (si.lead != null && si.lead.Id == l.Id) {
                foundLead = true;
            }
        }
        
        system.assert(foundAccount, 'Account was not returned.');
        system.assert(foundLead, 'Lead was not returned');
    }
    
    static testMethod void testGetLeadByTelemarAccountNumberMatch() {
        
        setup();
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {
            String Id = TestHelperClass.inferPostalCodeID('221o2', '1100');
        }
        User u = TestHelperClass.createSalesRepUser();
        Lead inputLead = null;
        System.runAs(u) {
            inputLead = TestHelperClass.createLead(u, false);
    
            inputLead.TelemarAccountNumber__c = '1234567890';
            insert inputLead; 
        }
        Test.startTest();
        Lead retrievedLead = null;
        System.runAs(u) {
            retrievedLead = SearchManager.getLeadByTelemarAccountNumber('1234567890');
        }
        System.assert(retrievedLead != null);
        System.assertEquals(inputLead.Id, retrievedLead.Id);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testGetLeadByTelemarAccountNumberNoMatch() {
        
        setup();
        Test.startTest();
        
        Lead retrievedLead = SearchManager.getLeadByTelemarAccountNumber('1234567890');
        
        System.assert(retrievedLead == null);
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testGetLeadByTelemarAccountNumberNull() {
        
        setup();
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {
            String Id = TestHelperClass.inferPostalCodeID('221o2', '1100');
        }
        User u = TestHelperClass.createSalesRepUser();
        Lead inputLead = null;
        System.runAs(u) {
            inputLead = TestHelperClass.createLead(u, false);
    
            inputLead.TelemarAccountNumber__c = '1234567890';
            insert inputLead; 
        }
        Test.startTest();
        Lead retrievedLead = null;
        System.runAs(u) {
            retrievedLead = SearchManager.getLeadByTelemarAccountNumber(null);
        }
        
        Test.stopTest();
        
        System.assert(retrievedLead == null);   
        
    }
    
    static testMethod void testGetAccountByBillingSystemCustId() {
        
        setup();
        User salesRep = TestHelperClass.createSalesRepUser();
        
        Account inputAcct;
        
        System.runAs(salesRep) {
            inputAcct = TestHelperClass.createAccountData();
            inputAcct.OwnerId = salesRep.Id;
            inputAcct.BillingSystem__c = '1';
            inputAcct.CustomerID__c = 'Q1234';
            update inputAcct;
        }
        
        Test.startTest();
        Account retrievedAcct;
        System.runAs(salesRep) {
            retrievedAcct = SearchManager.getAccount('1', 'Q1234');
        }
        
        Test.stopTest();
        
        System.assert(retrievedAcct != null);
        System.assertEquals(inputAcct.Id, retrievedAcct.Id);
        System.assertEquals(inputAcct.Name, retrievedAcct.Name);
        //System.assertEquals(38.94686000000000, retrievedAcct.Latitude__c);
        //System.assertEquals(-77.25470100000000, retrievedAcct.Longitude__c);
        
        
        
        
    }
    
    static testMethod void testGetAccountByBillingSystemCustIdTypeNoMatch() {
        
        setup();
        User salesRep = TestHelperClass.createSalesRepUser();
        
        Account inputAcct;
        
        Test.startTest();
        Account retrievedAcct;
        System.runAs(salesRep) {
            retrievedAcct = SearchManager.getAccount('1', 'Q1234', IntegrationConstants.TYPE_RIF);
        }
        
        Test.stopTest();
        
        System.assert(retrievedAcct == null);
        
            
    }
    
    static testMethod void testGetAccountByBillingSystemCustIdTypeNullInputs() {
        
        setup();
        User salesRep = TestHelperClass.createSalesRepUser();
        
        Account inputAcct;
        
        Test.startTest();
        Account retrievedAcct;
        System.runAs(salesRep) {
            retrievedAcct = SearchManager.getAccount(null, null, null);
        }
        
        Test.stopTest();
        
        System.assert(retrievedAcct == null);
        
            
    }
    
    static testMethod void testGetAccountByBillingSystemCustIdType() {
        
        setup();
        User admin = TestHelperClass.createAdminUser();
        User salesRep;
        System.runAs(admin) {
        	salesRep = TestHelperClass.createSalesRepUser();
        }
        Account inputAcct;
        
        System.runAs(salesRep) {
            inputAcct = TestHelperClass.createAccountData();
            inputAcct.OwnerId = salesRep.Id;
            inputAcct.BillingSystem__c = '1';
            inputAcct.CustomerID__c = 'Q1234';
            inputAcct.Type = IntegrationConstants.TYPE_RIF;
            update inputAcct;
        }
        
        Account testUpdateAcct = [select BillingSystem__c, CustomerID__c, Type from Account where Id = :inputAcct.Id];
        System.assertEquals('1', testUpdateAcct.BillingSystem__c);
        System.assertEquals('Q1234', testUpdateAcct.CustomerID__c);
        System.assertEquals(IntegrationConstants.TYPE_RIF, testUpdateAcct.Type);
        
        Test.startTest();
        Account retrievedAcct;
        //System.runAs(salesRep) {
            retrievedAcct = SearchManager.getAccount('1', 'Q1234', IntegrationConstants.TYPE_RIF);
        //}
        
        Test.stopTest();
        
        System.assert(retrievedAcct != null);
        System.assertEquals(inputAcct.Id, retrievedAcct.Id);
        System.assertEquals(inputAcct.Name, retrievedAcct.Name);
        //System.assertEquals(38.94686000000000, retrievedAcct.Latitude__c);
        //System.assertEquals(-77.25470100000000, retrievedAcct.Longitude__c);
            
    }

    private static void setup() {
        
        User current = [select Id from User where Id=:UserInfo.getUserId()];
        System.runAs(current) {        
            TestHelperClass.createReferenceDataForTestClasses();
            TestHelperClass.createReferenceUserDataForTestClasses();
        }
    }

}