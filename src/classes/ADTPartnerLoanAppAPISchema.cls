/************************************* MODIFICATION LOG ********************************************************************************************
* 
* Name: LoanApplicationAPISchema
*
* DESCRIPTION : Defines the schema of the LoanApplicationAPI web service
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                                      DATE                        Ticket              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari                              05/04/2019                  HRM-9547            Original
* Srinivas Yarramsetti                          06/06/2019                  HRM-9547            Moderator
********************************************************************************************************************************************************/
public class ADTPartnerLoanAppAPISchema {

    //Loan Request
    public class LoanApplicationRequestMessage{
        public LoanApplicationRequest loanApplicationRequest;
    }
    
    public class BillingAddress {
        public String addrLine1;
        public String addrLine2;
        public String city;
        public String state;
        public String postalCode;
    }

    public class PaymentInfo {
        public String brand;
        public String cardholdersName;
        public String maskedCardNumber;
        public String cardExpiry;
        public String refNumber;
        public String transactionNumber;
    }

    public class Customer {
        public String taxIdNumber;
        public String dateOfBirth;
        public Decimal annualIncome;
        public String creditCardCVV;
        public BillingAddress billingAddress;
        public BillingAddress previousAddress;
        public PaymentInfo paymentInfo;
    }

    public class LoanApplicationRequest {
        public String partnerId;
        public String callId;
        public String opportunityId;
        public String partnerRepName;
        public Customer customer;
    }
    
    //Loan Response
    public class LoanApplicationResponseMessage{
        public LoanApplicationResponse loanApplicationResponse;
    }
    
    public class LoanApplicationResponse {
        public String opportunityId;
        public String message;
        public LoanApplication loanApplication;
    }

    public class LoanApplication {
        public String id;
        public String loanStatus;
        public String decisionStatus;
        public String decisionReasonCd;
        public String decisionReasonDesc;
        public Boolean retryAllowed;
        public Boolean ofac;
        public String submittedDate;
        public String expiryDate;
        
    }
    
    public class zootError{
        public list<errors> errors;
    }
    public class errors{
        public string status;
        public string errorCode;
        public string errorMessage;
    }
}