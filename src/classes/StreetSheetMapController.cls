/************************************* MODIFICATION LOG ********************************************************************************************
* StreetSheetMapController
*
* DESCRIPTION : Used by the Street Sheet Map Visual Force page to obtain all dynamic data for display.  
*               It is a Controller in the SFDC MVC architecture.
*
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*             				 10/14/2011			- Original Version
*
*													
*/
public with sharing class StreetSheetMapController {
	
    public String streetSheetID {get;set;}
    public String streetSheetName {get;set;}
    
    public String mapDataPointsStr {get; set;}
    public String errorMessage {get; set;}  
    public List <StreetSheetItem__c> noGeocodeItemsList {get;set;}
    
    private List <SearchItem> streetSheetItemList {get; set;}
    
    private String DATA_DELIMITER = '~';
	private String POINT_DELIMITER = '^';
    

    public StreetSheetMapController() {
		streetSheetItemList = new List<SearchItem>();
		noGeocodeItemsList = new List <StreetSheetItem__c>();
		
		streetSheetID=System.currentPageReference().getParameters().get('ssid');
		
       	if (streetSheetID != null) {
       		try {
				streetSheetName = [select name from StreetSheet__c where id=:streetSheetID].name;
			} catch (exception e){
				system.debug(e.getMessage());
			}
       		
       		streetSheetItemList = SearchManager.findStreetSheetItemsForMapping(streetSheetID);
       		processItems();
       		
       		if (noGeocodeItemsList.size() > 0) {
       			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info, 'All items could not be plotted on the map.  Refer to the table below.'));
       		}
       	}
       	
    }
 
	public void processItems() {
				
		mapDataPointsStr='';
		for (SearchItem si : streetSheetItemList) {
			if (si.ssItem.Latitude__c != null && si.ssItem.Latitude__c != 0 && 
				si.ssItem.Longitude__c != null && si.ssItem.Longitude__c != 0) {
					
				// add this item to the data for display
				mapDataPointsStr += si.ssItem.Latitude__c;
				mapDataPointsStr += DATA_DELIMITER;
				mapDataPointsStr += si.ssItem.Longitude__c;
				mapDataPointsStr += DATA_DELIMITER;
				mapDataPointsStr += si.ssItem.AccountName__c;
				mapDataPointsStr += DATA_DELIMITER;
				mapDataPointsStr += si.ssItem.SiteStreet__c;
				mapDataPointsStr += DATA_DELIMITER;
				mapDataPointsStr += si.ssItem.SiteCity__c;
				mapDataPointsStr += DATA_DELIMITER;
				mapDataPointsStr += si.ssItem.SiteState__c;
				mapDataPointsStr += DATA_DELIMITER;
				mapDataPointsStr += si.ssItem.SitePostalCode__c;
				mapDataPointsStr += DATA_DELIMITER;
				if (si.ssItem.DisconnectDate__c != null) {
					mapDataPointsStr += si.ssItem.DisconnectDate__c.format();
				}
				else {
					mapDataPointsStr += '';
				}	
				mapDataPointsStr += DATA_DELIMITER;
				if (si.ssItem.DisconnectReason__c != null) {
					mapDataPointsStr += si.ssItem.DisconnectReason__c;
				}
				else {
					mapDataPointsStr += 'N/A';
				}	
				mapDataPointsStr += DATA_DELIMITER;
				mapDataPointsStr += si.ssItem.Data_Source__c;
				mapDataPointsStr += DATA_DELIMITER;
				mapDataPointsStr += si.ssItem.Type__c;
				mapDataPointsStr += DATA_DELIMITER;
				if (si.ssItem.AccountID__c != null) {
					mapDataPointsStr += si.ssItem.AccountID__c;
				} else {
					mapDataPointsStr += si.ssItem.LeadID__c;
				}
				mapDataPointsStr += POINT_DELIMITER;
			}
			else {
				// add to no geocode data
				noGeocodeItemsList.add(si.ssItem);
			}
		}
	}
	
    public PageReference initMapLoad() {
        return null;
    }


}