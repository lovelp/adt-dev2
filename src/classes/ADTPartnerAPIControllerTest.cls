@IsTest
public class ADTPartnerAPIControllerTest {
    
    //create address
  private static Address__c createAddress(){
    Address__c addr = new Address__c();
        // Set Addr
        addr.Street__c = '1501 yamato road';
        addr.City__c = 'Boca raton';
        addr.State__c = 'FL';
        addr.PostalCode__c = '33431';
        addr.OriginalAddress__c = '1501 YAMATO ROAD+BOCA RATON+FL+33431';
        addr.StandardizedAddress__c ='1501 YAMATO ROAD,BOCA RATON,FL,33431';
         addr.StandardizedAddress__c = addr.StandardizedAddress__c.toUpperCase();
         addr.OriginalAddress__c = addr.OriginalAddress__c.toUpperCase();
        insert addr;
        return addr;
  }
    
   
  //create postal code 
  private static void createpostalCode(){
    List<Postal_Codes__c> pp = new List<Postal_Codes__c>();
    Postal_Codes__c p = new Postal_Codes__c(Name = '33431',BusinessID__c = '1100');
    pp.add(p);
    postal_Codes__c p1 = new Postal_Codes__c(Name = '32258',BusinessID__c = '1100');
    pp.add(p1);
    insert pp;
  }
   
   
    
     private static Call_Data__c createcallData(){
        Lead l = new Lead();
        l.LastName = 'Test';
        l.Channel__c = 'Resi Direct Sales';
        l.AddressID__r = createAddress();
        
         
        Call_Data__c cData = new Call_Data__c();
        cData.name = 'test call';
        cData.Lead__c = l.id;
        insert cData;
        return cData;
    }
     private static Opportunity createopportunity(){
    Opportunity opp = new Opportunity();
        Account acct = TestHelperClass.createAccountData();
        acct.Business_Id__c = '1100 - Residential';
        acct.EquifaxApprovalType__c = '';
        acct.EquifaxRiskGrade__c = '';
        acct.Equifax_Last_Check_DateTime__c= system.today()-2;
        acct.AddressID__c = null;
        update acct;
        opp.Name = acct.Name;
        opp.Accountid = acct.id;
        opp.StageName = 'Proposal/Price Quote';
        opp.CloseDate = Date.today();
        insert opp;
        return opp;
  }
    @IsTest
    public static void testADTPartnerCtrl(){
        List<Equifax__c> efx = new List<Equifax__c> ();
        efx.add(new Equifax__c (name= 'Default Condition Code' , value__c= 'CAE1'));
        efx.add(new Equifax__c (name= 'Days to allow additional check' , value__c= '1'));
        efx.add(new Equifax__c (name= 'Default Risk Grade' , value__c= 'W'));
        insert efx;
        
        Equifax_Mapping__c eMap = new Equifax_Mapping__c();
        eMap.CreditScoreStartValue__c = '';
        eMap.CreditScoreEndValue__c = '';
        eMap.ApprovalType__c = 'CAE1';
        eMap.RiskGrade__c = 'Y';
        eMap.RiskGradeDisplayValue__c = '(NO Hit) Approved: Annual, Easypay, Full Deposit';
        eMap.NoHit__c = true;
        eMap.HitCode__c = '2';
        eMap.HitCodeResponse__c = '';
        insert eMap;
        Equifax_Conditions__c EquifaxConditionSetting1 = new Equifax_Conditions__c( Name='CAE1');
        EquifaxConditionSetting1.Agent_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting1.Agent_Quoting_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting1.Customer_Message__c = 'We were able to pull your credit and we are able offer you our best pricing';
        EquifaxConditionSetting1.Deposit_Required_Percentage__c = 0.0;
        EquifaxConditionSetting1.EasyPay_Required__c = true;
        EquifaxConditionSetting1.Payment_Frequency__c = 'A';
        EquifaxConditionSetting1.Three_Pay_Allowed__c = true;
        insert EquifaxConditionSetting1;
        ResaleGlobalVariables__c rsaleGlobal1 = new ResaleGlobalVariables__c();
        rsaleGlobal1.Name= 'IntegrationUser';
        rsaleGlobal1.Value__c = userInfo.getName();  
        insert rsaleGlobal1;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name = 'NewRehashHOARecordOwner';
        Organization org = [SELECT id, isSandbox FROM Organization limit 1];
        if(org.isSandbox){
            rgv.value__c = 'jbelch@adt.com.adttest1';
        }else{
            rgv.value__c = 'jbelch@adt.com';
        }
        insert rgv;
        ResaleGlobalVariables__c rgvlist = new ResaleGlobalVariables__c(name = 'DataRecastDisableAccountTrigger', value__c = 'True');
        insert rgvlist;
        ResaleGlobalVariables__c rgvlist1 = new ResaleGlobalVariables__c(Name = 'DaysToPopulateDNISOnAccount',Value__c = '1');
        insert rgvlist1;
        ResaleGlobalVariables__c rgvlist2 = new ResaleGlobalVariables__c(Name = 'RvServiceabilityOrderTypes',Value__c = 'N1;N2;N3;N4;A1;R1;R2;R3;R4');
        insert rgvlist2;
        
        List<PartnerConfiguration__c> pConfigList = new List<PartnerConfiguration__c>();
        PartnerConfiguration__c pConfig1 = new PartnerConfiguration__c();
        pConfig1.AllowedOrderType__c = 'N1; R1';
        pConfig1.AllowedLOB__c = '1100 - Residential';
        pConfig1.PartnerID__c = 'RV';
        pConfig1.PartnerMessage__c = 'Test1';
        pConfigList.add(pConfig1);
        
        PartnerConfiguration__c pConfig2 = new PartnerConfiguration__c();
        pConfig2.AllowedOrderType__c = 'N1; R1';
        pConfig2.AllowedLOB__c = '1100 - Residential';
        pConfig2.PartnerID__c = 'ECOM';
        pConfig2.AccountUpdateAPI__c = 'Deep Clone; Match Account';
        pConfig2.PartnerMessage__c ='Test';
        pConfigList.add(pConfig2);
        insert pConfigList;
        
        List<ID> leadList = new List<ID>();
        Postal_Codes__c pc = new Postal_Codes__c ();
        pc.Name = '22039';
        pc.BusinessID__c ='1100';
        insert pc;
        
        //Create Addresses first
        Address__c addr1 = new Address__c();
        addr1.Latitude__c = 38.72686000000000;
        addr1.Longitude__c = -77.25470100000000;
        addr1.Street__c = '8507 Oak Pointe Way';            
        addr1.City__c = 'Fairfax Station';
        addr1.State__c = 'VA';
        addr1.PostalCode__c = '22039';
        addr1.OriginalAddress__c = '8507 Oak Pointe Way, Fairfax Station, VA 22o39';            
        insert addr1;
        
        Lead l = new Lead();
        l.FirstName = 'John';
        l.LastName = 'Smith';
        l.Phone = '904- 999- 1234';
        l.Channel__c = 'Resi Direct Sales';
        l.Business_Id__c ='1100 - Residential';
        l.SiteStreet__c = '8507 Oak Pointe Way';
       // l.SiteStreet2__c = 'Unit 3';
        l.SiteStreetName__c = 'First Street';
        l.SiteStreetNumber__c = '101';
        l.SiteCity__c = 'Fairfax Station';
        l.SiteStateProvince__c = 'VA';
        l.SiteCounty__c = 'Duval';
        l.SiteCountryCode__c = 'US';
        l.IncomingLatitude__c = 30.332184;
        l.IncomingLongitude__c = -81.60647;
        l.SitePostalCode__c = '22039';
        l.SitePostalCodeAddOn__c = '1234';
        l.SiteValidated__c = True;
        l.SiteValidationMsg__c= 'test';
        l.PostalCodeID__c = pc.id;
        l.Status = 'Open';
        l.AddressID__c = addr1.id;
        insert l;
        system.debug('LeadId==>'+l.id);
        leadList.add(l.id);
        Account acc = new Account();
        acc.name = 'test';
        acc.Business_Id__c = '1100 - Residential';
        acc.EquifaxApprovalType__c = 'CAE1';
        acc.EquifaxRiskGrade__c = 'Y';
        acc.PostalCodeID__c = pc.id;
        acc.Equifax_Last_Check_DateTime__c = system.today()-2;
        acc.MMBLookup__c = true;
        acc.LastMMBLookupRun__c =  System.now();
        acc.TelemarAccountNumber__c = '1233';
        acc.Channel__c = 'Custom Home Sales';
        insert acc;
        Call_Data__c callID = new Call_Data__c();
        callID.name = 'test call';
        callID.Lead__r = l;
         callID.Account__c= acc.id;
        insert callID;
        
        Town__c to = new Town__c();
        to.Name = 'AK Anchorage-BUSI';
        to.District__c='621';
        to.BusinessID__c='1200';
        to.Area__c='131';
        to.Region__c='30';
        to.Active__c=true;
        to.Address1__c='5520 Lake Otis Pkwy';
        to.city__c='Anchorage';
        to.State__c='AK';
        to.Zip__c='99507';
        to.Zip_Code__c='1777';
        to.County__c='Anchorage';
        insert to;
        
        
        Serviceability__c se = new Serviceability__c();
        se.Business_ID__c = '1100 - Residential';
        se.Channel__c = 'Custom Home Sales';
        se.Order_Type__c = 'R2';
        se.Active__c = true;
        se.Order_Type_for_Phone__c = 'R2';
        se.PostalCode__c = pc.id;
        se.Reason__c = 'test reason';
        se.Sales_Appointment__c  = 'R2';
        se.State__c = 'FL';
        se.TM_TownId__c = '54321';
        se.Town__c = to.id;
        se.Start_Date__c=date.today().adddays(-1);
        se.End_Date__c=date.today().adddays(+1);
        insert se;
        
        
        String call = String.valueOf(callID.Id);
        
        ADTPartnerCustomerLookupSchema.CustomerLookupRequest req = new ADTPartnerCustomerLookupSchema.CustomerLookupRequest();
        req.callId = call;
        req.partnerId = 'ECOM';
        ADTPartnerAPIController adtPrtAPI = new ADTPartnerAPIController();
       // adtPrtAPI.getservicability(req);
       system.debug('===============acc'+acc);
        adtPrtAPI.getservicability(acc);
        system.debug('==>'+l.IsConverted);
        l = [Select Id,Name,IsConverted from Lead where ID =:l.id];
        callID.Lead__r = l;
        callID.Lead__r.AddressId__c = addr1.id;
        system.debug('==>'+callID.Lead__r);
        
        adtPrtAPI.getOpportunityId(req);
        //adtPrtAPI.getCreditRating(req);
        adtPrtAPI.makeServiceabilityCall();//newly added
        adtPrtAPI.getleadManagementId();
        
        Call_Data__c callID1 = new Call_Data__c();
        callID1 = [Select id,DNIS__r.name, Account__c, Lead__c, AccountLastActivityDate__c, lead__r.PostalCodeID__c, Lead__r.IsConverted,Lead__r.ConvertedAccountId,lead__r.addressId__c, lead__r.addressId__r.PostalCode__c, lead__r.AddressId__r.CountryCode__c FROM Call_Data__c WHERE id =: callID.ID limit 1];
               
        
        callID1.Account__c= null;//acc.id;
        Update callID1;
        adtPrtAPI.getOpportunityId(req);
        
        req.callId = call;
        
       // adtPrtAPI.getservicability(req);
        adtPrtAPI.getservicability(acc);
        ADTPartnerAPIController.deleteLead(leadList);
        Account accObj = new Account();
        callID.Account__c= acc.id;
        callID.Lead__r = l;
        update callID;
        //adtPrtAPI.getservicability(req);
        adtPrtAPI.getservicability(acc);
        Opportunity opp = createopportunity();
        opp.Name ='Testopp';
        opp.StageName = 'Proposal/Price Quote';
        opp.account = acc;
        update opp;
        System.assertNotEquals(opp.Id, null);
        String oppid = String.valueOf(opp.Id);
        req.OpportunityId = oppid;
        system.debug('oppID ==> '+ oppid);
        system.debug('call Id'+callID.Lead__r.AddressId__c);
        adtPrtAPI.getOpportunityId(req);
        req.callId = call;
        //adtPrtAPI.getCreditRating(req);
        adtPrtAPI.getCreditRating();
    }
    
    @IsTest
    public static void adtPartnerCtrlNegetiveTest(){
        List<Equifax__c> efx = new List<Equifax__c> ();
        efx.add(new Equifax__c (name= 'Default Condition Code' , value__c= 'CAE1'));
        efx.add(new Equifax__c (name= 'Days to allow additional check' , value__c= '1'));
        efx.add(new Equifax__c (name= 'Default Risk Grade' , value__c= 'W'));
        insert efx;
        Equifax_Mapping__c eMap = new Equifax_Mapping__c();
        eMap.CreditScoreStartValue__c = '';
        eMap.CreditScoreEndValue__c = '';
        eMap.ApprovalType__c = 'CAE1';
        eMap.RiskGrade__c = 'Y';
        eMap.RiskGradeDisplayValue__c = '(NO Hit) Approved: Annual, Easypay, Full Deposit';
        eMap.NoHit__c = true;
        eMap.HitCode__c = '2';
        eMap.HitCodeResponse__c = '';
        insert eMap;
        Equifax_Conditions__c EquifaxConditionSetting1 = new Equifax_Conditions__c( Name='CAE1');
        EquifaxConditionSetting1.Agent_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting1.Agent_Quoting_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting1.Customer_Message__c = 'We were able to pull your credit and we are able offer you our best pricing';
        EquifaxConditionSetting1.Deposit_Required_Percentage__c = 0.0;
        EquifaxConditionSetting1.EasyPay_Required__c = true;
        EquifaxConditionSetting1.Payment_Frequency__c = 'A';
        EquifaxConditionSetting1.Three_Pay_Allowed__c = true;
        insert EquifaxConditionSetting1;
        ResaleGlobalVariables__c rsaleGlobal1 = new ResaleGlobalVariables__c();
        rsaleGlobal1.Name= 'IntegrationUser';
        rsaleGlobal1.Value__c = userInfo.getName();  
        insert rsaleGlobal1;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name='NewRehashHOARecordOwner';
        Organization org = [SELECT id, isSandbox FROM Organization limit 1];
        if(org.isSandbox){
            rgv.value__c = 'jbelch@adt.com.adttest1';
        }else{
            rgv.value__c = 'jbelch@adt.com';
        }
        insert rgv;
        ResaleGlobalVariables__c rgvlist = new ResaleGlobalVariables__c(name = 'DataRecastDisableAccountTrigger', value__c = 'True');
        insert rgvlist;
        ResaleGlobalVariables__c rgvlist1 = new ResaleGlobalVariables__c(Name='DaysToPopulateDNISOnAccount',Value__c='1');
        insert rgvlist1;
        ResaleGlobalVariables__c rgvlist2 = new ResaleGlobalVariables__c(Name='RvServiceabilityOrderTypes',Value__c='N1;N2;N3;N4;A1;R1;R2;R3;R4');
        insert rgvlist2;
        
        List<PartnerConfiguration__c> pConfigList = new List<PartnerConfiguration__c>();
        PartnerConfiguration__c pConfig1 = new PartnerConfiguration__c();
        pConfig1.AllowedOrderType__c = 'N1; R1';
        pConfig1.AllowedLOB__c = '1100 - Residential';
        pConfig1.PartnerID__c = 'RV';
        pConfig1.PartnerMessage__c = 'Test1';
        pConfigList.add(pConfig1);
        
        PartnerConfiguration__c pConfig2 = new PartnerConfiguration__c();
        pConfig2.AllowedOrderType__c = 'N1; R1';
        pConfig2.AllowedLOB__c = '1100 - Residential';
        pConfig2.PartnerID__c = 'ECOM';
        pConfig2.AccountUpdateAPI__c = 'Deep Clone; Match Account';
        pConfig2.PartnerMessage__c ='Test';
        pConfigList.add(pConfig2);
        insert pConfigList;
        
          IntegrationSettings__c is=new IntegrationSettings__c();
          is.MMBSearchEndpoint__c = 'https://dev.api.adt.com/adtclup';
          is.MMBSearchUsername__c = 'salesforceclup';
          is.MMBSearchPassword__c = 'abcd1234';
          is.MMBSearchCalloutTimeout__c = 60000;
          insert is;
        
        Postal_Codes__c pc = new Postal_Codes__c ();
        pc.Name = '32212';
        pc.BusinessID__c ='1100';
        insert pc;
        
        Lead l = new Lead();
        l.FirstName = 'John12';
        l.LastName = 'Smith';
        l.Phone = '904- 999- 1234';
        l.Channel__c = 'Resi Direct Sales';
        l.Business_Id__c ='';
        l.SiteStreet__c = '101 Main Street';
        l.SiteStreet2__c = 'Unit 3';
        l.SiteStreetName__c = 'First Street';
        l.SiteStreetNumber__c = '101';
        l.SiteCity__c = 'Jacksonville';
        l.SiteStateProvince__c = 'FL';
        l.SiteCounty__c = 'Duval';
        l.SiteCountryCode__c = 'US';
        l.IncomingLatitude__c = 30.332184;
        l.IncomingLongitude__c = -81.60647;
        l.SitePostalCode__c = '32212';
        l.SitePostalCodeAddOn__c = '1234';
        l.SiteValidated__c = True;
        l.SiteValidationMsg__c= 'test';
        l.PostalCodeID__c = pc.id;
        l.Status = 'Open';
        insert l;
        
        Account acc = AccountInfo.getAccount();
        acc.name = 'test';
        acc.Business_Id__c = '1100 - Residential';
        acc.EquifaxApprovalType__c = '';
        acc.EquifaxRiskGrade__c = '';
        acc.PostalCodeID__c = pc.id;
        //acc.Equifax_Last_Check_DateTime__c = system.today()-2;
        insert acc;
        Call_Data__c callID = new Call_Data__c();
        callID.name = 'test call';
        callID.Lead__r = l;
         
        callID.Account__r= acc;
        insert callID;
        String accID = String.valueOf(acc.Id);
        AccountInfo.accountid = accID;
        Opportunity opp = new Opportunity();
        opp.Name ='Testopp';
        opp.StageName = 'Proposal/Price Quote';
        opp.CloseDate = system.today();
        opp.accountid = acc.id;
        insert opp;
        system.debug('oppid==> '+opp);
        String oppid = String.valueOf(opp.Id);
        ADTPartnerCustomerLookupSchema.CustomerLookupRequest req = new ADTPartnerCustomerLookupSchema.CustomerLookupRequest();
        ADTPartnerAPIController adtPrtAPI = new ADTPartnerAPIController();
        ADTPartnerAPIController.convertLeadtoAcc(l.Id);
        adtPrtAPI.oppId = opp.id;
        callId.Account__c = acc.id;
        String call = String.valueOf(callID.Id);
        req.OpportunityId = oppid;
        req.partnerId = 'ECOM';
        req.callId = call;
        AccountInfo.accountid ='';
        //adtPrtAPI.getCreditRating(req);
        adtPrtAPI.getCreditRating();
       // adtPrtAPI.getservicability(req);
        adtPrtAPI.getservicability(acc);
        adtPrtAPI.acc = acc;
        adtPrtAPI.getleadManagementId();  
        req.OpportunityId = '';
        system.debug('account from call data ==> '+callId.Account__c);
        //adtPrtAPI.getCreditRating(req);
        adtPrtAPI.getCreditRating();
    }
}