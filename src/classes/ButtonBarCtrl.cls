/************************************* MODIFICATION LOG ********************************************************************************************
* ButtonBarCtrl
*
* DESCRIPTION : Controller for the visualforce page handling add-on buttons
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                TICKET      REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel herrera               03/18/2014                      - Original Version
* Leonard Alarcon               07/10/2015          HRM-475     - Added support for special characteres, if special char 
*                                                               will popup a warning message (AccountButtonBar.page)
* Magdiel Herrera               04/28/2014                      - Added credit scoring validation button
* Abhinav Pandey                01/24/2014                      - Added logic to display credit check button
* Siddarth Asokan               09/15/2018                      - Refactored credit check logic    
* Abhinav Pandey                11/28/2018          HRM-8531    - Bank Loan Application
* Siddarth Asokan               01/07/2019          HRM-6905    - Modified the required params for credit check 
* Nitin Gottiparthi             06/29/2019          HRM-10106   - opt out
* Varun Sinha                   08/20/2019          HRM-8470    - Serviceability Rule Check for 1300
* Nishanth Mandala              10/22/2019          HRM-11053   - Removal of HOA flag
*/

public without sharing class ButtonBarCtrl {
    
    //---------------------------------------------------------------------------------------------------
    //  Record id from where the controller is being called when embedded in a page layout
    //---------------------------------------------------------------------------------------------------
    public String recId {get;set;} 
    public Account acc  {get;set;}
    public User currentUser;
    
    //---------------------------------------------------------------------------------------------------
    //  MMB Search visibility flag
    //---------------------------------------------------------------------------------------------------
    public Boolean renderMMBSearchBtn {get;set;}

    //---------------------------------------------------------------------------------------------------
    //  New Quote visibility flag
    //---------------------------------------------------------------------------------------------------
    public Boolean renderNewQuoteBtn {get;set;}
    
    //---------------------------------------------------------------------------------------------------
    //  Display related list of sites for an HOA & ADTGo
    //---------------------------------------------------------------------------------------------------
    public Boolean renderHOASiteList {get;set;}
    public Boolean renderGoToADTBtn{get;set;}
    
    //---------------------------------------------------------------------------------------------------
    //  Validate customer's credit scoring for better offers
    //---------------------------------------------------------------------------------------------------
    public Boolean renderCreditCheck {get;set;}
    
    //HRM - 10881
    public List<LoanApplication__c> lstLoanApplication = new List<LoanApplication__c>();
    
    //Validate the condition for opt out flag is set to true or false to display the opt out button
    public Boolean renderOptOut {get;set;}
    //---------------------------------------------------------------------------------------------------
    // HRM - 8531 Validate customer's  Bank Loan Application conditions
    //---------------------------------------------------------------------------------------------------
    public Boolean disableLoanApplication {get;set;}
    public String disableLoanMessage {get;set;}
    public Boolean displaySpecCharErr {get;set;}
    public String specCharErrText {get;set;}
    /**
    *  @Constructor
    */
    public ButtonBarCtrl(Apexpages.StandardController con){
        renderMMBSearchBtn = false;
        renderNewQuoteBtn = false;
        renderHOASiteList = false;
        renderCreditCheck = false;
        renderOptOut = false;
        disableLoanApplication = false; //HRM - 8531 Bank Loan Application
        recId = con.getId();
        List<Account> accList = [SELECT Name, Firstname__c, lastname__c, Channel__c, Business_Id__c, MMBOrderType__c, MMBLookup__c, PostalCodeID__c ,Email__c, HOA__c, HOA_Type__c, ADTEmployee__c,Equifax_Last_Check_DateTime__c, EquifaxApprovalType__c, DOB_encrypted__c, EquifaxRiskGrade__c, Profile_RentOwn__c
                                 FROM Account Where Id = :recId];
        if(accList.size()>0) {
            acc = accList[0];
        }
        
        lstLoanApplication = [Select id from LoanApplication__c where Account__c=:recId];
        List<User> userList = [SELECT isMMBLookupSearch__c, Business_Unit__c, Profile_type__c, EmployeeNumber__c FROM User WHERE ID = :UserInfo.getUserId()];
        if(userList.size()>0){
            currentUser = userList[0];
        }
        HandleVisibleActions();
    }
    
    /**
    *  Init visibility for actions available depending on record information and user access
    *  @method HandleVisibleActions
    *  @return void
    */
    public void HandleVisibleActions(){
        //Set MMB search visibility
        renderMMBSearchBtn = currentUser.isMMBLookupSearch__c != null && currentUser.isMMBLookupSearch__c == true ;
        
        if(String.isNotBlank(recId) && recId.startsWith(Account.sObjectType.getDescribe().getKeyPrefix())){
            //Set new quote visibility
            renderNewQuoteBtn = acc.Business_Id__c.startsWith('1300')? true : acc.MMBLookup__c; //Changed from false to true - FOR HRM 8470

            //Set display HOA detail visible 
            renderHOASiteList = (acc.HOA_Type__c == 'Association');

            //Set run credit check visible
            renderCreditCheck = Utilities.isCreditButtonVisible(acc);  //HRM-11053   - Removal of HOA flag
            //Set GoToADT Visibility
            renderGoToADTBtn = (acc.email__c != null && label.ADTGO_Live == 'true');
            try{
            //set to true of there is special characters in account name, HRM-10096
                if(string.isNotBlank(Label.SpecialCharactersChannels) && Label.SpecialCharactersChannels.split(';').contains(acc.Channel__c)){
                    if(Utilities.containsSpecialChar(acc.firstname__c) || Utilities.containsSpecialChar(acc.lastname__c) || Utilities.containsSpecialChar(acc.name)){
                        specCharErrText =  ErrorMessages__c.getInstance('AccountNameSpecialChar').Message__c;
                        displaySpecCharErr = true;
                    }
                }
            }
            catch(exception e){
                system.debug('Error--'+ e.getMessage() +' Line :'+e.getLineNumber());
            }
        }
        
        //Display opt out button
        renderOptOut = ResaleGlobalVariables__c.getinstance('runEwcMarketing')!= null && String.isNotBlank(ResaleGlobalVariables__c.getinstance('runEwcMarketing').value__c) && ResaleGlobalVariables__c.getinstance('runEwcMarketing').value__c.equalsIgnoreCase('true') ? true : false;
        disableLoanApplication = (FlexFiConfig__c.getinstance('CFG Disabled') != null && String.isNotBlank(FlexFiConfig__c.getinstance('CFG Disabled').Value__c)) && FlexFiConfig__c.getinstance('CFG Disabled').value__c.equalsIgnoreCase('true') && lstLoanApplication.size() == 0 ? true : false; 
    }
    
    public void callADTGo(){
        String role = currentUser.profile_type__c.containsIgnoreCase('NSC')? 'P':'F';
        Integer empNo = (currentUser.EmployeeNumber__c != null && currentUser.EmployeeNumber__c.isNumeric())?Integer.valueOf(currentUser.EmployeeNumber__c):0;
        GoToADTControllerExtension.PostInviteItemRequestWrapper postInviteItemWrapperInstance = new GoToADTControllerExtension.PostInviteItemRequestWrapper(acc.email__c, empNo, 'ADTGO', role,'SFDC');
        String jsonString = JSON.serialize(postInviteItemWrapperInstance);
        renderGoToADTBtn = false;
        GoToADTControllerExtension gotoADTInstance = new GoToADTControllerExtension();
        gotoADTInstance.doCallOut(jsonString,acc.id); 
    }
    
    @AuraEnabled
    public static string callMMBLookup(String recId){
        // Static methods does not invoke constructors so doing a query on the user in the method
        list<User> userList = [SELECT isMMBLookupSearch__c FROM User WHERE ID = :UserInfo.getUserId() AND isMMBLookupSearch__c = true ];
        if(userList != null && userList.size() > 0){
            // Redirect to MMB lookup page
            list<Account> aList = [SELECT Business_Id__c from Account where ID =:recId];
            String mmbUrl = (aList != null && aList.size()>0 && aList[0].Business_Id__c.contains('1300'))? '/apex/MMBCustomerSiteLookup?lookupType=Multisite&objId=' : '/apex/MMBCustomerSiteLookup?objId=';
            PageReference redirectPage = new PageReference(mmbUrl + recId);
            aura.redirect(redirectPage);
        }else{
            return 'MMB lookup is restricted for the current user.';
        }
        return 'Success';
    }
}