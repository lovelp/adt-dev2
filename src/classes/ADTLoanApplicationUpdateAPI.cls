@RestResource(urlMapping='/LoanApplicationUpdate/*')
/************************************* MODIFICATION LOG ********************************************************************************************
*
* DESCRIPTION : EDS API WebService Update
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                   TICKET              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Varun Sinha               11/29/2018                                 - Original Version
* Abhinav Pandey            10/21/2019                 HRM-10881       - Flex Fi
*/
global class ADTLoanApplicationUpdateAPI {
    @HttpPost
    global static void accessData(){
        ADTLoanApplicationSchema.LoanApplicationUpdateRequest reqJSON = null;
        ADTLoanApplicationSchema.MessageResponse resJSON = null;
        ADTLoanApplicationController fnLoanApp = new ADTLoanApplicationController();
        boolean tilaPNoteFlag = false;
        String jSONRequestBody = null;
        string jSONWhiteSpace = null;
        boolean jobClosedFlag = false;
        try{
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;
            jSONRequestBody = req.requestBody.toString().trim();
            //Perform Validations
            resJSON = fnLoanApp.performUpdateValidations(req);
            if(resJSON == null || resJSON.message.contains(FlexFiMessaging__c.getinstance('400').ErrorMessage__c)){
                prepareRESTResponse(resJSON,400);
                return;
            }
            reqJSON = reqJSONUpdate(jSONRequestBody);
            //To check whether tilaPnoteAcceptedDtTm is present in the request or not
            jSONWhiteSpace = jSONRequestBody.deleteWhitespace(); 
            if(jSONWhiteSpace.contains('"tilaPnoteAcceptedDtTm":null')){
                tilaPNoteFlag = true;
            }
            if(jSONWhiteSpace.contains('"jobClosed":true')){
                jobClosedFlag = true;
            }
            //Wrapper Creation for Response
            resJSON = fnLoanApp.updateLoanApplication(reqJSON,tilaPNoteFlag,jobClosedFlag,jSONWhiteSpace);
            //HRm -10881
            if(resJSON.message == FlexFiMessaging__c.getinstance('Loan Updated').ErrorMessage__c || resJSON.message == FlexFiMessaging__c.getinstance('Quote Job Updated').ErrorMessage__c){
                prepareRESTResponse(resJSON,200);
                return;
            }
            if(resJSON.message == FlexFiMessaging__c.getinstance('404 - Account').ErrorMessage__c){
                prepareRESTResponse(resJSON,404);
                return;
            }
            if(resJSON.message.contains(FlexFiMessaging__c.getinstance('Update Failed - TilaPNote').ErrorMessage__c)){
                prepareRESTResponse(resJSON,409);
                return;
            }
            if(resJSON.message.contains(FlexFiMessaging__c.getinstance('500').ErrorMessage__c)){
                prepareRESTResponse(resJSON,500);
                return;
            }
            
        }catch(Exception ex){
            ADTApplicationMonitor.log(ex, 'ADTLoanApplicationUpdateAPI', 'accessData',ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
        }
    }
    //Deserializing Update
    public static ADTLoanApplicationSchema.LoanApplicationUpdateRequest reqJSONUpdate(string jSONRequestBody){
        ADTLoanApplicationSchema.LoanApplicationUpdateRequest reqJSON = null;
        reqJSON = (ADTLoanApplicationSchema.LoanApplicationUpdateRequest)JSON.deserializeStrict(jSONRequestBody,ADTLoanApplicationSchema.LoanApplicationUpdateRequest.class);//WPR_LoanApplicationRequestJSON.parse(req.requestBody.toString().trim());
        return reqJSON;
    }
    //Serializing Wrapper
    public static RestResponse prepareRESTResponse(Object sObj, Integer statusCode){
        RestResponse res = null;
        try{
            res = RestContext.response;
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf(JSON.serialize(sObj,true));
            res.statusCode = statusCode;
            System.debug('The loan applications response is'+JSON.serialize(sObj));
        }catch(Exception ex){
            ADTApplicationMonitor.log(ex, 'ADTLoanApplicationRetrieveAPI', 'prepareRESTResponse',ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
        }
        return res;
    }
}