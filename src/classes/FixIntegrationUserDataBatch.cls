/************************************* MODIFICATION LOG ********************************************************************************************
* FixIntegrationUserDataBatch
*
* DESCRIPTION : Batch class used to correct a Phase 1 issue.
*               Retained for potential future reuse but not planned for deployment with Phase 2. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli             2/9/2012          - Original Version
*
*/

global class FixIntegrationUserDataBatch  implements Database.batchable<sObject>{
    
    global final String query;

    global FixIntegrationUserDataBatch() {
        this.query = 'select Id, Name, Type, RecordTypeId, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, OwnerId, AddressID__c, AssignedBy__c, Business_Id__c, CustomerID__c, Data_Source__c, DateAssigned__c, DealerStatus__c, DisconnectComment__c, DisconnectDate__c, DisconnectReason__c, DispositionCode__c, DispositionComments__c, DispositionDate__c, InService__c, LastName__c, Latitude__c, LeadExternalID__c, LeadStatus__c, ManuallyAssigned__c, NewLead__c, NewMoverDate__c, NewMoverType__c, NewMover__c, PostalCodeID__c, ResaleArea__c, ResaleDistrict__c, ResaleRegion__c, ResaleTown__c, ResoldType__c, SiteCity__c, SitePostalCode__c, SiteState__c, SiteStreet__c, UnassignedLead__c, Channel__c, RecordType.Name, ProcessingType__c from Account where owner.name = \'Integration User\'';
    }
    
    //get Querylocator with the specitied query
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sObject> accounts) {
        List<Account> updatedAccounts = new List<Account>();
        for(sObject s : accounts)
        {
            updatedAccounts.add((Account)s);
        }
        ChangeAccountOwnerController.changeOwnershipWithListOfAccounts(updatedAccounts);
        update updatedAccounts;
    }
    
    global void finish(Database.BatchableContext bc) {

    }
    
    public void changeOwnership(List<Account> accounts)
    {
        String Error = '';
        List<Id> accountIds = new List<Id>();
        List<Id> postalCodeIds = new List<id>();
        Map<Id, Id> postalCodeTerritoryOwnerMap = new Map<id, id>();
        Map<Id, String> PostalCodeTowns = new Map<Id, String>();
        
        Map<Id, String> PostalCodeOnlyTowns = new Map<Id, String>();
        
        Map<Id, Id> PostalCodeOwners = new Map<Id, Id>();
        Map<Id, Id> PostalCodeManagerOwners = new Map<Id, Id>();
        List<Id> PostalCodeNoOwners = new List<Id>();
        
        Map<String, Id> managerTownsMap = new Map<String, Id>();
        Map<Id, String> townOwners = new Map<Id, String>();
        
        for(Account acct : accounts)
        {
            accountIds.add(acct.id);
        }
        //get the zipcode and and business id from account
        for(Account acct : accounts)
        {
            postalCodeIds.add(acct.PostalCodeID__c);    
        }
        
        String GlobalUnassingnedUser = Utilities.getGlobalUnassignedUser();
        
        //get postal code towns
        List<Postal_Codes__c> pcs = [select id, name, townId__c, BusinessId__c from Postal_Codes__c where id in : postalCodeIds];
        for(Postal_Codes__c pc : pcs)
        {
            PostalCodeTowns.put(pc.Id, pc.TownId__c + ';' + pc.BusinessId__c);
            PostalCodeOnlyTowns.put(pc.id, pc.TownId__c);
        }
        //get the territory assignment for each of the zipcodes
        List<TerritoryAssignment__c> terrAssign = [select id, Name, PostalCode__c, OwnerId, PostalCodeID__c from TerritoryAssignment__c where PostalCodeId__c in : postalCodeIds];
        for(TerritoryAssignment__c ta : terrAssign)
        {
            postalCodeTerritoryOwnerMap.put(ta.PostalCodeID__c, ta.OwnerId);
        }
        //if no territory assignement exists, get the town for each of the unassigned zipcodes
        List<ManagerTown__c> managerTowns = [Select id, name, ManagerId__c, TownId__c, BusinessId__c from ManagerTown__c where TownId__c in : PostalCodeOnlyTowns.values()];
        for(ManagerTown__c mt : managerTowns)
        {
            managerTownsMap.put(mt.TownId__c + ';' + mt.BusinessId__c, mt.ManagerId__c);
        }
        for(Id pcid : PostalCodeTowns.keySet())
        {
            townOwners.put(pcid, managerTownsMap.get(PostalCodeTowns.get(pcid)));
        }
        
        for (id pcid : postalCodeIds)
        {
            if(postalCodeTerritoryOwnerMap.get(pcid) != null) 
            {
                PostalCodeOwners.put(pcid, postalCodeTerritoryOwnerMap.get(pcid));
            }
            else if(townOwners.get(pcid) != null)
            {
                PostalCodeManagerOwners.put(pcid, townOwners.get(pcid));
            }
            else
            {
                PostalCodeNoOwners.add(pcid);
            }
        }
        //if the town is assigned to a rep, change the assignment
        //if the town is assigned to a manager, assign the leads to the manager and create account teams        
        //if no town assignment exists, leads are loaded to a global unassigned queue 
        List<Account> teamMemberAccounts = new List<Account>();
        for(Account acct : accounts)
        {
            if(PostalCodeOwners.get(acct.PostalCodeId__c) != null)
            {
                acct.OwnerId = PostalCodeOwners.get(acct.PostalCodeId__c);
                acct.DateAssigned__c = DateTime.now();
                acct.UnassignedLead__c = false;
            }
            else if (PostalCodeManagerOwners.get(acct.PostalCodeId__c) != null)
            {
                acct.OwnerId = PostalCodeManagerOwners.get(acct.PostalCodeId__c);
                acct.DateAssigned__c = DateTime.now();
                acct.UnassignedLead__c = true;
                teamMemberAccounts.add(acct);
            }
            else
            {
                acct.OwnerId = GlobalUnassingnedUser;
                acct.DateAssigned__c = DateTime.now();
                acct.UnassignedLead__c = true;
            }
        }
    }
}