/************************************* MODIFICATION LOG ********************************************************************************************
* DirectMailControllerExtension
*
* DESCRIPTION : Supports Direct Mail exports by retrieving Account data and performing the disposition to Mailed Letter 
*               for each selected Account and supports Direct Mail submissions by creating a Task for the User’s manager   
*               It is a Controller in the SFDC MVC architecture.
*
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*                            10/14/2011         - Original Version
*
*                                                   
*/

public with sharing class DirectMailControllerExtension {
    
    private String streetSheetID {get;set;}
    
    public List <SearchItem> itemList {get; set;}
    
    public String userName {get;set;}
    public String userPhone {get; set;}
    
    public List <User> managerList {get; set;}
    
    public static final String LEADMAILEDRESICODE = 'Mailed (Resi)';
    public static final String LEADMAILEDRESIDETAIL = 'Mailed - Letter';
    public static final String LEADMAILEDSBCODE = 'Mailed Letter (SB)';
    
    
    public DirectMailControllerExtension(ApexPages.StandardController stdController) {
        streetSheetID = stdController.getId();
    }
    
    // Retrieve data needed to create the Direct Mail export and update the disposition
    public PageReference process() {
    
        // determine who created this Street Sheet
        StreetSheet__c streetSheet = [select CreatedById from StreetSheet__c where Id=:streetSheetID limit 1];
        String userID = streetSheet.CreatedById;
        User currentUser = [select Name, Phone from User where Id=:userID limit 1];
        // the export needs that user's name and phone number
        userName = currentUser.Name;
        userPhone = currentUser.Phone;
        
        // retrieve the Street Sheet Items
        itemList = SearchManager.findStreetSheetItems(streetSheetID);
        
        // need to update leads and accounts -- set disposition to mailed letter
        Set<Id> aids = new Set<Id>();
        Set<Id> lids = new Set<Id>();
        for (SearchItem si : itemList) {
            if (si.ssItem.AccountID__c != null) {
                aids.add(si.ssItem.AccountID__c);
            } else if (si.ssItem.LeadID__c != null) {
                lids.add(si.ssItem.LeadID__c);
            }
        }
        
        list<Account> accounts = new list<Account>([select Id,Business_ID__c,DispositionCode__c,DispositionDetail__c from Account where Id IN :aids]);
        list<Lead> leads = new list<Lead>([select Id,Business_ID__c,DispositionCode__c,DispositionDetail__c from Lead where Id IN :lids]);
        
        for (Account a : accounts) {
            if (a.Business_Id__c != null && a.Business_Id__c.contains('1200')) {
                a.DispositionCode__c = ChangeAccountDispositionController.mailedSBCode;
            } else if (a.Business_Id__c != null && a.Business_Id__c.contains('1100')) {
                a.DispositionCode__c = ChangeAccountDispositionController.mailedResiCode;
                a.DispositionDetail__c = ChangeAccountDispositionController.mailedResiDetail;
            }
        }
        for (Lead l : leads) {
            if (l.Business_Id__c != null && l.Business_Id__c.contains('1200')) {
                l.DispositionCode__c = LEADMAILEDSBCODE;
                l.DispositionDetail__c = null;
            } else if (l.Business_Id__c != null && l.Business_Id__c.contains('1100')) {
                l.DispositionCode__c = LEADMAILEDRESICODE;
                l.DispositionDetail__c = LEADMAILEDRESIDETAIL;
            }
        }
        
        update accounts;
        update leads;
        
        return null;    
    }
    
    // Create a task for the current user's manager requesting a Direct Mail export
    public PageReference request() {
    
        User currentUser = [select Id, Name, UserRoleId from User where Id=:UserInfo.getUserId() limit 1];
        
        List<User> managerList = Utilities.getManagersForRole(currentUser.UserRoleId);      
      
        // Typically, a sales rep will report to one manager
        // Although not expected, it is possible for the role hierarchy to be configured such that
        // a sales rep has two managers.  In such a case, assign the task to the first manager returned.
        Id managerId;
        if (managerList.size() >= 1) { 
            managerId = managerList[0].Id;
        }
        else {
            // No manager found so report the problem
            return new PageReference('/apex/ShowError?Error=NOMGR');
        }
            
        try {
            TaskManager.createDirectMailExportTask(currentUser, managerId, streetSheetID);
        } catch (Exception e){
            system.debug('Exception creating a Direct Mail Export Task: ' + e.getMessage());
            return new PageReference('/apex/ShowError?Error=DATAERROR');
        }
        
        PageReference pr = new PageReference ('/apex/ManageProspectList?id='+ streetSheetID+'&displayMessage=true');
        pr.setRedirect(true);
        return pr; 
        
    }

}