/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : UpdateGeocodingBatchTest is a test class for UpdateGeocodingBatch.cls
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Erin McGee      07/08/2013      - Original Version
*                        
*/
@isTest
private class UpdateGeocodingBatchTest
{   
    @isTest
    static void testUpdateGeocodingBatch()
    {
        Test.startTest();
        UpdateGeocodingBatch z = new UpdateGeocodingBatch();
        //z.testFlag = true;
        /* DateTime timenow = system.now().addMinutes(5);
        String seconds = String.valueOf(timenow.second()); //Execute at Zero Seconds
        String minutes = String.valueOf(timenow.minute()); //Execute at every 10th minute of hour
        String hours = String.valueOf(timenow.hour()); // Execute Every Hour
        String sch = seconds + ' ' + minutes + ' * * * ?';
        String jobName = 'Geocode New Addresses' + timenow;
        system.schedule(jobName, sch, z);*/
        Address__c addr = new Address__c();
        addr.Street__c = '5700 Lombardo Center';
        addr.City__c = 'Seven Hills';
        addr.State__c = 'OH';
        addr.PostalCode__c = '44131';
        addr.OriginalAddress__c = '5700 Lombardo Center, Seven Hills, OH 44131';
        insert addr;

        Geocoding__c geocode = new Geocoding__c(AddressToBeGeocoded__c = addr.Id,IsBeingProcessed__c=false);
        insert geocode;
        
        DateTime timenow = system.now().addMinutes(5);
        String seconds = String.valueOf(timenow.second()); //Execute at Zero Seconds
        String minutes = String.valueOf(timenow.minute()); //Execute at every 10th minute of hour
        String hours = String.valueOf(timenow.hour()); // Execute Every Hour
        String sch = seconds + ' ' + minutes + ' * * * ?';
        String jobName = 'Geocode New Addresses' + timenow;
        system.schedule(jobName, sch, z);
        Test.stopTest();
    }
}