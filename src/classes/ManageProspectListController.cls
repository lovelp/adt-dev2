/************************************* MODIFICATION LOG ********************************************************************************************
* ManageProspectListController
*
* DESCRIPTION : Controller for street sheet item list VF page on prospect list detail page
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli               7/29/2012               - Original Version
*
* Erin McGee                    6/20/2013               -Added Repeat Disposition Functionality                              
* Magdiel Herrera               6/5/2014                -Added scoring feature to prospect items                              
*/

public with sharing class ManageProspectListController {
    
    public StreetSheet__c 	selectedSS 			{ get; set; }
    public Lead 			dispLead 			{ get; set; }
    public Account 			dispAccount 		{ get; set; }
    public Id 				ssid 				{ get; set; }
    public Id 				dispLeadId 			{ get; set; }
    public Id 				dispAccountId 		{ get; set; }
    public Id 				dispId 				{ get; set; }
    public Boolean 			dispAccountStandard { get; set; }
    public Boolean 			dispAccountOther	{ get; set; } 
    public Boolean 			dispAccountRIF 		{ get; set; } 
    public Boolean 			showReasonSystem	{ get; set; }
    public Boolean 			showReasonDetail	{ get; set; }
    public String 			errormsg 			{ get; set; }
    public String 			deleteItemId 		{ get; set; }
    public String 			sortType			{ get; set; }
    public String			sortColumn			{ get; set; }

	public Enum SORT_COLUMN	{name, site, city, postalcode}
	public Map<SORT_COLUMN, String> SORT_COLUMN_LABEL = new Map<SORT_COLUMN, String>{SORT_COLUMN.name => 'Name', SORT_COLUMN.site => 'Site Street', SORT_COLUMN.city => 'Site City', SORT_COLUMN.postalcode => 'Site Postal Code'};   
	    
    private ApexPages.Standardcontroller 	stdcon;
    private List<StreetSheetItem__c> 		ssitems;
    private Map<String, ProspectItem> 		StreetSheetItemMap;
	
	public List<SelectOption> sortOptions {
		get{
			List<SelectOption> resVal = new List<SelectOption>();
			for(SORT_COLUMN c: SORT_COLUMN.values()){
				resVal.add( new SelectOption(c.name(), SORT_COLUMN_LABEL.get(c)) );
			}
			return resVal; 
		}
	} 
	
    public List<Account> accounts { 
    	get {
    		List<Account> resList = new List<Account>(); 
    		for(ProspectItem pi: StreetSheetResultList) {
    			if(pi instanceOf AccountProspectItem){
    				resList.add( ((AccountProspectItem)pi).AccountObj );
    			}
    		}
    		return resList;
    	}
	}
    public List<Lead> leads { 
    	get {
    		List<Lead> resList = new List<Lead>(); 
    		for(ProspectItem pi: StreetSheetResultList) {
    			if(pi instanceOf LeadProspectItem){
    				resList.add( ((LeadProspectItem)pi).LeadObj );
    			}
    		}
    		return resList;
    	}
	}
    
    /**
     * @Constructor
     */
	public ManageProspectListController( ApexPages.Standardcontroller stdcontroller ) {
        stdcon = stdcontroller;
        ssid = stdcon.getId();
        sortType = 'ASC';
        sortColumn = 'name';
        List<StreetSheet__c> selectedSSes=[Select Id, Name, DaysSinceLastUpdated__c, NumberOfLeads__c, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate from StreetSheet__c where id =: ssid Limit 1];
        if(!selectedSSes.isEmpty()) {
            selectedSS = selectedSSes[0];
        }
        else {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'There was an error retrieving this Prospect List.'));
        }
        
        getItems();
    }
    
    /**
     *	Action called at pre-initialization of the page
     *	@method	displayPageMessage
     *	@return	PageReference 
     */
    public PageReference displayPageMessage() {
        if(System.currentPageReference().getParameters().get('displayMessage')!=null)
        {
            if(System.currentPageReference().getParameters().get('displayMessage')=='true')
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Your request to submit direct mail for this prospect list has been sent to your manager.'));
            }
        }
        return null;
    }
    
    /**
     *	Returns a list of StreetSheetItems in a custom order handled by a Comparable wrapper
     *	@property	StreetSheetResultList	
     *	@return  	List<StreetSheetItem>	List of items related to this StreetSheet
     */
    public List<ProspectItem> StreetSheetResultList {
    	get{
    		List<ProspectItem> resItemList = new List<ProspectItem>();
    		if(StreetSheetItemMap!=null && !StreetSheetItemMap.isEmpty()){
    			resItemList = StreetSheetItemMap.values(); 
    		}
    		resItemList.sort();
    		return resItemList; 
    	}
    }
    
    /**
     *	Evaluate current execution context user's profile.
     *	@property 	bDispPN
     *	@return 	Boolean		True in case this current user's profile is 'ADT NA Sales Representative - PN'  
     */
    public boolean bDispPN {
	    get {
	        User usrRec=[SELECT Profile.Name FROM User WHERE id=:userinfo.getUserId()];
	        if (usrRec.Profile.Name=='ADT NA Sales Representative - PN'){
	            return true;
	        }
	        else {
	            return false;
	        }
	    }
	    set;        
    }
    
    public PageReference sortProspectItemList(){
    	for(String k: StreetSheetItemMap.keySet()){
    		ProspectItem p = StreetSheetItemMap.get(k);
    		p.sortColumn = sortColumn;
    		p.sortType = sortType;
    	}
    	return null;
    }
    
    /**
     *	Delete a StreetSheetItem
     *	@method	deleteSSItem	
     *	@return PageReference	returns null to refresh form on client side
     */
    public PageReference deleteSSItem() {
        
        try{
            String acctprefix = Schema.Sobjecttype.Account.getKeyPrefix();
            String leadprefix = Schema.Sobjecttype.Lead.getKeyPrefix();
            if (deleteItemId == null || deleteItemId.length() == 0 || StreetSheetItemMap == null || StreetSheetItemMap.isEmpty()) {
                return null;
            }
            
            if(StreetSheetItemMap.containsKey(deleteItemId)){
            	ProspectItem piObj = StreetSheetItemMap.get(deleteItemId);            	
            	delete  piObj.ssiObj;
            	
            	// We only remove this item from our viewState if the streetsheetitem__c record was succesfully deleted 
            	StreetSheetItemMap.remove(deleteItemId);
            }
            
            /* Deprecated 6/16/2014 by Magdiel Herrera
            // Reason: Unefficient code referencing data structures not in use since the enhancement 
            for (StreetSheetItem__c ssi : ssitems) {
                if (ssi.AccountID__c == deleteItemId || ssi.LeadID__c == deleteItemID) {
                    delete ssi;
                    break;
                }
            }
            
            if (deleteItemId.startsWith(acctprefix)) {
                for (Integer i=accounts.size()-1;i>=0;i--) {
                    if (deleteItemId == accounts[i].Id) {
                        accounts.remove(i);
                        break;
                    }
                }
            }
            else if (deleteItemId.startsWith(leadprefix)) {
                for (Integer i=leads.size()-1;i>=0;i--) {
                    if (deleteItemId == leads[i].Id) {
                        leads.remove(i);
                        break;
                    }
                }
            }
            */
            
        } catch (Exception ex){
            //do nothing
        }
        return null;
    }    
    
    public PageReference doCancel() {
        errormsg = null;
        return null;
    }
    
    public PageReference StreetSheetItemDisp() {
    	if(	isAccountId(dispId) ){
    		dispAccountId = dispId; 
    		return accountDisp();
    	}
    	else if( isLeadId(dispId) ){
    		dispLeadId = dispId;
    		return leadDisp();
    	}
    	return null;
    } 
    
    public Boolean isAccountId(String objId){        
        return objId.startsWith(Schema.Sobjecttype.Account.getKeyPrefix());
    }
    
    public Boolean isLeadId(String objId){
        return objId.startsWith(Schema.Sobjecttype.Lead.getKeyPrefix());
    } 
    
    public PageReference leadDisp() {
        dispLead = null;
        dispAccount = null;     
        try {
            dispLead = [ SELECT Id, Name, Company, DispositionCode__c, DispositionDetail__c, Business_ID__c, ClosePotential__c, Competitor__c, DispositionDate__c, CompetitorContractEndDate__c, DispositionComments__c, ExistingSolutions__c, ExistingEquipment__c, EquipmentSold__c, DoNotCall, DoNotMail__c, Channel__c, Repeat_Disposition__c
                		 FROM Lead 
                		 WHERE Id = :dispLeadId ];
        } catch (Exception ex) {
            return null;
        }
        return null;
    }
    
    public PageReference accountDisp() {
        dispLead 			= null;
        dispAccount 		= null;     
        showReasonSystem 	= false;
        showReasonDetail	= false;
        dispAccountStandard = false;
        dispAccountOther 	= false;
        dispAccountRIF 		= false;
        try {
            dispAccount = [ SELECT Id, Name, DispositionCode__c, DispositionDetail__c, Reason1ForSystem__c, Reason1DetailForSystem__c, Business_Id__c, DispositionComments__c, DispositionDate__c, Competitor__c, CreateFollowupAfter__c, CompetitorContractEndDate__c, DoNotCall__c, DoNotMail__c, ExistingSolutions__c, SolutionsNotInstalled__c, ModelSalesCallconducted__c, ROIDetermined__c, PulseDemonstrated__c, VideoDiscussed__c, PackageProposed__c, AccessDiscussed__c, ResidentialOffer__c, DoesHomeownerOwnOrManageABusines__c, ClosePotential__c, ServicesProposed__c, ADSCQuoted__c, ANSCQuoted__c, RecordType.DeveloperName, Repeat_Disposition__c
                			FROM Account 
                			WHERE Id = :dispAccountId ];
            if(dispAccount.RecordType.DeveloperName == RecordTypeDevName.STANDARD_ACCOUNT)
            {
                dispAccountStandard = true;
            }
            else if (dispAccount.RecordType.DeveloperName == RecordTypeDevName.RIF_ACCOUNT)
            {
                dispAccountRIF = true;
            }
            else
            {
                dispAccountOther = true;
            }
            
        } catch (Exception ex) {
            return null;
        }
        if (dispAccount.Business_Id__c != null) {
            if (dispAccount.Business_Id__c.contains('1100')) {
                showReasonSystem = true;
                showReasonDetail = true;
            } else if (dispAccount.Business_Id__c.contains('1200')) {
                showReasonSystem = true;
            }
        }
        return null;
    }
    
    public PageReference saveDisp() {
        errormsg = null;
        if (dispLead != null) {
            try {
                update dispLead;
            } catch(Exception ex) {
                errormsg = ex.getMessage();
                ApexPages.addMessages(ex);
            }
        } else if (dispAccount != null) {
            try {
                update dispAccount;
            } catch(Exception ex) {
                errormsg = ex.getMessage();
                ApexPages.addMessages(ex);
            }
        }
        if (errormsg == null) {
            getItems();
        }
        return null;
    }
    
    /**
     *	Initialize StreetSheet and StreetSheetItems information
     *	@method	getItems
     *	@return void
     */
    private void getItems() {
        
        //get ss items
        //ssitems = new List<StreetSheetItem__c>([ SELECT Id,AccountId__c, LeadId__c FROM StreetSheetItem__c WHERE StreetSheet__c = :ssid ]);
        StreetSheetItemMap = new Map<String, ProspectItem> ();
        Map<String, StreetSheetItem__c> ssiObjMap = new Map<String, StreetSheetItem__c>(); 
        
        list<Id> aids = new list<Id>();
        list<id> lids = new List<Id>();
        
        for (StreetSheetItem__c ssi : [ SELECT Id,AccountId__c, LeadId__c, Score__c, Touched__c  FROM StreetSheetItem__c WHERE StreetSheet__c = :ssid ]) {
            if (ssi.AccountID__c != null) {
                aids.add(ssi.AccountID__c);
                ssiObjMap.put(ssi.AccountID__c, ssi);
            }
            else if (ssi.LeadID__c != null) {
                lids.add(ssi.LeadID__c);
                ssiObjMap.put(ssi.LeadID__c, ssi);
            }
            
        }        
        
        //accounts = new List<Account>();
        for(Account a: [ SELECT Id, Name, DispositionCode__c, DispositionDate__c, SiteStreet__c, SiteCity__c, SitePostalCode__c, SitePostalCodeAddOn__c, Phone, DispositionDetail__c, Reason1ForSystem__c, Data_Source__c, Type, DisconnectReason__c, Business_ID__c, Repeat_Disposition__c
        				 FROM Account
        				 WHERE Id IN :aids ]) {
            StreetSheetItemMap.put(a.Id, new AccountProspectItem(a, ssiObjMap.get(a.Id)));
        }
        
        //leads = new List<Lead>();
        for(Lead l: [ SELECT Id, Name, DispositionCode__c, DispositionDate__c, SiteStreet__c, SiteCity__c, SitePostalCode__c, SitePostalCodeAddOn__c, Phone, DispositionDetail__c, LeadSource, Type__c, Company, Business_ID__c, Channel__c, Repeat_Disposition__c
            		  FROM Lead
            		  WHERE Id IN :lids ]) {
            StreetSheetItemMap.put(l.Id, new LeadProspectItem(l, ssiObjMap.get(l.Id)));
        }
    }

	//---------------------------------------------------------------------------------------------------
	//	Inner class used as base for encapsulating the prospect item
	//---------------------------------------------------------------------------------------------------	
	public abstract class ProspectItem implements Comparable
	{
    	public StreetSheetItem__c ssiObj 		{ get; set; }
	    public String 			  sortType		{ get; set; }
	    public String			  sortColumn	{ get; set; }
		public ProspectItem(StreetSheetItem__c ssiObjParam)
		{
			ssiObj = ssiObjParam;
			sortType = 'ASC';
			sortColumn = SORT_COLUMN.name.name();
		}
	    public Integer sortOrdinal {	    	
	    	get{
	    	   if( sortType == 'ASC' ) return 1;
	    	   return -1;
	    	}
	    }
		public virtual String getName()
		{
			return '';
		}
		public virtual String getId()
		{
			return '';
		}
		public virtual String getSiteStreet()
		{
			return '';
		}
		public virtual String getSiteCity()
		{
			return '';
		}
		public virtual String getSitePostalCode()
		{
			return '';
		}
		public virtual String getSitePostalCodeAddOn()
		{
			return '';
		}
		public virtual String getPhone()
		{
			return '';
		}
		public virtual String getDataSource()
		{
			return '';
		}
		public virtual String getDisconnectReason()
		{
			return '';
		}
		public virtual String getDispositionCode()
		{
			return '';
		}
		public virtual String getType()
		{
			return '';
		}
		public Integer compareTo(Object compareTo) 
		{
	       	ProspectItem compareToEmp = (ProspectItem)compareTo;
	       	
	       	// default
	       	Integer sortVal = -1;
	       	
	       	// compare by score if it has value
	       	if((ssiObj.Score__c!=null || compareToEmp.ssiObj.Score__c!=null) && ssiObj.Score__c > compareToEmp.ssiObj.Score__c){
	       		sortVal = 1;
	       	}
	       	// compare by sorting criteria if score is empty
	       	// same score values are sorted by user criteria
	       	else if(ssiObj.Score__c == null || compareToEmp.ssiObj.Score__c == null
	       	   || ( ssiObj.Score__c != null && compareToEmp.ssiObj.Score__c != null && ssiObj.Score__c == compareToEmp.ssiObj.Score__c)  ){
	       		// name
	       		if(sortColumn.equalsIgnoreCase( SORT_COLUMN.name.name() ) ){
			        if (getName() == compareToEmp.getName()) sortVal = 0;
			        else if (getName() > compareToEmp.getName()) sortVal = 1;	       			
	       		}
	       		// site
	       		else if(sortColumn.equalsIgnoreCase( SORT_COLUMN.site.name() ) ){
			        if (getSiteStreet() == compareToEmp.getSiteStreet()) sortVal = 0;
			        else if (getSiteStreet() > compareToEmp.getSiteStreet()) sortVal = 1;	       			
	       		}
	       		// city 
	       		else if(sortColumn.equalsIgnoreCase( SORT_COLUMN.city.name() ) ){
			        if (getSiteCity() == compareToEmp.getSiteCity()) sortVal = 0;
			        else if (getSiteCity() > compareToEmp.getSiteCity()) sortVal = 1;	       			
	       		}
	       		// postalcode
	       		else if(sortColumn.equalsIgnoreCase( SORT_COLUMN.postalcode.name() ) ){
			        if (getSitePostalCode() == compareToEmp.getSitePostalCode()) sortVal = 0;
			        else if (getSitePostalCode() > compareToEmp.getSitePostalCode()) sortVal = 1;	       			
	       		}
	       	}
	       	
	        return sortVal;
		}
	} 
	
	//----------------------------------------------------------------------------------------------------------
	//	Inner class extending the base prospectItem class to provide polymorphic access to Account object fields
	//----------------------------------------------------------------------------------------------------------	
	public class LeadProspectItem extends ProspectItem 
	{
		private Lead LeadObj { get; set; }
		public LeadProspectItem(Lead l, StreetSheetItem__c ssiObj)
		{
			super(ssiObj);
			LeadObj = l;
		}
		public override String getName()
		{
    		return LeadObj.name; 
		}
		public override String getId()
		{
			return LeadObj.Id;
		}
		public override String getSiteStreet()
		{
			return LeadObj.SiteStreet__c;
		}
		public override String getSiteCity()
		{
			return LeadObj.SiteCity__c;
		}
		public override String getSitePostalCode()
		{
			return LeadObj.SitePostalCode__c;
		}
		public override String getSitePostalCodeAddOn()
		{
			return LeadObj.SitePostalCodeAddOn__c;
		}
		public override String getPhone()
		{
			String phone = LeadObj.Phone;
			phone = ( !Utilities.isEmptyOrNull(phone) )?phone.replaceAll('[^0-9]+',''):''; 
			return phone;
		}
		public override String getDataSource()
		{
			return LeadObj.LeadSource;
		}
		public override String getDispositionCode()
		{
			return LeadObj.DispositionCode__c;
		}
		public override String getType()
		{
			return LeadObj.Type__c;
		}
	}
		
	//----------------------------------------------------------------------------------------------------------
	//	Inner class extending the base prospectItem class to provide polymorphic access to Lead object fields
	//----------------------------------------------------------------------------------------------------------	
	public class AccountProspectItem extends ProspectItem 
	{
		private Account AccountObj { get; set; }
		public AccountProspectItem(Account a, StreetSheetItem__c ssiObjParam)
		{
			super(ssiObjParam);
			AccountObj = a;
		}
		public override String getName()
		{
    		return AccountObj.name; 
		}
		public override String getId()
		{
			return AccountObj.Id;
		}
		public override String getSiteStreet()
		{
			return AccountObj.SiteStreet__c;
		}
		public override String getSiteCity()
		{
			return AccountObj.SiteCity__c;
		}
		public override String getSitePostalCode()
		{
			return AccountObj.SitePostalCode__c;
		}
		public override String getSitePostalCodeAddOn()
		{
			return AccountObj.SitePostalCodeAddOn__c;
		}
		public override String getPhone()
		{
			String phone = AccountObj.Phone;
			phone = ( !Utilities.isEmptyOrNull(phone) )?phone.replaceAll('[^0-9]+',''):''; 
			return phone;
		}
		public override String getDataSource()
		{
			return AccountObj.Data_Source__c;
		}
		public override String getDisconnectReason()
		{
			return AccountObj.DisconnectReason__c;
		}
		public override String getDispositionCode()
		{			
			return AccountObj.DispositionCode__c;
		}
		public override String getType()
		{
			return AccountObj.Type;
		}
	}
    
}