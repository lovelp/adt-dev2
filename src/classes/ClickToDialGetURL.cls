global class ClickToDialGetURL {

    webService static String getCTDURL() {
        CTClickToDialConfig__c mySetting = CTClickToDialConfig__c.getInstance();
        return mySetting.RESTURL__c;
    }
}