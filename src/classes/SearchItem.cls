/************************************* MODIFICATION LOG ********************************************************************************************
* SearchItem
*
* DESCRIPTION : Serves as a data structure for returned search results from Search Manager
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* 							 10/14/2011			- Original Version
*
*													
*/

public class SearchItem {
	
    public Account acct {get; set;}
    public StreetSheetItem__c ssItem {get;set;}
    public Lead lead {get;set;}
    public Boolean selected {get; set;} 
    public String formattedDiscoDate;
    public String formattedSortCode {get;set;}
    public String businessName;
    public String customerName;
    
    private static final String RESIDENTIAL = '1100';
    private static final String SMALL_BUSINESS = '1200';
    
    //result variables
    public String id {get;set;}
    public String name {get;set;}
    public String siteStreet {get;set;}
    public String siteCity {get;set;}
    public String siteState {get;set;}
    public String sitePostalCode {get;set;}
    public Date discoDate {get;set;}
    public String discoReason {get;set;}
    public String dataSource {get;set;}
    public String type {get;set;}
        
    //phase 2
    public String phone {get;set;}
    public String disposition {get;set;}

    
    public SearchItem(Account a, boolean isSelected){
    	acct = a;
        selected = isSelected;
       
        formattedSortCode+='<br/>';	                                           

		id = a.Id;
		name = a.Name;
		siteStreet = a.SiteStreet__c;
		siteCity = a.SiteCity__c;
		siteState = a.SiteState__c;
		sitePostalCode = (a.SitePostalCode__c != null ? a.SitePostalCode__c : '') + (a.SitePostalCodeAddOn__c != null ? a.SitePostalCodeAddOn__c : '');
		discoDate = a.DisconnectDate__c;
		discoReason = a.DisconnectReason__c;
		dataSource = a.Data_Source__c;
		phone = a.Phone;
		disposition = a.DispositionCode__c;
		type = a.Type;

	}
	
	public String getformattedDiscoDate(){
	                                              
       if (acct != null && acct.DisconnectDate__c != null) {
       		formattedDiscoDate = acct.DisconnectDate__c.format();
       }
       else if (ssItem != null && ssItem.DisconnectDate__c != null) {
       		formattedDiscoDate = ssItem.DisconnectDate__c.format();
       }
       else if (lead != null && lead.DisconnectDate__c != null) {
       		formattedDiscoDate = lead.DisconnectDate__c.format();
       }
       return formattedDiscoDate;
	}
	
	public String getFormattedActiveSite() {
		String formatted = 'No';
		
		if (acct != null && acct.InService__c != null) {
       		if (acct.InService__c) {
       			formatted = 'Yes';
       		}
       	}
       	else if (ssItem != null && ssItem.DisconnectDate__c != null) {
       		formatted = ssItem.InService__c;
       	}
       	return formatted;
	}
	
	public String getBusinessName() {
		
		if (acct != null && acct.Business_Id__c != null) {
			if (acct.Business_ID__c.contains(SMALL_BUSINESS)) {
				businessName = acct.Name;
			}
       		else {
       			businessName = '';
       		}
       }
       else if (ssItem != null && ssItem.Business_ID__c != null) {
       		if (ssItem.Business_ID__c.contains(SMALL_BUSINESS)) {
				businessName = ssItem.AccountName__c;
			}
       		else {
       			businessName = '';
       		}	
       }
       else if (lead != null && lead.Business_Id__c != null) {
       		if (lead.Business_Id__c.contains(SMALL_BUSINESS)) {
				businessName = lead.Company;
			}
       		else {
       			businessName = '';
       		}
       }
       return businessName;
		
	}
	
	public String getCustomerName() {
		
		if (acct != null && acct.Business_Id__c != null) {
			if (acct.Business_ID__c.contains(RESIDENTIAL)) {
				customerName = acct.Name;
			}
       		else {
       			customerName = '';
       		}
       }
       else if (ssItem != null && ssItem.Business_ID__c != null) {
       		if (ssItem.Business_ID__c.contains(RESIDENTIAL)) {
				customerName = ssItem.AccountName__c;
			}
       		else {
       			customerName = '';
       		}	
       }
       else if (lead != null && lead.Business_Id__c != null) {
       		if (lead.Business_Id__c.contains(RESIDENTIAL)) {
       			customerName = lead.FirstName + ' ' + lead.LastName;
       		} else {
       			customerName = '';
       		}
       }
       return customerName;
		
	}
	
	public SearchItem(StreetSheetItem__c item) {
		ssItem = item;
       
        formattedSortCode+='<br/>';
	}
	
	public SearchItem(Lead l, Boolean isSelected)
	{
		lead = l;
        selected = isSelected;
        formattedSortCode+='<br/>';

		id = l.Id;
		if(l.Channel__c != null && l.Channel__c.Contains('SB'))
		{
			name = l.Company;
		}
		else
		{
			name = l.Name;
		}
		siteStreet = l.SiteStreet__c;
		siteCity = l.SiteCity__c;
		siteState = l.SiteStateProvince__c;
		sitePostalCode = (l.SitePostalCode__c != null ? l.SitePostalCode__c : '') + (l.SitePostalCodeAddOn__c != null ? l.SitePostalCodeAddOn__c : '');
		dataSource = l.LeadSource;
		phone = l.Phone;
		disposition = l.DispositionCode__c;
		type = l.Type__c;
	}

}