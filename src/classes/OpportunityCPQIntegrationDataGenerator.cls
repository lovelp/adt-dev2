/************************************* MODIFICATION LOG ********************************************************************************************
* 
* OpportunityCPQIntegrationDataGenerator 
*
* DESCRIPTION :
* Generates the JSON String which contains the following objects data:
*
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE             Ticket         REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Abhinav Pandey                10/21/2019        HRM-10881      Added JSON details for Opportunity
*/
public class OpportunityCPQIntegrationDataGenerator {
    
    public class IntegrationDataSource{
        public opportunity opp   { get; set; }
        public String qOrderType {get;set;}
        
        public IntegrationDataSource(){
            // default use account owner
            //  this.useContextUser = false;
        } 
        
        
        public IntegrationDataSource( Id oppId,String quoteOrderType){
            this();
            this.opp = [SELECT Name, AccountId, HomeHealthPaytype__c, OppIntegrationData__c, BillingAddress__c, BillingCity__c, BillingCompoundAddress__c, BillingPostalCode__c, BillingState__c, BillingStreet__c, BillingStreet2__c, ShippingAddress__c, ShippingCity__c, ShippingPostalCode__c, ShippingState__c, ShippingStreet__c, ShippingStreet2__c  FROM Opportunity WHERE Id = :oppId];
            this.qOrderType= quoteOrderType;
        }
    }  
    
    private class CPQData{
        //class that needs to be serialized finally
        public ShippingData ShippingDetails;
        public BillingData BillingDetails;
        public LoanApplicationDetails LoanApplicationDetails; //HRm -10881 Adding loan application details
        public EquifaxData EquifaxData; // HRM-10881 Adding equifax Data
        public PType OrderDetails;  
    }
    
    //HRM-10881
    private class LoanApplicationDetails {
        public String PayName;
        public String PayCustRef;
        public String PayType;
        public String PayLast4;
        public String PayExpiry;
        public String ThirdPartyCustNbr;
        public String Finance;
        
    }
    
    //HRM-10881 Equifax Data
    //----------------------------------------------------------------------------------------
    //  Credit Check wrapper
    //----------------------------------------------------------------------------------------
    private class EquifaxData {
        public String RiskGrade;
        public String ApprovalType;
        public String PaymentFreq;
        public String DepositPercent;
        public String ThreePay;
        public String EasyPay;
        public String maximumAmountFinance;
        public String minimumAmountFinance;
    }
    
    
    Private class PType{
        public string Paytype;
    }
    
    private class ShippingData {
        public string ShippingAddress;
        public string ShippingCity;
        public string ShippingCode;
        public string ShippingState;
        public string ShippingStreet;
        public string ShippingStreet2;
        
    }
    
    private class BillingData {
        public string BillingAddress;
        public string BillingCity;
        public string BillingCompoundAddress;
        public string BillingPostalCode;
        public string BillingState;
        public string BillingStreet;
        public string BillingStreet2;
        
    }
    
    public static string getCPQIntegrationJSON( IntegrationDataSource iSource ){
        CPQData mainData = new CPQData(); //add all wrappers to mainData and convert it to JSON msg
        Opportunity opp = iSource.opp;
        Account acc = new Account();//HRM - 10881
        acc = Utilities.queryAccount(opp.accountId);//HRM - 10881
        String orderTypeValue = String.isNotBlank(iSource.qOrderType) ? iSource.qOrderType : acc.MMBOrderType__c;
        String jsonString = '';
        ShippingData shpData = new ShippingData();
        shpData.ShippingAddress = opp.ShippingAddress__c;
        shpData.ShippingCity = opp.ShippingCity__c;
        shpData.ShippingCode = opp.ShippingPostalCode__c;
        shpData.ShippingState = opp.ShippingState__c;
        shpData.ShippingStreet = opp.ShippingStreet__c;
        shpData.ShippingStreet2 = opp.ShippingStreet2__c;
        
        BillingData billData = new BillingData();
        billData.BillingAddress = opp.BillingAddress__c;
        billData.BillingCity = opp.BillingCity__c;
        billData.BillingCompoundAddress = opp.BillingCompoundAddress__c;
        billData.BillingPostalCode = opp.BillingPostalCode__c;
        billData.BillingState = opp.BillingState__c;
        billData.BillingStreet = opp.BillingStreet__c;
        billData.BillingStreet2 = opp.BillingStreet2__c;
        
        PType pty = new PType();
        pty.PayType = String.isNotBlank(opp.HomeHealthPaytype__c) ? opp.HomeHealthPaytype__c.deleteWhitespace() : '';
        
        
        mainData.ShippingDetails= shpData;
        mainData.BillingDetails = billData;
        mainData.OrderDetails= pty;
        //HRM-10881
        if(String.isNotBlank(acc.Business_Id__c) && acc.Business_Id__c.contains('1100')){
            EquifaxData ccInf = new EquifaxData();
            if(Utilities.checkIfCreditCheckIsObsolete(acc)){
                // HRM-6905 - Assigning Defaults if credit is expired
                Account tempAccount = Utilities.setCreditDefaults(acc);
                ccInf.RiskGrade = tempAccount.EquifaxRiskGrade__c.deleteWhitespace();
                ccInf.ApprovalType = tempAccount.EquifaxApprovalType__c.deleteWhitespace();
            }else{
                //Added by Jitendra
                //Check if Profile, Order type is not blank and Not ADT Employee
                if(String.isNotBlank(acc.Profile_RentOwn__c) && (String.isNotBlank(orderTypeValue)) && !acc.ADTEmployee__c){
                    String allowedOrderTypeForCreditCheckForOwners = Equifax__c.getinstance('Order Types For Credit Check') != null? Equifax__c.getinstance('Order Types For Credit Check').value__c:'';
                    String allowedOrderTypeForCreditCheckForRenters = Equifax__c.getinstance('Order Types For Credit Check - Renters') != null? Equifax__c.getinstance('Order Types For Credit Check - Renters').value__c:'';
                    String flexFiRiskGrades = Equifax__c.getinstance('FlexFiApprovalRiskGrade') != null? Equifax__c.getinstance('FlexFiApprovalRiskGrade').value__c : '';
                    // Check for allowed order types for Owners & Renters, should not be in FlexFiRiskApprovedGrade
                    if(String.isNotBlank(acc.EquifaxRiskGrade__c) && String.isNotBlank(orderTypeValue) && !flexFiRiskGrades.contains(acc.EquifaxRiskGrade__c) &&
                       Equifax__c.getinstance('Default Approved Condition Code') != Null &&
                       Equifax__c.getinstance('Default Approved Risk Grade') != Null &&
                       ((String.isNotBlank(allowedOrderTypeForCreditCheckForOwners) && acc.Profile_RentOwn__c == 'O' && !allowedOrderTypeForCreditCheckForOwners.contains(orderTypeValue )) ||
                        (String.isNotBlank(allowedOrderTypeForCreditCheckForRenters) && acc.Profile_RentOwn__c == 'R' && !allowedOrderTypeForCreditCheckForRenters.contains(orderTypeValue )))
                      ){
                          // Credit Check defaults in case of Q APPROV
                          ccInf.ApprovalType = Equifax__c.getinstance('Default Approved Condition Code').value__c != null ? Equifax__c.getinstance('Default Approved Condition Code').value__c:'';
                          ccInf.RiskGrade = Equifax__c.getinstance('Default Approved Risk Grade').value__c  != null ? Equifax__c.getinstance('Default Approved Risk Grade').value__c:'';
                      }
                }
                //Not a valid combination of Order type for Owners and renters.
                if(String.isBlank(ccInf.RiskGrade) && String.isBlank(ccInf.ApprovalType)){
                    // Credit Check fields from account
                    ccInf.RiskGrade = String.isNotBlank(acc.EquifaxRiskGrade__c) ? acc.EquifaxRiskGrade__c:'';
                    ccInf.ApprovalType = String.isNotBlank(acc.EquifaxApprovalType__c) ? acc.EquifaxApprovalType__c:'';
                }
                System.debug('### h6');
                //Added by Jitendra
            }
            
            if(String.isNotBlank(ccInf.ApprovalType) && Equifax_Conditions__c.getValues(ccInf.ApprovalType) != Null){
                ccInf.PaymentFreq = Equifax_Conditions__c.getValues(ccInf.ApprovalType).Payment_Frequency__c;
                ccInf.DepositPercent = String.ValueOf(Equifax_Conditions__c.getValues(ccInf.ApprovalType).Deposit_Required_Percentage__c);
                ccInf.ThreePay = (Equifax_Conditions__c.getValues(ccInf.ApprovalType).Three_Pay_Allowed__c)?'Y':'N';   
                ccInf.EasyPay = (Equifax_Conditions__c.getValues(ccInf.ApprovalType).EasyPay_Required__c)?'Y':'N';
            }
            System.debug('### h7');
            if(acc.ADTEmployee__c){
                ccInf.EasyPay = 'Y';
            }
            System.debug('### h8');
            ccInf.maximumAmountFinance =FlexFiConfig__c.getinstance('MaximumAmountFinance')!= null &&  String.isNotBlank(FlexFiConfig__c.getinstance('MaximumAmountFinance').Value__c) ? FlexFiConfig__c.getinstance('MaximumAmountFinance').Value__c: '4000';
            ccInf.minimumAmountFinance=FlexFiConfig__c.getinstance('MinimumAmountFinanceADT')!= null &&  String.isNotBlank(FlexFiConfig__c.getinstance('MinimumAmountFinanceADT').Value__c) ? FlexFiConfig__c.getinstance('MinimumAmountFinanceADT').Value__c: '200';
            if(String.isNotBlank(ccInf.RiskGrade) && String.isNotBlank(ccInf.ApprovalType)){
                mainData.EquifaxData = ccInf;
            }
            System.debug('### h9');
        }
        
        //HRM - 10881 
        list<LoanApplication__c> lstLoanApplication = new list<LoanApplication__c>();
        lstLoanApplication = [Select id,Name,CreditCardName__c,Account__c,Account__r.Equifax_Last_Check_DateTime__c,Account__r.EquifaxRiskGrade__c,CreditCardExpirationDate__c,DecisionStatus__c,CreditCardNumber__c,CardType__c,PaymentechProfileId__c,SubmittedDateTime__c from LoanApplication__c where Account__c=:opp.accountid and ActiveLoanApplication__c = true limit 1];
        if(lstLoanApplication != null && lstLoanApplication.size()>0 ){
            if(String.isNotBlank(lstLoanApplication[0].CreditCardName__c) || String.isNotBlank(lstLoanApplication[0].CreditCardExpirationDate__c) || String.isNotBlank(lstLoanApplication[0].CreditCardNumber__c) || String.isNotBlank(lstLoanApplication[0].CardType__c)){
                LoanApplicationDetails details = new LoanApplicationDetails();
                if(String.isNotBlank(lstLoanApplication[0].CreditCardName__c)){
                    details.PayName      = (lstLoanApplication[0].CreditCardName__c).replace(' ','_');
                }
                if(String.isNotBlank(lstLoanApplication[0].PaymentechProfileId__c)){
                    details.PayCustRef      = lstLoanApplication[0].PaymentechProfileId__c;
                }
                if(String.isNotBlank(lstLoanApplication[0].CardType__c)){
                    details.PayType         = lstLoanApplication[0].CardType__c;
                }
                if(String.isNotBlank(lstLoanApplication[0].CreditCardNumber__c)){
                    details.PayLast4        = lstLoanApplication[0].CreditCardNumber__c.right(4);
                }
                if(String.isNotBlank(lstLoanApplication[0].CreditCardExpirationDate__c)){
                    details.PayExpiry       = lstLoanApplication[0].CreditCardExpirationDate__c;
                }        
                String Eligibleriskgrade = Equifax__c.getinstance('FlexFiApprovalRiskGrade') != null ? Equifax__c.getinstance('FlexFiApprovalRiskGrade').value__c: '';
                //Active credit check,Active loaAplication with Approved status
                if(!Utilities.checkIfCreditCheckIsObsolete(acc) && lstLoanApplication[0].SubmittedDateTime__c !=null && String.isNotBlank(Eligibleriskgrade) && Eligibleriskgrade.contains(lstLoanApplication[0].Account__r.EquifaxRiskGrade__c) && (FlexFiConfig__c.getinstance('CFG Disabled') != null && String.isNotBlank(FlexFiConfig__c.getinstance('CFG Disabled').Value__c)) && FlexFiConfig__c.getinstance('CFG Disabled').value__c.equalsIgnoreCase('false')){                  
                    if(lstLoanApplication[0].DecisionStatus__c == 'APPROVED' && FlexFiConfig__c.getinstance('ThirdPartyCustomerNumber') != null && String.isNotBlank(FlexFiConfig__c.getinstance('ThirdPartyCustomerNumber').Value__c)){            
                        details.ThirdPartyCustNbr = FlexFiConfig__c.getinstance('ThirdPartyCustomerNumber').Value__c;
                    }
                    details.Finance = 'Y';
                }
                //What if the customer is in paid status and later the flip of the switcch haappened before submission. Should we take below logic or previous logic?
                if((FlexFiConfig__c.getinstance('CFG Disabled') != null && String.isNotBlank(FlexFiConfig__c.getinstance('CFG Disabled').Value__c)) && FlexFiConfig__c.getinstance('CFG Disabled').value__c.equalsIgnoreCase('true')){                   
                    String financeAllowed = '';
                    LoanApplicationDataAccess loanObj = new LoanApplicationDataAccess();
                    financeAllowed = loanObj.getLoanApplicationAllowedMessage(acc,true,orderTypeValue);//HRM-10881 added boolean true
                    if(financeAllowed.equalsIgnoreCase('false')){
                        details.Finance ='Y';
                    }
                }
                mainData.LoanApplicationDetails= details;
            }
        } 
        
        else {
            if((FlexFiConfig__c.getinstance('CFG Disabled') != null && String.isNotBlank(FlexFiConfig__c.getinstance('CFG Disabled').Value__c)) && FlexFiConfig__c.getinstance('CFG Disabled').value__c.equalsIgnoreCase('true')){
                LoanApplicationDetails details = new LoanApplicationDetails();
                String Eligibleriskgrade = Equifax__c.getinstance('FlexFiApprovalRiskGrade') != null ? Equifax__c.getinstance('FlexFiApprovalRiskGrade').value__c: '';
                //Active credit check,Active loaAplication with Approved status
                String financeAllowed = '';
                LoanApplicationDataAccess loanObj = new LoanApplicationDataAccess();
                financeAllowed = loanObj.getLoanApplicationAllowedMessage(acc,true,orderTypeValue);//HRM-10881 added boolean true
                if(financeAllowed.equalsIgnoreCase('false')){
                    details.Finance ='Y';
                }
                mainData.LoanApplicationDetails= details;
            }
        }
        
        
        
        if (mainData != null){
            jsonString = convertDataToJSON(mainData); 
        } 
        
        system.debug('jsonmsg**'+jsonString);
        
        return CPQIntegrationDataGenerator.stripJsonNulls(jsonString);
    }
    
    private static string convertDataToJSON(CPQData data){
        return JSON.Serialize(data,true);        
    }
    
}