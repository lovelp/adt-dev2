/************************************* MODIFICATION LOG ********************************************************************************************
* ShowError
* 
DESCRIPTION : Obtains error message information from custom settings for display on a dedicated error page.
*             It is a Controller in the SFDC MVC architecture. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*               				10/14/2011			- Original Version
*
*													  
*													
*/

public with sharing class ShowError {
	public ErrorMessages__c ErrorMessage {get; set;}
	public ShowError()
	{
		String ErrorId = ApexPages.currentPage().getParameters().get('Error');
		ErrorMessage = ErrorMessages__c.getInstance(ErrorId);
	}
}