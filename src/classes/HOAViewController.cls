/************************************* MODIFICATION LOG ********************************************************************************************
* HOAViewController
*
* DESCRIPTION : Controller class for the HOA Dashboard
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel herrera             	09/24/2014			- Original Version
*
*													
*/

public with sharing class HOAViewController {
	
	public String 			objID 					{ get; set; }
	public String 			userPic 				{ get; set; }
	public String 			userName 				{ get; set; }
	public HOA 				HOAAssociation 			{ get; set; }
	public Boolean 			initError 				{ get; set; }
	public String 			initErrorMsg 			{ get; set; }
	public HOA				currentHOACustomerSite 	{ get; set; }
	 
	public Boolean renderCustomerSite {
		get{
			return currentHOACustomerSite != null;
		}
	}
	
	public Boolean HOAAssociationIsGeocoded {
		get{
			return HOAAssociation.hasGeoLocation;
		}
	}	
	
	/**
	 * @Constructor
	 */
	public HOAViewController(){
		objID = Apexpages.CurrentPage().getParameters().get('Id');
		userPic = ConnectApi.ChatterUsers.getPhoto(null, Userinfo.getUserId()).smallPhotoUrl;
		userName = UserInfo.getName();
		
		try{
			HOAAssociation = new HOA(objID);
			initError = false;
		}
		catch(Exception err){
			initErrorMsg = err.getMessage();
			initError = true;
            // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,''));
		}		
	}
	
	@RemoteAction
	public static List<HOA> getHOAAssociationSiteList( String hoaId){
		List<HOA> resultList = new List<HOA>();

		if(hoaId.startsWith(HOA.AccPrefix)){
			for(Account a: [SELECT Name, AddressID__c, Longitude__c, Latitude__c, MMBCustomerNumber__c, MMBSiteNumber__c, MMB_TownID__c, ResaleRegion__c, ResaleDistrict__c, ResaleTown__c
						 	FROM Account 
						 	WHERE HOA_Association__c = :hoaId]){
				resultList.add( new HOA(a) );
			}
		}
		/** Removed from requirements - enable if hoa lead enhanced list views are supported
		else if(hoaId.startsWith(HOA.LeadPrefix)){
			for(Lead l: [SELECT Name, AddressID__c, Longitude__c, Latitude__c, MMBCustomerNumber__c, MMBSiteNumber__c, Town__c, Region__c, District__c 
						 FROM Lead 
						 WHERE HOA_Association__c = :hoaId]){
				resultList.add( new HOA(l) );
			}
		}
		else{
			throw new HOA.HOAException('Id of parent HOA not currently supported. Only Account or Lead.' );
		}
		**/
		else{
			throw new HOA.HOAException('Id of parent HOA not currently supported. Only Account.' );
		}
		
		return resultList;
	}
	
	public PageReference clearSiteDetailInfo(){
		currentHOACustomerSite = null;
		return null;
	}
	
	public PageReference HOASiteDetail(){
		
		String hoaSiteCustomerId = Apexpages.currentPage().getParameters().get('hoaSiteCustomerId');
		if( Utilities.isEmptyOrNull(hoaSiteCustomerId) ){
			throw new HOA.HOAException('Expected site Id received empty.' );
		}
		
		currentHOACustomerSite = new HOA( hoaSiteCustomerId );
		
		return null;
	}
	
}