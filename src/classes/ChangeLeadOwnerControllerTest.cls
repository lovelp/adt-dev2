@isTest
private class ChangeLeadOwnerControllerTest {

    static testMethod void ChangeLeadOwner() {
        Test.startTest();
        User salesRep = TestHelperClass.createSalesRepUserWithManager();
        UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
        User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId and isactive=true Limit 1];
        User adminUser = TestHelperClass.createAdminUser();
        List<Lead> leads = new List<Lead>();
        
        ApexPages.PageReference ref = new PageReference('/apex/ChangeLeadOwner');
        Test.setCurrentPageReference(ref);
        
        System.runAs(adminUser) {
            Lead l = TestHelperClass.createLead(ManagerUSer);
            leads.add(l);
        }
        
        ApexPages.Standardsetcontroller sc = new ApexPages.Standardsetcontroller(leads);
        sc.setSelected(leads);
        System.runAs(ManagerUser) {

            ChangeLeadOwnerController caet = new ChangeLeadOwnerController(sc);
            caet.Validate();
            caet.getAllSalesReps();
            caet.setSelectedSalesRep(salesRep.Id);
            caet.getSelectedSalesRep();
            caet.save();
            Lead led = [select ownerId from Lead where id=: leads[0].id];
            System.assertEquals(led.OwnerId, caet.getSelectedSalesRep(), 'Ownership changed');
        }       
        Test.stopTest();        
    }
    
    static testMethod void ChangeLeadOwnerNoSalesRep() {
        Test.startTest();
        User salesRep = TestHelperClass.createSalesRepUserWithManager();
        UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
        User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId and isactive=true Limit 1];
        User adminUser = TestHelperClass.createAdminUser();
        List<Lead> leads = new List<Lead>();
        
        ApexPages.PageReference ref = new PageReference('/apex/ChangeLeadOwner');
        Test.setCurrentPageReference(ref);
        
        System.runAs(adminUser) {
            Lead a = TestHelperClass.createLead(ManagerUser);
            leads.add(a);
        }
        
        ApexPages.Standardsetcontroller sc = new ApexPages.Standardsetcontroller(leads);
        sc.setSelected(leads);
        System.runAs(ManagerUser) {
            ChangeLeadOwnerController caet = new ChangeLeadOwnerController(sc);
            caet.save();
            System.assertEquals(ApexPages.getMessages().size(), 1, 'Failure because sales rep not selected. Expected.');
        }       
        
        Test.stopTest();        
    }
    
    static testMethod void RunLeadAssignmentTest() {
        Test.startTest();
        User salesRep = TestHelperClass.createSalesRepUserWithManager();
        UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
        User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId and isactive=true Limit 1];
        User adminUser = TestHelperClass.createAdminUser();
        list<Lead> leads = new list<Lead>();
        
        ApexPages.PageReference ref = new PageReference('/apex/ChangeLeadOwner');
        Test.setCurrentPageReference(ref);
        
        System.runAs(adminUser) {
            Lead l1 = TestHelperClass.createLead(ManagerUSer);
            Lead l2 = TestHelperClass.createLead(ManagerUSer);
            l2.Affiliation__c = 'Builder';
            leads.add(l1);
            leads.add(l2);
        }
        
        ApexPages.Standardsetcontroller sc = new ApexPages.Standardsetcontroller(leads);
        sc.setSelected(leads);
        System.runAs(ManagerUser) {
            ChangeLeadOwnerController caet = new ChangeLeadOwnerController(sc);
            caet.onLoad();
            caet.getListSizeForFalse();
            caet.getListSizeForTrue();
        }       
        Test.stopTest();        
    }
    
    static testMethod void RunLeadAssignmentFromTriggerTest() {
        Test.startTest();
        User salesRep = TestHelperClass.createSalesRepUser();
        User salesManager = TestHelperClass.createManagerUser();
        User adminUser = TestHelperClass.createAdminUser();
        adminUser.Business_Unit__c = 'Business Direct Sales';
        update adminUser;
        
        list<Lead> leads = new list<Lead>();
        
        System.runAs(adminUser) {
            String postalId = TestHelperClass.inferPostalCodeID('22102', '1300');
            Lead l1 = new Lead();
            l1.LastName = 'TestLastName1';
            l1.Company = 'TestCompany1';
            l1.LeadSource = 'Manual Import';
            l1.Business_Id__c = Channels.TYPE_COMMERCIAL;
            l1.Channel__c = Channels.TYPE_BUSINESSDIRECTSALES;
            l1.SiteStreet__c = '8952 Brook Rd';
            l1.SiteCity__c = 'McLean';       
            l1.SiteStateProvince__c = 'VA';
            l1.SiteCountryCode__c = 'US';
            l1.SitePostalCode__c = '22102';
            l1.PostalCodeID__c = postalId;
            l1.Affiliation__c = 'Builder';
            leads.add(l1);
            
            String postalId2 = TestHelperClass.inferPostalCodeID('33445', '1300');
            
            TerritoryAssignment__c ta = new TerritoryAssignment__c();
            ta.OwnerId = salesRep.Id;
            ta.PostalCodeId__c = postalId2;
            ta.TerritoryType__c = Channels.TERRITORYTYPE_BUILDER;
            insert ta;
            
            Lead l2 = new Lead();
            l2.LastName = 'TestLastName2';
            l2.Company = 'TestCompany2';
            l2.LeadSource = 'Manual Import';
            l2.Business_Id__c = Channels.TYPE_COMMERCIAL;
            l2.Channel__c = Channels.TYPE_BUSINESSDIRECTSALES;
            l2.SiteStreet__c = '4868 N Citation Dr';
            l2.SiteCity__c = 'Delray Beach';       
            l2.SiteStateProvince__c = 'FL';
            l2.SiteCountryCode__c = 'US';
            l2.SitePostalCode__c = '33445';
            l2.PostalCodeID__c = postalId2;
            l2.Affiliation__c = 'Builder';
            l2.ownerUsername__c = 'unitTestSalesRep1@testorg.com';
            leads.add(l2);
            
            Lead l3 = new Lead();
            l3.LastName = 'TestLastName2';
            l3.Company = 'TestCompany2';
            l3.LeadSource = 'Manual Import';
            l3.Business_Id__c = Channels.TYPE_COMMERCIAL;
            l3.Channel__c = Channels.TYPE_BUSINESSDIRECTSALES;
            l3.SiteStreet__c = '4868 N Citation Dr';
            l3.SiteCity__c = 'Delray Beach';       
            l3.SiteStateProvince__c = 'FL';
            l3.SiteCountryCode__c = 'US';
            l3.SitePostalCode__c = '33445';
            l3.PostalCodeID__c = postalId2;
            leads.add(l3);
            
            String postalId3 = TestHelperClass.inferPostalCodeID('33431', '1300');
            
            ManagerTown__c mt = new ManagerTown__c();
            mt.ManagerID__c = salesManager.Id;
            mt.TownID__c = 'TestTNID';
            mt.Type__c = Channels.TYPE_BUSINESSDIRECTSALES;
            insert mt;
            
            Lead l4 = new Lead();
            l4.LastName = 'TestLastName3';
            l4.Company = 'TestCompany3';
            l4.LeadSource = 'Manual Import';
            l4.Business_Id__c = Channels.TYPE_COMMERCIAL;
            l4.Channel__c = Channels.TYPE_BUSINESSDIRECTSALES;
            l4.SiteStreet__c = '1501 Yamato Rd';
            l4.SiteCity__c = 'Boca Raton';       
            l4.SiteStateProvince__c = 'FL';
            l4.SiteCountryCode__c = 'US';
            l4.SitePostalCode__c = '33431';
            l4.PostalCodeID__c = postalId3;
            leads.add(l4);
            
            insert leads;
        }
        Test.stopTest();
    }
}