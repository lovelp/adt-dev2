/************************************* MODIFICATION LOG ********************************************************************************************
* RequestQueueBatchProcessor
*
* DESCRIPTION : Responsible for processing a single Request Queue record by constructing the appropriate OutgoingMessage subclass 
*               and having it perform the necessary HTTPS callout.  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner                	03/05/2012              - Original Version
* Siddarth Asokan				02/22/2019				- Removed Webleads requests from the batch query
*/

global class RequestQueueBatchProcessor implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    /*
    * Uses a SOQL query defined as a public String member variable of the class in order to increase testability
    */
    global Database.QueryLocator start( Database.Batchablecontext bc){
    	Integer maxCount = Integer.valueOf(IntegrationSettings__c.getInstance().RequestQueueMaxErrors__c);
    	String query = 'Select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate,EventID__c,Counter__c From RequestQueue__c Where';
    	query += ' RequestStatus__c = \'Ready\' And (Counter__c = null OR (Counter__c != null AND Counter__c <=' + maxCount +'))';
    	query += ' And ServiceTransactionType__c NOT IN (\'sendEBRRequest\', \'sendACKEmail\', \'sendToOneClick\') Order By RequestDate__c';
        return Database.getQueryLocator(query);
    }
    
    /*
    * Before initiating a callout, verifies that there is no other message which should be transmitted first
    */
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        for(RequestQueue__c rq : (List<RequestQueue__c>)scope){
            try{               
                // check if an existing, older request record with a matching account id is waiting to be processed
                // can query in loop because a scope of 1 is necessary for callout, no gov limits will be hit
                Boolean doCallout = true;
                if (rq.AccountID__c != null) {
                    List<RequestQueue__c> rqold = new List<RequestQueue__c>([
                        select Id
                        from RequestQueue__c
                        where AccountID__c = :rq.AccountID__c
                        and RequestStatus__c = :IntegrationConstants.REQUEST_STATUS_READY
                        and CreatedDate < :rq.CreatedDate
                    ]);
                    doCallout = rqold.size() == 0;
                }
                
                if (doCallout) {
                    OutgoingMessage om = OutgoingMessageFactory.createOutgoingMessage(rq.ServiceTransactionType__c);
                    om.ServiceMessage = rq.ServiceMessage__c;
                    om.EventID = rq.EventID__c;
                    om.process();
                    if (om.Status != null && om.Status.equalsIgnoreCase('error')){
                        // request was sent and the response received but with some kind of error so mark as such
                        rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
                    }else{
                        // request was sent and the response received so mark as success
                        rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_SUCCESS;
                    }
                }
            }
            catch (CalloutException ce){
                String errMsg = ce.getMessage();
                system.debug('CalloutException: ' + errMsg);
                // this could be a failure for technical reasons like timeout which should be marked ready to try again
                rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
                // but it could also be a SOAP fault which should be marked as an error
                if (errMsg.contains('Unexpected element.') || errMsg.contains('SalesMobility/:Fault')){
                    rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
                }
                rq.ErrorDetail__c = ce.getMessage();
                if(rq.Counter__c == null) rq.Counter__c = 0;
                rq.Counter__c += 1;
            }
            catch (IntegrationException ie){
                system.debug('IntegrationException: ' + ie.getMessage());
                // either an invalid status or message where status = FAIL so mark as an error
                rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
                rq.ErrorDetail__c = ie.getMessage();
                if(rq.Counter__c == null) rq.Counter__c = 0;
                rq.Counter__c += 1;
            }
            catch (Exception e){
                system.debug('Exception: ' + e.getMessage());
                // some other exception so mark as an error
                rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
                rq.ErrorDetail__c = e.getMessage();
                if(rq.Counter__c == null) rq.Counter__c = 0;
                rq.Counter__c += 1;
            }
        }
        update scope;
    }
    
    global void finish(Database.BatchableContext bc){

    }
}