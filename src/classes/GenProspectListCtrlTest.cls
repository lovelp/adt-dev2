/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class GenProspectListCtrlTest {
    
    static testMethod void testZipCodeList(){
        User u;
        // Create a sales rep user for testing
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {        
          u = TestHelperClass.createSalesRepUser();
        }
        
        system.runas(u) {

            PageReference pRef = Page.GenProspectList;
            Test.setCurrentPage(pRef);          
            
            GenProspectListCtrl genCtrlObj = new GenProspectListCtrl();
            genCtrlObj.listType = 'Field';
            genCtrlObj.GenerateProspects();
            
            // Location
            ApexPages.currentPage().getParameters().put('rLat','38.94686000000000');
            ApexPages.currentPage().getParameters().put('rLon','-77.25470100000000');        
            genCtrlObj.GenerateProspects();

        }
            
    } 
    
    static testMethod void testFieldListNewRep() {
        
        Event e;
        User u;
        Account a, a1;
        
        //Create Addresses first
        Address__c addr = new Address__c( Latitude__c           = 38.925924,
                                          Longitude__c          = -77.229562, 
                                          Street__c             = '1645 International Dr',
                                          City__c               = 'McLean',
                                          State__c              = 'VA',
                                          PostalCode__c         = '221o2',
                                          OriginalAddress__c    = 'test address' );
        insert addr;
        a1 = new Account(   AddressID__c        = addr.Id,
                            Name                = 'ProspectListTest',
                            LeadStatus__c       = 'Active',
                            UnassignedLead__c   = false,
                            InService__c        = true,
                            Business_Id__c      = '1100 - Residential',
                            PostalCodeID__c     = TestHelperClass.inferPostalCodeID(addr.PostalCode__c, '1100'),
                            DispositionCode__c  = '',
                            ShippingPostalCode  = '221o2',
                            OwnerId             = UserInfo.getUserId(),
                            Data_Source__c      = 'TEST' );
        insert a1;
        
        // Create a sales rep user for testing
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {        
          u = TestHelperClass.createSalesRepUser();

        }

        RecordType rt = Utilities.getRecordType(RecordTypeName.INSTALL_APPOINTMENT, 'Event');
        Date todayRun = Date.today().addDays(1); 
        RecordType rt1 = Utilities.getRecordType(RecordTypeName.SELF_GENERATED_APPOINTMENT, 'Event');
        Event reassignEvent = new Event( StartDateTime  = Datetime.newInstance(todayRun.year(), todayRun.month(), todayRun.day(), 15, 0, 0),
                                         EndDateTime    = Datetime.newInstance(todayRun.year(), todayRun.month(), todayRun.day(), 16, 0, 0),
                                         Subject        = 'Test',
                                         WhatId         = a1.Id,
                                         OwnerId        = u.Id,
                                         RecordTypeId   = rt.Id );
        insert reassignEvent;
            
        system.runas(u) {
            
            a = TestHelperClass.createAccountData();
            //morning schedule
            List<Event> eList = new List<Event>(); 
            eList.add(new Event( StartDateTime  = Datetime.newInstance(todayRun.year(), todayRun.month(), todayRun.day(), 10, 0, 0),
                                 EndDateTime    = Datetime.newInstance(todayRun.year(), todayRun.month(), todayRun.day(), 11, 0, 0),
                                 Subject        = 'Test morning appt',
                                 WhatId         = a.Id,
                                 OwnerId        = u.Id,
                                 RecordTypeId   = rt.Id ) );
            eList.add(new Event( StartDateTime  = Datetime.newInstance(todayRun.year(), todayRun.month(), todayRun.day(), 9, 30, 0),
                                 EndDateTime    = Datetime.newInstance(todayRun.year(), todayRun.month(), todayRun.day(), 10, 0, 0),
                                 Subject        = 'Test morning appt',
                                 WhatId         = a.Id,
                                 OwnerId        = u.Id,
                                 RecordTypeId   = rt1.Id ) );
            eList.add(new Event( StartDateTime  = Datetime.newInstance(todayRun.year(), todayRun.month(), todayRun.day(), 15, 0, 0),
                                 EndDateTime    = Datetime.newInstance(todayRun.year(), todayRun.month(), todayRun.day(), 16, 0, 0),
                                 Subject        = 'Test',
                                 WhatId         = a.Id,
                                 OwnerId        = u.Id,
                                 RecordTypeId   = rt.Id ) );
            insert eList;
            
            PageReference pRef = Page.GenProspectList;
            Test.setCurrentPage(pRef);
            
            GenProspectListCtrl genCtrlObj = new GenProspectListCtrl();
            PageReference p1 = genCtrlObj.evalProspectList();
            
            // Location
            ApexPages.currentPage().getParameters().put('rLat','38.94686000000000');
            ApexPages.currentPage().getParameters().put('rLon','-77.25470100000000');        
            
            // Field List
            genCtrlObj.listType = 'Field';
            genCtrlObj.GenerateProspects(); //AM
            genCtrlObj.meridianSchedule = 'PM';
            genCtrlObj.GenerateProspects(); //PM

        }
    }    
        
    static testMethod void testPhoneListResiRep() {
        
        User u;
        Account a;

        // Create a sales rep user for testing
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {        
          u = TestHelperClass.createSalesRepUser();
          u.Business_Unit__c = 'Resi Resale';
          update u;
        }

        system.runas(u) {
            
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
            insert addr;
            
            List<Account> accList = new List<Account>(); 
            
            // Condition #1         
            Account acct = new Account();
            acct.Type = 'Discontinuance';
            acct.Phone = '1231231234';
            acct.AddressID__c = addr.Id;
            acct.Name = TestHelperClass.ACCOUNT_NAME;
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            acct.PostalCodeID__c = TestHelperClass.inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
            acct.DispositionCode__c = '';
            acct.ShippingPostalCode = '221o2';
            acct.Data_Source__c = 'TEST';
            acct.OwnerId = u.Id;
            acct.DisconnectDate__c = Date.today().addDays(-10);
            accList.add( acct );            
            
            // Condition #2         
            Account acct1 = new Account();
            acct1.Type = 'New Mover';
            acct1.Phone = '1231231834';
            acct1.AddressID__c = addr.Id;
            acct1.Name = TestHelperClass.ACCOUNT_NAME + '2';
            acct1.LeadStatus__c = 'Active';
            acct1.UnassignedLead__c = false;
            acct1.InService__c = true;
            acct1.Business_Id__c = '1100 - Residential';
            acct1.PostalCodeID__c = TestHelperClass.inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
            acct1.DispositionCode__c = '';
            acct1.ShippingPostalCode = '221o2';
            acct1.Data_Source__c = 'TEST';
            acct1.OwnerId = u.Id;
            acct1.NewMoverDate__c = Date.today().addDays(-10);
            accList.add( acct1 );           
            
            // Condition #3         
            Account acct2 = new Account();
            acct2.Type = 'New Mover';
            acct2.Phone = '1231291834';
            acct2.AddressID__c = addr.Id;
            acct2.Name = TestHelperClass.ACCOUNT_NAME + '3';
            acct2.LeadStatus__c = 'Active';
            acct2.UnassignedLead__c = false;
            acct2.InService__c = true;
            acct2.Business_Id__c = '1100 - Residential';
            acct2.PostalCodeID__c = TestHelperClass.inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
            acct2.DispositionCode__c = '';
            acct2.ShippingPostalCode = '221o2';
            acct2.Data_Source__c = 'TEST';
            acct2.OwnerId = u.Id;
            acct2.NewMoverDate__c = Date.today().addDays(-100);
            acct2.DateAssigned__c = system.today();
            acct2.NewLead__c = true;
            acct2.PreviousOwner__c = acct2.id ;
            accList.add( acct2 );
                        
            insert accList;
            
            PageReference pRef = Page.GenProspectList;
            Test.setCurrentPage(pRef);
            
            GenProspectListCtrl genCtrlObj = new GenProspectListCtrl();
            
            // Phone List
            genCtrlObj.listType = 'Phone';
            genCtrlObj.GenerateProspects();
            
        }
    }
    
    static testMethod void testPhoneListNewSalesRep() {
    
        User u;

        // Create a sales rep user for testing
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
          u = TestHelperClass.createSalesRepUser();
          u.Business_Unit__c = 'Resi Direct Sales';
          update u;
        }

        system.runas(u) {
            
            List<Lead> lList = new List<Lead>();
            
            String Id = TestHelperClass.inferPostalCodeID('221o2', '1100');
            String Source = 'apex test'; 
            
            // Condition #1
            Lead l = new Lead();
            l.LastName = 'TestLastName';
            l.Type__c = 'Pre Mover';
            l.Company = 'TestCompany';
            l.Phone = '1233213567';
            l.DispositionDetail__c = '';
            l.OwnerId = u.Id;
            l.LeadSource = Source != '' ? Source : null;
            l.SGCOM3__SG_infoGroupId__c = l.LeadSource == 'Salesgenie.com' ? '12345' : null;
            l.DispositionCode__c = '';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '8952 Brook Rd';
            l.SiteCity__c = 'McLean';       
            l.SiteStateProvince__c = 'VA';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '22102';
            l.PostalCodeID__c = null;
            l.AddressID__c = null;
            l.Business_Id__c = null;            
            lList.add(l);
            
            // Condition #2
            Lead l1 = new Lead();
            l1.LastName = 'TestLastName1';
            l1.Type__c = 'New Mover';
            l1.DispositionDetail__c = '';
            l1.Company = 'TestCompany';
            l1.NewMoverDate__c = Date.today().addDays(-10);
            l1.OwnerId = u.Id;
            l1.Phone = '1233213867';
            l1.LeadSource = Source != '' ? Source : null;
            l1.SGCOM3__SG_infoGroupId__c = l.LeadSource == 'Salesgenie.com' ? '12345' : null;
            l1.DispositionCode__c = '';
            l1.Channel__c = 'Resi Direct Sales';
            l1.SiteStreet__c = '8952 Brook Rd';
            l1.SiteCity__c = 'McLean';       
            l1.SiteStateProvince__c = 'VA';
            l1.SiteCountryCode__c = 'US';
            l1.SitePostalCode__c = '22102';
            l1.PostalCodeID__c = null;
            l1.AddressID__c = null;
            l1.Business_Id__c = null;           
            lList.add(l1);
            
            // Condition #3
            Lead l2 = new Lead();
            l2.LastName = 'TestLastName2';
            l2.DispositionDetail__c = '';
            l2.Company = 'TestCompany';
            l2.NewMoverDate__c = Date.today().addDays(-100);
            l2.OwnerId = u.Id;
            l2.Phone = '1233213867';
            l2.LeadSource = Source != '' ? Source : null;
            l2.SGCOM3__SG_infoGroupId__c = l.LeadSource == 'Salesgenie.com' ? '12345' : null;
            l2.DispositionCode__c = '';
            l2.Channel__c = 'Resi Direct Sales';
            l2.SiteStreet__c = '8952 Brook Rd';
            l2.SiteCity__c = 'McLean';       
            l2.SiteStateProvince__c = 'VA';
            l2.SiteCountryCode__c = 'US';
            l2.SitePostalCode__c = '22102';
            l2.PostalCodeID__c = null;
            l2.AddressID__c = null;
            l2.Business_Id__c = null;           
            lList.add(l2);
            
            insert lList;
            
            PageReference pRef = Page.GenProspectList;
            Test.setCurrentPage(pRef);
            
            GenProspectListCtrl genCtrlObj = new GenProspectListCtrl();
            
            // Phone List
            genCtrlObj.listType = 'Phone';
            genCtrlObj.GenerateProspects();
        }
        
    }
    
}