/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : TermsAndConditionsReset is a batch class to reset all of the already accepted Terms and Conditions.
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Erin McGee      07/08/2013    - Original Version
*
*                           
*/
global class TermsAndConditionsReset implements Database.Batchable<sObject> {
	
	public String query='Select Id, Terms_and_Conditions_Accepted__c From User Where Terms_and_Conditions_Accepted__c!=null';
	
	global TermsAndConditionsReset() 
	{
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) 
	{
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) 
   	{
   		List<User> updateUsers=new List<User>();
		for(User u:(List<User>)(scope))
		{
			u.Terms_and_Conditions_Accepted__c=null;
			updateUsers.add(u);
		}

		if(!updateUsers.isEmpty())
		{
			update updateUsers;
		}
	}
	
	global void finish(Database.BatchableContext BC) 
	{
		
	}
	
}