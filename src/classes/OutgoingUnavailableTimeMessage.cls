/************************************* MODIFICATION LOG ********************************************************************************************
* OutgoingUnavailableTimeMessage
*
* DESCRIPTION : Creates and processes setUnavailableTime messages for Telemar Interface
*				Extends OutgoingMessage with message specific logic
*				Delegates to ADTSalesMobilityFacade (wsdl2apex code)
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					1/19/2012				- Original Version
*											
*/
public with sharing virtual class OutgoingUnavailableTimeMessage extends OutgoingMessage {
	
	public User u {get;set;}
	public Event e {get;set;}
	
	private ADTSalesMobilityFacade.setUnavailableTimeRequest_element request;
	private ADTSalesMobilityFacade.setUnavailableTimeResponse_element response;
	
	public OutgoingUnavailableTimeMessage()
	{
		this.ServiceName = 'setUnavailableTime';
	}

	
	public override String createRequest()
	{
		User userPerformingAction = [Select Id, EmployeeNumber__c, TimeZoneSidKey from User where ID = :UserInfo.getUserId()];
		User userBeingScheduled = u;

		request = new ADTSalesMobilityFacade.setUnavailableTimeRequest_element();
		if (TransactionType == IntegrationConstants.TRANSACTION_CANCEL) {
			request.MessageID = getMessageId(e.WhoId);
		} else {
			request.MessageID = getMessageId(u.Id);
		}	
		request.TransactionType = this.TransactionType;
		request.TelemarScheduleID = (e.ScheduleID__c == null ? '' : e.ScheduleID__c);
		request.UserHREmployeeID = (userPerformingAction == null || userPerformingAction.EmployeeNumber__c == null ? '' : userPerformingAction.EmployeeNumber__c);
		
		if (TransactionType != IntegrationConstants.TRANSACTION_CANCEL)
		{
			String timeZoneOffset = '';
			// if the user performing the action is the same as the user being scheduled
			if (userPerformingAction != null && userPerformingAction.Id == userBeingScheduled.Id) {
				// get the current user's offset as of the time of the event
				// this handles time changes between daylight savings and standard time
				timeZoneOffset = e.StartDateTime.format('Z');
			} else {
				if (userBeingScheduled.TimeZoneSidKey == userPerformingAction.TimeZoneSidKey) {
					// no difference in time zone so get the current user's offset as of the time of the event
					// this handles time changes between daylight savings and standard time
					timeZoneOffset = e.StartDateTime.format('Z');
				} else {
					// otherwise, use the user's current time zone identifier and the scheduled date to derive the offset
					timeZoneOffset = EventUtilities.deriveUserTimeZoneOffsetForDate(e.StartDateTime, userBeingScheduled.TimeZoneSidKey);
				}
			
			}
			
			request.HREmployeeID = (userBeingScheduled == null || userBeingScheduled.EmployeeNumber__c == null ? '' : userBeingScheduled.EmployeeNumber__c);
			request.ScheduleDateTime = e.StartDateTime;
			request.ScheduleOffsetToGMT = timeZoneOffset;
			Double minutes = (e.EndDateTime.getTime() - e.StartDateTime.getTime()) / 1000 / 60;
			request.Duration = String.valueof((Integer)minutes);
			request.PostalCode = truncatePostalCode((e.PostalCode__c == null ? '' : e.PostalCode__c.replace('-','')), '');
		//	request.ScheduleNotes = (e.Description == null ? '' : (e.Description.length() > 300 ? e.Description.substring(0, 300) : e.Description) );
			request.ScheduleNotes = truncateTextArea(e.Description, 300);
		}
		
		return system.Json.serialize(request);
	}
	
	public virtual override void processResponse()
	{
		if (request == null && this.ServiceMessage != null) {
			request = (ADTSalesMobilityFacade.setUnavailableTimeRequest_element)system.Json.deserialize(this.ServiceMessage, system.type.forName('ADTSalesMobilityFacade.setUnavailableTimeRequest_element'));
		}
		else if (request == null) {
			createRequest();
		}
		
		ADTSalesMobilityFacade.SalesMobilitySOAP soap = new ADTSalesMobilityFacade.SalesMobilitySOAP();
		response = soap.setUnavailableTime(
			request.MessageID,
			request.TransactionType,
			request.TelemarScheduleID,
			request.HREmployeeID,
			request.ScheduleDateTime,
			request.ScheduleOffsetToGMT,
			request.Duration,
			request.PostalCode,
			request.ScheduleNotes,
			request.UserHREmployeeID   
		);  
		
		mid = response.MessageID;
		mstatus = response.MessageStatus;
		err = response.Error;
		sid = response.TelemarScheduleID;
		
		System.debug('$$$ mid: ' + mid);
		System.debug('$$$ mstatus: ' + mstatus);
		System.debug('$$$ err: ' + err);
		System.debug('$$$ sid: ' + sid);
		
		
		if (mstatus != IntegrationConstants.MESSAGE_STATUS_OK && mstatus != IntegrationConstants.MESSAGE_STATUS_FAIL) {
			throw new IntegrationException('Invalid response from Telemar: MessageStatus of ' + mstatus + ' is invalid');
		}
		
		if (mstatus == IntegrationConstants.MESSAGE_STATUS_FAIL) {
			throw new IntegrationException (err);
		}
	}

}