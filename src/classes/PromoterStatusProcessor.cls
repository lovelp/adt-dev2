/************************************* MODIFICATION LOG ********************************************************************************************
* PromoterStatusProcessor
*
* DESCRIPTION : A schedulable class that initiates the process to inactivate promoters.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner				 3/5/2012			- Original Version
*
*													
*/

global class PromoterStatusProcessor implements Schedulable {

	global void execute(SchedulableContext SC) {
		
		PromoterHelper.inactivatePromoters();
		 
   	}

}