@istest
public class CallDataTriggerHelperTest {
    static testMethod void testCallDataTriggerhelper(){
        User uAdmin = TestHelperClass.createAdminUser();
        System.runAs(uAdmin){
        TestHelperClass.createReferenceDataForTestClasses();
        TestHelperClass.createReferenceUserDataForTestClasses();
        }
        User u = TestHelperClass.createNSCRepUser();
        system.runas(u){
        
        Account a = TestHelperClass.createAccountData();
        APNVDN__c apnvdn = new APNVDN__c(Name = '8887776666');
        insert apnvdn;
        DNIS__c dnis = new DNIS__C(Name = '8887776666',
                                   Contact_Name__c='test',
                                   Short_Description__c='testtetsts',
                                   VDN__c=apnvdn.id,
                                   Business_segment__c='US: Residential',
                                   Ownership__c='ADT-NSC',
                                   Assignment__c='Other');
        insert dnis;
        Call_Data__c cD= new Call_Data__c(Name='Test Calldata',DNIS__c=dnis.id );
        insert cD;
        cD.Account__c=a.id;
        cD.AccountLastActivityDate__c =Date.today();
        Update cD;
        }
    }
    
    static testMethod void testCallDataTriggerhelper2(){
        User uAdmin = TestHelperClass.createAdminUser();
        System.runAs(uAdmin){
        TestHelperClass.createReferenceDataForTestClasses();
        TestHelperClass.createReferenceUserDataForTestClasses();
        }
        User u = TestHelperClass.createNSCRepUser();
        system.runas(u){
        
        Account a = TestHelperClass.createAccountData();
        APNVDN__c apnvdn = new APNVDN__c(Name = '8887776666');
        insert apnvdn;
        DNIS__c dnis = new DNIS__C(Name = '8887776666',
                                   Contact_Name__c='test',
                                   Short_Description__c='testtetsts',
                                   VDN__c=apnvdn.id,
                                   Business_segment__c='US: Residential',
                                   Ownership__c='ADT-NSC',
                                   Assignment__c='Other');
        insert dnis;
        Call_Data__c cD= new Call_Data__c(Name='Test Calldata',DNIS__c=dnis.id );
        insert cD;
        cD.Account__c=a.id;
        cD.AccountLastActivityDate__c =Date.today()-2;
        Update cD;
        }
    }
    
    static testMethod void testCallDataTriggerhelper3(){
        User uAdmin = TestHelperClass.createAdminUser();
        System.runAs(uAdmin){
        TestHelperClass.createReferenceDataForTestClasses();
        TestHelperClass.createReferenceUserDataForTestClasses();
        }
        User u = TestHelperClass.createNSCRepUser();
        system.runas(u){
        
        Account a = TestHelperClass.createAccountData();
        a.DNIS__c = '8887776666';
        a.DNIS_Modified_Date__c =system.now()-5;
        a.DNIS_Modifier_Name__c = userinfo.getUserid();
        update a;
        APNVDN__c apnvdn = new APNVDN__c(Name = '8887776666');
        insert apnvdn;
        DNIS__c dnis = new DNIS__C(Name = '8887776666',
                                   Contact_Name__c='test',
                                   Short_Description__c='testtetsts',
                                   VDN__c=apnvdn.id,
                                   Business_segment__c='US: Residential',
                                   Ownership__c='ADT-NSC',
                                   Assignment__c='Other');
        insert dnis;
        Call_Data__c cD= new Call_Data__c(Name='Test Calldata',DNIS__c=dnis.id );
        insert cD;
        cD.Account__c=a.id;
        cD.AccountLastActivityDate__c =Date.today()-2;
        Update cD;
        }
    }
}