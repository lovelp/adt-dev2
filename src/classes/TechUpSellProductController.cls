public class TechUpSellProductController {
    
    public Account acc {get;set;}
    public String fullProductCatalogNames {get;set;}
    public String fullProductCatalogQuantity {get;set;}
    public String productCatalogExistingList {get;set;}
    public TechUpSellDao daoObj = new TechUpSellDao();
    public TechUpSellProductController(ApexPages.StandardController controller){
        this.initializeAccountFromStandardController(controller)
        .getAllProductCatalogNames()
        .getAllProductCatalogQuantity()
        .initializeExistingProductcatalog();
    }
    
    public void setTechUpSellDao(TechUpSellDao daoObj){
        this.daoObj = daoObj;
    }
    
    private TechUpSellProductController initializeAccountFromStandardController(ApexPages.StandardController controller){
        this.acc = (Account)controller.getRecord();
        System.debug('### account is'+this.acc);
        return this;
    }

    private TechUpSellProductController getAllProductCatalogNames(){
        if(this.acc != null){            
            this.fullProductCatalogNames = this.daoObj.getAllProductCatalogNames();
        }       
        return this;
    }
    
    private TechUpSellProductController getAllProductCatalogQuantity(){
        if(this.acc != null){
            TechUpSellDao daoObj = new TechUpSellDao();
            this.fullProductCatalogQuantity = this.daoObj.getAllProductCatalogQuantity();
        }       
        return this;
    }
    
    private TechUpSellProductController initializeExistingProductcatalog(){
        if(this.acc != null){
            TechUpSellDao daoObj = new TechUpSellDao();
            this.productCatalogExistingList = this.daoObj.getExistingProductCatalog(this.acc);
        }
        return this;
    }
    
    @RemoteAction
     public static Boolean saveCatalog(String accountId, String productCatalogToSave){ 
             Boolean saveFlag = false;
             TechUpSellDao daoObj = new TechUpSellDao();
             saveFlag = daoObj.saveProductDetailsOnAccount(accountId,productCatalogToSave);               
             return saveFlag;
    }
}