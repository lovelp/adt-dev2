/************************************* MODIFICATION LOG ********************************************************************************************
* PendingDiscoArchiveBatch
*
* DESCRIPTION : This batch archives pending disco accounts that are more than five days beyond their disco date.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*              				 10/14/2011			- Original Version
*
*													
*/
global class PendingDiscoArchiveBatch implements Database.batchable<sObject>, Database.AllowsCallouts{

	//incoming query
	global String query;
	
	//Constructor - set the local variable with the query string
	global PendingDiscoArchiveBatch() {
		this.query = 'select id, LeadStatus__c, Type, Data_Source__c, OwnerId, ProcessingType__c, RecordTypeId, UnassignedLead__c, NewLead__c from Account where Type = \'Pending Discontinuance\' and PendingDisco5__c = \'Yes\' and LeadStatus__c = \'Active\' and RecordType.DeveloperName = \'' + RecordTypeDevName.ADT_NA_RESALE + '\' limit 10000';
	}
	
	//get Querylocator with the specitied query
	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext bc, List<sObject> accounts) {
		List<Account> updatedAccounts = new List<Account>();
		
		Id rifOwnerUser = Utilities.getRIFAccountOwner();
		RecordType rifAcctRecType = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account');
		
		for(sObject s : accounts)
		{
			Account a = (Account)s;
			// A Pending Disco from MMB
			if (a.Data_Source__c == IntegrationConstants.DATA_SOURCE_MASTERMIND_BUSINESS) {
				// stays Active and should be transformed back into a RIF account
				a.RecordTypeId = rifAcctRecType.Id;
				a.ProcessingType__c = IntegrationConstants.PROCESSING_TYPE_RIF;
				a.Data_Source__c = IntegrationConstants.DATA_SOURCE_GDW;
				a.Type = IntegrationConstants.TYPE_RIF;
				a.OwnerId = rifOwnerUser;
				a.UnassignedLead__c = true;
            	a.NewLead__c = true;
			} else {
				// Otherwise, the account gets archived and removed from the Resale pool
				a.LeadStatus__c = 'Archived';
			}	
			updatedAccounts.add(a);
		}
		//update updatedAccounts;
		Database.Saveresult[] SRs = database.update(updatedAccounts, false);
		List<Account> reUpdates = new List<Account>();
		Integer counter = 0;
		for(Database.SaveResult sr:SRs)
		{
			if(!sr.isSuccess())
			{
				reUpdates.add(updatedAccounts[counter]); 
			}
			counter++;
		}		
		if(reUpdates.size() > 0)
		{
			Database.Saveresult[] nSRs = database.update(reUpdates, false);
			for(Database.SaveResult nsr:nSRs)
			{
				if(!nsr.isSuccess())
				{
					//we can do something here if necessary
				}
			}				
		}
	}
	
	global void finish(Database.BatchableContext bc) {

	}
}