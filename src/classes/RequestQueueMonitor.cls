/************************************* MODIFICATION LOG ********************************************************************************************
* RequestQueueMonitor
*
* DESCRIPTION : Implements Schedulable and is responsible for retrieving records from Request Queue with the Request Status = Ready.  
*               Records are retrieved ordered by Request Date to ensure delivery of messages in the appropriate order.  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					1/19/2012				- Original Version
*
*													
*/

global class RequestQueueMonitor implements Schedulable {
	
	/*
	* Uses scope parameter of 1 since the batch class being constructed will make a callout and SFDC limits allow 
	* a batch class to make only one callout.
	* 
	* Includes logic to schedule its next execution based upon the custom setting RequestQueueMonitorInterval.
	*/
	global void execute(SchedulableContext sc)
	{
		//String requestMonitorRunning = BatchState__c.getInstance('IsRequestQueueMonitorRunning').value__c;
		if(BatchJobHelper.canThisBatchRun(BatchJobHelper.RequestQueueBatchProcessor))
		{
			Database.executeBatch(new RequestQueueBatchProcessor(), 1);
		}
		//reschedule the same class again
		RequestQueueMonitor rqm = new RequestQueueMonitor();
		DateTime timenow = DateTime.now().addMinutes(Integer.valueOf(ResaleGlobalVariables__c.getinstance('RequestQueueMonitorInterval').value__c));
		String seconds = '0';
		String minutes = String.valueOf(timenow.minute());
		String hours = String.valueOf(timenow.hour());
		
		//Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
		String jobName = 'Request Queue Processing - ' + hours + ':' + minutes;
		if( Test.isRunningTest() ){
			jobName = 'BATCH TEST - '+jobName;
		}
		system.schedule(jobName, sch, rqm);
		if (sc != null)		
			system.abortJob(sc.getTriggerId());	
	}
}