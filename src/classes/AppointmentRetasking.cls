public class AppointmentRetasking{
    
    public class GenericException extends Exception{}
    public string retaskCode{get;set;}
    public string schDate{get;set;}
    public list<RetaskCode__mdt> retascodelist = new list<RetaskCode__mdt>();
    public Event currentEvent;
    public Id accountId;
    list<RetaskingAppointmentProcessing.AvailableSalesRep> allRepDetails;
    public list<DateTime> fullhours{get;set;}
    public Boolean enableRetasking{get;set;}
    public Map<String, RetaskCode__mdt> custMetaMap;
    public Boolean displayCancel{get;set;}
    public string retaskComments{get;set;}
    public string retaskTime{get;set;}
    public string selRepId{get;set;}
    public GoogleMapsTimeZone.GoogleTZResponse googleTZResCust;
    public Boolean displayUpdate{get;set;}
    public String allTimes='DUMMY';
    public String newAppntName{get;set;}
    public String appntDuration{get;set;}
    public Account acnt;
    public String retaskWithDesc{get;set;}
    public String customerTimezone{get;set;}
    public AppointmentRetasking(Apexpages.Standardcontroller stdController){
        displayCancel = false;
        currentEvent = (Event)stdController.getRecord();
        if(currentEvent != null && String.isNotBlank(currentEvent.Id)){
        currentEvent = [Select WhoId, WhatId, Description,TaskCode__c, Subject, Status__c, StartDateTime,ShowAs,ScheduleID__c,ReminderDateTime,RecordTypeId, 
						OwnerId,IsReminderSet,InstallTechnician__c,EndDateTime,PostalCode__c, Quote_Id__c, Appointment_Type__c, RetaskingComments__c, RetaskingAppointment__c
					    FROM Event WHERE Id = :currentEvent.Id];
        String selProfiles = label.retaskingProfiles;
        String profileName = [SELECT name FROM Profile WHERE id=:userinfo.getProfileId()].name;
        String rdTypeName = [SELECT name FROM RecordType WHERE id=:currentEvent.recordTypeId].name;
        /*
        Id permssionSetId = [SELECT Id FROM PermissionSet WHERE ProfileId = :UserInfo.getProfileId()].Id;
        Boolean checkAccess=false;
        if([SELECT ParentId, SetupEntityId FROM SetupEntityAccess WHERE ParentId = :permssionSetId AND SetupEntityType = 'AppointmentRetasking'] !=null)
            checkAccess=true;*/
        //Available only for certain profiles and sales appointments only
        if(selProfiles.containsIgnoreCase(profileName) && rdTypeName == RecordTypeName.SELF_GENERATED_APPOINTMENT)//&& currentEvent.Status__c != 'Canceled' && currentEvent.StartDateTime > System.now())
            enableRetasking = true;
        else
            enableRetasking = false;
        custMetaMap = new Map<String, RetaskCode__mdt>();
        retascodelist = [SELECT id, MasterLabel, Action__c, Duration__c, Channel__c, NeedNotification__c,OrderTypes__c, Description__c, AppointmentType__c,
                        TaskCodeToUse__c FROM RetaskCode__mdt 
                        WHERE Active__c = TRUE AND Channel__c !=NULL AND OrderTypes__c !=NULL];

        Time startTime = Time.newInstance(5,0,0,0);
        Time midnight = Time.newInstance(0, 0, 0, 0);
        fullhours = new list<DateTime>();
        do{
            system.debug('Time'+startTime);
            if(fullhours ==  null || fullhours.size() != 19)
                fullhours.add(DateTime.newInstanceGmt(system.today(), startTime) );
            startTime = startTime.addMinutes(60);
        }while(startTime != midnight);
        //HRM-7300 Checking if event has account associated to it
        if(currentEvent.WhatId !=null){
        acnt = new Account();
        acnt = [Select Id, TelemarAccountNumber__c, MMBOrderType__c ,SalesAppointmentDate__c,Rep_User__c,Rep_User__r.Profile.Name, FirstName__c, LastName__c, Name, 
                     SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, 
                     Phone, Email__c, DispositionComments__c, QueriedSource__c, ReferredBy__c, Channel__c, Business_ID__c, 
                     ProcessingType__c, TaskCode__c, TransitionDate2__c, LeadExternalID__c, Data_Source__c, SiteValidated__c, 
                     Type, AccountStatus__c, OutboundTelemarAccountNumber__c, Affiliation__c, RifID__c, SiteStreet2__c 
                     FROM Account WHERE Id = :currentEvent.WhatId];
            }
        }
        
    }
    public list<SelectOption> getTaskcodes(){
        List<SelectOption> pickItems = new List<SelectOption>();
        pickItems.add(new Selectoption('SELECT','SELECT'));
        for(RetaskCode__mdt ret:retascodelist){
            if(acnt!=null && ((String.isNotBlank(acnt.Channel__c) && ret.Channel__c.contains(acnt.Channel__c)) || ret.Channel__c =='ANY') && ((String.isNotBlank(acnt.MMBOrderType__c) && ret.OrderTypes__c.contains(acnt.MMBOrderType__c)) || ret.OrderTypes__c =='ANY')){
                pickItems.add(new SelectOption(ret.Id, ret.MasterLabel));
                if(ret.Description__c !=null){
                    retaskWithDesc += ret.Id+'::'+'Description: '+ret.Description__c+';;';
                }
            }
            custMetaMap.put(ret.Id,ret);
        }
        return pickItems;
    }

    public class FieldRepWrapper{          
        public id fieldRepId {get;set;}
        public String fieldRepName {get;set;}
        public id accountId {get;set;}
        public list<timeWithColor> tmcolr{get;set;}
        public String allTimesString{get;set;}
        
    }
    
    public class timeWithColor{
        public String timeSlot{get;set;}
        public String endTimeSlot{get;set;}
        public String color{get;set;}
        public String allAvailTimes{get;set;}
    }
    
    public PageReference allRepsAndSch(){
        
        if(String.isNotBlank(retaskCode) && custMetaMap.get(retaskCode) != null && custMetaMap.get(retaskCode).Action__c == 'CAN'){
            displayCancel = true;
        }
        else if(String.isNotBlank(retaskCode) && custMetaMap.get(retaskCode) != null && custMetaMap.get(retaskCode).Action__c == 'UPD'){
            displayCancel = true;
            displayUpdate = true;
        }
        else if(String.isNotBlank(schDate) && custMetaMap.get(retaskCode) != null && String.isNotBlank(retaskCode) && currentEvent != null){
            newAppntName = EventManager.SUBJECT_PREFIX + custMetaMap.get(retaskCode).TaskCodeToUse__c+' - ' + acnt.SiteCity__c + ', ' + acnt.SiteState__c;
            appntDuration = custMetaMap.get(retaskCode).Duration__c;
            displayCancel = false;
            //Event eve = [SELECT whatId FROM Event where id=:currentEvent.Id];
            accountId = currentEvent.whatId;
            RetaskingAppointmentProcessing reApntObj = new RetaskingAppointmentProcessing();
            reApntObj.eventId = currentEvent.Id;
            reApntObj.accID = currentEvent.whatId;
            reApntObj.requestSchDate = Date.parse(schDate);
            allRepDetails = reApntObj.getAllEligibleReps();
            googleTZResCust = new GoogleMapsTimeZone.GoogleTZResponse();
            googleTZResCust = reApntObj.googleTZResp;
            customerTimezone = googleTZResCust.timeZoneName;
        }
        return null;
    }
    
    //public list<FieldRepWrapper> getRepDetails(){return null;}
    
    public list<FieldRepWrapper> getRepDetails(){
        if(allRepDetails != null){
            list<FieldRepWrapper> allFieldWrap = new list<FieldRepWrapper>();
            for(RetaskingAppointmentProcessing.AvailableSalesRep rep:allRepDetails){
                FieldRepWrapper frepWrap = new FieldRepWrapper();
                frepWrap.fieldRepId = rep.repId;
                frepWrap.fieldRepName = rep.repName;
                frepWrap.accountId = accountId;
                frepWrap.tmcolr = timeAndColor(rep.exisitngCalEvents, rep.startEndTimes);
                frepWrap.allTimesString = allTimes;
                allFieldWrap.add(frepWrap);
            }
        return allFieldWrap;
        }
        else{
            return null;
        }
    }
       
    public list<timeWithColor> timeAndColor(list<RetaskingAppointmentProcessing.ExistingEvents> existRepEvents, Time[] startEndTime){
        Time stTime = Time.newInstance(5,0,0,0); 
        Time edTime = Time.newInstance(23,59,59,59);
        list<timeWithColor> twclist = new list<timeWithColor>();
        Time midnight = Time.newInstance(0, 0, 0, 0);
        Integer i = 1;
        allTimes = 'DUMMY';
        if(existRepEvents != null){
            do{
                timeWithColor twc = new timeWithColor();
                if(twc == null || i!= 73){
                    String tempAllTimes;
                    String convTime = DateTime.newInstance(Date.parse(schDate),stTime).format('h:mm a');
                    twc.timeSlot = convTime;//DateTime.newInstance(Date.parse(schDate),stTime).format('h:mm a');
                    twc.endTimeSlot = DateTime.newInstance(Date.parse(schDate),stTime.addMinutes(Integer.valueOf(appntDuration))).format('h:mm a');
                    if(stTime >= startEndTime[0] && stTime < startEndTime[1]){
                        //all time will have additional 15 minutes. They indicate end of 15 min slot appointment.
                        tempAllTimes = DateTime.newInstance(Date.parse(schDate),stTime.addMinutes(15)).format('h:mm a');//DateTime.newInstance(Date.parse(schDate),stTime).format('h:mm a')+';';
                        twc.color = '#4CBB17';
                    }
                    for(RetaskingAppointmentProcessing.ExistingEvents ere:existRepEvents){
                        //system.debug('##Time'+stTime+ere.existStartEnd[0]+ ere.existStartEnd[1]);
                        //Block appointment duration before travel time
                        Integer appntDur = Integer.valueOf(custMetaMap.get(retaskCode).Duration__c);
                        //Duration of travel can cross end of the day. So making it day time to ensure it doesn't cross that day.
                        Time addTimeBefore = timeRound(ere.existStartEnd[0], -1*(ere.travelTimeBeforeAfter), startEndTime[0]);
                        Time addTimeAfter = timeRound(ere.existStartEnd[1], ere.travelTimeBeforeAfter, startEndTime[1]);
                        if(stTime >= addTimeBefore && stTime < addTimeAfter){
                            tempAllTimes =''; 
                            twc.color = '#555555';
                    }
                    }
                    if(String.isNotBlank(tempAllTimes))
                        allTimes = allTimes+';'+tempAllTimes;
                    twclist.add(twc);
                    }
                    stTime = stTime.addMinutes(15);
                    i++;
                }while(stTime != midnight);
        }
        else{
            do{
                timeWithColor twc = new timeWithColor();
                if(twc == null || i!= 73){
                    String convTime = DateTime.newInstance(Date.parse(schDate),stTime).format('h:mm a');
                    twc.timeSlot = convTime;//DateTime.newInstance(Date.parse(schDate),stTime).format('h:mm a');
                    twc.endTimeSlot = DateTime.newInstance(Date.parse(schDate),stTime.addMinutes(Integer.valueOf(appntDuration))).format('h:mm a');
                    //system.debug('##Time'+stTime+startEndTime[0]+ startEndTime[1]);
                    if(stTime >= startEndTime[0] && stTime < startEndTime[1]){
                        allTimes = allTimes+';'+DateTime.newInstance(Date.parse(schDate),stTime.addMinutes(15)).format('h:mm a');
                        twc.color = '#4CBB17';
                    }
                    twclist.add(twc);
                }
                stTime = stTime.addMinutes(15);
                i++;
            }while(stTime != midnight);
        }
        return twclist;
    }
    
    public PageReference goBack(){
        return null;
    }
    
    private Time timeRound(Time tempTime, Integer duration, Time returnTime){
        Datetime tempDtTime = Datetime.newInstance(Date.parse(schDate),tempTime);
        Datetime addtempDtTime = tempDtTime.addMinutes(duration);
        if(tempDtTime.Date() != addtempDtTime.Date()){
            return returnTime;
        }
        else if(duration < 0 && tempTime.addMinutes(duration) < returnTime){
            return returnTime;//tempTime.addMinutes(duration);
        }
        else if(duration > 0 && tempTime.addMinutes(duration) > returnTime){
            return returnTime;
        }
        else{
            return tempTime.addMinutes(duration);
        }
        
    }
    
    public PageReference retaskCancel(){
        //need to get all required event fields
		if (currentEvent != null) {
		    try{
		        String prevTaskCode = currentEvent.TaskCode__c;
		        Id prevOwner = currentEvent.OwnerId;
		        update cancelEvent(currentEvent);
		        createAccountDisposition(currentEvent.WhatId,prevTaskCode,currentEvent.TaskCode__c);
		        //sendCancelAppntToTelemar(currentEvent.Id, currentEvent.WhatId);
		        //Cancellation email- HRM-7300
		        EmailMessageUtilities.SendAppointmentNotification(currentEvent.Id, prevOwner, false);
		        // Don't want to return to event on cancel.
		        return new PageReference('/'+currentEvent.WhatId);
		    }
            //Boolean success = EventManager.cancelEvent(currentEvent);
            catch(Exception ae){
			    ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to cancel event.');
			    ApexPages.addMessage(m);
			    return null;
		    }
		}
		else{
		    ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to cancel event.');
			ApexPages.addMessage(m);
			return null;
		}
	}
	
	public pageReference updateAppnt(){
	    try{
	        system.debug('Re-Task Comments###'+retaskComments);
	        Account a = acnt;
	        currentEvent.RetaskingAppointment__c = true;
            currentEvent.RetaskingComments__c = retaskComments;
            String previousTaskCode = currentEvent.TaskCode__c;
            currentEvent.TaskCode__c = custMetaMap.get(retaskCode).TaskCodeToUse__c;
            currentEvent.Appointment_Type__c = custMetaMap.get(retaskCode).AppointmentType__c;
            currentEvent.subject = EventManager.SUBJECT_PREFIX + custMetaMap.get(retaskCode).TaskCodeToUse__c+' - ' + a.SiteCity__c + ', ' + a.SiteState__c;
	        Id prevOwnerId = currentEvent.OwnerId;
	        update currentEvent;
            system.debug('##Update'+currentEvent);
            createAccountDisposition(a.Id,previousTaskCode,currentEvent.TaskCode__c);
            //UserAppointmentCount.setAppointmentUserAggregate(currentEvent.Id,prevOwnerId,false);
            sendUpdateAppntToTelemar(currentEvent.Id,a.Id);
        }
        catch(Exception ae){
            ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to Update event.');
		    ApexPages.addMessage(m);
		    return null;
        }
        return new PageReference('/'+currentEvent.WhatId);
	}
	
	public Event cancelEvent(Event evnt){
	    Account a = acnt;
		evnt.RetaskingComments__c = retaskComments;
		evnt.RetaskingAppointment__c = true;
		evnt.TaskCode__c = custMetaMap.get(retaskCode).TaskCodeToUse__c;
		evnt.subject = 'CANCEL ' + custMetaMap.get(retaskCode).TaskCodeToUse__c+' - ' + a.SiteCity__c + ', ' + a.SiteState__c;
		evnt.Status__c = EventManager.STATUS_CANCELED;
		evnt.ShowAs = EventManager.SHOW_AS_FREE;
		User u = [Select Name from User where Id = :evnt.OwnerId];
		evnt.OwnerWhenCanceled__c = u.Name;
	    return evnt;
	}
    
    /*********************************************************************************************
     * Description: Final save event
     * 
     * ********************************************************************************************/
    
    public PageReference saveEvent(){
        system.debug('##FinalTime'+retaskComments+retaskTime+selRepId);
        String err;
        //Not sure if appointment needs to be blocked if it is back date. Any appointments can be retasked.
        /*try{
            if(converCusttologUserTime(DateTime.parse(Date.parse(schDate).format() + ' ' + retaskTime)) < System.now()){
                throw new GenericException('New appointment start date is in the past.');
            }
        }
        catch(Exception ae){
            ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.ERROR, ae.getMessage());
			ApexPages.addMessage(m);
			return null;
        }*/
        Account a = acnt;
        if(String.isNotBlank(retaskCode) && String.isNotBlank(selRepId) && custMetaMap.get(retaskCode) != null && custMetaMap.get(retaskCode).Action__c == 'NEW'){
            //Cacel current event first
            currentEvent.RetaskingComments__c = retaskComments;
		    currentEvent.RetaskingAppointment__c = true;
		    String prevTaskcode = currentEvent.TaskCode__c;
		    //HRM-7300 Send notifcation to prev owner
		    Id prevOwnerId = currentEvent.OwnerId;
		    try{
		        update cancelEvent(currentEvent);
		        UserAppointmentCount.setAppointmentUserAggregate(currentEvent.Id,prevOwnerId,false);
		        createAccountDisposition(currentEvent.WhatId,prevTaskcode,currentEvent.TaskCode__c);
		        EmailMessageUtilities.SendAppointmentNotification(currentEvent.Id, prevOwnerId, false);
		    }
            //Boolean success = EventManager.cancelEvent(currentEvent);
            Catch(Exception ae){
			    ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to cancel event.');
			    ApexPages.addMessage(m);
			    return null;
		    }
		    //Book a new Event
		    Event newEvent = new Event();
		    newEvent.ShowAs = EventManager.SHOW_AS_BUSY;
		    newEvent.Status__c= EventManager.STATUS_SCHEDULED;
		    newEvent.TaskCode__c = custMetaMap.get(retaskCode).TaskCodeToUse__c;
		    newEvent.subject = EventManager.SUBJECT_PREFIX + custMetaMap.get(retaskCode).TaskCodeToUse__c+' - ' + a.SiteCity__c + ', ' + a.SiteState__c;
		    newEvent.RetaskingAppointment__c = true;
            newEvent.RetaskingComments__c = retaskComments;
            newEvent.StartDateTime = converCusttologUserTime(DateTime.parse(Date.parse(schDate).format() + ' ' + retaskTime));
            newEvent.EndDateTime = converCusttologUserTime(DateTime.parse(Date.parse(schDate).format() + ' ' + retaskTime).addMinutes(Integer.valueOf(custMetaMap.get(retaskCode).Duration__c)));
            newEvent.ReminderDateTime = converCusttologUserTime(DateTime.parse(Date.parse(schDate).format() + ' ' + retaskTime).addMinutes(-1*15));//Default reminder
            newEvent.OwnerId = selRepId;
            newEvent.WhatId = a.Id;
            newEvent.TimeZoneId__c = TimeZone.getTimeZone(googleTZResCust.timeZoneId).getID();  //Srini
            newEvent.Appointment_Type__c = custMetaMap.get(retaskCode).AppointmentType__c;
            // Set appointment record type
            RecordType rt = [select id, name, sobjecttype from RecordType where sobjectType = 'Event' AND Name = :RecordTypeName.SELF_GENERATED_APPOINTMENT];
            newEvent.RecordTypeId = rt.Id;
            //newEvent.Appointment_Type__c = fielAppntType; 
            try{
                a.OwnerId = selRepId;
                a.Rep_User__c = selRepId;
                update a;
                insert newEvent;
                UserAppointmentCount.setAppointmentUserAggregate(newEvent.Id,null,false);
                //createAccountDisposition(newEvent.WhatId,prevTaskcode,newEvent.TaskCode__c);
                sendNewAppntToTelemar(newEvent.Id,a.Id);
                if(custMetaMap.get(retaskCode).NeedNotification__c){
                    EmailMessageUtilities.SendAppointmentNotification(newEvent.Id, selRepId);
                    if(Date.parse(schDate) == Date.today())
                    EmailMessageUtilities.NotifyUserBySMS(newEvent.Id, false);
                }
                return new PageReference('/'+a.Id);
            }
            catch(Exception ae){
               	ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to create new appointment.');
			    ApexPages.addMessage(m);
			    return null; 
            }
            //err = EventManager.createAppointmentForAccount(newEvent, a);
        }
        else{
             ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please contact system admin.');
			 ApexPages.addMessage(m);
		     return null;
        }
        //return new PageReference('/'+currentEvent.WhatId);
    }
    /******************************************************************************
     * Description: convert timezone to logged in user from customer timezone
     * 
     * ****************************************************************************/
    public DateTime converCusttologUserTime(Datetime appntDateTime){
        system.debug('##appntDate: '+appntDateTime+googleTZResCust);
        //integer customeOffsert = googleTZResp.dstOffset + googleTZResp
        if(appntDateTime !=null && String.valueOf(UserInfo.getTimeZone()) != googleTZResCust.timeZoneId){
            Timezone repTz = Timezone.getTimeZone(googleTZResCust.timeZoneId);
            Timezone userTz = Userinfo.getTimeZone();
            //DateTime currentDateTime = Datetime.newInstance(requestSchDate, appntTime);
            integer timeDiff = userTz.getOffset(appntDateTime)-repTz.getOffset(appntDateTime);
            appntDateTime = appntDateTime.addMinutes(timeDiff/(60*1000));
            system.debug('timeDiff - Rep'+timeDiff);
            system.debug('##Coverted: '+appntDateTime);
        }
        //system.debug('##Check-repTZ'+repsTimeZone+'CustTZ'+googleTZResp.timeZoneId+);
        return appntDateTime;
    }
    
    /******************************************************************************
     * Description: Create Disposition
     * 
     * ****************************************************************************/    
    
    public void createAccountDisposition(Id acct, String prevTaskCode, String currTaskCode){
        disposition__c newDispstn = new disposition__c();
        newDispstn.DispositionType__c = 'System Generated';
        newDispstn.DispositionDetail__c = 'Appointment Retasking';
        newDispstn.DispositionDate__c = DateTime.now();
        newDispstn.AccountID__c = acct;    
        newDispstn.DispositionedBy__c = UserInfo.getUserId();
        system.debug('Comments'+'Task Code:'+prevTaskCode+' to '+currTaskCode+';Comments:'+retaskComments);
        newDispstn.Comments__c = 'Task Code:'+prevTaskCode+' to '+currTaskCode+';Comments:'+retaskComments;
        //Retaskin actions: Cancelled; Tass code: ; Comments: 
        insert newDispstn;
        
    }
    
    @future(callout=true)
    public static void sendNewAppntToTelemar(String eventId, String accntId){
      try{
            
            //TelemarGateway.ScheduleResult result = new TelemarGateway.ScheduleResult();
            OutgoingAppointmentMessage om = new OutgoingAppointmentMessage();
            Account a = [SELECT id, Type, TelemarAccountNumber__c, OutboundTelemarAccountNumber__c, AccountStatus__c, ReferredBy__c, Data_Source__c, Affiliation__c
                         FROM ACCOUNT 
                         WHERE Id =:accntId];
            Event e = [SELECT Id, Subject, StartDateTime, EndDateTime, OwnerId, Reschedule__c, Reschedule_To__c, Reschedule_FromID__c, Reschedule_ToID__c, WhoId, Owner.Name, WhatId, What.Name, TaskCode__c, Status__c, ShowAs, ScheduleID__c, ReminderDateTime, IsReminderSet, InstallTechnician__c, PostalCode__c, RecordTypeId, Description, Quote_Id__c 
                       FROM Event 
                       WHERE id=:eventId];
            om.a = a;
            om.e = e;
            if (a.OutboundTelemarAccountNumber__c != null) {
                om.TransactionType = IntegrationConstants.TRANSACTION_APPOINTMENT_ONLY;
            } else {
                om.TransactionType = IntegrationConstants.TRANSACTION_NEW;
            }
            // MT1
            if (a.Type == IntegrationConstants.TYPE_RIF || IntegrationConstants.STATUS_CLOSED_VALUES.contains(a.AccountStatus__c)) {
                om.TransactionType += '-RIF';
            }
            //001
            User u = [Select Business_Unit__c, EmployeeNumber__c,Profile.Name from User where ID = :e.OwnerId];
            if (u.Business_Unit__c == 'Devcon'){
                om.TransactionType += '-DEV';
            }
            else{
                om.TransactionType += '-NSC';
            }
            IntegrationSettings__c settings = IntegrationSettings__c.getInstance(UserInfo.getUserId());
            RequestQueue__c rq = new RequestQueue__c();
            Datetime startTimestamp;
            Datetime endTimestamp;
            if (settings.TelemarCalloutsEnabled__c && settings.TelemarCalloutsAsync__c && u.EmployeeNumber__c.substring(0,1) <> 'Z' && a.ReferredBy__c <> 'ZZZ') {
                system.debug(e.id);
                //newReq = TelemarGateway.createRequest(om, IntegrationConstants.REQUEST_STATUS_ASYNCREADY, e, a);
                rq.ServiceMessage__c = om.createRequest();
                rq.ServiceTransactionType__c = om.ServiceTransactionType;
                rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCREADY;
                rq.RequestDate__c = DateTime.now();
                if (e != null) {
                    rq.EventID__c = e.Id;
                }
                if (a != null) {
                    rq.AccountID__c = a.Id;
                }
                rq.MessageID__c = om.MessageID;
                om.ServiceMessage = rq.ServiceMessage__c;
                om.EventID = rq.EventID__c;
                try {
                    startTimestamp = Datetime.now();
                    om.process();
                    rq.SentDateTime__c = startTimestamp;
                    rq.ResponseDateTime__c = Datetime.now();
                    if (om.Status != null && om.Status.equalsIgnoreCase('error')){
                        // request was sent and the response received but with some kind of error so mark as such
                        rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCERROR;
                        insert rq;
                    }
                    else{
                        rq.ResponseScheduleID__c = om.TelemarScheduleID;
                        rq.ResponseTelemarNumber__c = om.TelemarAccountNumber;
                        rq.ResponseTaskCode__c = om.TaskCode;
                        // request was sent and the response received so mark as success
                        rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCSUCCESS;
                        //result.ScheduleID = rq.ResponseScheduleID__c;
                        //result.TaskCode = rq.ResponseTaskCode__c;
                        system.debug('** ServiceTranType:' + rq.ServiceTransactionType__c);
                        /*if (!rq.ServiceTransactionType__c.contains(IntegrationConstants.TRANSACTION_APPOINTMENT_ONLY)){
                            system.debug('** Not Contains:' + rq.ServiceTransactionType__c);
                            a.TelemarAccountNumber__c = rq.ResponseTelemarNumber__c;
                            //result.AccountNumber = rq.ResponseTelemarNumber__c;
                        }*/
                        rq.Processed__c = true;
                        insert rq;
                        if (String.isNotBlank(rq.id)){
                            e.ScheduleID__c = om.TelemarScheduleID;
                            e.TaskCode__c = om.TaskCode;
                            //e.Subject = SUBJECT_PREFIX + e.TaskCode__c + getAcctCityState(e.WhatId);
                            //createEvent(e);
                            update e;
                            a.SalesApptCancelledFlag__c = null;
                            if (a.AccountStatus__c == IntegrationConstants.STATUS_CAV) {
                                a.AccountStatus__c = null;
                            }
            
                            update a;
                        }
                    }
                }
                catch (CalloutException ce) {
                    endTimestamp = Datetime.now();
                    Long durationMs = endTimestamp.getTime() - startTimestamp.getTime();
                    rq.ErrorDetail__c = ce.getMessage() + '  ** DURATION (in ms): ' + durationMs;
                    rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCERROR;
                    insert rq;
                } 
                //******************************** future request *********************************
                //throw new IntegrationException ('** Request has been sent to Telemar - Please hit button to check for a response');
                }
            }
            catch(Exception ae){
                //Any kind of exception is handled with Exception message
                ADTApplicationMonitor.log(ae, 'ADTPartnersetFieldSalesAppController', 'syncWithTelemar', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
            }
    }
    
    @future(callout=true)
    public static void sendUpdateAppntToTelemar(String eventId, String accntId){
        
        Account a = [Select Id, TelemarAccountNumber__c, SalesAppointmentDate__c,Rep_User__c,Rep_User__r.Profile.Name, FirstName__c, LastName__c, Name, 
                     SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, 
                     Phone, Email__c, DispositionComments__c, QueriedSource__c, ReferredBy__c, Channel__c, Business_ID__c, 
                     ProcessingType__c, TaskCode__c, TransitionDate2__c, LeadExternalID__c, Data_Source__c, SiteValidated__c, 
                     Type, AccountStatus__c, OutboundTelemarAccountNumber__c, Affiliation__c, RifID__c, SiteStreet2__c 
                     FROM Account WHERE Id = :accntId];
        Event e = [Select WhoId, WhatId, Description,TaskCode__c, Subject, Status__c, StartDateTime,ShowAs,ScheduleID__c,ReminderDateTime,RecordTypeId, 
				   OwnerId,IsReminderSet,InstallTechnician__c,EndDateTime,PostalCode__c, Quote_Id__c, Appointment_Type__c, RetaskingComments__c, RetaskingAppointment__c
				   FROM Event WHERE Id = :eventId];
		TelemarGateway.ScheduleResult result = TelemarGateway.rescheduleAppointment(a, e);
	    if (result == null)	{
			//put into the log
		}
		else{
			e.ScheduleID__c = result.ScheduleID;
			e.TaskCode__c = result.TaskCode;
			update e;
		}
    }
    
    @future(callout=true)
    public static void sendCancelAppntToTelemar(String eventId, String accntId){
        
        Account a = [Select Id, TelemarAccountNumber__c, SalesAppointmentDate__c,Rep_User__c,Rep_User__r.Profile.Name, FirstName__c, LastName__c, Name, 
                     SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, 
                     Phone, Email__c, DispositionComments__c, QueriedSource__c, ReferredBy__c, Channel__c, Business_ID__c, 
                     ProcessingType__c, TaskCode__c, TransitionDate2__c, LeadExternalID__c, Data_Source__c, SiteValidated__c, 
                     Type, AccountStatus__c, OutboundTelemarAccountNumber__c, Affiliation__c, RifID__c, SiteStreet2__c 
                     FROM Account WHERE Id = :accntId];
        Event e = [Select WhoId, WhatId, Description,TaskCode__c, Subject, Status__c, StartDateTime,ShowAs,ScheduleID__c,ReminderDateTime,RecordTypeId, 
				   OwnerId,IsReminderSet,InstallTechnician__c,EndDateTime,PostalCode__c, Quote_Id__c, Appointment_Type__c, RetaskingComments__c, RetaskingAppointment__c
				   FROM Event WHERE Id = :eventId];
		TelemarGateway.cancelAppointment(a, e);
    }
    
    

}