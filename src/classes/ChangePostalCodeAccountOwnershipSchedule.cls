/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : ChangePostalCodeAccountOwnershipSchedule.cls is a schecdulable class that invokes ChangePostalCodeAccountOwnershipBatch 
*               and reschedules itself x minutes after the current run based on a custom setting determining x (interval)
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli               11/05/2011          - Origininal Version
*
* Sunil Addepalli               02/10/2012          - Modified to add ability to process leads
*                                                     Added functionality to skip stqarting the batch if another batch is running.
*                                                    
*/
global class ChangePostalCodeAccountOwnershipSchedule  implements Schedulable {
    
    
    global void execute(SchedulableContext SC) {
        //String mgrAcctBatch = BatchState__c.getInstance('IsAcctManagerTownBatchRunning').value__c;
        //String mgrLeadBatch = BatchState__c.getInstance('IsLeadManagerTownBatchRunning').value__c;
        //String pcAcctBatch = BatchState__c.getInstance('IsAcctTerritoryRealignmentBatchRunning').value__c;
        //String pcLeadBatch = BatchState__c.getInstance('IsLeadTerritoryRealignmentBatchRunning').value__c;
        String MTInterval = BatchState__c.getInstance('PCOwnerChgScheduledBatchInterval').value__c;
        List<Id> PCIds = new List<Id>();
        //run this batch only if no other manager town batch or postal code realignment batch is running
            if(BatchJobHelper.canThisBatchRun(BatchJobHelper.ChangePostalCodeAccountOwnershipBatch))
            {            	
                List<PostalCodeReassignmentQueue__c> lstPCQNoPostalcode= [select id, PostalCodeId__c, PostalCodeNewOnwer__c, TerritoryType__c from PostalCodeReassignmentQueue__c where PostalCodeId__c=null];
                 if(lstPCQNoPostalcode.size()>0){
                 	List<Database.deleteresult> dr = database.Delete(lstPCQNoPostalcode);
                 } 
		 
		//get the first record in the queue
                List<PostalCodeReassignmentQueue__c> PCQr= [select id, PostalCodeId__c, PostalCodeNewOnwer__c, TerritoryType__c from PostalCodeReassignmentQueue__c order by createddate limit 1];
                if(PCQr.size() > 0)
                {
                	User U = [Select UserRoleId, Qualification__c from User where id =: PCQr[0].PostalCodeNewOnwer__c Limit 1];
                    //get all the similar recors based on PCQr. There will atleast be one record in this query (PCQr[0])
                    List<PostalCodeReassignmentQueue__c> PCQ = [select id, PostalCodeId__c, PostalCodeNewOnwer__c, TerritoryType__c from PostalCodeReassignmentQueue__c where TerritoryType__c = : PCQr[0].TerritoryType__c and PostalCodeNewOnwer__c =: PCQr[0].PostalCodeNewOnwer__c]; 
                    string PostalCodeIds = '';
                    //Integer length = PCQ.size();
                    //Integer counter = 0;
                    /*for(PostalCodeReassignmentQueue__c P : PCQ)
                    {
                        counter++;
                        if(counter != length){
                            PostalCodeIds += '\'' + P.PostalCodeId__c + '\', ';
                        } else {
                            PostalCodeIds += '\'' + P.PostalCodeId__c + '\'';
                        }
                        PCIds.add(p.PostalCodeId__c);
                    }*/
                    
                    for(PostalCodeReassignmentQueue__c P : PCQ)
                    {
                    	if(string.isNotBlank(p.PostalCodeId__c)){	                        
	                        PostalCodeIds += '\'' + P.PostalCodeId__c + '\', ';
	                        PCIds.add(p.PostalCodeId__c);
                        }
                    }
                    PostalCodeIds = PostalCodeIds.removeEnd(', ');
                    map<string, string> managers = Utilities.getManagersForPostalCodes(PCIds);
                    //use existing sets for closed and sold statuses (integrationconstants class)
                    if(PCQ[0].TerritoryType__c == Channels.TYPE_RESIDIRECTPHONESALES || PCQ[0].TerritoryType__c == Channels.TYPE_RESIRESALEPHONESALES ||
                     PCQ[0].TerritoryType__c == Channels.TYPE_SBDIRECTPHONESALES || PCQ[0].TerritoryType__c == Channels.TYPE_SBRESALEPHONESALES
                      ||  PCQ[0].TerritoryType__c == Channels.TYPE_BUSINESSDIRECTSALES || PCQ[0].TerritoryType__c == Channels.TYPE_NATIONAL)
                    {
                        String acctQry = 'Select id, OwnerId, DateAssigned__c, UnassignedLead__c, ManuallyAssigned__c, PreviousOwner__c, Affiliation__c, Business_Id__c, Channel__c, PostalCodeId__c from Account where PostalCodeId__c in (';
                        acctQry += PostalCodeIds + ')'; 
                        acctQry += 'and Channel__c = \'' + Channels.getMappedChannel(PCQ[0].TerritoryType__c, true) + '\' ';
                        if(PCQ[0].TerritoryType__c != Channels.TYPE_BUSINESSDIRECTSALES || PCQ[0].TerritoryType__c != Channels.TYPE_NATIONAL){
                        	acctQry += 'and ProcessingType__c = \'NSC\' ';
                        }
                        acctQry += 'and RecordType.DeveloperName != \'ADTNAResale\' and AccountStatus__c NOT IN (\'SLD\', \'SLS\',\'CLS\')';                    
                        id acctBatchId = database.executeBatch(new ChangePostalCodeAccountOwnershipBatch(acctQry, PCQ[0].PostalCodeNewOnwer__c, true, PCQ, U.Qualification__c, managers), 1000);
                    }
                    else if (PCQ[0].TerritoryType__c == Channels.TYPE_RESIRESALE || PCQ[0].TerritoryType__c == Channels.TYPE_SBRESALE)
                    {
                        String acctQry = 'Select id, OwnerId, DateAssigned__c, UnassignedLead__c, ManuallyAssigned__c, PreviousOwner__c, Affiliation__c, Business_Id__c, Channel__c, PostalCodeId__c from Account where PostalCodeId__c in (';
                        acctQry += PostalCodeIds + ')'; 
                        acctQry += 'and Channel__c = \'' + Channels.getMappedChannel(PCQ[0].TerritoryType__c, true) + '\' ';
                        acctQry += 'and RecordType.DeveloperName = \'ADTNAResale\' and AccountStatus__c NOT IN (\'SLD\', \'SLS\',\'CLS\')';                    
                        id acctBatchId = database.executeBatch(new ChangePostalCodeAccountOwnershipBatch(acctQry, PCQ[0].PostalCodeNewOnwer__c, true, PCQ, U.Qualification__c, managers), 1000);                      
                    }
                    /*else if (PCQ[0].TerritoryType__c == Channels.TYPE_RESIRIF || PCQ[0].TerritoryType__c == Channels.TYPE_SBRIF)
                    {
                    	String acctQry = 'Select id, OwnerId, DateAssigned__c, UnassignedLead__c, ManuallyAssigned__c, PreviousOwner__c, Affiliation__c, Business_Id__c, Channel__c, PostalCodeId__c from Account where PostalCodeId__c in (';
                        acctQry += PostalCodeIds + ')'; 
                        acctQry += 'and Channel__c = \'' + Channels.getMappedChannel(PCQ[0].TerritoryType__c, true) + '\' ';
                        acctQry += 'and RecordType.DeveloperName = \''+ RecordTypeDevName.RIF_ACCOUNT +'\'';                    
                        id acctBatchId = database.executeBatch(new ChangePostalCodeAccountOwnershipBatch(acctQry, PCQ[0].PostalCodeNewOnwer__c, true, PCQ, U.Qualification__c, managers), 1000);                      
                    	
                    }*/
                    if(PCQ[0].TerritoryType__c == Channels.TYPE_RESIDIRECT || PCQ[0].TerritoryType__c == Channels.TYPE_RESIRESALE || PCQ[0].TerritoryType__c == Channels.TYPE_SBDIRECT 
                    		|| PCQ[0].TerritoryType__c == Channels.TYPE_SBRESALE || PCQ[0].TerritoryType__c == Channels.TYPE_CUSTOMHOMESALES 
                    		|| PCQ[0].TerritoryType__c == Channels.TYPE_HOMEHEALTH)
                    {
                        String leadQry = 'Select id, OwnerId, DateAssigned__c, UnassignedLead__c, PreviousOwner__c, Affiliation__c, Business_Id__c, Channel__c, PostalCodeId__c, TownNumber__c from Lead where PostalCodeId__c in ('; 
                        leadQry += PostalCodeIds + ')';
                        leadQry += 'and Channel__c = \'' + Channels.getMappedChannel(PCQ[0].TerritoryType__c, true) + '\' ';
                        leadQry += 'and LeadSource in (\'BUDCO\', \'Manual Import\', \'Telemar Rehash\' ) ';
                        leadQry += 'and Affiliation__c not in (\'Builder\')';
                        id leadBatchId = database.executeBatch(new ChangePostalCodeAccountOwnershipBatch(leadQry, PCQ[0].PostalCodeNewOnwer__c, false, PCQ, U.Qualification__c, managers), 1000);
                    }
             /*       else if(PCQ[0].TerritoryType__c == Channels.TERRITORYTYPE_USAA)
                    {
						String leadQry = 'Select id, OwnerId, DateAssigned__c, UnassignedLead__c, PreviousOwner__c from Lead where PostalCodeId__c in ('; 
                        leadQry += PostalCodeIds + ')';
                        leadQry += 'and Channel__c = \'Resi Direct Sales\' ';
                        leadQry += 'and LeadSource in (\'Manual Import\', \'Telemar Rehash\' ) ';
                        leadQry += 'and Affiliation__c not in (\'USAA\')';
                        id leadBatchId = database.executeBatch(new ChangePostalCodeAccountOwnershipBatch(leadQry, PCQ[0].PostalCodeNewOnwer__c, false, PCQ), 1000);                    	
                    }   */
                    else if (PCQ[0].TerritoryType__c == Channels.TERRITORYTYPE_BUILDER)
                    {
						String leadQry = 'Select id, OwnerId, DateAssigned__c, UnassignedLead__c, PreviousOwner__c, Affiliation__c, Business_Id__c, Channel__c, PostalCodeId__c, TownNumber__c from Lead where PostalCodeId__c in ('; 
                        leadQry += PostalCodeIds + ')';
                        leadQry += 'and Channel__c = \'Resi Direct Sales\' ';
                        leadQry += 'and LeadSource in (\'Manual Import\', \'Telemar Rehash\' ) ';
                        leadQry += 'and Affiliation__c  in (\'Builder\')';
                        id leadBatchId = database.executeBatch(new ChangePostalCodeAccountOwnershipBatch(leadQry, PCQ[0].PostalCodeNewOnwer__c, false, PCQ, U.Qualification__c, managers), 1000);                    	
                    }
                }
                //delete PCQ;
            }
        //reschedule the same class again
        ChangePostalCodeAccountOwnershipSchedule m = new ChangePostalCodeAccountOwnershipSchedule();
        DateTime timenow = system.now().addMinutes(Integer.ValueOf(MTInterval));
        String seconds = String.valueOf(timenow.second()); //Execute at Zero Seconds
        String minutes = String.valueOf(timenow.minute()); //Execute at every 10th minute of hour
        String hours = String.valueOf(timenow.hour()); // Execute Every Hour
        //String dayOfMonth = String.valueOf(timenow.day()); // Execute Every Day of the Month
        
        //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = seconds + ' ' + minutes + ' * * * ?';
        String jobName = 'Change Account Ownership - ' + timenow;
        system.schedule(jobName, sch, m);       
        system.abortJob(sc.getTriggerId()); 
    }
        

}