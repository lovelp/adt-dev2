@isTest
global class ADTPartnerGetDLLAppointmentCalloutMock implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
            system.debug('@@ADTPartnerGetDLLAppointmentCalloutMock Class');
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{ "dstOffset" : 0,"rawOffset" : -18000, "status" : "OK", "timeZoneId" : "America/New_York","timeZoneName" : "Eastern Standard Time"}');
        response.setStatusCode(200);
        System.debug('Response'+response.getbody());
        system.debug(json.deserializeUntyped(response.getBody()));
        return response; 
    }
}