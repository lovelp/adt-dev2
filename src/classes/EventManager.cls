/************************************* MODIFICATION LOG ********************************************************************************************
* EventManager
*
* DESCRIPTION : Utility methods for handling various event actions
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    TICKET                REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  02/15/2012                                    - Original Version
* Srinivas Yarramsetti          09/10/2018              HRM 5710 & 7271
* Srinivas Yarramsetti          11/29/2018              HRM-8623              - Admin Blocks - Remove Telemar Dependency 
* Srinivas Yarramsetti          01/24/2019              HRM-8851              - Added shipping country to the account query
* Siddarth Asokan               04/22/2019              HRM-9751              - Remove Telemar Number Nextup from createSGAndConvertLead method                         
*/

public without sharing class EventManager { 
	
	// String literal constants for valid status values of the event
	public static final String STATUS_SCHEDULED = 'Scheduled';
	public static final String STATUS_RESCHEDULED = 'Rescheduled';
	public static final String STATUS_CANCELED = 'Canceled';
	
	// String literal constants for the Show As values
	public static final String SHOW_AS_BUSY = 'Busy';
	public static final String SHOW_AS_FREE = 'Free';
	
	// String literal constant for the Subject
	public static final String SUBJECT_PREFIX = 'Appt: ';
	
	// Boolean constants relating to reminders
	public static final Boolean REMINDER_NEEDED = true;
	public static final Boolean NO_REMINDER_NEEDED = false;
	
	// Incoming Type values
	public static final String INCOMING_TYPE_CG = 'CG';
	public static final String INCOMING_TYPE_SG = 'SG';
	public static final String INCOMING_TYPE_INSTALL = 'Install';
	
	// String literal constants for GPS Metrics
	public static final String LESS_THAN_30_MINS = '< 30 min';
	public static final String BETWEEN_30_AND_60_MINS = '30-60 min';
	public static final String BETWEEN_60_AND_90_MINS = '60-90 min';
	public static final String MORE_THAN_90_MINS = '> 90 min';
	public static final String NOT_AT_LOCATION = 'Not At Location';
	public static final String DATA_NOT_AVAILABLE = 'No Data Available';
	public static final String ONTIME = 'On Time';
	public static final String LATE_BY_5_TO_10_MINS = '5-10 min late';
	public static final String LATE_BY_10_TO_20_MINS = '10-20 min late';
	public static final String LATE_BY_MORE_THAN_20_MINS = '> 20 min late';
  
  	private static Boolean isCancelEventContext;
  
  	static{
  		isCancelEventContext = false;
  	}

	/**  Generic Callout Methods   **/
	
	/**
		createEvent	create Company-Generated or Install Appointment
		@param	e	Event to insert
	*/
	public static void createEvent(Event e){
		insert e;
    	UserAppointmentCount.setAppointmentUserAggregate(e.Id, true);
	}
	
	/**
		createEvent	Create event and assign to account
		@param	e	Event to insert
		@param	a	The account to assign to the event
	*/
	public static void createEvent(Event e, Account a){
		createEvent(e, a.Id);
	}
	
	/**
		createEvent	Create event and assign to account
		@param	e	Event to insert
		@param	accountId	The account id to assign to the event
	*/
	public static void createEvent(Event e, Id accountId){
		e.WhatId = accountId;
		createEvent(e);
	}
	
	/**
		updateEvent	Update an event
		@param	e	Event to update
	*/
	public static void updateEvent(Event e){
		Id OwnerBeforeCancel = e.OwnerId;
		update e;
    	if( isCancelEventContext ){
    		UserAppointmentCount.setAppointmentUserAggregate(e.Id, OwnerBeforeCancel, false);
    	}
	}
	
	
	/**
		updateEvent	Update an event
		@param	e	Event to update
		@param	a	Account to assign to event
	*/
	public static void updateEvent(Event e, Account a){
		updateEvent(e, a.Id);
	}
	
	/**
		updateEvent	Update an event
		@param	e	Event to update
		@param	accountId	Account Id to assign to event
	*/
	public static void updateEvent(Event e, Id accountId){
		e.WhatId = accountId;
		updateEvent(e);
	}
	
	/**
		deleteEvent	Deletes an event record
		@param	e	The event record to be deleted
		@return	Returns error message on failure, null on success
	*/
	public static String deleteEvent(Event e){
		try {
			delete e;
		} catch (Exception ex) {
			return ex.getMessage();
		}
		return null;
	}
	
	/**
		createSGAndConvertLead	Create SG Appointment and convert lead to account to assign to event
		@param	e	Event to insert
		@param	l	Lead to convert
		@return	Returns error message on failure, null on success
	*/
  	//HRM-7271 & 5710 Changes made by Srini to skip telemar sync errors for SGL Appointments and accounts
	public static String createSGAndConvertLead(Event e, Lead l){
	    SavePoint sp;
	    try{
	        sp = Database.setSavepoint();
	        //Set task code
	        String selTaskCode;
	        Map<String, TaskCodeEventRecordTypeMap__c> tascodeevenMap  = TaskCodeEventRecordTypeMap__c.getAll();
	        for(TaskCodeEventRecordTypeMap__c tcer:tascodeevenMap.values()){
	          if(tcer.Channel__c != null && tcer.Channel__c == l.Channel__c)
	            selTaskCode = tcer.TaskCode__c;
	        }
	        e.TaskCode__c = selTaskCode;
	        //Create event for lead first.
	        e.whoId = l.Id;
	        //Show city and state if comming from a lead appt.
	        if(l.id != null){
	            e.Subject = SUBJECT_PREFIX + e.TaskCode__c + ' - ' + l.SiteCity__c + ', ' + l.SiteStateProvince__c;
	        }
	        //convert lead
	        LeadConversion lc;
	        /* HRM-9751 - LeadManagementId changes - Start */
	        if(String.isBlank(l.TelemarAccountNumber__c)){
	            if(String.isNotBlank(l.LeadManagementId__c)){
	            	l.TelemarAccountNumber__c = l.LeadManagementId__c;
	            }else{
	            	// If there is no lead mangement Id on lead, create a dummy lead, get the lead management id and destroy it
	            	list<String> leadMangementIds = Utilities.createLeadMangementId(1);
	            	if(leadMangementIds != null && leadMangementIds.size() >0){
	            		l.TelemarAccountNumber__c = leadMangementIds[0];
	            	}
	            }
	            if(String.isNotBlank(l.TelemarAccountNumber__c)) update l;
	        }
	        lc = new LeadConversion(l.Id, l.TelemarAccountNumber__c); 
	        /* HRM-9751 - LeadManagementId changes - End */
	        Boolean success = lc.convertLead();
	        if(success){
	            e.WhoId = null;        
	            Account acc = [SELECT Name,AddressID__c,MMBOrderType__c, Id,Rep_User__c,Owner.Profile.Name,Rep_User__r.Profile.Name, TelemarAccountNumber__c, 
	                           SalesAppointmentDate__c, FirstName__c, LastName__c, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, 
	                           SiteCountryCode__c, Phone, PhoneNumber3__c, PhoneNumber2__c, PhoneNumber4__c, DNIS__c, Email__c, DispositionComments__c, QueriedSource__c, ReferredBy__c, Channel__c, Business_ID__c, ProcessingType__c, 
	                           TaskCode__c, SiteValidated__c,TransitionDate2__c, LeadExternalID__c, Data_Source__c, Type, AccountStatus__c, OutboundTelemarAccountNumber__c, Affiliation__c, 
	                           TMTownID__c, TMSubTownID__c, ExtraSkills__c,SiteStreet2__c, RifID__c,ShippingCountry FROM Account WHERE id = :lc.AccountId];
	            e.Channel__c = acc.Channel__c;
	            e.OrderType__c = String.isBlank(acc.MMBOrderType__c)?'N1':acc.MMBOrderType__c;
	            
	            createEvent(e, lc.AccountId);
	            //Outgoing Account and appointment/
	            
	            OutgoingAppointmentMessage om = new OutgoingAppointmentMessage();
	            om.a = acc;
	            om.e = e;
	            om.TransactionType = IntegrationConstants.TRANSACTION_NEW; 
	            User u = [Select Business_Unit__c, EmployeeNumber__c from User where ID = :e.OwnerId];
	            if (u.Business_Unit__c == 'Devcon'){
	                om.TransactionType += '-DEV';
	            }
	        
	            if (ResaleGlobalVariables__c.getInstance('TelemarSGOverride').value__c != null &&
	                ResaleGlobalVariables__c.getInstance('TelemarSGOverride').value__c == 'TRUE'){
	            	om.TransactionType += '-OVR';
	            }
	            RequestQueue__c reqForAccount = new RequestQueue__c();
	            reqForAccount = TelemarGateway.createRequest(om, IntegrationConstants.REQUEST_STATUS_ASYNCREADY, e, acc ); 
	            reqForAccount.SentDateTime__c = system.now();
	            insert reqForAccount;
	            if(TelemarDecommission__c.getInstance('FieldAccountTelmarSync').value__c == 'true'){               
	                SendAccountToTelemarQueue sendAccnt = new SendAccountToTelemarQueue(acc, reqForAccount, e);
	                Id accntJobId = System.enqueueJob(sendAccnt);
	            }
	            else{
	            	e.ScheduleID__c = String.valueof(system.now().getTime()).subString(0,10);
	            	update e;
	            }
	        }
	        else{   
	        	String errors = '';
	        	for (Database.Error error : lc.errors){
	          		errors += error.getMessage() + ';';
	        	}
	        	return errors;
	      	}
    	}
	    catch (Exception ex) {
	        if(sp != null) {
	            Database.rollback(sp);
	        }
	      return ex.getMessage();
	    }
    	return null;
    }

	public static String createAppointmentForAccount(Event e, Account a){
		return createAppointmentForAccount( e, a, false);
	}
	
	/**
		createSGForAccount	Create self-generated appointment and assign to an account
		@param	e	Event to create
		@param	a	Account to assign to event
		@return	Returns error message on failure, null on success
	*/
     public static String createAppointmentForAccount(Event e, Account a, Boolean UpdateAccountBefore){
     	try{
        	TelemarDecommission__c fieldSalesAppointmentTelemarDecommission = TelemarDecommission__c.getInstance('FieldSalesAappointment');
            // When telemar is decomissioned no need for updating account as it only updates the telemar number
         
            //e.Subject = SUBJECT_PREFIX + e.TaskCode__c + getAcctCityState(e.WhatId);
            a.SalesApptCancelledFlag__c = null;
          	// So if an appt is set, should we clear out the status flag if it is CAV?
	        if (a.AccountStatus__c == IntegrationConstants.STATUS_CAV) {
	            a.AccountStatus__c = null;
	        }
          	createEvent(e);
          	update a;
          	if(fieldSalesAppointmentTelemarDecommission.value__c.toUpperCase().equalsIgnoreCase('FALSE')){
            	TelemarGateway.createAppointment(a, e);
          	}
          	else{
          		e.ScheduleID__c = String.valueof(system.now().getTime()).subString(0,10);
          		update e;
          	}
        }
        catch (IntegrationException ie){
        	system.debug(' Integration Exception...'+ie.getMessage()+' trace:'+ie.getStackTraceString());
        	return ie.getMessage();
        }
        catch (Exception ex){
        	system.debug(' Generic Exception...'+ex.getMessage()+' trace:'+ex.getStackTraceString());
        	return prepareError();
        }
        return null;
	}
	
	/**
		updateSGAppointment	update an SG event
		@param	e	Event to update
		@param	a	Account assigned to event
		@return	Returns error message on failure, null on success
	*/
	public static String updateAppointment(Event e, Account a){
        try{
        	// If the apppointment type it's travel do not include City+State in the subject
        	if ((e.TaskCode__c == 'TRV') || (e.TaskCode__c == 'RHS')){
            	e.Subject = SUBJECT_PREFIX + e.TaskCode__c;
          	}else{
            	e.Subject = SUBJECT_PREFIX + e.TaskCode__c + getAcctCityState(e.WhatId);
          	}
          	TelemarGateway.ScheduleResult result = TelemarGateway.rescheduleAppointment(a, e);
          	updateEvent(e, a);
        }
        catch (IntegrationException ie){
        	return ie.getMessage();
        }
        catch (Exception ex){
          return prepareError();
        }
        return null;
      }
	
	/**
		cancelSGAppointment	Cancel an SG event
		@param	e	Event to update
		@param	a	Account assigned to event
		@return	Returns error message on failure, null on success
	*/
	public static String cancelAppointment(Event e, Account a){
    	try{
        	TelemarDecommission__c fieldSalesAppointmentTelemarDecommission = TelemarDecommission__c.getInstance('FieldSalesAappointment');
          	setEventToCanceled(e);
          	updateEvent(e, a);
    
          	if (DateTime.now() < a.SalesAppointmentDate__c) {
          		a.SalesApptCancelledFlag__c = IntegrationConstants.CANCELLED_BEFORE;
          	}else{
            	a.SalesApptCancelledFlag__c = IntegrationConstants.CANCELLED_AFTER;
          	}
          	a.SalesAppointmentDate__c = null;
          	update a;
          	if(fieldSalesAppointmentTelemarDecommission.value__c.toUpperCase().equalsIgnoreCase('FALSE')){
          		try{
             		TelemarGateway.cancelAppointment(a, e);
              	}
           		catch(exception ex){
                	System.debug('### error in cancel appointment '+ex);
              	}
          	}
        }
        catch (Exception ex){
        	return ex.getMessage() + ' \n '+ ex.getStackTraceString();
        	system.debug( '\n ERROR ==> '+ex.getStackTraceString()+'\n' );
        }
        return null;
      }
	
	public static String cancelActivationAppointment(Event e, Account a){
	    try{
            setEventToCanceled(e);
            updateEvent(e, a);
	    }
	    catch(Exception ex){
            return ex.getMessage() + ' \n '+ ex.getStackTraceString();
	    }
	    return null;
	}
	
	/**
		setEventToCanceled	Sets status of event to cancelled, and populates/updates relevant fields
		@param	e	The event being cancelled.
	*/
	public static void setEventToCanceled(Event e) {
        e.Status__c = STATUS_CANCELED;
        e.Subject = 'CANCEL ' + e.Subject;
        e.ShowAs = SHOW_AS_FREE;
        User u = [Select Name from User where Id = :e.OwnerId];
        e.OwnerWhenCanceled__c = u.Name;
        isCancelEventContext = true;
	}
	
	/**
		createAdminAppointment	Create unavailable time for users
		@param	e	Event to create
		@param	users	Users to assign to event
		@return	Returns error message on failure, null on success
	*/
	public static String createAdminAppointment(Event e, List<User> users){
		return createAdminAppointment(e, users, null);
	}
	
	/**
		createAdminAppointment	Create unavailable time for users
		@param	e	Event to create
		@param	users	Users to assign to event
		@param	managerid	The id of the manager assigning the admin time
		@return	Returns error message on failure, null on success
	*/
	//HRM-8623 Admin Blocks - Remove Telemar Dependency 
	public static String createAdminAppointment(Event e, List<User> users, Id managerId){
    	try{
			//sync, not manager
			// needed to add profile helper check because new logic -- manager can create admin time for subordinates without creating for himself
			if (managerId == null && users.size() == 1 && !ProfileHelper.isManager()) {	//sync telemar admin time
				try {
				   	 //e.ScheduleID__c = String.valueof(system.now().getTime()).subString(0,10);
				    createEvent(e);
					TelemarGateway.createUnavailableTime(users[0].Id, e.Id,  IntegrationConstants.TRANSACTION_NEW);
				}
				catch (IntegrationException ie) {
					return null;
				}
				catch (Exception ex) {
					return null;
				}
				return null;
			}

			Map<User, Event> userEventMap = new Map<User,Event>();
			
			//insert event before calling telemar gateway
			List<Event> events = new List<Event>();
			for (User u : users){
				Event eclone = e.clone(false);
				eclone.OwnerId = u.Id;
				events.add(eclone);
				userEventMap.put(u, eclone);
			}
			insert events;
				
			//async for users, sync for manager
			if (users != null && users.size() > 0) {
				try{
					TelemarGateway.createUnavailableTime(userEventMap);
				}
				catch (Exception ex){
					return null;
				}
			} 
			
			//finally do manager sync create
			if (managerId != null) {
				createEvent(e);
			}
		}
		catch (Exception ex){
			return null;
		}
		return null;
	}
	
	
	/**
		updateAdminAppointment	Update an admin time
		@param	e	Event to update
		@param	u	User assigned to event
		@return	Returns error message on failure, null on success
	*/
	//HRM-8623 Admin Blocks - Remove Telemar Dependency
	public static String updateAdminAppointment(Event e, User u){
		try{
		    if(hasDataChanged(e)){
    		    updateEvent(e);
    			TelemarGateway.updateUnavailableTime(u.id, e.id);
		    }
		}catch (IntegrationException ie){
			return ie.getMessage();
		}catch (Exception ex){
			return prepareError();
		}
		return null;
	}
	
	@TestVisible       
    private static Boolean hasDataChanged(Event e) {
        if (e.Id == null) return true;
        System.debug('$$$ NEw Event ' + e);
        Event eventInDB = [SELECT StartDateTime, EndDateTime, PostalCode__c, Description FROM Event WHERE Id = :e.Id];
        System.debug('$$$ DB Event ' + e);
        return (
            eventInDB.StartDateTime.format() != e.StartDateTime.format() ||
            eventInDB.EndDateTime.format() != e.EndDateTime.format() ||
            eventInDB.PostalCode__c != e.PostalCode__c ||
            (eventInDB.Description != e.Description && !( Utilities.isEmptyOrNull(eventInDB.Description) && Utilities.isEmptyOrNull(e.Description) ) )
        );
    }
	
	/**
		cancelAdminAppointmnet	cancel an admin time
		@param	e	Event to cancel
		@return	Returns error message on failure, null on success
	*/
	//HRM-8623 Admin Blocks - Remove Telemar Dependency
	public static String cancelAdminAppointment(Event e)
	{
		try{
		    TelemarGateway.cancelUnavailableTime(e.id);
		}
		catch(exception ae){
		}
		return null;
	}
	
	
	/** END GENERIC CALL OUT METHODS **/
	
	public static String validateEvent(Event e){
		Map<Id,String> rtmap = Utilities.getRecordTypesForObject('Event');
		return validateEvent(e, rtmap.get(e.RecordTypeId));
	}
	
	public static String validateEvent(Event e, List<User> selectedusers){
		Map<Id,String> rtmap = Utilities.getRecordTypesForObject('Event');
		return validateEvent(e, rtmap.get(e.RecordTypeId), selectedusers);
	}
	
	public static String validateEvent(Event e, String rtname){
		return validateEvent(e, rtname, null);
	}
	
	public static String validateEvent(Event e, String rtname, List<User> selectedusers){
		String err;
		//get error map
		Map<String, ErrorMessages__c> errormap = ErrorMessages__c.getAll();
		Boolean isManager = ProfileHelper.isManager();
		
		Boolean valid = true;
		
		if (valid) {	// don't overwrite the error message, if one exists, no need for extra checks
			//end time must be after start time
			if (e.EndDateTime <= e.StartDateTime)
			{
				valid = false;
				//err = 'The Event end date and time must occur after the start date and time.';
				err = errormap.get('EM_date_order').Message__c;
			}
		}
		
		if (valid) {
			// start time must not be in the past
			if (e.StartDateTime < DateTime.now() && e.Id == null) {
				valid = false;
				//EM_startdate_past
				err = errormap.get('EM_startdate_past').Message__c;
			}
		}
		
		if (valid) {
			//check for time conflicts
			//multi-user admin times should not check conflicts with events on manager's calendar
			Id mid = null;
			List<Id> usercheck = new List<Id>();
			if (isManager && rtname.equalsIgnoreCase(RecordTypeName.ADMIN_TIME)){
				mid = UserInfo.getUserId();
				for (User u : selectedUsers){
					if (u.Id != mid){
						usercheck.add(u.Id);
					}
				}
			}else {
				usercheck.add(UserInfo.getUserId());
			}
			
			List<Event> timeconflicts = new List<Event>([
				select Id, StartDateTime, EndDateTime from Event
				where Id <> :e.Id
				AND OwnerId <> :mid
				AND OwnerId IN :usercheck
				AND Status__c <> :STATUS_CANCELED
				AND ShowAs <> :SHOW_AS_FREE		
				AND (
					(StartDateTime > :e.StartDateTime and EndDateTime < :e.EndDateTime) or
					(StartDateTime < :e.EndDateTime and EndDateTime > :e.EndDateTime) or
					(StartDateTime < :e.StartDateTime and EndDateTime > :e.StartDateTime) or
					(StartDateTime < :e.StartDateTime and EndDateTime > :e.EndDateTime) or 
					(StartDateTime = :e.StartDateTime and EndDateTime = :e.EndDateTime)
				)
			]);
			
			if (timeconflicts.size() > 0){
				valid = false;
				//err = 'The time block requested is unavailable. Please select a different time block and try again.';
				err = errormap.get('EM_conflict').Message__c;
			}
		}
		
		if (valid) {
			//admin time -- max for users is two 2-hour time blocks a day (4 hours, 2 blocks)
			//should not validate if created by manager (check for new event or created user != current user)
			if (rtname != null && rtname.equalsIgnoreCase(RecordTypeName.ADMIN_TIME) && !isManager && (e.Id == null || (e.CreatedById == UserInfo.getUserId()))){
				List<Id> uids = new List<Id>();
				if (isManager && selectedusers.size()>0){
					Id mid = UserInfo.getUserId();
					for (User u : selectedusers){
						if (u.Id != mid){
							uids.add(u.Id);
						}
					}
				}
				//multi-user admin times should not check conflicts with events on manager's calendar
				if (!isManager){
					uids.add(UserInfo.getUserId());
				}
				//check max admin time
				String eid = (e.Id == null ? '' : e.Id);
				List<Event> adminevents = new List<Event>([
					select Id, OwnerId, CreatedById, StartDateTime, EndDateTime
					from Event
					where OwnerId IN :uids
					and RecordType.DeveloperName = :RecordTypeDevName.ADMIN_TIME
					and CALENDAR_YEAR(StartDateTime) = :e.StartDateTime.year()
					and CALENDAR_MONTH(StartDateTime) = :e.StartDateTime.month()
					and DAY_IN_MONTH(StartDateTime) = :e.StartDateTime.day()
					and Id <> :eid
				]);
				
				//SOQL does not do field comparison
				//so loop through adminevents and remove events where ownerid and createdbyid are not the same
				for (Integer i=adminevents.size()-1; i>=0; i--) {
					if (adminevents[i].OwnerId != adminevents[i].CreatedById) {
						adminevents.remove(i);
					}
				}
				
				// this needs to be done for each of the selected users, so I need to make a map and then loop through
				Map<Id, List<Event>> ownerEventMap = new Map<Id, List<Event>>();
				
				Long totalms = e.EndDateTime.getTime() - e.StartDateTime.getTime();
				Double hours = totalms / 1000.0 / 60.0 / 60.0;
				if (hours > 2) {	// a single block can't exceed 2 hours
					valid = false;
				}else{	// make sure there are only up to 2 timeblocks for the day
					//add event being inserted/updated to event map (should not be included in existing events query -- this is to account for time updates)
					ownerEventMap.put(UserInfo.getUserId(), new List<Event>{e});
					for (Event ae : adminevents){
						if (!ownerEventMap.containsKey(ae.OwnerId)){
							ownerEventMap.put(ae.OwnerId, new List<Event>{ae});
						}else{
							ownerEventMap.get(ae.OwnerId).add(ae);
						}
					}
					for (Id owner : ownerEventMap.keySet()){
						totalms = 0;
						for (Event ae : ownerEventMap.get(owner)){
							totalms += ae.EndDateTime.getTime() - ae.StartDateTime.getTime();
						}
						hours = totalms / 1000.0 / 60.0 / 60.0;
		
						if (hours > 4){
							valid = false;
							//err = 'A maximum of two 2 hour time blocks are allowed per day. The time block requested cannot be created.';
						}
					}
				}
				
				if (!valid){
					err = errormap.get('EM_adminmax').Message__c;
				}
			}
		}
		
		if (valid) {
			//self generated appointment must be at least 2 hours long
			if (rtname != null && rtname.equalsIgnoreCase(RecordTypeName.SELF_GENERATED_APPOINTMENT) && String.isBlank(e.Quote_Id__c)){
				Double hours = Double.valueof( (e.EndDateTime.getTime() - e.StartDateTime.getTime()) / 1000 / 60 / 60 );
				if (hours < 2){
					valid = false;
					//err = 'A minimum of a 2-hour Time block is required for creating a Self-Generated Appointment. Please change the Start/End Times and try again.';
					err = errormap.get('EM_sgmin').Message__c;
				}
			}
		}
		return err;
	}
	
	public static Event getInstallAppointmentForAccount(Id accountID) {
		List<Event> events = null;
		Event evt = null;
		try {
			RecordType rt = Utilities.getRecordType(RecordTypeName.INSTALL_APPOINTMENT, 'Event');
		
			events = [select Id, DurationInMinutes, StartDateTime, TaskCode__c, InstallTechnician__c, Status__c, Subject, OwnerID, OwnerWhenCanceled__c
		    			from Event 
		           		where WhatId = : accountID AND Status__c <> :STATUS_CANCELED AND 
		           		RecordTypeId = : rt.Id 
		           		order by StartDateTime desc];
		}catch (Exception e){
			// No action needed; simply will return null object
		}  
		
		if(events != null && events.size() > 0){
			evt = events[0];
		}         
		return evt;
	}
	
	public static Event getEventByScheduleID(String scheduleID) {
		Event evt = null;
		
		try {
			evt = [select Id, WhatId, ShowAs, DurationInMinutes, StartDateTime, TaskCode__c, ScheduleID__c, InstallTechnician__c, Status__c,  
						Subject, OwnerID, OwnerWhenCanceled__c, IsReminderSet, ReminderDateTime, Description, RecordTypeId, WhoId
		    			from Event 
		           		where ScheduleID__c = : scheduleID];
		} catch (Exception e) {
			// Do nothing; proceed with a null Event
		}
		return evt;
	}
	
	public static Event getEvent(String eventID) {
		Event evt = null;
		try {
			evt = [select Id, WhatId, ShowAs, DurationInMinutes, StartDateTime, EndDateTime, TaskCode__c, ScheduleID__c, InstallTechnician__c,    
						Status__c, Subject, OwnerID, OwnerWhenCanceled__c, IsReminderSet, ReminderDateTime, Description, RecordTypeId, WhoId
		    			from Event 
		           		where Id = : eventID];
		}catch (Exception e) {
			// Do nothing; proceed with a null Event
		}
		return evt;
	}
	
	/**
		syncEventAccountOwner	Synchronize event owner ids for a list of accounts
		@param	accounts	List of accounts that have a new owner
		@return	Returns null if successful, error message otherwise
	*/
	public static String syncEventAccountOwner(List<Account> accounts) {
		List<Event> events = new List<Event>([
			Select Id, OwnerId, WhatId from Event where WhatId IN :accounts
		]);
		//update event owners
		for (Account a : accounts){
			for (Event e : events){
				system.debug(e.WhatId == a.Id);
				if (e.WhatId == a.Id){
					e.OwnerId = a.OwnerId;
				}
			}
		}
		try {
			update events;
		} catch (Exception ex) {
			return ex.getMessage();
		}
		return null;
	}
	
	private static String prepareError(){
		//get error map
		Map<String, ErrorMessages__c> errormap = ErrorMessages__c.getAll();
		return errormap.get('EM_Telemar').Message__c;
	}
	
	/**
	updateObservedValues - Update Observed Start Time and Observed End Time for Events.
	@param Events - List of Events that are created the previous day.
	@return - void
	*/
	public static void updateObservedValues(List<Event> events){
		List<Event> updateEventsList = new List<Event>();
		for(Event eve : events){			
			if(String.valueOf(eve.WhatId).toLowerCase().startsWith(Schema.Sobjecttype.Account.getKeyPrefix().toLowerCase())){	
				try{
					LocationDataProvider ldp = LocationDataFactory.getProvider();
					Integer EventStartTimeOffset = Integer.valueOf(ResaleGlobalVariables__c.getinstance('EventStartTimeOffset').value__c);
					Integer EventEndTimeOffset = Integer.valueOf(ResaleGlobalVariables__c.getinstance('EventEndTimeOffset').value__c);
					
					map<String,List<LocationItem>> stopInfoMap = ldp.getStopLocations(eve.StartDateTime.addMinutes(EventStartTimeOffset), eve.EndDateTime.addMinutes(EventEndTimeOffset), eve.DevicePhoneNumber__c);			
				
					Account acct = SearchManager.getAccount(eve.WhatId); 
					
					if(acct != null && !stopInfoMap.isEmpty() && acct.Latitude__c != null && acct.Longitude__c != null && acct.Latitude__c != 0 && acct.Longitude__c != 0){
						Datetime ActualStartTime, ActualEndTime; 		
						Decimal AcceptableDistance = Decimal.valueOf(ResaleGlobalVariables__c.getinstance('AcceptableDistanceForTelenavStopReport').value__c);
						
						list<LocationItem> startGPSLocationItemList = stopInfoMap.get(LocationDataProvider.START_GPS); 
						list<LocationItem> endGPSLocationItemList = stopInfoMap.get(LocationDataProvider.END_GPS);
						Boolean NoStop = false;
						
						if(startGPSLocationItemList != null && startGPSLocationItemList.size() > 0){
							for(LocationItem l : startGPSLocationItemList){
								Decimal howFarAway = MapUtilities.getDistance(Decimal.valueOf(l.pLatitude), Decimal.valueOf(l.pLongitude), acct.Latitude__c, acct.Longitude__c);			
								if(howFarAway <= AcceptableDistance){
									ActualStartTime = l.pDateTime;
									break;						
								}					
							}	
						}			
						if(endGPSLocationItemList != null && endGPSLocationItemList.size() > 0){
							for(LocationItem l : endGPSLocationItemList){
								Decimal howFarAway = MapUtilities.getDistance(Decimal.valueOf(l.pLatitude), Decimal.valueOf(l.pLongitude), acct.Latitude__c, acct.Longitude__c);				
								if(howFarAway <= AcceptableDistance){
									ActualEndTime = l.pDateTime;						
								}else{
									break;
								}		
							}	
						}
						
						if(startGPSLocationItemList == null || endGPSLocationItemList == null){
							NoStop = true;
						}
						
						if(NoStop){
							setOnTimeIndicator(eve, null, NoStop);
							setObservedDuration(eve, null, NoStop);
						}else{
							if(ActualStartTime != null){
								eve.ObservedStartDateTime__c = 	ActualStartTime;
								long delay = (ActualStartTime.getTime() - eve.StartDateTime.getTime()) / 60000;	
								setOnTimeIndicator(eve, delay, NoStop);	
														
								if(delay <= 0 && ActualEndTime != null){
									eve.ObservedEndDateTime__c = ActualEndTime;
									long duration = (ActualEndTime.getTime() - eve.StartDateTime.getTime()) / 60000;
									setObservedDuration(eve, duration, NoStop);
								}else if(delay > 0 && ActualEndTime != null){
									eve.ObservedEndDateTime__c = ActualEndTime;
									long duration = (ActualEndTime.getTime() - ActualStartTime.getTime()) / 60000;
									setObservedDuration(eve, duration, NoStop);
								}						
							}else{
								setOnTimeIndicator(eve, null, NoStop);
								setObservedDuration(eve, null, NoStop);
							}
						}					
					}	
					updateEventsList.add(eve);
				}
				catch(Exception ex){
					system.debug('Exception: '+ex.getMessage()+' \n'+ex.getStackTraceString());
				}			
			}			
		}	
		
		if(updateEventsList != null && updateEventsList.size() > 0){
			update updateEventsList;
		}	
	}
	
	public static void setObservedDuration(Event eve, long duration, Boolean NoStop){
		if(NoStop){
			eve.ObservedDuration__c = DATA_NOT_AVAILABLE;
		}
		else{
			if(duration == null){
				eve.ObservedDuration__c = NOT_AT_LOCATION;
			}else if( duration < 30){
				eve.ObservedDuration__c = LESS_THAN_30_MINS;
			}else if( duration < 60){
				eve.ObservedDuration__c = BETWEEN_30_AND_60_MINS;
			}else if( duration < 90){
				eve.ObservedDuration__c = BETWEEN_60_AND_90_MINS;
			}else if( duration > 90){
				eve.ObservedDuration__c = MORE_THAN_90_MINS;
			}			
		}		
	}
	
	public static void setOnTimeIndicator(Event eve, long delay, Boolean NoStop){
		if(NoStop){
			eve.OnTimeIndicator__c = DATA_NOT_AVAILABLE;
		}
		else{
			if(delay == null){
				eve.OnTimeIndicator__c = NOT_AT_LOCATION;
			}else if( delay < 5){
				eve.OnTimeIndicator__c = ONTIME;
			}else if( delay < 10){
				eve.OnTimeIndicator__c = LATE_BY_5_TO_10_MINS;
			}else if( delay < 20){
				eve.OnTimeIndicator__c = LATE_BY_10_TO_20_MINS;
			}else if( delay > 20){
				eve.OnTimeIndicator__c = LATE_BY_MORE_THAN_20_MINS;
			}				
		}		
	}
	
	/**
        cancelEvent Handles logic for cancellation of different event types
        
        @param	e	Event to cancel
        
        @return	Returns boolean representing successful event cancelation
    */
    public static Boolean cancelEvent(Event e)
    {
    	Boolean success = true;
   
        //get record type name
        Map<Id,String> recmap = Utilities.getRecordTypesForObject('Event');
        String recname = recmap.get(e.RecordTypeId);
        
        if (recname == null) return false;

    	system.debug('recname = ' + recname);
    	
    	// DLL appointment
    	if(String.isNotBlank(e.Quote_Id__c) && String.isNotBlank(e.Appointment_Type__c) && e.Appointment_Type__c == 'In House Activation'){
           String err = EventManager.cancelActivationAppointment( e, [SELECT name FROM Account WHERE Id = :e.WhatId]);
           success = (err == null);
    	}
        //sf only
        else if (recname.equalsIgnoreCase(RecordTypeName.SALESFORCE_ONLY_EVENT)){
            //delete event
            try{
                delete e;
            }catch (Exception ex){
            	system.debug('Error = ' + ex.getMessage());
            	success = false;
            }
        }
        else if (recname.equalsIgnoreCase(RecordTypeName.ADMIN_TIME)){
        	String err = EventManager.cancelAdminAppointment(e);
        	success = (err == null);
        }
        else if (recname.equalsIgnoreCase(RecordTypeName.SELF_GENERATED_APPOINTMENT)){
        	Account a = [Select Id, TelemarAccountNumber__c, SalesAppointmentDate__c, Data_Source__c, ReferredBy__c,
        	             Type, AccountStatus__c,Rep_User__r.Profile.Name, OutboundTelemarAccountNumber__c,dnis__c from Account where Id = :e.WhatId];
        	String err = EventManager.cancelAppointment(e, a);
        	success = (err == null);
        }
        // cg, or install appointment
        else if (recname.equalsIgnoreCase(RecordTypeName.COMPANY_GENERATED_APPOINTMENT) ||
                 recname.equalsIgnoreCase(RecordTypeName.INSTALL_APPOINTMENT) ){
            EventManager.setEventToCanceled(e);
            try{
            	Id OwnerBeforeCancel = e.OwnerId;            	
                update e;
    			UserAppointmentCount.setAppointmentUserAggregate(e.Id, OwnerBeforeCancel, false);
            } 
            catch (Exception ex){
            	system.debug('Error = ' + ex.getMessage());
            	success = false;
            }
        }
        return success;
    }
    
   /**
	*getAcctCityState retrieves City and State for an appointment
	*and added to the subject line.
	*@param	ID
	*/	
	public static String getAcctCityState(ID idWhat){
		String sCity = null;
 		String sState = null;
 		try{
 			Account a = [Select siteCity__c, SiteState__c from Account where Id = :idWhat];
 			sCity = a.SiteCity__c;
 			sState = a.SiteState__c;
 		}
 		catch (Exception ex){
 			return ' ';
 		}			
 		return ' - ' + sCity + ', ' + sState;
	}
}