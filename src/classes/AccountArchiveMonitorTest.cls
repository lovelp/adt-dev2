/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : AccountArchiveMonitorTest is a test class for AccountArchiveMonitor.apxc
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Jason Pinlac      07/11/2016      - Origininal Version
*
*                           
*/
@isTest
private class AccountArchiveMonitorTest {
    
    static testMethod void testAccountArchiveMonitor() {
    
       
    TestHelperClass.createReferenceDataForTestClasses();  
        
        AccountArchiveMonitor aam = new AccountArchiveMonitor();
    //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year  
    //* Specifies all values. For example, if Month is specified as *, the job is scheduled for every month.
        String chron = '0 0 23 * * ?';
        
        Test.startTest();
    //System.schedule(jobName, cronExp, schedulable)        
        System.schedule('Test Schedule', chron, aam);
    Test.stopTest();     
    }
}