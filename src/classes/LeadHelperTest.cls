/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LeadHelperTest {

    static testMethod void testDeleteOldResidentialAndBuilderLeads() {
        System.runAs(TestHelperClass.createAdminUser())
        {
            TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();        
        Lead l;
        User u;
        StreetSheetItem__c ssitem;
        Disposition__c d;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            l = TestHelperClass.createLead(u, false);
            l.Business_Id__c = '1100 - Residential';
            l.CreatedDate = DateTime.now().addDays(-181);
			l.NewMoverType__c = 'RL';
            l.LeadSource = 'BUDCO';
            insert l;
            d = new Disposition__c();
            d.LeadID__c = l.Id;
            insert d;
            StreetSheet__c ss = new StreetSheet__c();
            insert ss;
            ssitem = new StreetSheetItem__c();
            ssitem.LeadID__c = l.Id;
            ssitem.StreetSheet__c = ss.Id;
            insert ssitem;
        }
        
        test.startTest();
            System.runAs(TestHelperClass.createUser(20001))
            {
                LeadHelper.deleteOldResidentialLeads();
                LeadHelper.deleteOldResidentialBuilderLeads();
            }           
        test.stopTest();
        
        List<Lead> leads = new List<Lead>([select Id, CreatedDate, Business_Id__c, LeadSource from Lead where Id = :l.Id]);
        List<Disposition__c> disps = new List<Disposition__c>([select Id from Disposition__c where Id = :d.Id]);
        List<StreetSheetItem__c> ssitems = new List<StreetSheetItem__c>([select Id from StreetSheetItem__c where Id = :ssitem.Id]);
        
        if (leads.size() > 0)
            system.debug('lead = ' + leads[0]);
        
        system.assertEquals(0, leads.size());
        system.assertEquals(0, ssitems.size());
        system.assertEquals(0, disps.size());
    }
    
    static testMethod void testsetDnis() {
    		Profile p = [select id from profile where name='ADT NA Sales Representative'];
    		User NewRepUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='NewRep1', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com', EmployeeNumber__c='T' + 1234678,
            StartDateInCurrentJob__c = Date.today());
            
            insert NewRepUser;  		
    		
    		TestHelperClass.createReferenceDataForTestClasses();
    		
    		list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
    		rgvs.add(new ResaleGlobalVariables__c(Name='DataRecastDisableAccountTrigger', value__c='FALSE'));
			rgvs.add(new ResaleGlobalVariables__c(Name='IntegrationUser', value__c='Integration User'));
			rgvs.add(new ResaleGlobalVariables__c(Name='BypassQuoteSecurityProfiles', value__c='System Administrator'));
			rgvs.add(new ResaleGlobalVariables__c(Name='PastDueAllowedOrdertype', value__c='R2,A1,N4'));
		    rgvs.add(new ResaleGlobalVariables__c(Name='GlobalResaleAdmin', value__c='Global Admin'));
		    insert rgvs;
    		
    		//create accounts
    		Account acc = new Account();
            acc=TestHelperClass.createAccountData();
            //acc.SiteStreet__c='1501 Yamato Rd';
            acc.ShippingStreet = '8952 Brook Rd';
            acc.ShippingStreet2__c = '';
            acc.ShippingCity = 'McLean';
            acc.ShippingState = 'VA';
            acc.ShippingPostalCode = '221o2';
            acc.FirstName__c = 'Unit_Test';
            acc.LastName__c = 'Execution_';
            acc.MMBCustomerNumber__c='401093713';
            acc.MMB_Past_Due_Balance__c=740.00;
            acc.MMBSiteNumber__c='47453518';
            acc.channel__c='Resi Resale';
            acc.MMB_Multisite__c=false;
            acc.MMB_Relo_Customer__c=false;
            acc.MMB_Disco_Site__c=false;
            acc.MMBOrderType__c='R2';
            acc.Phone='7324317789';
            acc.MMBDisconnectDate__c = null;
            acc.MMB_Relo_Address__c = '';
            acc.MMB_Relo_Site_Number__c = '';
            acc.email__c='test@test.com';
            acc.DOB_encrypted__c = '01/02/2017';
            acc.MMBLookup__c=true;
            acc.MMBBillCodes__c='MONBA';
            acc.MMBOrderType__c ='R1';
            acc.Data_Source__c ='Amplifinity';
            //acc.Rep_HRID__c='125977';
            //acc.Rep_Phone__c='8176899075';
            acc.TelemarAccountNumber__c='39463559';
            acc.Profile_YearsInResidence__c='1';
            acc.recordtypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
            update acc;
            
            Lead_Convert__c lc = new Lead_Convert__c();
            lc.Name ='AMP REF';
            lc.SourceNo__c='655';
            insert lc;
            
            Lead_Convert__c lc1 = new Lead_Convert__c();
            lc1.Name ='AMP SGL';
            lc1.SourceNo__c='654';
            insert lc1;
            
            
            DNIS__c dd = new DNIS__c();
		    dd.Name = 'SFAMP00001';
		    dd.Tel_Lead_Source__c=lc1.Id;
		    
		    insert dd;
		    Map<string,DNIS__C> strMap = new Map<string,DNIS__C>();
		    strMap.put('SFAMP00001',dd);
		    test.startTest();	     
		    	LeadHelper.setDnis(acc,'Amplifinity',NewRepUser,strMap);
	     	test.stopTest();
            
    }
    
}