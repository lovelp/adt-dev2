/************************************* MODIFICATION LOG ********************************************************************************************
* WebCustomerQuestions
*
* DESCRIPTION : Main controller used in MATRIX flow to keep a session of the agent for the current call.
*
* Abhinav Pandey                09/02/2019          HRM-10519           -Original Version : Configurator Screen on discovery page
* Jitendra Kothari				10/10/2019			HRM-10967			Configurator Cheetahmail Requirement
*/

global class WebCustomerQuestions {
  
    public Map<String,List<CustomerConfigWrapper>> configMap {get;set;}
    public String rowJson{get;set;}
    public Boolean initialized =false;
    public Boolean showEmptyError {get;set;}
    public Boolean displaySearchPanel {get;set;}
    public String accountComponent{
       get;
        set {
            accountComponent = value;
            if (!initialized) {
                // componenet setter method  to assign account id values.
                initializeThatDependsOnReceivedString();
                initialized = true;
            }
        }
    }

    List<Call_Data__c> callDataList;
  
      //wrapper class 
       public Class CustomerConfigWrapper implements comparable {
        public String recordId {get;set;}
        public String uniqueVisitorNumber {get;set;}
        public String friendlyId {get;set;}
        public String entity {get;set;}
        public String callData {get;set;}
        public String entityType {get;set;}
        public String lastModifiedDate {get;set;}
        public String sequenceNumber {get;set;}
        public String response {get;set;}
        public CustomerConfigWrapper(){
            friendlyId = '';
            recordId ='';
            callData ='';
            uniqueVisitorNumber = '';
            entity ='';
            entityType='';
            sequenceNumber= '';
            response = '';
        }
        //compare to interface to sort based on sequence.
        public Integer compareTo(Object compareTo){
            Integer returnValue = 0;
            CustomerConfigWrapper wrapperObj = (CustomerConfigWrapper)compareTo;
            if(Integer.valueOf(wrapperObj.sequenceNumber) > Integer.valueOf(sequenceNumber)){
                return -1;
            }
            else if(Integer.valueOf(wrapperObj.sequenceNumber) < Integer.valueOf(sequenceNumber)){
                return 1;
            }
            return returnValue;
        }
    }
    
    //empty contructor to call setter method for accoutn id assignment.
    public WebCustomerQuestions() {
               
    }
        
    //method called from setter to assign account id.    
    public void initializeThatDependsOnReceivedString() { 
        User loggedInUser = [Select Id, Profile.Name From User Where Id =: UserInfo.getUserId()];
        displaySearchPanel = (loggedInUser != null && loggedInUser.Profile.Name.contains('NSC')) ? true : false;
        initializeQuestionAnswers(); 
    }
    
    //method to get customer questions and sort them based on sequence number and last modified date. 
    public void initializeQuestionAnswers(){
        String searchString = ' ';
        configMap = new Map<String,List<CustomerConfigWrapper>>();
        callDataList = new List<Call_Data__c>();
        List<DateTime> lastModifiedStringList = new List<DateTime>();
        List<CustomerInterestConfig__c> customerConfigList = new List<CustomerInterestConfig__c>();
        Set<Id> callDataId = new Set<Id>();
        try{
            // Call Data based on account id.
            callDataList = [select id,name from Call_Data__c where UniqueVisitorId__c != null and Account__c =: accountComponent];    
            for(Call_Data__c c:callDataList){
                 callDataId.add(c.id);
            }
            //START HRM-10967
            set<String> displayType = new set<String>();
            for (Interest_Type__mdt mapping : [SELECT Type__c FROM Interest_Type__mdt WHERE Is_Display__c = true]) {
				displayType.add(mapping.Type__c);
			}
			//END HRM-10967
            // Query to get web customer questions.
            customerConfigList = [select id,ConfiguratorVersion__c,CallData__c,QuestionText__c,Type__c,uniqueVisitorNumber__c,lastModifiedDate,SequenceNumber__c,Response__c,FriendlyId__c FROM CustomerInterestConfig__c 
            						where CallData__c != null and CallData__c in:callDataId  
            						AND Type__c IN :displayType//HRM-10967
            						ORDER BY LastModifiedDate DESC];
            
            for(CustomerInterestConfig__c customerConfig : customerConfigList){
                CustomerConfigWrapper customerConfigObj = new CustomerConfigWrapper();
                customerConfigObj.uniqueVisitorNumber = String.isNotBlank(customerConfig.uniqueVisitorNumber__c) ? customerConfig.uniqueVisitorNumber__c : '';
                customerConfigObj.friendlyId = customerConfig.FriendlyId__c;
                customerConfigObj.entity = customerConfig.QuestionText__c;
                customerConfigObj.entityType = customerConfig.Type__c;
                customerConfigObj.recordId = customerConfig.id;
                customerConfigObj.callData = customerConfig.CallData__c;
                customerConfigObj.response = customerConfig.Response__c;
                customerConfigObj.sequenceNumber = customerConfig.SequenceNumber__c;
                //if key exists in the map.
                if(configMap.containsKey(customerConfig.uniqueVisitorNumber__c+'--'+customerConfig.FriendlyId__c)){  
                    List<CustomerConfigWrapper> customerConfigWrapperTemp = configMap.get(customerConfig.uniqueVisitorNumber__c+'--'+customerConfig.FriendlyId__c);
                    customerConfigWrapperTemp.add(customerConfigObj);
                    configMap.put(customerConfig.uniqueVisitorNumber__c+'--'+customerConfig.FriendlyId__c,customerConfigWrapperTemp);
                }
                // record creation for the first time. 
                else{
                    customerConfigObj.lastModifiedDate = customerConfig.lastModifiedDate.format();
                    configMap.put(customerConfig.uniqueVisitorNumber__c+'--'+customerConfig.FriendlyId__c, new List<CustomerConfigWrapper> { customerConfigObj});
                }
            }
            
            //serialize to pass it to the page.
            rowJson = JSON.serialize(configMap);            
        }catch(exception ex){
            showEmptyError = true;
            System.debug('exception'+ex);
        }
        
    }
    @RemoteAction
    global static String filterResultsOfSearch(String searchString,String accountId){
        String rowJson='';
        Map<String,List<CustomerConfigWrapper>> configMap = new Map<String,List<CustomerConfigWrapper>>();
        List<Call_Data__c> callDataList = new List<Call_Data__c>();
        List<CustomerInterestConfig__c> customerConfigList = new List<CustomerInterestConfig__c>();
        List<CustomerInterestConfig__c> searchResultsWithoutCallData = new List<CustomerInterestConfig__c>();
        Set<Id> callDataId = new Set<Id>();
        try{
            // get call data with respect to account id.
            callDataList = [select id,name from Call_Data__c where UniqueVisitorId__c != null and Account__c =:accountId];    
            for(Call_Data__c c:callDataList){
                 callDataId.add(c.id);
            }
            //START HRM-10967
            set<String> displayType = new set<String>();
            for (Interest_Type__mdt mapping : [SELECT Type__c FROM Interest_Type__mdt WHERE Is_Display__c = true]) {
				displayType.add(mapping.Type__c);
			}
			//END HRM-10967 
            //if search string is not empty get web questions based on the friendly id.      
            if(String.isNotBlank(searchString)){
                searchResultsWithoutCallData = [select id,ConfiguratorVersion__c,CallData__c,QuestionText__c,Type__c,uniqueVisitorNumber__c,lastModifiedDate,SequenceNumber__c,Response__c,FriendlyId__c FROM CustomerInterestConfig__c 
                									where CallData__c = null AND FriendlyId__c =: searchString  
                									AND Type__c IN :displayType//HRM-10967
                									ORDER BY LastModifiedDate DESC];
                if(searchResultsWithoutCallData.size() > 0){
                    for(CustomerInterestConfig__c citConfig : searchResultsWithoutCallData){
                        customerConfigList.add(citConfig);
                    }
                }
            }
            
            String uniqueVisitorFriendlyId = '';
            for(CustomerInterestConfig__c customerConfig : customerConfigList){
                CustomerConfigWrapper customerConfigObj = new CustomerConfigWrapper();
                customerConfigObj.uniqueVisitorNumber = String.isNotBlank(customerConfig.uniqueVisitorNumber__c) ? customerConfig.uniqueVisitorNumber__c : '';
                customerConfigObj.friendlyId = customerConfig.FriendlyId__c;
                customerConfigObj.entity = customerConfig.QuestionText__c;
                customerConfigObj.entityType = customerConfig.Type__c;
                customerConfigObj.recordId = customerConfig.id;
                customerConfigObj.response = customerConfig.Response__c;
                customerConfigObj.callData = customerConfig.CallData__c;
                customerConfigObj.lastModifiedDate = customerConfig.lastModifiedDate.format();
                customerConfigObj.sequenceNumber = customerConfig.SequenceNumber__c;
                if(configMap.containsKey(customerConfig.uniqueVisitorNumber__c+'--'+customerConfig.FriendlyId__c)){  
                    List<CustomerConfigWrapper> customerConfigWrapperTemp = configMap.get(customerConfig.uniqueVisitorNumber__c+'--'+customerConfig.FriendlyId__c);
                    customerConfigWrapperTemp.add(customerConfigObj);
                    configMap.put(customerConfig.uniqueVisitorNumber__c+'--'+customerConfig.FriendlyId__c,customerConfigWrapperTemp);
                }
                else{
                    if(String.isBlank(uniqueVisitorFriendlyId )){
                        uniqueVisitorFriendlyId = customerConfig.uniqueVisitorNumber__c+'--'+customerConfig.FriendlyId__c;
                        configMap.put(customerConfig.uniqueVisitorNumber__c+'--'+customerConfig.FriendlyId__c, new List<CustomerConfigWrapper> { customerConfigObj});
                    }
                }
            }
            rowJson = JSON.serialize(configMap); 
        }catch(exception ex){
            System.debug('exception'+ex);
        }
        return rowJson;
    }
    
    public PageReference saveAttachQuestions() {
        try{
            System.debug('calling save');
            String saveString = Apexpages.currentPage().getParameters().get('questionsMap');
            String accountComponent = Apexpages.currentPage().getParameters().get('accountCompId');
            Map<String,object> saveMap = (Map<String,object>)JSON.deserializeUntyped(saveString);
            Set<String> customerQuestionIdSet = new Set<String>();
            List<Call_Data__c> callDataList = new List<Call_Data__c>();
            List<Call_Data__c> callDataListToUpdate = new List<Call_Data__c>();
            List<CustomerInterestConfig__c> customerConfigList = new List<CustomerInterestConfig__c>();
            List<CustomerInterestConfig__c> customerConfigListUpdate = new List<CustomerInterestConfig__c>();
            
            //get set of id of web questions to be saved which do not have a call data.
            for(String key : saveMap.keyset()){
                List<Object> oList = (List<Object>)saveMap.get(key);
                for(Object oo : oList){
                    CustomerConfigWrapper wrapperObj = (CustomerConfigWrapper)JSON.deserialize(JSON.serialize(oo),CustomerConfigWrapper.class);
                    if(String.isBlank(wrapperObj.callData)){
                        System.debug('### id set');
                        customerQuestionIdSet.add(wrapperObj.recordId);
                    }
                }
            }
            if(customerQuestionIdSet.size() > 0){
                // get the current call data.
                callDataList = [select id from Call_Data__c where Account__c =: accountComponent];
                if(callDataList.size() > 0 ){
                    // get all the records of web questions to be saved.
                    customerConfigList =[select id,ConfiguratorVersion__c,CallData__c,QuestionText__c,Type__c,uniqueVisitorNumber__c,lastModifiedDate,SequenceNumber__c,Response__c,FriendlyId__c FROM CustomerInterestConfig__c where id IN: customerQuestionIdSet ];
                    for(CustomerInterestConfig__c configRecord : customerConfigList){
                        CustomerInterestConfig__c con = new CustomerInterestConfig__c(id=configRecord.id);
                        con.CallData__c = callDataList[0].id;                       
                        callDataList[0].UniqueVisitorId__c = configRecord.uniqueVisitorNumber__c;
                        customerConfigListUpdate.add(con);
                        System.debug('### updating');
                    }
                    if(callDataListToUpdate.size() == 0){
                             callDataListToUpdate.add(callDataList[0]);
                             update callDataListToUpdate;
                    }
                    update customerConfigListUpdate;
                } 
            }
        }catch(exception ex){
            System.debug('### cannot save details'+ex);
        }
        return null;
    }
}