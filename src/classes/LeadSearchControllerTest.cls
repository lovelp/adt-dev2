/************************************* MODIFICATION LOG ********************************************************************************************
* LeadSearchControllerTest
*
* DESCRIPTION : test coverage for 
*  1. LeadSearchController
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                        DATE              TICKET            REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari                 05/26/2019                          Original
*/
@isTest
private class LeadSearchControllerTest {
    static Address__c addr;
    static Lead L1;
    static User adminuser;
    static testMethod void myUnitTest() {
        TestHelperClass.createrehashOwnerfortestclasses();
        adminuser = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        
        System.runAs(adminuser){
            createTestData();
            Test.startTest();
            LeadSearchController.createLead(L1, addr.Street__c, '',addr.City__c,addr.state__c,addr.PostalCode__c, true);
            LeadSearchController.searchLead(L1, addr.Street__c, '',addr.City__c,addr.state__c,addr.County__c, addr.PostalCode__c);
            LeadSearchController.getPickListValuesList('Lead', 'Channel__c');
            LeadSearchController.getUser();
            String Id = TestHelperClass.inferPostalCodeID('32223', '1100');
            insert L1;
            LeadSearchController.getExistingLead(L1.Id);
            LeadSearchController.getExistingLead('Test');
            
            insert addr;
            MelissaDataAPI.MelissaDataResults mdr = LeadSearchController.checkForMelissaData(addr);
            LeadSearchController.updateInvalidAddress(mdr, addr);  
            LeadSearchController.updateLead(L1, addr);
            LeadSearchController.doLeadUpdate(null, false);
            
            delete [select id from Address__c];
            MelissaDataAPI.MelissaDataResults melissaDataResults = new MelissaDataAPI.MelissaDataResults();
            melissaDataResults.lat = '0.00';
            melissaDataResults.lon = '0.00';
            melissaDataResults.Suite = 'Test Suite';
            melissaDataResults.Addr1 = 'Test Address';
            melissaDataResults.ResultCodes = new List<String>{'1', '2'};
                LeadSearchController.createAdress(L1, melissaDataResults, true);  
            addr = [SELECT Id,OriginalAddress__c,Validated__c ,Street__c,Street2__c,StreetNumber__c,StreetName__c,CHS__c,
                    City__c,County__c,State__c,PostalCode__c,PostalCodeAddOn__c,CountryCode__c,
                    Latitude__c,Longitude__c FROM Address__c WHERE PostalCode__c =: addr.PostalCode__c];                   
            
            Test.stopTest();
        }
    }
    static void createTestData(){
        TestHelperClass.createReferenceDataForTestClasses();
        TestHelperClass.createAlphaconversionsettingvalues();
        
        ResaleGlobalVariables__c rgv = [Select id,Name,Value__c from ResaleGlobalVariables__c where name='NewRehashHOARecordOwner'];
        rgv.Value__c ='unitTestrehashRep1@testorg.com';
        update rgv;
        
        addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '22102';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 22102';
        //addr.CHS__c = true;
        
        
        list<Lead> lstLds = new list<Lead>();
        //String Id = TestHelperClass.inferPostalCodeID('22102', '1100');
        
        L1 =TestHelperClass.createLead(adminuser,false,'');
        L1.Business_Id__c = '1100 - Residential';
        lstLds.add(L1);
        //insert lstLds;
    }
    static testMethod void myUnitUpdateTest() {
        TestHelperClass.createrehashOwnerfortestclasses();
        adminuser = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        
        System.runAs(adminuser){
            createTestData();
            Test.startTest();
            MelissaDataAPI.MelissaDataResults melissaDataResults = new MelissaDataAPI.MelissaDataResults();
            melissaDataResults.lat = '0.00';
            melissaDataResults.lon = '0.00';
            melissaDataResults.Suite = 'Test Suite';
            melissaDataResults.Addr1 = 'Test Address';
            melissaDataResults.ResultCodes = new List<String>{'1', '2'};
                LeadSearchController.doLeadUpdate(L1, true);
            Test.stopTest();
        }
    }
}