/************************************* MODIFICATION LOG ********************************************************************************************
* EventController
*
* DESCRIPTION : Controller for manage events page
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  1/25/2012               - Original Version
* Jaydip Bhattacharya           6/6/2013                - removed line to null out LeadExternal Id
* Mike Tuckman                  8/15/13                 - Changed to assign normal task code for RIF accounts and modify event desc
*                                                   
*/

public with sharing class EventController {
    
    public Event e {get;set;}                           //event record
    public Boolean editMode {get;set;}                  //View or edit mode?
    public Boolean canEdit {get;set;}                   //Can the event be edited (based on record type)
    public Boolean waiting {get;set;}                   //Can the event be edited (based on record type)
    public Boolean isNew {get;set;}                     //Create or update
    public Boolean isManager {get;set;}                 //is the logged in user a manager?
    public Boolean isTelemarSync {get;set;}             //if telemar needs to be kept in sync with updates and actions
    public Boolean isDLLAppointment {get;set;}
    public List<SelectOption> users {get;set;}          //manager and subordinates
    public List<Id> selectedusers {get;set;}            //users selected by manager
    public String pageBlockTitle {get;set;}             //title of the page block (depends on record type)
    public Boolean ongoingEvent {get;set;}              //event is currently taking place
    
    public List<DateTime> hours {get;set;}              //Hour display for calendar view
    public String startDate {get;set;}                  //starting date string for calendar view
    public DateTime sd {get;set;}                       //starting datetime for calendar view
    public List<UserDay> userdays {get;set;}            //list of selected users' schedules for event start date time
    public String selUsersStr {get;set;}                //selected users string
    
    //field visibility
    public Boolean showTaskCode {get;set;}              //display task code on page
    public Boolean showPostalCode {get;set;}            //display postal code on page
    public Boolean showScheduleID {get;set;}            //display schedule id on page
    public Boolean showInstallTech {get;set;}           //display install tech on page
    public Boolean showUserSelect {get;set;}            //show user select section
    public Boolean canEditSubject {get;set;}            //can the user edit the subject
    public Boolean isAdminTime {get;set;}
    public Boolean isReloCertification {get;set;}
    
    private ApexPages.Standardcontroller sc;
    private RecordType rt;
    private String returnUrl;
    private Map<Id, User> usermap;
    
    // date / times
    public Date eStartDate {get;set;}
    public String eStartTime {get;set;}
    public Date eEndDate {get;set;}
    public String eEndTime {get;set;}
    public Date eReminderDate {get;set;}
    public String eReminderTime {get;set;}

    
    public EventController( Apexpages.Standardcontroller stdController )
    {
        //by default all activities are sync with Telemar
        isTelemarSync = true;
        isDLLAppointment = false;
        //read appointment type if sent to this page
        String appointment_type = ApexPages.currentPage().getParameters().get('appointment_type');
        if( appointment_type == 'DLL' ){
            isDLLAppointment = true;
        }
        
        //accept standard controller
        sc = stdController;

        //check if create or update
        isNew = sc.getId() == null;
        
        //set event
        e = (Event)sc.getRecord();
        
        //for some reason, certain events aren't coming in with all fields, check for that and requery
        if (e.Id != null) {
            try {
                e = [Select e.WhoId, e.WhatId, e.Description, e.CreatedById,
                            e.TaskCode__c, e.Subject, e.Status__c, e.StartDateTime, 
                            e.ShowAs, e.ScheduleID__c, e.ReminderDateTime, e.RecordTypeId, 
                            e.OwnerId, e.IsReminderSet, e.InstallTechnician__c, e.EndDateTime,CreatedByRepDep__c,
                            e.PriorAppointmentStartDateTime__c, e.Rescheduler__c, e.TimesAppointmentRescheduled__c,  //HRM# 5893: Field Reschedule Appointment -  Changes Made by TCS
                            e.PostalCode__c, e.Quote_Id__c, e.Appointment_Type__c, e.InstallEndDateTime__c, e.Channel__c, e.OrderType__c
                        From Event e 
                        where e.Id = :e.Id
                    ];
                    eStartDate = e.StartDateTime.date();
                    eStartTime = timeToStr(e.StartDateTime.time());
                    eEndDate = e.EndDateTime.date();
                    eEndTime = timeToStr(e.EndDateTime.time());
                    eReminderDate = e.ReminderDateTime.date();
                    eReminderTime = timeToStr(e.ReminderDateTime.time());
            } catch (Exception ex1) {
                system.debug(ex1.getMessage());
            }
        }

        //this is an express activation appointment, do not sync with Telemar
        if( (!String.isBlank(e.Quote_Id__c) && !String.isBlank(e.Appointment_Type__c) && e.Appointment_Type__c.equalsIgnoreCase('In House Activation')) ||
             (!String.isBlank(appointment_type) && appointment_type == 'DLL') ) {
            isTelemarSync = false;
            isDLLAppointment = true;
        }
        //Added for HRM-6682 RLC appointments will not synced to telemar
        if(String.isNotBlank(e.Appointment_Type__c) && e.Appointment_Type__c.equalsIgnoreCase('ReloCertificate') &&
            String.isNotBlank(e.TaskCode__c) && e.TaskCode__c.equalsIgnoreCase('RLC')){
            isTelemarSync = false;
            isReloCertification = true;
          
        }

        //get record type
        String rtid;
        if (e.RecordTypeId == null) {
            rtid = ApexPages.currentPage().getParameters().get('RecordType');
            e.RecordTypeId = rtid;
        }
        else {
            rtid = e.RecordTypeId;
        }
        
        if (rtid == null)
        {
            rt = Utilities.getRecordType(RecordTypeName.ADMIN_TIME, 'Event');
            e.RecordTypeId = rt.Id;
        }
        else
        {
            Map<Id, String> recmap = Utilities.getRecordTypesForObject('Event');
            String rtname = recmap.get(rtid);
            rt = Utilities.getRecordType(rtname, 'Event');
        }
        
        //set defaults for new event
        if (isNew)
        {
            setDefaults();
        }
        
        //check record type and set editable
        if ((rt != null && rt.Name != null && 
            (                
                rt.Name.equalsIgnoreCase(RecordTypeName.INSTALL_APPOINTMENT)
            ))
            || e.EndDateTime < DateTime.now()
        )
        {
            canEdit = false;
        }
        else
        {
            canEdit = true;
            waiting = false;
        }
        
        ongoingEvent = e.Id != null && e.StartDateTime < DateTime.now() && e.EndDateTime > DateTime.now();
        
        //check for edit mode in query string
        String mode = ApexPages.currentPage().getParameters().get('mode');
        editmode = (mode != null && mode.equalsIgnoreCase('edit') && canEdit);
        
        returnUrl = ApexPages.currentPage().getParameters().get('retURL');
        if (returnUrl == null) returnUrl = '/home/home.jsp';
        
        //set record type name as page block title
        if( appointment_type == 'DLL' ){
            pageBlockTitle = 'DLL Appointment';
        }
        else{
            pageBlockTitle = rt.Name;
        }
        
        //check for manager
        isManager = ProfileHelper.isManager();
        isAdminTime = rt.Name.equalsIgnoreCase( RecordTypeName.ADMIN_TIME );
        showUserSelect = isManager && isNew && isAdminTime;
        
        if ( showUserSelect )   //get users manager can create admin time for
        {
            usermap = new Map<Id, User>();
            users = new List<SelectOption>();
            User current = [Select Id, Name, EmployeeNumber__c, isActive from User where Id = :UserInfo.getUserId()];
            users.add(new SelectOption(current.Id,current.Name));
            usermap.put(current.Id,current);
            
            List<User> subordinates = Utilities.getManagersAndUserSubordinates(new Set<Id>{UserInfo.getUserId()});
            for (User u : subordinates)
            {
                if (u.IsActive) {
                    users.add(new SelectOption(u.Id,u.Name));
                    usermap.put(u.Id, u);
                }
            }
        }
        
        setFieldVisibility(rt.Name);
        
        //setup calendar view
        setUserEvents();
        
    }
    

    
    public PageReference doSave()
    {
            if (!validateInputs()) {
                return null;
            }
            
            Map<String, ErrorMessages__c> errormap = ErrorMessages__c.getAll();
            PageReference returl;
            String err;
            
            //get selected user list
            Boolean isManagerSelected = false;
            List<User> selusers = new List<User>();
            // do not add manager to user list
            Id mid = null;
            if (isManager) {
                mid = UserInfo.getUserId();
                if (selectedusers != null && usermap != null) {
                    mid = UserInfo.getUserId();
                    for (Id uid : selectedusers) {
                        if (uid == mid) {
                            isManagerSelected = true;
                        }
                        else if (usermap.containsKey(uid)) {
                            selusers.add(usermap.get(uid));
                        }
                    }
                }
            }
    
            // validate event
            err = EventManager.validateEvent(e, rt.Name, selusers);
            if (err != null)
            {
                addError(err);
                return null;
            }
            
            //check record type and whether record is new, and call proper event manager method
            if (rt.Name.equalsIgnoreCase( RecordTypeName.SELF_GENERATED_APPOINTMENT ))
            {
                Account a;
                Lead l;
                if (isNew)
                {
                    if (e.WhoId != null)
                    {
                        l = [Select Id,name,LeadManagementId__c,TelemarAccountNumber__c, FirstName, LastName, Company, SiteStreet__c, SiteCity__c, 
                             SiteStateProvince__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, Phone, Email, QueriedSource__c,
                             DispositionComments__c, ReferredBy__c, Channel__c, Business_ID__c,  Affiliation__c, LeadSource, SiteValidated__c, SiteStreet2__c, AddressID__c 
                             from Lead where Id = :e.WhoId];
                        //err = EventManager.createSGAndConvertLead(e, l);
                        //HRM-8349
                        string errMsg = AccountAppointmentCreation.accountAvailable(l);
                        if(errMsg!=null && errMsg!=''){
                            if(errMsg.containsIgnoreCase('an account already exists')){
                                err = errMsg;
                            }
                            else{
                                err = EventManager.createSGAndConvertLead(e, l);
                            }
                        }
                        else{
                            err = EventManager.createSGAndConvertLead(e, l);
                        }
                        //End HRM 8349
                    }
                    else if (e.WhatId != null)
                    {
                        a = [Select Id, TelemarAccountNumber__c, Rep_User__c,Rep_User__r.Profile.Name,SalesAppointmentDate__c, FirstName__c, LastName__c, Name, 
                             SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, 
                             SiteCountryCode__c, Phone, Email__c, DispositionComments__c, QueriedSource__c, ReferredBy__c, MMBOrderType__c,
                             Channel__c, Business_ID__c, ProcessingType__c, TaskCode__c, TransitionDate2__c, LeadExternalID__c, DNIS__c, DNIS_Modified_Date__c,DNIS_Modifier_Name__c,
                             Data_Source__c, Type, AccountStatus__c, OutboundTelemarAccountNumber__c, Affiliation__c, RifID__c, SiteValidated__c, SiteStreet2__c 
                             from Account where Id = :e.WhatId];
                        //Set task code
                        String selTaskCode;
                        Map<String, TaskCodeEventRecordTypeMap__c> tascodeevenMap  = TaskCodeEventRecordTypeMap__c.getAll();
                        for(TaskCodeEventRecordTypeMap__c tcer:tascodeevenMap.values()){
                        if(tcer.Channel__c != null && tcer.Channel__c == a.Channel__c)
                            selTaskCode = tcer.TaskCode__c;
                        }
                        e.Channel__c = a.Channel__c;
                        e.OrderType__c = String.isBlank(a.MMBOrderType__c)?'N1':a.MMBOrderType__c;
                        e.TaskCode__c = selTaskCode;     
                        if( isTelemarSync ){
                            err = EventManager.createAppointmentForAccount(e, a);
                        }
                        else{
                            try{
                                EventManager.createEvent(e);
                            }
                            catch(Exception e){
                                err = e.getMessage();
                            }
                        }     
                        // processing type changes to User unless this is a RIF account
                        if (a.ProcessingType__c != IntegrationConstants.PROCESSING_TYPE_RIF) {
                            a.ProcessingType__c = IntegrationConstants.PROCESSING_TYPE_USER;
                        }   
                        // could be scheduling for account converted from a lead which will have a Prospect to Appt Transition Date
                        // or a resale account which will not have a Prospect to Appt Transition Date
                        // so set this value to today only if there is not a value already
                        if (a.TransitionDate2__c == null) {
                            a.TransitionDate2__c = system.today();
                        }   
                        // a will be updated below
                    }
                }
                else
                {
                    a = [Select Id, TelemarAccountNumber__c, SalesAppointmentDate__c,Rep_User__c,Rep_User__r.Profile.Name, FirstName__c, LastName__c, Name, 
                         SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, 
                         Phone, Email__c, DispositionComments__c, QueriedSource__c, ReferredBy__c, Channel__c, Business_ID__c, 
                         ProcessingType__c, TaskCode__c, TransitionDate2__c, LeadExternalID__c, Data_Source__c, SiteValidated__c, 
                         Type, AccountStatus__c, OutboundTelemarAccountNumber__c, Affiliation__c, RifID__c, SiteStreet2__c,DNIS__c, DNIS_Modified_Date__c,DNIS_Modifier_Name__c 
                         from Account where Id = :e.WhatId];
                    if( isTelemarSync ){
                        err = EventManager.updateAppointment(e, a);
                    }
                    else{
                        try{
                            EventManager.updateEvent(e, a);
                        }
                        catch(Exception e){
                            err = e.getMessage();
                        }
                    }     
                }
                if (err == null) {
                    // check if lead was converted
                    if (a == null) {
                        a = [Select Id, DNIS__c, DNIS_Modified_Date__c,DNIS_Modifier_Name__c,SalesAppointmentDate__c,Rep_User__c,Rep_User__r.Profile.Name, ProcessingType__c, TaskCode__c, SiteCity__c, SiteState__c, TransitionDate2__c, LeadExternalID__c, Type from Account where Id = :e.WhatId];
                    }
                    a.SalesAppointmentDate__c = e.StartDateTime;   
                    if (EventUtilities.shouldSetTaskCodeForAccount(e.TaskCode__c, a.Type)) {  
                        a.TaskCode__c = e.TaskCode__c;
                    }   
                   //Jaydip B modified below for srint#11.
                   // a.LeadExternalID__c = null;
                    update a;

                    returl = new Pagereference('/' + e.WhatId);
                }
            }
            else if (rt.Name.equalsIgnoreCase(RecordTypeName.ADMIN_TIME))
            {
                User u = [Select Id, EmployeeNumber__c, TimeZoneSidKey from User where Id = :UserInfo.getUserId()];
                if (isNew)
                {
                    if (isManager)
                    {
                        if (isManagerSelected) {
                            err = EventManager.createAdminAppointment(e, selusers, mid);
                        } else {
                            err = EventManager.createAdminAppointment(e, selusers);
                        }
                    }
                    else
                    {
                        err = EventManager.createAdminAppointment(e, new List<User>{u});
                    }
                    
                    if (err == null)
                    {
                        returl = new PageReference('/home/home.jsp');
                    }
                }
                else
                {
                    err = EventManager.updateAdminAppointment(e, u);
                }
            }
            else if(rt.Name.equalsIgnoreCase(RecordTypeName.COMPANY_GENERATED_APPOINTMENT))
            {
                    Account a = [Select Id, TelemarAccountNumber__c,Rep_User__c,Rep_User__r.Profile.Name, SalesAppointmentDate__c, FirstName__c, LastName__c, Name, 
                         SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, 
                         Phone, Email__c, DispositionComments__c, QueriedSource__c, ReferredBy__c, Channel__c, Business_ID__c, 
                         ProcessingType__c, TaskCode__c, TransitionDate2__c, LeadExternalID__c, Data_Source__c, DNIS__c, DNIS_Modified_Date__c,DNIS_Modifier_Name__c,
                         Type, AccountStatus__c, OutboundTelemarAccountNumber__c, Affiliation__c, RifID__c 
                         from Account where Id = :e.WhatId];
                    if( isTelemarSync ){
                        err = EventManager.updateAppointment(e, a);
                    }
                    else{
                        try{
                            EventManager.updateEvent(e, a);
                        }
                        catch(Exception e){
                            err = e.getMessage();
                        }
                    }     
            }
            else //bad record type
            {
                err = errormap.get('EM_recordtype').Message__c;
                
            }

            if (err != null && err.startswith('** Request')) {
                addInfo(err);
                waiting = true;
            }
            
            else if (err != null) {
                addError(err);
                waiting = false;
            }

            else if (returl == null) {
                editMode = false;
            }
            
            return returl;
    }
    
    public PageReference doCancel()
    {
        if (e.Id == null)
        {
            return new PageReference(returnUrl);
        }
        editMode = false;
        return null;
    }
    
    public PageReference doEdit()
    {
        editMode = true;
        return null;
    }
    
    public PageReference cancelEvent() {
        if (e.Id == null) return null;
        if(rt.Name.equalsIgnoreCase(RecordTypeName.COMPANY_GENERATED_APPOINTMENT)) {
            addError('Cannot cancel Company Generated Appointment');
            return null;
        }
        PageReference ref = Page.EventCancel;
        ref.getParameters().put('id',e.Id);
        String returl = ApexPages.currentPage().getUrl();
        ref.getParameters().put('returl',returl);
        return ref;
    }
    
    public PageReference startDateChanged()
    {
        setUserEvents();
        return null;
    }
    
    public PageReference userSelectChanged()
    {
        setUserEvents();
        return null;
    }
    
    public static String timeToStr(Time t) {
        DateTime d = DateTime.newInstance(system.today(), t);
        return d.format('hh:mm a');
    }
    
    private boolean validateInputs() {
        String err = null;
        try {
            e.StartDateTime = stringToDatetime(eStartDate, eStartTime);
            // Use same start and end date to restrict user from creating event spanning multiple days
            e.EndDateTime = stringToDatetime(eStartDate, eEndTime);
            e.ReminderDateTime = stringToDatetime(eReminderDate, eReminderTime);
        } catch (Exception ex) {
            err = 'The date and time values must be in the correct format (mm/dd/yyyy) and (hh:mm TT).';
        }
        
        if (e.StartDateTime == null || e.EndDateTime == null || e.ReminderDateTime == null) {
            err = 'The date and time values must be in the correct format (mm/dd/yyyy) and (hh:mm TT).';
        } else if (isAdminTime && isManager && isNew && (selectedusers == null || selectedusers.size() == 0)) {
            err = 'You must select at least one user to schedule admin time for.';
        }
        
        if (err != null) {
            Utilities.addError(err);
        }
        
        if( isDLLAppointment ){
            if( String.isBlank(e.Quote_Id__c) ){
                err = 'Quote ID required for DLL appointments';
            }
            else{
                List<scpq__SciQuote__c> quote = [SELECT id, scpq__Status__c,
                                                  (SELECT id, DLL__c FROM Quote_Jobs__r)
                                           FROM scpq__SciQuote__c 
                                           WHERE account__c= :e.WhatId AND scpq__QuoteId__c = :e.Quote_Id__c ];
                if( quote != null && !quote.isEmpty() ){
                    Boolean quoteQualify = false;
                    for(Quote_Job__c QJ:quote[0].Quote_Jobs__r   ){
                        if( !String.isBlank( quote[0].scpq__Status__c ) && NSCHelper.isDLL(quote[0], QJ.DLL__c) ){
                            quoteQualify = true;
                        }
                    }
                    if( !quoteQualify ){
                        err = 'This quote ID '+e.Quote_Id__c+' does not qualify for DLL appointment';
                    }
                }   
                else {
                    err = 'Quote ID '+e.Quote_Id__c+' not found on this account.';
                }
            }
            
            if( !String.isBlank(err) ){
                addError(err);
            }
        }
        
        return err == null;
    }
    
    private Datetime stringToDatetime(Date d, String tstr) {
        
        return DateTime.parse(d.format() + ' ' + tstr);
    }
    
    private Datetime stringToDatetime(String dstr, String tstr) {
        Date d = Date.parse(dstr);
        return stringToDatetime(d, tstr);
    }
    
    private void setDefaults()
    {
        EventDefaults__c ed = EventDefaults__c.getInstance();
        
        e.StartDateTime = system.now().addMinutes( Integer.valueOf( ed.StartDateTimeOffsetMinutes__c ) );
        e.EndDateTime = e.StartDateTime.addMinutes( Integer.valueOf( ed.DurationMinutes__c ) );
        eStartDate = e.StartDateTime.date();
        eEndDate = e.EndDateTime.date();
        eStartTime = timeToStr( e.StartDateTime.time() );
        eEndTime = timeToStr( e.EndDateTime.time() );
        e.ShowAs = EventManager.SHOW_AS_BUSY;
        e.Status__c = EventManager.STATUS_SCHEDULED;
        e.IsReminderSet = ed.IsReminderSet__c;
        e.ReminderDateTime = e.StartDateTime.addMinutes( -1 * Integer.valueOf( ed.ReminderMinutesBeforeEvent__c ) );
        eReminderDate = e.ReminderDateTime.date();
        eReminderTime = timeToStr( e.ReminderDateTime.time() );
        e.OwnerId = UserInfo.getUserId();
        String lCity = null;
        String lState = null;
        if( isDLLAppointment ){
            e.Appointment_Type__c = 'In House Activation';
        }
        
        if ( rt.Name != null &&
            (
                rt.Name.equalsIgnoreCase(RecordTypeName.SELF_GENERATED_APPOINTMENT) ||
                rt.Name.equalsIgnoreCase(RecordTypeName.ADMIN_TIME)
            )
        )
        {
            e.InstallTechnician__c = null;
        }
        
        if (rt.Name != null && rt.Name.equalsIgnoreCase(RecordTypeName.ADMIN_TIME))
        {
            e.TaskCode__c = 'ADM';
            e.WhatId = null;
        }
        
        if ( rt.Name != null && rt.Name.equalsIgnoreCase(RecordTypeName.SELF_GENERATED_APPOINTMENT) )
        {
            String accountId = ApexPages.currentPage().getParameters().get('def_account_id');
            String leadId = ApexPages.currentPage().getParameters().get('def_lead_id');
            String channel = '';
            String accountType = '';
            String accountStatus = '';
            
            String aName = '';
            
            if (accountId != null)
            {
                e.WhatId = accountId;
                try
                {
                    Account a = [Select DNIS__c, DNIS_Modified_Date__c,DNIS_Modifier_Name__c,Channel__c,Rep_User__c,Rep_User__r.Profile.Name, OwnerId, Name, Type, AccountStatus__c from Account where Id = :accountId];
                    channel = a.Channel__c;
                    accountType = a.Type;
                    accountStatus = a.AccountStatus__c;
                    e.OwnerId = a.OwnerId;
                    aName = a.Name;
                }
                catch (Exception ex) {}
            }
            else if (leadID != null)
            {
                e.WhoId = leadId;
                try
                {
                    Lead l = [Select Channel__c, OwnerId, Company, sitecity__c, SiteStateProvince__c from Lead where Id = :leadID];
                    channel = l.Channel__c;
                    e.OwnerId = l.OwnerId;
                    aName = l.Company;
                    lCity = l.sitecity__c;
                    lState = l.SiteStateProvince__c;
                }
                catch (Exception ex) {}
            }
           
            // RIF accounts and Closed Accounts always use a specific "task code" 
            // MT 8/15 - Removing RIF logic - assigning Task Code like normal for RIF accounts
            //if (accountType == IntegrationConstants.TYPE_RIF || IntegrationConstants.STATUS_CLOSED_VALUES.contains(accountStatus)) {
            //  e.TaskCode__c = IntegrationConstants.TASK_CODE_RIF_VISIT;
            //} else {
                // for other accounts and all leads, use channel / rec type to derive  the appropriate SG task code
                Map<String, TaskCodeEventRecordTypeMap__c> tcmap = TaskCodeEventRecordTypeMap__c.getAll();
                if (channel == null) {
                    channel = '';
                }   
                for (String key : tcmap.keySet())
                {
                    TaskCodeEventRecordTypeMap__c tc = tcmap.get(key);
                    if (
                        rt.DeveloperName != null &&
                        rt.DeveloperName.equalsIgnoreCase(tc.RecordTypeDevName__c) &&
                        channel.equalsIgnoreCase(tc.Channel__c)   
                        )
                    {
                        e.TaskCode__c = tc.TaskCode__c;
                        break;
                    }
                }
            //} 
            // MH - For InHouse activation appointments override TaskCode with DLL as the record type is not evaluated
            if( isDLLAppointment ){
                e.TaskCode__c = 'DLL';
               for( scpq__SciQuote__c quote : [SELECT id, name, scpq__Status__c, createdDate, scpq__QuoteId__c, 
                                          (SELECT id, ParentQuote__r.scpq__QuoteId__c, DLL__c FROM Quote_Jobs__r)
                                   FROM scpq__SciQuote__c 
                                   WHERE account__c= :e.WhatId ORDER BY createddate desc] ){
                    for(Quote_Job__c QJ : quote.Quote_Jobs__r){
                        if( !String.isBlank( quote.scpq__Status__c ) && NSCHelper.isDLL(quote, QJ.DLL__c) && String.isBlank(e.Quote_Id__c) ){
                            e.Quote_Id__c = quote.scpq__QuoteId__c;
                            break;
                        }                    
                    }                                   
                }
            }
            // Adding City and State to the Subject line for appointments.
            String sTempSubjectCityState = EventManager.getAcctCityState(e.WhatId);
            
            // MT 8/15 - Prepending RIF to task code for event description
            String wrkTaskCode = e.TaskCode__c;
            if (accountType == IntegrationConstants.TYPE_RIF || IntegrationConstants.STATUS_CLOSED_VALUES.contains(accountStatus)) {
                wrkTaskCode = IntegrationConstants.TASK_CODE_RIF_VISIT + e.TaskCode__c;
            }

            // MT 8/15 - changed description to use wrkTaskCode instead of e.TaskCode__c
            if (leadID != null){
                e.Subject = EventManager.SUBJECT_PREFIX + wrkTaskCode + ' - ' + lCity + ', ' + lState;
            }else{
                e.Subject = EventManager.SUBJECT_PREFIX + wrkTaskCode + sTempSubjectCityState;  
            }
            
        }
    }
    
    private void setFieldVisibility(String rtname) {
        if (rtname == null) rtname = '';
        
        showTaskCode = true;
        showPostalCode = false;
        showScheduleID = false;
        showInstallTech = false;
        canEditSubject = true;
        //admin time
        if (rtname.equalsIgnoreCase(RecordTypeName.ADMIN_TIME)) {
            showTaskCode = false;
            showPostalCode = true;
            showScheduleID = true;
        }
        else if (rtname.equalsIgnoreCase( RecordTypeName.INSTALL_APPOINTMENT)) {
            showInstallTech = true;
        }
        else if (rtname.equalsIgnoreCase( RecordTypeName.SELF_GENERATED_APPOINTMENT)) {
            canEditSubject = false;
            if( isDLLAppointment ){
                showTaskCode = false;
                showScheduleID = true;
            }
        }
        
    }
    
    private void addInfo(String msg)
    {
        ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.INFO, msg);
        ApexPages.addMessage(m);
    }
    
    private void addError(String msg)
    {
        ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.ERROR, msg);
        ApexPages.addMessage(m);
    }
    
    private void setUserEvents()
    {
        List<String> userids = new List<String>();
        userids.add( (e.OwnerId == null ? UserInfo.getUserId() : e.OwnerId) );
        if (isManager && selUsersStr != null && selUsersStr.length() > 0) // multiple users
        {
            String[] selusers = selUsersStr.split(',');
            userids.addAll(selusers);
        }
        
        List<User> users = new List<User>([
            select Id, Name from User
            where Id IN :userids
            order by LastName asc
        ]);
        
        sd = e.StartDateTime;
        if (startDate != null)
        {
            try
            {
                sd = DateTime.newInstance( Date.parse(startDate), Time.newInstance(0, 0, 0, 0) );
            } catch (Exception ex) {}
        }
        List<Event> events = new List<Event>([
            select Id,Subject,StartDateTime,EndDateTime,OwnerId,TaskCode__c from Event 
            where OwnerId IN :userids
            //Change to Query to properly pull appointments in UTC, as this is the timezone that Salesforce uses for dates
            /*AND CALENDAR_YEAR(StartDateTime) = :sd.year()
            AND CALENDAR_MONTH(StartDateTime) = :sd.month()
            AND DAY_IN_MONTH(StartDateTime) = :sd.day()*/
            AND StartDateTime >= :DateTime.newInstance( sd.date(), Time.newInstance(0, 0, 0, 0) )
            AND StartDateTime < :(DateTime.newInstance( sd.date(), Time.newInstance(0, 0, 0, 0) )+1)
            AND Status__c <> :EventManager.STATUS_CANCELED
            AND ShowAs <> :EventManager.SHOW_AS_FREE
            limit 1000
        ]);
        
        
        //create user events map
        Map<User,List<Event>> uemap = new Map<User,List<Event>>();
        for (User u : users)
        {
            uemap.put(u, new List<Event>());
            for (Event ev : events)
            {
                if (ev.OwnerId == u.Id)
                {
                    uemap.get(u).add(ev);
                }
            }
        }
        
        userdays = new List<UserDay>();
        
        for (User u : uemap.keySet())
        {
            UserDay ud = new UserDay();
            ud.user = u;
            if (hours == null || hours.size() != 48)
                hours = new List<DateTime>();
            List<DayHour> dayhours = new List<DayHour>();
            Time midnight = Time.newInstance(0, 0, 0, 0);
            //what time to start?
            Time t = Time.newInstance(5,0,0,0);
            do
            {
                if (hours == null || hours.size() != 48)
                    hours.add( DateTime.newInstanceGmt(system.today(), t) );
                List<CalEvent> cevents = new List<CalEvent>();
                for (Event ev: uemap.get(u))
                {
                    if (ev.StartDateTime.time() >= t && ev.StartDateTime.time() < t.addMinutes(30))
                    {
                        cevents.add( new CalEvent(ev) );
                    }
                }
                dayhours.add( new DayHour(t, cevents) );
                t = t.addMinutes(30);
            } while (t != midnight);
            ud.userdayhours = dayhours;
            userdays.add(ud);
        }
    }
    
    private class UserDay
    {
        public User user {get;set;}
        public List<DayHour> userdayhours {get;set;}
        
        public UserDay()
        {
            
        }
    }
    
    private class DayHour
    {
        public Time dt {get;set;}
        public List<CalEvent> events {get;set;}
        
        public DayHour(Time dtime, List<CalEvent> hourevents)
        {
            dt = dtime;
            events = hourevents;
        }
    }
    
    private class CalEvent
    {
        public Event e {get;
            set
            {
                e = value;
                calcDur();
                calcHHOffset();
                formatTimeToString();
                tCode = e.TaskCode__c;
            }
        }
        public Double durAsHalfHourPct {get {return dur;}}
        public Double halfhourOffsetPct {get {return hhpct;}}
        public String formattedStartTime {get {return starttime;} }
        public String formattedEndTime {get {return endtime; } }
        public String taskCode {get{return tCode;}}
        
        private Double dur;
        private Double hhpct;
        private String starttime;
        private String endtime;
        private String tCode;
        
        public CalEvent(Event ev)
        {
            e = ev;
        }
        
        private void calcDur()
        {
            Double minutes = (e.EndDateTime.getTime() - e.StartDateTime.getTime()) / 1000 / 60;
            dur = (minutes / 30) * 100;
        }
        
        private void calcHHOffset()
        {
            Integer minute = e.StartDateTime.minute();
            hhpct = 0;
            if (minute > 30)
                hhpct = ((Double)minute - 30) / 30;
            else if (minute > 0 && minute < 30)
                hhpct = (Double)minute / 30;
                
            hhpct = hhpct * 100;
        }
        
        private void formatTimeToString()
        {
            starttime = e.StartDateTime.format('h:mm a');
            endtime = e.EndDateTime.format('h:mm a');
        }
    }
}