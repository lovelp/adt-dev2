/* Class Description: Unit Test class to cover following Apex controller: 
 * DataloadDeleteBatch, DataLoadDeleteBatchScheduler
 * 
 * ---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                  REASON
 * ---------------------------------------------------------------------------------------------------------------------------------------------------
 * 
 * Jitendra Kothari             11/02/2018         - Coverage increase of DataLoadDeleteBatchScheduler
 *
 */
@istest
public class DataloadDeleteBatchTest {

    static testmethod void DeleteDataLoadRecords(){
        List<DataloadTemp__c> dltemp = new List<DataloadTemp__c>();
        for(integer i=0;i<200;i++){
           DataloadTemp__c dlAcc = new DataloadTemp__c();
           	dlAcc=new DataloadTemp__c();
            dlAcc.Type__c='Account';
            dlAcc.BusinessId__c='1100 - Residential';
            dlAcc.MmbSiteNo__c='46995701';
            dlAcc.AddressLine1__c='abc';
            dlAcc.AddressLine2__c='xyz';
            dlAcc.SitePostalCode__c='33029';
            dlAcc.SiteLastName__c='DemoLastName'+i;
            dlAcc.Processed__c=false;
            dlAcc.CreatedDate = System.now()- 34;
            dltemp.add(dlAcc);
        }
        
        if(dltemp!=null && dltemp.size()>0)
            insert dltemp;
        test.startTest();
       	//Database.executeBatch(New DataloadDeleteBatch());
       	DataLoadDeleteBatchScheduler sc = new DataLoadDeleteBatchScheduler();
       	sc.execute(null);
        test.stopTest();
        
    }    
}