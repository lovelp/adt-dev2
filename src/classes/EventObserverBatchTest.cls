/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EventObserverBatchTest {

    static testMethod void testObservedValues() {
        Event e;
        List<Event> eList= new List<Event>();
        Account a;
        User u;
        User current = [select Id from User where ID = :UserInfo.getUserId()];
        system.runas(current) {
        	u = TestHelperClass.createSalesRepUser();
        	a = TestHelperClass.createAccountData();
        	a.OwnerId = u.Id;
        	update a;
        	e = TestHelperClass.createEvent(a, u, false);
        	e.DevicePhoneNumber__c = '5555555555';
        	e.ObservedDuration__c = EventManager.MORE_THAN_90_MINS;
            e.status__c= EventManager.STATUS_SCHEDULED;
        	insert e;
            eList.add(e);
        }
        
        /*test.startTest();
        	EventObserverBatch eob = new EventObserverBatch();
        	eob.query += ' and Id = \'' + e.Id + '\'';
        	system.debug('query = ' + eob.query);
        	Database.executeBatch(eob);
        test.stopTest();
        */
        test.startTest();
        EventObserverBatch eob = new EventObserverBatch();
        //eob.query += ' and Id = \'' + e.Id + '\'';
        Database.QueryLocator ql = eob.start(null);
		eob.execute(null,eList);
		eob.Finish(null);
        test.stopTest();
        
        Event econfirm = [Select Id, ObservedDuration__c from Event where ID = :e.Id];
        system.assertEquals( EventManager.MORE_THAN_90_MINS, econfirm.ObservedDuration__c);
    }
}