/************************************* MODIFICATION LOG ********************************************************************************************
 *
 * DESCRIPTION : DOARequestTriggerHelp.cls has methods that govern behaviour of inserted and updated DOA Request records
 *
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                  REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Mike Tuckman                  08/07/2015          - Original Version
 * Mike Tuckman                  01/08/2016          - Changed Class declaration to without sharing
 * Abhinav Pandey                10/28/2018          - HRM - 8009          - Credit ByPass Process
 * Abhinav Pandey                12/07/2018          - HRM-8637 breakfix Cyber December Release
 */
public without sharing class DOARequestTriggerHelper {
       
   
    //------------------------------------------------------------------------------------------------------------------------------
    //ProcessDOARequestBeforeInsert()
    //------------------------------------------------------------------------------------------------------------------------------

     
    //------------------------------------------------------------------------------------------------------------------------------
    //ProcessDOARequestAfterInsert()
    //------------------------------------------------------------------------------------------------------------------------------
    public static void ProcessDOARequestAfterInsert(List<DOA_Request__c> NewDOARequests) {
        
         for(DOA_Request__c NewDOARequest : NewDOARequests)
        {           
            //HRM-8637 breakfix Cyber December Release
            List<scpq__SciQuote__c> quoteList = [select ID, CreditBypassRequested__c ,CreditBypassStatus__c, DOAApprovalStatus__c, DOALockID__c from scpq__SciQuote__c where ID = : NewDOARequest.Quote__c];
            if ( quoteList != null && quoteList.size() > 0 && NewDOARequest.Approval_Status__c == 'Automatic') {                
                try {
                    if (NewDOARequest.DOALockID__c == quoteList[0].DOALockID__c){
                        quoteList[0].DOAApprovalStatus__c = 'Yes';
                        update quoteList;
                    }
                }
                catch(System.Exception e) {
                    system.debug('[DOARequestTriggerHelper]::[ProcessDOARequestAfterInsert] --- '+e.getMessage()+'\n '+e.getStackTraceString()); 
                }
            }
            
                // HRM - 8009 Bypass Credit 
                //HRM-8637 breakfix Cyber December Release    
            if(quoteList != null && quoteList.size() > 0 && quoteList[0].CreditBypassRequested__c && String.isBlank(quoteList[0].CreditBypassStatus__c)){      
                     quoteList[0].CreditBypassStatus__c = 'Pending';     
                     update quoteList;
            }       
        }


    }

    //------------------------------------------------------------------------------------------------------------------------------
    //ProcessDOARequestBeforeUpdate()
    //------------------------------------------------------------------------------------------------------------------------------
    public static void ProcessDOARequestBeforeUpdate(Map<Id, DOA_Request__c> NewDOARequests, Map<Id, DOA_Request__c> OldDOARequests) {
        
        List<Approval.ProcessSubmitRequest> appReqs = new List<Approval.ProcessSubmitRequest>();

        for(DOA_Request__c NewDOARequest : NewDOARequests.values())
        {           
            DOA_Request__c OldDOARequest = OldDOARequests.get(NewDOARequest.Id);

            if (NewDOARequest.Approval_Status__c <> OldDOARequest.Approval_Status__c) {
                try {
                    DOA_Request__c DOAreqHist = [Select Id, (Select StepStatus, ID, Comments From ProcessSteps order by SystemModstamp desc limit 1) from DOA_Request__c where ID = :NewDOARequest.ID];
                    ProcessInstanceHistory procHist = DOAreqHist.ProcessSteps;
                    NewDOARequest.Approver_Comments__c = procHist.Comments;
                }
                catch(System.Exception e) { 
                    system.debug('[DOARequestTriggerHelper]::[ProcessDOARequestBeforeUpdate] --- '+e.getMessage()+'\n '+e.getStackTraceString()); 
                }
            }
            
        }
        
    }
    


    //------------------------------------------------------------------------------------------------------------------------------
    //ProcessDOARequestAfterUpdate()
    //------------------------------------------------------------------------------------------------------------------------------
    public static void ProcessDOARequestAfterUpdate(Map<Id, DOA_Request__c> NewDOARequests, Map<Id, DOA_Request__c> OldDOARequests) {
        Map<Id, String> doaStatusonAcc = new Map<Id, String>();
        list<Id> accntIds = new list<Id>();
        for(DOA_Request__c NewDOARequest : NewDOARequests.values())
        {           
            DOA_Request__c OldDOARequest = OldDOARequests.get(NewDOARequest.Id);

            if (NewDOARequest.Approval_Status__c <> OldDOARequest.Approval_Status__c && NewDOARequest.Approval_Status__c == 'Cancelled') {
                try {
                    rejectRecord(NewDOARequest.ID);
                }
                catch(System.Exception e) { 
                    system.debug('[DOARequestTriggerHelper]::[ProcessDOARequestAfterUpdate] 1--- '+e.getMessage()+'\n '+e.getStackTraceString()); 
                }
            }
            
            if (NewDOARequest.Approval_Status__c <> OldDOARequest.Approval_Status__c) {
                try {
                    scpq__SciQuote__c quote = [select ID, DOAApprovalStatus__c,CreditBypassStatus__c, CreditBypassRequested__c ,DOALockID__c from scpq__SciQuote__c where ID = : NewDOARequest.Quote__c];
                    if (NewDOARequest.DOALockID__c == quote.DOALockID__c){
                        quote.DOAApprovalStatus__c = NewDOARequest.Approval_Status__c;
                        update quote;
                    }
 
                         //HRM - 8009 ByPass Credit     
                     if(quote.CreditBypassRequested__c && !NewDOARequest.Approval_Status__c.equalsIgnoreCase('PENDING')){        
                         quote.CreditBypassStatus__c = String.isNotBlank(NewDOARequest.Approval_Status__c) && !NewDOARequest.Approval_Status__c.equalsIgnoreCase('Cancelled') && NewDOARequest.Approval_Status__c.equalsIgnoreCase('YES') ? 'Approved' : 'Rejected';     
                         update quote;
                     } 
                }
                catch(System.Exception e) { 
                    system.debug('[DOARequestTriggerHelper]::[ProcessDOARequestAfterUpdate] 2--- '+e.getMessage()+'\n '+e.getStackTraceString()); 
                }
            }
            
            //HRM# 5707 
            if(NewDOARequest.Approval_for_No_Email_in_Account__c == true && NewDOARequest.Approval_Status__c == 'Yes'){
                accntIds.add(NewDOARequest.Account__c);
                doaStatusonAcc.put(NewDOARequest.Account__c, 'Approved');
            } 
            else if(NewDOARequest.Approval_for_No_Email_in_Account__c == true && NewDOARequest.Approval_Status__c == 'No'){
                accntIds.add(NewDOARequest.Account__c);
                doaStatusonAcc.put(NewDOARequest.Account__c, 'Rejected');
            }
            
        }
            if(accntIds.size()>0){
            list<Account> accObjlist = [Select name, Approval_Status__c from Account where ID IN: accntIds];
            for(Account accObj:accObjlist){
                system.debug('==> Value '+doaStatusonAcc.get(accObj.Id));
                accObj.Approval_Status__c = doaStatusonAcc.get(accObj.Id);
            }
            update accObjlist;
        }
        
    }
    


    //------------------------------------------------------------------------------------------------------------------------------
    //
    //------------------------------------------------------------------------------------------------------------------------------
    public static Id getWorkItemId(Id targetObjectId) {

        Id retVal = null;

        for(ProcessInstanceWorkitem workItem  : [Select p.Id from ProcessInstanceWorkitem p
            where p.ProcessInstance.TargetObjectId =: targetObjectId])
        {
            retVal  =  workItem.Id;
        }

        return retVal;
    }

    public static void rejectRecord(ID targetObjectId) {

        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Request removed by trigger action');
        req.setAction('Removed');
        Id workItemId = getWorkItemId(targetObjectId);   

        if(workItemId == null)
        {
            system.debug('** No workItemID');
        }
        else
        {
            req.setWorkitemId(workItemId);
            // Submit the request for approval
            Approval.ProcessResult result =  Approval.process(req);
            system.debug('** Result');
            system.debug(result);
        }
    }
    
}