/* @@ Description: Controller class to support tilapnote agreement page on sites. 
 *
 * 
 *        Developer              Date                Change log
 *-----------------------------------------------------------------
 * Srinivas Yarramsetti     02/12/2019               Initial Verson
 *
 */


public class TilaPnoteController{
    public class GenericException extends Exception{}
    public String citizenOneAgreement{get;set;}
    public String citEcosentForm{get;set;}
    public String citRecuringCreditAgree{get;set;}
    public Boolean isAgreementAlreadysined{get;set;}
    public Boolean isEconsentAgreedAlready{get;set;}//If econsent is already signed - first modal.
    public Boolean istermsconditionsOnlyAcc{get;set;}//If intial terms and conditions agreed - Second modal
    public Boolean isRecurrenCCSigned{get;set;}
    public Boolean noofAttemptsCrossed{get;set;}
    public TilaPnoteController(ApexPages.StandardController controller){}
    public TilaPnoteController(){
        noofAttemptsCrossed = false;
        String loanId = Apexpages.currentPage().getParameters().get('id');
        LoanApplication__c loanApp = returnLoanApp(loanId);
        if(loanApp.TilaPnoteAcceptedDateTime__c == null){
            isAgreementAlreadysined = false;
            if(loanApp.eConsentAcceptedDateTime__c == null){
                isEconsentAgreedAlready = false;
            }
            else{
                isEconsentAgreedAlready = true;
            }
            //Get econsent form from static resource
            StaticResource econsent = [Select Body From StaticResource Where Name = 'CitizenOneEconsentDiv'];
            Blob econscontent = econsent.Body;
            String etermsAndConditions = EncodingUtil.base64Encode(econscontent);
            econscontent = EncodingUtil.base64Decode(etermsAndConditions);
            etermsAndConditions = econscontent.toString();
            citEcosentForm = etermsAndConditions;

            if(loanApp.AgreementAcceptedDateTime__c == null){
               istermsconditionsOnlyAcc = false; 
            }
            else{
                istermsconditionsOnlyAcc = true;
            }
            StaticResource agreement = [Select Body From StaticResource Where Name = 'CitizenOneAgreemDiv'];
            Blob fileContent = agreement.Body;
            String termsAndConditions = EncodingUtil.base64Encode(fileContent);
            fileContent = EncodingUtil.base64Decode(termsAndConditions);
            termsAndConditions = fileContent.toString();
            citizenOneAgreement = termsAndConditions;
            
            if(loanApp.RecurringCCAgreemStamp__c == null){
                isRecurrenCCSigned = false;
            }
            else{
                isRecurrenCCSigned = true;
            }
            StaticResource recagreement = [Select Body From StaticResource Where Name = 'RecurringCreditPaymntAgree'];
            Blob fileContentrec = recagreement.Body;
            String termsAndConditionsRecur = EncodingUtil.base64Encode(fileContentrec);
            fileContentrec = EncodingUtil.base64Decode(termsAndConditionsRecur);
            termsAndConditionsRecur = fileContentrec.toString();
            citRecuringCreditAgree = termsAndConditionsRecur;
        }
        else{
            isAgreementAlreadysined = true;
        }
        if(loanApp.TilaPnoteFailedAttempts__c > Integer.valueOf(FlexFiConfig__c.getInstance('TilePnoteFailedAttempts').value__c)){
             noofAttemptsCrossed = true;
        }
        }
    /*************************************************************************************************************************
     * Description: Below method authenticates the applications;
     * Return type to page: ErrorCode1 - Crossed unscuccesful attemps referred to custom label.TilePnoteFailedAttempts
     *                      ErrorCode2 - Failed Attempt to authenticate.
     *                      ErrorCode3 - Any kind of exception occured.
     *                      ErrorCode4 - last attempt to hit unsuccesful attempts
     * *************************************************************************************************************************/
    @RemoteAction
    public static String displayAgreement(String last4SSN, String dob, String loanId){
        try{
            String errorCode =''; 
            Boolean lastAttempt = false;
            LoanApplication__c loanApp = returnLoanApp(loanId);
            if(loanApp.TilaPnoteFailedAttempts__c > Integer.valueOf(FlexFiConfig__c.getInstance('TilePnoteFailedAttempts').value__c)){
                throw new GenericException('Unsuccesful Attempts Crossed');
            }
            else if(loanApp.TilaPnoteFailedAttempts__c == Integer.valueOf(FlexFiConfig__c.getInstance('TilePnoteFailedAttempts').value__c)-1){
                lastAttempt = true;
            }
            Date dobDate;
            if(dob.contains('/')){
                String[] dateArray = dob.split('/');
                dobDate = Date.newInstance(Integer.valueOf(dateArray[2]), Integer.valueOf(dateArray[0]), Integer.valueOf(dateArray[1]));                
            }
            else{
                String[] dateArray = dob.split('-');
                dobDate = Date.newInstance(Integer.valueOf(dateArray[0]), Integer.valueOf(dateArray[1]), Integer.valueOf(dateArray[2]));
            }
            
            if(loanApp.SSN__c.substring(loanApp.SSN__c.length()-4) == last4SSN && Date.parse(loanApp.DOB__c) == dobDate){
                return String.valueOf(loanApp.installmentAmt__c)+';'+String.valueOf(loanApp.AmountFinanced__c)+';'+loanApp.CFGEmail__c+';'+
                       String.valueOf(loanApp.Term__c);
            }
            else{
                loanApp.TilaPnoteFailedAttempts__c = loanApp.TilaPnoteFailedAttempts__c == null?1:loanApp.TilaPnoteFailedAttempts__c+1;
                update loanApp;
                if(lastAttempt)
                    return 'ErrorCode4';
                else    
                    return 'ErrorCode2';
            }
        }
        catch(Exception ae){
            ADTApplicationMonitor.log ('Error in querying loan App', ae.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);
            return (ae.getmessage()=='Unsuccesful Attempts Crossed'?'ErrorCode1':'ErrorCode3');
        }
    }
    
    @RemoteAction
    public static void eConsentAccepted(String loanId){
        LoanApplication__c loanApp = new LoanApplication__c(Id=loanId, eConsentAcceptedDateTime__c = DateTime.now());
        update loanApp;
    }
    //Remote action for saving terms and conditions
    @RemoteAction
    public static void termsAndConditionsOnly(String loanId){
        LoanApplication__c loanApp = new LoanApplication__c(Id=loanId, AgreementAcceptedDateTime__c = DateTime.now());
        update loanApp;
    }
    //Remote action for accepting all terms and agreements
    @RemoteAction
    public static void acceptAllTerms(String loanId){
        LoanApplication__c loanApp = new LoanApplication__c(Id=loanId, TilaPnoteAcceptedDateTime__c = DateTime.now());
        update loanApp;
    }
    //Remote action for saving recurring payment agreement.
    @Remoteaction
    public static void recurringPayment(String loanId){
        LoanApplication__c loanApp = new LoanApplication__c(Id=loanId, RecurringCCAgreemStamp__c = DateTime.now());
        update loanApp;
    }
    //Query for loan application details.
    public static LoanApplication__c returnLoanApp(String loanAppId){
        return [SELECT id, Account__r.Email__c,CFGEmail__c, Term__c,NeedRemoteSigning__c, SSN__c, DOB__c, eConsentAcceptedDateTime__c, AgreementAcceptedDateTime__c,AmountFinanced__c,installmentAmt__c,TilaPnoteAcceptedDateTime__c, 
                RecurringCCAgreemStamp__c,TilaPnoteFailedAttempts__c FROM LoanApplication__c WHERE id=:loanAppId];
    }
    

}