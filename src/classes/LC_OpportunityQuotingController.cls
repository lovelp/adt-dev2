/************************************* MODIFICATION LOG ********************************************************************************************
 *
 * DESCRIPTION : LC_OpportunityQuotingController to control opty creation for Home Health
 *
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                TICKET             REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Srinivas Yarramsetti         11/08/2019           HRM-11053          - Original Version
*/

public class LC_OpportunityQuotingController {
    
    public static String cpq_prefix = ResaleGlobalVariables__c.getinstance('CPQ_Prefix').value__c;
    public static list<Account> acc;
    public class OpptyWrapper{
        @AuraEnabled public Opportunity opty;
        @AuraEnabled public list<scpq__SciQuote__c> quoteDetails;
        @AuraEnabled public Account acc;
    }
    
    @AuraEnabled
    public static String saveOpportunity(Opportunity opp, String qtURL){
        system.debug('Save new opty'+opp);
        try{
            Boolean recordCreated;
            Database.UpsertResult ur = Database.upsert(opp, false);
            if (ur.isCreated()){
                recordCreated = true;
                }
            OpportunityCPQIntegrationDataGenerator.IntegrationDataSource iSourceOpp = new OpportunityCPQIntegrationDataGenerator.IntegrationDataSource(opp.Id, null);
            opp.OppIntegrationData__c = OpportunityCPQIntegrationDataGenerator.getCPQIntegrationJSON(isourceOpp);
           System.debug('opp--'+opp);
            update opp;
            
            if(opp.Id != null){
                createContact(opp, recordCreated);      
            }
            return newQTURLField(qtURL, opp);
        }catch(Exception ae){
            System.debug('exce--'+ae.getLineNumber()+'--'+ae.getStackTraceString()+'--'+ae.getMessage());
            throw new AuraHandledException('Save Opportunity failed. '+ae.getMessage());
        }
    }
    
    private static void createContact(Opportunity opp, Boolean isInsert){
        Contact con;
        Boolean createCon = false;
        OpportunityContactRole ocr;
        if(acc == null){
            acc = acclist(opp.AccountId);
        }
        try{
            if(isInsert){
                ocr = [SELECT Id, ContactId FROM OpportunityContactRole WHERE OpportunityId =:opp.id AND IsPrimary = true limit 1];
                System.debug('ocr--'+ocr);
                con = new Contact(Id = ocr.ContactId);
            }
            
        } catch(Exception e){
            con = [SELECT Id FROM Contact WHERE AccountId =:acc[0].Id limit 1];
        }           
        if (con == null){
            con = new Contact();
            con.AccountId = acc[0].Id;
            createCon = true;
        }
        
        con.Email = acc[0].Email__c;
        con.FirstName = acc[0].FirstName__c;
        con.LastName = acc[0].LastName__c;
        con.MailingStreet = acc[0].SiteStreet__c;
        con.MailingCity = acc[0].SiteCity__c;
        con.MailingState = acc[0].SiteState__c;
        con.MailingPostalCode = acc[0].SitePostalCode__c;
        con.OtherStreet = acc[0].SiteStreet__c;
        con.OtherCity = acc[0].SiteCity__c;
        con.OtherState = acc[0].SiteState__c;
        con.OtherPostalCode = acc[0].SitePostalCode__c;
        con.Phone = acc[0].Phone;
        if (createCon) insert con;
        else update con;
        Boolean createOcr = false;
        if (ocr == null){
            ocr = new OpportunityContactRole();
            ocr.OpportunityId = opp.Id;
            createOcr = true;
        }
        ocr.IsPrimary = true;
        ocr.ContactId = con.id;
        ocr.Role = 'Decision Maker';
        if (createOcr) insert ocr;
        else update ocr;
    }
    
    public class optyQuoteWrapper{
        @AuraEnabled public String optyId;
        @AuraEnabled public String optyName;
        @AuraEnabled public String accName;
        @AuraEnabled public String closeDate;
        @AuraEnabled public String payType;
        @AuraEnabled public String billingCompoundAddress;
        @AuraEnabled public String billingAddrsId;
        @AuraEnabled public String shippingAddrsId;    
        @AuraEnabled public String shippingCompoundAddress;
        @AuraEnabled public list<quoteWrapper> quotesWraper;
        @AuraEnabled public String newQuoteUrl;
        @AuraEnabled public String accAddrsId;
        @AuraEnabled public String accAddrsStandId;
        @AuraEnabled public Account acc;
        @AuraEnabled public String stageName;
        @AuraEnabled public Boolean newQuoteButton;
        @AuraEnabled public Boolean restricQuote;
    }
    
    public class quoteWrapper{
        @AuraEnabled public String qtName;
        @AuraEnabled public String quoteId;
        @AuraEnabled public String orderType;
        @AuraEnabled public String quoteStatus;
        @AuraEnabled public String jobNumber;
        @AuraEnabled public String lastModifiedDate;
        @AuraEnabled public String installAmountBeforeDiscount;
        @AuraEnabled public String installAmountAfterDiscount;
        @AuraEnabled public String monthlyPay;
        @AuraEnabled public String editURL;
    }
    
    @AuraEnabled
    public static list<optyQuoteWrapper> getOptyList(String recId, String agentType, String qtUrl, String qtId){
        acc = new list<Account>();
        //Get record id based on quote id for phone sales :(
        
        if(String.isBlank(recId) && String.isNotBlank(qtId)){
            recId =  [SELECT Id, scpq__OpportunityId__c FROM scpq__SciQuote__c WHERE Id=:qtId].scpq__OpportunityId__c;
        
        }
        //Get acc rec if recid is acc else its opty
        if(recId.startsWith('001')){acc = acclist(recId); 
        }

        Map<Id, optyQuoteWrapper> optyQuoteMap = new Map<Id, optyQuoteWrapper>();
        list<optyQuoteWrapper> optyQtWraplist = new list<optyQuoteWrapper>();
        for(Opportunity op: optyList(recId)){
            System.debug('opportunity--'+op);                     
            optyQuoteWrapper opqwp = new optyQuoteWrapper();
            opqwp.optyId = op.Id;
            opqwp.optyName = op.Name;
            opqwp.accName = op.Account.Name;
            opqwp.accAddrsId = op.Account.AddressID__c;
            opqwp.accAddrsStandId = op.Account.StandardizedAddress__c;
            opqwp.closeDate = String.valueOf(op.CloseDate);
            opqwp.payType = op.HomeHealthPaytype__c;
            opqwp.billingCompoundAddress = op.BillingCompoundAddress__c;
            opqwp.shippingCompoundAddress = op.ShippingCompoundAddress__c;
            opqwp.billingAddrsId = op.BillingAddress__c;
            opqwp.shippingAddrsId = op.ShippingAddress__c;
            opqwp.newQuoteUrl = newQTURLField(qtUrl, op);
            opqwp.stageName = op.stageName;
            System.debug('acc--'+acc);
			System.debug('op.AccountId--'+op.AccountId);
			System.debug('acclist(op.AccountId)--'+acclist(op.AccountId));
            acc = (acc.size()>0)?acc:acclist(op.AccountId);
            opqwp.acc = acc[0];
            optyQuoteMap.put(op.Id, opqwp);
            optyQtWraplist.add(opqwp);
            
            }
        //Change qtId later to opty or acc id.
        if(optyQtWraplist.size()>0){
            Map<Id, list<quoteWrapper>> quteMap = new Map<Id, list<quoteWrapper>>();
            for(scpq__SciQuote__c scq: [SELECT Id, Name, scpq__OpportunityId__c, scpq__QuoteId__c, MMBOrderType__c, MMBOutOrderType__c, scpq__Status__c, MMBJobNumber__c, LastModifiedDate, 
                                        scpq__QuoteValueBeforeDiscounts__c, Submitted_Date_Time__c, scpq__QuoteValueAfterDiscounts__c, QuoteValueMonthly__c, Account__c, QuotePaidOn__c FROM scpq__SciQuote__c WHERE Id=:qtId]){
                          System.debug('scq---'+scq);
                 if(scq.scpq__OpportunityId__c != null){
                     if(!quteMap.keyset().contains(scq.scpq__OpportunityId__c)){
                         list<quoteWrapper> tempList = new list<quoteWrapper>();
                         tempList.add(makeWrap(scq, Acc[0]));
                         quteMap.put(scq.scpq__OpportunityId__c, tempList);
                     }else{
                         list<quoteWrapper> tempList = new list<quoteWrapper>();
                         tempList.addAll(quteMap.get(scq.scpq__OpportunityId__c));
                         tempList.add(makeWrap(scq, Acc[0]));
                         quteMap.put(scq.scpq__OpportunityId__c, tempList);
                     }
                 }
            }
            Boolean restricQuoting;
            //Handles only single opportunity and first quote. Last min changes.
            for(scpq__SciQuote__c scq: [SELECT Id, scpq__Status__c, scpq__OpportunityId__c, scpq__OpportunityId__r.HomeHealthPaytype__c, Submitted_Date_Time__c FROM scpq__SciQuote__c 
                                                WHERE Account__c =:acc[0].Id AND QuotePaidOn__c != null]){
                system.debug(optyQtWraplist[0].payType+':'+scq.scpq__OpportunityId__r.HomeHealthPaytype__c);                                   
                if(!optyQtWraplist[0].payType.equalsIgnoreCase(scq.scpq__OpportunityId__r.HomeHealthPaytype__c) || (recId == scq.scpq__OpportunityId__c && scq.Submitted_Date_Time__c != null)){
                    restricQuoting = true;
                }
                                                }
            for(optyQuoteWrapper oqw: optyQtWraplist){
                if(quteMap.keyset().contains(oqw.optyId)){
                    oqw.newQuoteButton = true;
                    oqw.quotesWraper = quteMap.get(oqw.optyId);
                }
                oqw.restricQuote = restricQuoting;
                
            }   
            
        }
        
        if(optyQtWraplist.size()==0){
           optyQuoteWrapper opqwp = new optyQuoteWrapper();
           opqwp.acc = acc[0];
           optyQtWraplist.add(opqwp);
           
        }
        system.debug('##'+optyQtWraplist);
        return optyQtWraplist;
        
    }
    
    private static String newQTURLField(String qturl, Opportunity opp){
        String url = qturl;
        url +=  '&'+cpq_prefix + '='+EncodingUtil.urlEncode(opp.Name, 'UTF-8') ;
        url +=  '&' + cpq_prefix+'_lkid='+opp.Id;
        system.debug('$$'+url);
        return url;
    }

    private static quoteWrapper makeWrap(scpq__SciQuote__c quoteObj, Account acc){
        quoteWrapper newQtWrap = new quoteWrapper();
        newQtWrap.qtName = quoteObj.Name;
        newQtWrap.quoteId = quoteObj.scpq__QuoteId__c;
        newQtWrap.orderType = quoteObj.MMBOutOrderType__c;
        if(quoteObj.Submitted_Date_Time__c != null){
            newQtWrap.quoteStatus = 'Submitted';
        }else if(quoteObj.QuotePaidOn__c != null){
            newQtWrap.quoteStatus = 'Paid';
        }else{
            newQtWrap.quoteStatus = 'Created';
        }
        newQtWrap.jobNumber = quoteObj.MMBJobNumber__c;
        newQtWrap.lastModifiedDate = quoteObj.LastModifiedDate.format('yyyy-MM-dd');
        newQtWrap.installAmountBeforeDiscount = String.valueOf(quoteObj.scpq__QuoteValueBeforeDiscounts__c);
        newQtWrap.installAmountAfterDiscount = String.valueOf(quoteObj.scpq__QuoteValueAfterDiscounts__c);
        newQtWrap.monthlyPay = String.valueOf(quoteObj.QuoteValueMonthly__c);
        //newQtWrap.editURL = openQuote(acc); //come later
        return newQtWrap;
    }
    
    @AuraEnabled
    public static Map<String, list<Map<String, String>>> restrictPayType(String recId){
        set<String> quoteOptyType = new set<String>();
        Map<String, list<Map<String, String>>> finalMap = new Map<String, list<Map<String, String>>>();
        Boolean setDefaults = true;
        String setSelToTrue = 'true';
        List<Map<String, String>> items = new List<Map<String, String>>();
        //Check if there is at least one Quote which is in paid status
        list<scpq__SciQuote__c> qutelist = [SELECT Id, scpq__Status__c, scpq__OpportunityId__r.HomeHealthPaytype__c FROM scpq__SciQuote__c 
                                            WHERE (Account__c =:recId OR scpq__OpportunityId__c =:recId) AND QuotePaidOn__c != null LIMIT 1];
        if(qutelist.size()>0){
            for(scpq__SciQuote__c scq:qutelist){
                quoteOptyType.add(scq.scpq__OpportunityId__r.HomeHealthPaytype__c);
            }
            if(quoteOptyType.size() > 0){
                setDefaults = false;
            }
        }
        //Assign the default value if there is a quote in paid status.
        Schema.DescribeFieldResult fieldResult = Opportunity.HomeHealthPaytype__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            if(!setDefaults && quoteOptyType.contains(f.getValue())){
                items.add(new Map<String, String>{'value' => f.getValue(), 'label' => f.getValue(), 'selected' => setSelToTrue});
            }
            else if(setDefaults){
                items.add(new Map<String, String>{'value' => f.getValue(), 'label' => f.getValue(), 'selected' => setSelToTrue}); 
            }
            setSelToTrue = 'false';
        }
        //Add pay type picklist values.
        finalMap.put('PayType', items);
        //Get opty stage from label
        list<Map<String, String>> optyStage = new list<Map<String, String>>();
        String setStageSel = 'true';
        for(String stage:label.HH_OptyStage.split(';')){
            optyStage.add(new Map<String, String>{'value' => stage, 'label' => stage, 'selected' => setStageSel});
            setStageSel = 'false';
        }
        //Add stage picklist
        finalMap.put('OptyStage', optyStage);
        system.debug('##$$'+finalMap);
        return finalMap;
    }

    private static list<Account> acclist(String recId){
        return [SELECT Id, Name, AddressID__c, StandardizedAddress__c, MMBOrderType__c, MMBBillingSystem__c, SitePostalCode__c, PostalCodeID__r.Town_Lookup__r.name, 
                Phone, Email__c, FirstName__c, LastName__c, SiteStreet__c, SiteCity__c, TMTownID__c, TMSubTownID__c, Business_ID__c, Channel__c, SiteState__c, OwnerId, PostalCodeID__c FROM Account WHERE Id=:recId];
    }
    
    private static list<Opportunity> optyList(String recId){
        return [SELECT Id, Name, Accountid, Account.Name, Account.AddressID__c, Account.StandardizedAddress__c, BillingAddress__c, BillingCity__c, BillingPostalCode__c, BillingState__c, 
                             BillingStreet__c,ShippingCompoundAddress__c, BillingStreet2__c, ShippingAddress__c, ShippingCity__c, ShippingPostalCode__c, ShippingState__c, 
                             ShippingStreet__c, ShippingStreet2__c,BillingCompoundAddress__c, CloseDate, stageName, HomeHealthPaytype__c FROM Opportunity WHERE AccountId =:recId OR Id=:recId];
    }

}