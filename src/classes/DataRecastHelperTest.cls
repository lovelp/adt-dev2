/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DataRecastHelperTest {

	// FUNCTIONAL TESTS
	
	static testMethod void testProcessEvents() {
		Account a;
		Event e;
		User u;
		User current = [Select ID from User where ID = :UserInfo.getUserID()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
        	u = TestHelperClass.createSalesRepUser();
        	e = TestHelperClass.createEvent(a, u, false);
        	e.RecordTypeId = null;
        }
        
        test.startTest();
        	DataRecastHelper.processEvents(new List<Event>{e});
        test.stopTest();
        
        RecordType rt = Utilities.getRecordType( RecordTypeName.SALESFORCE_ONLY_EVENT, 'Event' );
        system.assertEquals(rt.Id, e.RecordTypeId);
	}

    static testMethod void testProcessAccounts() {
        Account a;
        Address__c addr;
        User current = [Select ID from User where ID = :UserInfo.getUserID()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
        	a.Channel__c = null;
        	a.Business_Id__c = '1100';
        	// TODO : FILL IN STAGE CRITERIA
        	a.ProcessingType__c = null;
        	// TODO : FILL IN PROC TYPE CRITERIA
        	a.TransitionDate1__c = null;
        	//a.CreatedDate = system.now();
        	a.DispositionCode__c = 'UA - Undeliverable As Addressed';
        	a.LeadExternalID__c = 'a';
        	update a;
        }
        
        test.startTest();
        	DataRecastAccountProcessor dra = new DataRecastAccountProcessor();
        	dra.query += ' and ID IN (\'' + a.Id + '\') limit 200';
        	Database.executeBatch(dra);
        test.stopTest();
        
        Account aconfirm = [select Channel__c, ProcessingType__c, CreatedDate, TransitionDate1__c, DispositionCode__c, DispositionDetail__c, BillingSystem__c, LeadExternalId__c from Account where ID = :a.Id];
        system.assertEquals('Resi Resale', aconfirm.Channel__c);
        system.assertEquals(aconfirm.CreatedDate.date(), aconfirm.TransitionDate1__c);
        system.assertEquals(aconfirm.LeadExternalId__c.substring(0,1), aconfirm.BillingSystem__c);
        system.assertEquals('Field', aconfirm.ProcessingType__c);
        
    }
    
    static testMethod void testProcessAccounts1() {
        Account a;
        Address__c addr;
        User current = [Select ID from User where ID = :UserInfo.getUserID()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
            a.name='Test Acc 1';
        	a.Channel__c = null;
        	a.Business_Id__c = '1200';
        	// TODO : FILL IN STAGE CRITERIA
        	a.ProcessingType__c = null;
        	// TODO : FILL IN PROC TYPE CRITERIA
        	a.TransitionDate1__c = null;
        	//a.CreatedDate = system.now();
        	a.DispositionCode__c = 'UA - Undeliverable As Addressed';
        	a.LeadExternalID__c = 'a';
        	update a;
        }
        
        test.startTest();
        	DataRecastAccountProcessor dra = new DataRecastAccountProcessor('admin');
        	dra.query += ' and ID IN (\'' + a.Id + '\') limit 200';
        	Database.executeBatch(dra);
        
        test.stopTest();
        
    }
    
    static testMethod void testProcessAccounts2() {
        Account a;
        Address__c addr;
        User current = [Select ID from User where ID = :UserInfo.getUserID()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
            a.name='Test Acc 2';
        	a.Channel__c = null;
        	a.Business_Id__c = '1100';
        	// TODO : FILL IN STAGE CRITERIA
        	a.ProcessingType__c = null;
        	// TODO : FILL IN PROC TYPE CRITERIA
        	a.TransitionDate1__c = null;
        	//a.CreatedDate = system.now();
        	a.DispositionCode__c = 'UA - Undeliverable As Addressed';
        	a.LeadExternalID__c = 'a';
        	update a;
        }
        
        test.startTest();
        	DataRecastAccountProcessor dra = new DataRecastAccountProcessor('budco');
        	dra.query += ' and ID IN (\'' + a.Id + '\') limit 200';
        	Database.executeBatch(dra);
        
        test.stopTest();
        
        
    }
    
    static testMethod void testProcessAccounts3() {
        Account a;
        Address__c addr;
        User current = [Select ID from User where ID = :UserInfo.getUserID()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
            a.name='Test Acc 3';
        	a.Channel__c = null;
        	a.Business_Id__c = '1200';
        	// TODO : FILL IN STAGE CRITERIA
        	a.ProcessingType__c = null;
        	// TODO : FILL IN PROC TYPE CRITERIA
        	a.TransitionDate1__c = null;
        	//a.CreatedDate = system.now();
        	a.DispositionCode__c = 'UA - Undeliverable As Addressed';
        	a.LeadExternalID__c = 'a';
        	update a;
        }
        
        test.startTest();
        	DataRecastAccountProcessor dra = new DataRecastAccountProcessor('informix');
        	dra.query += ' and ID IN (\'' + a.Id + '\') limit 200';
        	Database.executeBatch(dra);
        
        test.stopTest();
        
        
    }
    
    static testMethod void testProcessAccounts4() {
        Account a;
        Address__c addr;
        User current = [Select ID from User where ID = :UserInfo.getUserID()];
        system.runas(current) {
        	a = TestHelperClass.createAccountData();
            a.name='Test Acc 4';
        	a.Channel__c = null;
        	a.Business_Id__c = '1200';
        	// TODO : FILL IN STAGE CRITERIA
        	a.ProcessingType__c = null;
        	// TODO : FILL IN PROC TYPE CRITERIA
        	a.TransitionDate1__c = null;
        	//a.CreatedDate = system.now();
        	a.DispositionCode__c = 'UA - Undeliverable As Addressed';
        	a.LeadExternalID__c = 'a';
        	update a;
        }
        
        test.startTest();
        	DataRecastAccountProcessor dra = new DataRecastAccountProcessor('broadview');
        	dra.query += ' and ID IN (\'' + a.Id + '\') limit 200';
        	Database.executeBatch(dra);
        
        test.stopTest();
        
        
    }
    
    static testMethod void testEventProcessor() {
    	// can't really make an event with a null record type id here, so we're just going to get some coverage
    	Event e;
    	Account a;
    	User u;
    	User current = [Select Id from User where Id = :UserInfo.getUserId()];
    	system.runas(current) {
    		u = TestHelperClass.createSalesRepUser();
    		a = TestHelperClass.createAccountData();
    		e = TestHelperClass.createEvent(a, u);
    		e.RecordTypeId = null;
            e.isPrivate= false;
    		update e;
    	}
    	
    	test.startTest();
        	List<Event> eveList= new List<Event>();
        	eveList.add(e);
    		DataRecastEventProcessor dre = new DataRecastEventProcessor();
    		dre.query += ' and Id IN (\'' + e.Id + '\') limit 200';
        	dre.execute(null,eveList);
    		Database.executeBatch(dre);
    	test.stopTest();
    	
    	Event eConfirm = [Select RecordType.Name from Event where Id = :e.Id];
    	system.assertEquals(eConfirm.RecordType.Name, RecordTypeName.SALESFORCE_ONLY_EVENT);
    }
    
    static testMethod void testProcessAddresses() {
    	Address__c a = new Address__c();
    	a.CountryCode__c = null;
    	a.Street__c = '5700 Lombardo Center';
    	a.City__c = 'Independence';
    	a.State__c = 'OH';
    	a.PostalCode__c = '44131';
    	a.OriginalAddress__c = '5700 Lombardo Center';
    	insert a;
    	
    	test.startTest();
    		DataRecastAddressProcessor dra = new DataRecastAddressProcessor();
    		dra.query += ' and ID IN (\'' + a.Id + '\')';
    		Database.executeBatch(dra);
    	test.stopTest();
    	
    	Address__c aconfirm = [Select CountryCode__c from Address__c where Id = :a.Id];
    	system.assertEquals('US', aconfirm.CountryCode__c);
    }
 
    // PERFORMANCE TESTS
    
    /*static testMethod void testAccountPerformance() {
    	Integer anum = 2000;
    	Account control;
    	List<Account> accounts = new List<Account>();
    	User current = [Select Id from User where ID = :UserInfo.getUserID()];
    	system.runas(current) {
    		control = TestHelperClass.createAccountData();
    		Address__c addr = [Select ID, PostalCode__c from Address__c where ID = :control.AddressID__c];
    		for (Integer i=0;i<anum;i++) {
    			Account a = TestHelperClass.createAccountData(addr, control.PostalCodeID__c, false);
    			accounts.add(a);
    		}
    		insert accounts;
    	}
    	
    	DateTime startTime = system.now();
    	test.startTest();
    		DataRecastHelper.processAccounts(accounts);
    	test.stopTest();
    	DateTime endTime = system.now();
    	
    	long mstime = endTime.getTime() - startTime.getTime();
    	system.debug(LoggingLevel.Error, 'Total account processing time = ' + mstime + 'ms');
    }*/
}