/************************************* MODIFICATION LOG ********************************************************************************************
* IntegrationConstants
*
* DESCRIPTION : Defines constants used by integrations
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE             Ticket             REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner         1/19/2012                          - Original Version
* Mounika Anna         2/22/2018        HRM-4729          - CLose/Cancel Feed
*                          
*/

public with sharing class IntegrationConstants {
  
  public static final String MESSAGE_STATUS_OK = 'OK';
  public static final String MESSAGE_STATUS_FAIL = 'Fail'; 
 // HRM-4729 SetCloseCancel
  public static final String FeedClose ='C';
  public static final String FeedCancel = 'L';
  
  // HRM - 5708 Install Appointment --- Mounika Anna/Giribabu Geda
  public static final String NewAppt = 'O';
  public static final String RescheduleAppt = 'R';
  public static final String CancelAppt = 'L';
  //end HRM - 5708 
  
  public static final String DISPOCODE_MMX = 'MMX';
  public static final String DISPOCODE_MMC = 'MMC';
 //end - 4729
  public static final String TRANSACTION_NEW = 'New';
  public static final String TRANSACTION_UPDATE = 'Update';
  public static final String TRANSACTION_UPDATE_RIF = 'Update-RIF';
  public static final String TRANSACTION_CANCEL = 'Cancel';
  public static final String TRANSACTION_RESCHEDULE = 'Reschedule';
  public static final String TRANSACTION_APPOINTMENT_ONLY = 'AppointmentOnly';
  public static final String TRANSACTION_ACCOUNT = 'Account';
  public static final String TRANSACTION_DISPOSITION_ONLY = 'DispositionOnly';
  public static final String TRANSACTION_NEW_FIELD = 'New-Field';
  public static final String TRANSACTION_NEW_RIF = 'New-RIF';
  
  public static final String REQUEST_STATUS_READY = 'Ready';
  public static final String REQUEST_STATUS_SUCCESS = 'Success';
  public static final String REQUEST_STATUS_ERROR = 'Error';
  public static final String REQUEST_STATUS_INFO = 'Info';
  public static final String REQUEST_STATUS_SKIP = 'SKIP';
  public static final String REQUEST_STATUS_ASYNCREADY = 'Async-Ready';
  public static final String REQUEST_STATUS_ASYNCERROR = 'Async-Error';
  public static final String REQUEST_STATUS_ASYNCSUCCESS = 'Async-Success';

  public static final Set<String> REQUEST_STATUS_ASYNC_VALUES = new Set<String> {REQUEST_STATUS_ASYNCREADY, 
                                          REQUEST_STATUS_ASYNCERROR,
                                          REQUEST_STATUS_ASYNCSUCCESS};
  
  public static final String DATA_SOURCE_TELEMAR = 'Telemar';
  public static final String DATA_SOURCE_TELEMAR_REHASH = 'Telemar Rehash';
  public static final String DATA_SOURCE_TELEMAR_RELO = 'Telemar RELO';
  public static final String DATA_SOURCE_MASTERMIND_BUSINESS = 'MASterMind Business';
  public static final String DATA_SOURCE_GDW = 'GDW';
  
  public static final String MESSAGE_ID_PREFIX = 'SF-';
  
  public static final String APPOINTMENT_TYPE_SG = 'SG';
  public static final String APPOINTMENT_TYPE_CG = 'CG';
    public static final String APPOINTMENT_VIEW_SALES = 'FieldSalesAppointment';
    public static final String APPOINTMENT_VIEW_ACTIVATION = 'ActivationAppointment';
    // 6682 changes
    public static final string RELO_CERTIFICATE_APPOINTMENT = 'ReloCertificateAppointment';
  
  public static final String PROCESSING_TYPE_FIELD = 'Field';
  public static final String PROCESSING_TYPE_USER = 'User';
  public static final String PROCESSING_TYPE_NSC = 'NSC';
  public static final String PROCESSING_TYPE_RIF = 'RIF';
    public static final String PROCESSING_TYPE_NSCLeadSharing = 'NSCLeadSharing';
  
  public static final String DEFAULT_NSC_DISPOSITION = 'NSC';
  
  public static final String DISPOSITION_DETAIL_CI_NOTE = 'CI Note';
  
  public static final String AFFILIATION_USAA = 'USAA';
  
  public static final String TYPE_OTHER = 'Other';
  public static final String TYPE_RIF = 'Customer Site';
  public static final String TYPE_PENDING_DISCO = 'Pending Discontinuance';
  public static final String TYPE_DISCO = 'Discontinuance';
  public static final String TYPE_NEW_MOVER = 'New Mover';
  public static final String TYPE_RESALE = 'Resale';
  
  public static final String TASK_CODE_RIF_VISIT = 'Visit';
  
  public static final String ARCHIVED_REASON_OUT_OF_SERVICE = 'Out of Service';
  public static final String ARCHIVED_REASON_IN_SERVICE = 'In Service';
  
  public static final String STATUS_SLS = 'SLS';
  public static final String STATUS_SLD = 'SLD';
  public static final String STATUS_CLS = 'CLS';
  public static final String STATUS_CCO = 'CCO';
  public static final String STATUS_CCC = 'CCC';
  public static final String STATUS_LOS = 'LOS';
  public static final String STATUS_LST = 'LST';
  public static final String STATUS_CAV = 'CAV';
  public static final String STATUS_CAS = 'CAS';
  // TODO: Awaiting confirmation of this value
  public static final String STATUS_NIO = 'NIO';
  
  public static final Set<String> STATUS_CANCEL_VALUES = new Set<String> {STATUS_CCO, 
                                      STATUS_CCC,
                                      STATUS_LOS,
                                      STATUS_LST,
                                      STATUS_NIO,
                                      STATUS_CAV};
                                      
  public static final Set<String> STATUS_SOLD_VALUES = new Set<String> {STATUS_SLS, 
                                      STATUS_SLD};
                                      
  public static final Set<String> STATUS_CLOSED_VALUES = new Set<String> {STATUS_CLS};
  
  public static final String CANCELLED_BEFORE = 'Before';
  public static final String CANCELLED_AFTER = 'After';
  
  public static final String DNC = 'Do Not Call';
  
  public static final String BILLING_SYSTEM_MMB = '5';
  
  public static final String SALES_MANAGER_ASSISTANCE = 'Sales Management Attention needed';
  

}