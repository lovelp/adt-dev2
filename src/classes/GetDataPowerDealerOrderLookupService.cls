/************************************* MODIFICATION LOG ************************************************************
* GetDataPowerDealerOrderLookupService
*
* DESCRIPTION : Restful webservice to Service Power to get Dealer Leads
*-------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE              REASON
*-------------------------------------------------------------------------------------------------------------------
* Khushbu Agarwal-TCS     7/03/2018    - Original Version
* Giribabu Geda            04/23/2018   - modifed the createEndpointRequest Method to accomidate the address sting to endpoint.
*/
public without sharing class GetDataPowerDealerOrderLookupService {
    
    private String endpoint='';
    public Address__c addAfterMelissaCheck;
    public class GetDPDealerOrderLookupRes{
        public boolean hasDealerOrder;
        public string createTs;
        public string orderStatus;
        public Site site;
    }
    
    public class Site{
        public string addrLine1;
        public string addrLine2;
        public string city;
        public string state;
        public string postalCode;
        public Dealer dealer;
    }
    
    public class Dealer{
        public string dealerName;
        public Integer dealerNumber;
        public string dealerPhoneNumber;
    }
    
    public class errorWrapper{
        public errorBody[] errors;
    }
    
    public class errorBody{
        public string status;
        public string errorCode;
        public string errorMessage;
    }
 
    public Boolean isBadResponse;
    public errorWrapper errWrap; 
    public boolean saveDealerInfo = true; 
    
    public GetDPDealerOrderLookupRes getDealerOrderLookup(String accountId){
        try{
        if(!Utilities.isEmptyOrNull(accountId)){
            createEndpointRequest(accountId);
            if(string.isNotBlank(endpoint)){
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                String userName = IntegrationSettings__c.getinstance().DPDealerLookupEndpointUsername__c.trim();
                String password = IntegrationSettings__c.getinstance().DPDealerLookupEndpointPassword__c.trim();
                Integer timeout = Integer.ValueOf(IntegrationSettings__c.getinstance().DPDealerLookupEndpointTimeout__c);
                //Code commented for mock server
                Blob headerValue = Blob.valueOf(userName + ':' + password);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                system.debug('Header :'+authorizationHeader);
                req.setHeader('Authorization', authorizationHeader);
                req.setEndpoint(endpoint.replaceAll(' ','%20'));
                req.setTimeout(timeout);
                req.setMethod('GET');
                system.debug('HTTP Request: '+req);
                HttpResponse res = new HttpResponse();
                
                res = http.send(req);
                system.debug('HTTP Response: '+res.getBody()+'res'+res);
                if(res!= null && res.getBody() !=null && res.getStatusCode()== 200 ){
                    GetDPDealerOrderLookupRes resp = new GetDPDealerOrderLookupRes();
                    resp = (GetDPDealerOrderLookupRes)JSON.deserialize(res.getBody(),GetDPDealerOrderLookupRes.class);
                    if(resp != null && saveDealerInfo){
                        saveDealerInfoOnAccount(res.getBody(), accountId);
                    }
                    return resp;
                }
                else{
                    isBadResponse = true;
                    errWrap = new errorWrapper();
                    if(res!= null &&  res.getBody()!=null && res.getStatusCode() == 400){
                        errWrap = (errorWrapper)JSON.deserialize(res.getBody(),errorWrapper.class);
                        // for any invalid input parameters, log a record into ADTLog
                        ADTApplicationMonitor.log ('Dealer Order Lookup Service Callout Error', errWrap.errors[0].errorMessage, true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);
                        system.debug('Deserialized Class: '+errWrap);
                        return null;
                    }else{
                        string accIDError= 'Error occured for accountId: '+accountId+ ' and response code is '+ res.getStatusCode()+ ' with a status '+res.getStatus();
                        ADTApplicationMonitor.log ('Dealer Order Lookup Service Callout Error',accIDError, true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);
                    }
                    return null;
                }
            }
        }
        }
        catch(Exception ae){
            ADTApplicationMonitor.log(ae, 'GetDataPowerDealerOrderLookupService', 'GetDPDealerOrderLookupRes', ADTApplicationMonitor.CUSTOM_APP.STANDARD);
        }
        return null;
    }

    public void createEndpointRequest(string accountId){
        //https://dev.api.adt.com/adtutil/order/lookup?addrLine1=18476 Wide Meadow Sq&addrLine2=Apt 118&city=LEESBURG&state=VA&postalCode=20176
        Address__c addr = new Address__c();
        //HRM-7035 if the passes parameteer is actual accountid then we gointo the if condition build the endpoint with address on the accountid.
        //if it is not Accountid string but actual address string then we go into else loop and pass the address parameters directly to the endpoint.
        if(accountId.StartsWith('001')){
            //If the address sent from CPQ is not null then query else use the same address
            if(addAfterMelissaCheck != null && addAfterMelissaCheck.Id != null){
                addr = addAfterMelissaCheck;
                system.debug('addr ##'+addr);
            }
            else{
                Account acnt = [SELECT id, AddressID__c FROM Account WHERE id =:accountId];
                addr = [SELECT Street__c, Street2__c, City__c, State__c, PostalCode__c FROM Address__c WHERE id=:acnt.AddressID__c];
            }
            if(addr!=null && addr.Street__c !=null && addr.State__c !=null && addr.PostalCode__c!=null && addr.City__c!=null){
                endpoint = IntegrationSettings__c.getinstance().DPDealerLookupEndpoint__c;
                endpoint= endpoint+'?addrLine1='+addr.Street__c;
                if(String.isNotBlank(addr.Street2__c)){
                    endpoint= endpoint+'&addrLine2='+addr.Street2__c;
                }
                endpoint= endpoint+'&city='+addr.City__c+'&state='+addr.State__c+'&postalCode='+addr.PostalCode__c;
            }
        }        
        else{           
            endpoint= IntegrationSettings__c.getinstance().DPDealerLookupEndpoint__c;
            endpoint= endpoint+'?'+accountId;
        }
    }
    
    public static boolean compareWithExistingDealer(string accId){

        Account a = new Account();
        a = [SELECT id, QbDealerOverride__c FROM Account WHERE id=: accId];
        system.debug('Called compareWithExistingDealer');

        return a.QbDealerOverride__c;
    }
    

    public void saveDealerInfoOnAccount(String response,string accId){
        GetDPDealerOrderLookupRes resBody = new GetDPDealerOrderLookupRes();
        resBody = (GetDPDealerOrderLookupRes)JSON.deserialize(response,GetDPDealerOrderLookupRes.class);
        system.debug('Called saveDealerInfoOnAccount'+resBody);
        Account acnt = new Account();
        acnt = [SELECT id, DP_Dealer_Name__c, DP_Dealer_Number__c, DP_Dealer_Phone_Number__c, QbDealerOverride__c, DealerQuoteStatus__c
                FROM Account WHERE id=: accId];
        if(!Utilities.isEmptyOrNull(accId) && resBody!=null){
            //If quote status has changed disable override
            if(resBody.hasDealerOrder){
                //On first time call. If dealer dealerName is blank
                if(String.isBlank(acnt.DP_Dealer_Name__c)){
                    acnt.DP_Dealer_Name__c = resBody.site.dealer.dealerName;
                    acnt.DP_Dealer_Number__c = String.valueOf(resBody.site.dealer.dealerNumber);
                    acnt.DP_Dealer_Phone_Number__c = resBody.site.dealer.dealerPhoneNumber;
                    acnt.Is_DP_Dealer_Account__c = resBody.hasDealerOrder;
                    acnt.DP_Transaction_Id__c = resBody.createTs;
                    acnt.DealerQuoteStatus__c = resBody.orderStatus;
                }
                //On second call if acnt has dealer already and new values.
                else if((String.isNotBlank(acnt.DP_Dealer_Name__c) && !acnt.DP_Dealer_Name__c.equalsIgnoreCase(resBody.site.dealer.dealerName)) || 
                    (String.isNotBlank(acnt.DP_Dealer_Number__c) && !acnt.DP_Dealer_Number__c.equalsIgnoreCase(String.valueOf(resBody.site.dealer.dealerNumber))) ||
                    (String.isNotBlank(acnt.DealerQuoteStatus__c) && !acnt.DealerQuoteStatus__c.equalsIgnoreCase(resBody.orderStatus))){
                    acnt.DP_Dealer_Name__c = resBody.site.dealer.dealerName;
                    acnt.DP_Dealer_Number__c = String.valueOf(resBody.site.dealer.dealerNumber);
                    acnt.DP_Dealer_Phone_Number__c = resBody.site.dealer.dealerPhoneNumber;
                    acnt.Is_DP_Dealer_Account__c = resBody.hasDealerOrder;
                    acnt.DP_Transaction_Id__c = resBody.createTs;
                    acnt.DealerQuoteStatus__c = resBody.orderStatus; 
                    acnt.QbDealerOverride__c = false;
                    system.debug('### Dealer details changed');
                }
            }
            else{
                acnt.Is_DP_Dealer_Account__c= resBody.hasDealerOrder;
                acnt.DP_Transaction_Id__c= resBody.createTs;
                acnt.DP_Dealer_Name__c= '';
                acnt.DP_Dealer_Number__c= '';
                acnt.DP_Dealer_Phone_Number__c= '';
                acnt.Is_DP_Dealer_Account__c = false;
                acnt.DealerQuoteStatus__c = '';
                acnt.QbDealerOverride__c = false;
            }
            try{
                //Future method
                //saveDealerOnaccount(JSON.serialize(acnt));
                update acnt;
            }
            catch(Exception e){
                 system.debug('**Exception Occured while saving Account**'+ e.getMessage());
                 ADTApplicationMonitor.log(e, 'GetDataPowerDealerOrderLookupService', 'saveDealerInfoOnAccount', ADTApplicationMonitor.CUSTOM_APP.STANDARD);
            }
        }
    }
    // END of Controller ***********************************************************************
}