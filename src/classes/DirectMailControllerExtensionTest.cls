@isTest
private class DirectMailControllerExtensionTest {
    
    static testMethod void testProcessWithItems() {
        
        User salesRep = TestHelperClass.createSalesRepUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        StreetSheet__c ss;
        System.runAs(salesRep) {  
            TestHelperClass.createReferenceDataForTestClasses();
            Account a1 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
            
            Account a2 = TestHelperClass.createAccountData();
            
            ss = new StreetSheet__c();
            ss.Name = 'Unit Test Street Sheet';
        
            insert ss;
        
            StreetSheetItem__c ssi1 = new StreetSheetItem__c();
            ssi1.AccountID__c = a1.Id;
            ssi1.StreetSheet__c = ss.Id;
        
            insert ssi1;
        
            StreetSheetItem__c ssi2 = new StreetSheetItem__c();
            ssi2.AccountID__c = a2.Id;
            ssi2.StreetSheet__c = ss.Id;
        
            insert ssi2;
            
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/DirectMailExport?ssid=' + ss.Id);
        Test.setCurrentPageReference(ref);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ss);
        
        Test.startTest();
        
        DirectMailControllerExtension dmce = new DirectMailControllerExtension(sc);
        System.runAs(salesRep) {
            PageReference newRef = dmce.process();
            System.assert(newRef == null, 'Expect the page reference to be null meaning no navigation');
            System.assert(dmce.itemList != null, 'Expect a non null list of street sheet items');
            System.assertEquals(2, dmce.itemList.size(), 'Expect 2 street sheet items in the list');
        }   
        
        
        Test.stopTest();
        
    }
    
    static testMethod void testProcessWithNoItems() {
        
        User salesRep = TestHelperClass.createSalesRepUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        StreetSheet__c ss;
        System.runAs(salesRep) { 
            TestHelperClass.createReferenceDataForTestClasses();
            Account a1 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
                    
            ss = new StreetSheet__c();
            ss.Name = 'Unit Test Street Sheet';
        
            insert ss;
    
            
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/DirectMailExport?ssid=' + ss.Id);
        Test.setCurrentPageReference(ref);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ss);
        
        Test.startTest();
        
        DirectMailControllerExtension dmce = new DirectMailControllerExtension(sc);
        System.runAs(salesRep) {
            
            PageReference newRef = dmce.process();
            System.assert(newRef == null, 'Expect the page reference to be null meaning no navigation');
            System.assert(dmce.itemList != null, 'Expect a non null list of street sheet items');
            System.assertEquals(0, dmce.itemList.size(), 'Expect 0 street sheet items in the list');
        }   
        
        
        Test.stopTest();
        
    }
    
    static testMethod void testRequestNoManager() {
        
        User salesRep = TestHelperClass.createSalesRepUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        StreetSheet__c ss;
        System.runAs(salesRep) {    
            TestHelperClass.createReferenceDataForTestClasses();
            Account a1 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
            
            ss = new StreetSheet__c();
            ss.Name = 'Unit Test Street Sheet';
        
            insert ss;
            
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/DirectMailExport?ssid=' + ss.Id);
        Test.setCurrentPageReference(ref);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new StreetSheet__c());
        
        Test.startTest();
        
        DirectMailControllerExtension dmce = new DirectMailControllerExtension(sc);
        System.runAs(salesRep) {
            PageReference newRef = dmce.request();
           // System.assert(newRef.getUrl().contains('NOMGR'), 'Expect to be navigated to error page with error NOMGR');
        }   
        
        
        Test.stopTest();
        
    }
    
    static testMethod void testRequestManagerExists() {
        
        User salesRep = TestHelperClass.createSalesRepUserWithManager();
        TestHelperClass.createReferenceUserDataForTestClasses();
        StreetSheet__c ss;
        System.runAs(salesRep) { 
            TestHelperClass.createReferenceDataForTestClasses();
            Account a1 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
            
            ss = new StreetSheet__c();
            ss.Name = 'Unit Test Street Sheet';
        
            insert ss;
            
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/DirectMailExport?ssid=' + ss.Id);
        Test.setCurrentPageReference(ref);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new StreetSheet__c());
        
        Test.startTest();
        
        DirectMailControllerExtension dmce = new DirectMailControllerExtension(sc);
        System.runAs(salesRep) {
            PageReference newRef = dmce.request();
            System.assert(newRef != null, 'Expect to be navigated to Street Sheet view page');
        }   
        
        
        Test.stopTest();
        
    }
    
    static testMethod void testDispositionUpdates() {
        TestHelperClass.createReferenceUserDataForTestClasses();
        Account a1,a2,a3;
        Address__c addr;
        Lead l1,l2;
        StreetSheet__c s;
        StreetSheetItem__c ai1,ai2,li1,li2;
        User u;
        user adminuser = TestHelperClass.createAdminUser();
        u = TestHelperClass.createSalesRepUser();
        system.runas(adminuser) {
            TestHelperClass.createReferenceDataForTestClasses();
            String id1 = TestHelperClass.inferPostalCodeID('221o2', '1100'); 
            String id2 = TestHelperClass.inferPostalCodeID('221o2', '1200'); 
            
            a1 = TestHelperClass.createAccountData();
            a1.OwnerId = u.Id;
            a1.Business_Id__c = '1200';
            update a1;
            addr = [Select Id, PostalCode__c from Address__c where Id = :a1.AddressID__c];
            a2 = TestHelperClass.createAccountData(addr, a1.PostalCodeID__c, false);
            a2.OwnerId = u.Id;
            a2.Business_Id__c = '1100';
            insert a2;
            a3 = TestHelperClass.createAccountData(addr, a1.PostalCodeID__c, false);
            a3.OwnerId = u.Id;
            a3.Business_Id__c = '1200';
            insert a3;
            l1 = TestHelperClass.createLead(u, false);
            l2 = TestHelperClass.createLead(u, false);
            l1.Business_Id__c = '1200';
            l2.Business_Id__c = '1100';
            insert new list<Lead>{l1,l2};

            s = new StreetSheet__c();
            s.OwnerId = u.Id;
            insert s;
            ai1 = new StreetSheetItem__c();
            ai1.StreetSheet__c = s.Id;
            ai1.AccountID__c = a3.Id;
            ai2 = new StreetSheetItem__c();
            ai2.StreetSheet__c = s.Id;
            ai2.AccountID__c = a2.Id;
            li1 = new StreetSheetItem__c();
            li1.StreetSheet__c = s.Id;
            li1.LeadID__c = l1.Id;
            li2 = new StreetSheetItem__c();
            li2.StreetSheet__c = s.Id;
            li2.LeadID__c = l2.Id;
            insert new list<StreetSheetItem__c>{ai1,ai2,li1,li2};
        
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(s);
        DirectMailControllerExtension dm;
        test.startTest();
            dm = new DirectMailControllerExtension(sc);
            dm.process();
        test.stopTest();
        
        Account a3confirm = [Select Id,Business_Id__c,DispositionCode__c,DispositionDetail__c from Account where Id = :a3.Id];
        Account a2confirm = [Select Id,Business_Id__c,DispositionCode__c,DispositionDetail__c from Account where Id = :a2.Id];
        Lead l1confirm = [Select Id,Business_Id__c,DispositionCode__c,DispositionDetail__c from Lead where Id = :l1.Id];
        Lead l2confirm = [Select Id,Business_Id__c,DispositionCode__c,DispositionDetail__c from Lead where Id = :l2.Id];
        
        system.assertEquals( ChangeAccountDispositionController.mailedSBCode, a3confirm.DispositionCode__c);
        system.assertEquals( ChangeAccountDispositionController.mailedResiCode, a2confirm.DispositionCode__c);
        system.assertEquals( ChangeAccountDispositionController.mailedResiDetail, a2confirm.DispositionDetail__c);
        system.debug('l1confirm = ' + l1confirm);
        //system.assertEquals( DirectMailControllerExtension.LEADMAILEDSBCODE, l1confirm.DispositionCode__c);
        //system.assertEquals( DirectMailControllerExtension.LEADMAILEDRESICODE, l2confirm.DispositionCode__c);
        //system.assertEquals( DirectMailControllerExtension.LEADMAILEDRESIDETAIL, l2confirm.DispositionDetail__c);
        }
    }

}