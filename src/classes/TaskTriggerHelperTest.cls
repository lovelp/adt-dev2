@isTest
public class TaskTriggerHelperTest{

    static testMethod void accountAttachedToTaskSMB(){       
        
        User admin = TestHelperClass.createAdminUser();
        system.runAs(admin){
            TestHelperClass.createReferenceUserDataForTestClasses();
            TestHelperClass.createReferenceDataForTestClasses();
            test.startTest();
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '101 Main Street';
            addr.City__c = 'Jacksonville';
            addr.State__c = 'FL';
            addr.PostalCode__c = '32212';
            addr.OriginalAddress__c = '101 MAIN STREET+JACKSONVILLE+FL+32212';
            
            insert addr;
            
            Account acct = new Account();
            acct.AddressID__c = addr.Id;
            acct.Name = 'test';
            acct.FirstName__c = 'testfirst';
            acct.LastName__c ='testlast';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1200 - SMB';
            acct.PostalCodeID__c = TestHelperClass.inferPostalCodeID(addr.PostalCode__c,'1100');
            acct.DispositionCode__c = 'test';
            acct.ShippingPostalCode = '32212';
            acct.Data_Source__c = 'TEST';
            acct.Phone = '904-999-1234';
            acct.ShippingPostalCode = '22102';
            acct.EquifaxApprovalType__c = 'CAE1';
            insert acct;
            Task t = new Task();
            t.whatId = acct.Id;
            t.GNCC__AccessMethod__c ='Click-To-Dial';        
            insert t;
            t.CallDisposition ='Left Message';
            update t;            
            test.stopTest();
            }
    }
       

    static testMethod void accountAttachedToTaskRESI(){       
        
        User admin = TestHelperClass.createAdminUser();
        system.runAs(admin){
            TestHelperClass.createReferenceUserDataForTestClasses();
            TestHelperClass.createReferenceDataForTestClasses();
            test.startTest();
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '101 Main Street';
            addr.City__c = 'Jacksonville';
            addr.State__c = 'FL';
            addr.PostalCode__c = '32212';
            addr.OriginalAddress__c = '101 MAIN STREET+JACKSONVILLE+FL+32212';
            
            insert addr;
            
            Account acct = new Account();
            acct.AddressID__c = addr.Id;
            acct.Name = 'test';
            acct.FirstName__c = 'testfirst';
            acct.LastName__c ='testlast';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - RESI';
            acct.PostalCodeID__c = TestHelperClass.inferPostalCodeID(addr.PostalCode__c,'1100');
            acct.DispositionCode__c = 'test';
            acct.ShippingPostalCode = '32212';
            acct.Data_Source__c = 'TEST';
            acct.Phone = '904-999-1234';
            acct.ShippingPostalCode = '22102';
            acct.EquifaxApprovalType__c = 'CAE1';
            insert acct;
            Task t = new Task();
            t.whatId = acct.Id;
            t.GNCC__AccessMethod__c ='Click-To-Dial';        
            insert t;
            t.CallDisposition ='Left Message';
            update t;            
            test.stopTest();
            }
    }
    
    
    static testMethod void leadAttachedToTask(){       
        
        User admin = TestHelperClass.createAdminUser();
        system.runAs(admin){                
            TestHelperClass.createReferenceUserDataForTestClasses(); 
            TestHelperClass.createSalesRepUser();           
            TestHelperClass.createReferenceDataForTestClasses();
            Lead ld = TestHelperClass.createLead(admin, false, '');
            test.startTest();
            ld.Business_Id__c = '1200 - Small Business';
            insert ld;
            System.debug('Lead with smb business Id is'+ld);
            Task t = new Task();
            t.whoId = ld.Id;
            t.GNCC__AccessMethod__c ='Click-To-Dial';        
            insert t;
            t.CallDisposition ='Left Message';
            update t;            
            test.stopTest();
            }
    }
    
}