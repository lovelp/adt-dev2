public virtual without sharing class TechUpSellDao {
    
    public class ProductCatalogWrapper{
        public String pName;
        public String pQuantity;
        public String pNotes;
        ProductCatalogWrapper(){
            pName = '';
            pQuantity = '';
            pNotes = '';
        }
    }
    
    public virtual String getExistingProductCatalog(Account acc){
        List<ProductCatalogWrapper> existingProducCatalogList = new List<ProductCatalogWrapper>();
        List<ProductCatalog__c> productList = [SELECT id,ProductName__c, Product_Quantity__c, Product_Notes__c FROM ProductCatalog__c WHERE ProductAccount__c =: acc.id];
        if(productList.size() > 0){
          for(ProductCatalog__c pCatalog : productList){
             System.debug('### data present');
            ProductCatalogWrapper  existingObjWrapper = new ProductCatalogWrapper();
            existingObjWrapper.pName =  String.isNotBlank(pCatalog.ProductName__c) ? pCatalog.ProductName__c : '';
            existingObjWrapper.pQuantity = String.isNotBlank(pCatalog.Product_Quantity__c) ? pCatalog.Product_Quantity__c : '';
            existingObjWrapper.pNotes = String.isNotBlank(pCatalog.Product_Notes__c) ? pCatalog.Product_Notes__c : '';
            existingProducCatalogList.add(existingObjWrapper);
          }           
        }
        else{
              System.debug('###  no data present');
              ProductCatalogWrapper  existingObjWrapper = new ProductCatalogWrapper();
              existingObjWrapper.pName =  '';
              existingObjWrapper.pQuantity = '';
              existingObjWrapper.pNotes =  '';
              existingProducCatalogList.add(existingObjWrapper);
        }
        
        return JSON.serialize(existingProducCatalogList);                                
    }
    
    public virtual String getAllProductCatalogNames(){
        List<ProductCatalogWrapper> producCatalogNamesList = new List<ProductCatalogWrapper>();
        Schema.DescribeFieldResult objectResultList = ProductCatalog__c.ProductName__c.getDescribe();
        List<Schema.PicklistEntry> ple = objectResultList.getPicklistValues();
        for(Schema.PicklistEntry p : ple){
           ProductCatalogWrapper  fullObjWrapper = new ProductCatalogWrapper(); 
           fullObjWrapper.pName =  String.isNotBlank(p.getValue()) ? p.getValue(): '';
           producCatalogNamesList.add(fullObjWrapper);
        }        
        return JSON.serialize(producCatalogNamesList);
    }
    
    public virtual String getAllProductCatalogQuantity(){
        List<ProductCatalogWrapper> producCatalogQuantityList = new List<ProductCatalogWrapper>();
        Schema.DescribeFieldResult objectResultList = ProductCatalog__c.Product_Quantity__c.getDescribe();
        List<Schema.PicklistEntry> ple = objectResultList.getPicklistValues();
        for(Schema.PicklistEntry p : ple){
           ProductCatalogWrapper  fullObjWrapper = new ProductCatalogWrapper(); 
           fullObjWrapper.pQuantity =  String.isNotBlank(p.getValue()) ? p.getValue(): '';
           producCatalogQuantityList.add(fullObjWrapper);
        }        
        return JSON.serialize(producCatalogQuantityList);
    }
    
    public virtual Boolean saveProductDetailsOnAccount(String accountId, String productCatalog){
        List<ProductCatalog__c> prodcutCatalogList = new List<ProductCatalog__c>();
        List<ProductCatalog__c> prodcutCatalogListTemp = new List<ProductCatalog__c>();
        List<Object> results = (List<Object>)JSON.deserializeUntyped(productCatalog);
        Set<Id> prodcutCatalogIdSet = new Set<Id>(); 
        String catalogNotes = '';  
        prodcutCatalogListTemp = [SELECT id,ProductName__c, Product_Quantity__c, Product_Notes__c FROM ProductCatalog__c WHERE ProductAccount__c =: accountId];
        delete prodcutCatalogListTemp;     
        for (Object obj : results) {
            Map<String, Object> objMap = (Map<String, Object>)obj;
            ProductCatalog__c pCat = new ProductCatalog__c();
            for(String str : objMap.keySet()){               
                System.debug('string is'+str+'value is'+objMap.get(str));
                if(str.equalsIgnoreCase('pName')){
                    pCat.Name = string.ValueOf(objMap.get(str));
                    pCat.ProductName__c = string.ValueOf(objMap.get(str));
                }
                else if(str.equalsIgnoreCase('pQuantity')){
                    pCat.Product_Quantity__c = String.valueof(objMap.get(str));
                }
                else if(str.equalsIgnoreCase('pNotes')){
                    pCat.Product_Notes__c = String.valueof(objMap.get(str));
                    catalogNotes = String.valueof(objMap.get(str));
                }
                pCat.ProductAccount__c = accountId;                                                      
            }
            prodcutCatalogList.add(pCat);             
        }    
        for(ProductCatalog__c pc : prodcutCatalogList){
            pc.ProductAccount__c = accountId;
        }
        // Disposition record for hot leads
        Disposition__c newDispo = new Disposition__c();
        newDispo.DispositionType__c = 'System Generated';
        newDispo.DispositionDetail__c = 'Simple Sell Product Changes';
        newDispo.Comments__c = String.isNotBlank(catalogNotes)? catalogNotes : '';
        newDispo.DispositionDate__c = DateTime.now();
        newDispo.AccountID__c = accountId;
        newDispo.DispositionedBy__c = UserInfo.getUserId();
                
        try{
            insert newDispo;
            insert prodcutCatalogList;
        }catch(exception e){
            System.debug('cannot insert'+e);
        }
        
        return true;
    }
}