/**
 Description- This test class used for LeadAddrEditButtonContoller.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class LeadAddrEditButtonContollerTest {
    static testMethod void testMethod1() {
        Lead lead = new Lead();
        lead.firstname= 'Test Lead';
        lead.lastname= 'Test Lead';
       
        ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(lead);
        LeadAddrEditButtonContoller lc= new LeadAddrEditButtonContoller (sc1);
        test.startTest();
        lc.init();
        lc.goEdit();
        test.stopTest();
        //system.assertNotEquals('true', lc.allowEdit);
    }
    
    static testMethod void testMethod2() {
        Profile p1 = [select id from profile where name='ADT NSC Sales Representative'];
        UserRole ur = [select id from UserRole where name like '%Rep' limit 1];
        User dummyUser= new User(alias = 'uatsr1', email='unitTestSalesAgent1@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p1.Id, userroleid = ur.Id,EmployeeNumber__c = TestHelperClass.generateEmployeeNumber(),
                timezonesidkey='America/Los_Angeles', username='unitTestSalesAgent1@testorg.com',
                StartDateInCurrentJob__c = Date.today(),Address_Validation__c = true);
                
        insert dummyUser;
        
        system.runAs(dummyUser){
            Lead lead = new Lead();
            lead.firstname= 'Test Lead';
            lead.lastname= 'Test Lead';
            
            ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(lead);
            LeadAddrEditButtonContoller lc= new LeadAddrEditButtonContoller (sc1);
            test.startTest();
            lc.init();
            lc.goEdit();
            test.stopTest();
            //system.assertEquals('true', lc.allowEdit); 
        }
    }
}