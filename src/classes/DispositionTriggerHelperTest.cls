/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DispositionTriggerHelperTest {

    static testMethod void insertTest() {
        //update custom setting
        User me = [Select Id from User where Id = :Userinfo.getUserId()];
        //use runas to avoid mixed dml exception
        system.runas(me) {
            ResaleGlobalVariables__c dc = [select Id,Name,Value__c from ResaleGlobalVariables__c where Name='DataConversion' limit 1];
            dc.Value__c = 'TRUE';
            update dc;
        }
        
        User u = TestHelperClass.createManagerUser();
        
        User dataconversionuser = [Select Id from User where Id = :Utilities.getDataConversionUser() limit 1];        
        system.runas(dataconversionuser)
        {
            Test.startTest();
            try{
                createAccountAndDisposition();
            }
            catch(Exception ae){}
            Test.stopTest();        
        }   
    }
    
      
    static void createAccountAndDisposition()
    {
       list<recordtype> lstrecordtype = [Select id,Name from Recordtype where sObjecttype='Account' and name ='Standard'];
        Account a = TestHelperClass.createAccountData();
        a.LeadExternalID__c = 'TE12345ST';
        a.Name = 'test';
        a.recordtypeid= lstrecordtype[0].Id;
        update a;
        
        Disposition__c d = new Disposition__c();
        d.LeadExternalId__c = a.LeadExternalID__c;
        d.AgentId__c = 'TE12345ST';
        
        Disposition__c d2 = new Disposition__c();
        d2.LeadExternalId__c = a.LeadExternalID__c;
        d2.AgentId__c = 'TE12345ST';
                
        insert new List<Disposition__c>{d, d2};             
    }
}