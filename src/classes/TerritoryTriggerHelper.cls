/************************************* MODIFICATION LOG ********************************************************************************************
* TerritoryTriggerHelper
*
* DESCRIPTION : Defines helper methods for use by TerritoryTrigger
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*            				 10/14/2011			- Original Version
*
*													
*/

public class TerritoryTriggerHelper {
	
	public void DeleteTerritoryAssociation(Map<Id, Territory__c> DeletedTerritories)
	{
		Set<Id> DeletedTerritoryIds = DeletedTerritories.keySet();
		List<TerritoryAssignment__c> TAs_ToBeDeleted = [Select id from TerritoryAssignment__c where TerritoryId__c in : DeletedTerritoryIds];
		delete TAs_ToBeDeleted;
	}
	
	public void updateHiddenOwner(List<Territory__c> newTerritory)
	{
		for(Territory__c terr : newTerritory)
		{
			terr.OwnerHidden__c = terr.OwnerId;
		}
	}
	
	public void UpdateTerritoryOwner(Map<Id, Territory__c> newRecs, Map<Id, Territory__c> oldRecs)
	{
		Map<id, List<TerritoryAssignment__c>> terrAssignMap = new Map<id, List<TerritoryAssignment__c>>();
		List<TerritoryAssignment__c> TAS = [Select id, TerritoryId__c, OwnerId from TerritoryAssignment__c where TerritoryId__c in : newRecs.keySet()];
		List<TerritoryAssignment__c> updatedTAs = new List<TerritoryAssignment__c>();
		for(TerritoryAssignment__c TA : TAS)
		{
			List<TerritoryAssignment__c> terrAssigns;
			if(terrAssignMap.get(TA.TerritoryId__c) == null)
			{
				terrAssigns = new List<TerritoryAssignment__c>();
				terrAssigns.add(TA);
				terrAssignMap.put(TA.TerritoryId__c, terrAssigns);
			}
			else
			{
				terrAssigns = terrAssignMap.get(TA.TerritoryId__c);
				terrAssigns.add(TA);
				terrAssignMap.put(TA.TerritoryId__c, terrAssigns);
			}
		}
		for(id terrId : newRecs.keySet())
		{
			if(newRecs.get(terrId).OwnerId != oldRecs.get(terrId).OwnerId)
			{
				
				if(terrAssignMap.get(terrId) != null)
				{
					for(TerritoryAssignment__c ta : terrAssignMap.get(terrId))
					{
						ta.OwnerId = newRecs.get(terrId).OwnerId;
						updatedTAs.add(ta);
					}
				}
			}
		}
		update updatedTAs;
	}

}