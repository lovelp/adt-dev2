/************************************* MODIFICATION LOG ********************************************************************************************
* StreetSheetExpirationProcessor
*
* DESCRIPTION : Initiates processing to delete Street Sheets that have had 30 days of inactivity.  
*               Implements the Schedulable interface in order to be executed as Scheduled Apex.
*
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*             				 10/14/2011			- Original Version
*
*													
*/

global class StreetSheetExpirationProcessor implements Schedulable {
	
   global void execute(SchedulableContext SC) {
		
		 Database.executeBatch( new StreetSheetExpirationBatchProcessor());
   }
}