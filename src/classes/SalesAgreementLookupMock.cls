/************************************* MODIFICATION LOG ********************************************************************************************
 * SalesAgreementLookupMock
 *
 * DESCRIPTION : Mock response for Sales Agreement Lookup Request API
 *  1. Create
 *               
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                        DATE              TICKET            REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Jitendra Kothari                 05/23/2019        HRM-9710           Original
 * Jitendra Kothari                 07/15/2019         HRM-10409         Enhance Existing Sales Agreement Lookup to Add Fields
 * Jitendra Kothari                 09/06/2019         HRM-9280          Ability to capture existing equipment that customer can bring to the new site 
 */

@isTest
global class SalesAgreementLookupMock implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://example.com/example/test', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status": true, "message": null, "statusCode": 0, "result": { "primarySalesRepID" : "81234567", "salesAgreementID": 890138624,'
            +'"sfdcOpportunityID" : "0064D000004NmQL", "primarySalesRepName" : "Fleetwood, Lauren",'
            +'"companyName" : "Test",'
            +'"salesAgreementName" : "TestOPP",'
            +'"attention" : "Vision Quest",'
            +'"customerAddress1" : "123 E Main St",'
            +'"customerAddress2" : "Suite #210",'
            +'"customerCity" : "Elliot City",'
            +'"customerState" : "MD",'
            +'"customerCounty" : "Howard",'
            +'"customerZip" : "21042",'
            +'"customerEmail" : "myemail@domain.com",'
            +'"customerPhone" : "4433670422",'
            +'"mastermindCustomerNumber" : "16000754",'
            +'"mastermindInstanceID" : "1",'
            +'"installCharge" : 1234.56,'
            +'"sellingTotalRMR" : 123.45,'
            +'"salesStatus" : "Active",'
            +'"agreementPhase" : "Proposed",'
            + '"sites": [ { "siteID": 889494243, '+
		        '"mastermindSiteNumber": "0",'+
		        '"siteTypeName": "Commercial",'+
		        '"siteStatus": "New Site",'+
		        '"siteName": "MyCompany TestKP",'+
		        '"siteAddress1": "2221 S Dobson Rd", '+
		        '"siteAddress2": "", '+
		        '"siteCity": "MESA", '+
		        '"siteState": "AZ", '+
		        '"siteZip": "85210", '+
		        '"siteCounty": "MARICOPA", '+
		        '"siteEmail": "mcoKP@mcoKP.com",'+
		        '"sitePhone": "4805555650",'+
		        '"systemDesigns": [ { "systemDesignId": 889503617, ' +
		            '"designTypeName": "Burglary",'+
		            '"installCharge": 770.57000000, '+
		            '"sellingRMR": 0.0, '+
		            '"includedOnContract": true,'+
		            '"Ownership": "CompanyOwned or Leased",'+
		            '"DesignName": "New Burg System"} ]  } '
            //+'"systemDesigns": [{"systemDesignId": "888840538","designTypeName": "Burglary",'
            //+'"installCharge": 199, "sellingRMR": 59.95,"includedOnContract": true},'
            //+'{"jobNumber": 123,"systemDesignId": "888853618","designTypeName": "CCTV","installCharge": 3180.26,"sellingRMR": 0,"includedOnContract": false },' 
            //+'{"jobNumber": 1234,"systemDesignId": "888853619","designTypeName": "Fire","installCharge": 527.73,"sellingRMR": 0,"includedOnContract": true}'
            +'],'
            +'"lastModified" : "2019-05-03T08:43:55.507-05:00"}}');
        res.setStatusCode(200);
        return res;
    }
}