/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : ChatterControllerTest is a test class for ChatterController.cls
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Erin McGee      07/08/2013      - Original Version
*
*                           
*/
@isTest
private class ChatterControllerTest {
	
	@isTest static void ChatterControllerTest() 
	{
		ChatterController cc= new ChatterController();
		User sru=TestHelperClass.createSalesRepUser();
		User currentUser=[Select Id From User Where Id=:UserInfo.getUserID()];
				Terms_and_Condition__c tc= new Terms_and_Condition__c();
				system.runAs(currentUser)
				{
		tc.Terms_and_Conditions_Text__c='Text';
		insert tc;
		}
				PageReference pageRef= Page.Chatter;
		Test.setCurrentPageReference(pageRef);
		System.runAs(sru)
		{
			cc.redirect();
			tc=cc.tc;
			cc.isAccepted=true;
			cc.save();
			cc= new ChatterController();
			cc.redirect();
			cc.isAccepted=false;
			cc.save();
		}

	}
	
	
}