/************************************* MODIFICATION LOG ********************************************************************************************
 * ProcessHotLeads- 
 *
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                        DATE              TICKET            REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Abhinav Pandey               07/25/2019          Ownership Assignment - Added logic to separate Hot Leads and ownership assignment to different class.  
 */

public class ProcessHotLeads{

    public static String dispositionMessage =  'SuccessFully Assigned';
    
    public static void processHotLeadsAction(String usId,Account acc,Disposition__c newDispo,String actionType){
        //dispositionMessage =actionType;
        Set<String> quoteStatusSet = new Set<String>{'paid','signed','submitted','installed','scheduled','on hold'};
        String allowedOrderTypeForHotLeads = ResaleGlobalVariables__c.getinstance('allowedOrderTypesHotLeads').value__c;
        Set<String> hotLeadsValueSet = new Set<String>();
        hotLeadsValueSet.addAll(allowedOrderTypeForHotLeads.split(';'));        
        String comments = newDispo.Comments__c;
        list<User> userlist = new list<user>();
        Boolean runLeadsAssignment = true;
        User userDetails; 
        if(actionType.containsIgnoreCase('hot leads')){
            //validate hot leads eligibility  
            if(acc!= null && acc.MMBOrderType__c != null && hotLeadsValueSet.contains(acc.MMBOrderType__c)){
                    List<scpq__SciQuote__c> quotes = [SELECT Id,Name,scpq__Status__c FROM scpq__SciQuote__c WHERE Account__c = :acc.id];
                    if(quotes.size() > 0){                  
                        for(scpq__SciQuote__c qt : quotes){
                            if(quoteStatusSet.contains(qt.scpq__Status__c.toLowerCase())){
                                runLeadsAssignment= false;
                                dispositionMessage = 'Cannot change Ownership. Account is in Paid Status';
                                break;
                            }                           
                        }
                    }
              }          
              //if account order type is not a hot lead assigned order type
             else {
                 dispositionMessage = 'Cannot change Ownership. Account does not a valid Order Type to run hot Lead assignment.';
                 runLeadsAssignment= false;
             }
        }
        
        system.debug('accchannel**'+acc.channel__c);
          
        if(acc.Channel__c.containsIgnoreCase(Channels.TYPE_NATIONAL)){
            String defaultNationalOwnerId  = '';
            if(String.isNotBlank(ResaleGlobalVariables__c.getinstance('DefaultNationalOwnerUsername').value__c)){
                String defaultNationalUsername  = ResaleGlobalVariables__c.getinstance('DefaultNationalOwnerUsername').value__c;
                for(User NationalOwner: [Select Id from User where UserName =: defaultNationalUsername limit 1]){
                    defaultNationalOwnerId = NationalOwner.Id;
                    //Change OwnerShip
                    acc.OwnerId = defaultNationalOwnerId;
                    acc.Rep_User__c = defaultNationalOwnerId;
                    acc.Lead_Origin__c  = actionType;//update lead origin
                    update acc;      
                    runLeadsAssignment = false;
                    dispositionMessage = 'Successfully Assigned to National Queue';
                }
            }
        }      
        if(runLeadsAssignment){
             userlist = getUserDetails(acc);
             if(!userlist.isempty() && acc != null){
                userDetails=userlist[0];
                //Change OwnerShip
                acc.OwnerId = userDetails.Id;
                acc.Rep_User__c = userDetails.Id;
                acc.Lead_Origin__c  = actionType;//update lead origin
                acc.HotLeadAssignedDateTime__c = System.now();          
                update acc;           
                String addressAcc = acc.SiteStreet__c+', '+acc.SiteCity__c+', '+acc.SiteState__c+', '+acc.SitePostalCode__c;
                String accStatus = String.isNotBlank(acc.AccountStatus__c) ? acc.AccountStatus__c : '';
                String dtEmail = System.now().format();
                String fName = String.isNotBlank(acc.FirstName__c)? acc.FirstName__c : '';
                String lName = String.isNotBlank(acc.LastName__c) ? acc.LastName__c : '';
                String contact = fName+' '+lName;
                String userEmail=userDetails.Email;
                String orderType = String.isNotBlank(acc.MMBOrderType__c) ? acc.MMBOrderType__c : '';
                if(String.isNotBlank(userEmail)){
                    sendEmailForOwnerShipchanged(acc.Id,userDetails.Id,userDetails.email,acc.TelemarAccountNumber__c,acc.Name,acc.Channel__c,addressAcc,acc.phone,accStatus,NSCLeadCaptureController.convertChanneltoLOB( acc.Channel__c ),orderType,comments,contact,dtEmail);
                }
                if(String.isNotBlank(userDetails.MobilePhone)){
                  sendSMSForOwnerShipChanged(acc.Id,userDetails.Id,userDetails.email,acc.TelemarAccountNumber__c,acc.Name,acc.Channel__c,addressAcc,acc.phone,accStatus,NSCLeadCaptureController.convertChanneltoLOB( acc.Channel__c ),orderType,comments,userDetails.MobilePhone,contact,dtEmail);
                }
            } 
            else{
                dispositionMessage ='Unable to find the territory owner.';
                if(acc.Channel__c.equalsIgnoreCase(Channels.TYPE_BUSINESSDIRECTSALES)){
                    //Change OwnerShip
                    acc.OwnerId = '00530000005dBvw';
                    acc.Rep_User__c = '00530000005dBvw';
                    acc.Lead_Origin__c  = actionType;//update lead origin
                    dispositionMessage  = 'Assigned to Global Admin';
                    update acc; 
                }
            }
        }
        
             // Disposition record for hot leads
            Disposition__c nDispo = new disposition__c();
            nDispo.DispositionType__c = 'System Generated';
            nDispo.DispositionDetail__c = actionType;
            nDispo.Comments__c =dispositionMessage;
            nDispo.DispositionDate__c = DateTime.now();
            nDispo.AccountID__c = acc.Id;
            // Dispositioned by Global Admin
            nDispo.DispositionedBy__c = '00530000005dBvw';//Global Admin Id 
            System.debug('### inserting dispo'+nDispo);
            insert nDispo;
            System.debug('### inserted dispo'+nDispo);
    }
        
    
    
    /**********************************************************************************************************
    find the Rep or Manager - based on the postal code
    ***********************************************************************************************************/
   // public static User getUserDetails(Account acc){
      public static list<User> getUserDetails(Account acc){
        system.debug('===========Future call4');
        List<SchedUserTerr__c> schUserList = new list<SchedUserTerr__c>();      
        try{
            //leadSharing 
            List<Id> PostalCodeIds = new List<Id>();
            //List<UserDetailsObject> userDetailsList = new List<UserDetailsObject>();
            List<Postal_Codes__c> allPostalCodes = new List<Postal_Codes__c>();
            Map<String, String> PCOwners = new Map<String, String>();
            Set<String> allTowns = new Set<String>();
            List<TerritoryAssignment__c> allTAs = new List<TerritoryAssignment__c>();
            Map<Id, String> PCTownsMap = new Map<Id, String>();
            Map<String, String> MTOwners = new Map<String, String>();
            List<Id> ownerIdList = new List<Id> ();
            List<User> usList = new List<User>();
            String BussId = Channels.getFormatedBusinessId(acc.Business_Id__c,Channels.BIZID_OUTPUT.NUM);
            if(acc.postalcodeId__c != null){
                PostalCodeIds.add(acc.postalcodeId__c);
            }
            system.debug('=============PostalCodeIds'+PostalCodeIds);
            //1. get all towns related to the postal code
            allPostalCodes = [Select Id, TownId__c, BusinessId__c, TownUniqueId__c from Postal_Codes__c where Id in : PostalCodeIds];
            for(Postal_Codes__c pcs : allPostalCodes){
                allTowns.add(pcs.TownId__c);
                PCTownsMap.put(pcs.id, pcs.TownId__c);
            }
            
            //2. territory assignment
            allTAs = [Select Id, PostalCodeId__c, OwnerId, TerritoryType__c from TerritoryAssignment__c where PostalCodeId__c in : PostalCodeIds];
            for(TerritoryAssignment__c TA : allTAs){
                PCOwners.put(TA.PostalCodeId__c + ';' + TA.TerritoryType__c, TA.OwnerId);
                ownerIdList.add(TA.OwnerId);
            }
            system.debug('=============ownerIdList'+ownerIdList);
            
            //3. Manager towns
            List<ManagerTown__c> MTs = [Select id, ManagerId__c, TownId__c, BusinessId__c, Type__c from ManagerTown__c where TownId__c in : allTowns];
            for(ManagerTown__c MT : MTs){
                for(String mtts : MT.Type__c.split(';')){
                    MTOwners.put(MT.TownId__c + ';' + MT.BusinessId__c + ';' + mtts, MT.ManagerId__c);
                }
            }
            System.debug('### pc owners are'+PCOwners);
            System.debug('### MTOwners are'+MTOwners);
            System.debug('### ownerIdList are'+ownerIdList);
            System.debug('pc town map is'+PCTownsMap);
            System.debug('### second map condition is'+ BussId + acc.Channel__c);
            System.debug('### first map condition is'+acc.PostalCodeId__c + ';' + acc.Channel__c);
            //Getting the user Details
            if(PCOwners.get(acc.PostalCodeId__c + ';' + acc.Channel__c) != null) {
                    String userId = PCOwners.get(acc.PostalCodeId__c + ';' + acc.Channel__c);
                    System.debug('user id is'+userId);
                    if(userId != null){
                    usList = [SELECT Id,Name,Email,MobilePhone FROM User WHERE Id =: userId];    
                    
                    }
                    
                    
             }
             
             else if (MTOwners.get(PCTownsMap.get(acc.PostalCodeId__c) + ';' + BussId + ';' + acc.Channel__c) != null){
                    String userId =MTOwners.get(PCTownsMap.get(acc.PostalCodeId__c) + ';' + BussId + ';' + acc.Channel__c);
                    System.debug('user id manager is'+userId);
                    if(userId != null){
                    usList = [SELECT Id,Name,Email,MobilePhone FROM User WHERE Id =: userId];    
                    
                    }
                    
             }
             //System.debug('### hot lead owner lst is'+userDetailsList);
            //return usList.size() > 0 ? usList[0] : new User() ;//finalUserDetailStringList;
            return usList;
        }
        catch(exception e){
            ADTApplicationMonitor.log ('ProcessHotLead-getUser', e.getMessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);  
            System.debug('somethign went wrong in ownership change ###'+e);
        }
        return null;
    }

    /****************************************************************************************************
      Method to send mail 
    *****************************************************************************************************/
    public static void sendEmailForOwnerShipchanged(String accId,String usId,String userEmail,String telAccountNumber,String accountName,String ch,String addrAcc,String phAcc,String accStatus,String LOB,String orderType,String comments,String contact,String dtEmail){
        system.debug('===========Future call6');
        try{           
            System.debug('### email sending start');
            String baseUrlCampaign = URL.getSalesforceBaseUrl().toExternalForm();
            System.debug('Account to access is'+accId+' and base url is'+baseUrlCampaign);
            Messaging.singleEmailmessage email = new Messaging.singleEmailmessage();
            email.setsubject('New '+ch+' Hot Lead Assigned: '+accountName);
            email.setPlainTextbody('A new '+ch+' Hot Lead has been assigned to you. Please contact the customer. '+'\n Account Telemar Number: '+telAccountNumber+'\n Account Name: '+accountName+'\n Address: '+addrAcc+'\n Phone: '+phAcc+'\n Date/Time: '+dtEmail+'\n Account Status: '+accStatus+'\n LOB: '+LOB+'\n Order Type: '+orderType+'\n Contact: '+contact+'\n Hot Leads Notes: '+comments+'\n link to Account: '+baseUrlCampaign+'/'+accId);
            email.setToAddresses(new String[] { usId });
            email.saveAsActivity = false;
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
            System.debug('###email sent');
        }
        catch(exception e){
            System.debug('cannot send email'+e);
            ADTApplicationMonitor.log ('ProcessHotLead-Send email', e.getMessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);  
        }
    }
    
    /******************************************************************************************
      Method to send SMS 
    ********************************************************************************************/
    public static void sendSMSForOwnerShipChanged(String accId,String usId,String userEmail,String telAccountNumber,String accountName,String ch,String addrAcc,String phAcc,String accStatus,String LOB,String orderType,String comments,String repPhone,String contact,String dtEmail){
         system.debug('===========Future call7');
         try{
             List<Wireless_Carrier__c> WirelessCarrierList = Wireless_Carrier__c.getAll().values();
             if( WirelessCarrierList != null && !WirelessCarrierList.isEmpty() ){
                        String regexNumeric = '[^a-zA-Z0-9]';
                        String mobileNumber = repPhone.replaceAll(regexNumeric, '').trim();
                        if(mobileNumber.length() == 11){
                            mobileNumber = mobileNumber.subString(1);
                        }
                        System.debug('### mobileNumber is'+mobileNumber);
                        List<String> SendTo = new List<String>();
                         // Iterate every carrier and use those enabled with an email2sms email address
                        for( Wireless_Carrier__c wCarrier: WirelessCarrierList ){
                            if( wCarrier.SMS_Enabled__c && !String.isBlank(wCarrier.SMS_Email_Service__c)  ){
                                SendTo.add( wCarrier.SMS_Email_Service__c.replaceFirst('number', mobileNumber) );
                            }
                        }
                  String subject ='New '+ch+' Hot Lead Assigned: '+accountName;
                  String msgBody = 'A new '+ch+' Hot Lead has been assigned to you. Please contact the customer. '+' Account Telemar Number: '+telAccountNumber+' Account Name: '+accountName+' Address: '+addrAcc+' Phone: '+phAcc+' Date/Time: '+dtEmail+' Account Status: '+accStatus+' LOB: '+LOB+' Order Type: '+orderType+'Contact: '+contact+' Hot Leads Notes: '+comments;   
                  EmailMessageUtilities.SendEmailNotification(sendTo,subject,msgBody); 
                  System.debug('sms send');
               }
         }
         catch(exception e){             
             System.debug('cannot send sms'+e);
             ADTApplicationMonitor.log ('ProcessHotLead-Send SMS', e.getMessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);  
         }  
    }
}