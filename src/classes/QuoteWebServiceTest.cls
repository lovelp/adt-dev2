/**
 Description- This test class used for QuoteWebService.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class QuoteWebServiceTest{
    
    static testMethod void returnQuoteTest() {
    
        test.startTest();
        
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));

        insert rgvList;
        
        Account a1 = TestHelperClass.createAccountData();
        
        Opportunity opp = New Opportunity();
        opp.AccountId = a1.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp; 
        
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Name = 'Test Quote';
        q.Account__c = a1.Id;
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'Sub';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        scpq__SciQuote__c sc= [select id, scpq__QuoteId__c from scpq__SciQuote__c where id =: q.id];
        scpq__SciQuote__c quote= QuoteWebService.returnQuote(sc.scpq__QuoteId__c );
        system.assertEquals(q.id, quote.id);
        
        //to cover exception
        scpq__SciQuote__c quote1= QuoteWebService.returnQuote('errorid377');
         
        test.stopTest();

    }
}