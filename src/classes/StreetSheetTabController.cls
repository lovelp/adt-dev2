/************************************* MODIFICATION LOG ********************************************************************************************
* StreetSheetTabController
*
* DESCRIPTION : Used by the StreetSheetTab VisualForce page to actually redirect the User to the All Listview.  
*               It is a Controller in the SFDC MVC architecture.
*
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*            				 10/14/2011			- Original Version
*
*													
*/

public with sharing class StreetSheetTabController {
	
	public StreetSheetTabController(){
	
	}
	public PageReference redirect (){

		return new PageReference('/'+Schema.SObjectType.StreetSheet__c.getKeyPrefix());
	}

}