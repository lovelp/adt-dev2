global class ADTPartnerAccUpdateResGenerator implements HttpCalloutMock
{
    global HTTPResponse respond(HTTPRequest req) 
    {        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        
        Boolean isCustomerLookup = false;
        Boolean isSiteLookup = false;
        XmlStreamReader reader = new XmlStreamReader(req.getBody());
        while(reader.hasNext()) 
        {
            if (reader.getEventType() == XmlTag.START_ELEMENT) 
            {
                if('LookupSite' == reader.getLocalName() ){
                    isSiteLookup = true;
                    break;  
                }
                if('LookupCustomer' == reader.getLocalName() ){
                    isCustomerLookup = true;
                    break;  
                }
            }
            reader.next();
        }
        
        // Create a fake response for a customer lookup web service call
        String xmlBodyStr = '';
        if(isSiteLookup == true)
        {
            xmlBodyStr='<NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/">'
                    +'   <NS1:Body>'
                    +'      <NS2:LookupSiteResponse xmlns:NS2="http://mastermindservice.adt.com/lookup">'
                    +'          <NS2:siteInfo>'
                    +'              <NS2:SiteNo>46255536</NS2:SiteNo>'
                    +'              <NS2:SiteType>SM BUS - Small Business</NS2:SiteType>'
                    +'              <NS2:SiteName>IRWAN TEST02</NS2:SiteName>'
                    +'              <NS2:SiteAddress1>8221 GLADES RD</NS2:SiteAddress1>'
                    +'              <NS2:SiteAddress2 />'
                    +'              <NS2:City>BOCA RATON</NS2:City>'
                    +'              <NS2:State>FL</NS2:State>'
                    +'              <NS2:ZipCode>33434-4072</NS2:ZipCode>'
                    +'              <NS2:Pics>'
                    +'                  <NS2:Pic>NNBH</NS2:Pic>'
                    +'              </NS2:Pics>'
                    +'              <NS2:Partners />'
                    +'              <NS2:HOA>N</NS2:HOA>'
                    +'              <NS2:BHT>N</NS2:BHT>'
                    +'              <NS2:CHS>N</NS2:CHS>'
                    +'              <NS2:MTMSiteNo />'
                    +'              <NS2:BillingCustNos>'
                    +'                  <NS2:BillingCustNo>400306876</NS2:BillingCustNo>'
                    +'              </NS2:BillingCustNos>'
                    +'              <NS2:BillingSystems>'
                    +'                  <NS2:BillingSystem>P1MMB</NS2:BillingSystem>'
                    +'              </NS2:BillingSystems>'
                    +'              <NS2:CS_NO>9400306876</NS2:CS_NO>'
                    +'              <NS2:PulseService />'
                    +'              <NS2:SystemType>CARD</NS2:SystemType>'
                    +'              <NS2:SystemStartDate>2014-05-19 19:19:06</NS2:SystemStartDate>'
                    +'              <NS2:ZoneCount>0</NS2:ZoneCount>'
                    +'              <NS2:Smoke>N</NS2:Smoke>'
                    +'              <NS2:CoDetection>N</NS2:CoDetection>'
                    +'              <NS2:PanelType>Card Access</NS2:PanelType>'
                    +'              <NS2:ServicePlan>1090CS</NS2:ServicePlan>'
                    +'              <NS2:CsAccountType>Primary</NS2:CsAccountType>'
                    +'              <NS2:CellBackup>N</NS2:CellBackup>'
                    +'              <NS2:ServiceActive>OUT</NS2:ServiceActive>'
                    +'              <NS2:InstallCompany>623</NS2:InstallCompany>'
                    +'              <NS2:SYSTEM_NO>1234</NS2:SYSTEM_NO>'
                    +'              <NS2:ResaleEligible>ELIGIBLE</NS2:ResaleEligible>'
                    +'          </NS2:siteInfo>'
                    +'          <NS2:ResultSummaryMessage>SUCCESS</NS2:ResultSummaryMessage>'
                    +'      </NS2:LookupSiteResponse>'
                    +'  </NS1:Body>'
                    +'</NS1:Envelope>';             
        }
        else if(isCustomerLookup == true)
        {
            xmlBodyStr='<NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/">'
                        +'  <NS1:Body>'
                        +'      <NS2:LookupCustomerResponse xmlns:NS2="http://mastermindservice.adt.com/lookup">'
                        +'          <NS2:customerInfo>'
                        +'              <NS2:FirstName />'
                        +'              <NS2:LastName>IRWAN TEST02</NS2:LastName>'
                        +'              <NS2:Address1>8221 GLADES RD</NS2:Address1>'
                        +'              <NS2:Address2 />'
                        +'              <NS2:City>BOCA RATON</NS2:City>'
                        +'              <NS2:State>FL</NS2:State>'
                        +'              <NS2:ZipCode>33434-4072</NS2:ZipCode>'
                        +'              <NS2:Phone>9991231234</NS2:Phone>'
                        +'              <NS2:BillingSystem>MMB</NS2:BillingSystem>'
                        +'              <NS2:BillingCustNo>400306876</NS2:BillingCustNo>'
                        +'              <NS2:CustomerStatus>I</NS2:CustomerStatus>'
                        +'              <NS2:ServiceActive>IN</NS2:ServiceActive>'                      
                        +'              <NS2:DiscoDate />'
                        +'              <NS2:DiscoReasonCode />'
                        +'              <NS2:PastDueStatus />'
                        +'              <NS2:PastDueAmount />'
                        +'              <NS2:PIC />'
                        +'              <NS2:Partner />'
                        +'              <NS2:ContractStartDate />'
                        +'              <NS2:ContractExpirationDate />'
                        +'              <NS2:ContractNo>16529640</NS2:ContractNo>'
                        +'              <NS2:ContractTerm />'
                        +'              <NS2:SiteNo>46255536</NS2:SiteNo>'
                        +'              <NS2:CustType>MMB-Small Business</NS2:CustType>'
                        +'              <NS2:MTMCustNo />'
                        +'              <NS2:MTMSiteNo />'
                        +'          </NS2:customerInfo>'
                        +'          <NS2:ResultSummaryMessage>SUCCESS</NS2:ResultSummaryMessage>'
                        +'      </NS2:LookupCustomerResponse>'
                        +'  </NS1:Body>'
                        +'</NS1:Envelope>';
        }
        
        res.setBody(xmlBodyStr);
            
        res.setStatusCode(200);
        return res;
        
    }
}