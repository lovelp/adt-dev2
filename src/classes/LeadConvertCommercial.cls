/************************************* MODIFICATION LOG ********************************************************************************************
* LeadConvertCommercial
*
* DESCRIPTION : Used in the commercial lightning flow
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                 DATE                TICKET          REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Srinivas Yarramsetti      05/15/2019                          - Original Version
* Siddarth Asokan           08/19/2019          HRM-10846       - Updated assign to Manager method to update the child records
*/

public without sharing class LeadConvertCommercial {
    @AuraEnabled
    public static String converLead(String recId){
        if(String.isNotBlank(recId)){
            // Assign Lead MangementId
            lead l = [Select Id, LeadManagementId__c from Lead where Id =:recId];
            String leadManagementId = l.LeadManagementId__c;
            if(String.isBlank(leadManagementId)){
                // Create a leadmanagementId if missing
                list<String> leadMgmtIdList = Utilities.createLeadMangementId(1);
                leadManagementId = leadMgmtIdList[0];
            }
            // Lead Conversion
            LeadConversion lc = new LeadConversion(recId);
            Boolean success = lc.convertLead();
            if(success && String.isNotBlank(lc.accountId)){
                // Update Account with leadManagementId
                Account a = new Account();
                a.id = lc.accountId;
                a.TelemarAccountNumber__c = leadManagementId;
                update a;
                
                list<Account> acc = [SELECT id, FirstName__c, LastName__c, Name, Email__c, phone, business_Id__c FROM Account WHERE id=:lc.accountId];
                createOpportunity(acc[0]);
                createContact(acc[0]);
                transferChildRecords(recId, lc.accountId);
                // Redirect to MMB lookup page
                PageReference redirectPage;
                if(!Test.isRunningTest()){
                    if(acc != null && acc.size() > 0 && String.isNotBlank(acc[0].business_Id__c) && acc[0].business_Id__c.contains('1300')){
                        redirectPage = new PageReference('/apex/MMBCustomerSiteLookup?lookupType=Multisite&objId=' + acc[0].id);
                    }else{
                        redirectPage = new PageReference('/apex/MMBCustomerSiteLookup?objId=' + acc[0].id);
                    }
                    aura.redirect(redirectPage);
                }
            }else{
                 throw new AuraHandledException('Error occured while converting lead.');
            }
        }
        return null;
    }
    
    @AuraEnabled
    public static RecordSearchController.sObjectWrapper returnSortedAccounts(String recId){
        RecordSearchController.sObjectWrapper matchingResults = RecordSearchController.searchRecords(null, recId);
        if(matchingResults.accntlist != null && matchingResults.accntlist.size() > 0){
            return matchingResults;
        }else{
            converLead(recId);
            return null;
        }
    }
    
    @AuraEnabled
    public static void continueToAccount(Id objId, Id leadId, Id usrid ){
        RecordSearchController.changeOwnerShipLC(objId, leadId, usrid);
    }
    
    public class optyContWrapper{
        @AuraEnabled
        public list<Opportunity> optylist;
        @AuraEnabled
        public list<Contact> contlist;
    }
    
    @AuraEnabled
    public static optyContWrapper getRelatedOptyCont(id accid){
        optyContWrapper optw = new optyContWrapper();
        list<Opportunity> opt = [SELECT id, Name, StageName, CloseDate FROM Opportunity WHERE AccountId =:accid];
        list<Contact> cont = [SELECT id, Name FROM Contact WHERE AccountId =:accid];
        optw.optylist = opt;
        optw.contlist = cont;
        return optw;
    }
    
    @AuraEnabled
    public static String assignToManager(String recId){
        String status = 'Success';
        try{
            // Get the manager of the logged in User
            Id loggedInUserId = UserInfo.getUserId();
            String managerId;
            for(user loggedInUsr : [Select Id, managerId from user where Id =: loggedInUserId And managerId != null And manager.isActive = true]){
                managerId = loggedInUsr.managerId;
            }
            if(String.isBlank(managerId)){
                managerId = RoleUtils.getNextAvailableAsignee(loggedInUserId);
            }
            system.debug('Manager Id: '+ managerId + ' Logged in User Id: ' + loggedInUserId);
            //system.debug('ManIdLoggUser'+managerId +loggedInUserId);
            if(String.isNotBlank(recId) && String.isNotBlank(managerId) && managerId != loggedInUserId){
                if(recId.subString(0,3) == Schema.SobjectType.Lead.getKeyPrefix()){
                    list<lead> leadList = new list<lead>();
                    leadList = [SELECT id , ownerId FROM Lead Where id=: recId];
                    if(leadList.size() > 0 && leadList[0].ownerId != managerId){
                        // Assign the lead to manager
                        leadList[0].ownerId = managerId;                
                        update leadList[0];

                        if(!Test.isRunningTest()){
                            // Redirect to leads page
                            PageReference redirectPage = new PageReference('/' + Schema.SobjectType.Lead.getKeyPrefix() + '/o');
                            aura.redirect(redirectPage);
                        }
                    }else{
                        status = 'Record is Already Owned By Manager';
                    }
                }else if(recId.subString(0,3) == Schema.SobjectType.Account.getKeyPrefix()){
                    list<account> acctList = new list<account>();
                    acctList = [SELECT id, ownerId, (Select Id, OwnerId From Contacts), (Select Id, OwnerId From Opportunities) FROM Account Where id=: recId];

                    if(acctList.size() > 0 && acctList[0].ownerId != managerId){
                        // Assign the lead to manager
                        acctList[0].ownerId = managerId;
                        update acctList[0];

                        // HRM-10846 - Update child contacts
                        if(acctList[0].Contacts.size() > 0){
                            list<contact> contactsToUpdate = new list<contact>();
                            for(Contact c: acctList[0].Contacts){
                                c.OwnerId = managerId;
                                contactsToUpdate.add(c);
                            }
                            if(contactsToUpdate.size() > 0){
                                database.update(contactsToUpdate,false); 
                            }
                        }
                        
                        // HRM-10846 - Update child opportunities
                        if(acctList[0].Opportunities.size() > 0){
                            list<Opportunity> oppToUpdate = new list<Opportunity>();
                            for(Opportunity opp: acctList[0].Opportunities){
                                opp.OwnerId = managerId;
                                oppToUpdate.add(opp);
                            }
                            if(oppToUpdate.size() > 0){
                                database.update(oppToUpdate,false); 
                            }
                        }
                        
                        if(!Test.isRunningTest()){
                            // Redirect to accounts page
                            PageReference redirectPage = new PageReference('/' + Schema.SobjectType.Account.getKeyPrefix() + '/o');
                            aura.redirect(redirectPage);
                        }
                    }else{
                        status ='Record is Already Owned By Manager';
                    }
                }else{
                     status ='Cannot change ownership. Record is neither a lead or account';
                }
            }else{
                status ='No Manager Exists for the User';
            }
        }catch(exception ex){
            status = ex.getMessage();
        }
        return String.isNotBlank(status)? status : 'Cannot change ownership. Please contact the salesforce support team';
    }
    
    //Create Opportunity
    private static void createOpportunity(Account a){
        Opportunity oppObj = new Opportunity();
        oppObj.Name = a.Name;
        oppObj.Accountid = a.id;
        oppObj.StageName = 'Needs Analysis';
        oppObj.CloseDate = Date.today();
        oppObj.RecordTypeId = Utilities.optyRecordTypeId(a);
        insert oppObj;
    }
    
    //Create Contact
    private static void createContact(Account acc){
        Contact con = new Contact();
        con.firstname = acc.FirstName__c;
        con.lastname = acc.LastName__c;
        con.Email = acc.Email__c;
        con.phone = acc.Phone;
        con.AccountId = acc.Id;
        insert con;
    }
    
    private static void transferChildRecords(String leadId, String accid){
        list<Disposition__c> dispoListToUpdate = new list<Disposition__c>();
        for(Disposition__c cd: [SELECT id, LeadID__c FROM Disposition__c WHERE LeadID__c =: leadId]){
            cd.AccountID__c = accid;
            dispoListToUpdate.add(cd);
        }
        if(dispoListToUpdate.size()>0){
            update dispoListToUpdate;
        }
    }
    
    @AuraEnabled
    public static String runMMB(String accid){
        String message = NSCLeadCaptureController.updateAccount(accid); 
        return message;
    }
}