/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AssignmentBatchProcessorTest {
  
    static testMethod void testAccountExpiration() {
        Account expiredAccount;
        Account validAccount;
        Account noPrevOwner;
        Account sgAccount;
        User u;
        List<Account> accts;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        Integer days = Integer.valueof(ResaleGlobalVariables__c.getInstance('DaysBeforeReassignment').value__c);
        system.runAs(current) {
            u = TestHelperClass.createSalesRepUser();
            
            expiredAccount = TestHelperClass.createAccountData();
            expiredAccount.PreviousOwner__c = current.Id;
            expiredAccount.DispositionDate__c = system.now().addDays(-1 * (days+1));
            expiredAccount.DateAssigned__c = system.now().addDays(-1 * (days+1));
            expiredAccount.OwnerId = u.Id;
            
            Address__c addr = [Select Id, PostalCode__c from Address__c where ID = :expiredAccount.AddressID__c];
            
            validAccount = TestHelperClass.createAccountData(addr);
            validAccount.PreviousOwner__c = current.Id;
            validAccount.DispositionDate__c = system.now().addDays(-1 * (days-1));
            validAccount.OwnerId = u.Id;
            
            noPrevOwner = TestHelperClass.createAccountData(addr);
            noPrevOwner.PreviousOwner__c = null;
            noPrevOwner.DispositionDate__c = system.now().addDays(-1 * (days-1));
            noPrevOwner.DateAssigned__c = system.now().addDays(-1 * (days+1));
            noPrevOwner.OwnerId = u.Id;
            
            sgAccount = TestHelperClass.createAccountData(addr);
            sgAccount.PreviousOwner__c = current.Id;
            sgAccount.DispositionDate__c = system.now().addDays(-1 * (days+1));
            sgAccount.ProcessingType__c = IntegrationConstants.PROCESSING_TYPE_USER;
            sgAccount.DateAssigned__c = system.now().addDays(-1 * (days+1));
            sgAccount.OwnerId = u.Id;
            
            accts = new List<Account> {expiredAccount, validAccount, noPrevOwner, sgAccount};
            update accts;
        }
        
        String aidstr = '';
        for (Account a : accts) {
            aidstr += '\'' + a.Id + '\',';
        }
        aidstr = aidstr.substring(0, aidstr.length()-1);
        
        test.startTest();
        
            AccountAssignmentBatchProcessor aa = new AccountAssignmentBatchProcessor();
            aa.query += ' and ID IN (' + aidstr + ') limit 200';
            Database.executeBatch(aa);
        
        test.stopTest();
        
        List<Account> aconfirm = [Select Id, OwnerId from Account where ID IN :accts and OwnerId = :current.Id];
        system.assertEquals(2, aconfirm.size());
        system.assertEquals(expiredAccount.Id, aconfirm[0].Id);
    }
  
    static testMethod void testAccountExpirationRif() {
        Account nullActivityDatesAccount;
        Account recentApptAccount;
        Account recentInstallAccount;
        User u;
        List<Account> accts;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        Integer days = Integer.valueof(ResaleGlobalVariables__c.getInstance('DaysBeforeReassignment').value__c);
        system.runAs(current) {
            u = TestHelperClass.createSalesRepUser();
            
            nullActivityDatesAccount = TestHelperClass.createAccountData();
            nullActivityDatesAccount.LastName__c = 'Null Activity Dates';
            nullActivityDatesAccount.PreviousOwner__c = current.Id;
            nullActivityDatesAccount.DispositionDate__c = null;
            nullActivityDatesAccount.DateAssigned__c = null;
            nullActivityDatesAccount.SalesAppointmentDate__c = null;
            nullActivityDatesAccount.ScheduledInstallDate__c = null;
            nullActivityDatesAccount.Type = IntegrationConstants.TYPE_RIF;
            nullActivityDatesAccount.OwnerId = u.Id;
            
            Address__c addr = [Select Id, PostalCode__c from Address__c where ID = :nullActivityDatesAccount.AddressID__c];
            
            recentApptAccount = TestHelperClass.createAccountData(addr);
            recentApptAccount.LastName__c = 'Recent Appt';
            recentApptAccount.PreviousOwner__c = current.Id;
            recentApptAccount.DispositionDate__c = null;
            recentApptAccount.DateAssigned__c = null;
            recentApptAccount.SalesAppointmentDate__c = system.now().addDays(-1 * (days-1));
            recentApptAccount.ScheduledInstallDate__c = null;
            recentApptAccount.Type = IntegrationConstants.TYPE_RIF;
            recentApptAccount.OwnerId = u.Id;
            
            recentInstallAccount = TestHelperClass.createAccountData(addr);
            recentInstallAccount.LastName__c = 'Recent Install';
            recentInstallAccount.PreviousOwner__c = current.Id;
            recentInstallAccount.DispositionDate__c = null;
            recentInstallAccount.DateAssigned__c = null;
            recentInstallAccount.SalesAppointmentDate__c = null;
            recentInstallAccount.ScheduledInstallDate__c = system.now().addDays(-1 * (days-1));
            recentInstallAccount.Type = IntegrationConstants.TYPE_RIF;
            recentInstallAccount.OwnerId = u.Id;
            
            accts = new List<Account> {nullActivityDatesAccount, recentApptAccount, recentInstallAccount};
            update accts;
        }
        
        String aidstr = '';
        for (Account a : accts) {
            aidstr += '\'' + a.Id + '\',';
        }
        aidstr = aidstr.substring(0, aidstr.length()-1);
        
        test.startTest();
        
            AccountAssignmentBatchProcessor aa = new AccountAssignmentBatchProcessor();
            aa.query += ' and ID IN (' + aidstr + ') limit 200';
            Database.executeBatch(aa);
        
        test.stopTest();
        
        List<Account> aconfirm = [Select Id, OwnerId, PreviousOwner__c from Account where ID IN :accts and OwnerId = :current.Id];
        system.assertEquals(1, aconfirm.size());
        system.assertEquals(nullActivityDatesAccount.Id, aconfirm[0].Id);
        system.assert(aconfirm[0].PreviousOwner__c == null, 'Previous Owner should be null');
    }
   
    static testMethod void testLeadExpiration() {
        Lead expiredAccount;
        Lead validAccount;
        Lead noPrevOwner;
        User u;
        List<Lead> accts;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        Integer days = Integer.valueof(ResaleGlobalVariables__c.getInstance('DaysBeforeReassignment').value__c);
        system.runAs(current) {
            u = TestHelperClass.createSalesRepUser();
            
            expiredAccount = TestHelperClass.createLead(u);
            expiredAccount.PreviousOwner__c = current.Id;
            expiredAccount.DispositionDate__c = system.now().addDays(-1 * (days+1));
            expiredAccount.DateAssigned__c = system.now().addDays(-1 * (days+1));
            expiredAccount.OwnerId = u.Id;
            
            validAccount = TestHelperClass.createLead(u);
            validAccount.PreviousOwner__c = current.Id;
            validAccount.DispositionDate__c = system.now().addDays(-1 * (days-1));
            validAccount.OwnerId = u.Id;
            
            noPrevOwner = TestHelperClass.createLead(u);
            noPrevOwner.PreviousOwner__c = null;
            noPrevOwner.DispositionDate__c = system.now().addDays(-1 * (days-1));
            noPrevOwner.DateAssigned__c = system.now().addDays(-1 * (days+1));
            noPrevOwner.OwnerId = u.Id;
            
            accts = new List<Lead> {expiredAccount, validAccount, noPrevOwner};
            update accts;
        }
        
        String aidstr = '';
        for (Lead a : accts) {
            aidstr += '\'' + a.Id + '\',';
        }
        aidstr = aidstr.substring(0, aidstr.length()-1);
        
        test.startTest();
        
            LeadAssignmentBatchProcessor aa = new LeadAssignmentBatchProcessor();
            aa.query += ' and ID IN (' + aidstr + ') ';
            Database.executeBatch(aa);
        
        test.stopTest();
        
        List<Lead> aconfirm = [Select Id, OwnerId from Lead where ID IN :accts and OwnerId = :current.Id];
        system.assertEquals(2, aconfirm.size());
        system.assertEquals(expiredAccount.Id, aconfirm[0].Id);
    }
    
}