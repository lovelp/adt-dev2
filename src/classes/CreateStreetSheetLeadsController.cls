/************************************* MODIFICATION LOG ********************************************************************************************
* CreateStreetSheetLeadsController
*
* DESCRIPTION : Supports the custom list view button Create Street Sheet for Leads.    
*               It is a Controller in the SFDC MVC architecture.
*
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					3/21/2012				- Original Version
*
*													
*/

public with sharing class CreateStreetSheetLeadsController {

	private List<Lead> leads;
	
	public CreateStreetSheetLeadsController( ApexPages.StandardSetController std ) {
		leads = std.getSelected();
	}

	public PageReference createStreetSheet() {
		
		if (leads.size() == 0) {
			return new PageReference('/apex/ShowError?Error=LNS');
		}
		
		Set<String> streetSheetItemSet = new Set<String>();
		for (Lead l : leads) {
			streetSheetItemSet.add(l.Id);
		}
		
		String streetSheetId = null;
		try {
			streetSheetId = StreetSheetManager.create(streetSheetItemSet);
		}
		catch (Exception e) {
			system.debug('Exception creating a Street Sheet: ' + e.getMessage());
			return new PageReference('/apex/ShowError?Error=DATAERROR');
		}
		PageReference pr = new PageReference ('/'+ streetSheetId);
        pr.setRedirect(true);
        return pr;
	}
}