@isTest
public class RoleHierarchyProximitySearcherTest 
{        
     static testMethod void testfindNearbyLeadsAndAccounts() 
     {
        Test.startTest();       
        User salesRep = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :TestHelperClass.createExecWithTeam().Id];
                                 
        User salesMgr = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :Utilities.getManagersForRole(salesRep.UserRoleId)[0].Id];
        User salesExec = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :Utilities.getManagersForRole(salesMgr.UserRoleId)[0].Id];
        
        User outOfRoleHierarchyMgr = TestHelperClass.createManagerUser();
        
        ProximitySearchSettings__c pss = ProximitySearchSettings__c.getInstance(salesExec.ProfileId);                                
                                 
        
        
        System.runAs(outOfRoleHierarchyMgr) 
        {                   
            String Id = TestHelperClass.inferPostalCodeID('221o2', '1100');
            //Create Addresses first
            Address__c addr1 = new Address__c();
            addr1.Latitude__c = 38.94686000000000;
            addr1.Longitude__c = -77.25470100000000;
            addr1.Street__c = '8950 Brook Rd';
            addr1.City__c = 'McLean';
            addr1.State__c = 'VA';
            addr1.PostalCode__c = '221o2';
            addr1.OriginalAddress__c = '8950 Brook Rd, McLean, VA 221o2';           
            insert addr1;
            
            Lead l = new Lead();
            l.LastName = 'James';
            l.Company = '007';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '8950 Brook Rd';
            l.SiteCity__c = 'McLean';       
            l.SiteStateProvince__c = 'VA';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '221o2';          
            l.AddressID__c = addr1.Id;              
            insert l;
            
            Account acct = new Account();
            acct.AddressID__c = addr1.Id;
            acct.Name = 'Unit Test Account 2';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            acct.ShippingPostalCode = '221o2';          
            insert acct;
        }
        
        System.runAs(salesMgr) 
        {                       
            //Create Addresses first
            Address__c addr2 = new Address__c();
            addr2.Latitude__c = 38.94686000000000;
            addr2.Longitude__c = -77.25470100000000;
            addr2.Street__c = '8951 Brook Rd';
            addr2.City__c = 'McLean';
            addr2.State__c = 'VA';
            addr2.PostalCode__c = '221o2';
            addr2.OriginalAddress__c = '8951 Brook Rd, McLean, VA 221o2';           
            insert addr2;
            
            Lead l = new Lead();
            l.LastName = 'William';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '8951 Brook Rd';
            l.SiteCity__c = 'McLean';       
            l.SiteStateProvince__c = 'VA';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '221o2';
            l.AddressID__c = addr2.Id;                      
            insert l;
            
            Account acct = new Account();
            acct.AddressID__c = addr2.Id;
            acct.Name = 'Unit Test Account 2';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            acct.ShippingPostalCode = '221o2';          
            insert acct;
        }
                
        System.runAs(salesExec) 
        {           
            //Create Addresses first
            Address__c addr3 = new Address__c();
            addr3.Latitude__c = 38.94686000000000;
            addr3.Longitude__c = -77.25470100000000;
            addr3.Street__c = '8952 Brook Rd';
            addr3.City__c = 'McLean';
            addr3.State__c = 'VA';
            addr3.PostalCode__c = '221o2';
            addr3.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';           
            insert addr3;
            
            Lead l = new Lead();
            l.LastName = 'Wallace';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '8952 Brook Rd';
            l.SiteCity__c = 'McLean';       
            l.SiteStateProvince__c = 'VA';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '221o2';
            l.AddressID__c = addr3.Id;
            insert l;
            
            Account acct = new Account();
            acct.AddressID__c = addr3.Id;
            acct.Name = 'Unit Test Account';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            acct.ShippingPostalCode = '221o2';          
            insert acct;
            
            // create a MapItem from the lat and long
            MapItem startingPoint = new MapItem(addr3.Latitude__c, addr3.Longitude__c);
            
            // retrieve all accounts that are within a latitude and longitude bounding box calculated from the initial distance of 1 mile
            String boundingCondition = MapUtilities.getBoundingCondition(startingPoint, pss.InitialDistance__c);
            String endLimit = String.valueOf(Math.roundToLong(pss.FirstRowLimit__c));
            
            RoleHierarchyProximitySearcher RHPS = new RoleHierarchyProximitySearcher();
            list<Account> accountResultList = RHPS.findNearbyAccounts(boundingCondition, endLimit, salesExec, false, false);
            list<Lead> leadsResultList = RHPS.findNearbyLeads(boundingCondition, endLimit, salesExec, false, false);            
            
            //System.assert(accountResultList.size() == 2, 'findNearbyAccounts should return only 2 Accounts');
            //COMMENTED OUT FOR DEPLOYMENT
            //System.assert(leadsResultList.size() == 2, 'findNearbyLeads should return only 2 Leads');                 
        }       
        
        Test.stopTest();     
     }
     
     static testMethod void testfindNearbyLeadsAcctDistMoreThanTenMiles() 
     {
        Test.startTest();       
        User salesRep = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :TestHelperClass.createExecWithTeam().Id];
                                 
        User salesMgr = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :Utilities.getManagersForRole(salesRep.UserRoleId)[0].Id];
       
        User salesExec = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :Utilities.getManagersForRole(salesMgr.UserRoleId)[0].Id];
       
        
        User outOfRoleHierarchyMgr = TestHelperClass.createManagerUser();
        
        ProximitySearchSettings__c pss = ProximitySearchSettings__c.getInstance(salesExec.ProfileId);                                
                                 
        
        
        System.runAs(outOfRoleHierarchyMgr) 
        {                       
            String Id = TestHelperClass.inferPostalCodeID('221o2', '1100');
            //Create Addresses first
            Address__c addr1 = new Address__c();
            addr1.Latitude__c = 38.94686000000000;
            addr1.Longitude__c = -77.25470100000000;
            addr1.Street__c = '8950 Brook Rd';
            addr1.City__c = 'McLean';
            addr1.State__c = 'VA';
            addr1.PostalCode__c = '221o2';
            addr1.OriginalAddress__c = '8950 Brook Rd, McLean, VA 221o2';           
            insert addr1;
            
            Lead l = new Lead();
            l.LastName = 'James';
            l.Company = '007';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '8950 Brook Rd';
            l.SiteCity__c = 'McLean';       
            l.SiteStateProvince__c = 'VA';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '221o2';
            l.PostalCodeID__c = null;
            l.AddressID__c = addr1.Id;
            l.Business_Id__c = null;            
            insert l;
            
            Account acct = new Account();
            acct.AddressID__c = addr1.Id;
            acct.Name = 'Unit Test Account 2';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            acct.ShippingPostalCode = '221o2';          
            insert acct;
        }
        
        System.runAs(salesMgr) 
        {
            String Id = TestHelperClass.inferPostalCodeID('83716', '1100');
            //Create Addresses first
            Address__c addr2 = new Address__c();
            addr2.Latitude__c = 43.56931100000000;
            addr2.Longitude__c = -116.10939300000000;
            addr2.Street__c = '5961 E Millet Dr';
            addr2.City__c = 'Boise';
            addr2.State__c = 'ID';
            addr2.PostalCode__c = '837a6';
            addr2.OriginalAddress__c = '5961 E Millet Dr, Boise, ID 83a16';         
            insert addr2;
            
            Lead l = new Lead();
            l.LastName = 'William';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '5961 E Millet Dr';
            l.SiteCity__c = 'Boise';        
            l.SiteStateProvince__c = 'ID';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '83716';
            l.PostalCodeID__c = null;
            l.AddressID__c = addr2.Id;
            l.Business_Id__c = null;            
            insert l;
            
            Account acct = new Account();
            acct.AddressID__c = addr2.Id;
            acct.Name = 'Unit Test Account 2';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            acct.ShippingPostalCode = '83716';          
            insert acct;
        }
                
        System.runAs(salesExec) 
        {           
            //Create Addresses first
            Address__c addr3 = new Address__c();
            addr3.Latitude__c = 38.94686000000000;
            addr3.Longitude__c = -77.25470100000000;
            addr3.Street__c = '8952 Brook Rd';
            addr3.City__c = 'McLean';
            addr3.State__c = 'VA';
            addr3.PostalCode__c = '221o2';
            addr3.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';           
            insert addr3;
            
            Lead l = new Lead();
            l.LastName = 'Wallace';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '8952 Brook Rd';
            l.SiteCity__c = 'McLean';       
            l.SiteStateProvince__c = 'VA';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '221o2';
            l.PostalCodeID__c = null;
            l.AddressID__c = addr3.Id;
            l.Business_Id__c = null;            
            insert l;
            
            Account acct = new Account();
            acct.AddressID__c = addr3.Id;
            acct.Name = 'Unit Test Account';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            acct.ShippingPostalCode = '221o2';          
            insert acct;
            
            // create a MapItem from the lat and long
            MapItem startingPoint = new MapItem(addr3.Latitude__c, addr3.Longitude__c);
            
            // retrieve all accounts that are within a latitude and longitude bounding box calculated from the initial distance of 1 mile
            String boundingCondition = MapUtilities.getBoundingCondition(startingPoint, pss.InitialDistance__c);
            String endLimit = String.valueOf(Math.roundToLong(pss.FirstRowLimit__c));
            
            RoleHierarchyProximitySearcher RHPS = new RoleHierarchyProximitySearcher();
            list<Account> accountResultList = RHPS.findNearbyAccounts(boundingCondition, endLimit, salesExec, false, false);
            list<Lead> leadsResultList = RHPS.findNearbyLeads(boundingCondition, endLimit, salesExec, false, false);                
            System.assertEquals('RoleHierarchyProximitySearcher', RHPS.getName());    
            //System.assert(accountResultList.size() == 1, 'findNearbyAccounts should return only 1 Accounts');
            //COMMENTED OUT FOR DEPLOYMENT
            //System.assert(leadsResultList.size() == 1, 'findNearbyLeads should return only 1 Leads');                 
        }       
        
        Test.stopTest();     
     }
}