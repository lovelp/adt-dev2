@isTest
private class DeleteTerritoryControllerTest {
	
	static testMethod void testDeleteAndRedirect() {
		
		User manager1 = TestHelperClass.createManagerUser();
		
		Territory__c terr1;

		System.runAs(manager1) {
			terr1 = new Territory__c();
			terr1.Name = 'Resi-Test';
			//terr1.BusinessId__c = '1100';
			
			insert terr1;
			
		}
		
		ApexPages.PageReference ref = new PageReference('/apex/DeleteTerritory?id=' + terr1.Id);
	    Test.setCurrentPageReference(ref);
		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		
		Test.startTest();
		
		DeleteTerritoryController dtc = new DeleteTerritoryController(sc);
		
		// set the controller extension's public variable
		dtc.terr = terr1;
		
		PageReference newRef = dtc.deleteAndRedirect();
		
		System.assert(newRef != null, 'Expect to be navigated');
		
		String terrCountSOQL = 'select count() from Territory__c where Id  = \'' + terr1.Id + '\'';
		
		Integer countOfTerritories = database.Countquery(terrCountSOQL);
		
		System.assertEquals(0, countOfTerritories, 'Manager should own no territories');
		
		Test.stopTest();
		
		
	}

}