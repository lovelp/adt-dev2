@isTest
global class MockHttpResponseCustomerInfoApi implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        // Only set a mock response for specific endpoints DM and PROD
        HttpResponse res = new HttpResponse();
        if( req.getEndpoint() == 'https://sgbeta.adt.com/adtclup' || req.getEndpoint() == 'https://sg.adt.com/adtclup' ){
	        res.setHeader('Content-Type', 'application/xml');
	        res.setBody('<?xml version="1.0" encoding="UTF-8"?><NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/"><NS1:Body><NS2:LookupCustomerResponse xmlns:NS2="http://mastermindservice.adt.com/lookup"><NS2:customerInfo><NS2:FirstName>302</NS2:FirstName><NS2:LastName>ACG TEXAS LP DBA IHOP</NS2:LastName><NS2:Address1>1778 N PLANO RD STE 100</NS2:Address1><NS2:Address2/><NS2:City>RICHARDSON</NS2:City><NS2:State>TX</NS2:State><NS2:ZipCode>75081-1962</NS2:ZipCode><NS2:Phone>2143466520</NS2:Phone><NS2:BillingSystem>MMB</NS2:BillingSystem><NS2:BillingCustNo>2371739</NS2:BillingCustNo><NS2:CustomerStatus>I</NS2:CustomerStatus><NS2:ServiceActive>IN</NS2:ServiceActive><NS2:DiscoDate>04/11/2014</NS2:DiscoDate><NS2:DiscoReasonCode>Existing Customer New Contract</NS2:DiscoReasonCode><NS2:PastDueStatus/><NS2:PastDueAmount/><NS2:ReallyPastDueAmount/><NS2:PIC/><NS2:Partner/><NS2:ContractStartDate>11/02/2010</NS2:ContractStartDate><NS2:ContractExpirationDate/><NS2:ContractTerm/><NS2:SiteNo>20373018</NS2:SiteNo><NS2:CustType>MMB-Small Business</NS2:CustType><NS2:MTMCustNo>102371739</NS2:MTMCustNo><NS2:MTMSiteNo/><NS2:BillCodes/><NS2:ContractNo>9299645</NS2:ContractNo></NS2:customerInfo><NS2:customerInfo><NS2:FirstName>302</NS2:FirstName><NS2:LastName>ACG TEXAS LP DBA IHOP</NS2:LastName><NS2:Address1>1778 N PLANO RD STE 100</NS2:Address1><NS2:Address2/><NS2:City>RICHARDSON</NS2:City><NS2:State>TX</NS2:State><NS2:ZipCode>75081-1962</NS2:ZipCode><NS2:Phone>2143466520</NS2:Phone><NS2:BillingSystem>MMB</NS2:BillingSystem><NS2:BillingCustNo>2371739</NS2:BillingCustNo><NS2:CustomerStatus>I</NS2:CustomerStatus><NS2:ServiceActive>IN</NS2:ServiceActive><NS2:DiscoDate>04/11/2014</NS2:DiscoDate><NS2:DiscoReasonCode>Existing Customer New Contract</NS2:DiscoReasonCode><NS2:PastDueStatus/><NS2:PastDueAmount/><NS2:ReallyPastDueAmount/><NS2:PIC/><NS2:Partner/><NS2:ContractStartDate>09/11/2013</NS2:ContractStartDate><NS2:ContractExpirationDate>09/30/2016</NS2:ContractExpirationDate><NS2:ContractTerm>7</NS2:ContractTerm><NS2:SiteNo>20373018</NS2:SiteNo><NS2:CustType>MMB-Small Business</NS2:CustType><NS2:MTMCustNo>102371739</NS2:MTMCustNo><NS2:MTMSiteNo/><NS2:BillCodes/><NS2:ContractNo>9299646</NS2:ContractNo></NS2:customerInfo><NS2:ResultSummaryMessage>SUCCESS</NS2:ResultSummaryMessage></NS2:LookupCustomerResponse></NS1:Body></NS1:Envelope>');
	        res.setStatusCode(200);
        }
        return res;
    }
}