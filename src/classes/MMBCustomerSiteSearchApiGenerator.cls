/**
 Description- This test class used for MMBCustomerSiteSearchApi mock response class.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
global class MMBCustomerSiteSearchApiGenerator implements HttpCalloutMock {

     global HTTPResponse respond(HTTPRequest req) {
         
         HttpResponse res = new HttpResponse();
         res.setHeader('Content-Type', 'application/xml');
          // Create a fake response for a customer lookup web service call
          //xmlBodyStr = 'empty response';
         String xmlBodyStr='<NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/">'
                    +'  <NS1:Body>'
                    +'      <NS2:SetContractDOBResponse xmlns:NS2="http://mastermindservice.adt.com/lookup">'
                    +'          <NS2:ErrorMessage>Sent To Queue is Completed is Completed</NS2:ErrorMessage>'
                    +'      </NS2:SetContractDOBResponse>'
                    +'  </NS1:Body>'
                    +'</NS1:Envelope>';  
         res.setBody(xmlBodyStr);
         res.setStatusCode(200);
         return res;
     }
}