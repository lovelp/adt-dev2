/************************************* MODIFICATION LOG *****************************************
* SplashPopupMessageController
*
* DESCRIPTION : Controller class for the deployment of a 
*               VF Component popup message
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Leonard Alarcon              11/28/2014          -  Original Version
*
*/
global class SplashPopupMessageController{     
    
    public List<UserMessage> userMessages {get;set;}
    
    /**
     *  @Constructor
     */
    public SplashPopupMessageController() {   
       userMessages =  LoadUserMessages(); 
    }
    
    @RemoteAction
    global static boolean checkMessages(){
        List<UserMessage> resList = LoadUserMessages();

         return !resList.isEmpty();
    }
    
    public SplashPopupMessageController(ApexPages.StandardController stdController) {}
    
    public static List<UserMessage> LoadUserMessages() {
        List<UserMessage> resList = new List<UserMessage>();
        Date dte_today = Date.Today();
        
        for( Message__c msgObj:[SELECT Name, Message_Text__c, Dismissible__c, Alert_End_Date__c, Alert_Begin_Date__c, Active__c,
                                      (Select OwnerId, User__c, Message_Hidden_Date__c From Hidden_Messages__r Where User__c = :UserInfo.getUserId()) 
                                FROM Message__c
                                WHERE Alert_Begin_Date__c <= :dte_today 
                                      AND Alert_End_Date__c >= :dte_today  
                                      AND Active__c = true ORDER BY CreatedDate DESC] )
        {
            if( msgObj.Dismissible__c && msgObj.Hidden_Messages__r.size()==0) {
                resList.add( new UserMessage(msgObj) );
            }
            else if(msgObj.Dismissible__c == false) {
                resList.add( new UserMessage(msgObj) );
            }
        }
        return resList;
        
    }    
    
    public PageReference save() {
        List<Hidden_Message__c> usrHiddenMessage = new List<Hidden_Message__c>(); 
        for(UserMessage uMsg: userMessages){
            if( uMsg.msgObj.Dismissible__c && uMsg.dismissed){
                Hidden_Message__c hiddenMsgObj = new Hidden_Message__c();
                hiddenMsgObj.User__c = UserInfo.getUserId(); 
                hiddenMsgObj.Message_Hidden_Date__c = Date.today();
                hiddenMsgObj.Message__c = uMsg.msgObj.Id;
                usrHiddenMessage.add(hiddenMsgObj);
            }
        }
        try{
            insert usrHiddenMessage;
            userMessages =  LoadUserMessages(); 
        }
        catch(Exception err) {
            apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error while hiding message'+err.getMessage()+'</b>. </br>'+err.getStackTraceString()));
        }
        return null;
    }
    
    public class UserMessage {
         public Message__c msgObj {get;set;}
         public Boolean dismissed {get;set;}
         public UserMessage(Message__c m){
            msgObj = m;
         }
    }
  }