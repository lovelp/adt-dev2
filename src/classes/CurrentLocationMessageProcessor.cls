/************************************* MODIFICATION LOG ********************************************************************************************
* CurrentLocationMessageProcessor
*
* DESCRIPTION : Extends LocationDataMessageProcessor and implements the interface specific to the current 
*               location (Where are They Now?) message.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					10/14/2011				- Original Version
*
*													
*/

public class CurrentLocationMessageProcessor extends LocationDataMessageProcessor {
	
	private static final String CLASS_NAME = 'CurrentLocationMessageProcessor';
	
	public override String getName() {
		return CLASS_NAME;
	}
	
	public override Boolean isValidInput(String userId, String userIdForTeam, String searchDate) {
		
		// assume valid input unless...
		Boolean validInput = true;
		if ((userId == NONE && userIdForTeam == NONE)||(userId==null && userIdForTeam==null)) {
			// determine otherwise
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'A User or a Team must be selected.'));
			validInput = false;
		}
		
		return validInput;
	}
	
	public override LocationDataMessageParameters buildParameters(String userId, String userIdForTeam, String searchDate) {
				
		// Logic for building a list of user ids and the team indicator is common for all messages 
		// so have the parent of this class do that.
		LocationDataMessageParameters parameters = super.buildParameters(userId, userIdForTeam, searchDate);
		// Then set date and historical parameters as required for this message
		parameters.searchDate = System.today();
		parameters.historicalDataRequired = false;
		return parameters; 
		
	}
	
	public override Boolean interpretResults(LocationDataMessageParameters parameters, List<LocationItem> itemsList) {
		
		Boolean readyForDisplayOnMap = true;
		
		// Results are presented differently for a team
		if (parameters.isForATeam) {	
			if (!itemsList.isEmpty()){
				Set <Id> foundUserSet = new Set<Id>();
	 			for (LocationItem loc:itemsList ){
	 				if (loc.rUser==null) {
	 					List<Id> teamManagerIdList = new List<Id>();
						teamManagerIdList.add(parameters.teamManagerId);
						inferUserNames(teamManagerIdList);
	 					ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No location history available for  ' + getUserName(parameters.teamManagerId) +'\'s Team.'));
	 				}	
	 				else {
	 					foundUserSet.add(loc.rUser.id);
	 				}
	 				if (loc.pDateTime.addMinutes(30)<system.now()) {
	 					ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'The latest GPS coordinates received for ' + loc.rUser.Name +' are over 30 minutes old.  The most recent recorded location is displayed below.'));
	 				}	
	 			}
	 			// Messaging about any team member who does not have any data points for past 24 hours
	 			for (User u: [select id, name from User where id in :parameters.userIdList and isActive=true]){
	 				if (!foundUserSet.contains(u.id)) {
	 					ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No GPS coordinates received for '+u.name+' in the past 24 hours.'));
	 				}
	 			}
			}
			else {
				List<Id> teamManagerIdList = new List<Id>();
				teamManagerIdList.add(parameters.teamManagerId);
				inferUserNames(teamManagerIdList);
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No location history available for  ' + getUserName(parameters.teamManagerId) +'\'s Team.'));
				readyForDisplayOnMap=false;
			}
		}
		// than for an individual user	
		else {
			if (!itemsList.isEmpty()){
	    		LocationItem latestLoc = itemsList[0];
	     		if (latestLoc.pDateTime.addMinutes(30)<system.now()){
	     			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'The latest GPS Coordinates received for '+ latestLoc.rUser.Name + ' are over 30 minutes old.  The most recent recorded location is displayed below.'));
	     		}
	 		}
	 		else {
	 			inferUserNames(parameters.userIdList);
	 			String thisUserId = parameters.userIdList[0];
	 			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No location history available for ' + getUserName(thisUserId) + '.'));  
	 			readyForDisplayOnMap=false;
	 		}
		}	
		return readyForDisplayOnMap;
	}
			
}