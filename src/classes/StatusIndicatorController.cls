/************************************* MODIFICATION LOG ********************************************************************************************
* StatusIndicatorController
*
* DESCRIPTION : Controller for status indicator component on account / lead standard layout pages
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					3/5/2012				- Original Version
* Leonard Alarcon				5/30/2012				- Sprint#10 Added to ShowAccountStatus() method, flow
*														  for VI on when account is an ADT_NA_RESALE
*
*													
*/

public with sharing class StatusIndicatorController {
	
	public String status {get;set;}
	
	private ApexPages.Standardcontroller sc;
	
	public StatusIndicatorController(ApexPages.Standardcontroller stdcon) {
		sc = stdcon;
	}
	
	public PageReference showAccountStatus()
	{
		Id aid = sc.getId();
		Account a = [Select Id, LeadStatus__c, AccountStatus__c, SalesApptCancelledFlag__c, RecordType.Name, Type, 
		SoldDate__c, ScheduledInstallDate__c, SalesAppointmentDate__c from Account where Id = :aid];
				
		//Map<Id, String> rectypes = Utilities.getRecordTypesForObject('Account');
		//String rtname = rectypes.get(a.RecordTypeId);
		
		String leadstatus = (a.LeadStatus__c == null ? '' : a.LeadStatus__c);
		
		if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.STANDARD_ACCOUNT )
				&& a.SalesApptCancelledFlag__c != null && a.SalesApptCancelledFlag__c == IntegrationConstants.CANCELLED_BEFORE 
			)
		{
			status = 'cancelled_before';
		}
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.STANDARD_ACCOUNT )
				&& a.SalesApptCancelledFlag__c != null && a.SalesApptCancelledFlag__c == IntegrationConstants.CANCELLED_AFTER 
			)
		{
			status = 'cancelled_after';
		}
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.STANDARD_ACCOUNT )
					&& a.AccountStatus__c != null && a.AccountStatus__c == IntegrationConstants.STATUS_NIO
				) 
		{
			status = 'noInstall';
		}
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.STANDARD_ACCOUNT )
					&& (a.AccountStatus__c != null && Utilities.contains( IntegrationConstants.STATUS_SOLD_VALUES, a.AccountStatus__c))
					&& (a.SoldDate__c != null && a.SoldDate__c < system.today().addDays(-30)) 
					&& (a.ScheduledInstallDate__c == NULL || a.ScheduledInstallDate__c < system.today())
				) 
		{
			status = 'noInstall';
		}
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.STANDARD_ACCOUNT )
					&& Utilities.contains( IntegrationConstants.STATUS_CANCEL_VALUES, a.AccountStatus__c)
				) 
		{
			status = 'canceled';
		}
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.STANDARD_ACCOUNT )
				&& a.AccountStatus__c != IntegrationConstants.STATUS_SLD && a.AccountStatus__c != IntegrationConstants.STATUS_SLS && a.AccountStatus__c != IntegrationConstants.STATUS_CLS 
			)
		{
			status = 'appointment';
		}
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.STANDARD_ACCOUNT )
					&& Utilities.contains( IntegrationConstants.STATUS_SOLD_VALUES, a.AccountStatus__c)
				)
		{
			status = 'sold';
		}
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.STANDARD_ACCOUNT )
					&& Utilities.contains( IntegrationConstants.STATUS_CLOSED_VALUES, a.AccountStatus__c)
				) 
		{
			status = 'closed';
		}
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.RIF_ACCOUNT )) {
			status = 'rif'; 
		}// LA Sprint#10 - Resale, and Discontinuance then set to OOS
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.ADT_NA_RESALE ) 
				&& (a.Type == 'Discontinuance'&& a.SalesApptCancelledFlag__c == null)
				&& a.AccountStatus__c != IntegrationConstants.STATUS_SLD 
				&& a.AccountStatus__c != IntegrationConstants.STATUS_SLS 
				&& a.AccountStatus__c != IntegrationConstants.STATUS_CLS 
				&& a.AccountStatus__c != IntegrationConstants.STATUS_CAV )
		{
				status = 'former';
				if ( a.SalesAppointmentDate__c >= system.today()){
					status = 'appointment';
				} 
		} // LA Sprint#10 - Resale, Appt set to Pending Disco.
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.ADT_NA_RESALE ) 
				&& (a.Type == 'Pending Discontinuance' && a.SalesApptCancelledFlag__c == null)
				&& a.AccountStatus__c != IntegrationConstants.STATUS_SLD 
				&& a.AccountStatus__c != IntegrationConstants.STATUS_SLS 
				&& a.AccountStatus__c != IntegrationConstants.STATUS_CLS
				&& a.AccountStatus__c != IntegrationConstants.STATUS_CAV )
		{
				status = 'pendingDisco';
				
				if ( a.SalesAppointmentDate__c >= system.today()){
					status = 'appointment';
				} 
		} // LA Sprint#10 - Resale, Appt created in last 60 days.
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.ADT_NA_RESALE )
					&& a.AccountStatus__c != IntegrationConstants.STATUS_SLD 
					&& a.AccountStatus__c != IntegrationConstants.STATUS_SLS 
					&& a.AccountStatus__c != IntegrationConstants.STATUS_CLS
					&& (a.SalesAppointmentDate__c) >= system.today().addDays(-60))
		{

			status = 'appointment';
			
		} // LA Sprint#10 - Resale, Cancel Before Appt in last 60 day
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.ADT_NA_RESALE )
				&& a.SalesApptCancelledFlag__c != null 
				&& a.SalesApptCancelledFlag__c == IntegrationConstants.CANCELLED_BEFORE )
		{
			status = 'cancelled_before';
			if (a.SalesAppointmentDate__c != null)
			{
				if (a.SalesAppointmentDate__c < system.today().addDays(-60) ){
					status = 'former';
					if (a.Type == 'Pending Discontinuance'){
						status = 'pendingDisco';
					}else if (a.Type == 'New Mover'){
						status = null;
					}	
				}else if ( a.SalesAppointmentDate__c >= system.today()){
					status = 'appointment';
				}
			}
		}// LA Sprint#10 - Resale, Cancel Appt in last 60 day
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.ADT_NA_RESALE )
				&& a.SalesApptCancelledFlag__c != null 
				&& a.SalesApptCancelledFlag__c == IntegrationConstants.CANCELLED_AFTER )
		{
			status = 'cancelled_after';
			if (a.SalesAppointmentDate__c != null)
			{
				if (a.SalesAppointmentDate__c < system.today().addDays(-60) ){
					status = 'former';
					if (a.Type == 'Pending Discontinuance'){
						status = 'pendingDisco';
					}else if (a.Type == 'New Mover'){
						status = null;
					}					
				}else if ( a.SalesAppointmentDate__c >= system.today()){
					status = 'appointment';
				}
			}
			
		}  // LA Sprint#10 - Resale which meets criteria as "At Risk"
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.ADT_NA_RESALE )
					&& (a.AccountStatus__c != null 
					&& Utilities.contains( IntegrationConstants.STATUS_SOLD_VALUES, a.AccountStatus__c))
					&& (a.SoldDate__c != null && a.SoldDate__c < system.today().addDays(-30)) 
					&& (a.ScheduledInstallDate__c == NULL || a.ScheduledInstallDate__c < system.today())) 
		{
			status = 'noInstall';
			
		}// LA Sprint#10 - Resale, which has sold in last 60 days
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.ADT_NA_RESALE )
					&& Utilities.contains( IntegrationConstants.STATUS_SOLD_VALUES, a.AccountStatus__c))
		{
			//status = 'sold';
			if ((a.SoldDate__c) >= system.today().addDays(-60)){
				status = 'sold';
			}else{
				status = 'noInstall';
			}
			
		}// LA Sprint#10 - Resale account that has been closed		
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.ADT_NA_RESALE )
					&& Utilities.contains( IntegrationConstants.STATUS_CLOSED_VALUES, a.AccountStatus__c)) 
		{
			status = 'closed';
			
		}// LA Sprint#10 Set to Cancel Sale 
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.ADT_NA_RESALE )
					&& Utilities.contains( IntegrationConstants.STATUS_CANCEL_VALUES, a.AccountStatus__c)) 
		{
			status = 'canceled';
			
		}// LA Sprint#10 - Resale account is either Appt, Cancel Before or After set to Out of service
		else if (a.RecordType.Name.equalsIgnoreCase( RecordTypeName.ADT_NA_RESALE )
				&& (system.today().addDays(-60) > a.SalesAppointmentDate__c )) 
		{
			
			if ( (a.AccountStatus__c != IntegrationConstants.STATUS_SLD) 
				&& (a.AccountStatus__c != IntegrationConstants.STATUS_SLS)
				&& (a.AccountStatus__c != IntegrationConstants.STATUS_CLS) )
			{
				status = 'former';
			}
			if (a.SalesApptCancelledFlag__c != null 
				&& a.SalesApptCancelledFlag__c == IntegrationConstants.CANCELLED_BEFORE
				|| a.SalesApptCancelledFlag__c == IntegrationConstants.CANCELLED_BEFORE)
			{
				status = 'former';
			}
			
		}
		
		return null;
	}
	
	public PageReference showLeadStatus() {
		status = 'prospect';
		return null;
	}
	
}