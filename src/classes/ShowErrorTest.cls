@isTest
private class ShowErrorTest {
	
	static testMethod void testShowErrorANS() {
		
		ApexPages.PageReference ref = new PageReference('/apex/ShowError?Error=ANS');
	    Test.setCurrentPageReference(ref);
	    
	    Test.startTest();
	    
	    ShowError se = new ShowError();
	    
	    System.assert(se.ErrorMessage != null, 'Error Message should be populated');
	    
	    Test.stopTest();
		
	}
	
	static testMethod void testShowErrorDATAERROR() {
		
		ApexPages.PageReference ref = new PageReference('/apex/ShowError?Error=DATAERROR');
	    Test.setCurrentPageReference(ref);
	    
	    Test.startTest();
	    
	    ShowError se = new ShowError();
	    
	    System.assert(se.ErrorMessage != null, 'Error Message should be populated');
	    
	    Test.stopTest();
		
	}
	
	static testMethod void testShowErrorNEDTA() {
		
		ApexPages.PageReference ref = new PageReference('/apex/ShowError?Error=NEDTA');
	    Test.setCurrentPageReference(ref);
	    
	    Test.startTest();
	    
	    ShowError se = new ShowError();
	    
	    System.assert(se.ErrorMessage != null, 'Error Message should be populated');
	    
	    Test.stopTest();
		
	}
	
	static testMethod void testShowErrorNMT() {
		
		ApexPages.PageReference ref = new PageReference('/apex/ShowError?Error=NMT');
	    Test.setCurrentPageReference(ref);
	    
	    Test.startTest();
	    
	    ShowError se = new ShowError();
	    
	    System.assert(se.ErrorMessage != null, 'Error Message should be populated');
	    
	    Test.stopTest();
		
	}
	
	static testMethod void testShowErrorNOMGR() {
		
		ApexPages.PageReference ref = new PageReference('/apex/ShowError?Error=NOMGR');
	    Test.setCurrentPageReference(ref);
	    
	    Test.startTest();
	    
	    ShowError se = new ShowError();
	    
	    System.assert(se.ErrorMessage != null, 'Error Message should be populated');
	    
	    Test.stopTest();
		
	}
	
	static testMethod void testShowErrorUNM() {
		
		ApexPages.PageReference ref = new PageReference('/apex/ShowError?Error=UNM');
	    Test.setCurrentPageReference(ref);
	    
	    Test.startTest();
	    
	    ShowError se = new ShowError();
	    
	    System.assert(se.ErrorMessage != null, 'Error Message should be populated');
	    
	    Test.stopTest();
		
	}

}