/************************************* MODIFICATION LOG ********************************************************************************************
* ChangeAccountDispositionController
*
* DESCRIPTION : Supports the dispositioning of accounts.
*               It is a Controller Extension in the SFDC MVC architecture.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2011			- Original Version
*
*													
*/
public with sharing class ChangeAccountDispositionController {
	public Integer NumberOfRecords {get; set;}
	private List<Account> selectedAccounts {get; set;}
	public List<Account> selectedAccountDetails {get; set;}
    //public String bulkDispoVal {get;set;}
    public static final String mailedResiCode = 'Mailed (Resi)';
	public static final String mailedResiDetail = 'Mailed - Letter';
	public static final String mailedSBCode = 'Mailed Letter (SB)';

	//Constructor
	public ChangeAccountDispositionController(ApexPages.StandardSetController controller)
	{
		selectedAccounts = controller.getSelected();
		NumberOfRecords = selectedAccounts.size();
		if(NumberOfRecords > 0)
		{
			BuildListOfAccountsSelected();
		}
	}
	
	public pageReference Validate()
	{
		if(selectedAccounts.size() == 0)
		{
			return new pageReference('/apex/ShowError?Error=ANS');
		}
		return null;
	}
	
	public pageReference goBack()
	{
		return new pageReference('/' + schema.Sobjecttype.Account.getKeyPrefix() );
	}	
	
		
	private void BuildListOfAccountsSelected()
	{
		Set<id> accountIds = new Set<id>();
		selectedAccountDetails = new List<Account>(); 
		for(Account acct : selectedAccounts)
		{
			accountIds.add(acct.id);
		}
		
		for(List<Account> allQueriedAccounts : [select id, Name, DispositionCode__c, DispositionDetail__c, Business_Id__c, RecordType.Name, Type from Account where Id in : accountIds])
		{
			for(Account queriedAccount : allQueriedAccounts)
			{
				selectedAccountDetails.add(queriedAccount);
			}
		}
		
	}
	
	

    //------------------------------------------------------------------------------------------------------------------------------
    //Save()
    //------------------------------------------------------------------------------------------------------------------------------
	public pageReference Save()
	{
		list<Account> updatedAccounts = new list<Account>();
		boolean rifAcctExists = false;
        for(Account acc : selectedAccountDetails)
        {
        	if(acc.RecordType.Name == RecordTypeDevName.RIF_ACCOUNT || acc.Type == IntegrationConstants.TYPE_RIF)
        	{
        		rifAcctExists = true;
        		//do nothing
        	}
        	else
        	{
	        	if(acc.Business_Id__c.contains('1100'))
	        	{
	        		acc.DispositionCode__c = mailedResiCode;
	        		acc.DispositionDetail__c = mailedResiDetail;
	        	}
	        	else if(acc.Business_Id__c.contains('1200'))
	        	{
	        		acc.DispositionCode__c = mailedSBCode;
	        		acc.DispositionDetail__c = null;
	        	}
	        	updatedAccounts.add(acc);
        	}
        }

		update updatedAccounts;
		if(rifAcctExists)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'There were 1 or more RIF accounts in the selected account list. ALL ACCOUNTS EXCEPT RIF ACCOUNTS HAVE BEEN SUCCESFULLY DISPOSITIONED.');
            ApexPages.addMessage(msg);
		}
		else
		{
			return new pageReference('/' + schema.Sobjecttype.Account.getKeyPrefix() );
		}
		return null;
	}
	
}