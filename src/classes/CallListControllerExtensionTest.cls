@isTest
private class CallListControllerExtensionTest {
	
	static testMethod void testProcessWithItems() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
        
        StreetSheet__c ss;
        System.runAs(salesRep) {    
        	Account a1 = TestHelperClass.createAccountData();
        	

        	
        	ss = new StreetSheet__c();
			ss.Name = 'Unit Test Street Sheet';
		
			insert ss;
		
			StreetSheetItem__c ssi1 = new StreetSheetItem__c();
			ssi1.AccountID__c = a1.Id;
			ssi1.StreetSheet__c = ss.Id;
		
			insert ssi1;

			
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/CallList?ssid=' + ss.Id);
	    Test.setCurrentPageReference(ref);
	    
		ApexPages.StandardController sc = new ApexPages.StandardController(new StreetSheet__c());
		
		Test.startTest();
		
		CallListControllerExtension clce = new CallListControllerExtension(sc);
		System.runAs(salesRep) {
			PageReference newRef = clce.process();
			System.assert(newRef == null, 'Expect the page reference to be null meaning no navigation');
			System.assert(clce.itemList != null, 'Expect a non null list of street sheet items');
			System.assertEquals(1, clce.itemList.size(), 'Expect 2 street sheet items in the list');
		}	
		
		
		Test.stopTest();
		
	}
	
	static testMethod void testProcessWithNoItems() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
        
        StreetSheet__c ss;
        System.runAs(salesRep) {    
        	//Account a1 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
        	    	
        	ss = new StreetSheet__c();
			ss.Name = 'Unit Test Street Sheet';
		
			insert ss;
	
			
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/CallList?ssid=' + ss.Id);
	    Test.setCurrentPageReference(ref);
	    
		ApexPages.StandardController sc = new ApexPages.StandardController(new StreetSheet__c());
		
		Test.startTest();
		
		CallListControllerExtension clce = new CallListControllerExtension(sc);
		System.runAs(salesRep) {
			PageReference newRef = clce.process();
			System.assert(newRef == null, 'Expect the page reference to be null meaning no navigation');
			System.assert(clce.itemList != null, 'Expect a non null list of street sheet items');
			System.assertEquals(0, clce.itemList.size(), 'Expect 0 street sheet items in the list');
		}	
		
		
		Test.stopTest();
		
	}

}