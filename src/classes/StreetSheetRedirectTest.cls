@isTest
private class StreetSheetRedirectTest {
	
	static testMethod void testRedirect() {
		
		ApexPages.StandardController sc = new ApexPages.StandardController(new StreetSheet__c());
		
		Test.startTest();
		
		StreetSheetRedirect ssr = new StreetSheetRedirect(sc);

		PageReference newRef = ssr.redirect();
		System.assert(newRef.getUrl().contains('ManageStreetSheet'), 'Expect to be navigated to the ManageStreetSheet page');

	
		Test.stopTest();
		
		
	}

}