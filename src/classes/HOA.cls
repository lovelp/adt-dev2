/************************************* MODIFICATION LOG ********************************************************************************************
* HOAController
*
* DESCRIPTION : Data class for encapsulating information about the HOA
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel herrera             	09/24/2014			- Original Version
*
*													
*/

public class HOA {	
	
	//---------------------------------------------------------------------------------------------------
	//	public instance information
	//---------------------------------------------------------------------------------------------------
	public String 	Name			{ get; set; }
	public String 	Id				{ get; set; }
	public String 	MMBCustomerNo 	{ get; set; }
	public Boolean 	hasGeoLocation 	{ get; set; }
	public Decimal 	lat 			{ get; set; }
	public Decimal 	lon 			{ get; set; }
	public String 	SiteNo 			{ 
		get; 
		set {
			SiteNo = ( Utilities.isEmptyOrNull(value) )? '' : value;
		} 
	}
	public String 	MMBTownID 		{ 
		get; 
		set {
			MMBTownID = ( Utilities.isEmptyOrNull(value) )? '' : value;
		} 
	}
	public String 	Region 			{ 
		get; 
		set {
			Region = ( Utilities.isEmptyOrNull(value) )? '' : value;
		} 
	}
	public String 	District 		{ 
		get; 
		set {
			District = ( Utilities.isEmptyOrNull(value) )? '' : value;
		} 
	}
	public String 	Town 			{ 
		get; 
		set {
			Town = ( Utilities.isEmptyOrNull(value) )? '' : value;
		} 
	}
	
	//---------------------------------------------------------------------------------------------------
	//	static declarations used for identifying the type of standard object loaded
	//---------------------------------------------------------------------------------------------------
	public static String AccPrefix;
	public static String LeadPrefix;
	
	static {
		AccPrefix  = Schema.Sobjecttype.Account.getKeyPrefix();
		LeadPrefix = Schema.Sobjecttype.Lead.getKeyPrefix();
	}
	
	//---------------------------------------------------------------------------------------------------
	//	Custom Exception Handler 
	//---------------------------------------------------------------------------------------------------
	public class HOAException extends Exception {}
	
	//---------------------------------------------------------------------------------------------------
	//	HOA base class defining the commonly used signatures 
	//---------------------------------------------------------------------------------------------------
	private abstract class HOAInfo {
		public HOAInfo(){}
		public virtual Boolean hasAddress(){
			return false;
		}
		public virtual String getName(){
			return '';
		}
		public virtual String getId(){
			return '';
		}
		public virtual String getMMBCustomerNo(){
			return '';
		}
		public virtual String getSiteNo(){
			return '';
		}
		public virtual String getMMBTownID(){
			return '';
		}
		public virtual String getRegion(){
			return '';
		}
		public virtual String getDistrict(){
			return '';
		}
		public virtual String getTown(){
			return '';
		}
		public virtual Decimal getLatitude(){
			return 0.0;
		}
		public virtual Decimal getLongitude(){
			return 0.0;
		}
	}
	
	private class HOAAccountInfo extends HOAInfo {
		public Account InfObj {get;set;}
		public override String getName(){
			return InfObj.Name;
		} 
		public override String getId(){
			return InfObj.Id;
		}
		public override String getMMBCustomerNo(){
			return InfObj.MMBCustomerNumber__c;
		}
		public override String getSiteNo(){
			return InfObj.MMBSiteNumber__c;
		}
		public override String getMMBTownID(){
			return InfObj.MMB_TownID__c;
		}
		public override String getRegion(){
			return InfObj.ResaleRegion__c;
		}
		public override String getDistrict(){
			return InfObj.ResaleDistrict__c;
		}
		public override String getTown(){
			return InfObj.ResaleTown__c;
		}
		public override Decimal getLatitude(){
			return InfObj.Latitude__c;
		}
		public override Decimal getLongitude(){
			return InfObj.Longitude__c;
		}
		public override Boolean hasAddress(){
			return (InfObj.AddressID__c != null)?true:false;
		}
		public HOAAccountInfo( String parentId ){
			super();
			InfObj = HOAHelper.readAccountHOADetail( parentId );
		}
		public HOAAccountInfo( Account accObj ){ 
			super();
			InfObj = accObj;
		}
	}
	
	private class HOALeadInfo extends HOAInfo {
		public Lead InfObj {get;set;}
		public override String getName(){
			return InfObj.Name;
		} 
		public override String getId(){
			return InfObj.Id;
		}
		public override String getMMBCustomerNo(){
			return InfObj.MMBCustomerNumber__c;
		}
		public override String getSiteNo(){
			return InfObj.MMBSiteNumber__c;
		}
		public override String getMMBTownID(){
			return InfObj.Town__c;
		}
		public override String getRegion(){
			return InfObj.Region__c;
		}
		public override String getDistrict(){
			return InfObj.District__c;
		}
		public override String getTown(){
			return InfObj.Town__c;
		}
		public override Decimal getLatitude(){
			return InfObj.Latitude__c;
		}
		public override Decimal getLongitude(){
			return InfObj.Longitude__c;
		}
		public override Boolean hasAddress(){
			return (InfObj.AddressID__c != null)?true:false;
		}
		public HOALeadInfo( String parentId ){
			super();
			InfObj = HOAHelper.readLeadHOADetail( parentId );
		}
		public HOALeadInfo( Lead lObj ){
			super();
			InfObj = lObj;
		}
	}
	
	private HOAInfo HOADetail;
	
	private void setPropertyValueFromSObject(){
		Name 			= HOADetail.getName();
		Id 				= HOADetail.getId();
		MMBCustomerNo 	= HOADetail.getMMBCustomerNo();	
		SiteNo 			= HOADetail.getSiteNo();
		District 		= HOADetail.getDistrict();
		MMBTownID 		= HOADetail.getMMBTownID();
		Region 			= HOADetail.getRegion();
		Town 			= HOADetail.getTown();
		if ( HOADetail.hasAddress() && 
			 HOADetail.getLatitude() != 0 && 
			 HOADetail.getLatitude() != null && 
			 HOADetail.getLongitude() != null && 
			 HOADetail.getLongitude() != 0 	){
			hasGeoLocation = true;		
		}
		else {
			hasGeoLocation = false;
		}
		lat = HOADetail.getLatitude();
		lon = HOADetail.getLongitude();
		
	}
	
	/**
	 *	@Constructor
	 *	Expecting Master HOA record ID
	 */
	public HOA(String parentId){
		
		if( Utilities.isEmptyOrNull(parentId) ){
			throw new HOAException('Id of HOA expected. Received "'+parentId+'"');
		}
		
		if(parentId.startsWith(AccPrefix)){
			HOADetail = (HOAInfo) new HOAAccountInfo(parentId);
		}
		else if(parentId.startsWith(LeadPrefix)){
			HOADetail = (HOAInfo) new HOALeadInfo(parentId);
		}
		else{
			throw new HOAException('Id of parent HOA not currently supported. Only Account or Lead.' );
		}
		
		setPropertyValueFromSObject();
	}
	
	/**
	 *	@Constructor
	 *	Expecting Account HOA record
	 */
	public HOA(Account accObj){
		HOADetail = (HOAInfo) new HOAAccountInfo(accObj);
		setPropertyValueFromSObject();
	}
 
	/**
	 *	@Constructor
	 *	Expecting Lead HOA record
	 */
	public HOA(Lead leadObj){ 
		HOADetail = (HOAInfo) new HOALeadInfo(leadObj);
		setPropertyValueFromSObject();
	}
	
}