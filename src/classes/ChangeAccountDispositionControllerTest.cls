@isTest
private class ChangeAccountDispositionControllerTest {

    static testMethod void ChangeAccountDisposition() {
        Test.startTest();
        User salesRep = TestHelperClass.createSalesRepUserWithManager();
        UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
        User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId Limit 1];
        User adminUser = TestHelperClass.createAdminUser();
        List<Account> accounts = new List<Account>();
        
        ApexPages.PageReference ref = new PageReference('/apex/ChangeAccountDisposition');
        Test.setCurrentPageReference(ref);
        Account a, a1;
        
        System.runAs(adminUser) {
            a = TestHelperClass.createAccountData();
            a.OwnerId = salesRep.id;
            update a;
            accounts.add(a);
            
            a1 = TestHelperClass.createAccountData(system.today().addDays(30), '1200 - Small Business',1);
            a1.OwnerId = salesRep.id;
            update a1;
            accounts.add(a1);
        }
        
        ApexPages.Standardsetcontroller sc = new ApexPages.Standardsetcontroller(accounts);
        sc.setSelected(accounts);
        System.runAs(salesRep) {

            ChangeAccountDispositionController caet = new ChangeAccountDispositionController(sc);
            caet.Validate();
            //caet.getBulkDispoOptions();
            caet.goBack();
            //caet.bulkDispoVal = 'Test Disposition';
            caet.save();
            //Account acct = [select DispositionCode__c from Account where id=: accounts[0].id];
            //System.assertEquals(acct.DispositionCode__c, 'Mailed (Resi)', 'Disposition changed');
            Account acct = [select DispositionCode__c from Account where id = : a.id];
            System.assertEquals(acct.DispositionCode__c, 'Mailed (Resi)', 'Disposition changed');
            acct = [select DispositionCode__c from Account where id = : a1.id];
            System.assertEquals(acct.DispositionCode__c, 'Mailed Letter (SB)', 'Disposition changed');
        }       
        
        
        Test.stopTest();        
    }
}