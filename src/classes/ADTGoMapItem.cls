/************************************* MODIFICATION LOG ********************************************************************************************
* ADTGoMapItem
*
* DESCRIPTION : Data structure representing a point to be displayed on a map.
*               Capable to created a representation of itself as a String for use by VisualForce pages.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Mike Tuckman 				 8/15/13				- Original Version
*
*													
*/
public with sharing class ADTGoMapItem {

	private static final String DATA_DELIMITER = '^';
	
	private static final Set<String> badChars = new Set<String>{'[', ']'};
	
    public String rId {get;set;}
    public String createdFrom {get;set;}
    public String rPhone{get;set;}
    public String rAddress{get;set;}
    public String rName{get;set;}
    public Datetime rLastUpdated{get;set;}
    public Decimal rLat{get;set;} 
    public Decimal rLon{get;set;}
    

    public ADTGoMapItem(Decimal latitude, Decimal longitude, String address, Datetime updated) {
    	
    	rLat = latitude;
    	rLon = longitude;
    	rAddress = address;
    	rLastUpdated = updated;
    	createdFrom = 'geocoded';
    }
                                                         
    public String getCleanName(String n){
      String theName='';
      

      try{
            theName = String.escapeSingleQuotes(n+'');  
            theName = theName.trim().replaceAll('[&();:#@"!]+','');
			theName = theName.trim().replaceAll('[-~$%^*_+=?]+','');
			theName = theName.trim().replaceAll('[|\n\r{}]+','');
        
            for(String theChar: badChars)
            {
                theName = theName.replace(theChar,'');    
            }
            
            if(theName.length()>20){
            	theName=theName.substring(0,20);
            }
        }
        catch(Exception e){ 
        	theName='-';
        }
        return theName; 
      
    }
                                                           

	
	public String asString() {
		
		String itemString ='id=';
		itemString += rId;
        itemString += DATA_DELIMITER;
        itemString += 'name=';
        itemString += getCleanName(rName);
        itemString += DATA_DELIMITER;
        itemString += 'phone=';
        if (rPhone == null) {
        	itemString += '';	
        } else {
        	itemString += rPhone;
        }
        itemString += DATA_DELIMITER;
        itemString += 'lat=';
        itemString += rLat;
        itemString += DATA_DELIMITER;
        itemString += 'lon=';
        itemString += rLon;
        itemString += DATA_DELIMITER;
        itemString += 'address=';
        itemString += rAddress;
        itemString += DATA_DELIMITER;
        itemString += 'updated=';
        itemString += rLastUpdated.format();
        itemString += DATA_DELIMITER;
        System.debug('itemString....: ' + itemString);
        return itemString;
	}
	
}