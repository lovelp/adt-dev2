/* LifeshiledCallout
*
* DESCRIPTION : Class for searching Customers in Lifeshield's database
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                        DATE              TICKET            REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Viraj Shah                   	10/09/2019       	HRM-11260         Original
* 
*/
public class LifeshiledCallout {
    
    private String LS_ENDPOINT {
        get{
            return LifeshiledCalloutHelper.getLSEndpoint();
        }
    }
    
    private Integer LS_TIMEOUT {
        get{
            return LifeshiledCalloutHelper.getLSTimeout();
        }
    }
    private String HSERVICENS {
        get{
            return LifeshiledCalloutHelper.HSERVICENS;
        }
    }
    private String LS_SEARCH_SOAPENV {
        get{
            return LifeshiledCalloutHelper.LS_SEARCH_SOAPENV;              
        }
    }
    private String LS_SEARCH_HPREFIX {
        get{
            return LifeshiledCalloutHelper.LS_SEARCH_HPREFIX;
        }
    }
    private String SERVICENS {
        get{
            return LifeshiledCalloutHelper.SERVICENS;
        }
    }
    private String LS_SEARCH_PREFIX {
        get{
            return LifeshiledCalloutHelper.LS_SEARCH_PREFIX;
        }
    }
    
    private String xmlBody;
    private  Dom.Document CreateEnvelopeHeader(){
        return LifeshiledCalloutHelper.createEnvelopeHeader();
    }
    
    
    private string getValueFromTag(XMLStreamReader reader) {
        String resVal;           
        while (reader.hasNext()) {
            if (reader.getEventType() == XmlTag.END_ELEMENT) {
                break;
            } else if (reader.getEventType() == XmlTag.CHARACTERS) {
                resVal = reader.getText();
            }
            reader.next();
        }            
        return resVal;
    }
    private dom.XmlNode setXMLHeader(DOM.document soap){
        DOM.Xmlnode envelope = soap.getRootElement();
        
        // ADD HEADER TO ENVELOPE
        dom.XmlNode header = envelope.addChildElement('Header', LS_SEARCH_SOAPENV, null);
        dom.XmlNode security = header.addChildElement('Security', HSERVICENS, LS_SEARCH_HPREFIX);
        dom.XmlNode token = security.addChildElement('UsernameToken', HSERVICENS, LS_SEARCH_HPREFIX);
        token.addChildElement( 'Username', HSERVICENS, LS_SEARCH_HPREFIX).addTextNode('xxx');
        token.addChildElement( 'Password', HSERVICENS, Ls_SEARCH_HPREFIX).addTextNode('yyy');
        
        // ADD BODY TO ENVELOPE
        dom.XmlNode body = envelope.addChildElement('Body', LifeshiledCalloutHelper.LS_SEARCH_SOAPENV, null);
        return body;
    }
    private void setXMLBody(){                     
        //CREATE THE ENVELOPE HEADER
        DOM.document soap = CreateEnvelopeHeader();
        dom.XmlNode body = setXMLHeader(soap);
        
        // ADD SITE NODE
        DOM.XmlNode siteNode = body.addChildElement( 'LookupSite', LifeshiledCalloutHelper.SERVICENS, LifeshiledCalloutHelper.LS_SEARCH_PREFIX );
        
        siteNode.addChildElement( 'SiteNo', SERVICENS, LS_SEARCH_PREFIX);
        siteNode.addChildElement( 'FirstName', SERVICENS, LS_SEARCH_PREFIX);
        siteNode.addChildElement( 'LastName', SERVICENS, LS_SEARCH_PREFIX);
        siteNode.addChildElement( 'Address1', SERVICENS, LS_SEARCH_PREFIX).addTextNode('1501 Yamato Rd');
        siteNode.addChildElement( 'City', SERVICENS, LS_SEARCH_PREFIX);
        siteNode.addChildElement( 'State', SERVICENS, LS_SEARCH_PREFIX);
        siteNode.addChildElement( 'ZipCode', SERVICENS, LS_SEARCH_PREFIX).addTextNode('33431');
        
        xmlBody = soap.toXmlString(); 
        System.debug('xmlbody--'+xmlBody); 
    }
    
   
    private LookupCustomerResponse ParseXMLResponse(HttpResponse response){
        System.debug('RESPONSE STATUS:' + response.getStatusCode());
        if (!(  response.getStatusCode() == 200 || response.getStatusCode() == 201 || response.getStatusCode() == 202 )) {
            System.debug('[MMBCustomerSiteSearchApi]::[LookupCustomerRequest.ParseXMLResponse] Error Response Status:'+ response.getStatus() + ' Code:' + response.getStatusCode());
            throw new IntegrationException(' StatusCode='+response.getStatusCode()+' Status='+response.getStatus());        
        }
        LookupCustomerResponse customerResponse = new LookupCustomerResponse();
        customerResponse.customerInfo = new List<CustomerInfoResponse>(); 
        
        //Get XML document from response 
        Dom.Document doc = response.getBodyDocument();
        
        Dom.XMLNode xmlNodeEnvelope = doc.getRootElement();
        Dom.XMLNode xmlNodeBody = xmlNodeEnvelope.getChildElements()[0];
        Dom.XMLNode xmlNodelookupCustomerResponse = xmlNodeBody.getChildElements()[0];
        String resNode = xmlNodelookupCustomerResponse.getName();               
        
        if( resNode == 'Fault' ){
            for(Dom.XMLNode n: xmlNodelookupCustomerResponse.getChildElements()){
                if( n.getName() == 'faultstring')
                    customerResponse.ResultSummaryMessage = n.getText();
            }
        }
        else if (resNode == 'LookupCustomerResponse' || resNode == 'LookupInformixCustomerResponse'){
            for(Dom.XMLNode n: xmlNodelookupCustomerResponse.getChildElements()){
                
                // Add SiteNode
                if( (n.getName()=='customerInfo' || n.getName()=='informixCustomerInfo') && customerResponse.customerInfo.size()<500){ // Currently supporting up to 500 records only
                    customerResponse.customerInfo.add( xmlNodeToCustomer(n) );
                }
                
                if(n.getName()=='ResultSummaryMessage'){
                    customerResponse.ResultSummaryMessage = n.getText();
                }
                
            }
        }
        system.debug('CustomerResponse ##'+customerResponse);
        return customerResponse;            
    }
    
    private HttpResponse SearchCustomer(){
        HttpResponse response = null;
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setTimeout(LS_TIMEOUT);
        request.setEndPoint(LS_ENDPOINT);
        request.setHeader('Content-Type', 'application/xml');
        
        request.setBody(xmlBody);
        System.debug('Request:' + xmlBody);
        response = (new Http()).send(request);
        System.debug('Response:' + response.getBody());
        
        return response;
    }
    
    public LookupCustomerResponse submitSearch(){
        return ParseXMLResponse(SearchCustomer());
    }
    
    public LifeshiledCallout(){
        setXMLBody();
    }
    
    public class LookupCustomerResponse {
        public CustomerInfoResponse[] customerInfo;
        public Boolean Success {
            get{
                return !Utilities.isEmptyOrNull(ResultSummaryMessage) && ResultSummaryMessage == 'SUCCESS';
            }
        }
        public String ResultSummaryMessage; 
        public LookupCustomerResponse(){}
    }
    private CustomerInfoResponse xmlNodeToCustomer(Dom.XMLNode node){
        CustomerInfoResponse cInfo = new CustomerInfoResponse();
        system.debug('##CustomerNode'+node);
        // Parse leave nodes
        /* FirstName */
        Dom.XMLNode xnFirstName = node.getChildElement('FirstName', SERVICENS);
        if(xnFirstName != null){ cInfo.FirstName=xnFirstName.getText(); }
        /* LastName */
        Dom.XMLNode xnLastName = node.getChildElement('LastName', SERVICENS);
        if(xnLastName != null){ cInfo.LastName=xnLastName.getText();    }
        /* Address1 */
        Dom.XMLNode xnAddress1 = node.getChildElement('Address1', SERVICENS);
        
        /* City */
        Dom.XMLNode xnCity = node.getChildElement('City', SERVICENS);
        if(xnCity != null){ cInfo.City=xnCity.getText();    }
        /* State */
        Dom.XMLNode xnState = node.getChildElement('State', SERVICENS);
        
        /* ZipCode */
        Dom.XMLNode xnZipCode = node.getChildElement('ZipCode', SERVICENS);
        if(xnZipCode != null){  cInfo.ZipCode=xnZipCode.getText();  }
        /* Phone */
        Dom.XMLNode xnPhone = node.getChildElement('Phone', SERVICENS);
        if(xnPhone != null){    cInfo.Phone=xnPhone.getText();  }
        /* BillingSystem */
        Dom.XMLNode xnBillingSystem = node.getChildElement('BillingSystem', SERVICENS);
        if(xnBillingSystem != null){                   
            cInfo.BillingSystem=xnBillingSystem.getText(); 
            //if the response comes as "Informix" or "informix billed" or "INF" then Billing system should be INF 
            if(String.isNotBlank(cInfo.BillingSystem) && (cInfo.BillingSystem.startsWithIgnoreCase('INF'))){
                cInfo.BillingSystem = 'INF';
            }
        }
        /* BillingCustNo */
        Dom.XMLNode xnBillingCustNo = node.getChildElement('BillingCustNo', SERVICENS);
        if(xnBillingCustNo != null){    cInfo.BillingCustNo=xnBillingCustNo.getText();  }
        /* CustomerStatus */
        Dom.XMLNode xnCustomerStatus = node.getChildElement('CustomerStatus', SERVICENS);
        if(xnCustomerStatus != null){   cInfo.CustomerStatus=xnCustomerStatus.getText();    }
        /* ServiceActive */
        Dom.XMLNode xnServiceActive = node.getChildElement('ServiceActive', SERVICENS);
        if(xnServiceActive != null){    cInfo.ServiceActive=xnServiceActive.getText();  }
        /* PastDueStatus */
        Dom.XMLNode xnPastDueStatus = node.getChildElement('PastDueStatus', SERVICENS);
        
        /*WriteOffAmount*/
        Dom.XMLNode xnWriteOffAmount = node.getChildElement('WriteOffAmount', SERVICENS);
        
        /* SiteNo */
        Dom.XMLNode xnSiteNo = node.getChildElement('SiteNo', SERVICENS);
        if(xnSiteNo != null){   cInfo.SiteNo=xnSiteNo.getText();    }
        
        /* ContractNo */
        Dom.XMLNode xnContractNo = node.getChildElement('ContractNo', SERVICENS);
        if(xnContractNo != null){   cInfo.ContractNo=xnContractNo.getText();    }
        
        /********** HRM-963 **********/
        /* Address2*/
        Dom.XmlNode xnPulseService = node.getChildElement('',SERVICENS);
        if(xnPulseService!= null){cInfo.PulseService=xnPulseService.getText();}
        /* Partner */
        Dom.XMLNode xnPartner = node.getChildElement('Partner', SERVICENS);
        if(xnPartner != null){  cInfo.Partner=xnPartner.getText();  }
        /* MTMCustNo */
        
        /* MTMSiteNo */
        Dom.XMLNode xnMTMSiteNo = node.getChildElement('MTMSiteNo', SERVICENS);
        if(xnMTMSiteNo != null){    cInfo.MTMSiteNo=xnMTMSiteNo.getText();  }
        
        return cInfo;
    }
    public class CustomerInfoResponse{
        public String FirstName           {get;set;}
        public String siteInfo            {get;set;}
        public String LastName           {get;set;}
        public String Phone              {get;set;}
        public String BillingSystem      {get;set;}
        public String BillingCustNo      {get;set;}
        public String CustomerStatus     {get;set;}
        public String ServiceActive     {get;set;}
        public String SiteAddress1 		{get;set;}
        public String SiteAddress2 		{get;set;}
        public String SiteType          {get;set;}
        public String SiteTypeId        {get;set;} // HRM - 6662
        public String SiteName          {get;set;}
        public String[] Pics            {get;set;}
        public String[] Partners        {get;set;}
        public String Partner			{get;set;}
        public String HOA               {get;set;}
        public String BHT               {get;set;}
        public String CHS               {get;set;}
        public String MTMSiteNo         {get;set;}
        public String[] BillingCustNos  {get;set;}
        public Set<String> BillingSystems  {get;set;}
        public String CS_NO             {get;set;}
        public String PulseService      {get;set;}
        public String SystemType        {get;set;}
        public String SystemStartDate   {get;set;}
        public String SystemStartTenure {get;set;}
        public String ZoneCount         {get;set;}
        public String SystemDescription {get;set;}// Added by TCS HRM - 6236
        public String Smoke             {get;set;}
        public String CoDetection       {get;set;}
        public String PanelType         {get;set;}
        public String ServicePlan       {get;set;}
        public String CsAccountType     {get;set;}
        public String CellBackup        {get;set;}
        public String SYSTEM_NO         {get;set;}
        public String ResaleEligible    {get;set;}
        public String InstallCompany    {get;set;}
        public String CellDeactivateDate{get;set;}//HRM-9466
        public String CellCOnOff        {get;set;}//HRM-9466
        public String CellTech          {get;set;}//HRM-9466
        public String ContractNo        {get;set;}//HRM-10618
        public String SiteStartDate     {get;set;}//HRM-10745
        public String SiteStartTenure   {get;set;}
        public String SiteNo   			{get;set;}
        public String City   			{get;set;}
        public String ZipCode   		{get;set;}
        
    }
}