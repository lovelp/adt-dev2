@isTest
public with sharing class LeadLocatorPreProcessorControllerTest 
{
    
    
    @testSetup static void testDataSetup(){
        
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'unitTestSalesMgr3@testorg.com'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        
        insert rgvList;
        
    } 
    
    
   static testMethod void testInitFromAccountWithLatLon() 
   {
     User manager = TestHelperClass.createManagerWithTeam();     
      PageReference pageRef;
      
      Test.startTest();   
      
      System.runAs(manager) 
      {
        Postal_Codes__c pc = new Postal_Codes__c();
      pc.BusinessID__c = '1100 - Residential';
      pc.Name = '221o2';
      insert pc;
    
        Account a = TestHelperClass.createAccountData();      
      
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocatorPreProcessor');
        ref.getParameters().put('aid', a.Id);  
        Test.setCurrentPageReference(ref);
        
        LeadLocatorPreProcessorController llc = new LeadLocatorPreProcessorController();
        pageRef = llc.init();        
      }
      System.assert(pageRef != null, 'Expect to receive a non-null PageReference');
      
      Test.stopTest();      
   }
   
  
   static testMethod void testInitFromLeadWithLatLon() 
   {
     User manager = TestHelperClass.createManagerWithTeam();
      PageReference pageRef;
           
      Test.startTest();   
      
      System.runAs(manager) 
      {
        Postal_Codes__c pc = new Postal_Codes__c();
      pc.BusinessID__c = '1100 - Residential';
      pc.Name = '221o2';
      insert pc;
      
        String Id = TestHelperClass.inferPostalCodeID('221o2', '1100');
        //Create Addresses first
      Address__c addr = new Address__c();
      addr.Latitude__c = 38.94686000000000;
      addr.Longitude__c = -77.25470100000000;
      addr.Street__c = '8952 Brook Rd';
      addr.City__c = 'McLean';
      addr.State__c = 'VA';
      addr.PostalCode__c = '221o2';
      addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';      
      insert addr;
        
        Lead l = new Lead();
      l.LastName = 'TestLastName';
      l.Company = 'TestCompany';
      l.DispositionCode__c = 'Phone - No Answer';
      l.Channel__c = 'Resi Direct Sales';
      l.SiteStreet__c = '8952 Brook Rd';
      l.SiteCity__c = 'McLean';    
      l.SiteStateProvince__c = 'VA';
      l.SiteCountryCode__c = 'US';
      l.SitePostalCode__c = '221o2';
      l.PostalCodeID__c = null;
      l.AddressID__c = addr.Id;
      l.Business_Id__c = null;      
      insert l;
      
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocatorPreProcessor');
        ref.getParameters().put('aid', l.Id); 
        Test.setCurrentPageReference(ref);
        
        LeadLocatorPreProcessorController llc = new LeadLocatorPreProcessorController();
        pageRef = llc.init();       
      }
      System.assert(pageRef != null, 'Expect to receive a non-null PageReference');
      
      Test.stopTest();      
   }
   
   
   static testMethod void testInitFromAccountWithoutLatLon() 
   {
     User manager = TestHelperClass.createManagerWithTeam();     
      PageReference pageRef;

     
  
     System.runAs(manager) 
      {
      Postal_Codes__c pc = new Postal_Codes__c();
      pc.BusinessID__c = '1100 - Residential';
      pc.Name = '221o2';
      insert pc;
    
      Address__c addr1 = new Address__c();
      addr1.Latitude__c = 0;
      addr1.Longitude__c = 0;
      addr1.Street__c = '8952 Brook Rd';
      addr1.City__c = 'McLean';
      addr1.State__c = 'VA';
      addr1.PostalCode__c = '221o2';
      addr1.CountryCode__c= 'US';
      addr1.OriginalAddress__c = '8952 Brook Rd, McLean, VA';      
      insert addr1;
      
      
        Account a = TestHelperClass.createAccountData();      
        a.AddressID__c= addr1.id;
        update a;
       
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocatorPreProcessor');
        ref.getParameters().put('aid', a.Id);  
        Test.setCurrentPageReference(ref);
            
       Test.startTest();
       Test.setMock(HttpCalloutMock.class, new GoogleGeoCodeMockGenerator ());  


        LeadLocatorPreProcessorController llc = new LeadLocatorPreProcessorController();
        pageRef = llc.init();  
         Test.stopTest();        
      }
     //System.assert(pageRef != null, 'Expect to receive a non-null PageReference');
      
         
   }
   
   
   static testMethod void testInitFromLeadWithoutLatLon() 
   {
     User manager = TestHelperClass.createManagerWithTeam();
      PageReference pageRef;
           
         
      
      System.runAs(manager) 
      {
        Postal_Codes__c pc = new Postal_Codes__c();
      pc.BusinessID__c = '1100 - Residential';
      pc.Name = '221o2';
      insert pc;
      
        String Id = TestHelperClass.inferPostalCodeID('221o2', '1100');
        //Create Addresses first
      Address__c addr = new Address__c();
      addr.Latitude__c = 0;
      addr.Longitude__c = 0;
      addr.Street__c = '8952 Brook Rd';
      addr.City__c = 'McLean';
      addr.State__c = 'VA';
      addr.PostalCode__c = '221o2';
      addr.OriginalAddress__c = '8952 Brook, McLean, VA 221o2';      
      insert addr;
        
        Lead l = new Lead();
      l.LastName = 'TestLastName';
      l.Company = 'TestCompany';
      l.DispositionCode__c = 'Phone - No Answer';
      l.Channel__c = 'Resi Direct Sales';
      l.SiteStreet__c = '8952 Brook Rd';
      l.SiteCity__c = 'McLean';    
      l.SiteStateProvince__c = 'VA';
      l.SiteCountryCode__c = 'US';
      l.SitePostalCode__c = '221o2';
      l.PostalCodeID__c = null;
      l.AddressID__c = addr.Id;
      l.Business_Id__c = null;      
      insert l;
      
      ApexPages.PageReference ref = new PageReference('/apex/LeadLocatorPreProcessor');
      ref.getParameters().put('aid', l.Id); 
      Test.setCurrentPageReference(ref);
        
      Test.startTest();
      Test.setMock(HttpCalloutMock.class, new GoogleGeoCodeMockGenerator ());  
    
      LeadLocatorPreProcessorController llc = new LeadLocatorPreProcessorController();
      pageRef = llc.init();       
      }
      System.assert(pageRef != null, 'Expect to receive a non-null PageReference');
      
      Test.stopTest();      
   }
   
     
}