/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class HOATest {

    static testMethod void HOAControllerTest() {
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {        
          u = TestHelperClass.createSalesRepUser();
        }
	    Account acct = new Account();
	    Account acct1 = new Account();
		system.runas(u) {
			
	        //Create Addresses first
	        Address__c addr = new Address__c();
	        addr.Latitude__c = 38.94686000000000;
	        addr.Longitude__c = -77.25470100000000;
	        addr.Street__c = '8952 Brook Rd';
	        addr.City__c = 'McLean';
	        addr.State__c = 'VA';
	        addr.PostalCode__c = '221o2';
	        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';	        
	        insert addr;	        
	        
	        // Association account
	        acct.AddressID__c = addr.Id;
	        acct.Name = 'HOA TEST';
	        acct.Business_Id__c = '1100 - Residential';
	        acct.Channel__c = 'HOA Direct Sales';
			acct.HOA_Type__c = 'Association';
	        insert acct;
	        
	        acct.MMBMasterCustomerNumber__c = '12342689'; 
			acct.MMBCustomerNumber__c = '12342689';
			acct.MMBSiteNumber__c = '12332116';
	        update acct;

        	// Customer/Site
        	acct1.Phone = '1231231834';
	        acct1.AddressID__c = addr.Id;
	        acct1.Name = TestHelperClass.ACCOUNT_NAME + '2';
	        acct1.Business_Id__c = '1100 - Residential';
			acct1.HOA_Type__c = 'Homeowner/Site';
	        acct1.Channel__c = 'HOA Direct Sales';
	        acct1.MMBMasterCustomerNumber__c = '12342689'; 
			acct1.MMBCustomerNumber__c = '12342089';
			acct1.MMBSiteNumber__c = '12330116';
	        insert acct1; 
			
		}
		
		
		PageReference pRef = Page.HOAView;
	    Test.setCurrentPage(pRef);
		
		Apexpages.currentPage().getParameters().put('Id', acct.Id);
		
		//List<HOA> hoaList = HOAViewController.getHOAAssociationSiteList( a.Id );
				
    	HOAViewController HOACtrl = new HOAViewController(); 
    	Boolean renderCustomerSite = HOACtrl.renderCustomerSite;
    	Boolean HOAAssociationIsGeocoded = HOACtrl.HOAAssociationIsGeocoded;
    	List<HOA> hL = HOAViewController.getHOAAssociationSiteList( acct.Id );
    	system.assert(hL != null && hL.size() > 0 , 'No Homeowner site found on this Association');
    	
    	PageReference pr1 = HOACtrl.clearSiteDetailInfo();
		Apexpages.currentPage().getParameters().put('hoaSiteCustomerId', acct1.Id);
    	PageReference pr2 = HOACtrl.HOASiteDetail();
    	

    }

    static testMethod void HOARehashTest() {
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
		Lead hL = new Lead();
		Lead aL = new Lead();
        system.runas(current) {
          u = TestHelperClass.createSalesRepUser();
          u.Business_Unit__c = 'HOA Direct Sales';
          update u;
        }
		system.runas(u) {
            
            
        	String Id = TestHelperClass.inferPostalCodeID('94403', '1100');
            Address__c addr=new Address__c();
            addr.Street__c = '3182 Campus Dr1';
        	addr.City__c = 'San Mateo';
        	addr.State__c = 'CA';
        	addr.PostalCode__c = '94403';
        	addr.OriginalAddress__c = '3182 CAMPUS DR1+SAN MATEO+CA+94403';
            insert addr;
        	hL.LastName = 'TestLastName';
        	hL.Company = 'TestCompany';
			hL.Channel__c = 'HOA Direct Sales';
        	hL.SiteStreet__c = '3182 Campus Dr1';
        	hL.SiteCity__c = 'San Mateo';      
        	hL.SiteStateProvince__c = 'CA';
        	hL.SiteCountryCode__c = 'US';
        	hL.SitePostalCode__c = '94403';
			hL.Business_Id__c = '1100 - Residential';
			hL.HOA_Type__c = 'Homeowner/Site';
			hL.MMBCustomerNumber__c = '12342089';
			hL.MMBSiteNumber__c = '12330116';
            hL.AddressID__c=addr.Id;
			insert hL;

        	aL.LastName = 'TestLastName2';
        	aL.Company = 'TestCompany2';
			aL.Channel__c = 'HOA Direct Sales';
        	aL.SiteStreet__c = '3182 Campus Dr1';
        	aL.SiteCity__c = 'San Mateo';      
        	aL.SiteStateProvince__c = 'CA';
        	aL.SiteCountryCode__c = 'US';
        	aL.SitePostalCode__c = '94403';
			aL.Business_Id__c = '1100 - Residential';
			aL.HOA_Type__c = 'Association';
			insert aL;
		}
		
    	HOA hoaObj = new HOA(aL.Id); 
		LeadConversion	lc = new LeadConversion(aL.Id);
		Boolean success = lc.convertLead();
		system.assert(success, 'Conversion attempt failed.');
		
		Lead l = new Lead();
		l.MMBMasterCustomerNumber__c = '1234'; 
		l.HOA_Type__c = 'Homeowner/Site';
		l.MMBCustomerNumber__c = '1234';
		l.Channel__c = Channels.TYPE_HOADIRECTSALES;
    	Channels.setHOADefaults(l); 
    	
    	hoaObj = new HOA(l); 
    	
		Account a = new Account();
		a.MMBMasterCustomerNumber__c = '1234'; 
		a.HOA_Type__c = 'Homeowner/Site';
		a.MMBCustomerNumber__c = '1234';
		a.Channel__c = Channels.TYPE_HOADIRECTSALES;
    	Channels.setHOADefaults(a); 
    	
    	try{
    		hoaObj = new HOA('');
    	}catch(Exception ex){
    		system.assert(ex.getMessage().contains('Id of HOA expected. Received'));
    	} 
    	try{
    		hoaObj = new HOA(Userinfo.getUserId());
    	}catch(Exception ex){
    		system.assert(ex.getMessage().contains('Id of parent HOA not currently supported. Only Account or Lead.'));
    	}
    }
    
}