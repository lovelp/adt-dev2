public with sharing class ChatterUtilitiesTest {
	@isTest(seeAllData=true)
	static void testLinkPost() {
		User u;
		User current = [select Id from User where Id = :UserInfo.getUserId()];
		system.runas(current) {
			u = TestHelperClass.createManagerUser();
		}
		String link = 'http://www.acumensolutions.com';
		String bodytext = 'bodytext';
		
		FeedItem fi;
		test.startTest();
			fi = ChatterUtilities.chatterPost(u.Id, bodytext, true, link);
		test.stopTest();
		
		FeedItem ficonfirm = [Select Type, LinkUrl, Title, Body from FeedItem where Id = :fi.Id];
		
		system.assertEquals( ChatterUtilities.LINK_POST, ficonfirm.Type);
		system.assertEquals( link, ficonfirm.LinkUrl);
		system.assertEquals( 'Click for details.', ficonfirm.Title);
		system.assertEQuals( bodytext, ficonfirm.Body);
	}

	@isTest(seeAllData=true)
	static void testTextPost() {
		User u;
		User current = [select Id from User where Id = :UserInfo.getUserId()];
		system.runas(current) {
			u = TestHelperClass.createManagerUser();
		}
		String bodytext = 'bodytext';
		
		FeedItem fi;
		test.startTest();
			fi = ChatterUtilities.chatterPost(u.Id, bodytext, true);
		test.stopTest();
		
		FeedItem ficonfirm = [Select Type, LinkUrl, Title, Body from FeedItem where Id = :fi.Id];
		
		system.assertEquals( ChatterUtilities.TEXT_POST, ficonfirm.Type);
		system.assertEquals( null, ficonfirm.LinkUrl);
		system.assertEquals( null, ficonfirm.Title);
		system.assertEQuals( bodytext, ficonfirm.Body);
	}

	@isTest(seeAllData=true)
	static void testFeedItemWithFile(){
		User current = [select Id from User where Id = :UserInfo.getUserId()];
        ChatterSecurity__c cs =  new ChatterSecurity__c();
		cs.License_Enabled__c = 'Chatter Only';
		cs.Chatter_Super_User_Group__c = 'Chatter_Super_User';
        cs.file_type__c	= 'Text,ftype2,ftype3';
		insert cs;
		system.runas(current) {
			// Grant current user access to upload files to chatter
			ChatterUtilities.setChatterFileSuperUser(current.Id);
		}
		
		// Create a test file to use for upload in chatter post			
        Blob file = Blob.valueOf('This is a text file testing chatter uploading capabilities');
		ContentVersion v = new ContentVersion();
        String tStamp = ''+Datetime.now().getTime();
		v.versionData = file;
		v.title = 'testFile'+tStamp;
		v.PathOnClient = '/testChatterFileUpload.txt';
		v.OwnerId = UserInfo.getUserId();
		//v.FirstPublishLocationId  = UserInfo.getUserId();
        v.Origin = 'H';
		insert v;
		
		FeedItem newFI = new FeedItem();
		newFI.Type = 'ContentPost';
		newFI.RelatedRecordId = v.Id;
		newFI.ParentId = UserInfo.getUserId();
		newFI.title = 'testChatterFileUpload';
		newFI.Body = 'This post contains a file.';
		insert newFI;

		FeedComment newFCObj = new FeedComment();
		newFCObj.CommentBody = 'Some comment with file';
		newFCObj.FeedItemId = newFI.Id;
		newFCObj.RelatedRecordId = v.Id;        
		insert newFCObj;
                
        Map < String, FeedItem > feedItemObjMap = new Map < String, FeedItem >();
        Set<String> RelatedRecordIdSet = new Set<String>();
        feedItemObjMap.put(newFI.Id, newFI);
        RelatedRecordIdSet.add(newFI.RelatedRecordId);
        
        Map < String, FeedComment > feedCommentObjMap = new Map < String, FeedComment >(); 
        feedCommentObjMap.put(newFCObj.Id, newFCObj);
        Set<String> RelatedRecordIdSetComm = new Set<String>();
        RelatedRecordIdSetComm.add(newFCObj.RelatedRecordId);
        
        test.startTest();
        ChatterUtilities.EvalFileContent(feedItemObjMap, RelatedRecordIdSet);
        ChatterUtilities.EvalFileContent(feedCommentObjMap, RelatedRecordIdSetComm);
        test.stopTest();
			
	}
	
	@isTest(seeAllData=true)
	static void testBadUsers() {
		User u;
		User current = [select Id from User where Id = :UserInfo.getUserId()];
		system.runas(current) {
			u = TestHelperClass.createManagerUser();
			Profile p = [Select Id from Profile where Name IN :ProfileHelper.LIMITED_ACCESS_PROFILE_NAMES limit 1];
			u.ProfileId = p.Id;
			update u;
		}
		
		FeedItem limited;
		FeedItem guitem;
		test.startTest();
			system.runas(u) {
				limited = ChatterUtilities.chatterPost(u.Id, '', true);
			}
			guitem = ChatterUtilities.chatterPost(Utilities.getGlobalUnassignedUser(), '', true);
		test.stopTest();
		
		system.assertEquals(null, limited.Id);
		system.assertEquals(null, guitem.Id);
	}

	static testMethod void testContentVersionEvalTrigger(){       
        ChatterSecurity__c cs =  new ChatterSecurity__c();
		cs.License_Enabled__c = 'Chatter Only';
		cs.Chatter_Super_User_Group__c = 'Chatter_Super_User';
		insert cs;

		// Create a User with the Chatter Lisence
        Profile p = [select id, UserLicense.Name from profile where name='Chatter Only Enhanced'];

        User chatterUsr = new User(alias = 'stand', email='chatteruser@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testing123456@adt.com', EmployeeNumber__c='T123456',
            StartDateInCurrentJob__c = Date.today());
        insert chatterUsr;

        System.runAs(chatterUsr) {
            Test.startTest();
				//Create Content Version
				Blob file = Blob.valueOf('This is a text file testing chatter uploading capabilities');
				ContentVersion v = new ContentVersion();
				String tStamp = ''+Datetime.now().getTime();
				v.versionData = file;
				v.title = 'testFile'+tStamp;
				v.PathOnClient = '/testChatterFileUpload.txt';
				v.Origin = 'H';
				insert v;
            Test.stopTest();
        }        
	}
}