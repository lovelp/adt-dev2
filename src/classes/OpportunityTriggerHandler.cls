/************************************* MODIFICATION LOG ********************************************************************************************
 * OpportunityTriggerHandler
 *
 * DESCRIPTION : Handler class for OpportunityTrigger
 *               
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                        DATE              	TICKET            REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Jitendra Kothari                 07/24/2019         	HRM-         	CD1 - Link disposition to Opportunities as well
 * Jitendra Kothari					09/19/2019			HRM-10677		Pass Closed Lost Status Back to Sales Pilot
 */
public with sharing class OpportunityTriggerHandler {
	public static void processAfterUpdate(list<Opportunity> oppNew, map<Id, Opportunity> oldOpps){
		list<Disposition__c> newDispos = new list<Disposition__c>();
		set<Id> oppIds = new set<Id>();
		for(Opportunity newOpp : oppNew){
			Opportunity oldOpp = oldOpps != null ? oldOpps.get(newOpp.Id) : null;
			if((String.isNotBlank(newOpp.DispositionCode__c) || String.isNotBlank(newOpp.Disposition_Detail__c)) 
				&& (oldOpp == null || (newOpp.Disposition_Detail__c != oldOpp.Disposition_Detail__c
					|| newOpp.DispositionCode__c != oldOpp.DispositionCode__c))){
            	
            	Disposition__c newDispo = new Disposition__c();
		        //newDispo.AccountID__c = newOpp.AccountId;
		        newDispo.OpportunityID__c = newOpp.Id;
		        newDispo.DispositionedBy__c = UserInfo.getUserId();
		        newDispo.DispositionType__c = newOpp.DispositionCode__c;
            	newDispo.DispositionDetail__c = newOpp.Disposition_Detail__c;
            	newDispo.DispositionDate__c = DateTime.now();
            	
            	newDispos.add(newDispo);       
        	}
        	//Start HRM-10677
        	if(String.isNotBlank(newOpp.SalesAgreementId__c) && newOpp.StageName == 'Closed Lost' 
        		&& (oldOpp == null || newOpp.StageName != oldOpp.StageName)){
				oppIds.add(newOpp.Id);
				//to avoid future annotation allowed limit
				if(oppIds.size() == Limits.getLimitCallouts()){
					updateSalesAgreement(oppIds);
					oppIds.clear();
				}
			} 
			//End HRM-10677 
		}
		if(newDispos != null && !newDispos.isEmpty()){
            insert newDispos;
        } 
        //Start HRM-10677
        if(!oppIds.isEmpty()){
        	updateSalesAgreement(oppIds);
        }
        //End HRM-10677
	}  
	//Start HRM-10677 
	@future(callout=true)
	public static void updateSalesAgreement(set<Id> oppIds){
		list<Opportunity> opps = SalesAgreementAPI.getOpportunity(oppIds);
		for(Opportunity opp : opps){
			SalesAgreementSchema.SalesAgreementUpdateRequest sauReq = SalesAgreementAPI.getUpdateRequest(opp);
            opp.UpdateSalesAgreementRequestJSON__c = JSON.serialize(sauReq, true);
            SalesAgreementSchema.SalesAgreementUpdateResponse sauRes = SalesAgreementAPI.updateSalesAgreement(opp.Id, sauReq);
            opp.UpdateSalesAgreementResponseJSON__c = JSON.serialize(sauRes, true);
		}
		if(!opps.isEmpty()){
			update opps;
		}
	}
	//End HRM-10677
}