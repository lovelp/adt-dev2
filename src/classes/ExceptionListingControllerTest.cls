@isTest
public with sharing class ExceptionListingControllerTest {
	
	static testMethod void testIsManager() {
		User mgr;
		User current = [select Id from User where Id = :UserInfo.getUserId()];
		system.runas(current) {
			mgr = TestHelperClass.createManagerUser();
		}
		
		ExceptionListingController elc;
		test.startTest();
			elc = new ExceptionListingController();
			PageReference ref = elc.onPageLoad();
			system.assertNotEquals(null, ref);
			system.runas(mgr) {
				elc = new ExceptionListingcontroller();
				ref = elc.onPageLoad();
			}
			system.assertEquals(null, ref);
		test.stopTest();
	}
	
	static testMethod void testAccountCounts() {
		User mgr;
		list<User> subs;
		User exuser;
		Account subaccount, exaccount;
		Postal_Codes__c pc;
		ManagerTown__c mt;
		Account a;
		Address__c addr;
		User current = [select Id from User where Id = :UserInfo.getUserId()];
		system.runas(current) {
			mgr = TestHelperClass.createManagerWithTeam();
			subs = Utilities.getSubordinatesFromManagerRoles(new list<Id>{mgr.UserRoleId});
			exuser = TestHelperClass.createSalesRepUser();
			pc = TestHelperClass.createPostalCodes()[0];
			mt = TestHelperClass.createManagerTown(mgr, pc, Channels.TYPE_RESIDIRECT + ';' + Channels.TYPE_RESIRESALE);
			pc.TownId__c = mt.TownID__c;
			update pc;
			a = TestHelperClass.createAccountData();
			RecordType std = Utilities.getRecordType( RecordTypeName.STANDARD_ACCOUNT, 'Account');
			addr = [Select Id, PostalCode__c from Address__c where Id = :a.AddressID__c];
			subaccount = TestHelperClass.createAccountData(addr, pc.Id, false);
			subaccount.OwnerId = subs[0].Id;
			subaccount.Channel__c = Channels.TYPE_RESIDIRECT;
			subaccount.RecordTypeId = std.Id;
			subaccount.SalesAppointmentDate__c = system.now().addDays(-5);
			insert subaccount;
			exaccount = TestHelperClass.createAccountData(addr, pc.Id, false);
			exaccount.OwnerId = exuser.Id;
			exaccount.Channel__c = Channels.TYPE_RESIDIRECT;
			exaccount.RecordTypeId = std.Id;
			exaccount.SalesAppointmentDate__c = system.now().addDays(-10);
			insert exaccount;
			
			AccountShare ashare = new AccountShare();
			ashare.AccountId = exaccount.Id;
			ashare.UserOrGroupId = mgr.Id;
			ashare.AccountAccessLevel = 'Read';
			ashare.OpportunityAccessLevel = 'Read';
			insert ashare;
			
			system.debug('exaccount = ' + exaccount);
			UserRole ur = [Select Name from UserRole where id = :mgr.UserRoleId];
			system.debug('role = ' + ur);
		}
		
		ExceptionListingController elc;
		test.startTest();
			system.runas(mgr) {
				elc = new ExceptionListingController();
				system.assertEquals(2, elc.totalAccounts);
				system.assertEquals(1, elc.totalSubAccounts);
				system.assertEquals(1, elc.totalExAccounts);
				system.assertEquals(1, elc.towns.size());
			}
		test.stopTest();
	}
}