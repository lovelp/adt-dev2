/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : Channels.cls contains static variables to hold all the different channel values. This class also contains methods to 
*               set channel based on other inputs and to get phone sale channels based on non-phone sale channels
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli               02/25/2012          - Origininal Version
* Jaydip Bhattacharya           06/24/2013          - Modified for Sprint#12, to populate Lead Type as Pre Mover for BUDCO leads
* Jaydip Bhattacharya           08/01/2013          - To Assign Channel Resi Direct, changed values PM to PN and PR to PA   
* Leonard Alarcon               01/09/2014          - Added in setChannel(...) a check for NewMoverType == 'AP'                     
* Magdiel Herrera               09/19/2014          - added Business Direct Sales channel mapping to 1200 business id
* Leonard Alarcon               11/11/2014          - Added in setChannel(...) Story#686 -a check for NewMoverType == 'CI' and 'CA'                      
* Mounika Anna                  05/24/2018          - HRM-7162 Added the check for the new mover type to check 'CR'
*/

public with sharing class Channels {
    public enum BIZID_OUTPUT {LONGSTR,SHORTSTR,NUM,FULL,DEFAULT_CHANNEL}
    public static final String TYPE_RESI = 'Residential';
    public static final String TYPE_RESIDENTIAL = '1100 - Residential';
    public static final String TYPE_SB = 'Small Business';
    public static final String TYPE_SMALLBIZ = '1200 - Small Business';
    public static final String TYPE_COMM = 'Commercial';
    public static final String TYPE_COMMERCIAL = '1300 - Commercial';
    public static final String TYPE_RESIDIRECT = 'Resi Direct Sales';
    public static final String TYPE_RESIRESALE = 'Resi Resale';
    public static final String TYPE_SBDIRECT = 'SB Direct Sales';
    public static final String TYPE_SBRESALE = 'SB Resale';
    public static final String TYPE_CUSTOMHOMESALES = 'Custom Home Sales';
    public static final String TYPE_RESIDIRECTPHONESALES = 'Resi Direct Phone Sales';
    public static final String TYPE_RESIRESALEPHONESALES = 'Resi Resale Phone Sales';
    public static final String TYPE_SBDIRECTPHONESALES = 'SB Direct Phone Sales';
    public static final String TYPE_SBRESALEPHONESALES = 'SB Resale Phone Sales';
    public static final String TYPE_HOMEHEALTH = 'Home Health';
    public static final String TYPE_BUSINESSDIRECTSALES = 'Business Direct Sales';
    public static final String TYPE_NATIONAL = 'National Account Sales';
    public static final String TYPE_HOADIRECTSALES = 'HOA Direct Sales';
    public static final string TYPE_RESIRIF = 'Resi RIF';
    public static final string TYPE_SBRIF = 'SB RIF';
    public static final String BUSINESS_UNIT_RESIEXISTINGBUSINESS = 'Resi Existing Business';
    public static final String TERRITORYTYPE_BUILDER = 'Builder';
    
    public static void setChannel(Lead ld){
        if(ld.NewMoverType__c == 'RL' || ld.NewMoverType__c == 'RM' || ld.NewMoverType__c == 'RN'||ld.NewMoverType__c == 'PN'||ld.NewMoverType__c == 'PA' ||ld.NewMoverType__c == 'AP'){
            ld.Channel__c = TYPE_RESIDIRECT;            
        }else if (ld.NewMoverType__c == 'PI'){
            ld.Channel__c = TYPE_RESIRESALE; 
        }else if(ld.NewMoverType__c == 'RC' || ld.NewMoverType__c == 'CI' || ld.NewMoverType__c == 'CA' || ld.NewMoverType__c == 'HA' || ld.NewMoverType__c == 'HC' || ld.NewMoverType__c == 'CR'){
            // HRM-7162 Added the check for the new mover type to check 'CR' on the incoming data so it is updating the channel to Custom Home sales. --- Mounika Anna
            ld.Channel__c = TYPE_CUSTOMHOMESALES; 
        }else if(ld.NewMoverType__c == 'SN'){
            ld.Channel__c = TYPE_SBDIRECT; 
        }
    }
    
    // Set Biz Id for Lead
    public static void setBusinessId(Lead ld){
        if(String.isNotBlank(ld.Channel__c)){
            ld.Business_Id__c = getBusinessId(ld.Channel__c);
            if(ld.Business_Id__c == TYPE_SMALLBIZ && ld.Company == 'N/A'){
                ld.addError(ErrorMessages__c.getInstance('NA_Not_Valid_For_SB').Message__c);
            }
        }
    }

    // Get Biz Id based on channel
    public static string getBusinessId(String channel){
        if(String.isNotBlank(channel)){               
            if(channel == TYPE_SBDIRECT || channel == TYPE_SBRESALE || channel == TYPE_SBDIRECTPHONESALES || channel == TYPE_SBRESALEPHONESALES){  
                return TYPE_SMALLBIZ;
            }else if(channel == TYPE_BUSINESSDIRECTSALES || channel == TYPE_NATIONAL){
                return TYPE_COMMERCIAL;
            }else{
                return TYPE_RESIDENTIAL;
            }
        }
        return null;
    }

    public static string getDefaultChannel(String lineOfBiz){
        if(String.isNotBlank(lineOfBiz)){
            if(lineOfBiz == 'Small Business' || lineOfBiz.contains('1200') || lineOfBiz.containsIgnoreCase('smb')){
                return TYPE_SBDIRECT;
            }else if(lineOfBiz == 'Commercial' || lineOfBiz.contains('1300') || lineOfBiz.containsIgnoreCase('comm')){
                return TYPE_BUSINESSDIRECTSALES;
            }else{
                return TYPE_RESIDIRECT;
            }
        }
        return null;
    }
    
    public static string getDefaultReSaleChannel(String lineOfBiz){
        if(String.isNotBlank(lineOfBiz)){
            return lineOfBiz == 'Small Business' || lineOfBiz.contains('1200') || lineOfBiz.containsIgnoreCase('smb')? TYPE_SBRESALE : TYPE_RESIRESALE;
        }
        return null;
    }

    public static void setHOADefaults(Lead l){
        if (String.isNotBlank(l.Channel__c) && l.Channel__c == TYPE_HOADIRECTSALES){
            // Set HOAType depeding on values on the record
            if( HOAHelper.isHOACustomerSite(l) )
                l.HOA_Type__c = 'Homeowner/Site';
            if( HOAHelper.isHOAMaster(l) )
                l.HOA_Type__c = 'Association';
        }
    }

    public static void setHOADefaults(Account a){
        if (String.isNotBlank(a.Channel__c) && a.Channel__c == TYPE_HOADIRECTSALES){
            // Set HOAType depeding on values on the record
            if( HOAHelper.isHOACustomerSite(a) )
                a.HOA_Type__c = 'Homeowner/Site';
            if( HOAHelper.isHOAMaster(a) )
                a.HOA_Type__c = 'Association';
        }
    }
     
    public static String getMappedChannel(String TerritoryChannel, Boolean mapResale){
        String RetValue = TerritoryChannel;
        if(TerritoryChannel == TYPE_RESIDIRECTPHONESALES){
            RetValue = TYPE_RESIDIRECT;
        }else if(TerritoryChannel == TYPE_RESIRESALEPHONESALES && mapResale){
            RetValue = TYPE_RESIRESALE;
        }else if(TerritoryChannel == TYPE_SBDIRECTPHONESALES){
            RetValue = TYPE_SBDIRECT;
        }else if(TerritoryChannel == TYPE_SBRESALEPHONESALES && mapResale){
            RetValue = TYPE_SBRESALE;
        }else if (TerritoryChannel == TERRITORYTYPE_BUILDER)  {
            RetValue = TYPE_RESIDIRECT;
        }else if (TerritoryChannel == TYPE_RESIRIF){
            RetValue = TYPE_RESIDIRECT;
        }else if (TerritoryChannel == TYPE_SBRIF) {
            RetValue = TYPE_SBDIRECT;
        }else if (TerritoryChannel == TYPE_BUSINESSDIRECTSALES) {
            RetValue = TYPE_BUSINESSDIRECTSALES;
        }else if (TerritoryChannel == TYPE_NATIONAL) {
            RetValue = TYPE_NATIONAL;
        }
        return RetValue;   
    }

    public static String getPhoneSaleChannel(String AccountOrLeadChannel, Boolean mapResale){
        String RetValue = AccountOrLeadChannel;
        if(AccountOrLeadChannel == TYPE_RESIDIRECT){
            RetValue = TYPE_RESIDIRECTPHONESALES;
        }else if(AccountOrLeadChannel == TYPE_RESIRESALE && mapResale){
            RetValue = TYPE_RESIRESALEPHONESALES;
        }else if(AccountOrLeadChannel == TYPE_SBDIRECT){
            RetValue = TYPE_SBDIRECTPHONESALES;
        }else if(AccountOrLeadChannel == TYPE_SBRESALE && mapResale){
            RetValue = TYPE_SBRESALEPHONESALES;
        }else if(AccountOrLeadChannel == TYPE_BUSINESSDIRECTSALES ){
            RetValue = TYPE_BUSINESSDIRECTSALES;
        }else if(AccountOrLeadChannel == TYPE_NATIONAL ){
            RetValue = TYPE_NATIONAL;
        }else if(AccountOrLeadChannel == TYPE_BUSINESSDIRECTSALES ){
            RetValue = TYPE_BUSINESSDIRECTSALES;
        }else if(AccountOrLeadChannel == TYPE_NATIONAL ){
            RetValue = TYPE_NATIONAL;
        }
        return RetValue;   
    }
    
    public static string getFormatedBusinessId(String bizId, BIZID_OUTPUT outputFormat){
        if(String.isNotBlank(bizId) && outputFormat != null){
            if(bizId.contains('1200') || bizId.containsIgnoreCase('smb') || bizId == 'Small Business'){
                if(outputFormat == BIZID_OUTPUT.FULL){
                    return TYPE_SMALLBIZ;
                }else if(outputFormat == BIZID_OUTPUT.LONGSTR){
                    return TYPE_SB;
                }else if(outputFormat == BIZID_OUTPUT.SHORTSTR){
                    return 'smb';
                }else if(outputFormat == BIZID_OUTPUT.NUM){
                    return '1200';
                }
            }else if(bizId.contains('1300') || bizId.containsIgnoreCase('comm') || bizId == 'Commercial'){
                if(outputFormat == BIZID_OUTPUT.FULL){
                    return TYPE_COMMERCIAL;
                }else if(outputFormat == BIZID_OUTPUT.LONGSTR){
                    return TYPE_COMM;
                }else if(outputFormat == BIZID_OUTPUT.SHORTSTR){
                    return 'comm';
                }else if(outputFormat == BIZID_OUTPUT.NUM){
                    return '1300';
                }
            }else{
                // Default business Id is resi
                if(outputFormat == BIZID_OUTPUT.FULL){
                    return TYPE_RESIDENTIAL;
                }else if(outputFormat == BIZID_OUTPUT.LONGSTR){
                    return TYPE_RESI;
                }else if(outputFormat == BIZID_OUTPUT.SHORTSTR){
                    return 'resi';
                }else if(outputFormat == BIZID_OUTPUT.NUM){
                    return '1100';
                }
            }
        }
        return null;
    }
}