/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : DOA Request trigger that executes before or after the insert, update, delete, merge, upsert and undelete operation
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Mike Tuckman                  08/07/2015          - Origininal Version
*                                                
*/

global class QuoteWebService {
    
    webservice String QuoteID;
    webservice scpq__SciQuote__c quote;
    
    webservice static scpq__SciQuote__c returnQuote(String QuoteID) {
        
        if (Utilities.isEmptyOrNull(QuoteID)) return null;
        
        try {
            
            //List<String> fields = new List<String>(scpq__SciQuote__c.SObjectType.getDescribe().fields.getMap().keySet());
            
                
            scpq__SciQuote__c quote = [select DOAApprovalStatus__c, CreditBypassStatus__c,DepositWaived__c, DepositWaiverLock__c, DOAApprovalLevel__c, DOALock__c, DOALockID__c, EasyPay__c, Id, scpq__QuoteId__c 
                from scpq__SciQuote__c where scpq__QuoteId__c = :QuoteID];
                
            return quote;
            
        }
        catch(Exception e){
            system.debug('ERROR');
            system.debug(e);
            return null;
        }
        
    }
    
}