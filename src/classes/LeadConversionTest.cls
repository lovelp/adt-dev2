@isTest
public class LeadConversionTest {

    static testMethod void testLeadConversion()
    {
        User current = [Select Id from User where Id=:UserInfo.getUserId()];
        Lead l;
        User u;
        Account a;
        String dispcomments = 'Test copy to account';
        system.runas(current) {
            //u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();    //just doing this so it creates the postal code stuff
            l = TestHelperClass.createLead(current, false, '');
            l.DispositionComments__c = dispcomments;
            l.PostalCode = a.ShippingPostalCode;
            l.Business_Id__c = a.Business_Id__c;
            l.AddressID__c = a.AddressID__c;
            l.SiteStreet__c = a.SiteStreet__c;
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.BusinessID__c = l.Business_Id__c.Contains('1100') ? '1100' : '1200';
            pc.Name = l.PostalCode;
            insert pc;
            l.PostalCodeID__c = pc.Id;
            insert l;
            disposition__c d = new disposition__c();
            d.LeadID__c = l.Id;
            d.DispositionType__c = 'Called';
            insert d;
        }
        
        test.startTest();
            LeadConversion lc = new LeadConversion(l.Id);
            Boolean success = lc.convertLead();
        test.stopTest();
        
        List<disposition__c> dhist = new List<disposition__c>([select Id from disposition__c where AccountId__c = :lc.AccountId]);
        Account aconfirm = [select ProcessingType__c, DispositionComments__c from Account where Id = :lc.AccountId];
        
        system.assert(success);
        system.assertEquals(dispcomments, aconfirm.DispositionComments__c);
        system.assert(0 < dhist.size());
        
        system.assertEquals(IntegrationConstants.PROCESSING_TYPE_USER, aconfirm.ProcessingType__c);
    }
    
    static testMethod void testLeadConversionTelemarRehashProcessingTypeUser()
    {
        User current = [Select Id from User where Id=:UserInfo.getUserId()];
        Lead l;
        User u;
        Account a;
        String dispcomments = 'Test copy to account';
        
        Date sourceSystemCreateDate = Date.today();
        sourceSystemCreateDate = sourceSystemCreateDate.addMonths(-12);
        system.runas(current) {
            a = TestHelperClass.createAccountData();    //just doing this so it creates the postal code stuff
            l = TestHelperClass.createLead(current, false, '');
            l.DispositionComments__c = dispcomments;
            l.PostalCode = a.ShippingPostalCode;
            l.Business_Id__c = a.Business_Id__c;
            l.AddressID__c = a.AddressID__c;
            l.SiteStreet__c = a.SiteStreet__c;
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.BusinessID__c = l.Business_Id__c.Contains('1100') ? '1100' : '1200';
            pc.Name = l.PostalCode;
            insert pc;
            l.PostalCodeID__c = pc.Id;
            l.SourceAccountCreateDate__c = sourceSystemCreateDate;
            insert l;
        }
        
        test.startTest();
            LeadConversion lc = new LeadConversion(l.Id);
            Boolean success = lc.convertLead();
        test.stopTest();
        
        
        Account aconfirm = [select ProcessingType__c from Account where Id = :lc.AccountId];
        
        system.assert(success);
        
        
        system.assertEquals(IntegrationConstants.PROCESSING_TYPE_USER, aconfirm.ProcessingType__c);
    }
    
    static testMethod void testLeadConversionTelemarRehashProcessingTypeField()
    {
        User current = [Select Id from User where Id=:UserInfo.getUserId()];
        Lead l;
        User u;
        Account a;
        String dispcomments = 'Test copy to account';
        
        Date sourceSystemCreateDate = Date.today();
        sourceSystemCreateDate = sourceSystemCreateDate.addMonths(-5);
        system.runas(current) {
            a = TestHelperClass.createAccountData();    //just doing this so it creates the postal code stuff
            l = TestHelperClass.createLead(current, false, '');
            l.DispositionComments__c = dispcomments;
            l.PostalCode = a.ShippingPostalCode;
            l.Business_Id__c = a.Business_Id__c;
            l.AddressID__c = a.AddressID__c;
            l.SiteStreet__c = a.SiteStreet__c;
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.BusinessID__c = l.Business_Id__c.Contains('1100') ? '1100' : '1200';
            pc.Name = l.PostalCode;
            insert pc;
            l.PostalCodeID__c = pc.Id;
            l.SourceAccountCreateDate__c = sourceSystemCreateDate;
            insert l;
        }
        
        test.startTest();
            LeadConversion lc = new LeadConversion(l.Id);
            Boolean success = lc.convertLead();
        test.stopTest();
        
        
        Account aconfirm = [select ProcessingType__c from Account where Id = :lc.AccountId];
        
        system.assert(success);
        
        
        system.assertEquals(IntegrationConstants.PROCESSING_TYPE_FIELD, aconfirm.ProcessingType__c);
    }
}