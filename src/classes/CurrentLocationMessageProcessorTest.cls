@isTest
private class CurrentLocationMessageProcessorTest {
	
	private static final String USER = 'User';
	private static final String TEAM = 'Team';
	
	static testMethod void testIsValidInputReturnsFalse() {
		
		Test.startTest();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		Boolean returnVal = clmp.isValidInput(null, null, null);
		System.assert(!returnVal, '1. All null input should be invalid');
		System.assert(ApexPages.hasMessages(), '1. The page should have a message');
		
		returnVal = clmp.isValidInput(LocationDataMessageProcessor.NONE, LocationDataMessageProcessor.NONE, null);
		System.assert(!returnVal, '2. None as all inputs should be invalid');
		System.assert(ApexPages.hasMessages(), '2. The page should have a message');
		
		Test.stopTest();
		
	}
	
	static testMethod void testIsValidInputReturnsTrue() {
		
		Test.startTest();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		Boolean returnVal = clmp.isValidInput(USER, LocationDataMessageProcessor.NONE, null);
		System.assert(returnVal, '1. String for user and None for team should be valid');
		System.assert(!ApexPages.hasMessages(), '1. The page should not have a message');
		
		returnVal = clmp.isValidInput(USER, null, null);
		System.assert(returnVal, '2. String for user and null for team should be valid');
		System.assert(!ApexPages.hasMessages(), '2. The page should not have a message');
		
		returnVal = clmp.isValidInput(LocationDataMessageProcessor.NONE, TEAM, null);
		System.assert(returnVal, '3. String for team and None for user should be valid');
		System.assert(!ApexPages.hasMessages(), '3. The page should not have a message');
		
		returnVal = clmp.isValidInput(null, TEAM, null);
		System.assert(returnVal, '4. String for team and null for user should be valid');
		System.assert(!ApexPages.hasMessages(), '4. The page should not have a message');
		
		Test.stopTest();
		
	}
	
	static testMethod void testBuildParametersUser() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		
		Test.startTest();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = clmp.buildParameters(salesRep.Id, null, null);
		System.assert(ldmp.userIdList != null, 'Expect a non-null list of user ids');
		System.assertEquals(salesRep.Id, ldmp.userIdList[0], 'List should contain the supplied User');
		System.assertEquals(System.today(), ldmp.searchDate, 'Search date should be the current date');
		System.assert(!ldmp.historicalDataRequired, 'No historical data is required');
		System.assert(!ldmp.isForATeam, 'This search should not be for a team');
		System.assert(ldmp.teamManagerId == null, 'There should be no team manager id');
		
		ldmp = clmp.buildParameters(salesRep.Id, LocationDataMessageProcessor.NONE, null);
		System.assert(ldmp.userIdList != null, 'Expect a non-null list of user ids');
		System.assertEquals(salesRep.Id, ldmp.userIdList[0], 'List should contain the supplied User');
		System.assertEquals(System.today(), ldmp.searchDate, 'Search date should be the current date');
		System.assert(!ldmp.historicalDataRequired, 'No historical data is required');
		System.assert(!ldmp.isForATeam, 'This search should not be for a team');
		System.assertEquals(LocationDataMessageProcessor.NONE, ldmp.teamManagerId, 'Team manager id should match input');
		
		
		Test.stopTest();
		
	}
	
	static testMethod void testBuildParametersTeam() {
		
		User salesMgr = TestHelperClass.createManagerWithTeam();
		
		Test.startTest();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = clmp.buildParameters(null, salesMgr.Id, null);
		System.assert(ldmp.userIdList != null, 'Expect a non-null list of user ids');
		System.assert(ldmp.userIdList.size()>=1, 'List should contain atleast one User from the team managed by the supplied User Id');
		System.assertEquals(System.today(), ldmp.searchDate, 'Search date should be the current date');
		System.assert(!ldmp.historicalDataRequired, 'No historical data is required');
		System.assert(ldmp.isForATeam, 'This search should not be for a team');
		System.assertEquals(salesMgr.Id, ldmp.teamManagerId, 'Team manager id should match the supplied User Id');
		
		ldmp = clmp.buildParameters(LocationDataMessageProcessor.NONE, salesMgr.Id, null);
		System.assert(ldmp.userIdList != null, 'Expect a non-null list of user ids');
		System.assert(ldmp.userIdList.size()>=1, 'List should contain atleast one User from the team managed by the supplied User Id');
		System.assertEquals(System.today(), ldmp.searchDate, 'Search date should be the current date');
		System.assert(!ldmp.historicalDataRequired, 'No historical data is required');
		System.assert(ldmp.isForATeam, 'This search should not be for a team');
		System.assertEquals(salesMgr.Id, ldmp.teamManagerId, 'Team manager id should match the supplied User Id');
		
		
		Test.stopTest();
		
	}
	
	static testMethod void testInterpretResultsNoDataForUser() {
		
		
		User salesRep = TestHelperClass.createSalesRepUser();
		
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = clmp.buildParameters(salesRep.Id, null, null);
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		Test.startTest();
		
		Boolean returnValue = clmp.interpretResults(ldmp, itemsList);
		System.assert(!returnValue, 'Expect nothing to display on the map');
		System.assert(ApexPages.hasMessages(), 'Expect the page to have a message');
		
		
		Test.stopTest();
		
	}
	
	static testMethod void testInterpretResultsNoDataForTeam() {
		
		User salesMgr = TestHelperClass.createManagerWithTeam();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = clmp.buildParameters(null, salesMgr.Id, null);
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		Test.startTest();
		
		Boolean returnValue = clmp.interpretResults(ldmp, itemsList);
		System.assert(!returnValue, 'Expect nothing to display on the map');
		System.assert(ApexPages.hasMessages(), 'Expect the page to have a message');
		
		
		Test.stopTest();
		
	}
	
	static testMethod void testInterpretResultsOutdatedDataForUser() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = clmp.buildParameters(salesRep.Id, null, null);
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		LocationItem li = new LocationItem();
		li.pDateTime = Datetime.newInstance(2011, 1, 1);
		li.rUser = salesRep;
		
		itemsList.add(li);
		
		Test.startTest();
		
		Boolean returnValue = clmp.interpretResults(ldmp, itemsList);
		System.assert(returnValue, 'Expect to have outdated (more than 30 min old) data to display on the map');
		System.assert(ApexPages.hasMessages(), 'Expect the page to have a message since breadcrumb is for time: ' + li.pDateTime);
		
		Test.stopTest();
		
	}
	
	static testMethod void testInterpretResultsOutdatedDataForTeam() {
		
		User salesMgr = TestHelperClass.createManagerWithTeam();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = clmp.buildParameters(null, salesMgr.Id, null);
		
		List<Id> roleList = new List<Id>();
		roleList.add(salesMgr.userRoleId);
		
		List<User> userList = Utilities.getSubordinatesFromManagerRoles(roleList);
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		LocationItem li = new LocationItem();
		li.pDateTime = Datetime.newInstance(2011, 1, 1);
		li.rUser = userList[0];
		
		itemsList.add(li);
		
		Test.startTest();
		
		Boolean returnValue = clmp.interpretResults(ldmp, itemsList);
		System.assert(returnValue, 'Expect to have outdated (more than 30 min old) data to display on the map');
		System.assert(ApexPages.hasMessages(), 'Expect the page to have a message since breadcrumb is for time: ' + li.pDateTime);
		
		Test.stopTest();
		
	}
	
	static testMethod void testInterpretResultsDataForUser() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = clmp.buildParameters(salesRep.Id, null, null);
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		LocationItem li = new LocationItem();
		li.pDateTime = Datetime.now();
		li.rUser = salesRep;
		
		itemsList.add(li);
		
		Test.startTest();
		
		Boolean returnValue = clmp.interpretResults(ldmp, itemsList);
		System.assert(returnValue, 'Expect to have data to display on the map');
		System.assert(!ApexPages.hasMessages(), 'Do not expect the page to have a message since breadcrumb is for time: ' + li.pDateTime);
		
		Test.stopTest();
		
	}
	
	static testMethod void testInterpretResultsDataForTeam() {
		
		User salesMgr = TestHelperClass.createManagerWithTeam();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = clmp.buildParameters(null, salesMgr.Id, null);
		
		List<Id> roleList = new List<Id>();
		roleList.add(salesMgr.userRoleId);
		
		List<User> userList = Utilities.getSubordinatesFromManagerRoles(roleList);
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		LocationItem li = new LocationItem();
		li.pDateTime = Datetime.now();
		li.rUser = userList[0];
		
		itemsList.add(li);
		
		Test.startTest();
		
		Boolean returnValue = clmp.interpretResults(ldmp, itemsList);
		System.assert(returnValue, 'Expect to have data to display on the map');
		//System.assert(!ApexPages.hasMessages(), 'Do not expect the page to have a message since breadcrumb is for time: ' + li.pDateTime);
		
		Test.stopTest();
		
	}
	
	static testMethod void testInterpretResultsForTeamNoBreadcrumbIn24Hrs() {
		
		User salesMgr = TestHelperClass.createManagerWithTeam();
		User salesRep = TestHelperClass.createSalesRepUser();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = clmp.buildParameters(null, salesMgr.Id, null);
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		LocationItem li = new LocationItem();
		li.pDateTime = Datetime.now();
		li.rUser = salesRep;
		
		itemsList.add(li);
		
		Test.startTest();
		
		Boolean returnValue = clmp.interpretResults(ldmp, itemsList);
		System.assert(returnValue, 'Expect to have data to display on the map');
		System.assert(ApexPages.hasMessages(), 'Expect the page to have a message since one user has outdated (more than 24 hours old) data');
		
		Test.stopTest();
		
	}
	
	static testMethod void testInterpretResultsForTeamNullUserInLocationItem() {
		
		User salesMgr = TestHelperClass.createManagerWithTeam();
		User salesRep = TestHelperClass.createSalesRepUser();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		LocationDataMessageParameters ldmp = clmp.buildParameters(null, salesMgr.Id, null);
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		LocationItem li = new LocationItem();
		li.pDateTime = Datetime.now();
		li.rUser = null;
		
		itemsList.add(li);
		
		Test.startTest();
		
		Boolean returnValue = clmp.interpretResults(ldmp, itemsList);
		System.assert(returnValue, 'Expect to have data to display on the map');
		System.assert(ApexPages.hasMessages(), 'Expect the page to have a message since rUser is null the LocationItem');
		
		Test.stopTest();
		
	}
	
	static testMethod void testPrepareforMappingForUser() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		//LocationDataMessageParameters ldmp = clmp.buildParameters(salesRep.Id, null, null);
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		LocationItem li = new LocationItem();
		li.pDateTime = Datetime.now();
		li.rUser = salesRep;
		li.pLatitude = '38.925924';
		li.pLongitude = '-77.229562';
		
		itemsList.add(li);
		
		Test.startTest();
		
		clmp.prepareForMapping(itemsList);
		
		List<LocationDataMessageProcessor.LocationData> tableData = clmp.getLocationDataList(itemsList);
	    String mapDataPointsStr = clmp.getMapData(itemsList);
		System.assert(tableData != null, 'Expect to a non-null list for table data');
		System.assertEquals(1, tableData.size(), 'Expect the list of table data to have one element');
		System.assert(mapDataPointsStr != null, 'Expect this string ' + mapDataPointsStr + ' to contain data for mapping');
		
		Test.stopTest();
		
	}
	
	static testMethod void testGetLocationDataListForUser() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		LocationItem li = new LocationItem();
		li.pDateTime = Datetime.now();
		li.rUser = salesRep;
		li.pLatitude = '38.925924';
		li.pLongitude = '-77.229562';
		
		itemsList.add(li);
		
		Test.startTest();
		
		List<LocationDataMessageProcessor.LocationData> tableData = clmp.getLocationDataList(itemsList);
		System.assert(tableData != null, 'Expect to a non-null list for table data');
		System.assertEquals(1, tableData.size(), 'Expect the list of table data to have one element');
		
		// make a second call, since the class caches the result
		tableData = clmp.getLocationDataList(itemsList);
		System.assert(tableData != null, 'Expect to a non-null list for table data');
		System.assertEquals(1, tableData.size(), 'Expect the list of table data to have one element');
		
		Test.stopTest();
		
	}
	
	static testMethod void testGetMapDataForUser() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		LocationItem li = new LocationItem();
		li.pDateTime = Datetime.now();
		li.rUser = salesRep;
		li.pLatitude = '38.925924';
		li.pLongitude = '-77.229562';
		
		itemsList.add(li);
		
		Test.startTest();
		
	    String mapDataPointsStr = clmp.getMapData(itemsList);
		System.assert(mapDataPointsStr != null, 'Expect this string ' + mapDataPointsStr + ' to contain data for mapping');
		System.assert(mapDataPointsStr.contains(li.pLatitude), 'The data for mapping should include the latitude');
		System.assert(mapDataPointsStr.contains(li.pLongitude), 'The data for mapping should include the longitude');
		// make a second call, since the class caches the result
		mapDataPointsStr = clmp.getMapData(itemsList);
		System.assert(mapDataPointsStr != null, 'Expect this string ' + mapDataPointsStr + ' to contain data for mapping');
		System.assert(mapDataPointsStr.contains(li.pLatitude), 'The data for mapping should include the latitude');
		System.assert(mapDataPointsStr.contains(li.pLongitude), 'The data for mapping should include the longitude');
		
		Test.stopTest();
		
	}

}