@isTest
global class ADTSalesMobilityFacadeErrorMockImpl implements WebserviceMock {
    public static Boolean switchValues = false;
    
    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType)
    {
        Boolean responseAvailable = false;
        if( soapAction == 'http://www.adt.com/SalesMobility/setAppointment' )
        {
            ADTSalesMobilityFacade.setAppointmentResponse_element response_x
                = new ADTSalesMobilityFacade.setAppointmentResponse_element();
            response_x.MessageID = 'testID';
            response_x.MessageStatus = IntegrationConstants.MESSAGE_STATUS_FAIL;
            response_x.Error = 'Callout Exception';
            response_x.TelemarAccountNumber = '';
            response_x.TelemarScheduleID = '';
            response_x.TaskCode = '';
            response.put('response_x', response_x);
            responseAvailable = true;
        }
        else if( soapAction == 'http://www.adt.com/SalesMobility/setUnavailableTime' ){
            ADTSalesMobilityFacade.setUnavailableTimeResponse_element response_x
                = new ADTSalesMobilityFacade.setUnavailableTimeResponse_element();
            response_x.MessageID = 'testID';
            response_x.MessageStatus = IntegrationConstants.MESSAGE_STATUS_OK ;
            response_x.Error = '';
            response_x.TelemarScheduleID = '';
            response.put('response_x', response_x);
            responseAvailable = FALSE;
        }
        else if( responseAvailable == false ) response.put('response_x', null);
    }
    

}