@isTest
private class NewLeadControllerTest 
{
     static testMethod void testInIt() 
     {
         
      
        insert(new AlphaConversion__c(Name = 'À', AlphaCharValue__c = 'A'));
        insert(new ResaleGlobalVariables__c(Name = 'BestBuyDNIS', value__c = '800BESTADT'));
        insert(new ResaleGlobalVariables__c(Name = 'GlobalResaleAdmin', value__c = 'Global Admin'));

        List<User> usersToInsert = new List<User>();
        Profile p1 = [select id from profile where name='ADT NSC Sales Representative'];
        User salesRep = new User(alias = 'utsr1', email='unitTestSalesRep3@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p1.Id, EmployeeNumber__c = 'Z9874321Z',
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep3@testorg.com', Business_Unit__c = 'Resi Direct Sales',
                StartDateInCurrentJob__c = Date.today());
        usersToInsert.add(salesRep);

        Profile p2 = [select id from profile where name='System Administrator'];
        User sysAdmin = new User(alias = 'utsr2', email='unitTestSysAdmin@testorg.com', 
                emailencodingkey='UTF-8', firstName='Global',lastname='Admin', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p2.Id, EmployeeNumber__c = 'Z9874322Z', Address_Validation__c = true,
                timezonesidkey='America/Los_Angeles', username='unitTestSysAdmin@testorg.com', Business_Unit__c = 'Resi Direct Sales',
                StartDateInCurrentJob__c = Date.today());
        usersToInsert.add(sysAdmin);

        Profile p3 = [select id from profile where name='Best Buy Sales Representative'];
        User bestBuyUser = new User(alias = 'utsr3', email='unitTestBestBuy@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p3.Id, EmployeeNumber__c = 'Z9874323Z', Address_Validation__c = false,
                timezonesidkey='America/Los_Angeles', username='unitTestBestBuy@testorg.com', Business_Unit__c = 'Resi Direct Sales',
                StartDateInCurrentJob__c = Date.today());
        usersToInsert.add(bestBuyUser);
        
        Profile p4 = [select id from profile where name='ADT Integration User'];
        User Intuser = new User(alias = 'utsr3', email='unitTestBestBuy@testorg.com', 
                emailencodingkey='UTF-8', lastname='TestIntegration User', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p3.Id, EmployeeNumber__c = 'Z9894323Z', Address_Validation__c = false,
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com', Business_Unit__c = 'Resi Direct Sales',
                StartDateInCurrentJob__c = Date.today());
        usersToInsert.add(Intuser);
         
        insert usersToInsert;
         
        TestHelperClass.setupTestIntegrationSettings(sysAdmin);
         
        RecordType lRT = [Select Id from RecordType where SObjectType = 'Lead' limit 1];
        Postal_Codes__c p = new Postal_Codes__c (name='32223', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1200');
        insert p;
            
        Lead ld = new Lead();
        ld.FirstName = 'Test';
        ld.LastName = 'Last';
        ld.Company = 'CompanyÀ';
        ld.SiteStreet__c = '100 Main st';
        ld.SiteStreet2__c = '';
        ld.SiteCity__c = 'Jacksonville';
        ld.SiteStateProvince__c = 'FL';
        ld.SitePostalCode__c = '32223';
        ld.Channel__c = 'SB Direct Sales';

        Test.setCurrentPageReference(new PageReference('Page.NewLead')); 
        System.currentPageReference().getParameters().put('id', ld.id);
        System.currentPageReference().getParameters().put('RecordType', lRT.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ld);
        NewLeadController nlc = new NewLeadController(sc);
        
        PageReference createLeadPage;
        System.runAs(bestBuyUser){
            createLeadPage = nlc.init();
            System.currentPageReference().getParameters().put('id', ld.id);
        }
        System.runAs(sysAdmin){         
            createLeadPage = nlc.init();
            System.currentPageReference().getParameters().put('id', ld.id);
        }
        System.runAs(salesRep){
            Test.startTest();
            System.currentPageReference().getParameters().put('id', ld.id);
            TestHelperClass.createReferenceDataForTestClasses();
            /*ErrorMessages__c er = new ErrorMessages__c();
            er.Message__c = 'No zipcode found for assignment. Insert failed.';
            er.Name = 'No_Zipcode_Match';
            insert er;*/
            createLeadPage = nlc.init();            
            nlc.ld = ld;
            nlc.title = 'test';
            createLeadPage = nlc.doOverride();
            createLeadPage = nlc.doSave();            
            createLeadPage = nlc.ConvertLead();
            //createLeadPage = nlc.createLead();
            System.currentPageReference().getParameters().put('id', ld.id);
            List<Apexpages.Message> msgList = ApexPages.getMessages();
            System.assert(msgList.size()>0);
            Test.stopTest();
        }
    }
    
    static testMethod void testAddresslengthErrors() 
     {
       // insert(new  errormessages__c(name='EM_Telemar',message__c='hello'));
        insert(new AlphaConversion__c(Name = 'À', AlphaCharValue__c = 'A'));
        List<User> usersToInsert = new List<User>();
        Profile p1 = [select id from profile where name='ADT NSC Sales Representative'];
        User salesRep = new User(alias = 'utsr1', email='unitTestSalesRep3@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p1.Id, EmployeeNumber__c = 'Z9874321Z',
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep3@testorg.com', Business_Unit__c = 'Resi Direct Sales',
                StartDateInCurrentJob__c = Date.today());
        usersToInsert.add(salesRep);

        Profile p2 = [select id from profile where name='System Administrator'];
        User sysAdmin = new User(alias = 'utsr2', email='unitTestSysAdmin@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p2.Id, EmployeeNumber__c = 'Z9874322Z', Address_Validation__c = true,
                timezonesidkey='America/Los_Angeles', username='unitTestSysAdmin@testorg.com', Business_Unit__c = 'Resi Direct Sales',
                StartDateInCurrentJob__c = Date.today());
        usersToInsert.add(sysAdmin);

        Profile p3 = [select id from profile where name='Best Buy Sales Representative'];
        User bestBuyUser = new User(alias = 'utsr3', email='unitTestBestBuy@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p3.Id, EmployeeNumber__c = 'Z9874323Z', Address_Validation__c = false,
                timezonesidkey='America/Los_Angeles', username='unitTestBestBuy@testorg.com', Business_Unit__c = 'Resi Direct Sales',
                StartDateInCurrentJob__c = Date.today());
        usersToInsert.add(bestBuyUser);
        
        Profile p4 = [select id from profile where name='ADT Integration User'];
        User Intuser = new User(alias = 'utsr3', email='unitTestBestBuy@testorg.com', 
                emailencodingkey='UTF-8', lastname='TestIntegration User', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p3.Id, EmployeeNumber__c = 'Z9894323Z', Address_Validation__c = false,
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com', Business_Unit__c = 'Resi Direct Sales',
                StartDateInCurrentJob__c = Date.today());
        usersToInsert.add(Intuser);
         
        insert usersToInsert;
         
        TestHelperClass.setupTestIntegrationSettings(sysAdmin);
         
        RecordType lRT = [Select Id from RecordType where SObjectType = 'Lead' limit 1];
            
        Lead ld = new Lead();
        ld.FirstName = 'Test';
        ld.LastName = 'Last';
        ld.Company = 'CompanyÀ';
        ld.SiteStreet__c = '100 Main st100 Main st100 Main street';
        ld.SiteStreet2__c = 'unit 200';
        ld.SiteCity__c = 'Jacksonville';
        ld.SiteStateProvince__c = 'FL';
        ld.SitePostalCode__c = '32223';
        ld.Channel__c = 'SB Direct Sales';

        Test.setCurrentPageReference(new PageReference('Page.NewLead'));
        ApexPages.StandardController sc = new ApexPages.StandardController(ld);
        NewLeadController nlc = new NewLeadController(sc); 
        System.currentPageReference().getParameters().put('id', ld.id);
        System.currentPageReference().getParameters().put('RecordType', lRT.id);                
        PageReference createLeadPage;
        
        
        System.runAs(bestBuyUser){
            createLeadPage = nlc.init();
            
        }
        System.runAs(sysAdmin){         
            createLeadPage = nlc.init();
        }
        System.runAs(salesRep){
            Test.startTest();
            System.currentPageReference().getParameters().put('id', ld.id);
            TestHelperClass.createReferenceDataForTestClasses();
          /*  ErrorMessages__c er = new ErrorMessages__c();
            er.Message__c = 'No zipcode found for assignment. Insert failed.';
            er.Name = 'No_Zipcode_Match'; 
            insert er;*/
            createLeadPage = nlc.init();
            createLeadPage = nlc.ConvertLead();
            nlc.ld = ld;
            nlc.title = 'test';
            createLeadPage = nlc.doOverride();
            createLeadPage = nlc.createLead();
            //createLeadPage = nlc.doSave();
            List<Apexpages.Message> msgList = ApexPages.getMessages();
            System.assert(msgList.size()>0);
            //System.Assertequals('Address Line 1 or 2 is greater than 35 characters. Please shorten and correct',msgList.get(0).getSummary());
            Test.stopTest();
        }
    }
}