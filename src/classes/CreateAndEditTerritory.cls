/************************************* MODIFICATION LOG ********************************************************************************************
* CreateAndEditTerritory
*
* DESCRIPTION : Supports the VisualForce page used to create or edit territory.
*               It is a Controller in the SFDC MVC architecture.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2011			- Original Version
*
*													
*/
public with sharing class CreateAndEditTerritory {
	
	//Selected terrotory (populated in case of edit)
	public Territory__c SelectedTerritory {get; set;}
	//Variable to determine if the action is Create or Edit
	public Boolean isNew {get; set;}
	//All Zipcodes already assigned to an existing territory
	public TerritoryAssignment__c TerritoryZips {get; set;}
	//All availabe zipcodes that the manager can see
	public String AvailableZips {get; set;}
	//All selected Zipcodes to create territory
	public String SelectedZips {get; set;}
	//All available sales reps list
	private Map<Id, User> AllAvailableSalesRepsForManager = new Map<Id, User>();
	//list of all towns for a manager
	private List<String> Towns = new List<String>();
	//Selected managers available channels 
	private String AvailableMgrChannels = '';
	//all manager towns for a manager user
	private List<ManagerTown__c> allManagerTowns = new List<ManagerTown__c>();
	
	//Possible territory name prefixes
	private static final String TERRITORYPREFIX_RESI = 'Resi-';
	private static final String TERRITORYPREFIX_RESIRESALE = 'Resi Resale-';
	private static final String TERRITORYPREFIX_SB = 'Small Bus-';
	private static final String TERRITORYPREFIX_SBRESALE = 'Small Bus Resale-';
	private static final String TERRITORYPREFIX_CHS = 'CHS-';
	private static final String TERRITORYPREFIX_USAA = 'USAA-';
	private static final String TERRITORYPREFIX_BUILDER = 'Builder-';
	private static final String TERRITORYPREFIX_HOMEHEALTH = 'Home Health-';
	private static final String TERRITORYPREFIX_RESIRIF = 'Resi RIF-';
	private static final String TERRITORYPREFIX_SBRIF = 'SB RIF-';
	
	private static final String TERRITORYPREFIX_RESIPH = 'Resi Ph-';
	private static final String TERRITORYPREFIX_RESIRESALEPH = 'Resi Resale Ph-';
	private static final String TERRITORYPREFIX_SBPH = 'Small Bus Ph-';
	private static final String TERRITORYPREFIX_SBRESALEPH = 'Small Bus Resale Ph-';
	private static final String TERRITORYPREFIX_COMMDIRECTSALES = 'Commercial Sales-';
	private static final String TERRITORYPREFIX_COMMNATIONAL = 'National Sales-';
	
	//selected territory type
    String selectedTerritoryType = null;
    //getter for selected territory type
    public String getSelectedTerritoryType() { return selectedTerritoryType; }
    //setter for selected territory type 
    public void setSelectedTerritoryType(String sttype) { this.selectedTerritoryType = sttype; }
	
	//Indicators for the new and edit pages and steps
	public Boolean newStep1 {get; set;} //shows name and territory type selection and hides the rest
	public Boolean newStep2 {get; set;} //shows all fields including the postal code selection
	public Boolean Existing {get; set;} //shows all fields for editing an existing territory
	
	//Territory name prefix
	public String TerritoryNamePrefix {get; set;}

	private String anyLoadErrors = '';

	private Map<Id, Id> inactiveReps = new Map<Id, Id>();
	
	public pageReference Validate()
	{
    	//get the user id and the related towns that the user/manager is assigned to
		Id currentUserId = UserInfo.getUserId();
		Id currentUserRoleId = UserInfo.getUserRoleId();
		
		//Profile currentUserProfile = [select name from Profile where id = : UserInfo.getProfileId() Limit 1];
		//check if the current user id not a manager and redirect to an error page if so.
		//if(currentUserProfile.Name != 'ADT NA Resale Sales Manager')
		if (!ProfileHelper.isManager())
			return new pageReference('/apex/ShowError?Error=UNM');
		if(anyLoadErrors != '')
			return new pageReference('/apex/ShowError?Error=' + anyLoadErrors);
		return null;
	}
	
	//Constructor
	public CreateAndEditTerritory(ApexPages.StandardController controller)
	{
		SelectedTerritory = (Territory__c)controller.getRecord();	
		anyLoadErrors = buildAllSalesrepsForAManager();
		if(SelectedTerritory.id == null)
			isNew = true;
		else
			isNew = false;
			
		if(isNew){
			newStep1 = true;
			newStep2 = false;
			Existing = false;
			TerritoryNamePrefix = '';
		}
		else{
			//Get additional information for display and edit
			SelectedTerritory = [Select Id, Name, OwnerId, TerritoryType__c from Territory__c where id =: SelectedTerritory.Id Limit 1];
			SelectedTerritory.Name = ExtractNameAndSetTNPrefix(SelectedTerritory.Name, SelectedTerritory.TerritoryType__c);
			this.selectedTerritoryType = SelectedTerritory.TerritoryType__c;
			system.debug('########' + this.selectedTerritoryType);
			setSelectedSalesRep(SelectedTerritory.OwnerId);
			GetAvailableZipCodes();
			GetAssignedZipCodes();
			newStep1 = false;
			newStep2 = false;
			Existing = true;
		}
	}
	
	private String ExtractNameAndSetTNPrefix(String TerrName, String TerritoryType)
	{
		String retVal = '';
		if(TerritoryType == Channels.TYPE_RESIDIRECT)
		{
			if(TerrName.contains(TERRITORYPREFIX_RESI)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_RESI;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_RESI, '');
				TerritoryNamePrefix = TERRITORYPREFIX_RESI;
			}
		}
		else if(TerritoryType == Channels.TYPE_RESIDIRECTPHONESALES)
		{
			if(TerrName.contains(TERRITORYPREFIX_RESIPH)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_RESIPH;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_RESIPH, '');
				TerritoryNamePrefix = TERRITORYPREFIX_RESIPH;
			}
		}		
		else if(TerritoryType == Channels.TYPE_SBDIRECT)
		{
			if(TerrName.contains(TERRITORYPREFIX_SB)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_SB;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_SB, '');
				TerritoryNamePrefix = TERRITORYPREFIX_SB;
			}
		}	
		else if(TerritoryType == Channels.TYPE_SBDIRECTPHONESALES)
		{
			if(TerrName.contains(TERRITORYPREFIX_SBPH)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_SBPH;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_SBPH, '');
				TerritoryNamePrefix = TERRITORYPREFIX_SBPH;
			}
		}
		else if(TerritoryType == Channels.TYPE_RESIRESALE)
		{
			if(TerrName.contains(TERRITORYPREFIX_RESIRESALE)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_RESIRESALE;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_RESIRESALE, '');
				TerritoryNamePrefix = TERRITORYPREFIX_RESIRESALE;
			}
		}
		else if(TerritoryType == Channels.TYPE_RESIRESALEPHONESALES)
		{
			if(TerrName.contains(TERRITORYPREFIX_RESIRESALEPH)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_RESIRESALEPH;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_RESIRESALEPH, '');
				TerritoryNamePrefix = TERRITORYPREFIX_RESIRESALEPH;
			}
		}				
		else if(TerritoryType == Channels.TYPE_SBRESALE)
		{
			if(TerrName.contains(TERRITORYPREFIX_SBRESALE)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_SBRESALE;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_SBRESALE, '');
				TerritoryNamePrefix = TERRITORYPREFIX_SBRESALE;
			}
		}
		else if(TerritoryType == Channels.TYPE_SBRESALEPHONESALES)
		{
			if(TerrName.contains(TERRITORYPREFIX_SBRESALEPH)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_SBRESALEPH;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_SBRESALEPH, '');
				TerritoryNamePrefix = TERRITORYPREFIX_SBRESALEPH;
			}
		}		
		else if(TerritoryType.contains(Channels.TYPE_CUSTOMHOMESALES))
		{
			if(TerrName.contains(TERRITORYPREFIX_CHS)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_CHS;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_CHS, '');
				TerritoryNamePrefix = TERRITORYPREFIX_CHS;
			}
		}	
/*		else if(TerritoryType.contains(Channels.TERRITORYTYPE_USAA))
		{
			if(TerrName.contains(TERRITORYPREFIX_USAA)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_USAA;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_USAA, '');
				TerritoryNamePrefix = TERRITORYPREFIX_USAA;
			}
		}	
*/		
		else if(TerritoryType.contains(Channels.TERRITORYTYPE_BUILDER))
		{
			if(TerrName.contains(TERRITORYPREFIX_BUILDER)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_BUILDER;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_BUILDER, '');
				TerritoryNamePrefix = TERRITORYPREFIX_BUILDER;
			}
		}	
		else if(TerritoryType.contains(Channels.TYPE_HOMEHEALTH))
		{
			if(TerrName.contains(TERRITORYPREFIX_HOMEHEALTH)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_HOMEHEALTH;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_HOMEHEALTH, '');
				TerritoryNamePrefix = TERRITORYPREFIX_HOMEHEALTH;
			}
		}	
		else if(TerritoryType.contains(Channels.TYPE_RESIRIF))
		{
			if(TerrName.contains(TERRITORYPREFIX_RESIRIF)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_RESIRIF;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_RESIRIF, '');
				TerritoryNamePrefix = TERRITORYPREFIX_RESIRIF;
			}
		}	
		else if(TerritoryType.contains(Channels.TYPE_SBRIF))
		{
			if(TerrName.contains(TERRITORYPREFIX_SBRIF)) {
				retval = TerrName.Split('-')[1];
				TerritoryNamePrefix = TERRITORYPREFIX_SBRIF;
			} else {
				retval = TerrName.replace(TERRITORYPREFIX_SBRIF, '');
				TerritoryNamePrefix = TERRITORYPREFIX_SBRIF;
			}
		}							
		return retVal;
	}
	
	public pageReference StartOver()
	{
		PageReference pref = ApexPages.currentPage();
		pref.setRedirect(true);
		return pref;
	}	
	
	public pageReference Next()
	{
		if(selectedTerritoryType == null)
		{
			newStep1 = true;
			newStep2 = false;
			Existing = false;
			// Thorw an error message if type is not selected.
			// Name is a required field so salesforce takes care of the validation in that case
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMessages__c.getInstance('T_STT').Message__c);
			ApexPages.addMessage(myMsg);
		}
		else
		{		
			newStep1 = false;
			newStep2 = true;
			Existing = false;
			if(selectedTerritoryType == Channels.TYPE_RESIDIRECT) {
				TerritoryNamePrefix = TERRITORYPREFIX_RESI;
			} else if (selectedTerritoryType == Channels.TYPE_SBDIRECT) {
				TerritoryNamePrefix = TERRITORYPREFIX_SB;
			} else if (selectedTerritoryType == Channels.TYPE_RESIRESALE) {
				TerritoryNamePrefix = TERRITORYPREFIX_RESIRESALE;
			} else if (selectedTerritoryType == Channels.TYPE_SBRESALE) {
				TerritoryNamePrefix = TERRITORYPREFIX_SBRESALE;
			} else if (selectedTerritoryType.contains(Channels.TYPE_CUSTOMHOMESALES)) {
				TerritoryNamePrefix = TERRITORYPREFIX_CHS;
			} else if(selectedTerritoryType == Channels.TYPE_RESIDIRECTPHONESALES) {
				TerritoryNamePrefix = TERRITORYPREFIX_RESIPH;
			} else if (selectedTerritoryType == Channels.TYPE_SBDIRECTPHONESALES) {
				TerritoryNamePrefix = TERRITORYPREFIX_SBPH;
			} else if (selectedTerritoryType == Channels.TYPE_RESIRESALEPHONESALES) {
				TerritoryNamePrefix = TERRITORYPREFIX_RESIRESALEPH;
			} else if (selectedTerritoryType == Channels.TYPE_SBRESALEPHONESALES) {
				TerritoryNamePrefix = TERRITORYPREFIX_SBRESALEPH;
			} 
			/* else if (selectedTerritoryType == Channels.TERRITORYTYPE_USAA) {
				TerritoryNamePrefix = TERRITORYPREFIX_USAA;
			} 
			*/
			else if (selectedTerritoryType == Channels.TERRITORYTYPE_BUILDER) {
				TerritoryNamePrefix = TERRITORYPREFIX_BUILDER;
			} else if (selectedTerritoryType == Channels.TYPE_HOMEHEALTH) {
				TerritoryNamePrefix = TERRITORYPREFIX_HOMEHEALTH;
			} else if (selectedTerritoryType == Channels.TYPE_RESIRIF) {
				TerritoryNamePrefix = TERRITORYPREFIX_RESIRIF;
			} else if (selectedTerritoryType == Channels.TYPE_SBRIF) {
				TerritoryNamePrefix = TERRITORYPREFIX_SBRIF;
			}
			else if (selectedTerritoryType == Channels.TYPE_BUSINESSDIRECTSALES) {
				TerritoryNamePrefix = TERRITORYPREFIX_COMMDIRECTSALES;
			} 
			else if (selectedTerritoryType == Channels.TYPE_NATIONAL) {
				TerritoryNamePrefix = TERRITORYPREFIX_COMMNATIONAL;
			} 
			GetAvailableZipCodes();
		}
		return null;
	}
	
	//selected salesrep for assignment
	String selectedSalesRep = null;
	//getter for salesrep selected
	public String getSelectedSalesRep() {
        return selectedSalesRep;
    }
    //setter for salesrep selected
    public void setSelectedSalesRep(String ssr) { this.selectedSalesRep = ssr; }
    
    public List<SelectOption> getallTerritoryTypes(){
    	List<SelectOption> allAvailableOptions = PicklistHelper.getTerritoryTypeValues();
    	for(SelectOption so : allAvailableOptions)
    	{
    		so.setDisabled(true);
    		for(ManagerTown__c MT: allManagerTowns)
    		{
    			List<String> mttypeslist = MT.Type__c.split(';');
    			Set<String> mttypes = new Set<String>();
    			mttypes.addall(mttypeslist);
    			if(mttypes.contains(so.getValue()) || mttypes.contains(Channels.getMappedChannel(so.getValue(), true)))
    				so.setDisabled(false);
    		}
    	}
    	return allAvailableOptions;
    }
    
    public List<SelectOption> getallSalesreps() {
        List<SelectOption> SalesReps = new List<SelectOption>();
        SelectOption anOption; 
        User u;
        String Label;
        for(Id uid : AllAvailableSalesRepsForManager.KeySet())
        {
        	u = AllAvailableSalesRepsForManager.get(uid);
        	if(u.IsActive)
        		Label = u.Name;
        	else 
        		Label = u.Name + ' (InActive)';
        	anOption = new SelectOption(uid, Label);
        	if(!u.IsActive)
        	{
        		anOption.setDisabled(true);
        		inactiveReps.put(uid, uid);
        	}
			SalesReps.add(anOption);
        } 
		return SalesReps;
    }
    
    //Build all salesreps list and return a string in case of an error, else reutrn Blank
    private String buildAllSalesrepsForAManager()
    {
    	//get the user id and the related towns that the user/manager is assigned to
		Id currentUserId = UserInfo.getUserId();
		Id currentUserRoleId = UserInfo.getUserRoleId();
		List<Id> matrixedRoles = new List<Id>();
		matrixedRoles = Utilities.getMatrixedRolesForManager(currentUserRoleId);
		List<Id> allRoles = new List<Id>();
		allRoles.add(currentUserRoleId);
		if(matrixedRoles != null)
			allRoles.addAll(matrixedRoles);
		
		//get all subordinate roles for the user
		List<UserRole> subRoles = [select id, name from UserRole where parentRoleId =: allRoles];
		List<id> allSubordinateRoleIds = new List<Id>();
		for(UserRole ur: subRoles)
		{
			allSubordinateRoleIds.add(ur.id);
		}

		//get all salesreps for the manager
		List<User> allSubordinateUsers = [select id, name, IsActive from User where UserRoleId in : allSubordinateRoleIds];
		for(User u : allSubordinateUsers)
		{
			AllAvailableSalesRepsForManager.put(u.id, u);
		}
		
		//get all the towns that the manager is assigned to. If the manager has no towns assigned, redirect to an error page.
		List<ManagerTown__c> managerTowns = [select ManagerID__c, TownID__c, BusinessId__c, Type__c from ManagerTown__c where ManagerID__c = : currentUserId];
		allManagerTowns = managerTowns;
		if(managerTowns.size() == 0)
		{
			return 'NMT';
		} 
		for(ManagerTown__c mt : managerTowns)
		{
			Towns.add(mt.TownId__c + '-' + mt.BusinessId__c);
		}
		return '';
    }
	
	//Method to implement save process
	public pageReference Save()
	{
		String SaveError = '';
		try
		{
			List<TerritoryAssignment__c> tas = new List<TerritoryAssignment__c>();
		
			if(selectedSalesRep == null)
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMessages__c.getInstance('T_SSR').Message__c);
				ApexPages.addMessage(myMsg);
			}
			if(SelectedZips == null || SelectedZips.trim() == '')
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMessages__c.getInstance('T_SPC').Message__c);
				ApexPages.addMessage(myMsg);
			}
			if(inactiveReps.get(selectedSalesRep) != null)
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMessages__c.getInstance('T_SRI').Message__c);
				ApexPages.addMessage(myMsg);				
			}
			if(ApexPages.getMessages().size() != 0)
				return null;
			
			if(isNew)
			{
				//if creating terrotory, insert new territory record
				SelectedTerritory.OwnerId = selectedSalesRep;
				SelectedTerritory.TerritoryType__c = selectedTerritoryType;
				SelectedTerritory.Name = TerritoryNamePrefix + SelectedTerritory.Name;
				upsert SelectedTerritory;
				//Get all the selected zipcodes
				List<String> selectedZipStrings = SelectedZips.trim().split(' ');
				//Create all the Zipcode assignments and save to TerritoryAssignments
				for(String sel : selectedZipStrings)
				{
					tas.add(new TerritoryAssignment__c(TerritoryId__c=SelectedTerritory.Id, PostalCodeID__c=sel.split(';')[0], OwnerId=SelectedTerritory.OwnerId, TerritoryType__c=SelectedTerritory.TerritoryType__c ));
				}
				insert tas;
			}
			else
			{
				//Get all zipcodes selected
				List<String> selectedZipStrings = SelectedZips.trim().split(' ');
				//if editing a territoru, save edited territory details
				SelectedTerritory.OwnerId = selectedSalesRep;
				SelectedTerritory.Name = TerritoryNamePrefix + SelectedTerritory.Name;
				update SelectedTerritory;				
				//helper variables
				List<String> zipIds = new List<String>();
				List<String> zipsToSave = new List<String>();
				List<TerritoryAssignment__c> TAsToSave = new List<TerritoryAssignment__c>();
				Map<Id, TerritoryAssignment__c> matches = new Map<Id, TerritoryAssignment__c>();
				
				//get all selected zipcodes
				for(String sel : selectedZipStrings)
				{
					zipIds.add(sel.split(';')[0]);
				}
				
				//delete extra zips in the system
				List<TerritoryAssignment__c> existing_tas_todelete = [Select Id, Name, PostalCodeId__c from TerritoryAssignment__c where TerritoryID__c =: SelectedTerritory.Id and PostalCodeId__c not in : zipIds];
				delete(existing_tas_todelete);

				if(SelectedZips.trim() != '')
				{
					//get already existing zipcodes to match the selected zipcodes. These zipcodes will not be saved again
					List<TerritoryAssignment__c> existing_tas = [select id, Name, PostalCodeId__c from TerritoryAssignment__c where TerritoryID__c =: SelectedTerritory.Id and PostalCodeId__c in : zipIds];
					for(TerritoryAssignment__c t : existing_tas)
					{
						matches.put(t.PostalCodeId__c, t);
					}
					//if no match exists in territoryAssignment, mark record for save
					for(String zip : zipIds)
					{
						if(matches.get(zip) == null)
							zipsToSave.add(zip);
					}
					
					//If there are new zipcodes assignments to save, create a list of those assignments and insert them
					if(zipsToSave.size() > 0)
					{
						for(String zipsave : zipsToSave)
						{
							TAsToSave.add(new TerritoryAssignment__c(TerritoryId__c=SelectedTerritory.Id, PostalCodeID__c=zipsave, OwnerId=SelectedTerritory.OwnerId, TerritoryType__c=selectedTerritoryType ));
						}
						insert TAsToSave;
					}
				}
			}
		}
		catch (Exception ex)
		{
			SaveError = ex.getMessage();
			if(!SaveError.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, SaveError);
				ApexPages.addMessage(myMsg);
			}
			return null;
		}
		//return to the saved territory
		return new pageReference('/' + SelectedTerritory.Id);
	} 
	
	//Get all available zipcodes for a manager
	private void GetAvailableZipCodes()
	{
		AvailableZips = '';
		List<Id> allUsedZips = new List<Id>();
		List<Id> allPCIDs = new List<Id>();
		String businessId = '';
		
		//Get all the territory assignments for the postal codes with town + business id that the manager owns
		List<TerritoryAssignment__c> allExistingTAs = [Select id, name, territorytype__c, territoryid__c, PostalCodeId__c from TerritoryAssignment__c where postalcodeId__r.townuniqueid__c in : Towns and TerritoryType__c = : selectedTerritoryType];
		system.debug(allExistingTAs);
		List<Id> PCIDsNotIndluded = new List<Id>();
		for(TerritoryAssignment__c TA : allExistingTAs)
		{
			PCIDsNotIndluded.add(TA.PostalCodeId__c);
		}
		//based on the selected territory type, show or hide postal codes based on the business id. 
		if(selectedTerritoryType != null && selectedTerritoryType != '')
		{
			if(selectedTerritoryType.contains('SB'))
				businessId = '1200';
			else if(selectedTerritoryType.contains('Business Direct Sales') || selectedTerritoryType.contains('National Account Sales'))
				businessId = '1300';
			else
				businessId = '1100';
				
		}

		//get the final list of available zipcodes by applying all filters created above
		List<Postal_Codes__c> availableZipCodes = new List<Postal_Codes__c>();
		availableZipCodes = [select id, name, businessId__c from Postal_Codes__c where TownUniqueId__c in : Towns and id not in : PCIDsNotIndluded and BusinessId__c = : businessId order by name];
		system.debug(availableZipCodes);
		for(Postal_Codes__c z : availableZipCodes)
		{
			AvailableZips += z.ID + ';' + z.Name + ';' + z.BusinessId__c + ' ';
		}	
	}
	
	//get all the assigned zipcodes for a territory in case of territory editing
	private void GetAssignedZipCodes()
	{
		SelectedZips = '';
		List<TerritoryAssignment__c> TerritoryZips = [Select Name, PostalCode__c, PostalCodeID__c, TerritoryName__C, PostalcodeID__r.BusinessId__c from TerritoryAssignment__C where TerritoryId__c =: SelectedTerritory.Id order by PostalCode__c];
		for(TerritoryAssignment__c z : TerritoryZips)
		{
			SelectedZips += z.PostalCodeID__c + ';' + z.PostalCode__c + ';' + z.PostalCodeID__r.BusinessId__c + ' ';
		}
	}
}