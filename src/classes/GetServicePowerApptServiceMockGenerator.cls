/**
 Description- This test class used for GetServicePowerApptService mock response class.
 Developer- TCS Developer
 Date- 1/31/2018
 */
@isTest
global class GetServicePowerApptServiceMockGenerator implements HttpCalloutMock {

     global HTTPResponse respond(HTTPRequest req) {
     
         ADTGoDemoLocation__c obj= [select id,Active__c from ADTGoDemoLocation__c limit 1];
         
         system.debug('Active__c***'+obj.Active__c);
         HttpResponse res = new HttpResponse();
         res.setHeader('Content-Type', 'application/xml');
          // Create a fake response for a customer lookup web service call
          //xmlBodyStr = 'empty response';
         String xmlBodyStr= '   {  '  + 
            '     "jobRequests": [  '  + 
            '       {  '  + 
            '       "jobRequest": "BA",  '  + 
            '       "ErrorFlag": "false",  '  + 
            '       "ErrorMessage": "OK",  '  + 
            '         "ReturnValue": [  '  + 
            '           {  '  + 
            '             "start": "2018-02-01T08:00",  '  + 
            '             "end": "2018-02-01T12:00",  '  + 
            '             "promBand": "M",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Thu",  '  + 
            '             "offerToken": "035b5a731d9c000ffffffffffffffff00000000014c50001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "23"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-01T12:00",  '  + 
            '             "end": "2018-02-01T17:00",  '  + 
            '             "promBand": "A",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Thu",  '  + 
            '             "offerToken": "035b5a7355a0000ffffffffffffffff00000000014bb0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "25"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-02T08:00",  '  + 
            '             "end": "2018-02-02T12:00",  '  + 
            '             "promBand": "M",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Fri",  '  + 
            '             "offerToken": "035b5a746f1c000ffffffffffffffff00000000014c50001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "23"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-02T12:00",  '  + 
            '             "end": "2018-02-02T17:00",  '  + 
            '             "promBand": "A",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Fri",  '  + 
            '             "offerToken": "035b5a74a720000ffffffffffffffff00000000014bb0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "25"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-03T08:00",  '  + 
            '             "end": "2018-02-03T12:00",  '  + 
            '             "promBand": "M",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Sat",  '  + 
            '             "offerToken": "035b5a75c09c000ffffffffffffffff00000000014c50001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "23"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-03T12:00",  '  + 
            '             "end": "2018-02-03T17:00",  '  + 
            '             "promBand": "A",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Sat",  '  + 
            '             "offerToken": "035b5a75f8a0000ffffffffffffffff00000000014bb0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "25"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-05T08:00",  '  + 
            '             "end": "2018-02-05T12:00",  '  + 
            '             "promBand": "M",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Mon",  '  + 
            '             "offerToken": "035b5a78639c000ffffffffffffffff000000000428e0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "45"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-05T12:00",  '  + 
            '             "end": "2018-02-05T17:00",  '  + 
            '             "promBand": "A",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Mon",  '  + 
            '             "offerToken": "035b5a789ba0000ffffffffffffffff00000000043bd0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "71"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-06T08:00",  '  + 
            '             "end": "2018-02-06T12:00",  '  + 
            '             "promBand": "M",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Tue",  '  + 
            '             "offerToken": "035b5a79b51c000ffffffffffffffff00000000014c50001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "23"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-06T12:00",  '  + 
            '             "end": "2018-02-06T17:00",  '  + 
            '             "promBand": "A",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Tue",  '  + 
            '             "offerToken": "035b5a79ed20000ffffffffffffffff00000000014bb0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "25"  '  + 
            '           }  '  + 
            '         ]  '  + 
            '       },  '  + 
            '       {  '  + 
            '         "jobRequest": "PULSE VIDEO",  '  + 
            '         "ErrorFlag": "false",  '  + 
            '         "ErrorMessage": "OK",  '  + 
            '         "ReturnValue": [  '  + 
            '           {  '  + 
            '             "start": "2018-02-01T08:00",  '  + 
            '             "end": "2018-02-01T12:00",  '  + 
            '             "promBand": "M",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Thu",  '  + 
            '             "offerToken": "035b5a731d9c000ffffffffffffffff00000000014c50001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "23"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-01T12:00",  '  + 
            '             "end": "2018-02-01T17:00",  '  + 
            '             "promBand": "A",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Thu",  '  + 
            '             "offerToken": "035b5a7355a0000ffffffffffffffff00000000014bb0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "25"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-02T08:00",  '  + 
            '             "end": "2018-02-02T12:00",  '  + 
            '             "promBand": "M",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Fri",  '  + 
            '             "offerToken": "035b5a746f1c000ffffffffffffffff00000000014c50001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "23"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-02T12:00",  '  + 
            '             "end": "2018-02-02T17:00",  '  + 
            '             "promBand": "A",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Fri",  '  + 
            '             "offerToken": "035b5a74a720000ffffffffffffffff00000000014bb0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "25"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-03T08:00",  '  + 
            '             "end": "2018-02-03T12:00",  '  + 
            '             "promBand": "M",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Sat",  '  + 
            '             "offerToken": "035b5a75c09c000ffffffffffffffff00000000014c50001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "23"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-03T12:00",  '  + 
            '             "end": "2018-02-03T17:00",  '  + 
            '             "promBand": "A",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Sat",  '  + 
            '             "offerToken": "035b5a75f8a0000ffffffffffffffff00000000014bb0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "25"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-05T08:00",  '  + 
            '             "end": "2018-02-05T12:00",  '  + 
            '             "promBand": "M",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Mon",  '  + 
            '             "offerToken": "035b5a78639c000ffffffffffffffff000000000428e0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "45"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-05T12:00",  '  + 
            '             "end": "2018-02-05T17:00",  '  + 
            '             "promBand": "A",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Mon",  '  + 
            '             "offerToken": "035b5a789ba0000ffffffffffffffff00000000043bd0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "71"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-06T08:00",  '  + 
            '             "end": "2018-02-06T12:00",  '  + 
            '             "promBand": "M",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Tue",  '  + 
            '             "offerToken": "035b5a79b51c000ffffffffffffffff00000000014c50001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "23"  '  + 
            '           },  '  + 
            '           {  '  + 
            '             "start": "2018-02-06T12:00",  '  + 
            '             "end": "2018-02-06T17:00",  '  + 
            '             "promBand": "A",  '  + 
            '             "promType": "P",  '  + 
            '             "day": "Tue",  '  + 
            '             "offerToken": "035b5a79ed20000ffffffffffffffff00000000014bb0001005",  '  + 
            '             "offerText": "RW02O023",  '  + 
            '             "travelTime": "25"  '  + 
            '           }  '  + 
            '         ]  '  + 
            '       }  '  + 
            '     ]  '  + 
            '  }  ' ; 
      
        
         res.setBody(xmlBodyStr);
         res.setStatusCode(200);
         return res;
     }
}