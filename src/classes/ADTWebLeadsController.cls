/************************************* MODIFICATION LOG ********************************************************************************************
 * ADTWebLeadController - supports ADTWebleadAPI & ADTWebLeadReprocessBatch classes
 *
 * DESCRIPTION : Contains methods for contact type & lead type of webleads
 *               
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                        DATE              TICKET            REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Parameswaran Iyer              08/06/2018          HRM-5709        - Original Version
 * Siddarth Asokan                08/20/2018          HRM-5709        - Lead flow
 * Srini                          12/03/2018          HRM-8688        - Webleads Telemar Next Up Number : Lock Error
 * Siju Varghese                  01/04/2019          HRM-8851        - Canada Leads in Telemar are with 3 digit postal code
 * Siju Varghese                  01/04/2019          HRM-8906        - Missing FPL Account Number and FPL Agent fields 
 * Jitendra Kothari               02/21/2019          HRM-8517        - Modify Duplicate Web Lead Criteria in SFDC
 * Viraj Shah                     06/11/2019          SMS Marketing   - CALL EWC
 * Jitendra Kothari				  10/10/2019		  HRM-10967		  - Configurator Cheetahmail Requirement
 * 
 */

global with sharing class ADTWebLeadsController{
    /*
     * Method name : reprocessRecords
     * Controller to instantiate a batch process
     */
    webService static void reprocessRecords() {
        //Database.executeBatch(new ADTWebLeadsReprocessBatch());
    }
    
    public map<String,Id> filterComboMap = new map<String,Id>();
    /*
     * Method name : sendMassEmail
     * Parameters : Map<Id,String> auditLogIdToEmailMap
     * Return Type: Messaging.SendEmailResult[]
     * Reprocess flow for Contact type
     */
    public static Messaging.SendEmailResult[] sendMassEmail(List<Id> auditLogIdList){
        EmailTemplate emailTemp = [SELECT id FROM EmailTemplate WHERE developerName = 'Webleads_contact_notification'];
        List<Messaging.Email> emailMessagingList=new List<Messaging.Email>();
          OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'salesforcesupport@adt.com'];
        for(Integer i=0;i<auditLogIdList.size();i++){
           
                Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(emailTemp.Id, Userinfo.getUserId(), auditLogIdList[i]);
                mail.setToAddresses(new List<string>{'syarramsetti@adt.com'});
                if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);    
                }
                mail.setSaveAsActivity(false);
                emailMessagingList.add(mail);
            
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(emailMessagingList);

        return results;
    }
    
    /*
     * Method name : doContactProcessing
     * Parameters : Id auditLogId,List<string> useremail, string emailSubject
     * Return Type: void
     * Send email to the contact processed
     */
    public void doContactProcessing(Id auditLogId){
        try{
            AuditLog__c auditLogObj =[select id,status__c,FormName__c,Email__c from AuditLog__c where id=:auditLogId FOR UPDATE];
            //Query and create a map for custom metadata
            Map<String, WebleadsContactEmailRouting__mdt> contaEmailMap = new Map<String, WebleadsContactEmailRouting__mdt>();
            for(WebleadsContactEmailRouting__mdt wle:[SELECT label, EmailTo__c, EmailFrom__c, EmailTemplate__c FROM WebleadsContactEmailRouting__mdt WHERE IsActive__c = true]){
                contaEmailMap.put(wle.label,wle);
            }
            //Send to default if no matching form is found in custom metadata
            WebleadsContactEmailRouting__mdt selMetadataRec = new WebleadsContactEmailRouting__mdt();
            if(contaEmailMap.containskey(auditLogObj.FormName__c))
                selMetadataRec = contaEmailMap.get(auditLogObj.FormName__c);
            else
                selMetadataRec = contaEmailMap.get('default');
            String emailTempName = 'Webleads_contact_notification'; //Default template
            if(String.isNotBlank(selMetadataRec.EmailTemplate__c)){
                emailTempName = selMetadataRec.EmailTemplate__c;
            }
            // Query email template
            EmailTemplate emailTemp = [SELECT id FROM EmailTemplate WHERE developerName =:emailTempName];
            OrgWideEmailAddress[] owea = new list<OrgWideEmailAddress>();
            owea = [select Id from OrgWideEmailAddress where Address = 'salesforcesupport@adt.com'];
            Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(emailTemp.Id, Userinfo.getUserId(), auditLogId);
            if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);   
            }

            mail.setToAddresses(new List<string>{selMetadataRec.EmailTo__c});
            mail.setSaveAsActivity(false);
            if(String.isNotBlank(auditLogObj.Email__c))
                mail.setReplyTo(auditLogObj.Email__c);
            Messaging.SendEmailResult[] results = Messaging.sendEmail(new List<Messaging.Email>{mail});
            
            if (results[0].success) {
                auditLogObj.status__c = 'Email Sent';
            } else {
                auditLogObj.status__c = 'Email Failed';
            }   
            update auditLogObj;

        }catch(Exception e){
            ADTApplicationMonitor.log(e,'ADTWebLeadsController','doContactProcessing',ADTApplicationMonitor.CUSTOM_APP.HERMES);
        }
    }
    
    /*
     * Method name : populatePostalCodeLookup
     * Parameters : String zipCode,String lineOfBusiness
     * Return Type: Id
     * Returns the postal code record or creates a new one if missing
     */

    public Id populatePostalCodeLookup(String zipCode, String lineOfBusiness){
        Id postalCodeId;
        for(Postal_Codes__c postalCodeObj : [select id from Postal_Codes__c where Name=:zipCode and BusinessID__c=:lineOfBusiness limit 1]){
            postalCodeId = postalCodeObj.id;
            break;
        }
        /* Create postal code if missing
        if(postalCodeId == null){
            Postal_Codes__c postalCodeObj = new Postal_Codes__c();
            postalCodeObj.name = zipCode;
            postalCodeObj.BusinessID__c = lineOfBusiness;
            postalCodeObj.Town__c = lineOfBusiness == '1200'?'Out Of Market - BUSI':'Out Of Market -RESI';
            insert postalCodeObj;
            postalCodeId = postalCodeObj.id;
        }*/
        return postalCodeId;
    }
    
    /*
     * Method name : findMatchingAccount
     * Parameters : AuditLog__c
     * Return Type: Id (Account Id)
     * Find matching account & if no account is found then create a new account
     */
    public Account findMatchingAccount(AuditLog__c aLog){
        list<list<Account>> searchResults =  [FIND :aLog.PrimaryPhone__c RETURNING Account (id, Name, FirstName__c, LastName__c, Email__c, SiteState__c, PostalCodeID__c, AddressId__c,Business_Id__c,CreatedDate ORDER BY CreatedDate DESC)];
        Account partialMatchAccount;
        Account[] accntlist = (Account[])searchResults[0];
        for(Account a:accntlist){
            //Name, postalcode valid
            /******Jitendra Added  -HRM-8517*****************/
            if(aLog.LineOfBusiness__c == a.Business_Id__c  && (aLog.PostalCodeId__c == null || (aLog.PostalCodeId__c !=null && aLog.PostalCodeId__c == a.PostalCodeID__c))
              && (alog.LastName__c == a.LastName__c && (String.isBlank(aLog.BusinessName__c) || aLog.BusinessName__c == a.Name) && aLog.Email__c == a.Email__c)){
               return a;
            }
            else if(aLog.LineOfBusiness__c == a.Business_Id__c  && (aLog.PostalCodeId__c == null || (aLog.PostalCodeId__c !=null && aLog.PostalCodeId__c == a.PostalCodeID__c))
              && (alog.LastName__c == a.LastName__c || aLog.BusinessName__c == a.Name || aLog.Email__c == a.Email__c) && partialMatchAccount == null){
                partialMatchAccount = new Account();
                partialMatchAccount = a;
            }
            /****** Jitendra Added End -HRM-8517*************/
        }
        if(partialMatchAccount != null){
            return partialMatchAccount;
        }
        //Method doesnot return so execute below logic.
        try{
            Account a = createNewAccount(aLog);
            Utilities.setAccountLeadSource(a, null);
            insert a;
            TelemarDecommission__c tdSetting = TelemarDecommission__c.getInstance('WebLeads');
            if(tdSetting != null && String.isNotBlank(tdSetting.value__c) && tdSetting.value__c.equalsIgnoreCase('FALSE')){
                User u = [Select Business_Unit__c, EmployeeNumber__c from User where ID = :Userinfo.getUserId()];
                OutgoingAppointmentMessage om = new OutgoingAppointmentMessage();
                TelemarGateway.ScheduleResult result = new TelemarGateway.ScheduleResult();
                RequestQueue__c rq = new RequestQueue__c();
                Event e = new Event();
                e.StartDateTime = DateTime.now();
                e.EndDateTime = DateTime.now().addMinutes(30);
                e.OwnerId = UserInfo.getUserId();
                e.TaskCode__c = '';
                om.e = e;
                om.Dnis = aLog.PromotionCode__c;
                //om.Promotion = ar.Promotion;
                om.TransactionType = IntegrationConstants.TRANSACTION_NEW;
                om.TransactionType += '-NSC';
                list<Account> newccnlist = [SELECT FirstName__c, LastName__c, Name, Business_Id__c, SiteStreet__c, SiteValidated__c, SiteStreet2__c, SiteCity__c, SiteState__c, 
                                            TelemarAccountNumber__c,SitePostalCode__c, SitePostalCodeAddOn__c, Data_Source__c, SiteCountryCode__c, Phone, Email__c, QueriedSource__c, 
                                            ReferredBy__c,Channel__c,OutboundTelemarAccountNumber__c,Affiliation__c, DNIS__c,CompletePostalCodeWeblead__c,ShippingCountry
                          FROM Account WHERE Id =: a.id];
                om.a = newccnlist[0];
                rq = TelemarGateway.createRequest(om, IntegrationConstants.REQUEST_STATUS_ASYNCREADY, e, newccnlist[0]);
                insert rq;
                if(!Test.isRunningTest()){
                    TelemarGateway.futureRequest(rq.id);
                }
            }

            return a;
        }catch(exception e){
            ADTApplicationMonitor.log(e,'ADTWebLeadsController','CreateAccount',ADTApplicationMonitor.CUSTOM_APP.HERMES);
            return null;
        }
    }
    
    //Create a new account sobject - No dml
    public Account createNewAccount(AuditLog__c aLog){
            Account a = new Account();
            // Set Name
            if(String.isNotBlank(aLog.FirstName__c) && String.isNotBlank(aLog.LastName__c)){
                a.FirstName__c = aLog.FirstName__c;
                a.LastName__c = aLog.LastName__c;
            }else{
                a.LastName__c = aLog.FirstName__c;
            }
            if(String.isNotBlank(aLog.BusinessName__c))
                a.Name = aLog.BusinessName__c;
            else if(String.isNotBlank(a.FirstName__c) && String.isNotBlank(a.LastName__c))
                a.Name = a.FirstName__c + ' ' + a.LastName__c;
            else
                a.Name = a.LastName__c;
            // LOB & Channel population
            a.Business_Id__c = aLog.LineOfBusiness__c;
            a.Channel__c = aLog.Channel__c;
            // Phone, email & postal code population
            a.Phone = aLog.PrimaryPhone__c;
            if(String.isNotBlank(aLog.SecondaryPhone__c))
                a.PhoneNumber2__c = aLog.SecondaryPhone__c;
            a.email__c = aLog.Email__c;
            a.PostalCodeId__c = aLog.PostalCodeID__c;
            a.ShippingStreet = aLog.StreetAddress1__c;
            a.Shippingcity = aLog.City__c;
            a.ShippingPostalCode = aLog.ZipCode__c;
            a.ShippingCountry = aLog.CountryCode__c;
            a.ShippingState = aLog.StateCode__c;
            // Assign Equifax Defaults
            a.EquifaxApprovalType__c = Equifax__c.getinstance('Default Condition Code').value__c;
            a.EquifaxRiskGrade__c = Equifax__c.getinstance('Default Risk Grade').value__c;
            //Siju Added  - HRM-8851
            if(String.isNotBlank(aLog.ZipCode__c)){
                a.CompletePostalCodeWeblead__c = String.isNotBlank(aLog.PostalCodeAddOn__c)?aLog.ZipCode__c+aLog.PostalCodeAddOn__c: aLog.ZipCode__c;
            }
              
            //Siju Added HRM-8906        - Missing FPL Account Number and FPL Agent fields
            if(String.isNotBlank(aLog.Request_Custom_1__c) && aLog.Request_Custom_1__c.contains('Agent ID:')){
                a.FPLAgentID__c = aLog.Request_Custom_1__c.replace('Agent ID: ', '');
            }
            if(String.isNotBlank(aLog.Request_Custom_2__c) && aLog.Request_Custom_2__c.contains('Customer Number:')){
                a.FPLCustomerNumber__c = aLog.Request_Custom_2__c.replace('Customer Number: ','');
            }
             
            // Extra assignments
            a.Data_Source__c = 'WEB';
            a.ProcessingType__c = 'NSC';
            if(String.isNotBlank(aLog.PromotionCode__c)){
                a.DNIS__c = aLog.PromotionCode__c;
                a.DNIS_Modified_Date__c = system.today();
                a.DNIS_Modifier_Name__c = Userinfo.getUserId();
            }
            //HRM-8688 Webleads Telemar Next Up Number : Lock Error
           a.TelemarAccountNumber__c = [SELECT TelemarAccountNumber__c FROM AuditLog__c WHERE id =:alog.Id].TelemarAccountNumber__c;
            return a;
    }
     /*
     * Method name : doLeadProcessing
     * Parameters : AuditLog__c
     * Return Type: void
     * Find matching account and create call Data record
     */
    public void doLeadProcessing(AuditLog__c aLog){
        system.debug('Do lead processing method: '+aLog.Account__c);
        if(String.isNotBlank(aLog.Account__c)){
            try{
                // Call EBR
                ADTEBRAPI.sendToEBR(aLog.Account__c,aLog.Id);
                // Call to Cheetah mail
                //Start HRM-10967
                if(String.isBlank(aLog.Request_Custom_13__c)){
                	ADTCheetahMailAPI cheetahMailAPI = new ADTCheetahMailAPI();
                	cheetahMailAPI.sendToACKEmail(aLog);
                }
                //End HRM-10967
                // Call EWC
                ADTEWCConsentAPI.ServiceTransactionType = 'sendEWCRequest';//SMS Marketing
                ADTEWCConsentAPI.sendToEWC(aLog.Account__c,aLog.Id);//SMS Marketing
            } catch(exception e){
              System.debug('Lead processing failed'+e);  
            }   
        }
        
    }
    //Start HRM-10967
    public static void doWebInterestProcessing(AuditLog__c aLog){
    	/*list<AuditLog__c> auditLogList = [SELECT id, Request_Custom_15__c, Request_Custom_14__c, Email__c FROM AuditLog__c
                                                   WHERE Id =:auditId];
        if(!auditLogList.isEmpty()){*/
            try{
	            //ADTCheetahMailSchema.sendCheetahEmail(auditLogList.get(0), email);
	            RequestQueue__c rqToInsert = ADTCheetahMailSchema.sendCheetahEmail(aLog.Email__c, aLog.Request_Custom_15__c);
				
		        rqToInsert.AuditLog__c = aLog.Id;
		        insert rqToInsert;
            } catch(exception e){
              ADTApplicationMonitor.log(e, 'ADTWebLeadsController', 'doWebInterestProcessing', ADTApplicationMonitor.CUSTOM_APP.HERMES);
            }   
        //}
        
    }
    //End HRM-10967
    /*
     * Method name : doLeadProcessing
     * Parameters : List<Id> auditLogIdList
     * Return Type: void
     * Reprocess flow for Lead type
     */
    
    public void doLeadProcessing(Set<Id> auditLogIdSet){
        list<AuditLog__c> auditLogList = new list<AuditLog__c>();
        auditLogList = queryAuditLogs(auditLogIdSet);
        Map<Id, Id> auditlogAccntMap = new Map<Id, Id>();
        list<AuditLog__c> auditlogToUpdate = new list<AuditLog__c>();
        Map<Id, Account> accntToInsert = new Map<Id, Account>();
        if(auditLogList.size()>0){
            list<AuditLog__c> logsWithAddr = new list<AuditLog__c>();
            list<AuditLog__c> logsWithoutAddr = new list<AuditLog__c>();
            set<Id> addrIdSet = new set<Id>();
            set<Id> postalIdSet = new set<Id>();
            for(AuditLog__c aLog :auditLogList){
                // Audit Logs that passed form validation
                postalIdSet.add(aLog.PostalCodeId__c);
                // Create a filter map with postal code, state, email, phone, line of biz  & log ID
                filterComboMap.put(String.ValueOf(aLog.PostalCodeId__c+aLog.StateCode__c+aLog.Email__c+aLog.PrimaryPhone__c+aLog.LineOfBusiness__c).toUpperCase(),aLog.Id);
                system.debug('##filterCombo'+filterComboMap);
                if(aLog.AddressId__c != null){
                    addrIdset.add(aLog.AddressId__c);
                    logsWithAddr.add(aLog);
                }
                else{
                    logsWithoutAddr.add(aLog);
                }
            }
            // Find matching accounts based on addressId
            if(logsWithAddr.size()>0){
                Map<Id,Id> returnedMap = new Map<Id,Id>();
                returnedMap = findAccountForAuditLogs(logsWithAddr,addrIdSet,true);
                if(returnedMap != null && returnedMap.size()>0){
                    auditlogAccntMap.putAll(returnedMap);
                }
            }
            if(logsWithoutAddr.size()>0){
                Map<Id,Id> retMap = new Map<Id,Id>();
                retMap = findAccountForAuditLogs(logsWithoutAddr,postalIdSet,false);
                if(retMap != null && retMap.size()>0){
                    auditlogAccntMap.putAll(retMap);
                }
            }
            //Logic to elimated witn and without accounts
            for(AuditLog__c alg :auditLogList){
                if(auditlogAccntMap.containsKey(alg.Id)){
                    alg.Account__c = auditlogAccntMap.get(alg.Id);
                    alg.Status__c = 'Fixed';
                    auditlogToUpdate.add(alg);
                }
                else{
                    accntToInsert.put(alg.Id, createNewAccount(alg));
                }
            }
            //Insert All the accounts
            if(accntToInsert.values().size()>0){               
                insert accntToInsert.values();
            }
            for(Id algId: accntToInsert.keyset()){
                AuditLog__c audLog = new AuditLog__c(Id = algId, Account__c = accntToInsert.get(algId).Id, Status__c = 'Fixed');
                auditlogToUpdate.add(audLog);
            }
            update auditlogToUpdate;
        }
    }
    
    // Get the matching account Id for each audit logs 
    public map<Id,Id> findAccountForAuditLogs(list<AuditLog__c> logSet,set<Id> recordIdSet,Boolean isAddressPresent){
        map<Id,Id> returnMap = new map<Id,Id>();
        List<account> accountList = new list<account>();
        String query;
        String addressquery = 'Select Id, SiteState__c,PostalCodeID__c,phone,PhoneNumber2__c,PhoneNumber3__c,PhoneNumber4__c,email__c,addressId__c,Business_Id__c from Account where AddressId__c IN ';
        String postalCodequery = 'Select Id, SiteState__c,PostalCodeID__c,phone,PhoneNumber2__c,PhoneNumber3__c,PhoneNumber4__c,email__c,addressId__c,Business_Id__c from Account where PostalCodeID__c IN ';
        if(isAddressPresent)
            query = addressquery + ':recordIdSet Limit 50000';
        else
            query = postalCodequery + ':recordIdSet Limit 50000';
        
        if(filterComboMap.size() >0){
            for(account a: database.query(query)){
                system.debug('##Account'+a);
                if(filterComboMap.get(String.valueOf(a.PostalCodeID__c+a.SiteState__c+a.email__c+a.phone+a.Business_Id__c).toUpperCase()) != null){
                    returnMap.put(filterComboMap.get(String.valueOf(a.PostalCodeID__c+a.SiteState__c+a.email__c+a.phone+a.Business_Id__c).toUpperCase()),a.Id);
                }else if(filterComboMap.get(String.valueOf(a.PostalCodeID__c+a.SiteState__c+a.email__c+a.PhoneNumber2__c+a.Business_Id__c).toUpperCase()) != null){
                    returnMap.put(filterComboMap.get(String.valueOf(a.PostalCodeID__c+a.SiteState__c+a.email__c+a.PhoneNumber2__c+a.Business_Id__c).toUpperCase()),a.Id);
                }else if(filterComboMap.get(String.valueOf(a.PostalCodeID__c+a.SiteState__c+a.email__c+a.PhoneNumber3__c+a.Business_Id__c).toUpperCase()) != null){
                    returnMap.put(filterComboMap.get(String.valueOf(a.PostalCodeID__c+a.SiteState__c+a.email__c+a.PhoneNumber3__c+a.Business_Id__c).toUpperCase()),a.Id);
                }else if(filterComboMap.get(String.valueOf(a.PostalCodeID__c+a.SiteState__c+a.email__c+a.PhoneNumber4__c+a.Business_Id__c).toUpperCase()) != null){
                    returnMap.put(filterComboMap.get(String.valueOf(a.PostalCodeID__c+a.SiteState__c+a.email__c+a.PhoneNumber4__c+a.Business_Id__c).toUpperCase()),a.Id);
                }
            }
        }
        if(returnMap.size()>0)
            return returnMap;
        return null;
    }
    
    // Query the audit logs
    public list<AuditLog__c> queryAuditLogs(set<Id> auditLogIdList){
        return [Select Id,FormName__c,LineOfBusiness__c,BusinessName__c,FirstName__c,LastName__c,Email__c,PrimaryPhone__c,SecondaryPhone__c,StreetAddress1__c,City__c,StateCode__c,
                ZipCode__c,AddressID__c,PostalCodeId__c,Account__c,CountryCode__c, PromotionCode__c, Channel__c, OneClickMessageDivision__c,Request_Custom_1__c,Request_Custom_2__c From AuditLog__c Where Id IN: auditLogIdList];
    }    
}