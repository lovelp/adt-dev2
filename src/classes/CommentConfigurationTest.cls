@isTest
public class CommentConfigurationTest {

    static testMethod void controllerConstructorTest(){
        insert new CampaignDefaults__c(SetupOwnerId=UserInfo.getOrganizationId(), name='commentLength',commentLength__c=20);
        Dialer_List_Output_Control__c dlplcont = OutboundTestDataUtility.createDLPControl();
        insert dlplcont;
        Test.StartTest();
            ApexPages.currentPage().getParameters().put('id',dlplcont.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(dlplcont);
            CommentConfiguration obj = new CommentConfiguration(sc);
        Test.stoptest();    
    }
    
    static testMethod void getCommentFieldTest(){
        Dialer_List_Output_Control__c dp = OutboundTestDataUtility.createDLPControl();
        Map<String,String> returnMapVal = CommentConfiguration.buildCommentFields();
    }
    static testMethod void getBackToPageCancelTest(){
        Dialer_List_Output_Control__c dp = OutboundTestDataUtility.createDLPControl();
        insert dp;
        string s = CommentConfiguration.backToDialer(dp.Id);
    }
    static testMethod void getBackToPageCancelNegativeTest(){
        Dialer_List_Output_Control__c dp = OutboundTestDataUtility.createDLPControl();
        insert dp;
        string s = CommentConfiguration.backToDialer(null);
    }
    static testMethod void getBackToPageSaveNegativeTest(){
        Dialer_List_Output_Control__c dp = OutboundTestDataUtility.createDLPControl();
        insert dp;
        string s = CommentConfiguration.saveToDialer(null,null,null);
    }
    static testMethod void getBackToPageSaveTest(){
        Dialer_List_Output_Control__c dp = OutboundTestDataUtility.createDLPControl();
        dp.File_Output_Record_Layout__c ='[{"name":"Date","length":"8"},{"name":"FName","length":"15"},{"name":"Lname","length":"30"},{"name":"Phone1","length":"10"},{"name":"Addr1","length":"30"},{"name":"Addr2","length":"30"},{"name":"Addr3","length":"30"},{"name":"Addr4","length":"30"},{"name":"City","length":"15"},{"name":"State","length":"2"},{"name":"Zip","length":"9"},{"name":"Promotion","length":"10"},{"name":"Account","length":"14"},{"name":"Phone2","length":"10"},{"name":"Dialednum","length":"10"},{"name":"ListName","length":"10"},{"name":"Campaign","length":"6"},{"name":"Comment1","length":"20"},{"name":"comment2","length":20},{"name":"comment3","length":20},{"name":"comment4","length":20},{"name":"comment5","length":20},{"name":"comment6","length":20}]';
        String commentData = '[{"value":"CLV_Rating__c","name":"Comment1","length":"20"},{"name":"comment2","value":"Data_Source__c","length":20},{"name":"comment3","value":"HomeValue__c","length":20},{"name":"comment4","value":"SalesAppointmentDate__c","length":20},{"name":"comment5","value":"ScheduledInstallDate__c","length":20},{"name":"comment6","value":"TMTownID__c","length":20}]';
        String jsonComment = '[{"name":"comment1","length":"20"}]';
        insert dp;
        string s = CommentConfiguration.saveToDialer(dp.Id,commentData,jsonComment);
    }
}