/************************************* MODIFICATION LOG ********************************************************************************************
*
* DESCRIPTION : EDS API WebService Retrieve
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                   TICKET              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Varun Sinha               11/29/2018                                 - Original Version
*/
@RestResource(urlMapping='/LoanApplicationRetrieve/*')

global class ADTLoanApplicationRetrieveAPI {
    @HttpPost
    global static void accessData(){
        ADTLoanApplicationSchema.LoanApplicationRetrieveRequest reqJSON = null;
        ADTLoanApplicationSchema.messageResponse resMesJSON = null;
        ADTLoanApplicationSchema resJSON = null;
        ADTLoanApplicationController fnLoanApp = new ADTLoanApplicationController();
        String jSONRequestBody = null;
        try{
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;
            jSONRequestBody = req.requestBody.toString().trim();
            //Perform Validations
            resMesJSON = fnLoanApp.performValidations(req);
            if(resMesJSON == null || resMesJSON.message.contains(FlexFiMessaging__c.getinstance('400').ErrorMessage__c)){
                prepareRESTResponse(resMesJSON,400);
                return;
            }
            
            // reqJSON = (ADTLoanApplicationSchema.LoanApplicationRetrieveRequest)JSON.deserializeStrict(jSONRequestBody,ADTLoanApplicationSchema.LoanApplicationRetrieveRequest.class);//WPR_LoanApplicationRequestJSON.parse(req.requestBody.toString().trim());
            reqJSON = reqJSONRetrieve(jSONRequestBody);
            //Wrapper Creation for response
            resJSON = fnLoanApp.retrieveLoanApplication(reqJSON,jSONRequestBody);
            if(resJSON.message == FlexFiMessaging__c.getinstance('404 - Account').ErrorMessage__c){
                resMesJSON = fnLoanApp.messageResponse(resJSON.message,FlexFiMessaging__c.getinstance('404 - Account').ErrorCode__c);
                prepareRESTResponse(resMesJSON,404);
                return;
            } 
            if(resJSON.message == FlexFiMessaging__c.getinstance('404 - SSN').ErrorMessage__c){
                resMesJSON = fnLoanApp.messageResponse(resJSON.message,FlexFiMessaging__c.getinstance('404 - SSN').ErrorCode__c);
                prepareRESTResponse(resMesJSON,404);
                return;
            } 
            if(resJSON.message.contains(FlexFiMessaging__c.getinstance('500').ErrorMessage__c)){
                resMesJSON = fnLoanApp.messageResponse(resJSON.message,FlexFiMessaging__c.getinstance('500').ErrorCode__c);
                prepareRESTResponse(resMesJSON,500);
                return;
            }
            if(resJSON.message.contains('Success')){
                prepareRESTResponse(resJSON,200);
                return; 
            }
        }catch(Exception ex){
            ADTApplicationMonitor.log(ex, 'ADTLoanApplicationRetrieveAPI', 'accessData',ADTApplicationMonitor.CUSTOM_APP.FLEXFI);   
        }
    }
    //Deserializing Retrieve
    public static ADTLoanApplicationSchema.LoanApplicationRetrieveRequest reqJSONRetrieve(string jSONRequestBody){
        ADTLoanApplicationSchema.LoanApplicationRetrieveRequest reqJSON = null;
        reqJSON = (ADTLoanApplicationSchema.LoanApplicationRetrieveRequest)JSON.deserializeStrict(jSONRequestBody,ADTLoanApplicationSchema.LoanApplicationRetrieveRequest.class);//WPR_LoanApplicationRequestJSON.parse(req.requestBody.toString().trim());
        return reqJSON;
    }
    //Serializing Wrapper
    public static RestResponse prepareRESTResponse(Object sObj, Integer statusCode){
        RestResponse res = null;
        try{
            res = RestContext.response;
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf(JSON.serialize(sObj));
            res.statusCode = statusCode;
            System.debug('The loan applications response is'+JSON.serialize(sObj));
        }catch(Exception ex){
            ADTApplicationMonitor.log(ex, 'ADTLoanApplicationRetrieveAPI', 'prepareRESTResponse',ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
        }
        return res;
    }
}