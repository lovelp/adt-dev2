/************************************* MODIFICATION LOG ********************************************************************************************
* CTNSCSoftPhoneConnectController
*
* DESCRIPTION : This class is used from CTI to fire an initial call.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    Ticket            REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera               1/6/2016                               - Original Version
* Mounika Anna          02/12/2018           HRM-7272          - Made modifications to remove the check for the DNIS Start Date and End Date
*                                                   
*/

global without sharing class CTNSCSoftPhoneConnectController {
  
  public String LeadRecordTypeId {
    get{
          RecordType rt = Utilities.getRecordType(RecordTypeName.USER_ENTERED_LEAD, 'Lead');
          return rt.Id;
    }
  }  
  
  private final static ADTApplicationMonitor.CUSTOM_APP appContext = ADTApplicationMonitor.CUSTOM_APP.MATRIX;
  
  global class callInfo {
    public String callId;
    public String dnisId;
    public String lastrecordTypeId;
    public Boolean isError;
    public String ErrorMsg;
  }
  
  public string ANI {get;set;}
  public String DNIS {get;set;}
  public String selectedChannel{get;set;}
  
  public CTNSCSoftPhoneConnectController(){}
  
  private static Call_Data__c getSFCallRecord( String jsonStr){
    Call_Data__c cData = new Call_Data__c();
    cData.Received_By__c = UserInfo.getUserId();
    ADTApplicationMonitor.log(jsonStr);
    system.debug('CTI => '+jsonStr);
    try{
      if( !utilities.isEmptyOrNull(jsonStr) ){
        system.debug('CTI => Parsing JSON...'+jsonStr);
        Map<String, Object> CTInfoMap = (Map<String, Object>)JSON.deserializeUntyped(jsonStr);
        cData.ucid__c = (String)CTInfoMap.get('ucid');
        cData.Name = UserInfo.getName()+'_'+DateTime.now().format();     
        cData.vdn__c = (String)CTInfoMap.get('vdn');
        cData.status__c = (String)CTInfoMap.get('status');
        cData.sqlerror__c = (String)CTInfoMap.get('sqlerror');
        cData.isamerror__c = (String)CTInfoMap.get('isamerror');
        cData.unixtime__c = (String)CTInfoMap.get('unixtime');
        cData.unit__c = (String)CTInfoMap.get('unit');
        cData.line__c = (String)CTInfoMap.get('line');
        cData.calldate__c = (String)CTInfoMap.get('calldate');
        cData.calltime__c = (String)CTInfoMap.get('calltime');
        cData.duration__c = (String)CTInfoMap.get('duration');
        cData.apn__c = (String)CTInfoMap.get('apn');
        cData.dialednumber__c = (String)CTInfoMap.get('dialednumber');
        cData.appclient__c = (String)CTInfoMap.get('appclient');
        cData.appname__c = (String)CTInfoMap.get('appname');
        cData.adtaccnt__c = (String)CTInfoMap.get('adtaccnt');
        cData.sitetype__c = (String)CTInfoMap.get('sitetype');
        
        // payamount
        String payamount = (String)CTInfoMap.get('payamount');
        cData.payamount__c = (!Utilities.isEmptyOrNull(payamount))? Decimal.valueOf(payamount).setScale(2):0.0;
        
        cData.transferterm__c = (String)CTInfoMap.get('transferterm');
        cData.picstatus__c = (String)CTInfoMap.get('picstatus');
        cData.identifiedby__c = (String)CTInfoMap.get('identifiedby');
        cData.callerintent__c = (String)CTInfoMap.get('callerintent');
        cData.transfertype__c = (String)CTInfoMap.get('transfertype');
        cData.transferpoint__c = (String)CTInfoMap.get('transferpoint');
        cData.lastprompt__c = (String)CTInfoMap.get('lastprompt');
        cData.emergencycontact__c = (String)CTInfoMap.get('emergencycontact');
        
        // pendingpayment
        String pendingpayment = (String)CTInfoMap.get('pendingpayment');
        cData.pendingpayment__c = (!Utilities.isEmptyOrNull(pendingpayment))? Decimal.valueOf(pendingpayment).setScale(2):0.0;
        
        // pastdueamount
        String pastdueamount = (String)CTInfoMap.get('pastdueamount');
        cData.pastdueamount__c = (!Utilities.isEmptyOrNull(pastdueamount))? Decimal.valueOf(pastdueamount).setScale(2):0.0;
        
        // totalamountdue
        String totalamountdue = (String)CTInfoMap.get('totalamountdue');
        cData.totalamountdue__c = (!Utilities.isEmptyOrNull(totalamountdue))? Decimal.valueOf(totalamountdue).setScale(2):0.0;
        
        cData.nltag__c = (String)CTInfoMap.get('nltag');
      }
      else{
        ADTApplicationMonitor.log('CTI', 'Empty call data.', true);
        system.debug('CTI ==> Empty call data.');
      }
    }
    catch(Exception err){
      ADTApplicationMonitor.log(err, 'CTNSCSoftPhoneConnectController', 'getSFCallRecord', appContext);
      system.debug('\n ERR: '+err.getMessage() +'\n'+ err.getStackTraceString());
    }
    return cData;
  }
    
  @RemoteAction
  global static String startCall(String TFN, String ANI, String selectedchannel, String CTJSON) {
      system.debug( '@@ ==> TFN='+TFN+' ANI='+ANI+' selectedchannel='+selectedchannel+' CTJSON='+CTJSON );
      callInfo caller = new callInfo();
      caller.isError = false;
      Call_Data__c cData = getSFCallRecord(CTJSON);
      DNIS__c DNISobj;

        try{
            String regexNumeric = '[^a-zA-Z0-9]';
            String srchStr = TFN.replaceAll(regexNumeric, '').trim();
            
            // remove country code from TFN parameter which is actually a APN/VDN number
            if( !String.isBlank(srchStr) && srchStr.length() > 10){
              srchStr = srchStr.substring( 1,  srchStr.length());
            }
            
            // Include VDN in case of sent to us on this search for the DNIS mapping table
            String srchVDNStr; 
            if( !String.isBlank( cData.vdn__c ) ){
            srchVDNStr = cData.vdn__c.replaceAll(regexNumeric, '').trim();
            }
            else if ( !String.isBlank( cData.apn__c ) ) {
              // if no VDN default to whatever APN was sent to us on the TFN parameter
              srchVDNStr = cData.apn__c.replaceAll(regexNumeric, '').trim();
            }
            else {
              srchVDNStr = srchStr; 
            }
            
            
            Date tDate = Date.today();
            cData.Dial__c = srchStr;
            cData.selected_channel__c = selectedchannel;
            if( ANI != null ) {
                cData.ani__c=ANI;
            }
            
            // Atempt to find DNIS using the APN/VDN mapping with a line of business set from CTI (blank values are considered data restrictions to no LOB on DNIS)
            //HRM-7272 --- Removed the start and end date check for the Query
            List<DNIS__c> dnisList =[SELECT Start_Date__c, End_Date__c, LineOfBusiness__c FROM DNIS__c WHERE ID IN (Select TFN__c From APNVDN__c Where Name = :srchStr Or Name = :srchVDNStr ) AND lineofbusiness__c=:selectedchannel ];
            
            if( dnisList!=null && !dnisList.isEmpty() ){
                // DNIS found populate required info if any
                DNISobj = dnisList[0];                
                cData.DNIS__c = DNISobj.Id;
            }
            else {
              // Atempt to find DNIS using the APN/VDN mapping WITHOUT a line of business
                //if( utilities.isEmptyOrNull(selectedchannel) ){
                //HRM-7272 --- Removed the start and end date check for the Query
                dnisList=[SELECT Start_Date__c, End_Date__c, LineOfBusiness__c FROM DNIS__c WHERE ID IN (Select TFN__c From APNVDN__c Where Name = :srchStr Or Name = :srchVDNStr) order by lineofbusiness__c];
                //}
                if(dnisList!=null && !dnisList.isEmpty()){
                    DNISobj = dnisList[0];
                    // default to a resi DNIS if any found on data   
                    for( DNIS__c tDNIS: dnisList ){
                      if( !String.isBlank( tDNIS.LineOfBusiness__c ) && tDNIS.LineOfBusiness__c.equalsIgnoreCase('residential') ){
                        DNISobj = tDNIS;
                        break;
                      }
                    }             
                    cData.DNIS__c = DNISobj.Id;
                }else{
                    //caller.isError = true;
                    //errMsg = 'This number could not be located.';
                    //caller.ErrorMsg = 'The TFN supplied does not match any active TFN/VDN configured in the system.  To continue without a TFN/VDN selected, click OK.  Otherwise click Cancel.';   
                }
            }
            
            // Save our call tracking info
            insert cData;
            
            caller.callId = cData.Id;
            if( cData.DNIS__c != null ){
              caller.dnisId = cData.DNIS__c;
            }
        }
        catch (Exception err){
            caller.isError = true;
            caller.ErrorMsg  = 'error in controller'; //err.getMessage();
        ADTApplicationMonitor.log(err, 'CTNSCSoftPhoneConnectController', 'startCall', appContext);
        system.debug('\n ERR: '+err.getMessage() +'\n'+ err.getStackTraceString());        
        }
    
        RecordType rt = Utilities.getRecordType('User Entered Lead', 'Lead');
        caller.lastrecordTypeId = rt.Id;
       return JSON.serialize(caller);
  }
  

}