/************************************* MODIFICATION LOG ********************************************************************************************
* DistanceMap
*
* DESCRIPTION : Constructed from a list of accounts or leads and a reference point.
*               Capable of producing an list of those items in order of ascending distance from the reference point.
*               Used by the Locator and Cloverleaf tools
*               
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            1/23/2012           - original version
*                                                   
*/

public with sharing class DistanceMap
{   
    public Map<Decimal, List<MapItem>> distLocationMap = new Map<Decimal, List<MapItem>>();
    
    public DistanceMap(List<Account> accountsList, List<Lead> leadList, Decimal distance, MapItem startingPoint)
    {
        if(accountsList != null && !accountsList.isEmpty())
        {
            for(Account a : accountsList)
            {
                // calculate this account's distance from the starting point    
                Decimal distanceFromStartingPoint = MapUtilities.getDistance(startingPoint.rLat, startingPoint.rLon, a.Latitude__c, a.Longitude__c);
                List<MapItem> tempMapItemList;
                // since a straightline distance from the starting point forms a circle not a box, 
                // make sure the distance truly is in the circle
                
                if(distanceFromStartingPoint <= distance)
                {                   
                    MapItem item = new MapItem(a, distanceFromStartingPoint);

                    if(item.mRecordCategory != null)    
                    {
	                    tempMapItemList = distLocationMap.get(distanceFromStartingPoint);
	                    if(tempMapItemList == null)
	                    {
	                        tempMapItemList = new List<MapItem>();
	                    }    
	                    tempMapItemList.add(item);                     
	                    distLocationMap.put(distanceFromStartingPoint, tempMapItemList);
	                }    
                }           
            }
        }   
        
        if(leadList != null && !leadList.isEmpty())
        {
            for(Lead l : leadList)
            {
                // calculate this account's distance from the starting point    
                Decimal distanceFromStartingPoint = MapUtilities.getDistance(startingPoint.rLat, startingPoint.rLon, l.Latitude__c, l.Longitude__c);
                List<MapItem> tempMapItemList;
                // since a straightline distance from the starting point forms a circle not a box, 
                // make sure the distance truly is in the circle
                if(distanceFromStartingPoint <= distance)
                {                   
                    MapItem item = new MapItem(l, distanceFromStartingPoint);
                    tempMapItemList = distLocationMap.get(distanceFromStartingPoint);
                    if(tempMapItemList == null)
                    {
                        tempMapItemList = new List<MapItem>();
                    }        
                    tempMapItemList.add(item);                      
                    distLocationMap.put(distanceFromStartingPoint, tempMapItemList);                                                        
                }           
            }
        }           
    }   
    
    public List<MapItem> getProspectsList(Decimal MaxItems, Decimal oosPercent, Decimal rifPercent)
    {   
        List<MapItem> mapItemList = new List<MapItem>();            
        List<Decimal> distancesList = new List<Decimal>();
        
        distancesList.addAll(distLocationMap.keySet());     
        distancesList.sort();     
        
        Integer totalSoFar = 0;
        Integer oosThresholdTotal = Math.round(MaxItems * oosPercent * .01);
        Integer oosTotalSoFar = 0;
        Integer rifThresholdTotal = Math.round(MaxItems * rifPercent * .01);
        Integer rifTotalSoFar = 0;
        // iterate through the key set              
        for (Decimal key: distancesList) 
        {
            // check how many accounts are already in the list to be returned 
            if (totalSoFar >= Math.round(MaxItems)) 
            {
                // Drop out of the loop once have the MaxItems closest leads
                break;
            }
            
            // if less than MaxItems leads so far, get the list of MapItems from the map
            List<MapItem> tempMapItemList = distLocationMap.get(key);
            
            for (MapItem mi: tempMapItemList) {
            	if (mi.isOutOfService()) {
            		if (oosTotalSoFar < oosThresholdTotal) {
            			mapItemList.add(mi);
            			totalSoFar++;
            			oosTotalSoFar++;
            		}	
            	} else if (mi.isRevenueInForce()) {
            		if (rifTotalSoFar < rifThresholdTotal) {
            			mapItemList.add(mi);
            			totalSoFar++;
            			rifTotalSoFar++;
            		}	
            	} else {
            		mapItemList.add(mi);
            		totalSoFar++;
            	}
            }                 
        }

        return  mapItemList;    
    }
    
}