/**
 Description- This test class used for EventObserverSchedulable.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class EventObserverSchedulableTest{

        
    static testMethod void executeMethodTest() {
        
        EventObserverSchedulable es= new EventObserverSchedulable ();
        String chron = '0 0 23 * * ?';
        
        Test.startTest();
        System.schedule('Test EventObserver Schedule', chron, es);
        Test.stopTest();
        system.assert(true); 
    }
}