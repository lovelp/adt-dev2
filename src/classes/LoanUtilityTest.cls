/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class LoanUtilityTest {
    
    static testMethod void myUnitTest() {
        User u =  [Select Id from User where Id = :UserInfo.getUserId()];
        u.IsActive= true;
        update u;
        
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        insert rgvList;
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        Account acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'Unit Test Account 2';
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';
        acct.Email__c = 'abc@gmail.com';
        acct.MMBSiteNumber__c='MMBSTNumber';
        acct.MMBCustomerNumber__c='MMBCusTestNum';
        acct.TelemarAccountNumber__c = 'TAN';
        acct.DOB_encrypted__c='11/11/1973';
        acct.Income__c = '1000';
        acct.Phone = '14785894';
        insert acct;
        
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.Name = '32257';
        insert pc; 
        
        Account a1 = TestHelperClass.createAccountData();
        a1.FirstName__c = 'Manoja1';
        a1.LastName__c = 'duvuri';
        a1.Phone = '904-999-1234';
        a1.email__c = 'goodcust1@adt.com';
        a1.AddressID__c = addr.Id;
        a1.Partner_ID__c = 'Partner001';
        a1.Partner__c = 'test Partner';
        a1.LeadSourceNo__c = '1222';
        //a1.DOB_encrypted__c = '10/10/2016';
        a1.PhoneNumber2__c = '00087788889';
        a1.MMBCustomerNumber__c = '781821';
        a1.MMBSiteNumber__c = '1111';
        a1.MMBOrderType__c = 'Test Order Type';
        a1.DispositionComments__c = 'test';
        a1.OwnershipProfileCode__c = '5555';
        a1.Profile_BuildingType__c = 'CONDO';
        a1.Profile_YearsInResidence__c = '8';
        a1.CPQIntegrationData__c = 'test Integration Data';
        a1.Equifax_Last_Check_DateTime__c = Date.today();
        a1.EquifaxRiskGrade__c = 'A';
        a1.EquifaxApprovalType__c = 'Test';
        a1.PostalCodeID__c = pc.id;
        a1.MMB_Past_Due_Balance__c = 16.0; 
        a1.Rep_User__c = u.Id; 
        update a1;
        
        Opportunity opp = New Opportunity();
        opp.AccountId = acct.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp; 
        
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Name = 'Test Quote';
        q.Account__c = a1.Id;
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'Submitted';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        Quote_Job__c qj = new Quote_Job__c();
        qj.DLL__c = true;
        qj.ParentQuote__c = q.id;
        qj.JobNo__c = '435678123';
        insert qj;
        
        List<CustomerLoan__c> lstCustLoans = new List<CustomerLoan__c>();
        CustomerLoan__c customerLoan = new CustomerLoan__c();
        customerLoan.CustomerNumber__c = '12345678';
        customerLoan.Active__c = true;
        customerLoan.QuoteOrderNumber__c = '123456';
        lstCustLoans.add(customerLoan);
        insert lstCustLoans;
        
        LoanUtility.processCFGCustLoans('');
        
        LoanApplication__c LoanApplication = new LoanApplication__c();
        LoanApplication.Account__c = acct.id;
        LoanApplication.CFG_Loan_Status__c = 'Active';
        LoanApplication.BillingAddress__c =addr.id;
        LoanApplication.ShippingAddress__c =addr.id;
        LoanApplication.InterestRate__c = 1.0;
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c = DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.LastZootUpdatedDateTime__c = system.today();
        LoanApplication.SubmittedDateTime__c = system.today();
        LoanApplication.SSN__c='123456789';
        LoanApplication.LoanAcceptRequestDate__c = DateTime.parse('01/04/2018 11:46 AM');
        insert LoanApplication;
        
        Set<String> strSet = new Set<String>();
        strSet.add(customerLoan.CustomerNumber__c);
        Map<String, String> siteStatusMap = new Map<String, String>();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ADTTestCalloutMockBuilder(200, '{"notes": [ { "note": 195448305, "customer": 202546345, "site": 204554635, "initialAmount": 545.35, "installmentAmount": 29.99, "noInstallBills": 123456789, "lastInstallBillNo": 987654321, "totalBilledAmount": 845.10, "job": "435678123" }, { "note": 483051954, "customer": 634520254, "site": 463520455, "initialAmount": 485.35, "installmentAmount": 19.50, "noInstallBills": 678912345, "lastInstallBillNo": 432198765, "totalBilledAmount": 735, "job": "812343567" } ]}'));
        LoanUtility.queryLoans(strSet);
        LoanUtility.processCFGCustLoans('');
        LoanUtility.customerLoanRefresh('132', siteStatusMap);
        LoanUtility.getLoansForInactiveSites(acct, lstCustLoans);
        LoanUtility.getActiveLoansForAccountSite(acct);
        LoanUtility.limitOnLoanApplication(acct);
        LoanUtility.MMBLookupSiteInformation('');
        LoanUtility.customerLoanRefresh('202546345', siteStatusMap);
        Test.stopTest();
    }
    
    static testMethod void myUnitTest2() {
        User u =  [Select Id from User where Id = :UserInfo.getUserId()];
        u.IsActive= true;
        update u;
        
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        insert rgvList;
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        Account acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'Unit Test Account 2';
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';
        acct.Email__c = 'abc@gmail.com';
        acct.MMBSiteNumber__c='MMBSTNumber';
        acct.MMBCustomerNumber__c='MMBCusTestNum';
        acct.TelemarAccountNumber__c = 'TAN';
        acct.DOB_encrypted__c='11/11/1973';
        acct.Income__c = '1000';
        acct.Phone = '14785894';
        insert acct;
        
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.Name = '32257';
        insert pc; 
        
        Account a1 = TestHelperClass.createAccountData();
        a1.FirstName__c = 'Manoja1';
        a1.LastName__c = 'duvuri';
        a1.Phone = '904-999-1234';
        a1.email__c = 'goodcust1@adt.com';
        a1.AddressID__c = addr.Id;
        a1.Partner_ID__c = 'Partner001';
        a1.Partner__c = 'test Partner';
        a1.LeadSourceNo__c = '1222';
        //a1.DOB_encrypted__c = '10/10/2016';
        a1.PhoneNumber2__c = '00087788889';
        a1.MMBCustomerNumber__c = '781821';
        a1.MMBSiteNumber__c = '1111';
        a1.MMBOrderType__c = 'Test Order Type';
        a1.DispositionComments__c = 'test';
        a1.OwnershipProfileCode__c = '5555';
        a1.Profile_BuildingType__c = 'CONDO';
        a1.Profile_YearsInResidence__c = '8';
        a1.CPQIntegrationData__c = 'test Integration Data';
        a1.Equifax_Last_Check_DateTime__c = Date.today();
        a1.EquifaxRiskGrade__c = 'A';
        a1.EquifaxApprovalType__c = 'Test';
        a1.PostalCodeID__c = pc.id;
        a1.MMB_Past_Due_Balance__c = 16.0; 
        a1.Rep_User__c = u.Id; 
        update a1;
        
        Opportunity opp = New Opportunity();
        opp.AccountId = acct.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp; 
        
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Name = 'Test Quote';
        q.Account__c = a1.Id;
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'Submitted';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        Quote_Job__c qj = new Quote_Job__c();
        qj.DLL__c = true;
        qj.ParentQuote__c = q.id;
        qj.JobNo__c = '435678123';
        insert qj;
        
        List<CustomerLoan__c> lstCustLoans = new List<CustomerLoan__c>();
        CustomerLoan__c customerLoan = new CustomerLoan__c();
        customerLoan.CustomerNumber__c = '12345678';
        customerLoan.Active__c = true;
        customerLoan.QuoteOrderNumber__c = '123456';
        lstCustLoans.add(customerLoan);
        insert lstCustLoans;
        
        
        LoanApplication__c LoanApplication = new LoanApplication__c();
        LoanApplication.Account__c = acct.id;
        LoanApplication.CFG_Loan_Status__c = 'Active';
        LoanApplication.BillingAddress__c =addr.id;
        LoanApplication.ShippingAddress__c =addr.id;
        LoanApplication.InterestRate__c = 1.0;
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c = DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.LastZootUpdatedDateTime__c = system.today();
        LoanApplication.SubmittedDateTime__c = system.today();
        LoanApplication.SSN__c='123456789';
        LoanApplication.LoanAcceptRequestDate__c = DateTime.parse('01/04/2018 11:46 AM');
        insert LoanApplication;
        
        Set<String> strSet = new Set<String>();
        strSet.add(customerLoan.CustomerNumber__c);
        Map<String, String> siteStatusMap = new Map<String, String>();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ADTTestCalloutMockBuilder(200, '{"notes": [ { "note": 195448305, "customer": 202546345, "site": 204554635, "initialAmount": 545.35, "installmentAmount": 29.99, "noInstallBills": 123456789, "lastInstallBillNo": 987654321, "totalBilledAmount": 845.10, "job": "435678123" }, { "note": 483051954, "customer": 634520254, "site": 463520455, "initialAmount": 485.35, "installmentAmount": 19.50, "noInstallBills": 678912345, "lastInstallBillNo": 432198765, "totalBilledAmount": 735, "job": "812343567" } ]}'));
        LoanUtility.queryLoans(strSet);
        LoanUtility.processCFGCustLoans('');
        LoanUtility.getLoansForInactiveSites(acct, lstCustLoans);
        LoanUtility.getActiveLoansForAccountSite(acct);
        LoanUtility.limitOnLoanApplication(acct);
        LoanUtility.MMBLookupSiteInformation('');
        LoanUtility.customerLoanRefresh('202546345', siteStatusMap);
        Test.stopTest();
    }
    
    static testMethod void myUnitTest3() {
        User u =  [Select Id from User where Id = :UserInfo.getUserId()];
        u.IsActive= true;
        update u;
        
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        insert rgvList;
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        Account acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'Unit Test Account 2';
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';
        acct.Email__c = 'abc@gmail.com';
        acct.MMBSiteNumber__c='MMBSTNumber';
        acct.MMBCustomerNumber__c='202546345';
        acct.TelemarAccountNumber__c = 'TAN';
        acct.DOB_encrypted__c='11/11/1973';
        acct.Income__c = '1000';
        acct.Phone = '14785894';
        insert acct;
        
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.Name = '32257';
        insert pc; 
        
        Account a1 = TestHelperClass.createAccountData();
        a1.FirstName__c = 'Manoja1';
        a1.LastName__c = 'duvuri';
        a1.Phone = '904-999-1234';
        a1.email__c = 'goodcust1@adt.com';
        a1.AddressID__c = addr.Id;
        a1.Partner_ID__c = 'Partner001';
        a1.Partner__c = 'test Partner';
        a1.LeadSourceNo__c = '1222';
        //a1.DOB_encrypted__c = '10/10/2016';
        a1.PhoneNumber2__c = '00087788889';
        a1.MMBCustomerNumber__c = '781821';
        a1.MMBSiteNumber__c = '123456';
        a1.MMBOrderType__c = 'Test Order Type';
        a1.DispositionComments__c = 'test';
        a1.OwnershipProfileCode__c = '5555';
        a1.Profile_BuildingType__c = 'CONDO';
        a1.Profile_YearsInResidence__c = '8';
        a1.CPQIntegrationData__c = 'test Integration Data';
        a1.Equifax_Last_Check_DateTime__c = Date.today();
        a1.EquifaxRiskGrade__c = 'A';
        a1.EquifaxApprovalType__c = 'Test';
        a1.PostalCodeID__c = pc.id;
        a1.MMB_Past_Due_Balance__c = 16.0; 
        a1.Rep_User__c = u.Id; 
        update a1;
        
        Opportunity opp = New Opportunity();
        opp.AccountId = acct.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp; 
        
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Name = 'Test Quote';
        q.Account__c = a1.Id;
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'Submitted';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        Quote_Job__c qj = new Quote_Job__c();
        qj.DLL__c = true;
        qj.ParentQuote__c = q.id;
        qj.JobNo__c = '435678123';
        insert qj;
        
        List<CustomerLoan__c> lstCustLoans = new List<CustomerLoan__c>();
        CustomerLoan__c customerLoan = new CustomerLoan__c();
        customerLoan.CustomerNumber__c = '781821';
        customerLoan.Active__c = true;
        customerLoan.QuoteOrderNumber__c = '123456';
        customerLoan.SiteNumber__c = '123456';
        customerLoan.SiteStatus__c ='IN';
        lstCustLoans.add(customerLoan);
        CustomerLoan__c customerLoan1 = new CustomerLoan__c();
        customerLoan1.CustomerNumber__c = '781821';
        customerLoan1.Active__c = true;
        customerLoan1.QuoteOrderNumber__c = '123457';
        customerLoan1.SiteNumber__c = '123457';
        customerLoan1.SiteStatus__c ='IN';
        lstCustLoans.add(customerLoan1);
        CustomerLoan__c customerLoan2 = new CustomerLoan__c();
        customerLoan2.CustomerNumber__c = '781821';
        customerLoan2.Active__c = true;
        customerLoan2.QuoteOrderNumber__c = '123458';
        customerLoan2.SiteNumber__c = '123458';
        customerLoan2.SiteStatus__c ='OUT';
        lstCustLoans.add(customerLoan2);
        CustomerLoan__c customerLoan4 = new CustomerLoan__c();
        customerLoan4.CustomerNumber__c = '781821';
        customerLoan4.Active__c = true;
        customerLoan4.QuoteOrderNumber__c = '123450';
        customerLoan4.SiteNumber__c = '123450';
        customerLoan4.SiteStatus__c ='OUT';
        lstCustLoans.add(customerLoan4);
        CustomerLoan__c customerLoan3 = new CustomerLoan__c();
        customerLoan3.CustomerNumber__c = '781821';
        customerLoan3.Active__c = true;
        customerLoan3.QuoteOrderNumber__c = '123459';
        customerLoan3.SiteNumber__c = '123459';
        customerLoan3.SiteStatus__c ='OUT';
        lstCustLoans.add(customerLoan3);
        insert lstCustLoans;
        
        
        LoanApplication__c LoanApplication = new LoanApplication__c();
        LoanApplication.Account__c = acct.id;
        LoanApplication.CFG_Loan_Status__c = 'Active';
        LoanApplication.BillingAddress__c =addr.id;
        LoanApplication.ShippingAddress__c =addr.id;
        LoanApplication.InterestRate__c = 1.0;
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c = DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.LastZootUpdatedDateTime__c = system.today();
        LoanApplication.SubmittedDateTime__c = system.today();
        LoanApplication.SSN__c='123456789';
        LoanApplication.LoanAcceptRequestDate__c = DateTime.parse('01/04/2018 11:46 AM');
        insert LoanApplication;
        
        Set<String> strSet = new Set<String>();
        strSet.add(customerLoan.CustomerNumber__c);
        Map<String, String> siteStatusMap = new Map<String, String>();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ADTTestCalloutMockBuilder(300, '{"notes": [ { "note": 195448305, "customer": 202546345, "site": 204554635, "initialAmount": 545.35, "installmentAmount": 29.99, "noInstallBills": 123456789, "lastInstallBillNo": 987654321, "totalBilledAmount": 845.10 }, { "note": 483051954, "customer": 634520254, "site": 463520455, "initialAmount": 485.35, "installmentAmount": 19.50, "noInstallBills": 678912345, "lastInstallBillNo": 432198765, "totalBilledAmount": 735 } ]}'));
        LoanUtility.getLoansForInactiveSites(acct, lstCustLoans);
        LoanUtility.getActiveLoansForAccountSite(acct);
        LoanUtility.queryLoans(strSet);
        LoanUtility.processCFGCustLoans('');
        LoanUtility.limitOnLoanApplication(acct);
        LoanUtility.MMBLookupSiteInformation('');
        LoanUtility.customerLoanRefresh('202546345', siteStatusMap);
        Test.stopTest();
    }
    static testMethod void myUnitTest4() {
        User u =  [Select Id from User where Id = :UserInfo.getUserId()];
        u.IsActive= true;
        update u;
        
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        insert rgvList;
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        Account acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'Unit Test Account 2';
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';
        acct.Email__c = 'abc@gmail.com';
        acct.MMBSiteNumber__c='MMBSTNumber';
        acct.MMBCustomerNumber__c='202546345';
        acct.TelemarAccountNumber__c = 'TAN';
        acct.DOB_encrypted__c='11/11/1973';
        acct.Income__c = '1000';
        acct.Phone = '14785894';
        insert acct;
        
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.Name = '32257';
        insert pc; 
        
        Account a1 = TestHelperClass.createAccountData();
        a1.FirstName__c = 'Manoja1';
        a1.LastName__c = 'duvuri';
        a1.Phone = '904-999-1234';
        a1.email__c = 'goodcust1@adt.com';
        a1.AddressID__c = addr.Id;
        a1.Partner_ID__c = 'Partner001';
        a1.Partner__c = 'test Partner';
        a1.LeadSourceNo__c = '1222';
        //a1.DOB_encrypted__c = '10/10/2016';
        a1.PhoneNumber2__c = '00087788889';
        a1.MMBCustomerNumber__c = '781821';
        a1.MMBSiteNumber__c = '123456';
        a1.MMBOrderType__c = 'Test Order Type';
        a1.DispositionComments__c = 'test';
        a1.OwnershipProfileCode__c = '5555';
        a1.Profile_BuildingType__c = 'CONDO';
        a1.Profile_YearsInResidence__c = '8';
        a1.CPQIntegrationData__c = 'test Integration Data';
        a1.Equifax_Last_Check_DateTime__c = Date.today();
        a1.EquifaxRiskGrade__c = 'A';
        a1.EquifaxApprovalType__c = 'Test';
        a1.PostalCodeID__c = pc.id;
        a1.MMB_Past_Due_Balance__c = 16.0; 
        a1.Rep_User__c = u.Id; 
        update a1;
        
        Opportunity opp = New Opportunity();
        opp.AccountId = acct.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp; 
        
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Name = 'Test Quote';
        q.Account__c = a1.Id;
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'Submitted';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        Quote_Job__c qj = new Quote_Job__c();
        qj.DLL__c = true;
        qj.ParentQuote__c = q.id;
        qj.JobNo__c = '435678123';
        insert qj;
        
        List<CustomerLoan__c> lstCustLoans = new List<CustomerLoan__c>();
        CustomerLoan__c customerLoan = new CustomerLoan__c();
        customerLoan.CustomerNumber__c = '781821';
        customerLoan.Active__c = true;
        customerLoan.QuoteOrderNumber__c = '123456';
        customerLoan.SiteNumber__c = '123456';
        customerLoan.SiteStatus__c ='IN';
        lstCustLoans.add(customerLoan);
        CustomerLoan__c customerLoan1 = new CustomerLoan__c();
        customerLoan1.CustomerNumber__c = '781821';
        customerLoan1.Active__c = true;
        customerLoan1.QuoteOrderNumber__c = '123457';
        customerLoan1.SiteNumber__c = '123457';
        customerLoan1.SiteStatus__c ='IN';
        lstCustLoans.add(customerLoan1);
        CustomerLoan__c customerLoan2 = new CustomerLoan__c();
        customerLoan2.CustomerNumber__c = '781821';
        customerLoan2.Active__c = true;
        customerLoan2.QuoteOrderNumber__c = '123458';
        customerLoan2.SiteNumber__c = '123458';
        customerLoan2.SiteStatus__c ='OUT';
        lstCustLoans.add(customerLoan2);
        CustomerLoan__c customerLoan4 = new CustomerLoan__c();
        customerLoan4.CustomerNumber__c = '781821';
        customerLoan4.Active__c = true;
        customerLoan4.QuoteOrderNumber__c = '123450';
        customerLoan4.SiteNumber__c = '123450';
        customerLoan4.SiteStatus__c ='OUT';
        lstCustLoans.add(customerLoan4);
        CustomerLoan__c customerLoan3 = new CustomerLoan__c();
        customerLoan3.CustomerNumber__c = '781821';
        customerLoan3.Active__c = true;
        customerLoan3.QuoteOrderNumber__c = '123459';
        customerLoan3.SiteNumber__c = '123459';
        customerLoan3.SiteStatus__c ='OUT';
        lstCustLoans.add(customerLoan3);
        insert lstCustLoans;
        
        
        LoanApplication__c LoanApplication = new LoanApplication__c();
        LoanApplication.Account__c = acct.id;
        LoanApplication.CFG_Loan_Status__c = 'Active';
        LoanApplication.BillingAddress__c =addr.id;
        LoanApplication.ShippingAddress__c =addr.id;
        LoanApplication.InterestRate__c = 1.0;
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c = DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.LastZootUpdatedDateTime__c = system.today();
        LoanApplication.SubmittedDateTime__c = system.today();
        LoanApplication.SSN__c='123456789';
        LoanApplication.LoanAcceptRequestDate__c = DateTime.parse('01/04/2018 11:46 AM');
        insert LoanApplication;
        
        Set<String> strSet = new Set<String>();
        strSet.add(customerLoan.CustomerNumber__c);
        Map<String, String> siteStatusMap = new Map<String, String>();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ADTTestCalloutMockBuilder(300, '{"notes": [ { "note": 195448305, "customer": 202546345, "site": 204554635, "initialAmount": 545.35, "installmentAmount": 29.99, "noInstallBills": 123456789, "lastInstallBillNo": 987654321, "totalBilledAmount": 845.10 }, { "note": 483051954, "customer": 634520254, "site": 463520455, "initialAmount": 485.35, "installmentAmount": 19.50, "noInstallBills": 678912345, "lastInstallBillNo": 432198765, "totalBilledAmount": 735 } ]}'));
        LoanUtility.getActiveLoansForAccountSite(acct);
        LoanUtility.processCFGCustLoans('5125112');
        Test.stopTest();
    }
}