/************************************* MODIFICATION LOG ********************************************************************************************
 *
 * DESCRIPTION : AccountTriggerHelper.cls has methods that govern behaviour of inserted and updated Account records
 *
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                  TICKET               REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Siju Varghese                 12/13/2018                                 - Original Version
 * Siddarth Asokan               12/17/2018            HRM - 8740           - Loan Application logic is moved from trigger and added the before update logic
 * Varun Sinha					 05/29/2019			   HRM - 9931           - Called method which states future call in CallLoanAPI Class
 */
 
public class LoanApplicationTriggerHelper {
    //Before Insert
    public static void ProcessRecordsBeforeInsert(List<LoanApplication__c> NewLoanApplications){
        Set<id>parentIds = new Set<id>();
        for(LoanApplication__c  loanAppl : NewLoanApplications){
            parentIds.add(loanAppl.Account__c);
        }
        Map<id,Account> acc = new Map<id,Account>([SELECT id,FirstName__c,LastName__c,DOB_encrypted__c,Email__c,Income__c FROM Account WHERE id IN :parentIds]);
        list<FlexFiConfig__c> FlexFiConfigList = new list<FlexFiConfig__c>();
        FlexFiConfigList = [SELECT Name,Value__c FROM FlexFiConfig__c];
        Map<String,String> CustomMessageMap = new Map<String,String>();
        for(FlexFiConfig__c con: FlexFiConfigList){
            CustomMessageMap.put(con.name,con.value__c);
        }
        
        for(LoanApplication__c loanApp: NewLoanApplications){
            loanApp.FirstName__c = acc.get(loanApp.Account__c).FirstName__c; 
            loanApp.LastName__c = acc.get(loanApp.Account__c).LastName__c;
            //loanApp.DOB__c = acc.get(loanApp.Account__c).DOB_encrypted__c ;
            loanApp.TilaEmail__c = acc.get(loanApp.Account__c).Email__c;
            loanApp.CFGEmail__c = acc.get(loanApp.Account__c).Email__c;
            if (!String.isBlank(acc.get(loanApp.Account__c).Income__c))
            loanApp.AnnualIncome__c = decimal.valueof(acc.get(loanApp.Account__c).Income__c);
        }
    }
    
   //Before Update    
   public static void ProcessRecordsbeforeUpdate(List<LoanApplication__c> NewLoanApplications, Map<Id, LoanApplication__c> OldLoanApplications){
        for(LoanApplication__c loanApp: NewLoanApplications){
            // Future method to call the zoot accept loan request 
            // if(loanApp.JobClosed__c)
            //if(loanApp.TilaPnoteAcceptedDateTime__c != null && loanApp.LoanAcceptRequestDate__c == null && String.isNotBlank(loanApp.ApplicationStatus__c) && loanApp.ApplicationStatus__c.equalsIgnoreCase('Application Approved')){
            System.debug('The loan app is'+loanApp);
            if( loanApp.JobClosed__c && loanApp.TilaPnoteAcceptedDateTime__c != null && loanApp.LoanAcceptRequestDate__c == null && 
                String.isNotBlank(loanApp.DecisionStatus__c) && loanApp.DecisionStatus__c.equalsIgnoreCase('APPROVED') && 
                loanApp.ZootRequestStatus__c =='Update Complete' && loanApp.ThirdPartyAllowed__c){
                loanApp.ZootRequestStatus__c ='Accept Pending';
                 CallLoanAPI.callLoanAcceptMethod(loanApp.Id);
            }
        }
    }
}