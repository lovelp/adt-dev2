/************************************* MODIFICATION LOG ********************************************************************************************
* LeadDeleteProcessor
*
* DESCRIPTION : A schedulable class that initiates the deletion of expired leads
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover            		3/12/2012			- Original Version
*
*													
*/

global class LeadDeleteProcessor implements Schedulable {
	global void execute(SchedulableContext sc) {
		Database.executebatch(new LeadDeleteProcessBatch(), 1000);
	}
}