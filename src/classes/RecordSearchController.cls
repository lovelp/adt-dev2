public without sharing class RecordSearchController{
    
    public class sObjectWrapper{
        @AuraEnabled
        public list<Account> accntlist;
        @AuraEnabled
        public list<Lead> leadList; 
    }
    
    @AuraEnabled
    public static sObjectWrapper searchRecords(String searchString, String recId){
        system.debug(searchString+recId);
        LeadSearchController.searchLeadWrapper inputWrapper = new LeadSearchController.searchLeadWrapper();
        if(String.isBlank(recId)){
            inputWrapper = (LeadSearchController.searchLeadWrapper)JSON.deserialize(searchString,LeadSearchController.searchLeadWrapper.class);
        }
        else{
            list<lead> leadList = [SELECT id, SiteStateProvince__c,Business_Id__c,FirstName,SiteCity__c, SiteStreet__c, SiteStreet2__c,LastName,SitePostalCode__c,email, phone 
                                   FROM Lead
                                   WHERE id =:recId LIMIT 1];
            LeadSearchController.searchLeadWrapper leadSearchString = new LeadSearchController.searchLeadWrapper();
            if(leadList.size()>0){
                leadSearchString.leadSearch = leadList[0];
                inputWrapper = leadSearchString;
            }
        }
        return orderRecords(inputWrapper);
    }
    public static sObjectWrapper orderRecords(LeadSearchController.searchLeadWrapper inputLead){
        String siteNumber;
        String leadName = inputLead.leadSearch.FirstName+''+inputLead.leadSearch.LastName;
        if(String.isNotBlank(inputLead.leadSearch.SiteStreet__c)){
            if(inputLead.leadSearch.SiteStreet__c.subString(0,1).isNumeric()){
                Pattern p = Pattern.compile('(^\\d+)');
                Matcher m = p.matcher(inputLead.leadSearch.SiteStreet__c);
                if(m.find()){
                    siteNumber = m.group();
                }
            }
            else {
                siteNumber = inputLead.leadSearch.SiteStreet__c.trim().split('\\s+')[0];
            }   
        }
        String QueryString = 'SELECT id,Street__c FROM Address__c WHERE Street__c LIKE \''+siteNumber+'%\' AND PostalCode__c = \''+String.escapeSingleQuotes(inputLead.leadSearch.SitePostalCode__c)+ '\'';
        list<Address__c> address = Database.query(QueryString);

        if(address.size()>0){
            set<Id> addidset = new set<Id>();
            for(Address__c add:address){
               addidset.add(add.Id);
            }
            list<Lead> leadsMatchAddress = new list<lead>();
            Map<Id, lead> tempLeadMap = new Map<Id, lead>();
            Map<Id, Integer> leadForSorting = new Map<Id, Integer>();
            for(lead l: [SELECT id, Name, Company, LeadNameBasedOnCompany__c ,LeadSource,Business_Id__c, Phone, Email, StandardizedAddress__c, DispositionCode__c,Channel__c, LastModifiedDate,
                        OwnerId,Status,Rep_Name__c,LeadManagementId__c, SiteStreet__c, SiteStreet2__c
                         FROM Lead WHERE AddressID__c IN:addidset AND isConverted = false LIMIT 50]){// and Business_Id__c =: inputLead.leadSearch.Business_Id__c]){
                             leadForSorting.put(l.id, returnLevDistance(String.valueOf(inputLead.leadSearch.Phone),String.valueOf(l.Phone))+
                                                returnLevDistance(inputLead.leadSearch.email,l.email)+
                                                returnLevDistance(leadName, l.name)+returnLevDistance(inputLead.leadSearch.SiteStreet__c,l.SiteStreet__c)+
                                                returnLevDistance(inputLead.leadSearch.SiteStreet2__c,l.SiteStreet2__c));
                             tempLeadMap.put(l.id, l);                                        
                         }
            
            for(recordSorting rs:sortedMap(leadForSorting)){
                leadsMatchAddress.add(tempLeadMap.get(rs.mapId));
            }
            
            list<Account> accountMatchAddress = new list<Account>();
            Map<Id, Account> tempAccMap = new Map<Id, Account>();
            Map<Id, Integer> accForSorting = new Map<Id, Integer>();
            for(Account a:[SELECT id, Name, Email__c, phone, StandardizedAddress__c,Rep_Name__c,LeadStatus__c,LastActivityDate__c,Channel__c,TransitionDate3__c,
                           TelemarAccountNumber__c, OwnerId, Owner.Name, Status__c, AccountStatus__c, MMBLastRunDate__c, MMBSystemNumber__c, MMBCustomerType__c,
                           MMBSiteNumber__c, MMBCustomerNumber__c, StatusFormula__c, SiteStreet__c, SiteStreet2__c
                           FROM Account 
                           WHERE AddressID__c IN:addidset LIMIT 50]){// and Business_Id__c =: inputLead.leadSearch.Business_Id__c]){
                               accForSorting.put(a.id, returnLevDistance(String.valueOf(inputLead.leadSearch.Phone),String.valueOf(a.phone))+
                                                 returnLevDistance(inputLead.leadSearch.email,a.Email__c)+
                                                 returnLevDistance(leadName, a.name)+returnLevDistance(inputLead.leadSearch.SiteStreet__c,a.SiteStreet__c)+
                                                 returnLevDistance(inputLead.leadSearch.SiteStreet2__c,a.SiteStreet2__c));                                     
                               tempAccMap.put(a.Id, a);
                           }
            system.debug('Account before sort'+tempAccMap);
            for(recordSorting rs:sortedMap(accForSorting)){
                accountMatchAddress.add(tempAccMap.get(rs.mapId));
            }
            system.debug('Account after sort'+accountMatchAddress);
            
            sObjectWrapper objWrap = new sObjectWrapper();
            objWrap.accntlist = accountMatchAddress;
            objWrap.leadList = leadsMatchAddress;
            return objWrap;
        }
        else{
            return null;
        }
        
    }
    
    
    public static void changeOwnerShipLC(Id accId, Id leadId, Id existingOwner){
        try{
           if(String.isNotBlank(leadId)){
            copyleadToAccount(accId, leadId, existingOwner);
           }
           else{
                changeOwnerShip(accId,existingOwner);
           }
        }catch(exception e){
            throw new auraexception(e.getmessage());
        }
        
    }
    
    @AuraEnabled
    public static void changeOwnerShip(Id objectId, Id existingOwner){
        try{
            if(Id.valueOf(existingOwner) != Userinfo.getUserId()){
                if(String.valueOf(objectId).startswith('001')){
                    Account acc = new Account(Id = objectId, OwnerId = Userinfo.getUserId());
                    update acc;
                }else if(String.valueOf(objectId).startswith('00Q')){
                    lead ld = new lead(Id = objectId, OwnerId = Userinfo.getUserId());
                    update ld;
                }
                /* Update all future tasks to the new owner
                list<Task> tasksToUpdate = new list<Task>();
                for(Task t: [Select WhatId,WhoId From Task Where ActivityDate >= TODAY And (WhatId =: objectId OR WhoId =:objectId) AND OwnerId !=: UserInfo.getUserId()]){
                    t.OwnerId = UserInfo.getUserId(); 
                    tasksToUpdate.add(t);
                }
                if(tasksToUpdate.size()>0){
                    update tasksToUpdate;
                }*/
                // send email notification to old owner
                sendnotification(objectId,existingOwner);
            }
            redirectPage(objectId);
        }
        catch(Exception e){
            throw new auraexception(e.getmessage());
        }
    }
    public static pageReference redirectPage(Id objectId){
        if(objectId != null){
            PageReference retURL;
            if(String.valueOf(objectId).startswith('001')){
                retURL = new PageReference('/apex/MMBCustomerSiteLookup?objId='+objectId);
                aura.redirect(retURL);
            }else{
                retURL = new PageReference('/'+objectId);
                aura.redirect(retURL);
            }
        }
        return null;
    }
    
    private static integer returnLevDistance(String queryString, String inputString){
        if(String.isBlank(queryString)){
            queryString = '';
            return 1;
        }
        if(String.isBlank(inputString)){
            inputString = '';
            return 1;
        }
        return inputString.getLevenshteinDistance(queryString);
        
    }
    
    public static list<recordSorting> sortedMap(Map<Id, Integer> toSortMap){
        system.debug('Before'+toSortMap);
        list<recordSorting> recSortList = new list<recordSorting>();
        for(Id key: toSortMap.keyset()) {
            recSortList.add(new recordSorting(key, toSortMap.get(key)));
        }
        recSortList.sort();
        return recSortList;
        
    }
    
    public class recordSorting implements comparable{
        public Id mapId;
        public Integer levDist;
        public recordSorting(Id mapId, Integer levDist) {
            this.mapId = mapId;
            this.levDist = levDist;
        }
        public Integer compareTo(Object other) {
            recordSorting obj = (recordSorting)other;
            if(obj.levDist < levDist){
                return 1;
            }
            else if(obj.levDist > levDist){
                return -1;
            }
            return 0;
        }
    }
    //Email Notofication
    public Static PageReference sendnotification(Id objectId,Id userId){
        System.debug(objectId+' '+userId);
        list<User> currentOwnerDetails = new list<user>();
        //list<User> previousOwnerDetails = new list<user>();
        list<account> accList = new list<sObject>();
        list<lead> leadlist = new list<lead>();
        string body = '';
        string repName = '';
        if(userId != null && userId != UserInfo.getUserId()){
            currentOwnerDetails = [SELECT Id, Name, Email FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
            if(objectId != null && currentOwnerDetails[0].name != 'Global Admin'){
                if(String.valueOf(objectId).startswith('001')){
                    accList = [SELECT Id,Name,Rep_name__c,StandardizedAddress__c,Business_Id__c,Channel__c,TelemarAccountNumber__c,Status__c,LastActivityDate__c,Owner.Name from account where id =:objectId LIMIT 1];
                    if(accList[0].Owner.Name != null){
                        repName = accList[0].Owner.Name;
                        body = 'The account with the following details has been assigned to <b>'+currentOwnerDetails[0].name + '</b><br/><br/>';
                        body += 'Name: '+accList[0].Name+ '<br/>';
                        body += 'Address: '+accList[0].StandardizedAddress__c+ '<br/>';
                        body += 'Business Id: '+accList[0].Business_Id__c+ '<br/>';
                        body += 'Channel: '+accList[0].Channel__c+ '<br/>';
                        body += 'Telemar Account Number: '+accList[0].TelemarAccountNumber__c+ '<br/>';
                        body += 'Status: '+accList[0].Status__c+ '<br/>';
                        body += 'Last Activity Date: '+accList[0].LastActivityDate__c;
                    }
                }
                else if(String.valueOf(objectId).startswith('00Q')){
                    /*leadlist = [SELECT id,name,rep_name__c from lead where id =:objectId LIMIT 1];
                    if(leadlist[0].rep_name__c != null){
                        repName = leadlist[0].rep_name__c;
                        body = 'Dear User,Lead ownership is changed to '+currentOwnerDetails[0].name;
                    }*/
                    leadlist = [SELECT Id,Name,Company,Rep_name__c,StandardizedAddress__c,Business_Id__c,Channel__c,LeadManagementId__c,Status,LastModifiedDate, Owner.Name
                                FROM lead WHERE Id =:objectId LIMIT 1];
                    if(leadlist[0].Owner.Name != null){
                        repName = leadlist[0].Owner.Name;
                        body = 'The lead with the following details has been assigned to <b>'+currentOwnerDetails[0].name + '</b><br/><br/>';
                        body += 'Name: '+leadlist[0].Name+ '<br/>';
                        body += 'Company: '+leadlist[0].Company+ '<br/>';
                        body += 'Address: '+leadlist[0].StandardizedAddress__c+ '<br/>';
                        body += 'Business Id: '+leadlist[0].Business_Id__c+ '<br/>';
                        body += 'Channel: '+leadlist[0].Channel__c+ '<br/>';
                        body += 'Lead Management Id: '+leadlist[0].LeadManagementId__c+ '<br/>';
                        body += 'Status: '+leadlist[0].Status+ '<br/>';
                        body += 'Last Modified Date: '+leadlist[0].LastModifiedDate;
                    }
                }
                if(repName!=null){
                    String cName = currentOwnerDetails[0].name;
                    List< Messaging.SingleEmailMessage> emails = new List< Messaging.SingleEmailMessage>();
                    //String[] toAddresses = new String[] {previousOwnerDetails[0].email};
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setSenderDisplayName(cName);
                    mail.setSubject('Owner change notification');
                    //mail.setToAddresses(toAddresses);
                    mail.setTargetObjectId(userId);
                    mail.setSaveAsActivity(false);
                    mail.setHtmlBody(body);
                    emails.add(mail);
                    Messaging.sendEmail(emails);
                    
                    System.debug('Email sent');
                }
            }
        }
        return null;
    }
    
    private static void copyleadToAccount(Id accId, Id leadId, Id exisUser){
        list<Lead> ld = [SELECT id, TelemarLeadSource__c, Name FROM Lead WHERE id =: leadId];
        Account acc = new Account();
        if(ld.size()>0){
            acc.Id = accId;
            acc.TelemarLeadSource__c = ld[0].TelemarLeadSource__c;
            if(Id.valueOf(exisUser)!= Userinfo.getUserId()){
                acc.OwnerId = Userinfo.getUserId();
            }
            sendnotification(accId, exisUser);
            update acc;
            delete ld[0];
        }
        redirectPage(accId);
    }
}