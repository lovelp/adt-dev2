@isTest
private class LocationTrackingControllerTest {
	
	static testMethod void testProcessInvalidMapType() {
		
		User salesMgr = TestHelperClass.createManagerWithTeam();
		
		Test.startTest();
		
		System.runAs(salesMgr) {
			
			ApexPages.PageReference ref = new PageReference('/apex/LocationTracking');
	    	Test.setCurrentPageReference(ref);
	    	
	    	// invoking the controller executes getReps, getTeams
	    	LocationTrackingController ltc = new LocationTrackingController();
	    	
	    	System.assert(ltc.repList != null, 'The list of reps should not be null');
	    	System.assert(ltc.repList.size()>=1, 'The list of reps should have atleast one item');
	    	System.assert(ltc.teamList != null, 'The list of teams should not be null'); 
	    	System.assert(ltc.teamList.size()>=1, 'The list of teams should have atleasst one item');
	    	
	    	ltc.selectedMapType = '0';
	    	
	    	ltc.process();
	    	
	    	System.assert(!ltc.bMapValid, 'The map should not be valid');
	    	System.assert(ApexPages.hasMessages(), 'The page should have a message');
			
			
		}
		
		
		Test.stopTest();
		
	}
	
	static testMethod void testInitMapLoad() {
		
		User salesMgr = TestHelperClass.createManagerWithTeam();
		
		Test.startTest();
		
		System.runAs(salesMgr) {
			
			ApexPages.PageReference ref = new PageReference('/apex/LocationTracking');
	    	Test.setCurrentPageReference(ref);
	    	
	    	// invoking the controller executes getReps, getTeams
	    	LocationTrackingController ltc = new LocationTrackingController();
	    	
	    	PageReference newRef = ltc.initMapLoad();
	    	System.assert(newRef == null, 'Do not expect to any navigation');
	    
		}
		Test.stopTest();
	}
    
    static testMethod void testProcessValidMapType() {
		
		User salesMgr = TestHelperClass.createManagerWithTeam();
		
		Test.startTest();
		
		System.runAs(salesMgr) {
			
			ApexPages.PageReference ref = new PageReference('/apex/LocationTracking');
	    	Test.setCurrentPageReference(ref);
	    	
	    	// invoking the controller executes getReps, getTeams
	    	LocationTrackingController ltc = new LocationTrackingController();    	
	    	ltc.selectedMapType = '1';	    	
	    	ltc.process();
		}
		Test.stopTest();
		
	}

}