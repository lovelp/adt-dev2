/************************************* MODIFICATION LOG ********************************************************************************************
* AccountAffiliationController
*
* DESCRIPTION : Controller for the visualforce page handling account affiliations
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel herrera               03/02/2015          - Original Version
* Siddarth Asokan               12/08/2015          - Added Active checkbox
* TCS                           04/20/2017          - USAA Validation 
* Nishanth Mandala              10/09/2019          - HRM 11053 - Display affiliation values based on channel match
*/

public with sharing class AccountAffiliationController {
    
        private String accId{get;set;}   
        private Map<String, String> AccAffURLParams;
        private enum VIEW_TYPE {EDIT, CREATE, UNDEFINED} 
        private VIEW_TYPE contextView;
        public  Account_Affiliation__c accAff {get;set;}
        public String AccountAffiliation {get;set;}
        public Boolean UIDisabled {get;set;}
        public Boolean isActive {get;set;}
        public String AccName {get;set;}
        // Added by TCS --START
        public String affiliateId;
        public String selectedAffiliateValue { get;set; }
        public List<String> affiliateNames { get;set; }
        public Map<String,Affiliate__c> affNameRecordMap{ get;set; }
        public String logoURL { get;set; }
        public boolean isUSAA {get; set;}
        public String AffName;
        public decimal usaaFailCount;
        public decimal usaaTimeout;
        public decimal tMins;
        public String  sMilRank { get;set;}
        public Map<Id,Account_Affiliation__c> accAffMap { get;set; }
        public List<SelectOption> resVal = new List<SelectOption>();
        private Map<String, String> AccAffURLParamsedit;
        private Map<String, String> accAffPrefix;
        public boolean isMemberValidated {get;set;}
       
        // Added by TCS --END
        public Boolean renderSaveNew {
            get{
                return contextView == VIEW_TYPE.CREATE;
            }
        }
        
          
       
        
        public List<SelectOption> AffiliateList {
            get{
                //HRM 11053 - Affiliations based on channel - start
               string accChannel= [select channel__c from account where id=:accid].channel__c; 
                 resVal = new List<SelectOption>();       //added to avoid repitition of affilates - Nishanth  
                for(Affiliate__c affObj: [SELECT Name, Channel__c FROM Affiliate__c ]){
                    if(string.isBlank(affObj.Channel__c) || ((string.isNotBlank(affObj.Channel__c) && affObj.channel__c.contains(accChannel)) )){   
                        SelectOption so = new SelectOption(affObj.Id, affObj.Name); 
                        if( contextView == VIEW_TYPE.EDIT){
                            so.setDisabled( !affObj.Name.equalsIgnoreCase(AccountAffiliation) );
                        }
                        resVal.add( so );   
                    }            
                }
                return resVal;
            }
        }
        
        /**
        *  @Constructor
        */
        public AccountAffiliationController(Apexpages.StandardController stdcon){
            UIDisabled = false;
            isActive = false;
            AccountAffiliation = ''; 
            String AccLookupField = '';
            accAff = new Account_Affiliation__c();
            contextView = VIEW_TYPE.UNDEFINED;   
            //Initialized by Tcs -- START
            logoURL = '';
            selectedAffiliateValue = '';
            affiliateId = '';
            isUSAA = false;
            affiliateNames = new List<String>();
            affNameRecordMap = new Map<String,Affiliate__c>();
            AffName = '';
            usaaFailCount = 0;
            usaaTimeout = 0;
            tMins = 0;
            sMilRank = '';
            isMemberValidated = false;
            //Initialized by Tcs --END  
            
            // Account affiliation can only be started from an account
            String accPrefix = Schema.getGlobalDescribe().get('Account').getDescribe().getKeyPrefix();
            AccAffURLParams = ApexPages.currentPage().getParameters();
            accAffPrefix = ApexPages.currentPage().getParameters();
            for (String key : AccAffURLParams.keySet()) {
                // Edit   
                if(key.equalsIgnoreCase('Id')){
                    contextView = VIEW_TYPE.EDIT;
                    break;
                } 

                // New
                else if (key.startsWith('CF') && key.endsWith('lkid')) {
                    String accIdStr = AccAffURLParams.get(key);
                    if (accIdStr.startsWith(accPrefix)) {
                        accId = accIdStr;
                        isActive = false; // original code true
                        AccName = AccAffURLParams.get( key.substringBefore('_') );
                        contextView = VIEW_TYPE.CREATE;
                        break;
                    }
                }
            }    
            
            // We don't know where the page was fired from by just looking at the URL params
            if( contextView == VIEW_TYPE.UNDEFINED ){
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Account Affiliation can only be worked from an Account.') );
                UIDisabled = true;
            }
            
            if ( contextView == VIEW_TYPE.EDIT ){
                try{
                    AccAffURLParamsedit = ApexPages.currentPage().getParameters();
                    String ObjID = ((Account_Affiliation__c)stdcon.getRecord()).Id;          
                    accAff = [SELECT Affiliate__c, Active__c, Affiliate__r.Name, Affiliate_Quote__c, Member__c, Member__r.Name, MemberNumber__c,Military_Rank__c, Affiliate__r.Logo__c FROM Account_Affiliation__c WHERE Id = :ObjID];
                    AccName = accAff.Member__r.Name;
                    AccountAffiliation = accAff.Affiliate__r.Name;
                    accId = accAff.Member__c;
                    isActive = accAff.Active__c; 
                    // Added by TCS
                    if(AccountAffiliation=='USAA') {isUSAA=true;}                    
                    affiliateId = accAff.Affiliate__c; 
                    logoURL = accAff.Affiliate__r.Logo__c;
                    AffName = accAff.Affiliate__r.Name;
                    //End
                }
                catch(Exception err){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to load record for edit. '+err.getMessage()) );
                    contextView = VIEW_TYPE.UNDEFINED;
                }
            }  
            
            //if (contextView == VIEW_TYPE.CREATE){
                for (Affiliate__c aff : [Select Id,Name,Affiliate_Description__c,Member_Required__c, Logo__c,Member_validated_Field_Sales__c From Affiliate__c Where Active__c = true ORDER BY name]){
                    affNameRecordMap.put(aff.Name,aff);
                }
            //}
        } 

    
    //*******************************************************************
    //Method Name : getUSAAAttemptParameters defined by TCS
    //Return Type : void
    //Purpose     : get USAA Parameters
    //*******************************************************************

    @TestVisible private void getUSAAAttemptParameters (){
        if(Integer.valueof(ResaleGlobalVariables__c.getinstance('USAAAttempts').value__c) > 0){
            usaaFailCount = Integer.valueof(ResaleGlobalVariables__c.getinstance('USAAAttempts').value__c);
        }
        else{
            usaaFailCount = 4;
        }
         
        if(Integer.valueof(ResaleGlobalVariables__c.getinstance('USAATimeout').value__c) > 0){
            usaaTimeout = Integer.valueof(ResaleGlobalVariables__c.getinstance('USAATimeout').value__c);
        }
        else{
            usaaTimeout = 10;
        }
    }

    //*******************************************************************
    //Method Name : callUSAA -- defined by TCS
    //Return Type : Boolean
    //Purpose     : call USAA service to validate active USAA membership
    //*******************************************************************
    @testvisible    
    private boolean callUSAA(){
        boolean addAffiliation = true;
        isMemberValidated = false;
        if(isUSAA){
            String agentInput = '';
            String sResponse = '';     
            String LastName = [select LastName__c from Account where id =:accId].LastName__c;
            AffiliateValidationApi api = new AffiliateValidationApi();
            getUSAAAttemptParameters ();     //get USAA Parameters
         
            
            List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>();      
            affValLst = getAffValRec();   // Query the Affiliate Validation Records            
            agentInput = accAff.MemberNumber__c + ',' + LastName;
            
                
            
            if(affValLst.size() > 0 )
            {                  
                AFF_Validation__c affVal = affValLst[0];
                calculateTMins(affVal);
            
                system.debug('@@usaaFailCount: '+usaaFailCount+' affVal.Failed_Attempts__c'+affVal.Failed_Attempts__c);
                //if ( affVal.Failed_Attempts__c >= usaaFailCount && tMins < usaaTimeout && tMins != 0 && affVal.AffiliateID__r.Name == 'USAA'){
                 if ( affVal.Failed_Attempts__c >= usaaFailCount && tMins < usaaTimeout && affVal.AffiliateID__r.Name == 'USAA'){
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Maximum number of Validation reached - You must wait for ' + usaaTimeout + ' Minutes before attempting to validate again.');
                    ApexPages.addMessage(errorMsg); 
                    return false;                           
                }               
            }
            
            // Validating USAA membership with member number and account last name
            if (LastName != null){
                if(affNameRecordMap.get(AffName).Member_validated_Field_Sales__c){
                    sResponse = api.ValidateUSAAMembership(accAff.MemberNumber__c , LastName);
                    //isMemberValidated = true;
                    system.debug('@@Response: '+ sResponse );
                }                    
                else
                {
                    sResponse = 'No Need to Validate';
                    //isMembervalidated = true;
                }                                        
                
                if (sResponse != null){
                    //System.debug('Response from the AffiliateValidationApi: '+sResponse);
                    if (sResponse.contains('Success')){
                        // Reset the fail counter because of folowing good call                        
                        resetAffValRec();
                        // If response is success we add/update the record
                        accAff.Active__c = true;
                        sMilRank = sResponse.substringAfter('MilitaryRank:'); 
                        accAff.Military_Rank__c = sMilRank ;
                         system.debug('@@accAff.Military_Rank__c: '+accAff.Military_Rank__c);
                         isMemberValidated = true;
                    
                    }
                    else{
                        // If Membership NOT found mismatch DO NOT ADD/UPDATE record
                        if ( (sResponse.contains('404')) || (sResponse.contains('400')) || (sResponse.contains('412')) ) { 
                            // No Match Found
                            
                            system.debug('@@Response: '+sResponse);
                            addAffiliation = false;
                            //Track the number of Failed Attempts
                            addAFFValRec(agentInput);
                            
                            // To calculate the time remaining to do the usaa validation
                            List<AFF_Validation__c> newAffValLst1 = new List<AFF_Validation__c>();
                            newAffValLst1 = getAffValRec();                            
                            Decimal timeRemaining = usaaTimeout;
                            
                            //Check validation
                            if(newAffValLst1.size() > 0){ 
                                timeRemaining = usaaTimeout - tMins;
                                if (timeRemaining < 0)
                                {
                                    timeRemaining = usaaTimeout;
                                }
                                system.debug('@@tMins: '+tMins+ ' timeRemaining: '+timeRemaining+ ' usaaTimeout: '+usaaTimeout);
                            }
                            
                            //if(usaaFailCount <= 4){
                            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'No Match Found. After ' + usaaFailCount + ' Failed Attempts, you must wait for '+ timeRemaining +' Minutes Before you Validate Again.');
                            ApexPages.addMessage(errorMsg);
                            //}
                           
                        }
                        else if (sResponse.contains('No Need to Validate'))
                        
                        
                        { 
                            // Any other error we bypass and add/update record
                            accAff.Active__c = true;
                            //sMilRank = sResponse.substringAfter('militaryRank:');  
                            //accAff.Military_Rank__c = sMilRank ;
                            //system.debug('@@accAff.Military_Rank__c: '+accAff.Military_Rank__c);
                        }
                        else {
                            ApexPages.Message infoMsg = new ApexPages.Message(ApexPages.Severity.INFO,'USAA is currently unavailable, Please ADD Affiliate using a Member Number or try again later.' );
                            ApexPages.addMessage(infoMsg);
                            addAffiliation = false;
                        }
                        
                    }
                }  
            }                                    
        }
        return addAffiliation;
    }

    //**************************************************************************************
    // Method Name : validateMemberID method defined by TCS
    // Return Type : boolean
    // Purpose     : check if valid member ID is entered before sending request to USAA
    //**************************************************************************************

    private boolean validateMemberID(){        
       
        if (!String.isEmpty(AffName) && affNameRecordMap.get(AffName).Member_Required__c && string.isBlank(accAff.MemberNumber__c)){
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Member Number is Required to add Affiliate');
            ApexPages.addMessage(errorMsg);
            return false;
        }
        /*
        if(isUSAA && !accAff.MemberNumber__c.isNumeric()){
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Member Number should be valid value');
            ApexPages.addMessage(errorMsg);
            return false;    
        } 
        */
        accAffMap = new Map<Id,Account_Affiliation__c> ([Select Id, Name, Active__c,Affiliate__c, MemberNumber__c, Affiliate__r.Name, Affiliate__r.Affiliate_Description__c From Account_Affiliation__c Where Member__c =: accId]);
        
        if (!accAffMap.isEmpty()){
            for(Account_Affiliation__c existingAccAff : accAffMap.values()){
                if(existingAccAff.Affiliate__r.Name == AffName && existingAccAff.Active__c){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Active membership already exists for the Affiliate'));
                    return false;
                }
            }
        }
        return true;
    }
        
    //****************************************************************
    // Method Name : saveAccountAffiliate
    // Return Type : PageReference
    // Purpose     : Add or Update affiliate to Account 
    //****************************************************************

    private PageReference saveAccountAffiliate(Boolean createNew){
            
        if( !Utilities.isEmptyOrNull(AccountAffiliation) ){
            try{
                Boolean addAffiliation = true; 
                //member number empty validation for affilations that have member required falg true
                if (!validateMemberID()){
                    system.debug('@@ Validation failed');
                    return null;
                }
                if (isUSAA){
                    addAffiliation = callUSAA ();
                    if (!addAffiliation){
                        return null;
                    }
                }
                
                // Edit mode
                if( contextView == VIEW_TYPE.EDIT ){
                    update accAff; // pretty much just the membership number and/or description are allowed to update
                    isActive = accAff.Active__c;
                    AccountAffiliation = AffName;
                     ApexPages.Message errorMsg;
                    if (isUSAA){
                        if(affNameRecordMap.get(AffName).Member_validated_Field_Sales__c){
                            errorMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Member Number has been validated successfully.');}
                        else 
                            errorMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Member Number has been added successfully.');
                        ApexPages.addMessage(errorMsg);
                     }
                    return null;
                }
                
                // Insert mode
                if( contextView == VIEW_TYPE.CREATE ){
                    if (addAffiliation){
                        accAff.Affiliate__c = AccountAffiliation; // only supporting single select
                        accAff.Member__c = accId;      
                        accAff.Active__c = true;
                        insert accAff;
                        
                        isActive = accAff.Active__c;
                        AccountAffiliation = AffName;
                        if(createNew && contextView == VIEW_TYPE.CREATE)
                        {
                            PageReference pRef = new PageReference('/apex/AccountAffiliation').setRedirect(true);
                            pRef.getParameters().putAll(AccAffURLParams);
                            return pRef;
                        }
                        
                        if (isUSAA)
                        {
                            if(isMemberValidated){
                            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Member Number has been validated successfully.' )); 
                           isMemberValidated = false;  
                        }   
                        else {
                            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Member Number has been added successfully.' ));                     
                             //resetAffValRec();
                             //isMemberValidated = false;
                            }
                        }
                        
                        
                        return null;
                    }
                }
                else{
                    return new PageReference('/'+accId).setRedirect(true);
                }
            }
            catch(Exception err){
                system.debug('@@Exception: ' + err.getLineNumber() + ' Error: '+ err.getMessage());
                //ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Error while saving Affiliation. '+err.getMessage()) );
            }
        }
        return null;
    }
    
    public PageReference save(){
        return saveAccountAffiliate(false);
    }
    
    public PageReference saveNew(){
        return saveAccountAffiliate(true);
    }
    
    public PageReference cancel(){
        return new PageReference('/'+accId).setRedirect(true);
    }
    
    public PageReference updateRecord(){
        try{
            //if(validateMemberID()){
                update accAff;            
                if (contextView == VIEW_TYPE.CREATE)
                {
                    AccAffURLParams.clear();
                    AccAffURLParams.put('id', accAff.Id);
                    AccAffURLParams.put('retURL', accAff.Member__c);
                    AccAffURLParams.put('sfdc.override', '1');
                    system.debug('##AccAffURLParams: '+AccAffURLParams);
                }
                PageReference pRef = new PageReference('/apex/AccountAffiliation?id='+accAff.Member__c).setRedirect(true);
                pRef.getParameters().putAll(AccAffURLParams);
                system.debug('Page Reference is '+pRef);
                return pRef;
            }            
        //}
        catch(Exception err){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Error while updating Affiliation. '+err.getMessage()) );
        }
        return null;
    }
    
    //****************************************************************
    // Method Name : checkifUSAA defined by TCS
    // Return Type : PageReference
    // Purpose     : get the value user select from the drop down
    //****************************************************************
    
    public PageReference checkifUSAA (){
        isUSAA = false;
        logoURL = '';
        for (SelectOption so : resVal ){
            if (so.getValue() != null && so.getValue() == AccountAffiliation){
                AffName = so.getLabel();                        
            }
        }
        system.debug('AffName: '+AffName);
        if(AffName != Null && AffName != '' && AffName == 'USAA' ){
            isUSAA = true;
            logoURL = affNameRecordMap.get(AffName).Logo__c;
        }     
        return null;
    }
    
    /** Method and Test Method to check and reset the counter
    for USAA attempts **/
    
    private List<AFF_Validation__c> getAffValRec(){    
        List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>([Select Id, Failed_Attempts__c, Agent_Input__C, AffiliateID__c,AffiliateID__r.Name, CreatedDate from AFF_Validation__c where UserID__c =: UserInfo.getUserId() AND AccountID__c =: accId Order By CreatedDate Desc]);  
        return affValLst;
    }
    
    @TestVisible  private void createAffValRec(String inputStr){
        AFF_Validation__c newAffVal = new AFF_Validation__c();
        newAffVal.UserID__c = UserInfo.getUserId();
        newAffVal.AccountID__c = accId;
        newAffVal.AffiliateID__c = affNameRecordMap.get(AffName).Id;
        newAffVal.Failed_Attempts__c = 1;
        newAffVal.Agent_Input__C = 'Attempt 1 with input :'+inputStr;
        insert newAffVal;
    }
    
     @TestVisible public void calculateTMins(AFF_Validation__c affVal){
         Datetime startDate = affVal.CreatedDate; 
            Datetime endDate = system.now();
            integer intDays =  startDate.Date().daysBetween(endDate.Date());
            datetime sameDayEndDate = startDate.addDays(intDays);
            system.debug('@@stDate: '+startDate + ' edDate: '+endDate + ' intDays: '+intDays);
            system.debug('@@sameDayEndDate: '+sameDayEndDate);
            //tMins = ((endDate.getTime())/1000/60) - ((sameDayEndDate.getTime())/1000/60);
            tMins = ((endDate.getTime())/1000/60) - ((startDate.getTime())/1000/60);
            tMins = tMins + (intDays * 24* 60);
            system.debug('@@tMins: '+tMins + ' edDateTime: '+endDate.getTime() + ' saDateTime: '+startDate.getTime());
    }
    
    @TestVisible public void addAFFValRec( String agentInputStr){       
        // Initailize Affiliate Validation Record
        AFF_Validation__c affVal = new AFF_Validation__c();
        
        List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>();
        affValLst = getAffValRec();
            
        //Check validation
        if(affValLst.size() > 0){           
            affVal = affValLst[0];            
            Datetime startDate = affVal.CreatedDate; 
            Datetime endDate = system.now();
   
            integer intDays =  startDate.Date().daysBetween(endDate.Date());
            datetime sameDayEndDate = startDate.addDays(intDays);
            system.debug('@@stDate: '+startDate + ' edDate: '+endDate + ' intDays: '+intDays);
            system.debug('@@sameDayEndDate: '+sameDayEndDate);
            //tMins = ((endDate.getTime())/1000/60) - ((sameDayEndDate.getTime())/1000/60);
            tMins = ((endDate.getTime())/1000/60) - ((startDate.getTime())/1000/60);
            tMins = tMins + (intDays * 24* 60);
            system.debug('@@tMins: '+tMins + ' edDateTime: '+endDate.getTime() + ' saDateTime: '+startDate.getTime());
            if ( (tMins < usaaTimeout) && (affVal.Failed_Attempts__c <= usaaFailCount) ){                
                affVal.Failed_Attempts__c = affVal.Failed_Attempts__c +1;
                affVal.Agent_Input__C  += ' Attempts ' + affVal.Failed_Attempts__c + ' with input : ' + agentInputStr;
                update affVal;
            }else if (tMins > usaaTimeout){
                createAffValRec(agentInputStr);
            }
        }
        else{
            createAffValRec(agentInputStr);
        }
    }
    
    @TestVisible  private void resetAffValRec(){        
        // Initailize Affiliate Validation Record
        system.debug('@@reset failure count');
        AFF_Validation__c affVal = new AFF_Validation__c();
        
        List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>();
        affValLst = getAffValRec();
        system.debug('@@affValLst: '+affValLst);
            
        //Check validation
        if(affValLst.size() > 0){           
            affVal = affValLst[0];            
            system.debug('affVal: '+affVal);
            Datetime startDate = affVal.CreatedDate; 
            Datetime endDate = system.now();
   
            integer intDays =  startDate.Date().daysBetween(endDate.Date());
            datetime sameDayEndDate = startDate.addDays(intDays);
            tMins = ((endDate.getTime())/1000/60) - ((sameDayEndDate.getTime())/1000/60);
            system.debug('@@tMins: '+tMins + ' usaaTimeout: '+usaaTimeout);
            system.debug('@@affVal.Failed_Attempts__c: '+affVal.Failed_Attempts__c + ' usaaFailCount: '+usaaFailCount);
        
            if ( (tMins < usaaTimeout) && (affVal.Failed_Attempts__c <= usaaFailCount) ){
                // If a successful attempt is made the counter should be reset
                system.debug('Update record');
                affVal.Failed_Attempts__c = 0;
                affVal.Agent_Input__C = affVal.Agent_Input__C + ' Successful attempt made.';
                update affVal;
            }
        }
    }
    
    
}