@isTest
public class CRMServicesFacadeTest1 {
    
    private static final String TELEMAR_ACCOUNT_NUMBER_1 = 'T111111111';
    private static final String TELEMAR_SCHEDULE_ID = 'TST234356789';
    private static final String RESCHEDULE_TELEMAR_SCHEDULE_ID = 'S987654321';
    private static final String TRAVEL_SCHEDULE_ID = 'S34567890';
    private static final String RESCHEDULE_TRAVEL_SCHEDULE_ID = 'S43567890';
    private static final String NO_INTEREST_DISPOSITION = 'NI - No Interest';
    private static final String OTHER_DISPOSITION = 'X - Other';
    private static final String MESSAGE_ID = 'TestMessageID123';
    private static final String EMPLOYEE_NUMBER = 'HR456';
    private static final String BUSINESS_NAME = 'My Business';
    private static final String SG_TASK_CODE = 'SGL';
    private static final String CG_TASK_CODE = 'XYZ';
    private static final String TRAVEL_TASK_CODE = 'TRV';
    
    private static final Integer INSTALL_DURATION = 120;
    private static final String INSTALL_TECH_NAME = 'Joe Installer';
    private static final String INSTALL_TASK_CODE = 'In';
    
    private static final double TIME_TOLERANCE_MS = 3000;
    
	static testMethod void testSetInstallAppointmentUpdateWithOutscheduleID() {
        
        setup();
        Account a = TestHelperClass.createAccountData();
        a.TelemarAccountNumber__c = TELEMAR_ACCOUNT_NUMBER_1;
        update a; 
        a = SearchManager.getAccountByTelemarAccountNumber(TELEMAR_ACCOUNT_NUMBER_1);
        insert(new ResaleGlobalVariables__c(Name = 'InstallAppointmentTimeChangeThreshold', value__c = '2'));
        insert(new TelemarDecommission__c(Name = 'SetMMBInstallAppointment', value__c = 'TRUE'));
        CRMServicesFacade.InstallApptData iData = constructMMBInstallationData();
        iData.JobNo = '';
        iData.SeqNo = '';        
        CRMServicesFacade.InstallApptData iData1 = constructMMBInstallationData();
        iData1.Duration = iData1.Duration - 60;
        iData1.InstallTechName = iData1.InstallTechName + ' III';
        iData1.TaskCode = iData1.TaskCode + 'A';
        iData1.JobNo = '';
        iData1.SeqNo = '';
        
         User intUser = TestHelperClass.createIntegrationUser();
        
        CRMServicesFacade.MessageResponse mr1;
        System.runAs(intUser) {
            mr1 = CRMServicesFacade.setInstallationAppointment(MESSAGE_ID,
                                                'TestMessageFormat',
                                                IntegrationConstants.NewAppt,
                                                TELEMAR_ACCOUNT_NUMBER_1,
                                                iData1);
        }
        
        Test.startTest();
        
        CRMServicesFacade.MessageResponse mr = CRMServicesFacade.setInstallationAppointment(MESSAGE_ID,
                                                '',
                                                IntegrationConstants.RescheduleAppt,
                                                TELEMAR_ACCOUNT_NUMBER_1,
                                                iData);
   
        Test.stopTest();
        
        //System.assertEquals(IntegrationConstants.MESSAGE_STATUS_FAIL, mr.MessageStatus);
        //System.assert(mr.Error != null, 'An error should be present');
        //System.assert(mr.Error != '', 'The error message should not be the empty string');
    }
    
    static testMethod void testSetInstallAppointmentcancelWithOutscheduleID() {
        
        setup();
        Account a = TestHelperClass.createAccountData();
        a.TelemarAccountNumber__c = TELEMAR_ACCOUNT_NUMBER_1;
        update a; 
        a = SearchManager.getAccountByTelemarAccountNumber(TELEMAR_ACCOUNT_NUMBER_1);
        insert(new ResaleGlobalVariables__c(Name = 'InstallAppointmentTimeChangeThreshold', value__c = '2'));
        insert(new TelemarDecommission__c(Name = 'SetMMBInstallAppointment', value__c = 'TRUE'));
        CRMServicesFacade.InstallApptData iData = constructMMBInstallationData();
        iData.JobNo = '';
        iData.SeqNo = '';        
        CRMServicesFacade.InstallApptData iData1 = constructMMBInstallationData();
        iData1.Duration = iData1.Duration - 60;
        iData1.InstallTechName = iData1.InstallTechName + ' III';
        iData1.TaskCode = iData1.TaskCode + 'A';
        iData1.JobNo = '';
        iData1.SeqNo = '';
        
         User intUser = TestHelperClass.createIntegrationUser();
        
        CRMServicesFacade.MessageResponse mr1;
        System.runAs(intUser) {
            mr1 = CRMServicesFacade.setInstallationAppointment(MESSAGE_ID,
                                                'TestMessageFormat',
                                                IntegrationConstants.NewAppt,
                                                TELEMAR_ACCOUNT_NUMBER_1,
                                                iData1);
        }
        
        Test.startTest();
        
        CRMServicesFacade.MessageResponse mr = CRMServicesFacade.setInstallationAppointment(MESSAGE_ID,
                                                '',
                                                IntegrationConstants.CancelAppt,
                                                TELEMAR_ACCOUNT_NUMBER_1,
                                                iData);
   
        Test.stopTest();
        
        //System.assertEquals(IntegrationConstants.MESSAGE_STATUS_FAIL, mr.MessageStatus);
        //System.assert(mr.Error != null, 'An error should be present');
        //System.assert(mr.Error != '', 'The error message should not be the empty string');
    }
    
    
    private static CRMServicesFacade.InstallApptData constructMMBInstallationData() {
    
        CRMServicesFacade.InstallApptData i = new CRMServicesFacade.InstallApptData();
        i.InstallDateTime = Datetime.now().addDays(2);
        i.Duration = INSTALL_DURATION;
        i.InstallTechName = INSTALL_TECH_NAME;
        i.TaskCode = INSTALL_TASK_CODE;
        i.TelemarScheduleID = TELEMAR_SCHEDULE_ID;
        i.Jobno		='0123';
        i.CustNo	= '0345';
        i.SeqNo		= '1234';
        
        return i;
                  
    }
    
    private static void setup() {
        
        User current = [select Id from User where Id=:UserInfo.getUserId()];
        System.runAs(current) {        
            TestHelperClass.createReferenceDataForTestClasses();
            insert (new ResaleGlobalVariables__c(Name='SetProspectIgnoreDispo', value__c='GEN,ONA,INQ'));
            insert(new TelemarDecommission__c(Name = 'SetCloseCancelFeed', value__c = 'True'));
            
            TestHelperClass.createReferenceUserDataForTestClasses();
        }
    }
}