global class MockHTTPMMBResponse implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        // Create a fake response
        if (req.getEndpoint().endsWith('success')){
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{ "returnCode":0,"message":"success","prePayNo":195448305}');
            res.setStatusCode(200);
        }else  if (req.getEndpoint().endsWith('error500')){
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"errors": [{"status": "500","errorCode": "-1","errorMessage":"Internal Processing Error, please contact the Production Support team"}]}');
            res.setStatusCode(500);
        }
        /*else  if (req.getEndpoint().endsWith('error400')){
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"errors": [{"status": "400","errorCode": "-2","errorMessage": "Bad Request: Some Message"}]}');
            res.setStatusCode(400);
        }else if (req.getEndpoint().endsWith('error401')){
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"errors": [{"status": "401","errorCode": "-3","errorMessage": "Authorization failed"}]}');
            res.setStatusCode(401);
        }
        else if (req.getEndpoint().endsWith('error404')){
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"errors": [{"status": "404","errorCode": "-2","errorMessage": "Some Not Found Error Message"}]}');
            res.setStatusCode(404);
        }*/
        return res;
    }
}