@IsTest
public class ADTPartnerAccountUpdateControllerTest {
	@IsTest
    public Static Void adtPartnerAccountUpdate(){
        List<PartnerAPIMessaging__c> PAMList = new List<PartnerAPIMessaging__c>();
    	PAMList.add(new PartnerAPIMessaging__c(name = '400'));
        PAMList.add(new PartnerAPIMessaging__c(name = '404'));
        PAMList.add(new PartnerAPIMessaging__c(name = '500'));
        insert PAMList;
        
        list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
        rgvs.add(new ResaleGlobalVariables__c(Name='DataRecastDisableAccountTrigger', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(Name='IntegrationUser', value__c='Integration User'));
        rgvs.add(new ResaleGlobalVariables__c(Name='RIFAccountOwnerAlias', value__c='ADT RIF'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DataConversion', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(Name='MMBNameLength', value__c='28'));
        rgvs.add(new ResaleGlobalVariables__c(Name='MMBAccountNameLength', value__c='30'));
        rgvs.add(new ResaleGlobalVariables__c(Name='EmailValidationRegex', value__c='.*@.*\\..*'));
        insert rgvs;
        
        List<Equifax__c> efx = new List<Equifax__c> ();
        efx.add(new Equifax__c (name= 'Default Condition Code' , value__c= 'CAE1'));
        efx.add(new Equifax__c (name= 'Default Risk Grade' , value__c= 'W'));
        insert efx;
        
        Lead_Convert__c lc = new Lead_Convert__c();
        lc.Description__c = 'Default';
        lc.SourceNo__c = '020';
        insert lc;
        
          List<PartnerConfiguration__c> pcList = new List<PartnerConfiguration__c>();
          PartnerConfiguration__c pc = new PartnerConfiguration__c();
          pc.AccountLookUp__c ='Create PostalCode; Remove Phone; Call Id; Location Flags; Permits; Trip Fee; Alerts; Exclude Melissa and Address Check; Existing Lead Check';
          pc.PartnerID__c = 'ECOM';
          pc.AccountUpdateAPI__c = 'Deep Clone; Match Account';
          pc.IntegrationUser__c =userInfo.getUserId();
          pcList.add(pc);
          PartnerConfiguration__c pc1 = new PartnerConfiguration__c();
          pc1.AccountLookUp__c='Call Id; opportunity Id; Account List; Location Flags; Permits; Trip Fee; Alerts; Dealer Flag';
          pc1.PartnerID__c ='RV';
          pc1.IntegrationUser__c =userInfo.getUserId();
          pc1.AccountUpdateAPI__c = 'Deep Clone; Match Account';
          pcList.add(pc1);
          insert pcList;
        //create address for creating accounts
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
            insert addr;
        	
        	String addrId = String.valueOf(addr.Id);
        	RecordType standardAcctRecType = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account');
            Account acc = new Account();
            acc.RecordTypeId = standardAcctRecType.Id;
            acc.Name = 'test';
            acc.Business_Id__c = '1100 - Residential';
            acc.Channel__c = 'Resi Direct Sales';
            acc.AddressID__c = addr.Id;
            acc.MMBOrderType__c='N1';
            insert acc;
        
        Call_Data__c cData = new Call_Data__c();
        cData.name = 'test call';
        cData.Account__r = acc;
        insert cData;
        
        String cDataID = String.valueOf(cData.id);
        //Affiliate creation
            Affiliate__c aff = new Affiliate__c();
            aff.Name = 'USAA';
            aff.Active__c = true;
            insert aff;
       
            Account_Affiliation__c accaff = new Account_Affiliation__c();
            accaff.Active__c = true;
            accaff.Affiliate__c = aff.Id;
            accaff.Member__c = acc.id;
            accaff.MemberNumber__c='1234';
            insert accaff;
        
        
        String accId = string.valueOf(acc.id);
        
        String expectedValue = '{"animals": ["majestic badger", "fluffy bunny", "scary bear", "chicken", "mighty moose"]}';
        ADTPartnerAccountUpdateController adtPrtAcc = new ADTPartnerAccountUpdateController();
        ADTPartnerAccountUpdateController.affiliate affiliateWrapper = new ADTPartnerAccountUpdateController.affiliate();
        affiliateWrapper.name ='Aff Member';
        affiliateWrapper.memberNumber= '123456';
		ADTPartnerAccountUpdateController.profile profileWrapper = new ADTPartnerAccountUpdateController.profile();
        profileWrapper.affiliate = affiliateWrapper;
        ADTPartnerAccountUpdateController.name nameWrapper = new ADTPartnerAccountUpdateController.name();
		nameWrapper.first ='First';
		nameWrapper.last ='Last';
		nameWrapper.company ='Test Company';
        ADTPartnerAccountUpdateController.mmb mmbWrapper = new ADTPartnerAccountUpdateController.mmb();
        mmbWrapper.customerId ='789456';
        mmbWrapper.siteId='1234';
        mmbWrapper.orderType = 'Activate';
        ADTPartnerAccountUpdateController.phones phoneWrapper = new ADTPartnerAccountUpdateController.phones();
        phoneWrapper.cell='1236547895';
        phoneWrapper.phone ='1256325415';
		ADTPartnerAccountUpdateController.updateAccount updateAccWrapper = new ADTPartnerAccountUpdateController.updateAccount();
        updateAccWrapper.dateOfBirth = system.Today() -999;
        updateAccWrapper.email='test@tcs.com';
		updateAccWrapper.profile = profileWrapper;
        updateAccWrapper.name =nameWrapper;
        updateAccWrapper.mmb = mmbWrapper;
        updateAccWrapper.phones= phoneWrapper;
		ADTPartnerAccountUpdateController.AccountFeedFromPart accFeedWrapper = new ADTPartnerAccountUpdateController.AccountFeedFromPart();
        accFeedWrapper.account = updateAccWrapper;
        accFeedWrapper.callId =cDataID;
        accFeedWrapper.partnerId = 'ECOM';
        accFeedWrapper.opportunityId ='';
        accFeedWrapper.partnerRepName ='Test Name';
		adtPrtAcc.accountUpdateWrapper = accFeedWrapper;
        adtPrtAcc.parseDPRequest(expectedValue);
        adtPrtAcc.cloneAccount = acc;
        adtPrtAcc.getAccountDetails(accId);
        adtPrtAcc.findMatchingAccount('fName', 'lname', addrId, '1236547895', '1236547896', 'test1@tcs.com');
        adtPrtAcc.getCreateOpportunity(acc);
        adtPrtAcc.updateAccount(accId);
        adtPrtAcc.processAccountUpdate(accFeedWrapper);
        adtPrtAcc.contactUpdate(acc);
        adtPrtAcc.getCloneAccount(accId);
        
    }
}