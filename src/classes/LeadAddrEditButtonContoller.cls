/************************************* MODIFICATION LOG ********************************************************************************************
*
* DESCRIPTION : LeadAddrEditButtonContoller.cls is controller for LeadAddrEditButton.page. This contoller has init method that sets default values when User creates new Leads from Salesforce.com UI 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani			04/01/2011			- Origininal Version
*								
*/

public with sharing class LeadAddrEditButtonContoller 
{
	public String allowEdit {get;set;}
	public String leadId {get;set;}
	
	private User current;
	private ApexPages.Standardcontroller sc;
	
	public LeadAddrEditButtonContoller(ApexPages.StandardController stdController) {
		leadId = stdController.getId();
		allowEdit = 'false';
	}
	
	public PageReference init()
    {
    	
    	current = [select Id, Address_Validation__c from User where Id=:UserInfo.getUserId()];
    	System.debug('Address Validation:' + current.Address_Validation__c);
    	
    	if (current.Address_Validation__c)
    		allowEdit = 'true';
    		
		return null;
	
    }
    
    public PageReference goEdit(){
    	return null;    	
    } 

}