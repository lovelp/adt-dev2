/************************************* MODIFICATION LOG ********************************************************************************************
* LeadHelper
*
* DESCRIPTION : Helper methods acting upon the Lead object.  Includes logic that
*               deletes Resi Salesgenie and New Mover leads older than six months
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  3/12/2012           - Original Version
* Giribabu Geda					06/27/2018			- HRM-7313 Amplifinity feeding wrong leadsource to MMB
*                                                   
*/
public with sharing class LeadHelper {

    public static void deleteOldResidentialLeads(DateTime olddate) {
        List<Lead> leads = getOldLeads(olddate);
        deleteLeads(leads);
    }
    
    public static void deleteOldResidentialBuilderLeads(DateTime olddate) {
        List<Lead> leads = getOldBuilderLeads(olddate);
        deleteLeads(leads);
    }
    
    public static void deleteOldResidentialLeads() {
        Integer days = Integer.valueof( ResaleGlobalVariables__c.getinstance('DaysToRetainResidentialLeads').value__c );
        DateTime olddate = DateTime.now().addDays(-1 * days);
        deleteOldResidentialLeads(olddate);
    }
    
    public static void deleteOldResidentialBuilderLeads() {
        Integer days = Integer.valueof( ResaleGlobalVariables__c.getinstance('DaysToRetainResidentialBuilderLeads').value__c );
        DateTime olddate = DateTime.now().addDays(-1 * days);
        deleteOldResidentialBuilderLeads(olddate);
    }

    private static List<Lead> getOldLeads(DateTime olddate) {
        return new List<Lead>([
            SELECT Id from Lead
            WHERE CreatedDate < :olddate
            AND Business_Id__c like '%1100%'
            AND ( LeadSource = 'BUDCO' OR LeadSource = 'Salesgenie.com' OR LeadSource = 'Manual Import')
        ]);
    }
    
    private static List<Lead> getOldBuilderLeads(DateTime olddate) {
        return new List<Lead>([
            SELECT Id from Lead
            WHERE CreatedDate < :olddate
            AND Business_Id__c like '%1100%'
            AND ( LeadSource = 'Manual Import' and Affiliation__c = 'Builder%')
        ]);
    }
    
    private static void deleteLeads(List<Lead> leads) {
        List<StreetSheetItem__c> ssitems = new List<StreetSheetItem__c>([
            select Id from StreetSheetItem__c
            where LeadID__c IN :leads
        ]);
        
        delete ssitems;
        
        List<Disposition__c> disps = new List<Disposition__c>([
            select Id from Disposition__c
            where LeadID__c IN :leads
        ]);
        
        delete disps;
        
        delete leads;
    }
    
    //HRM-7272 DNIS Changes for Field Flow --- Mounika Anna
    //HRM-7313 Amplifinity changes on leadConversion
    Public static void setDnis(Account acc, String leadsource,user u,Map<string,DNIS__C> dnisMap) {
    	//HRM-7272 DNIS Changes for Field Flow --- Mounika Anna
    	//HRM-7313 Amplifinity changes
        Map<String,String> partnersourcingMdtMap = new Map<String,String>();
        for(Partner_Sourcing__mdt partnerMDT: [Select DeveloperName,LeadSource__c,PartnerDNIS__c from Partner_Sourcing__mdt]){
            if(String.isNotBlank(partnerMDT.LeadSource__c))
                partnersourcingMdtMap.put(partnerMDT.LeadSource__c,partnerMDT.PartnerDNIS__c);
        }
    	// HRM-7313 Amplifinity Changes --- Giribabu geda
    	for(String strLeadSource : partnersourcingMdtMap.keyset()){
    	    if(String.isnotblank(leadsource) && leadsource.equalsIgnoreCase(strLeadSource)){
    	    	String dnisVal = partnersourcingMdtMap.get(strLeadSource);
    	        acc.DNIS__c = partnersourcingMdtMap.get(strLeadSource);
    	        acc.DNIS_Modified_Date__c = acc.lastmodifieddate;
        	    acc.DNIS_Modifier_Name__c = u.id;
        	    if(String.isNotBlank(acc.data_Source__c) && acc.data_Source__c.equalsIgnoreCase(strLeadSource)){
        	    	if(string.isBlank(acc.TelemarLeadSource__c) && dnisMap!= null && dnisMap.get(dnisVal) !=null && dnisMap.get(dnisVal).Tel_Lead_Source__c !=null){
        	    		acc.TelemarLeadSource__c = dnisMap.get(dnisVal).Tel_Lead_Source__r.Name;
        	    		if(string.isBlank(acc.QueriedSource__c)){
        	    			acc.QueriedSource__c = dnisMap.get(dnisVal).Tel_Lead_Source__r.Name;
        	    		}            	    		
        	    	}
        	    }
    	    }
    	}
    	if(string.isBlank(acc.DNIS__c)){
    	    acc.DNIS__c = partnersourcingMdtMap.get('Field_Default_DNIS');
	        acc.DNIS_Modified_Date__c = acc.lastModifiedDate;
    	    acc.DNIS_Modifier_Name__c = u.id;
    	}
    } 
    

}