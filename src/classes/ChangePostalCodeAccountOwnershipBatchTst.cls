@isTest
private class ChangePostalCodeAccountOwnershipBatchTst {
	
	
	/*static testMethod void testExecute() {
		
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User user1;
		User user2;
    	System.runAs ( thisUser ) {
    		// this will be the owner of the account
    		user1 = TestHelperClass.createManagerUser();
    		// this user will be the owner of the zip code for the account
			user2 = TestHelperClass.createManagerWithTeam();
    	}
		
		Account a1;
		System.runAs(user1) {
			a1 = TestHelperClass.createAccountData();
		}
		
		String query = 'select id, PostalCodeId__c, OwnerId from Account where id IN (\'' + a1.id + '\')';	
		
		// PostalCodeId__c, User.Id
		Map<Id, Id> zipOwners = new Map<Id, Id>();
		// associate the postal code with user 2
		zipOwners.put(a1.PostalCodeId__c, user2.Id);
		// Manager.Id, User.Id
		Map<Id, Id> managerUsers = new Map<Id, Id>();
		// have a value in the map for user 2
		managerUsers.put(user2.Id, user2.Id);
		
		List<Id> deletedIds = new List<id>();
			
		Test.startTest();

    	System.runAs ( thisUser ) {

			ChangePostalCodeAccountOwnershipBatch batch = new ChangePostalCodeAccountOwnershipBatch(query, user2.Id, true, null);
		
			ID batchProcessId = Database.executeBatch(batch); 
		}

		Test.stopTest();
	
		Account a1After = [select Id, OwnerId, NewLead__c, UnassignedLead__c from Account where id = :a1.id];
		
		System.assertEquals(a1After.OwnerId, user2.Id, 'Owner should now be user 2');
		System.assert(a1After.NewLead__c, 'New Lead should be true');

		//Integer acct1ShareCount = database.Countquery('select count() from AccountShare where RowCause = \'Team\' and AccountId = \'' + a1.id + '\'');
		//Integer acct1TeamMemberCount = database.Countquery('select count() from AccountTeamMember where AccountId = \'' + a1.id + '\'');
		
		//System.assertEquals(1, acct1ShareCount, 'Should be one AccountShare for the first account');
		//System.assertEquals(1, acct1TeamMemberCount, 'Should be one AccountTeamMember for the first account'); 	
	}
	
	static testMethod void testAccountOwnerChange() {
		User rep;
		User mgr;
		Account a;
		User current = [Select Id from User where Id = :UserInfo.getUserId()];
		system.runas(current) {
			rep = TestHelperClass.createSalesRepUser();
			mgr = TestHelperClass.createManagerUser();
			a = TestHelperClass.createAccountData();
			a.OwnerId = rep.Id;
			update a;
		}
		
		String query = 'select id, PostalCodeId__c, OwnerId from Account where id IN (\'' + a.id + '\')';
		String newUserId = mgr.Id;
		Boolean isAccount = true;
		PostalCodeReassignmentQueue__c pcq = new PostalCodeReassignmentQueue__c();
		pcq.PostalCodeId__c = a.PostalCodeID__c;
		pcq.PostalCodeNewOnwer__c = newUserId;
		insert pcq;
		List<PostalCodeReassignmentQueue__c> pcqr = new List<PostalCodeReassignmentQueue__c>();
		pcqr.add(pcq);
		test.startTest();
			ChangePostalCodeAccountOwnershipBatch batch = new ChangePostalCodeAccountOwnershipBatch(query, newUserId, isAccount, pcqr);
			Database.executeBatch(batch);
		test.stopTest();
		
		Account aConfirm = [select Id, OwnerId, UnassignedLead__c from Account where Id = :a.Id];
		system.assertEquals(mgr.Id, aConfirm.OwnerId);
		system.assertEquals(false, aConfirm.UnassignedLead__c);
	}*/

}