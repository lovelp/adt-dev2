/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : ManagerTownLeadRealignmentBatch.cls is a batch class that changes account and lead ownership based on any changes made 
*				to the manager-town relationship
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli				11/07/2011			- Origininal Version
*
* Sunil Addepalli 				02/15/2012			- Modified to process both accounts and leads
*													- Added functionality to prevent multiple batches running simultaneously
*													 
*/

global class ManagerTownLeadRealignmentBatch implements Database.batchable<sObject>, Database.AllowsCallouts{

	//incoming query
	global final String query;
	global final String newOwnerId;
	global final Boolean isAccount;
	global final List<ManagerTownRealignmentQueue__c> deleteMQ;
	
	//Constructor - set the local variable with the query string
	global ManagerTownLeadRealignmentBatch(String query, String newOwnerId, Boolean isAccount, List<ManagerTownRealignmentQueue__c> deleteMQ) {
		this.query = query;
		this.newOwnerId = newOwnerId;
		this.isAccount = isAccount;
		this.deleteMQ = deleteMQ;
	}
	
	//get Querylocator with the specitied query
	global Database.QueryLocator start(Database.BatchableContext bc) {
		try{
			delete deleteMQ;
		} catch(exception ex) { 
			//do nothing. this will not thorw an error if the record is already deleted
		}
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext bc, List<sObject> sobjs) {
		if(IsAccount)
		{
			List<Account> updatedAccounts = new List<Account>();
			for(sObject sacct : sobjs)
			{
				Account acct = (account)sacct;
				acct.OwnerId = this.newOwnerId;
				acct.DateAssigned__c = DateTime.now();
				acct.UnassignedLead__c = true;
				updatedAccounts.add(acct);
			}
			
			Database.Saveresult[] SRs = database.update(updatedAccounts, false);
			List<Account> reUpdates = new List<Account>();
			Integer counter = 0;
			for(Database.SaveResult sr:SRs)
			{
				if(!sr.isSuccess())
				{
					reUpdates.add(updatedAccounts[counter]); 
				}
				counter++;
			}		
			if(reUpdates.size() > 0)
			{
				Database.Saveresult[] nSRs = database.update(reUpdates, false);
				for(Database.SaveResult nsr:nSRs)
				{
					if(!nsr.isSuccess())
					{
						//we can do something here if necessary
					}
				}				
			}
		}
		else
		{
			List<Lead> updatedLeads = new List<Lead>();
			for(sObject slead : sobjs)
			{
				Lead led = (Lead)slead;
				led.OwnerId = this.newOwnerId;
				led.DateAssigned__c = DateTime.now();
				led.UnassignedLead__c = true;
				updatedLeads.add(led);
			}
			
			Database.Saveresult[] SRs = database.update(updatedLeads, false);
			List<Lead> reUpdates = new List<Lead>();
			Integer counter = 0;
			for(Database.SaveResult sr:SRs)
			{
				if(!sr.isSuccess())
				{
					reUpdates.add(updatedLeads[counter]); 
				}
				counter++;
			}		
			if(reUpdates.size() > 0)
			{
				Database.Saveresult[] nSRs = database.update(reUpdates, false);
				for(Database.SaveResult nsr:nSRs)
				{
					if(!nsr.isSuccess())
					{
						//we can do something here if necessary
					}
				}				
			}			
		}
	}
	
	global void finish(Database.BatchableContext bc) {

	}
	
}