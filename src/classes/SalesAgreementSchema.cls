/************************************* MODIFICATION LOG ********************************************************************************************
 * SalesAgreementSchema
 *
 * DESCRIPTION : Containing Request and response for Sales Agreement
 *  1. Create
 *  2. Update
 *  3. Lookup
 *               
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                        DATE              TICKET            REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Jitendra Kothari                 05/18/2019         HRM-9710          Original
 * Jitendra Kothari                 07/15/2019         HRM-10409         Enhance Existing Sales Agreement Lookup to Add Fields
 * Jitendra Kothari                 08/22/2019         HRM-10982         SFDC & Sales Pilot Integration - Additional Fields
 * Jitendra Kothari                 09/06/2019         HRM-9820          Ability to capture existing equipment that customer can bring to the new site 
 */
public class SalesAgreementSchema {

    public class SalesAgreementCreateRequest{
        @AuraEnabled public String PrimarySalesRep{get;set;}
        @AuraEnabled public String SalesAgreementName{get;set;}
        @AuraEnabled public String sfdcOpportunityID{get;set;}
        @AuraEnabled public String companyName{get;set;}
        @AuraEnabled public String attention{get;set;}
        @AuraEnabled public String customerAddress1{get;set;}
        @AuraEnabled public String customerAddress2{get;set;}
        @AuraEnabled public String CustomerCity{get;set;}
        @AuraEnabled public String CustomerState{get;set;}
        @AuraEnabled public String CustomerCounty{get;set;}
        @AuraEnabled public String CustomerZip{get;set;}
        @AuraEnabled public String CustomerEmail{get;set;}
        @AuraEnabled public String CustomerPhone{get;set;}
        @AuraEnabled public String MastermindCustomerNumber{get;set;}
        @AuraEnabled public String mastermindInstanceID{get;set;}
    }
    
    public class SalesAgreementCreateResponse{
        @AuraEnabled public Boolean status{get;set;}
        //public String SalesAgreementID{get;set;}
        @AuraEnabled public String message{get;set;}
        @AuraEnabled public String result{get;set;}
        @AuraEnabled public Integer statusCode{get;set;}
    }
    
    public class SalesAgreementLookupRequest{
        @AuraEnabled public String SalesAgreementID{get;set;}
    }
    
    public class SalesAgreementLookupResp{
    	@AuraEnabled  public Boolean status{get;set;}
		@AuraEnabled  public String message{get;set;}
		@AuraEnabled  public SalesAgreementLookupResponse result{get;set;}
		@AuraEnabled public Integer statusCode{get;set;}
    }
    public class SalesAgreementLookupResponse{
        @AuraEnabled public String salesAgreementID{get;set;}
		@AuraEnabled public String salesAgreementName{get;set;}
		@AuraEnabled public String primarySalesRepID{get;set;}
		@AuraEnabled public String primarySalesRepName{get;set;}
		@AuraEnabled public String companyName{get;set;}
		@AuraEnabled public String attention{get;set;}
		@AuraEnabled public String customerAddress1{get;set;}
		@AuraEnabled public String customerAddress2{get;set;}
		@AuraEnabled public String customerCity{get;set;}
		@AuraEnabled public String customerState{get;set;}
		@AuraEnabled public String customerZip{get;set;}
		@AuraEnabled public String customerCounty{get;set;}
		@AuraEnabled public String customerEmail{get;set;}
		@AuraEnabled public String customerPhone{get;set;}
		@AuraEnabled public String mastermindCustomerNumber{get;set;}
		@AuraEnabled public Integer mastermindInstanceID{get;set;}
		@AuraEnabled public Decimal installCharge{get;set;}
		@AuraEnabled public Decimal sellingTotalRMR{get;set;}
		@AuraEnabled public String saleStatus{get;set;}
		@AuraEnabled public String agreementPhase{get;set;}
		@AuraEnabled public String lastModified{get;set;}
		@AuraEnabled public String sfdcOpportunityID{get;set;}
        //@AuraEnabled public List<SystemDesigns> systemDesigns{get;set;}
        @AuraEnabled public List<Sites> sites{get;set;}
        @AuraEnabled public Integer numberOfSites{get;set;} 
    }
    
	public class SystemDesigns {
		@AuraEnabled public String systemDesignId{get;set;}
		@AuraEnabled public String designTypeName{get;set;}
		@AuraEnabled public String designName{get;set;}
		@AuraEnabled public Decimal installCharge{get;set;}
		@AuraEnabled public Decimal sellingRMR{get;set;}
		@AuraEnabled public Boolean includedOnContract{get;set;}
		@AuraEnabled public String mastermindJobNumber{get;set;}
		@AuraEnabled public String ownership{get;set;}
		@AuraEnabled public String siteID{get;set;}
	}
	
	public class Sites {
		@AuraEnabled public String siteID{get;set;}
		@AuraEnabled public String mastermindSiteNumber{get;set;}
		@AuraEnabled public String siteTypeName{get;set;}
		@AuraEnabled public String siteStatus{get;set;}
		@AuraEnabled public String siteName{get;set;}
		@AuraEnabled public String siteAddress1{get;set;}
		@AuraEnabled public String siteAddress2{get;set;}
		@AuraEnabled public String siteCity{get;set;}
		@AuraEnabled public String siteState{get;set;}
		@AuraEnabled public String siteZip{get;set;}
		@AuraEnabled public String siteCounty{get;set;}
		@AuraEnabled public String siteEmail{get;set;}
		@AuraEnabled public String sitePhone{get;set;}
		@AuraEnabled public String siteAddress{get;set;}
		@AuraEnabled public List<SystemDesigns> systemDesigns{get;set;}
	}
    
    public class SalesAgreementUpdateRequest{
        @AuraEnabled public String SalesAgreementID{get;set;}
        @AuraEnabled public String sfdcOpportunityID{get;set;}
        @AuraEnabled public String Status{get;set;}
    }
    
    public class SalesAgreementUpdateResponse{
        @AuraEnabled public Boolean status{get;set;}
        @AuraEnabled public String message{get;set;}
        @AuraEnabled public String result{get;set;}
        @AuraEnabled public Integer statusCode{get;set;}
    }
    public class Errors {
		public String status;
		public String errorCode;
		public String errorMessage;
	}

    public class ErrorMessage{
        public List<Errors> errors;
    }
}