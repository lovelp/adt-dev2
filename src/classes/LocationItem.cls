/************************************* MODIFICATION LOG ********************************************************************************************
* LocationItem
*
* DESCRIPTION : A data structure for points rendered on the location tracking map views.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner                  10/14/2011              - Original Version
*
*                                                   
*/

public class LocationItem {
    public string telenavDate {get;set;}
    public string pTime {get;set;}
    public DateTime pDateTime {get; set;}
    public string pLatitude {get;set;}
    public string pLongitude {get;set;}
    public string pPhoneNum {get;set;}
    public string key {get;set;}
    public string reverseAddress {get;set;}
    public string AccountId {get;set;}
    public string gMapsLink {get;set;}
    public string sMapLinkText {get;set;}
    public string sRecordId {get;set;}
    public string sRecordText {get;set;}
    public string sAddressRecordId {get;set;}
    public string primaryColor {get;set;}
    public string name {get;set;}
    public string formattedDate {get;set;}
    public string sLabel {get;set;}
    public string sRecordName {get;set;}
    public User rUser {get;set;}
    public disposition__c knock {get;set;}
    
    public LocationItem(){  
        this.primaryColor='FF6666';
    }
    
    public LocationItem(disposition__c disposition, Account acct ) {
        
        this.platitude=string.valueOf(acct.latitude__c);
        this.plongitude=string.valueOf(acct.longitude__c);
        this.gMapsLink= '<a href="#" onclick="javascript:window.open(\'http://maps.google.com/?q='+platitude +','+plongitude+'\')">'+platitude +','+plongitude+'</a>';
        this.sMapLinkText='\'http://maps.google.com/?q='+platitude +','+plongitude+'\'';
        this.primaryColor='000099';
        if (disposition.DispositionType__c =='SA - Sold / Activated' || disposition.DispositionType__c == 'SD - Sold') {
            this.primaryColor='308014';
        }   
        this.name=disposition.DispositionType__c;
        this.sRecordId=acct.id;
        this.sRecordText=acct.SiteStreet__c;// + ' ' + a.SiteCity__c + ', ' + a.SitePostalCode__c;
        this.sRecordName = acct.name;
        this.sAddressRecordId = acct.AddressID__c;
        this.formattedDate=disposition.DispositionDate__c.format();
        this.pdateTime=disposition.DispositionDate__c;
        
    }
    
    public LocationItem(disposition__c disposition, String latitude, String longitude) {
        
        this.knock = disposition;
        this.platitude= latitude;
        this.plongitude = longitude;
        this.gMapsLink= '<a href="#" onclick="javascript:window.open(\'http://maps.google.com/?q='+platitude +','+plongitude+'\')">'+platitude +','+plongitude+'</a>';
        this.sMapLinkText='\'http://maps.google.com/?q='+platitude +','+plongitude+'\'';
    
        this.accountId=disposition.AccountID__c;
        this.pTime =disposition.DispositionDate__c.format('h:mm a');
        this.key=this.telenavDate+'-'+this.pPhoneNum;
        system.debug('KEY~~~~~~~'+this.key);
        //this.sHoverText = '<a href="/' + k.id +'" target="_blank">View Knock</a><br/>' +this.gMapsLink+'<br/>'+k.disposition__c+'<br/>'+this.pTime;
        this.primaryColor='FF6666';
        this.name=disposition.DispositionType__c;
        this.sRecordName = disposition.Name;
        this.srecordId=disposition.id;
        this.sRecordText='';
        this.pdateTime=disposition.DispositionDate__c;
        this.formattedDate=disposition.DispositionDate__c.format();
        
    }
    
}