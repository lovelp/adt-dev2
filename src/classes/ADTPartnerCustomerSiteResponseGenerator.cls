/************************************* MODIFICATION LOG ********************************************************************************************
* customer site partner API
*
* DESCRIPTION : Simulates responses from external services to test local applications
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Ravi Krishna             11/16/2017          - Original Version
*
*                                                   
*/

@isTest(SeeAllData=true)
global class ADTPartnerCustomerSiteResponseGenerator implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        
        Boolean isCustomerLookup = false;
        Boolean isSiteLookup = false;
        Boolean isGetJob = false;
        Boolean isGetZones = false;
        Boolean isBillingInfo=false;
        boolean isIncidentLookup=false;
        Boolean isInformixCustomerLookup=false;
        Boolean isEmployeeActions=false;
        Boolean isSysConfig=false;
        XmlStreamReader reader = new XmlStreamReader(req.getBody());
        while(reader.hasNext()) { 
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
                if('LookupSite' == reader.getLocalName() ){
                    isSiteLookup = true;
                    isCustomerLookup = false;
                    isGetJob = false;
                    isGetZones = false;
                    isBillingInfo=false;
                    isIncidentLookup=false;
                    isInformixCustomerLookup= false;
                    isSysConfig=false;
                    isEmployeeActions=false;
                    break;  
                }
                if('LookupCustomer' == reader.getLocalName() ){
                    isCustomerLookup = true;
                    //isCustomerLookup = false;
                    isSiteLookup = false;
                    isGetJob = false;
                    isGetZones = false;
                    isBillingInfo=false;
                    isIncidentLookup=false;
                    isInformixCustomerLookup= false;
                    isSysConfig=false;
                    isEmployeeActions=false;
                    break;  
                }
                if('LookupInformixCustomer' == reader.getLocalName() ){
                    isInformixCustomerLookup= true;
                    isCustomerLookup = false;
                    isSiteLookup = false;
                    isGetJob = false;
                    isGetZones = false;
                    isBillingInfo=false;
                    isIncidentLookup=false;
                    isEmployeeActions=false;
                    isSysConfig=false;
                    break;  
                }
                if('GetJob' == reader.getLocalName() ){
                    isGetJob = true;
                    isCustomerLookup = false;
                    isSiteLookup = false;
                    //isGetJob = false;
                    isGetZones = false;
                    isBillingInfo=false;
                    isIncidentLookup=false;
                    isInformixCustomerLookup= false;
                    isEmployeeActions=false;
                    isSysConfig=false;
                    break;  
                }
                if('GetZones' == reader.getLocalName() ){
                    isGetZones = true;
                    isCustomerLookup = false;
                    isSiteLookup = false;
                    isGetJob = false;
                    //isGetZones = false;
                    isBillingInfo=false;
                    isIncidentLookup=false;
                    isInformixCustomerLookup= false;
                    isEmployeeActions=false;
                    isSysConfig=false;
                    break;  
                }
                if('GetRecurrentLines'==reader.getLocalName()){
                    isBillingInfo=true;
                    isCustomerLookup = false;
                    isSiteLookup = false;
                    isGetJob = false;
                    isGetZones = false;
                    //isBillingInfo=false;
                    isIncidentLookup=false;
                    isInformixCustomerLookup= false;
                    isEmployeeActions=false;
                    isSysConfig=false;
                    break;
                
                }
                if('GetIncidentIssues'==reader.getLocalName()){
                    isIncidentLookup=true;
                    isCustomerLookup = false;
                    isSiteLookup = false;
                    isGetJob = false;
                    isGetZones = false;
                    isBillingInfo=false;
                    //isIncidentLookup=false;
                    isInformixCustomerLookup= false;
                    isEmployeeActions=false;
                    isSysConfig=false;
                    break;
                
                }
                if('GetEmployeeActions'==reader.getLocalName()){
                    isEmployeeActions=true;
                    isIncidentLookup=false;
                    isCustomerLookup = false;
                    isSiteLookup = false;
                    isGetJob = false;
                    isGetZones = false;
                    isBillingInfo=false;
                    isInformixCustomerLookup= false;
                    isSysConfig=false;
                    break;
                
                }
                if('GetSysConfigs'==reader.getLocalName()){
                    isEmployeeActions=false;
                    isIncidentLookup=false;
                    isCustomerLookup = false;
                    isSiteLookup = false;
                    isGetJob = false;
                    isGetZones = false;
                    isBillingInfo=false;
                    isInformixCustomerLookup= false;
                    isSysConfig=true;
                    break;
                
                }
            }
            reader.next();
        }
        
        system.debug('\n LookupSite='+isSiteLookup+' LookupCustomer='+isCustomerLookup+'\n');
        
        // Create a fake response for a customer lookup web service call
        String xmlBodyStr = '';
        if(isCustomerLookup == true){
            xmlBodyStr='<NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/">'
                        +'  <NS1:Body>'
                        +'      <NS2:LookupCustomerResponse xmlns:NS2="http://mastermindservice.adt.com/lookup">'
                        +'          <NS2:customerInfo>'
                        +'              <NS2:FirstName />'
                        +'              <NS2:LastName>IRWAN TEST02</NS2:LastName>'
                        +'              <NS2:Address1>8221 GLADES RD</NS2:Address1>'
                        +'              <NS2:Address2 />'
                        +'              <NS2:City>BOCA RATON</NS2:City>'
                        +'              <NS2:State>FL</NS2:State>'
                        +'              <NS2:ZipCode>33434-4072</NS2:ZipCode>'
                        +'              <NS2:Phone>9991231234</NS2:Phone>'
                        +'              <NS2:BillingSystem>MMB</NS2:BillingSystem>'
                        +'              <NS2:BillingCustNo>400306879</NS2:BillingCustNo>'
                        +'              <NS2:CustomerStatus>I</NS2:CustomerStatus>'
                        +'              <NS2:ServiceActive>OUT</NS2:ServiceActive>'
                        +'              <NS2:DiscoDate />'
                        +'              <NS2:DiscoReasonCode />'
                        +'              <NS2:PastDueStatus />'
                        +'              <NS2:PastDueAmount />'
                        +'              <NS2:PIC />'
                        +'              <NS2:Partner />'
                        +'              <NS2:ContractStartDate />'
                        +'              <NS2:ContractExpirationDate />'
                        +'              <NS2:ContractTerm />'
                        +'              <NS2:SiteNo>46255536</NS2:SiteNo>'
                        +'              <NS2:CustType>MMB-Small Business</NS2:CustType>'
                        +'              <NS2:CustTypeId>SMB</NS2:CustTypeId>'
                        +'              <NS2:MTMCustNo />'
                        +'              <NS2:MTMSiteNo />'
                        +'          </NS2:customerInfo>'
                        +'          <NS2:ResultSummaryMessage>SUCCESS</NS2:ResultSummaryMessage>'
                        +'      </NS2:LookupCustomerResponse>'
                        +'  </NS1:Body>'
                        +'</NS1:Envelope>';
        } else if(isInformixCustomerLookup == true){
            system.debug('------------------------>>>informix customer lookup');
            xmlBodyStr='<?xml version="1.0" encoding="UTF-8"?>'
                        +'<NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/">'
                        +'   <NS1:Body>'
                        +'      <NS2:LookupInformixCustomerResponse xmlns:NS2="http://mastermindservice.adt.com/lookup">'
                        +'         <NS2:informixCustomerInfo>'
                        +'            <NS2:FirstName>SPIVEYS AUTO PARTS</NS2:FirstName>'
                        +'            <NS2:LastName />'
                        +'            <NS2:Address1>4801 LEIGH DRIVE</NS2:Address1>'
                        +'            <NS2:Address2 />'
                        +'            <NS2:City>RALEIGH</NS2:City>'
                        +'            <NS2:State>NC</NS2:State>'
                        +'            <NS2:ZipCode>27604-2884</NS2:ZipCode>'
                        +'            <NS2:Phone>919-872-4930</NS2:Phone>'
                        +'            <NS2:BillingSystem>INF</NS2:BillingSystem>'
                        +'            <NS2:BillingCustNo>400306876</NS2:BillingCustNo>'
                        +'            <NS2:CustomerStatus>A</NS2:CustomerStatus>'
                        +'            <NS2:ServiceActive>IN</NS2:ServiceActive>'
                        +'            <NS2:DiscoDate>1999-01-11</NS2:DiscoDate>'
                        +'            <NS2:DiscoReasonCode>OUT OF BUSINESS</NS2:DiscoReasonCode>'
                        +'            <NS2:PastDueStatus />'
                        +'            <NS2:PastDueAmount>0.00</NS2:PastDueAmount>'
                        +'            <NS2:ReallyPastDueAmount>0.00</NS2:ReallyPastDueAmount>'
                        +'            <NS2:WriteOffAmount />'
                        +'            <NS2:DiscoCount>1</NS2:DiscoCount>'
                        +'            <NS2:PIC />'
                        +'            <NS2:Partner />'
                        +'            <NS2:ContractStartDate />'
                        +'            <NS2:ContractExpirationDate />'
                        +'            <NS2:ContractTerm>0</NS2:ContractTerm>'
                        +'            <NS2:SiteNo />'
                        +'            <NS2:CustType>SmallBis</NS2:CustType>'
                        +'            <NS2:MTMCustNo />'
                        +'            <NS2:MTMSiteNo />'
                        +'            <NS2:BillCodes />'
                        +'            <NS2:ContractNo>0</NS2:ContractNo>'
                        +'            <NS2:Comment>Per new format of MA# on cancelled account, updated MA# to X1N027400933</NS2:Comment>'
                        +'            <NS2:Comment>BILLING CYCLE UPDATED AS 99 FROM 4</NS2:Comment>'
                        +'            <NS2:Comment>RECVD BANCO CK #013783 IN AMT OF 104.50 RECVD TO COVER FUNDS</NS2:Comment>'
                        +'            <NS2:Comment>AMOUNT PLACED WITH COLLECTION AGENCY - $104.50</NS2:Comment>'
                        +'            <NS2:Comment>ACCOUNT WRITTEN OFF    $104.50</NS2:Comment>'
                        +'            <NS2:Comment>QUEUE REQUEST COMPLETED  - ORIGINAL REQ POSTED ON 1999-01-08 07:26:06</NS2:Comment>'
                        +'            <NS2:Comment>(*ARNOTE*ACCOUNT RECOVERY DEPARTMENT NOTEPAD)</NS2:Comment>'
                        +'            <NS2:Comment>(*MLD*OLD OWNER CANCELLATION REQUEST)</NS2:Comment>'
                        +'            <NS2:Comment>3RD PARTY ACTIVITY-SENT COL LTR #1</NS2:Comment>'
                        +'            <NS2:Comment>3RD PARTY ACTIVITY-SENT COL LTR #3</NS2:Comment>'
                        +'            <NS2:MonthlyRate>33.00</NS2:MonthlyRate>'
                        +'            <NS2:MonitoringAmount>33.00</NS2:MonitoringAmount>'
                        +'            <NS2:CollectionAgency>ABANCO</NS2:CollectionAgency>'
                        +'            <NS2:BillingFrequency>A</NS2:BillingFrequency>'
                        +'            <NS2:DiscoReqDate>1999-01-11</NS2:DiscoReqDate>'
                        +'            <NS2:DiscoEffectiveDate>1998-11-06</NS2:DiscoEffectiveDate>'
                        +'            <NS2:CTCflag>N</NS2:CTCflag>'
                        +'            <NS2:CTCBOCCharges>0.00</NS2:CTCBOCCharges>'
                        +'            <NS2:ChargeBackStatus>N</NS2:ChargeBackStatus>'
                        +'            <NS2:DealerNo>1022706</NS2:DealerNo>'
                        +'            <NS2:DealerName>ADT RALEIGH (INFORMIX) ACCTS</NS2:DealerName>'
                        +'            <NS2:DealerPhone>1-866-280-2326</NS2:DealerPhone>'
                        +'            <NS2:InstallingDealerNo>401581</NS2:InstallingDealerNo>'
                        +'            <NS2:MasterDealerNo>401581</NS2:MasterDealerNo>'
                        +'            <NS2:PastDueDays>0</NS2:PastDueDays>'
                        +'            <NS2:DealType>P</NS2:DealType>'
                        +'            <NS2:CancelCode>C179</NS2:CancelCode>'
                        +'            <NS2:ReinstateFlag>Y</NS2:ReinstateFlag>'
                        +'            <NS2:ReinstateReason>Special</NS2:ReinstateReason>'
                        +'            <NS2:RelocationFlag>N</NS2:RelocationFlag>'
                        +'            <NS2:RelocationReason>Account not Active</NS2:RelocationReason>'
                        +'            <NS2:AddOnFlag>N</NS2:AddOnFlag>'
                        +'            <NS2:AddOnReason>Account not Active</NS2:AddOnReason>'
                        +'            <NS2:ConversionFlag>N</NS2:ConversionFlag>'
                        +'            <NS2:ConversionReason>Account not Active</NS2:ConversionReason>'
                        +'            <NS2:MultisiteFlag>N</NS2:MultisiteFlag>'
                        +'            <NS2:MultisiteReason>Defender Account in Chargeback</NS2:MultisiteReason>'
                        +'         </NS2:informixCustomerInfo>'
                        +'         <NS2:ResultSummaryMessage>SUCCESS</NS2:ResultSummaryMessage>'
                        +'      </NS2:LookupInformixCustomerResponse>'
                        +'   </NS1:Body>'
                        +'</NS1:Envelope>';
        }
        else if(isSysConfig == true){
            system.debug('------------------------>>>sysconfig');
            xmlBodyStr='<?xml version="1.0" encoding="UTF-8"?>'
                        +'<NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/">'
                        +'   <NS1:Body>'
                        +'      <NS2:GetSysConfigsResponse xmlns:NS2="http://mastermindservice.adt.com/lookup">'
                        +'         <NS2:ErrorNumber>0</NS2:ErrorNumber>'
                        +'         <NS2:ErrorMessage>OK</NS2:ErrorMessage>'
                        +'            <SysConfigs>'
                        +'            <SysConfig>'
                        +'            <SystemNo>31785087</SystemNo>'
                        +'            <SeqNo>1</SeqNo>'
                        +'            <ItemNo>3G4000RF</ItemNo>'
                        +'            <ItemDescr>Transformer Location</ItemDescr>'
                        +'            <EquipLocID />'
                        +'            <ZoneID />'
                        +'            <Point />'
                        +'            <Comment>MSTR BED RM CLOSET</Comment>'
                        +'            <InstallDate>2013-07-22T00:00:00</InstallDate>'
                        +'            <InactiveDate>2079-01-01T00:00:00</InactiveDate>'
                        +'            <JobNo />'
                        +'            </SysConfig>'
                        +'         </SysConfigs>'
                        +'      </NS2:GetSysConfigsResponse>'
                        +'   </NS1:Body>'
                        +'</NS1:Envelope>';
        }
        else if(isSiteLookup == true){
            xmlBodyStr='<NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/">'
                    +'   <NS1:Body>'
                    +'      <NS2:LookupSiteResponse xmlns:NS2="http://mastermindservice.adt.com/lookup">'
                    +'          <NS2:siteInfo>'
                    +'              <NS2:SiteNo>46255536</NS2:SiteNo>'
                    +'              <NS2:SiteType>SM BUS - Small Business</NS2:SiteType>'
                    +'              <NS2:SiteTypeId>S</NS2:SiteTypeId>'
                    +'              <NS2:SiteName>IRWAN TEST02</NS2:SiteName>'
                    +'              <NS2:SiteAddress1>8221 GLADES RD</NS2:SiteAddress1>'
                    +'              <NS2:SiteAddress2 />'
                    +'              <NS2:City>BOCA RATON</NS2:City>'
                    +'              <NS2:State>FL</NS2:State>'
                    +'              <NS2:ZipCode>33434-4072</NS2:ZipCode>'
                    +'              <NS2:Pics>'
                    +'                  <NS2:Pic>NNBH</NS2:Pic>'
                    +'              </NS2:Pics>'
                    +'              <NS2:Partners />'
                    +'              <NS2:HOA>N</NS2:HOA>'
                    +'              <NS2:BHT>Y</NS2:BHT>'
                    +'              <NS2:CHS>N</NS2:CHS>'
                    +'              <NS2:MTMSiteNo />'
                    +'              <NS2:BillingCustNos>'
                    +'                  <NS2:BillingCustNo>400306877</NS2:BillingCustNo>'
                    +'              </NS2:BillingCustNos>'
                    +'              <NS2:BillingSystems>'
                    +'                  <NS2:BillingSystem>Informix Billed</NS2:BillingSystem>'
                    +'              </NS2:BillingSystems>'
                    +'              <NS2:CS_NO>9400306876</NS2:CS_NO>'
                    +'              <NS2:PulseService />'
                    +'              <NS2:SystemType>CARD</NS2:SystemType>'
                    +'              <NS2:SystemStartDate>2014-05-19 19:19:06</NS2:SystemStartDate>'
                    +'              <NS2:ZoneCount>0</NS2:ZoneCount>'
                    +'              <NS2:Smoke>N</NS2:Smoke>'
                    +'              <NS2:CoDetection>N</NS2:CoDetection>'
                    +'              <NS2:PanelType>Card Access</NS2:PanelType>'
                    +'              <NS2:ServicePlan>1090CS</NS2:ServicePlan>'
                    +'              <NS2:CsAccountType>Primary</NS2:CsAccountType>'
                    +'              <NS2:CellBackup>N</NS2:CellBackup>'
                    +'              <NS2:ServiceActive>OUT</NS2:ServiceActive>'
                    +'              <NS2:InstallCompany>623</NS2:InstallCompany>'
                    +'              <NS2:SYSTEM_NO>1234</NS2:SYSTEM_NO>'
                    +'              <NS2:ResaleEligible>ELIGIBLE</NS2:ResaleEligible>'
                    +'          </NS2:siteInfo>'
                    +'          <NS2:siteInfo>'
                    +'              <NS2:SiteNo>47962183</NS2:SiteNo>'
                    +'              <NS2:SiteType>RESI - Residential</NS2:SiteType>'
                    +'              <NS2:SiteTypeId>R</NS2:SiteTypeId>'
                    +'              <NS2:SiteName>CUFFE, CHRIS</NS2:SiteName>'
                    +'              <NS2:SiteAddress1>520 SAINT CLAUDE PL</NS2:SiteAddress1>'
                    +'              <NS2:SiteAddress2 />'
                    +'              <NS2:City>JACKSONVILLE</NS2:City>'
                    +'              <NS2:State>FL</NS2:State>'
                    +'              <NS2:ZipCode>32259-4074</NS2:ZipCode>'
                    +'              <NS2:Pics>'
                    +'                  <NS2:Pic>VEGAS</NS2:Pic>'
                    +'              </NS2:Pics>'
                    +'              <NS2:Partners />'
                    +'              <NS2:HOA>N</NS2:HOA>'
                    +'              <NS2:BHT>N</NS2:BHT>'
                    +'              <NS2:CHS>Y</NS2:CHS>'
                    +'              <NS2:MTMSiteNo />'
                    +'              <NS2:BillingCustNos>'
                    +'                 <NS2:BillingCustNo>400306876</NS2:BillingCustNo>'
                    +'              </NS2:BillingCustNos>'
                    +'              <NS2:BillingSystems>'
                    +'                 <NS2:BillingSystem>P1MMB</NS2:BillingSystem>'
                    +'              </NS2:BillingSystems>'
                    +'              <NS2:SYSTEM_NO>30690515</NS2:SYSTEM_NO>'
                    +'              <NS2:CS_NO>H404299522</NS2:CS_NO>'
                    +'              <NS2:PulseService />'
                    +'              <NS2:SystemType>FSVF</NS2:SystemType>'
                    +'              <NS2:SystemStartDate>2015-06-27 10:45:47</NS2:SystemStartDate>'
                    +'              <NS2:ZoneCount>34</NS2:ZoneCount>'
                    +'              <NS2:Smoke>N</NS2:Smoke>'
                    +'              <NS2:CoDetection>Y</NS2:CoDetection>'
                    +'              <NS2:PanelType>Safewatch 3000 (V20P)</NS2:PanelType>'
                    +'              <NS2:ServicePlan>1014CR</NS2:ServicePlan>'
                    +'              <NS2:CsAccountType>Primary</NS2:CsAccountType>'
                    +'              <NS2:CellBackup>N</NS2:CellBackup>'
                    +'              <NS2:ServiceActive>OUT</NS2:ServiceActive>'
                    +'              <NS2:InstallCompany>686</NS2:InstallCompany>'
                    +'              <NS2:ResaleEligible>ELIGIBLE</NS2:ResaleEligible>'
                    +'              </NS2:siteInfo>'
                    +'              <NS2:siteInfo>'
                    +'              <NS2:SiteNo>47962183</NS2:SiteNo>'
                    +'              <NS2:SiteType>RESI - Residential</NS2:SiteType>'
                    +'              <NS2:SiteName>CUFFE, CHRIS</NS2:SiteName>'
                    +'              <NS2:SiteAddress1>520 SAINT CLAUDE PL</NS2:SiteAddress1>'
                    +'              <NS2:SiteAddress2 />'
                    +'              <NS2:City>JACKSONVILLE</NS2:City>'
                    +'              <NS2:State>FL</NS2:State>'
                    +'              <NS2:ZipCode>32259-4074</NS2:ZipCode>'
                    +'              <NS2:Pics>'
                    +'                  <NS2:Pic>VEGAS</NS2:Pic>'
                    +'              </NS2:Pics>'
                    +'              <NS2:Partners />'
                    +'              <NS2:HOA>Y</NS2:HOA>'
                    +'              <NS2:BHT>N</NS2:BHT>'
                    +'              <NS2:CHS>N</NS2:CHS>'
                    +'              <NS2:MTMSiteNo />'
                    +'              <NS2:BillingCustNos>'
                    +'                 <NS2:BillingCustNo>400306879</NS2:BillingCustNo>'
                    +'              </NS2:BillingCustNos>'
                    +'              <NS2:BillingSystems>'
                    +'                 <NS2:BillingSystem>MMB-Residential</NS2:BillingSystem>'
                    +'              </NS2:BillingSystems>'
                    +'              <NS2:SYSTEM_NO>30690515</NS2:SYSTEM_NO>'
                    +'              <NS2:CS_NO>H404299522</NS2:CS_NO>'
                    +'              <NS2:PulseService />'
                    +'              <NS2:SystemType>FSVF</NS2:SystemType>'
                    +'              <NS2:SystemStartDate>2015-06-27 10:45:47</NS2:SystemStartDate>'
                    +'              <NS2:ZoneCount>34</NS2:ZoneCount>'
                    +'              <NS2:Smoke>N</NS2:Smoke>'
                    +'              <NS2:CoDetection>Y</NS2:CoDetection>'
                    +'              <NS2:PanelType>Safewatch 3000 (V20P)</NS2:PanelType>'
                    +'              <NS2:ServicePlan>1014CR</NS2:ServicePlan>'
                    +'              <NS2:CsAccountType>Primary</NS2:CsAccountType>'
                    +'              <NS2:CellBackup>N</NS2:CellBackup>'
                    +'              <NS2:ServiceActive>OUT</NS2:ServiceActive>'
                    +'              <NS2:InstallCompany>686</NS2:InstallCompany>'
                    +'              <NS2:ResaleEligible>ELIGIBLE</NS2:ResaleEligible>'
                    +'              </NS2:siteInfo>'                  
                    +'          <NS2:ResultSummaryMessage>SUCCESS</NS2:ResultSummaryMessage>'
                    +'      </NS2:LookupSiteResponse>'
                    +'  </NS1:Body>'
                    +'</NS1:Envelope>';             
        }else if(isGetZones==true){
                    
          xmlBodyStr='<NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/">'
                     +'  <NS1:Body>'
                     +'    <NS2:GetZonesResponse xmlns:NS2="http://mastermindservice.adt.com/lookup">'
                     +'        <NS2:ErrorNumber>0</NS2:ErrorNumber>'
                     +'      <NS2:ErrorMessage>OK</NS2:ErrorMessage>'
                     +'        <Zones>'
                     +'           <Zone>'
                     +'              <ZoneID>10</ZoneID>'
                     +'              <ZoneState>A</ZoneState>'
                     +'              <ZoneStateDescr>Alarm</ZoneStateDescr>'
                     +'              <EventID>325</EventID>'
                     +'              <EventDescr>BA-WINDOW(S)</EventDescr>'
                     +'              <ZoneComment>MBATH RM 1</ZoneComment>'
                     +'              <GlobalDispID/>'
                     +'              <DisPageNo/>'
                     +'              <RestoreReqdFlag>N</RestoreReqdFlag>'
                     +'              <ZoneToRestore/>'
                     +'           </Zone>'
                     +'           <Zone>'
                     +'              <ZoneID>11</ZoneID>'
                     +'              <ZoneState>A</ZoneState>'
                     +'              <ZoneStateDescr>Alarm</ZoneStateDescr>'
                     +'              <EventID>325</EventID>'
                     +'              <EventDescr>BA-WINDOW(S)</EventDescr>'
                     +'              <ZoneComment>MBATH RM 2</ZoneComment>'
                     +'             <GlobalDispID/>'
                     +'              <DisPageNo/>'
                     +'              <RestoreReqdFlag>N</RestoreReqdFlag>'
                     +'              <ZoneToRestore/>'
                     +'           </Zone>'
                     +'           <Zone>'
                     +'              <ZoneID>12</ZoneID>'
                     +'              <ZoneState>A</ZoneState>'
                     +'              <ZoneStateDescr>Alarm</ZoneStateDescr>'
                     +'              <EventID>325</EventID>'
                     +'              <EventDescr>BA-WINDOW(S)</EventDescr>'
                     +'              <ZoneComment>MBR</ZoneComment>'
                     +'              <GlobalDispID/>'
                     +'              <DisPageNo/>'
                     +'              <RestoreReqdFlag>N</RestoreReqdFlag>'
                     +'              <ZoneToRestore/>'
                     +'           </Zone>'
                     +'           <Zone>'
                     +'              <ZoneID>1</ZoneID>'
                     +'              <ZoneState>A</ZoneState>'
                     +'              <ZoneStateDescr>Alarm</ZoneStateDescr>'
                     +'              <EventID>5</EventID>'
                     +'              <EventDescr>FA-FIRE-SMOKE DET</EventDescr>'
                     +'              <ZoneComment>MBR</ZoneComment>'
                     +'              <GlobalDispID/>'
                     +'              <DisPageNo/>'
                     +'              <RestoreReqdFlag>N</RestoreReqdFlag>'
                     +'              <ZoneToRestore/>'
                     +'           </Zone>'
                     +'           <Zone>'
                     +'              <ZoneID>13</ZoneID>'
                     +'              <ZoneState>A</ZoneState>'
                     +'              <ZoneStateDescr>Alarm</ZoneStateDescr>'
                     +'              <EventID>1504</EventID>'
                     +'              <EventDescr>BA-BURG-DOOR</EventDescr>'
                     +'              <ZoneComment>LIV RM LT</ZoneComment>'
                     +'              <GlobalDispID/>'
                     +'              <DisPageNo/>'
                     +'              <RestoreReqdFlag>N</RestoreReqdFlag>'
                     +'              <ZoneToRestore/>'
                     +'           </Zone>'
                     +'           <Zone>'
                     +'              <ZoneID>14</ZoneID>'
                     +'              <ZoneState>A</ZoneState>'
                     +'              <ZoneStateDescr>Alarm</ZoneStateDescr>'
                     +'              <EventID>1504</EventID>'
                     +'              <EventDescr>BA-BURG-DOOR</EventDescr>'
                     +'              <ZoneComment>LIV RM RT</ZoneComment>'
                     +'              <GlobalDispID/>'
                     +'              <DisPageNo/>'
                     +'              <RestoreReqdFlag>N</RestoreReqdFlag>'
                     +'              <ZoneToRestore/>'
                     +'           </Zone>'
                     +'        </Zones>'
                     +'     </NS2:GetZonesResponse>'
                     +'  </NS1:Body>'
                    +'</NS1:Envelope>';
        
        
        
        }

        
        res.setBody(xmlBodyStr);
            
        res.setStatusCode(200);
        return res;
    }
}