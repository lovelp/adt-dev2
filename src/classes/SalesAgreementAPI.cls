/************************************* MODIFICATION LOG ********************************************************************************************
* SalesAgreementAPI
*
* DESCRIPTION : Containing Request and response for Sales Agreement
*  1. Create
*  2. Update
*  3. Lookup
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                        DATE              TICKET            REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari                 05/18/2019       HRM-9710           Original
* Jitendra Kothari                 07/15/2019       HRM-10409          Enhance Existing Sales Agreement Lookup to Add Fields
* Jitendra Kothari                 08/22/2019       HRM-10982          SFDC & Sales Pilot Integration - Additional Fields
* Jitendra Kothari                 09/06/2019       HRM-11324          Ability to capture existing equipment that customer can bring to the new site  
*/
public without sharing class SalesAgreementAPI {
    public static SalesAgreementSchema.SalesAgreementCreateResponse createSalesAgreement(String oppId, SalesAgreementSchema.SalesAgreementCreateRequest saReq){
        SalesAgreementSchema.SalesAgreementCreateResponse result = new SalesAgreementSchema.SalesAgreementCreateResponse();
        result.status = true;
        Opportunity opp = oppId != null ? getOpportunity(oppId) : new Opportunity(Id = saReq.sfdcOpportunityID);
        try {
            String endpoint = IntegrationSettings__c.getinstance().CreateSalesAgreementURL__c;
            HttpRequest req = prepareRequest(endpoint, 'POST');
            HttpResponse res = new HttpResponse();
            Http h = new Http();
            saReq = saReq !=null ? saReq : getCreateRequest(opp);
            String body = JSON.serialize(saReq, true);
            system.debug('Request: ' + body);
            req.setBody(body);
            opp.CreateSalesAgreementRequestJSON__c = body;
            res = h.send(req);
            system.debug('Response: ' + res);
            if(res != null && String.isNotBlank(res.getBody())){
                if(res.getStatusCode() == 200 ||  res.getStatusCode() == 417){
                    system.debug('Create Response' + res.getBody());
                    try{
                        result = (SalesAgreementSchema.SalesAgreementCreateResponse) System.JSON.deserialize(res.getBody(), SalesAgreementSchema.SalesAgreementCreateResponse.class);
                        if(result.status == true){
                            opp.SalesAgreementId__c = result.result;
                            opp.CreateSalesAgreementResponseJSON__c = res.getBody();
                            update opp;
                        }
                    }catch(DMLException e){
                        ADTApplicationMonitor.log ('Opportunity Update Error', e.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
                        result.status = false;
                        result.message = 'Opportunity Update Error' + e.getmessage();
                    }
                }else if(res.getStatusCode() == 500){
                    result.status = false;
                    result.message = getErrorMessage(res);
                }else{
                    result = (SalesAgreementSchema.SalesAgreementCreateResponse) System.JSON.deserialize(res.getBody(), SalesAgreementSchema.SalesAgreementCreateResponse.class);
                }
            }else{
                result.status = false;
                result.message = res.getStatusCode() + ' - ' + res.getStatus();
            }
        }catch(Exception e){
            ADTApplicationMonitor.log ('Sales Agreement Create Request Exception', e.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
            result.status = false;
            result.message = 'Error: While calling Sales Agreement Create Request - ' + e.getmessage();
            database.update(opp, false);
        }
        return result;
    }
    public static SalesAgreementSchema.SalesAgreementCreateRequest getCreateRequest(Opportunity opp){
        SalesAgreementSchema.SalesAgreementCreateRequest req = new SalesAgreementSchema.SalesAgreementCreateRequest();
        req.PrimarySalesRep = opp.Owner.EmployeeNumber__c;
        req.sfdcOpportunityID = opp.Id;
        req.SalesAgreementName = opp.Name;
        req.companyName = opp.Account.Name;
        req.attention = String.isNotBlank(opp.Account.FirstName__c) ? opp.Account.FirstName__c + ' ' + opp.Account.LastName__c : opp.Account.LastName__c;
        req.customerAddress1 = opp.Account.SiteStreet__c;
        req.customerAddress2 = opp.Account.SiteStreet2__c;
        req.CustomerCity = opp.Account.SiteCity__c;
        req.CustomerState = opp.Account.SiteState__c;
        req.CustomerCounty = opp.Account.SiteCounty__c;
        req.CustomerZip = opp.Account.SitePostalCode__c;
        req.CustomerEmail = opp.Account.Email__c;
        if(string.isNotBlank(opp.Account.Phone)){
            req.CustomerPhone = opp.Account.Phone.replaceAll('\\D', '');
        }
        req.MastermindCustomerNumber = opp.Account.MMBCustomerNumber__c;
        req.mastermindInstanceID = getInstance(opp.Account);
        return req; 
    }
    public static String getInstance(Account acc){
        if(acc.MMBBillingSystem__c == 'MMB'){
            return '2';
        } else {
            return '1';
        }
    }
    public static list<Opportunity> getOpportunity(set<Id> oppIds){
        List<Opportunity> opps = new List<Opportunity>();
        if(!oppIds.isEmpty()){
            opps = [SELECT Id, Name, AccountId, Owner.EmployeeNumber__c, Account.LastName__c, Account.FirstName__c, Account.Phone, Account.SiteStreet__c,
                    Account.SiteStreet2__c, Account.SiteState__c, Account.SitePostalCode__c, Account.SiteCounty__c, Account.SiteCity__c,
                    Owner.Name, Account.Name, StageName, Account.Business_Id__c,
                    Account.Email__c, Account.MMBCustomerNumber__c, Account.MMBBillingSystem__c, Account.SitePostalCodeAddOn__c, SalesAgreementId__c
                    FROM Opportunity WHERE Id IN :oppIds];
        }
        return opps;
    }
    public static Opportunity getOpportunity(String oppId){
        List<Opportunity> opps = getOpportunity(new set<Id>{oppId});
        return opps.isEmpty() ? null : opps.get(0);
    }
    public static SalesAgreementSchema.SalesAgreementLookupResp lookupSalesAgreement(String oppId, SalesAgreementSchema.SalesAgreementLookupRequest saReq){
        SalesAgreementSchema.SalesAgreementLookupResp result = new SalesAgreementSchema.SalesAgreementLookupResp();
        result.status = true;
        try { 
            Opportunity opp = oppId != null ? getOpportunity(oppId) : new Opportunity();
            String endpoint = IntegrationSettings__c.getinstance().LookupSalesAgreementURL__c + saReq.SalesAgreementID;
            HttpRequest req = prepareRequest(endpoint, 'GET');
            HttpResponse res = new HttpResponse();
            Http h = new Http();
            
            saReq = saReq !=null ? saReq : getLookupRequest(opp);
            res = h.send(req);
            if(res != null && String.isNotBlank(res.getBody())){
                system.debug('Lookup Response->' + res.getBody());
                if(res.getStatusCode() == 200 ||  res.getStatusCode() == 417){
                    result = (SalesAgreementSchema.SalesAgreementLookupResp) System.JSON.deserialize(res.getBody(), 
                                            SalesAgreementSchema.SalesAgreementLookupResp.class);
                }else if(res.getStatusCode() == 500){
                    result.status = false;
                    result.message = getErrorMessage(res);
                }else{
                    result = (SalesAgreementSchema.SalesAgreementLookupResp) System.JSON.deserialize(res.getBody(), SalesAgreementSchema.SalesAgreementLookupResp.class);
                }
            } else if(res.getStatusCode() == 404){
                result.status = false;
                //result.message = 'The sales agreement (1234) was not found.';
                String msg = CommercialMessages__c.getValues('SalesAgreementNotFound') !=null ? CommercialMessages__c.getValues('SalesAgreementNotFound').Message__c : 'The sales agreement [SalesAgreementId] was not found.';
                result.message = msg.replace('[SalesAgreementId]', saReq.SalesAgreementID);
            } else{
                result.status = false;
                result.message = res.getStatusCode() + ' - ' + res.getStatus();
            }
        }catch(Exception e){
            ADTApplicationMonitor.log ('Sales Agreement Lookup Request Exception', e.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
            result.status = false;
            result.message = 'Error: While calling Sales Agreement Lookup Request - ' + e.getmessage();
        }
        return result;  
    }
    
    public static String getErrorMessage(HttpResponse res){
        SalesAgreementSchema.ErrorMessage error = (SalesAgreementSchema.ErrorMessage) System.JSON.deserialize(res.getBody(), SalesAgreementSchema.ErrorMessage.class);
        if(error.errors.isEmpty()){
            return res.getStatusCode() + ' - ' + res.getStatus();
        }else{
            return error.errors.get(0).status + ' - ' + error.errors.get(0).errorMessage;
        }
    }
    public static SalesAgreementSchema.SalesAgreementLookupRequest getLookupRequest(Opportunity opp){
        SalesAgreementSchema.SalesAgreementLookupRequest req = new SalesAgreementSchema.SalesAgreementLookupRequest();
        req.SalesAgreementId = opp.SalesAgreementId__c;
        return req;
    }
    
    public static SalesAgreementSchema.SalesAgreementUpdateResponse updateSalesAgreement(String oppId, SalesAgreementSchema.SalesAgreementUpdateRequest saReq){
        SalesAgreementSchema.SalesAgreementUpdateResponse result = new SalesAgreementSchema.SalesAgreementUpdateResponse();
        result.status = true;       
        try { 
            Opportunity opp = oppId != null ? getOpportunity(oppId) : new Opportunity(Id = saReq.sfdcOpportunityID);
            //Start HRM-10677
            //String endpoint = IntegrationSettings__c.getinstance().UpdateSalesAgreementURL__c + saReq.SalesAgreementID + '/' + saReq.sfdcOpportunityID;
            String endpoint = IntegrationSettings__c.getinstance().UpdateSalesAgreementURL__c;
            //End //HRM-10677
            HttpRequest req = prepareRequest(endpoint, 'POST');
            HttpResponse res = new HttpResponse();
            Http h = new Http();
            //Start HRM-10677
            saReq = saReq !=null ? saReq : getUpdateRequest(opp);
            String body = JSON.serialize(saReq, true);
            system.debug('Request: ' + body);
            req.setBody(body);
            opp.UpdateSalesAgreementRequestJSON__c = body;
            //End HRM-10677
            res = h.send(req);
            if(res != null && String.isNotBlank(res.getBody())){
                if(res.getStatusCode() == 200 ||  res.getStatusCode() == 417){
                    system.debug('Udpate Response->' + res.getBody());
                    result = (SalesAgreementSchema.SalesAgreementUpdateResponse) System.JSON.deserialize(res.getBody(), 
                                                                                                     SalesAgreementSchema.SalesAgreementUpdateResponse.class);
                }else if(res.getStatusCode() == 500){
                    result.status = false;
                    result.message = getErrorMessage(res);
                }else{
                    result = (SalesAgreementSchema.SalesAgreementUpdateResponse) System.JSON.deserialize(res.getBody(), SalesAgreementSchema.SalesAgreementUpdateResponse.class);
                }
            }else {
                result.status = false;
                result.message = res.getStatusCode() + ' - ' + res.getStatus();
            }
        }catch(Exception e){
            ADTApplicationMonitor.log ('Sales Agreement Update Request Exception', e.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
            result.status = false;
            result.message = 'Error: While calling Sales Agreement Update Request - ' + e.getmessage();
        }
        return result; 
    }
    
    public static HttpRequest prepareRequest(String endPoint, String method){
        HttpRequest req = new HttpRequest();
        String userName = IntegrationSettings__c.getinstance().DPUsername__c.trim();
        String password = IntegrationSettings__c.getinstance().DPPassword__c.trim();
        Integer timeout = Integer.valueOf(IntegrationSettings__c.getinstance().SalesPointCalloutTimeout__c);
        
        Blob headerValue = Blob.valueOf(userName + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        
        req.setEndpoint(endpoint);
        req.setMethod(method);
        req.setTimeout(timeout);
        return req;
    }
    public static SalesAgreementSchema.SalesAgreementUpdateRequest getUpdateRequest(Opportunity opp){
        SalesAgreementSchema.SalesAgreementUpdateRequest req = new SalesAgreementSchema.SalesAgreementUpdateRequest();
        req.sfdcOpportunityID = opp.Id;
        req.SalesAgreementID = opp.SalesAgreementId__c;
        req.Status = opp.StageName == 'Closed Lost' ? 'Lost' : null;//HRM-10677
        return req; 
    }
    
    public static String getOppStageName(String value){
        SalesAgreementStages__c val = SalesAgreementStages__c.getValues(value); 
        return val != null ? val.Value__c : null;
    }
    public static Id getRepUserId(String repId){
        list<User> users = new list<User>();
        if(String.isNotBlank(repId)){
            users = [SELECT Id FROM User WHERE EmployeeNumber__c = :repId];
        }
        return users.isEmpty() ? null : users.get(0).Id;
    }
    public static String getBillingInstance(Integer mastermindInstanceID){
        if(mastermindInstanceID == 2){
            return 'MMB';
        } else {
            return 'P1MMB';
        }
    }
    public static void populateOppFromResponse(SalesAgreementSchema.SalesAgreementLookupResponse response, Opportunity opp){
    	populateOppFromResponse(response, opp, 'UI');
    }
    public static void populateOppFromResponse(SalesAgreementSchema.SalesAgreementLookupResponse response, Opportunity opp, String mode){
        if(response != null){
            if(String.isBlank(mode)) mode = 'UI';
            populateOpp(response, opp);
            //moved out due to query
		    Id repId = getRepUserId(response.primarySalesRepID);
		    if(repId != null){
		        opp.OwnerId = repId;
		    }
		    Account acc = [SELECT Id, Business_Id__c FROM Account WHERE Id = :opp.AccountId];
            acc = updateAccountFields(response, acc, mode);
            populateSiteAndSystemDesign(response, opp);
            update acc;
            update opp;
        }
    }
    public static void populateOpp(SalesAgreementSchema.SalesAgreementLookupResponse response, Opportunity opp){
        opp.Installation_Revenue__c = response.InstallCharge;
        //opp.SalesAgreement_LastLookupRun__c = system.now();
        opp.RMR__c = response.sellingTotalRMR;
        opp.SalesStatus__c = response.SaleStatus;
        opp.AgreementPhase__c = response.AgreementPhase;
        //Code changes done to not allow optystage to become null
        String optyStage = getOppStageName(response.AgreementPhase);
        if(String.isNotBlank(optyStage)){
            opp.StageName = optyStage;
        }
        //opp.StageName = getOppStageName(response.AgreementPhase);
        if(String.isNotBlank(response.salesAgreementName)){
            opp.Name =  response.salesAgreementName;
        }
        opp.NumberofSites__c = response.numberOfSites;
        if(response.lastModified != null){
            opp.SalesAgreement_LastModified__c = Datetime.valueOfGmt(response.lastModified.replace('T', ' '));
        }
    }
    public static Site__c populateSite(SalesAgreementSchema.Sites sys, Id oppId){
        Site__c site = new Site__c();
        site.SiteID__c = sys.siteID;
        site.Name = sys.siteName;
        site.SiteTypeName__c = sys.siteTypeName;
        site.SiteEmail__c = sys.siteEmail;
        site.Opportunity__c = oppId;
        site.SitePhone__c = sys.sitePhone;
        site.SiteStatus__c = sys.siteStatus;
        site.MMBSiteNo__c = sys.mastermindSiteNumber;
        site.SiteZip__c = sys.siteZip;
        site.SiteAddress1__c = sys.siteAddress1;
        site.SiteAddress2__c = sys.siteAddress2;
        site.SiteCity__c = sys.siteCity;
        site.SiteCounty__c = sys.siteCounty;
        site.SiteState__c = sys.siteState;
        return site;
    }
    public static System_Design__c populateSystemDesign(SalesAgreementSchema.SystemDesigns sysDes, Id oppId){
        System_Design__c design = new System_Design__c();
        design.SystemDesignId__c = sysDes.systemDesignId;
        design.Name = sysDes.designName;
        design.SystemDesignTypeName__c = sysDes.designTypeName;
        design.Installation_Revenue__c  = sysDes.installCharge;
        design.RMR__c  = sysDes.sellingRMR;
        design.Opportunity__c  = oppId;
        design.IncludedOnContract__c  = sysDes.includedOnContract;
        design.MMBJob__c = sysDes.mastermindJobNumber;
        design.Ownership__c = sysDes.ownership;
        return design;
    }
    public static void populateSiteAndSystemDesign(SalesAgreementSchema.SalesAgreementLookupResponse response, Opportunity opp){
        if(response.sites != null && !response.sites.isEmpty()){
            list<Site__c> siteList = new list<Site__c>();
            set<String> siteIds = new set<String>();
            list<System_Design__c> designList = new list<System_Design__c>();
            set<String> designIds = new set<String>();
            for(SalesAgreementSchema.Sites sys : response.sites){
                Site__c site = populateSite(sys, opp.Id);
                siteList.add(site);
                siteIds.add(sys.siteID);
                if(sys.systemDesigns != null && !sys.systemDesigns.isEmpty()){
                    for(SalesAgreementSchema.SystemDesigns sysDes : sys.systemDesigns){
                        System_Design__c design = populateSystemDesign(sysDes, opp.Id);
                        //design.Site__c  = SiteIdToSite.get(sys.siteID);
                        design.SiteId__c = sys.siteID;
                        designList.add(design);
                        designIds.add(sysDes.systemDesignId);
                    }
                }
            }
            
            if(!siteList.isEmpty()){
                upsert siteList SiteID__c;
            }
            map<String, Id> SiteIdToSite = new map<String, Id>();
            for(Site__c site : siteList){
                SiteIdToSite.put(site.SiteID__c, site.Id);
            }
            
            /*for(SalesAgreementSchema.Sites sys : response.sites){
                if(sys.systemDesigns != null && !sys.systemDesigns.isEmpty()){
                    for(SalesAgreementSchema.SystemDesigns sysDes : sys.systemDesigns){
                        System_Design__c design = populateSystemDesign(sysDes);
                        design.Site__c  = SiteIdToSite.get(sys.siteID);
                        design.SiteId__c = sys.siteID;
                        designList.add(design);
                        designIds.add(sysDes.systemDesignId);
                    }
                }
            }*/
            for(System_Design__c design : designList){
                design.Site__c = SiteIdToSite.get(design.SiteID__c);
            }
            list<Site__c> oldSiteList = [SELECT Id FROM Site__c WHERE Opportunity__c = :opp.Id AND SiteID__c NOT IN :siteIds];
            
            list<System_Design__c> oldDesignList = [SELECT Id FROM System_Design__c 
                                                    WHERE (Site__r.SiteID__c IN :siteIds AND SystemDesignId__c NOT IN :designIds) 
                                                            OR Site__c IN :oldSiteList];
            if(!oldDesignList.isEmpty()) delete oldDesignList;
            if(!oldSiteList.isEmpty()) {
                delete oldSiteList;
            }
            if(!designList.isEmpty()){
                upsert designList SystemDesignId__c;
            }
        }
    }
    public static Account updateAccountFields(SalesAgreementSchema.SalesAgreementLookupResponse response, Account acc, String mode){
        //Account acc = new Account(Id = opp.AccountId);
        //Account acc = [SELECT Id, Business_Id__c FROM Account WHERE Id = :opp.AccountId];
        //Account acc = opp.Account;
        if(String.isNotBlank(response.companyName)){
            acc.Name = response.companyName;
        }
        system.debug('$$' + response.attention);
        if(String.isNotBlank(response.attention)){
            String accname = response.attention.trim();
            if(String.isNotBlank(accname)){
                Integer firstSpace = accname.indexOf(' ', 0);
                acc.FirstName__c = accname.substringBefore(' ');
                acc.LastName__c = accname.substringAfter(' ');
            }
        }
        if(String.isNotBlank(response.CustomerEmail)){
            acc.Email__c = response.CustomerEmail;
        }
        if(String.isNotBlank(response.CustomerPhone)){
            acc.Phone = response.CustomerPhone;
        }
        if(String.isNotBlank(response.MastermindCustomerNumber)){
            acc.MMBCustomerNumber__c = response.MastermindCustomerNumber;
            acc.MMBBillingSystem__c = getBillingInstance(response.mastermindInstanceID);
        }
        if(String.isNotBlank(getAddressWithComma(response.customerAddress1, response.customerAddress2, response.CustomerCity, response.CustomerState, null, 
                        response.CustomerZip))){
                            
            updateAcc(acc, response.customerAddress1, response.customerAddress2, response.CustomerCity, response.CustomerState, 
                        response.CustomerZip, mode);
        }
        return acc;
    }
    public static String getAddressWithComma(String custAdd1, String custAdd2, String custCity, String custState, String custCounty, String custZip){
        list<String> addSet = new list<String>();
        if(String.isNotBlank(custAdd1)){
            addSet.add(custAdd1);
        }
        if(String.isNotBlank(custAdd2)){
            addSet.add(custAdd2);
        }
        if(String.isNotBlank(custCity)){
            addSet.add(custCity);
        }
        if(String.isNotBlank(custState)){
            addSet.add(custState);
        }
        if(String.isNotBlank(custCounty)){
            addSet.add(custCounty);
        }
        if(String.isNotBlank(custZip)){
            addSet.add(custZip);
        }
        return String.join(addSet, ', ');
    }
    public static void updateAcc(Account acc,String street1, String street2, String city, String state, String postalCode, String mode){
        try{
            if(acc != null){
                MelissaDataAPI.MelissaDataResults melissaResponse = new MelissaDataAPI.MelissaDataResults();
                //check if address has lat, lon, postal code.
                String uniqueOriginalAddress =street1+'+';
                uniqueOriginalAddress += String.isnotBlank(street2) ? street2+'+'+city+'+'+state+'+'+postalCode :city+'+'+state+'+'+postalCode;
                List<Address__c> addrResponseList = [SELECT Id, OriginalAddress__c, Validated__c, Street__c, Street2__c, StreetNumber__c, StreetName__c, CHS__c,
                                                         City__c, County__c, State__c, PostalCode__c, PostalCodeAddOn__c, CountryCode__c, Latitude__c, Longitude__c 
                                                         FROM Address__c 
                                                         WHERE OriginalAddress__c = :uniqueOriginalAddress];
                //if no address exists
                if(addrResponseList.size() == 0){
                    if(mode == 'UI'){
                        //melissa call
                        melissaResponse = MelissaDataAPI.validateAddr(street1, street2 !=null ? street2: '', city, state, postalCode);
                    }else if(mode == 'SCRIPT'){
                        //No melissa call
                        melissaResponse = melissaResponseWithoutCall(street1, street2 !=null ? street2: '', city, state, postalCode);
                    }
                    if(melissaResponse != null){
                        uniqueOriginalAddress ='';
                        uniqueOriginalAddress = melissaResponse.Addr1+'+';
                        String address2 = melissaResponse.Addr2;
                        if (String.isNotBlank(melissaResponse.Suite)){
                            address2 = melissaResponse.Suite;
                        }
                        uniqueOriginalAddress +=String.isnotBlank(address2) ? address2 + '+' +melissaResponse.City+ '+' +melissaResponse.State+'+'+melissaResponse.Zip : melissaResponse.City+ '+'+ melissaResponse.State+ '+'+ melissaResponse.Zip;
                        //Query the address
                        addrResponseList = [SELECT Id, OriginalAddress__c,Street__c,Validated__c ,Street2__c,StreetNumber__c,StreetName__c,CHS__c,
                                            City__c,County__c,State__c,PostalCode__c,PostalCodeAddOn__c,CountryCode__c,
                                            Latitude__c,Longitude__c FROM Address__c WHERE OriginalAddress__c =: uniqueOriginalAddress];
                        if(addrResponseList.size() == 0){
                            //Create Lead and Address with melissa data
                            addrResponseList.add(createAdress(melissaResponse,street1,street2,city,state,postalCode));
                        }else{                           
                            //address exists after melissa validation so assign address addrResponseList[0] to the new lead
                            if(String.isBlank(addrResponseList[0].PostalCode__c)||String.isBlank(addrResponseList[0].PostalCodeAddOn__c) ||addrResponseList[0].Latitude__c==NULL||addrResponseList[0].Longitude__c==NULL|| String.isBlank(addrResponseList[0].City__c) || String.isBlank(addrResponseList[0].State__c)|| String.isBlank(addrResponseList[0].CountryCode__c)|| String.isBlank(addrResponseList[0].County__c)){
                                melissaResponse = checkForMelissaData(addrResponseList[0], mode);
                                addrResponseList[0] = updateInvalidAddress(melissaResponse,addrResponseList[0]);
                            } 
                        }
                    }
                }
                
                //address exists in the system
                else{
                    //assign address addrResponseList[0] to the new lead
                    if(String.isBlank(addrResponseList[0].PostalCode__c)||String.isBlank(addrResponseList[0].PostalCodeAddOn__c) ||addrResponseList[0].Latitude__c==NULL||addrResponseList[0].Longitude__c==NULL|| String.isBlank(addrResponseList[0].City__c) || String.isBlank(addrResponseList[0].State__c)|| String.isBlank(addrResponseList[0].CountryCode__c)|| String.isBlank(addrResponseList[0].County__c)){
                      melissaResponse = checkForMelissaData(addrResponseList[0], mode);
                      addrResponseList[0] = updateInvalidAddress(melissaResponse, addrResponseList[0]);
                    }
                }
                acc.AddressID__c = addrResponseList[0].Id;
                for(Postal_Codes__c pList : [Select id,Name from Postal_Codes__c where BusinessId__c =: acc.Business_Id__c AND Name =: addrResponseList[0].PostalCode__c limit 1]){
                    acc.PostalCodeID__c = pList.id;
                }
            }
            
        }catch(Exception ae){
            ADTApplicationMonitor.log ('Sales pilot request', ae.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
        }
    }
    public static Address__c createAdress(MelissaDataAPI.MelissaDataResults mdr,String street1, String street2, String city, String state, String postalCode){
        Boolean error = false;
        Boolean didOverride = true;
        Address__c addr = new Address__c();
        // Set Addr
        addr.Street__c = mdr != null ? mdr.Addr1 : street1;
        String address2 = mdr!=null? mdr.Addr2:street2 ;
        if (mdr!=null && String.isNotBlank(mdr.Suite)){
            address2 = mdr.Suite;
        }
        addr.Street2__c = String.isNotBlank(address2) ? address2 : String.isNotBlank(street2)? street2 :'';
        addr.StreetNumber__c = mdr != null ? mdr.StreetNumber: '';
        addr.StreetName__c = mdr != null ? mdr.StreetName : '';
        addr.City__c = mdr != null ? mdr.City :String.isNotBlank(city) ? city : '';
        addr.County__c = mdr != null ? mdr.County :'';
        addr.State__c = mdr != null ?  mdr.State :String.isNotBlank(state) ? state : '';
        addr.PostalCode__c = (mdr != null && String.isNotBlank(mdr.Zip))? mdr.Zip : String.isNotBlank(postalCode) ? postalCode : '';
        addr.PostalCodeAddOn__c = mdr != null ? mdr.Plus4 :'';
        addr.CountryCode__c = (mdr != null && String.isNotBlank(mdr.CountryCode)) ? mdr.CountryCode : '';
        addr.Latitude__c = mdr != null ? decimal.valueOf(mdr.Lat) : 0.00;
        addr.Longitude__c = mdr != null ? decimal.valueOf(mdr.Lon) : 0.00;
        addr.OriginalAddress__c = mdr != null ? mdr.Addr1+'+':street1+'+';
        if(mdr != null){
            addr.Validated__c = true;
            String errorFlag = '';
            String melissaAddress2 = mdr.Addr2;
            if (String.isNotBlank(mdr.Suite)){
                melissaAddress2 = mdr.Suite;
            }
            addr.OriginalAddress__c += String.isnotBlank(melissaAddress2) ? melissaAddress2 + '+' +mdr.City+ '+' +mdr.State+'+'+mdr.Zip : mdr.City+ '+'+ mdr.State+ '+'+ mdr.Zip;
            for (String code : mdr.ResultCodes){
                if (code.containsIgnoreCase('AE')){
                    error = true;
                    errorFlag = 'Error:'+code;
                    break;
                }
            }     
        }else {
            addr.OriginalAddress__c += String.isnotBlank(street2) ? street2 + '+' +city+ '+' +state+'+'+postalCode : city+ '+'+ state+ '+'+ postalCode;
        }
        addr.StandardizedAddress__c = mdr != null ? mdr.Addr1+',' :street1 + ',';
        if(mdr != null){
            String meliAddress2 = mdr.Addr2;
            if (String.isNotBlank(mdr.Suite)){
                meliAddress2 = mdr.Suite;
            }
            addr.StandardizedAddress__c += String.isnotBlank(meliAddress2) ?meliAddress2 + ',' +mdr.City+ ',' +mdr.State+','+mdr.Zip : mdr.City+ ','+ mdr.State+ ','+ mdr.Zip;
        }else {
            addr.StandardizedAddress__c += String.isnotBlank(street2) ? street2 + ',' +city+ ',' +state+','+postalCode: city+ ','+ state+ ','+ postalCode;
        }
        //override codes
        if (error && didOverride) {
            addr.ValidationMsg__c = 'User Override';
            for (String code: mdr.ResultCodes) {
                addr.ValidationMsg__c += ':' + code.substring(0,4);
            }
        }
        addr.StandardizedAddress__c = addr.StandardizedAddress__c.toUpperCase();
        addr.OriginalAddress__c = addr.OriginalAddress__c.toUpperCase();         
        insert addr;
        return addr;
    }
    public static Address__c updateInvalidAddress(MelissaDataAPI.MelissaDataResults mdr, Address__c addr){
        Boolean error = false;
        Boolean didOverride = false;
        if(mdr != null){
            addr.PostalCode__c = mdr.Zip;
            addr.City__c = mdr.City;
            addr.State__c = mdr.State;
            addr.CountryCode__c =mdr.CountryCode;
            addr.County__c =mdr.County;
            addr.Latitude__c=Decimal.valueof(mdr.Lat);
            addr.Longitude__c=Decimal.Valueof(mdr.Lon);
            addr.PostalCodeAddOn__c=mdr.Plus4;
            addr.Validated__c = true;
            for (String code : mdr.ResultCodes){
                if (code.startsWith('AE')){
                    error = true;
                    didOverride = true;
                }
            }
            if (error && didOverride) {
                addr.ValidationMsg__c = 'User Override';
                for (String code: mdr.ResultCodes) {
                    addr.ValidationMsg__c += ':' + code.substring(0,4);
                }
            }
            update addr;
        }
        return addr;
    } 
    // melissa data check
    public static MelissaDataAPI.MelissaDataResults checkForMelissaData(Address__c addr, String mode){
        MelissaDataAPI.MelissaDataResults melissaResponse = new MelissaDataAPI.MelissaDataResults();
        //check if reqAddr is null
        if(addr != null){
            //melissaResponse = MelissaDataAPI.validateAddr(addr.Street__c, String.isNotBlank(addr.Street2__c)? addr.Street2__c : '', addr.City__c, addr.State__c,addr.PostalCode__c);
            if(mode == 'UI'){
                //melissa call
                melissaResponse = MelissaDataAPI.validateAddr(addr.Street__c, String.isNotBlank(addr.Street2__c)? addr.Street2__c : '', addr.City__c, addr.State__c,addr.PostalCode__c);
            }else if(mode == 'SCRIPT'){
                //No melissa call
                melissaResponse = melissaResponseWithoutCall(addr.Street__c, String.isNotBlank(addr.Street2__c)? addr.Street2__c : '', addr.City__c, addr.State__c,addr.PostalCode__c);
            }
        }
        return melissaResponse;
    }
    public static MelissaDataAPI.MelissaDataResults melissaResponseWithoutCall(String addr1, String addr2, String city, String state, String zip){
        MelissaDataAPI.MelissaDataResults melissaResponse = new MelissaDataAPI.MelissaDataResults();
        /*melissaResponse.ResultCodes;
        melissaResponse.AddrKey;
        melissaResponse.Suite;*/
        melissaResponse.Addr1 = addr1;
        melissaResponse.Addr2 = addr2;
        melissaResponse.City = city;
        melissaResponse.State = state;
        melissaResponse.Zip = zip;
        /*melissaResponse.Plus4;
        melissaResponse.StreetName;
        melissaResponse.StreetNumber;
        melissaResponse.County;
        melissaResponse.CountryCode;
        melissaResponse.Lat;
        melissaResponse.Lon;
        melissaResponse.UTC;
        melissaResponse.DeliveryIndicator;
        melissaResponse.UrbanizationName;*/
        return melissaResponse;
    }
}