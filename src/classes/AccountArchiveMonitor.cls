/************************************* MODIFICATION LOG ********************************************************************************************
* AccountArchiveMonitor
*
* DESCRIPTION : A schedulable class that initiates the batch classes PendingDiscoArchiveBatch
*               and RifActivatorBatch
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner             		10/17/2012			- Original Version
*
*													
*/

global class AccountArchiveMonitor implements Schedulable {
	
   global void execute(SchedulableContext SC) {
		Id batchProcessId1 = Database.executebatch(new PendingDiscoArchiveBatch(), 100);
		
		Id batchProcessId2 = Database.executebatch(new RifActivatorBatch(), 100);
		
   }
}