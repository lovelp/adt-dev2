/************************************* MODIFICATION LOG ********************************************************************************************
* CustomerWebInterestsAPI
*
* DESCRIPTION : API Class.
*
* 
* Jitendra Kothari				10/24/2019			HRM-10967			Configurator Cheetahmail Requirement
*/
@isTest
public class CustomerWebInterestsAPITest {
    
    static testmethod void WebInterestRequest() {
        CustomerWebQuestion__c objcust =new CustomerWebQuestion__c();
        objcust.Question__c = 'Test Question';
        insert objcust;
        
        Test.startTest();
        
        CustomerWebInterestsAPI.WebInterestRequest wrap = new CustomerWebInterestsAPI.WebInterestRequest();
        wrap.uniqueVisitorId = '12345';
        wrap.configuratorVersion = '1.1';
        wrap.friendlyId = '12343';
        wrap.email = 'test@tes.com';
        wrap.uniqueId = '6854';
        wrap.sequenceId = '1';
        wrap.question = 'Test Question';
        wrap.answer = 'Answer Aswer';
        String reqBody = JSON.serialize(wrap);
        
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri ='https://cs13.salesforce.com/services/apexrest/processWebInterest';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(reqBody);
        
        RestContext.request = request;
        RestContext.response = response;
        CustomerWebInterestsAPI.processRequest();
        system.assertEquals(response.statusCode, 200);
        system.assertEquals(response.responseBody, Blob.valueOf('{"message":"Web interest saved."}'));
        
        list<CustomerInterestConfig__c> cuslist = [SELECT id, UniqueQuestionId__c, Type__c, Response__c 
        												FROM CustomerInterestConfig__c 
		                                               WHERE UniqueVisitorNumber__c =:wrap.uniqueVisitorId
		                                               AND UniqueQuestionId__c = :wrap.uniqueId];
        
        system.assertEquals(cuslist.isEmpty(), false);
        system.assertEquals(cuslist.size(), 1);
        system.assertEquals(cuslist.get(0).Type__c, 'Question');
        system.assertEquals(cuslist.get(0).Response__c, 'Answer Aswer');
        
        wrap.uniqueVisitorId = '12345';
        wrap.configuratorVersion = '1.1';
        wrap.friendlyId = '12343';
        wrap.email = 'test@tes.com';
        wrap.uniqueId = '6854';
        wrap.sequenceId = '1';
        wrap.question = 'Test Question';
        wrap.answer = 'Answer Answer';
        reqBody = JSON.serialize(wrap);
        request.requestBody = Blob.valueof(reqBody);
        
        RestContext.request = request;
        RestContext.response = response;
        CustomerWebInterestsAPI.processRequest();
        
        system.assertEquals(response.statusCode, 200);
        system.assertEquals(response.responseBody, Blob.valueOf('{"message":"Web interest saved."}'));
        
        cuslist = [SELECT id, UniqueQuestionId__c, Type__c, Response__c 
        			FROM CustomerInterestConfig__c 
	                   WHERE UniqueVisitorNumber__c =:wrap.uniqueVisitorId
	                   AND UniqueQuestionId__c = :wrap.uniqueId];
        
        system.assertEquals(cuslist.isEmpty(), false);
        system.assertEquals(cuslist.size(), 1);
        //system.assertEquals(cuslist.get(0).Type__c, 'Question');
        system.assertEquals(cuslist.get(0).Response__c, 'Answer Answer');
        
        wrap.uniqueVisitorId = '12345';
        wrap.configuratorVersion = '1.1';
        wrap.friendlyId = '12343';
        wrap.email = 'test@tes.com';
        wrap.uniqueId = '6855';
        wrap.sequenceId = '2';
        wrap.question = 'Email Details';
        wrap.answer = 'CallNowButtonFlag:TRUE,TemplateID:email-pc-option-2,Protect:testProtect,KeyProduct1:KP1,KeyProduct2:KP2,TollFree:1234567890,HeroImage:H3,GroupProduct1:P-1,GroupProduct2:P-4,GroupProduct3:P-9,GroupProduct4:P-5,PersonalCode:12343';
        reqBody = JSON.serialize(wrap);
        request.requestBody = Blob.valueof(reqBody);
        
        RestContext.request = request;
        RestContext.response = response;
        CustomerWebInterestsAPI.processRequest();
        
        system.assertEquals(response.statusCode, 200);
        system.assertEquals(response.responseBody, Blob.valueOf('{"message":"Web interest saved."}'));
        cuslist = [SELECT id, UniqueQuestionId__c, Type__c 
        				FROM CustomerInterestConfig__c 
	                       WHERE UniqueVisitorNumber__c =:wrap.uniqueVisitorId
	                       AND UniqueQuestionId__c = :wrap.uniqueId];
        
        system.assertEquals(cuslist.isEmpty(), false);
        system.assertEquals(cuslist.size(), 1);
        system.assertEquals(cuslist.get(0).Type__c, 'Email Details');
        
        list<RequestQueue__c> rqList = [SELECT Id, ServiceTransactionType__c, RequestStatus__c, MessageID__c, counter__c 
        									FROM RequestQueue__c 
        									WHERE Customer_Web_Interest__c = :cuslist.get(0).Id];
        system.assertEquals(rqList.isEmpty(), false);
        system.assertEquals(rqList.size(), 1);
        system.assertEquals(rqList.get(0).ServiceTransactionType__c, 'sendPCEmail');
        system.assertEquals(rqList.get(0).RequestStatus__c, 'Ready');
        system.assertEquals(rqList.get(0).MessageID__c, wrap.email);
        system.assertEquals(rqList.get(0).counter__c, 0);
        
        Test.setMock(HttpCalloutMock.class, new ADTWEBLEADSMockTest());  
        ADTWebLeadsAPI.processWebleadsRequestQueue('sendPCEmail');
        rqList = [SELECT Id, ServiceTransactionType__c, RequestStatus__c, MessageID__c, counter__c 
        									FROM RequestQueue__c 
        									WHERE Customer_Web_Interest__c = :cuslist.get(0).Id];
        system.assertEquals(rqList.isEmpty(), false);
        system.assertEquals(rqList.size(), 1);
        system.assertEquals(rqList.get(0).MessageID__c, wrap.email);
        system.assertEquals(rqList.get(0).counter__c, 0);
        
        Test.stopTest();
    }
    
    static testmethod void WebInterestRequest2() {
    	list<PartnerAPIMessaging__c> pmList = new list<PartnerAPIMessaging__c>();
    	PartnerAPIMessaging__c pm500 = new PartnerAPIMessaging__c(Name = '500');
        pm500.Error_Message__c = 'Something went wrong while processing the request. Please try again later.';
        pmList.add(pm500);
        
        /*PartnerAPIMessaging__c pm400 = new PartnerAPIMessaging__c(Name = '400');
        pm400.Error_Message__c = 'Something went wrong while processing the request. Please try again later.';
        pmList.add(pm400);*/
        insert pmList;
        
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri ='https://cs13.salesforce.com/services/apexrest/processWebInterest';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        
        RestContext.request = request;
        RestContext.response = response;
        CustomerWebInterestsAPI.processRequest();
        
        system.assertEquals(response.statusCode, 400);
        system.assertEquals(response.responseBody, Blob.valueOf(CustomerWebInterestsAPI.errorResponse('JSON request is empty.',400)));
        
        CustomerWebInterestsAPI.WebInterestRequest wrap = new CustomerWebInterestsAPI.WebInterestRequest();
        wrap.configuratorVersion = '1.1';
        wrap.friendlyId = '12343';
        wrap.email = 'test@tes.com';
        wrap.uniqueId = '6854';
        wrap.sequenceId = '21';
        wrap.question = 'Test Question';
        wrap.answer = 'Answer Aswer';
        String reqBody = JSON.serialize(wrap);
        request.requestBody = Blob.valueof(reqBody);
        
        RestContext.request = request;
        RestContext.response = response;
        CustomerWebInterestsAPI.processRequest();
        
        system.assertEquals(response.statusCode, 400);
        system.assertEquals(response.responseBody, Blob.valueOf(CustomerWebInterestsAPI.errorResponse(PartnerAPIMessaging__c.getinstance('500').Error_Message__c,500)));
        
        wrap.uniqueVisitorId = '12345';
        reqBody = JSON.serialize(wrap);
        request.requestBody = Blob.valueof(reqBody);
        
        RestContext.request = request;
        RestContext.response = response;
        CustomerWebInterestsAPI.processRequest();
        system.assertEquals(response.statusCode, 200);
        system.assertEquals(response.responseBody, Blob.valueOf('{"message":"Web interest saved."}'));
        
        Test.stopTest();
    }
}