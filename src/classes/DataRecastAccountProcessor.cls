/************************************* MODIFICATION LOG ********************************************************************************************
* DataRecastAccountProcessor
*
* DESCRIPTION : Update accounts from phase 1
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* SahilGrover				  4/2/2012				- Original Version
*
*													
*/

global class DataRecastAccountProcessor implements Database.Batchable<sObject>, Database.Stateful {

	// the disposition code values are from generated code

	public String query = 'Select ID, Channel__c, Business_ID__c, ProcessingType__c, ' +
						  ' SiteCountryCode__c, CreatedDate, DispositionCode__c, DispositionDetail__c, ' +
						  ' BillingSystem__c, LeadExternalId__c, TransitionDate1__c ' +
						  ' from Account ' +
						  ' Where (DispositionCode__c IN (\'BN - Bad Phone Number\',\'CB - Call Back\',\'IA - Insufficient Address\',\'IA - Insufficient Address\',\'LB - Left Business Card\',\'LB - Left Business Card\',\'LC - Lost to Competition\',\'LC - Lost to Competition\',\'LM - Left Message\',\'MC - Mailed Postcard\',\'ML - Mailed Letter\',\'ML - Mailed Letter\',\'NI - No Interest\',\'NL - No Phone Line\',\'NR - No Mail Receptacle\',\'NS - No Such Address\',\'NS - No Such Address\',\'PP - Pending Proposal\',\'PP - Pending Proposal\',\'RA - Rescheduled Appointment\',\'RA - Rescheduled Appointment\',\'RT - Returned Mail No Reason\',\'RT - Returned Mail No Reason\',\'SA - Sold / Activated\',\'SA - Sold / Activated\',\'SC - Scheduled Appointment\',\'SD - Sold\',\'SD - Sold\',\'TO - Tagged Occupied\',\'UA - Undeliverable As Addressed\') ' +
						  ' or Channel__c = null or ProcessingType__c = null or TransitionDate1__c = null or BillingSystem__c = null ' +
						  ') ';
	
	public DataRecastAccountProcessor() { }

	public DataRecastAccountProcessor(String DataSource) {
		if(DataSource.toLowerCase() == 'admin')
		{
			query = query + ' and data_source__c = \'ADMIN\'';
		}
		else if (DataSource.toLowerCase() == 'budco')
		{
			query = query + ' and data_source__c = \'BUDCO\'';
		}
		else if (DataSource.toLowerCase() == 'informix')
		{
			query = query + ' and data_source__c = \'Informix\'';
		}
		else if (DataSource.toLowerCase() == 'broadview')
		{
			query = query + ' and data_source__c = \'Broadview\'';
		}
	}

	global Database.QueryLocator start( Database.Batchablecontext bc ) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext bc, List<sObject> scope) {
		DataRecastHelper.processAccounts((List<Account>)scope);
		update scope;
	}
	
	global void finish(Database.BatchableContext bc) {
		
	}

}