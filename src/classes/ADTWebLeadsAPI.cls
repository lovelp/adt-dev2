/************************************* MODIFICATION LOG ********************************************************************************************
 * ADTWebLeadAPI - check if webleads type is contact or lead ,then process record accordingly
 *
 * DESCRIPTION : Rest URI to recieve the json from DataPower and insert an audit log
 *               
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                        DATE              TICKET            REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Parameswaran Iyer              08/06/2018          HRM - 5709        - Original Version
 * Siddarth Asokan                08/20/2018          HRM - 5709        - Lead flow
 * Srinivas Yaramsetti            10/15/2018          HRM - 5709        - Code review and take over.
 * Srinivas Yaramsetti            12/01/2018          HRM - 8636        - Canada Webleads need to identify Canda Postal codes
 * Siddarth Asokan                04/09/2019          ECOM - 967        - processWebleadsRequestQueue method created to call from Integration Server
 * Viraj and Abhinav              06/29/2019          HRM - 10106       - EWC and send text
 * Viraj and Abhinav              09/29/2019          HRM - 10437       - API call to PossibleNow and Gryphon
 * Jitendra Kothari				  10/17/2019		  HRM-10967			- Configurator Cheetahmail Requirement
 */

@RestResource(urlMapping='/createWebLead/*')
global with sharing class ADTWebLeadsAPI{
    //Srini - do not require get set
    public  static String errorMsgTextArea;
    
    //Wrapper class to store JSON data
    public class DataPowerRequestInfo{
        public DataInfo data;
    }
    
    //Wrapper class to store info contained inside Data
    public class DataInfo{
        public String formName;   
        public String promotionCode;
        public String messageDivision;
        public String messageType;
        public String emailSubject;
        public String firstName;
        public String lastName;
        public String businessName;
        public String streetAddress;
        public String city;
        public String stateCode;
        public String countryCode;
        public String zipCode;
        public String primaryPhoneNumber;
        public String secondaryPhoneNumber;
        public String emailAddress;
        public String ownershipCode;
        public String sourceIP;
        public String createDateTime;
        public List<UDFParamsInfo> udf;
    }
    
    //Wrapper class to store UDFParams
    public class UDFParamsInfo{
        public String tag;   
        public String value;
    }
    
    /*
     * Method name : formatRequestField
     * Parameters : String strField
     * Return Type: String
     * Send email to the contact processed
     */

    public static String formatRequestField(String strField){
        return ((strField!=null)?strField.trim():'');
    }


    /*
     * Method name : dopostmethod
     * Parameters : None
     * Return Type: Void
     * Accepts HTTP Post request and inserts auditlog record based on weblead validation
     */

    @HttpPost
    global static void dopostmethod(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        system.debug('##Header param'+req);
        res.addHeader('Content-Type','application/json');
        try{ 
            if(!String.isBlank(req.requestBody.toString())){
                DataPowerRequestInfo dataPowerReqInfoWrapInstance= (DataPowerRequestInfo)JSON.deserialize(req.requestBody.toString(), DataPowerRequestInfo.class);
                Map<String, String> reqparam = req.params; 
                ADTWebLeadsController webLeadsController = new ADTWebLeadsController();
                AuditLog__c auditLogObj = new AuditLog__c();
                Boolean isError = false;
                String errorMsg = '';
                auditLogObj.name = formatRequestField(dataPowerReqInfoWrapInstance.data.formName)+'_'+System.now();
                auditLogObj.FormName__c = formatRequestField(dataPowerReqInfoWrapInstance.data.formName);
                String dnisCode = formatRequestField(dataPowerReqInfoWrapInstance.data.promotionCode);
                // Name
                String firstName = formatRequestField(dataPowerReqInfoWrapInstance.data.firstName);
                String lastName = formatRequestField(dataPowerReqInfoWrapInstance.data.lastName);
                String companyName = formatRequestField(dataPowerReqInfoWrapInstance.data.businessName);
                auditLogObj.FirstName__c = firstName;
                auditLogObj.LastName__c = String.isNotBlank(lastName)?lastName:firstName;
                auditLogObj.BusinessName__c = companyName;
                // Name Errors
                Integer totalNameLength = firstName.length() + lastName.length();
                if(totalNameLength > Integer.valueOf(ResaleGlobalVariables__c.getInstance('MMBNameLength').Value__c)){
                    isError = true;
                    errorMsg = 'First Name and last name is greater than '+ResaleGlobalVariables__c.getInstance('MMBNameLength').Value__c+':'+firstName+' '+lastName;
                }
                if(String.isNotBlank(companyName) && companyName.length() > Integer.valueOf(ResaleGlobalVariables__c.getInstance('MMBAccountNameLength').Value__c)){
                    isError = true;
                    errorMsg += 'Company name is greater than '+ResaleGlobalVariables__c.getInstance('MMBAccountNameLength').Value__c+':'+companyName;
                }
                
                String messageDiv = String.valueOf(dataPowerReqInfoWrapInstance.data.messageDivision);
                // Email & Email Subject
                auditLogObj.Email__c=formatRequestField(dataPowerReqInfoWrapInstance.data.emailAddress);
                auditLogObj.EmailSubject__c=(String.isNotBlank(dataPowerReqInfoWrapInstance.data.emailSubject))?formatRequestField(dataPowerReqInfoWrapInstance.data.emailSubject):'Dummy Subject';
                // Weblead Type
                auditLogObj.Type__c=(dataPowerReqInfoWrapInstance.data.messageType.containsIgnoreCase('C'))?'Contact':(dataPowerReqInfoWrapInstance.data.messageType.containsIgnoreCase('L'))?'Lead':'';
                if(auditLogObj.Type__c == ''){
                    isError = true;
                    errorMsg += 'Type cannot be determined.';
                }
                auditLogObj.JSONRequest__c=formatRequestField(req.requestBody.toString());
                // Phone
                String phone = Utilities.formatPhoneNumber(formatRequestField(dataPowerReqInfoWrapInstance.data.primaryPhoneNumber));
                auditLogObj.PrimaryPhone__c= phone;
                if(String.isBlank(phone)){
                    isError = true;
                    errorMsg += ' Error formating phone.';
                }
                auditLogObj.SecondaryPhone__c=Utilities.formatPhoneNumber(formatRequestField(dataPowerReqInfoWrapInstance.data.secondaryPhoneNumber));
                // Address
                String street = formatRequestField(dataPowerReqInfoWrapInstance.data.streetAddress);
                String city = formatRequestField(dataPowerReqInfoWrapInstance.data.city);
                String state = formatRequestField(dataPowerReqInfoWrapInstance.data.stateCode);
                String countryCode = formatRequestField(dataPowerReqInfoWrapInstance.data.countryCode);
                String postalCode = formatRequestField(dataPowerReqInfoWrapInstance.data.zipCode);
                String psAddOn;
                //HRM-8636 Changes made by Srini for CA postal logic.
                if(String.isNotBlank(countryCode) && countryCode.equalsIgnoreCase('ca')){
                    postalCode = formatRequestField(dataPowerReqInfoWrapInstance.data.zipCode.substring(0,3));
                    if(dataPowerReqInfoWrapInstance.data.zipCode.length()>3)
                    psAddOn = formatRequestField(dataPowerReqInfoWrapInstance.data.zipCode.substring(3));
                }
                else{
                    if(postalCode.length() > 5){
                        postalCode = formatRequestField(dataPowerReqInfoWrapInstance.data.zipCode.substring(0,5));
                        psAddOn = formatRequestField(dataPowerReqInfoWrapInstance.data.zipCode.substring(5));
                    }
                }
                auditLogObj.PostalCodeAddOn__c = psAddOn;
                Boolean applyAddressDefault = false;
                auditLogObj.StreetAddress1__c = street;
                auditLogObj.City__c = city;
                auditLogObj.StateCode__c = state;
                auditLogObj.ZipCode__c = postalCode;
                auditLogObj.CountryCode__c = countryCode;
                auditLogObj.MessageId__c = req.headers.get('X-REQUEST-ID');
                list<DNIS__c> dnisList = new list<DNIS__c>();
                // Query the DNIS record
                for(DNIS__c dnis : [Select Id, Name, Business_segment__c from DNIS__c where Name =:dnisCode LIMIT 1]){
                    dnisList.add(dnis);
                }
                if(dnisList.size()>0){
                    auditLogObj.PromotionCode__c = dnisCode;
                }
                WebLeadsDNIStoAccountMapping__mdt weleaddnisAcc = new WebLeadsDNIStoAccountMapping__mdt();
                Map<String, WebLeadsDNIStoAccountMapping__mdt> webleddnisAccMap = new Map<String, WebLeadsDNIStoAccountMapping__mdt>();
                for(WebLeadsDNIStoAccountMapping__mdt wldnisAcc:[SELECT label, AccountBusinessId__c, AccountChannel__c, OneClickMessageDivision__c 
                                                                 FROM WebLeadsDNIStoAccountMapping__mdt WHERE isActive__c = TRUE LIMIT 100]){
                    webleddnisAccMap.put(wldnisAcc.label.toUpperCase(), wldnisAcc);
                    //Comparing metadata with Dnis business segment
                    if(dnisList.size()>0 && String.isNotBlank(dnisList[0].Business_segment__c) && dnisList[0].Business_segment__c.containsIgnoreCase(wldnisAcc.label)){
                        weleaddnisAcc = wldnisAcc;
                        break;
                    }
                }
                system.debug('####'+weleaddnisAcc+webleddnisAccMap);
                if(String.isBlank(weleaddnisAcc.label) && webleddnisAccMap.containskey(messageDiv.toUppercase())){
                    weleaddnisAcc = webleddnisAccMap.get(messageDiv.toUppercase());
                }
                
                if(String.isNotBlank(weleaddnisAcc.label)){
                    auditLogObj.Channel__c = weleaddnisAcc.AccountChannel__c;
                    auditLogObj.LineOfBusiness__c = weleaddnisAcc.AccountBusinessId__c;
                    auditLogObj.OneClickMessageDivision__c = weleaddnisAcc.OneClickMessageDivision__c;    
                }
                else{
                    //All else fails, just for backup
                    auditLogObj.Channel__c = 'Resi Direct Sales';
                    auditLogObj.LineOfBusiness__c = '1100 - Residential';
                    auditLogObj.OneClickMessageDivision__c = 'Residential';  
                }
                //UDF Tags
                if(dataPowerReqInfoWrapInstance.data.udf != null && dataPowerReqInfoWrapInstance.data.udf.size()>0){
                    Schema.SObjectType auditLogSchema = Schema.getGlobalDescribe().get('AuditLog__c');
                    Map<String, Schema.SObjectField> fieldMap = auditLogSchema.getDescribe().fields.getMap();
                    Map<String,String> allAuditLogFldMap = new Map<String, String>();
                    for (String fieldName: fieldMap.keySet()){
                        allAuditLogFldMap.put(fieldMap.get(fieldName).getDescribe().getLabel(), fieldMap.get(fieldName).getDescribe().getName());
                    }
                    for(UDFParamsInfo ud: dataPowerReqInfoWrapInstance.data.udf){
                        if(allAuditLogFldMap.containsKey(ud.tag)){
                            AuditLog__c adl = (AuditLog__c)auditLogObj.put(allAuditLogFldMap.get(ud.tag),ud.value);
                        }
                    }
                    
                }
                system.debug('##'+auditLogObj);
                insert auditLogObj;
                
                Id postalCodeId;
                if(String.isBlank(auditLogObj.ZipCode__c)){
                    isError = true;
                    errorMsg += 'POSTALCODE_ERROR';
                }
                else if(String.isNotBlank(auditLogObj.LineOfBusiness__c)){
                    // Postal Code Lookup
                    postalCodeId = webLeadsController.populatePostalCodeLookup(postalCode,auditLogObj.LineOfBusiness__c.substring(0,4));
                    if(postalCodeId != null){
                        auditLogObj.PostalCodeId__c = postalCodeId;
                        auditLogObj.IsValidPostalCode__c = true;
                    }
                    else{
                        auditLogObj.IsValidPostalCode__c = false;
                        isError = true;
                        errorMsg += 'POSTALCODE_ERROR';
                    }
                }
                        
                // Find matching account
                if(auditLogObj.Type__c == 'Lead' || dataPowerReqInfoWrapInstance.data.messageType.containsIgnoreCase('L')){
                    
                    Account acc = new Account();
                    // Find existing account exists or a new account is created
                    acc = webLeadsController.findMatchingAccount(auditLogObj);
                    auditLogObj.Account__c = acc.Id;
                    Id AddressId = [SELECT AddressID__c FROM Account WHERE Id =:acc.Id].AddressID__c; 
                    if(AddressId != null)
                    auditLogObj.AddressID__c = AddressId;
                    auditLogObj.PostalCodeId__c = acc.PostalCodeID__c;
                    // Create Call Data
                    Call_Data__c cd = new Call_Data__c();
                    cd.Name = auditLogObj.FormName__c+'_'+String.valueOfGmt(DateTime.now());
                    if(dnisList.size()>0)
                        cd.DNIS__c = dnisList[0].Id;
                    else
                        cd.Dial__c = dnisCode;
                                        // Check if DNIS exists
                    if(dnisList.size() == 0){
                        isError = true;
                        if(String.isNotBlank(errorMsg))
                        errorMsg += ';';
                        errorMsg += 'DNIS_ERROR';
                    }
                    cd.Contact_Type__c = 'WebLead';
                    cd.ANI__c = auditLogObj.PrimaryPhone__c;
                    cd.Account__c = acc.Id;
                    cd.Received_By__c = Userinfo.getUserId(); // Integration User
                    cd.Accountlastactivitydate__c = system.today();
                    cd.UniqueVisitorId__c = auditLogObj.Request_Custom_13__c;
                    insert cd;
                    auditLogObj.CallData__c = cd.Id;
                    //Logic to map related/orphan customer web interests
                    if(String.isNotBlank(cd.UniqueVisitorId__c)){
                    list<CustomerInterestConfig__c> cilist = [SELECT id, CallData__c, Type__c FROM CustomerInterestConfig__c
                                                              WHERE UniqueVisitorNumber__c =:cd.UniqueVisitorId__c
                                                             	AND FriendlyId__c = :auditLogObj.Request_Custom_14__c];
                        boolean isProductFound = false;
                        for(CustomerInterestConfig__c cl:cilist){
                            cl.CallData__c = cd.Id;
                            //Start HRM-10967
                            if(cl.Type__c == 'Product'){
                            	isProductFound = true;
                            }
                            //End HRM-10967
                        }
                        update cilist;
                        //Start HRM-10967
                        /*if(emailDetailsCicId != null){
                        	ADTWebLeadsController.doWebInterestProcessing(auditLogObj);
                        }*/
                        if(isProductFound){
                        	ADTWebLeadsController.doWebInterestProcessing(auditLogObj);
                        }else{
                        	ADTCheetahMailAPI cheetahMailAPI = new ADTCheetahMailAPI();
                			cheetahMailAPI.sendToACKEmail(auditLogObj);
                        }
                        //End HRM-10967
                    }
                    
                }
                auditLogObj.PromotionCode__c = dnisCode;
                // Error Status
                if(isError){
                    auditLogObj.status__c = 'Form Error';
                    auditLogObj.Error__c = errorMsg;
                }
                update auditLogObj;
                // Proceed to lead flow if there are no form errors
                if(dataPowerReqInfoWrapInstance.data.messageType.containsIgnoreCase('L')){
                    webLeadsController.doLeadProcessing(auditLogObj);
                }
                // Contact flow to send email
                if(dataPowerReqInfoWrapInstance.data.messageType.containsIgnoreCase('C')){
                    webLeadsController.doContactProcessing(auditLogObj.Id);
                }
                res.statuscode = 200;
                res.responseBody = Blob.valueOf('Success');
        }
        else{
            res.statuscode = 400;
            res.responseBody = Blob.valueOf('Empty body');
            }
        }
        catch(Exception e){
            ADTApplicationMonitor.log(e,'ADTWebLeadsController','ADTWebLeadAPI',ADTApplicationMonitor.CUSTOM_APP.HERMES);
            res.statuscode = 500;
            res.responseBody = Blob.valueOf(e.getMessage());
        }
    }
    
   /*
    * Method name : processWebleadsRequestQueue
    * Parameters : String (EBR or 1Click or ACK)
    * Return Type: Void
    * Called from the Integration Server
    */
    public static void processWebleadsRequestQueue(String type){
        list<RequestQueue__c> webleadsReqQueuesToUpdate = new list<RequestQueue__c>();
        map<Id,RequestQueue__c> ReqMap = new map<Id,RequestQueue__c>();
        list<Disposition__c> disposToInsert = new list<Disposition__c>();
        if(type == 'EBR'){
            // Get EBR limit from custom setting
            String ebrLimitStr = BatchGlobalVariables__c.getinstance('EBR Limit') != null? BatchGlobalVariables__c.getinstance('EBR Limit').value__c: '10';
            Integer ebrLimit = Integer.ValueOf(ebrLimitStr);
            // Query EBR request queues
            for(RequestQueue__c ebrRequeue: [select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c,ErrorDetail__c from RequestQueue__c 
                    where RequestStatus__c = 'READY' AND ServiceTransactionType__c ='sendEBRRequest' limit :ebrLimit]){
                ebrRequeue = ADTEBRAPI.doEBRCallOut(ebrRequeue);
                if(ebrRequeue.IsPrimary__c && ebrRequeue.RequestStatus__c == IntegrationConstants.REQUEST_STATUS_SUCCESS){
                    ReqMap.put(ebrRequeue.Id,ebrRequeue);
                }
                webleadsReqQueuesToUpdate.add(ebrRequeue);
            }
        }else if(type == 'OneClick'){
             system.debug('oneclick**');
            // Get 1Click limit from custom setting
            String oneClickLimitStr = BatchGlobalVariables__c.getinstance('One Click Limit') != null? BatchGlobalVariables__c.getinstance('One Click Limit').value__c : '30';
            Integer oneClickLimit = Integer.ValueOf(oneClickLimitStr);
            // Query One click request queues
            for(RequestQueue__c OneClickRequeue: [select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c,ErrorDetail__c from RequestQueue__c 
                    where RequestStatus__c = 'READY' AND ServiceTransactionType__c ='sendToOneClick' limit :oneClickLimit]){
                
                // Create a disposition
                Disposition__c dispRec = new Disposition__c();
                dispRec.DispositionType__c = 'System Generated';
                dispRec.DispositionDate__c = System.now();
                dispRec.DispositionDetail__c = 'Web lead Request sent to OneClick';
                dispRec.AccountID__c = OneClickRequeue.AccountID__c;
                dispRec.DispositionedBy__c = UserInfo.getUserId();
                dispRec.Comments__c = OneClickRequeue.RequestStatus__c == IntegrationConstants.REQUEST_STATUS_SUCCESS? 'Request to OneClick is successful' : 'Request to OneClick failed';
                disposToInsert.add(dispRec);
                
                webleadsReqQueuesToUpdate.add(ADTOneClickAPI.doOneClickCallOut(OneClickRequeue));
            }
        }else if(type == 'ACK'){
        system.debug('ACK**');
            // Get ACK limit from custom setting
            String ackLimitStr = BatchGlobalVariables__c.getinstance('ACK Limit') != null? BatchGlobalVariables__c.getinstance('ACK Limit').value__c : '60';
            Integer ackLimit = Integer.ValueOf(ackLimitStr);
            // Query ACK request queues
            system.debug('acklimit**'+acklimit);
            for(RequestQueue__c ackRequeue: [select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c,ErrorDetail__c from RequestQueue__c 
                    where RequestStatus__c = 'READY' AND ServiceTransactionType__c ='sendACKEmail' limit :ackLimit]){
                    system.debug('comes here**');
                webleadsReqQueuesToUpdate.add(ADTCheetahMailAPI.doACKEmailCallOut(ackRequeue));     
            }
        }
        else if(type == 'sendEWCRequest'){
            // Get ECW limit from custom setting
            String ecwLimitStr = BatchGlobalVariables__c.getinstance('EWC Limit')!= null && String.isNotBlank(BatchGlobalVariables__c.getinstance('EWC Limit').value__c)? BatchGlobalVariables__c.getinstance('EWC Limit').value__c: '10';
            Integer ecwLimit = Integer.ValueOf(ecwLimitStr);
            Map<ID,ID> mapSendtext = new Map <Id, Id>();
            // Query ECW request queues
            for(RequestQueue__c ewcRequeue: [select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c,ErrorDetail__c from RequestQueue__c  where RequestStatus__c = 'READY' AND ServiceTransactionType__c ='sendEWCRequest' limit :ecwLimit]){
                ewcRequeue = ADTEWCConsentAPI.doEWCCallOut(ewcRequeue,'sendEWCRequest');
                if(ewcRequeue.RequestStatus__c == IntegrationConstants.REQUEST_STATUS_SUCCESS){
                    //ReqMap.put(ewcRequeue.Id,ewcRequeue);
                    // Added by VIRAJ for JIRA  - HRM - 10106
                    // Call Send Text API once ECW - addConsent is successful 
                    ADTEWCConsentAPI.ServiceTransactionType = 'sendText';
                    mapSendtext.put(ewcRequeue.AccountID__c,ewcRequeue.AuditLog__c);
                }
                webleadsReqQueuesToUpdate.add(ewcRequeue);
            }
            ADTEWCConsentAPI.sendText(mapSendtext);
        }
        
        else if(type == 'sendText'){
            // Get ECW limit from custom setting
            String ecwLimitStr = BatchGlobalVariables__c.getinstance('EWC Limit')!= null && String.isNotBlank(BatchGlobalVariables__c.getinstance('EWC Limit').value__c) ? BatchGlobalVariables__c.getinstance('EWC Limit').value__c: '10';
            Integer ecwLimit = Integer.ValueOf(ecwLimitStr);
            // Query ECW request queues
            for(RequestQueue__c ecwRequeue: [select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c,ErrorDetail__c from RequestQueue__c  where RequestStatus__c = 'READY' AND ServiceTransactionType__c ='sendText' limit :ecwLimit]){
                ecwRequeue = ADTEWCConsentAPI.doEWCCallOut(ecwRequeue,'sendText');
                webleadsReqQueuesToUpdate.add(ecwRequeue);
            }
        }
        
        else if(type == 'addEBRInquiry'){
            // Get ECW limit from custom setting
            String ecwLimitStr = BatchGlobalVariables__c.getinstance('EWC Limit')!= null && String.isNotBlank(BatchGlobalVariables__c.getinstance('EWC Limit').value__c)? BatchGlobalVariables__c.getinstance('EWC Limit').value__c: '10';
            Integer ecwLimit = Integer.ValueOf(ecwLimitStr);
            Map<ID,ID> possibleNow = new Map <Id, Id>();
            Integer maxCount = BatchGlobalVariables__c.getinstance('EWC Limit')!= null && String.isNotBlank(BatchGlobalVariables__c.getinstance('requestQueueRetryCounter').value__c)? Integer.valueOf(BatchGlobalVariables__c.getinstance('requestQueueRetryCounter').value__c): Integer.valueOf('5');
            // Query ECW request queues
            for(RequestQueue__c possibleNowGryphonRecord: [select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c,ErrorDetail__c from RequestQueue__c  where (RequestStatus__c =: IntegrationConstants.REQUEST_STATUS_READY  OR RequestStatus__c =:IntegrationConstants.REQUEST_STATUS_ERROR )And (Counter__c = null OR (Counter__c != null AND Counter__c <=:maxCount)) AND ServiceTransactionType__c= 'addEBRInquiry' limit :ecwLimit]){
                possibleNowGryphonRecord = AdtCustContactEBRServiceCallOut.doPossibleNowGryphonCallout(possibleNowGryphonRecord);
                webleadsReqQueuesToUpdate.add(possibleNowGryphonRecord);
            }
        }
        //Start HRM-10967
        else if(type == 'sendPCEmail'){
            system.debug('sendPCEmail**');
            // Get ACK limit from custom setting
            String ackLimitStr = BatchGlobalVariables__c.getinstance('PCACK Limit') != null? BatchGlobalVariables__c.getinstance('PCACK Limit').value__c : '60';
            Integer ackLimit = Integer.ValueOf(ackLimitStr);
            // Query ACK request queues
            system.debug('acklimit**'+acklimit);
            for(RequestQueue__c ackRequeue: [select Id, RequestStatus__c, ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c,ErrorDetail__c from RequestQueue__c 
                    where RequestStatus__c = 'READY' AND ServiceTransactionType__c ='sendPCEmail' limit :ackLimit]){
                    system.debug('comes here**');
                webleadsReqQueuesToUpdate.add(ADTCheetahMailSchema.doACKEmailCallOut(ackRequeue));     
            }
        }
        //End HRM-10967
        if(webleadsReqQueuesToUpdate.size() > 0){
            database.update(webleadsReqQueuesToUpdate,false);
        }
        
        if(ReqMap.size() > 0){
            for(String reqQueId: ReqMap.keyset()){
                ADTOneClickAPI.createOneClickReqQueue(ReqMap.get(reqQueId).AccountID__c,ReqMap.get(reqQueId).AuditLog__c);
            }
        }

        if(disposToInsert.size() > 0){
            database.insert(disposToInsert,false);
        }
    }
}