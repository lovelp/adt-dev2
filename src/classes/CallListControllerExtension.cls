/************************************* MODIFICATION LOG ********************************************************************************************
* CallListControllerExtension
*
* DESCRIPTION : Supports Call List exports by retrieving Account data.  
*               It is a Controller Extension in the SFDC MVC architecture.
*
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*             				 10/14/2011			- Original Version
*
*													
*/

public with sharing class CallListControllerExtension {
	
	private String streetSheetID {get;set;}
	
	public List <SearchItem> itemList {get; set;}
	
	public CallListControllerExtension(ApexPages.StandardController stdController) {
    	streetSheetID=System.currentPageReference().getParameters().get('ssid');
    }
	
	public PageReference process() {
		
		// retrieve the Street Sheet Items
		itemList = SearchManager.findStreetSheetItems(streetSheetID);
	
		return null;	
	}

}