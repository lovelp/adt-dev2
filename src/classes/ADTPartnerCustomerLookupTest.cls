/*
* DESCRIPTION : Test class for MMBCustomerSitePartner
*/

@isTest
public class ADTPartnerCustomerLookupTest{
    
    //create test data for the test class
    public static void createTestData(){
        list<PartnerAPIMessaging__c> apiMsglist = new list<PartnerAPIMessaging__c>();
        apiMsglist.add(new PartnerAPIMessaging__c(name='400', API_Name__c = 'All APIs', Error_Message__c = 'Invalid request'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='404', API_Name__c = 'All APIs', Error_Message__c = 'Opportunity not found'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='500', API_Name__c = 'All APIs', Error_Message__c = 'Something went wrong while processing the request. Please try again later.'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='AccountNewSale', API_Name__c = 'SiteCustomerLookupAPI', Error_Message__c = 'This Customer Account is not available for replacetype and will be booked as a New quote - New Sale'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='AddOnNoActiveContract', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Customer does not have any active contract for Add On')); 
        apiMsglist.add(new PartnerAPIMessaging__c(name='    BlockedSiteCustomerTypeId', API_Name__c = 'SiteCustomerLookupAPI', Error_Message__c = 'Site or Customer Type not allowed for sale'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='CellGuardNotAllowed', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'DLL Appointment is not allowed for this Radio'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='ConversionAllowed', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Conversion allowed for this customer site selection'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='CustomerNull', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Customer information missing, please retry.'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='ResaleEligibleSite', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Site eligible for Resale'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='HOA', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Must Set Field Sales Appointment'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='MustSetFieldSalesAppointment', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Must Set Field Sales Appointment'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='P1-CHS', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'P1-CHS is not allowed'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='P1MMBNotAllowedForSelectBoth', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = '   Select both is not allowed for P1MMB customers'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='PanelExistsButZonesNotAllowed', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Phone Activation is not Allowed.Panel Exists but Zone and Cell guard is not present.'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='SiteBillingPrefixInformix', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'INF'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='SiteBillingPrefixMMB', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'MMB'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='SiteBillingPrefixP1MMB', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'P1'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='TexasSmokeDectorMessage', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Phone activation is not allowed. You have Fire Smoke Detector Installed. We will send a technician to activate your System.'));
        apiMsglist.add(new PartnerAPIMessaging__c(name='TransferP1Site', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'P1 customer/site please transfer call to NSC'));    
        apiMsglist.add(new PartnerAPIMessaging__c(name='TexasZoneCommentMessage', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Must set Field Sales Appt'));  
        apiMsglist.add(new PartnerAPIMessaging__c(name='ZoneCommentMessage', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Must set Field Sales Appt'));  
        
        apiMsglist.add(new PartnerAPIMessaging__c(name='CustomerNullHOA', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Must set Field Sales Appt'));  
        //apiMsglist.add(new PartnerAPIMessaging__c(name='ZoneCommentMessage', API_Name__c= 'SiteCustomerLookupAPI', Error_Message__c = 'Must set Field Sales Appt'));  
          
        insert apiMsglist;
       
        Equifax__c eq1 = new Equifax__c();
        eq1.name = 'Default Risk Grade';
        eq1.value__c = 'W';
        insert eq1;
        Equifax__c eq2 = new Equifax__c();
        eq2.name = 'Default Condition Code';
        eq2.value__c = 'CAE1';
        insert eq2;
        
        list<ResaleGlobalVariables__c> rgvList = new list<ResaleGlobalVariables__c>();
        rgvList.add(new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger', value__c='FALSE'));
        rgvList.add(new ResaleGlobalVariables__c(name='IntegrationUser', value__c='Integration User'));
        rgvList.add(new ResaleGlobalVariables__c(name='RIFAccountOwnerAlias', value__c='ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c(name='DataConversion', value__c='FALSE'));
        User u = TestHelperClass.createAdminUser();
        rgvList.add(new ResaleGlobalVariables__c(name='NewRehashHOARecordOwner', value__c=u.username));
        rgvList.add(new ResaleGlobalVariables__c(name='RvServiceabilityOrderTypes', value__c='N1;N2;N3;N4;A1;R1;R2;R3;R4'));
        rgvList.add(new ResaleGlobalVariables__c(name='DaysToPopulateDNISOnAccount', value__c='1'));
        rgvList.add(new ResaleGlobalVariables__c(name='GlobalResaleAdmin',value__c = 'Global Admin'));
        insert rgvList;
                    
        PartnerSettings__c ps = new PartnerSettings__c();
        ps.accountLookupLimit__c = 5;
        ps.AllowedCusTypeId__c = 'SMB,RES,I,A';
        ps.AllowedSiteTypeId__c = 'B,C,N,Q,R,S,W';
        ps.AccountUpdateAllowedOrderTypes__c = 'N4;R3;A1';
        ps.MMB_Search_Results_Limit__c  = 5;
        ps.NewSaleBasedOnSiteTypeId__c = 'W';
        ps.SiteBillingSystems__c = 'INF, Informix Billed, P1MMB, MMB, MMB-Residential, MMB-Small Business';
        ps.ValidUSStateCodes__c = 'AL;AK;AZ;AR;CA;CO;CT;DE;DC;FL;GA;HI;ID;IL;IN;IA;KS;KY;LA;ME;MD;MA;MI;MN;MS;MO;MT;NE;NV;NH;NJ;NM;NY;NC;ND;OH;OK;OR;PA;PR;RI;SC;SD;TN;TX;UT;VT;VA;VI;WA;WV;WI;WY';
        insert ps;
                  
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.MMBSearchUsername__c = 'salesforceclup';
        is.MMBSearchPassword__c = 'abcd1234';
        is.Equifax_EndPoint__c = 'https://dev2.api.adt.com/adtfico';
        is.Equifax_Username__c = 'ibmcpqoms';
        is.Equifax_Password__c = 'abcd1234';
        is.MMBSearchCalloutTimeout__c = 60000;
        is.MMBSearchEndpoint__c = 'https://dev.api.adt.com/adtclup';
        insert is;
                       
        Equifax_Conditions__c ec = new Equifax_Conditions__c();
        ec.Agent_Message__c = 'test \n';
        ec.name = 'CAE1';
        ec.EasyPay_Required__c = true;
        ec.Payment_Frequency__c = 'A';
        ec.Deposit_Required_Percentage__c = 100;
        insert ec;
        
        PartnerConfiguration__c partnerConfig = new PartnerConfiguration__c();
        partnerConfig.PartnerMessage__c = 'Transfer to ADT';
        partnerConfig.AllowedOrderType__c = 'N1;R1';
        partnerConfig.PartnerID__c = 'RV';
        insert partnerConfig;
    }
    
    //create call data for test
    private static Call_Data__c createcallData(){
        Call_Data__c cData = new Call_Data__c();
        cData.name = 'test call';
        insert cData;
        return cData;
    }
    
    //create new lead
    public static Lead createNewLead(){
        Postal_Codes__c pc = new Postal_Codes__c ();
        pc.Name = '32212';
        pc.BusinessID__c ='1100';
        insert pc;
        
        Lead l = new Lead();
        l.FirstName = 'John';
        l.LastName = 'Smith';
        l.Phone = '904- 999- 1234';
        l.Channel__c = 'Resi Direct Sales';
        l.Business_Id__c ='1100 - Residential';
        l.SiteStreet__c = '101 Main Street';
        l.SiteStreet2__c = 'Unit 3';
        l.SiteStreetName__c = 'First Street';
        l.SiteStreetNumber__c = '101';
        l.SiteCity__c = 'Jacksonville';
        l.SiteStateProvince__c = 'FL';
        l.SiteCounty__c = 'Duval';
        l.SiteCountryCode__c = 'US';
        l.IncomingLatitude__c = 30.332184;
        l.IncomingLongitude__c = -81.60647;
        l.SitePostalCode__c = '32212';
        l.SitePostalCodeAddOn__c = '1234';
        l.SiteValidated__c = True;
        l.SiteValidationMsg__c= 'test';
        l.PostalCodeID__c = pc.id;
        insert l;
        return l;
    }
    
    public static Address__c createAddr(){
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '101 Main Street';
        addr.City__c = 'Jacksonville';
        addr.State__c = 'FL';
        addr.PostalCode__c = '32212';
        addr.OriginalAddress__c = '101 MAIN STREET+JACKSONVILLE+FL+32212';
        
        insert addr;
        return addr;
    }
    
    
    public static Account CreateAccount(Address__c addr){
        Account acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'test';
        acct.FirstName__c = 'testfirst';
        acct.LastName__c ='testlast';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.PostalCodeID__c = TestHelperClass.inferPostalCodeID(addr.PostalCode__c,'1100');
        acct.DispositionCode__c = 'test';
        acct.ShippingPostalCode = '32212';
        acct.Data_Source__c = 'TEST';
        
        acct.Phone = '904-999-1234';
        acct.ShippingPostalCode = '22102';
        acct.EquifaxApprovalType__c = 'CAE1';
        insert acct;
              
        /*acct.Phone = '904-999-1234';
        acct.ShippingPostalCode = '22102';
        acct.EquifaxApprovalType__c = 'CAE1';
        update acct;*/
        return acct;
    }
    
    public static Opportunity createOpp(account acct){
        Opportunity opp = New Opportunity();
        opp.AccountId = acct.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp;
        return opp;
    }
        
    // Test method for API
    public static testMethod void ADTPartnerCustomerLookupSchemaMethod(){
        ADTPartnerCustomerLookupSchema schema = new ADTPartnerCustomerLookupSchema(); 
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI
    public static testMethod void SiteCustomerLookupAPI(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u = TestHelperClass.createUser(3333);
        System.runAs(u) {    
               
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            Opportunity opp = createOpp(acct);
            call_data__c cd = createcallData();
            cd.lead__c = le.id;
            update cd;
                
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"partnerId" : "RV" ,"callId" : "' + cd.id+ '" ,"opportunityId" :"' + opp.id+ '" ,"partnerRepName" : "John Smith"}') ;
                           
            RestContext.request = req;
            RestContext.response = res;
            
            Test.starttest();
            // Using MMBCustomerSiteResponseGenerator
            Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteResponseGenerator());   
            Test.stoptest();
            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI
    public static testMethod void SiteCustomerLookupAPI2(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u = TestHelperClass.createUser(11);
        System.runAs(u) {                
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            Opportunity opp = createOpp(acct);
            call_data__c cd = createcallData();
            cd.lead__c = null;
            cd.account__c = acct.id;
            update cd; 
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
        
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"partnerId" : "RV" ,"callId" : "' + cd.id+ '" ,"opportunityId" :"'+opp.id+'" ,"partnerRepName" : "John Smith"}') ;
            
            RestContext.request = req;
            RestContext.response = res;
            
            Test.starttest(); 
            // Using ADTPartnerCustomerSiteResponseGenerator
            Test.setMock(HttpCalloutMock.class, new ADTPartnerCustomerSiteResponseGenerator());               
            Test.stoptest();
            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI Without Opportunity
    public static testMethod void SiteCustomerLookupAPIWithoutOppId(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u = TestHelperClass.createUser(111);
        System.runAs(u) {  
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            Opportunity op = createOpp(acct);
            call_data__c cd = createcallData();
            cd.lead__c = null;
            cd.account__c = acct.id;
            update cd;
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
        
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"partnerId" : "RV" ,"callId" : "' + cd.id+ '" ,"opportunityId" :"" ,"partnerRepName" : "John Smith"}') ;
            
            RestContext.request = req;
            RestContext.response= res;

            Test.starttest();
            // Using ADTPartnerCustomerSiteResponseGenerator
            Test.setMock(HttpCalloutMock.class, new ADTPartnerCustomerSiteResponseGenerator());
            Test.stoptest();
            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI Without Opportunity in Request but Opportunity exists for Account
    public static testMethod void SiteCustomerLookupAPIWithoutOppId2(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u = TestHelperClass.createUser(1111);
        System.runAs(u) {
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            Opportunity op = createOpp(acct);
            call_data__c cd = createcallData();
            cd.lead__c = null;
            cd.account__c = acct.id;
            update cd;
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
        
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"partnerId" : "RV" ,"callId" : "' + cd.id+ '" ,"opportunityId" :"" ,"partnerRepName" : "John Smith"}') ;
            
            RestContext.request = req;
            RestContext.response= res;
            
            Test.starttest();
            // Using MMBCustomerSiteResponseGenerator
            Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteResponseGenerator());   
            Test.stoptest();
            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI Without Opportunity
    public static testMethod void SiteCustomerLookupAPIWithoutOppId3(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u =  [Select Id from User where Id = :UserInfo.getUserId()];
        System.runAs(u) {
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            call_data__c cd = createcallData();
            cd.lead__c = null;
            cd.account__c = acct.id;
            update cd;
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
        
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"partnerId" : "RV" ,"callId" : "' + cd.id+ '" ,"opportunityId" :"" ,"partnerRepName" : "John Smith"}') ;
            
            RestContext.request = req;
            RestContext.response= res;
            
            Test.starttest();
            // Using MMBCustomerSiteResponseGenerator
            Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteResponseGenerator());
            Test.stoptest();

            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI Without Opportunity or PartnerId
    public static testMethod void SiteCustomerLookupAPIWithoutPartnerId(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u = TestHelperClass.createUser(22);
        System.runAs(u) {
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            call_data__c cd = createcallData();
            cd.lead__c = null;
            cd.account__c = acct.id;
            update cd; 
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
        
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"callId" : "' + cd.id+ '" ,"opportunityId" :"" ,"partnerRepName" : "John Smith"}') ;
            
            RestContext.request = req;
            RestContext.response= res;
            
            Test.starttest(); 
            Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteResponseGenerator());
            Test.stoptest();

            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI Without Opportunity and random parameter
    public static testMethod void SiteCustomerLookupAPIWithRandomParam(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u = TestHelperClass.createUser(222);
        System.runAs(u) {                 
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            call_data__c cd = createcallData();
            cd.lead__c = null;
            cd.account__c = acct.id;
            update cd;
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
        
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"callId" : "' + cd.id+ '" ,"opportunityId" :"" ,"partnerRepName" : "John Smith","test":"test"}') ;
            
            RestContext.request = req;
            RestContext.response= res;
            
            Test.starttest(); 
            Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteResponseGenerator());
            Test.stoptest();
            
            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI With lead in CallData
    public static testMethod void SiteCustomerLookupAPIWithLeadInCallData(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u = TestHelperClass.createUser(2222);
        System.runAs(u) {                 
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            call_data__c cd = createcallData();
            cd.lead__c = le.id;           
            update cd;
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
        
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"partnerId" : "RV" , "callId" : "' + cd.id+ '" ,"opportunityId" :"" ,"partnerRepName" : "John Smith"}') ;
            
            RestContext.request = req;
            RestContext.response= res;
            
            Test.starttest(); 
            Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteResponseGenerator());   
            Test.stoptest();
            
            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI With lead in CallData but Converted to Account
    public static testMethod void SiteCustomerLookupAPIWithAccInCallData(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u =  [Select Id from User where Id = :UserInfo.getUserId()];
        System.runAs(u) {                 
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            call_data__c cd = createcallData();
            cd.lead__c = le.id;
            
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(le.id);        
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            lc.setDoNotCreateOpportunity(TRUE);
            Database.LeadConvertResult lcr = Database.convertLead(lc);     
            update cd;
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
        
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"partnerId" : "RV" , "callId" : "' + cd.id+ '" ,"opportunityId" :"" ,"partnerRepName" : "John Smith"}') ;
            
            RestContext.request = req;
            RestContext.response= res;

            Test.starttest();
            // Using MMBCustomerSiteResponseGenerator
            Test.setMock(HttpCalloutMock.class, new MMBCustomerSiteResponseGenerator());   
            Test.stoptest();
            
            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI With lead in CallData but Converted to Account
    public static testMethod void SiteCustomerLookupAPIWithAccInCallData2(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        User u = TestHelperClass.createUser(33);
        System.runAs(u) {                 
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            Account acct = CreateAccount(addr);
            call_data__c cd = createcallData();
            cd.lead__c = le.id;  
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(le.id);        
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);            
            Database.LeadConvertResult lcr = Database.convertLead(lc);     
            update cd;
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
        
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"partnerId" : "RV" , "callId" : "' + cd.id+ '" ,"opportunityId" :"" ,"partnerRepName" : "John Smith"}') ;
            
            RestContext.request = req;
            RestContext.response= res;
            
            Test.starttest();
            // Using ADTPartnerCustomerSiteResponseGenerator
            Test.setMock(HttpCalloutMock.class, new ADTPartnerCustomerSiteResponseGenerator());   
            Test.stoptest();
            
            ADTPartnerSiteCustomerLookupAPI.doPost();
        }
    }
    
    // Test method for ADTPartnerSiteCustomerLookupAPI for Zones
    public static testMethod void SiteCustomerLookupAPIZones(){
        ADTPartnerSiteCustomerLookupAPI api = new ADTPartnerSiteCustomerLookupAPI();
        MMBCustomerSitePartner sitepartner = new MMBCustomerSitePartner();
        boolean zc = sitepartner.zoneCommentComparater('PA','test');
       
        User u = TestHelperClass.createUser(333);
        System.runAs(u) {                
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            addr.State__c = 'TX';
            update addr;
            Account acct = CreateAccount(addr);
            Opportunity op = createOpp(acct);
            call_data__c cd = createcallData();
            cd.lead__c = null;
            cd.account__c = acct.id;
            update cd;
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
        
            req.httpMethod = 'Get';
            req.requestBody = Blob.valueof('{"partnerId" : "RV" ,"callId" : "' + cd.id+ '" ,"opportunityId" :"'+op.id+'" ,"partnerRepName" : "John Smith"}') ;
            
            RestContext.request = req;
            RestContext.response = res;
            
            String testjsonCustRequest = req.requestBody.toString();
            Test.starttest(); 
            Test.setMock(HttpCalloutMock.class, new ADTPartnerCustomerSiteResponseGenerator());               
            Test.stoptest();
            ADTPartnerSiteCustomerLookupAPI.doPost();
            
            /*List<MMBCustomerSiteSearchApi.SiteInfoResponse> setINFOSITE = new List<MMBCustomerSiteSearchApi.SiteInfoResponse>();
            string[] arr1,arr2;
            setINFOSITE.add(new MMBCustomerSiteSearchApi.SiteInfoResponse('HOA'=>'Y'));*/
        
            //sitepartner.selectedResultSite.HOA ='Y';
            sitepartner = new MMBCustomerSitePartner();
            system.debug('*******************selectedResultSite.....'+sitepartner.selectedResultSite);
            sitepartner.selectedResultSite.ResaleEligible = 'ELIGIBLE';
            sitepartner.selectedResultSite.ServiceActive = 'OUT';
            sitepartner.selectedResultCustomer.BillingSystem = 'INF';
            sitepartner.selectedResultCustomer.ContractTerm = '12';
            sitepartner.selectedResultCustomer.contractExpirationDate = '2019-12-31';
            sitepartner.selectedResultCustomer.contractStartDate = '2019-01-01';
            sitepartner.selectedResultCustomer.ReinstateFlag = 'Y';
            sitepartner.selectedResultCustomer.ReinstateReason = 'Special';
            sitepartner.selectedResultCustomer.CustomerStatus = 'D';
            String checkChangeOrderTypeval = sitepartner.checkChangeOrderType();
            Map<Boolean,String> CheckSiteFlagsval = sitepartner.CheckErrorFlag();
            
            system.debug('$$$' + sitepartner.pSettings);
            ADTPartnerCustomerLookupSchema.CustomerLookupRequest req1 = new ADTPartnerCustomerLookupSchema.CustomerLookupRequest();
            req1.callId = cd.Id;
            req1.partnerId = 'ECOM';
            sitepartner.getCustomerSites(req1);
            
            /*cd.lead__c = le.Id;
            cd.account__c = null;
            update cd;
            sitepartner.getCustomerSites(req1);*/
        }
    }
    
    //Added by srini for test coverage
    private static string testdate(string y,string m,string d){
        string year = y;
        string month = m;
        string day = d;
        string hour = '12';
        string minute = '20';
        string second = '20';
        string stringDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
        return stringDate;
    }
    
    private static testmethod void testmethod_s(){
        MMBCustomerSiteSearchApi mmsiteSearch = new MMBCustomerSiteSearchApi();
        //mmsiteSearch.ResultSummaryMessage = 'SUCCESS';
        MMBCustomerSiteSearchApi.CustomerInfoResponse custinforesp = new MMBCustomerSiteSearchApi.CustomerInfoResponse();
        custinforesp.BillingCustNo = '12345';
        custinforesp.BillingSystem = 'INF';
        custinforesp.CustomerStatus = 'A';
        custinforesp.ContractStatus = 'A';
        custinforesp.ContractStartDate = testdate('2018','03','02');
        MMBCustomerSiteSearchApi.CustomerInfoResponse custinforesp1 = new MMBCustomerSiteSearchApi.CustomerInfoResponse();
        custinforesp1.BillingCustNo = '12345';
        custinforesp1.BillingSystem = 'INF';
        custinforesp1.ContractStatus = 'A';
        custinforesp1.ContractStartDate = testdate('2018','03','01');
        MMBCustomerSiteSearchApi.CustomerInfoResponse custinforesp2 = new MMBCustomerSiteSearchApi.CustomerInfoResponse();
        custinforesp2.BillingCustNo = '3245';
        custinforesp2.BillingSystem = 'MMB';
        custinforesp2.CustomerStatus = 'A';
        custinforesp2.ContractStatus = 'A';
        custinforesp2.ContractStartDate = testdate('2018','03','02');
        MMBCustomerSiteSearchApi.CustomerInfoResponse custinforesp3 = new MMBCustomerSiteSearchApi.CustomerInfoResponse();
        custinforesp3.BillingCustNo = '3245';
        custinforesp3.BillingSystem = 'MMB';
        custinforesp3.ContractStatus = 'A';
        custinforesp2.ContractStatus = 'A';
        custinforesp3.ContractStartDate = testdate('2018','03','01');
        list<MMBCustomerSiteSearchApi.CustomerInfoResponse> custinfolsit = new list<MMBCustomerSiteSearchApi.CustomerInfoResponse>();
        custinfolsit.add(custinforesp);
        custinfolsit.add(custinforesp1);
        MMBCustomerSiteSearchApi.LookupCustomerResponse llokcusre = new MMBCustomerSiteSearchApi.LookupCustomerResponse();
        llokcusre.customerInfo = custinfolsit;
        llokcusre.ResultSummaryMessage = 'SUCCESS';
        MMBCustomerSitePartner mmcus = new MMBCustomerSitePartner();
        mmcus.processCustomerResponse(llokcusre);
        User u = TestHelperClass.createUser(333);
        System.runAs(u) {                
            createTestData();
            Lead le = createNewLead();
            Address__c addr = createAddr();
            addr.State__c = 'TX';
            update addr;
            call_data__c cd = createcallData();
            cd.lead__c = le.Id;
            update cd;
            Test.starttest(); 
            Test.setMock(HttpCalloutMock.class, new ADTPartnerCustomerSiteResponseGenerator());               
            Test.stoptest();
            MMBCustomerSitePartner sitepartner = new MMBCustomerSitePartner();
            ADTPartnerCustomerLookupSchema.CustomerLookupRequest req1 = new ADTPartnerCustomerLookupSchema.CustomerLookupRequest();
            req1.callId = cd.Id;
            req1.partnerId = 'ECOM';
            sitepartner.getCustomerSites(req1);
            
        }
        
    }
}