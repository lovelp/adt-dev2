/************************************* MODIFICATION LOG ********************************************************************************************
*
* DESCRIPTION : RealTimePaymentController.cls is controller for RealTimePayment.page.  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Michael Tuckman				09/21/2014			- Original Version
* Varun Sinha					06/19/2019			- Updated Code To accept customer loan payment								
*/

public with sharing class RealTimePaymentController {
    
    public Boolean renderJobLookup		{get; set;}
    public Boolean renderJobDetails		{get; set;}
    public Boolean renderGateway		{get; set;}
    public Boolean renderTelemar		{get; set;}
    public Boolean renderComplete		{get; set;}
    public Boolean renderCancel			{get; set;}
    public Boolean disCustNo			{get; set;}
    public Boolean disJobNo				{get; set;}
    public Boolean disPayAmt			{get; set;}
    public Boolean disSearch			{get; set;}
    public Boolean disPayMe				{get; set;}
    public Boolean isTrip				{get; set;}
    public Boolean showHeader			{get; set;}
    public string mmbStatus   {get;set;}
    private String parmCustNo;
    private String parmJobNo;
    private String parmPayAmt;
    private String parmTelemar;
    private String parmClient;
    private String parmDesc;
    private String parmPayType;
    private String parmBalance;
    public String inCustNo 				{get; set;}
    public String inJobNo 				{get; set;}
    public String inPayAmt				{get; set;}
    public String inTelemarNo			{get; set;}
    public String inClient				{get; set;}
    public String inDesc				{get; set;}
    public String inTranJson			{get; set;}
    public String inError				{get; set;}
    public String inGatewayCode			{get; set;}
    public String inGatewayMsg			{get; set;}
    public String outCustName 			{get; set;}
    public String outCustAddr 			{get; set;}
    public String outDepositPaid		{get; set;}
    public String outOutstandingBalance	{get; set;}
    public String outframeSrc			{get; set;}
    public String imgSrc				{get; set;}
    //Added for Customer Loan
    public string showPayData 		{get;set;}
    public string accId					{get;set;}
    public string loanId				{get;set;}
    public string jobNumber;
    public string noteNumber		{get;set;}
    public User userDetail;
    public List<CustomerLoan__c> custloanList = new List<CustomerLoan__c>(); 
    public list<Real_Time_Payment__c> rtpList = new list<Real_Time_Payment__c>();
    public string rtpName;
    public boolean matrixDept{get;set;}
    public boolean mmbSuccessFlag{get;set;}
    string profileValue;
    public List<Account> accList = new List<Account>();
    public Boolean retryMMBButton				{get; set;}
    string zip;
    private MMBCustomerSiteSearchApi.CustomerInfoResponse cRes;
    private MMBCustomerSiteSearchApi.LookupJobResponse jobResponse;
    
    private String rtpID;
    private Boolean isECP;
    private Decimal savePayment;
    private TranJson tran;
    public Real_Time_Payment__c rtp	{get; set;}
    public static final String MobileTech = 'MOBILETECH';
    public static final String Telemar = 'TELEMAR';
    
    public RealTimePaymentController(){
        retryMMBButton = false;
    }
    
    public PageReference init() {
        mmbSuccessFlag = false;
        matrixDept = false;
        //Label created to get Phone Sales Users
        profileValue = System.Label.ProfileUnderPhoneSales;
        userDetail = [select id,EmployeeNumber__c,Profile_Name__c,Rep_Team__c from user where id =: UserInfo.getUserId()];
        //Check whether login user is a phone sales user
        if(string.isNotBlank(profileValue) && string.isNotBlank(userDetail.Profile_Name__c) && profileValue.contains(userDetail.Rep_Team__c)){
            matrixDept = true;
        }
        noteNumber = '';
        jobNumber = '';
        zip = '';
        showPayData = '';
        renderJobLookup = true;
        renderJobDetails = false;
        renderGateway = false;
        renderTelemar = false;
        disCustNo = false;
        disJobNo = false;
        disPayAmt = false;
        disSearch = false;
        disPayMe = false;
        inTranJson = '';
        inTelemarNo = '';
        showPayData = ApexPages.currentPage().getParameters().get('showLoanDataFlag');
        if(string.isBlank(showPayData)){
            showPayData = 'RealTimePay';
        }
        System.debug('The showPydata is'+showPayData);
        if(showPayData == 'RealTimePay'){
            parmCustNo = ApexPages.currentPage().getParameters().get('c');
            parmJobNo = ApexPages.currentPage().getParameters().get('j');
            parmPayAmt = ApexPages.currentPage().getParameters().get('p');
            parmTelemar = ApexPages.currentPage().getParameters().get('t');
            parmClient = ApexPages.currentPage().getParameters().get('cl');
            parmDesc = ApexPages.currentPage().getParameters().get('d');
            parmPayType = ApexPages.currentPage().getParameters().get('pt');
            savePayment = 0;
            Profile uProfile = [select Name, id from Profile where id = :Userinfo.getProfileID()];
            if (uProfile.Name.startsWith('Chatter')) showHeader = false;
            else showHeader = true;
            if (!Utilities.isEmptyOrNull(parmCustNo)) {
                inCustNo = parmCustNo;
                disCustNo = true;
            }
            if (!Utilities.isEmptyOrNull(parmJobNo)) {
                inJobNo = parmJobNo;
                disJobNo = true;
            }
            if (!Utilities.isEmptyOrNull(parmPayAmt)) {
                inPayAmt = Utilities.currency(parmPayAmt);
                disPayAmt = true;
            }
            
            if (!Utilities.isEmptyOrNull(parmTelemar)) {
                inTelemarNo = parmTelemar;
                renderTelemar = true;
            }
            if (!Utilities.isEmptyOrNull(parmClient)) {
                inClient = parmClient.toUpperCase();
            }
            
            if (!Utilities.isEmptyOrNull(parmDesc) && parmDesc.toUpperCase() == 'TRIP'){
                isTrip = true;
            }
            else{
                isTrip = false;
            }
            if (!Utilities.isEmptyOrNull(parmPayType) && parmPayType.toUpperCase() == 'ECP'){
                isECP = true;
            }
            else{
                isECP = false;
            }
            if (!Utilities.isEmptyOrNull(parmCustNo) && !Utilities.isEmptyOrNull(parmJobNo)) {
                if (Utilities.isEmptyOrNull(inClient)) {
                    if (Utilities.isEmptyOrNull(parmTelemar))
                        inClient = MobileTech;
                    else inClient = Telemar;
                }
                renderJobLookup = false;
                search();    		
            }
        }else if(showPayData == 'CustLoan'){
            //Added for Customer Loan
            loanId = ApexPages.currentPage().getParameters().get('lId');
            jobNumber = ApexPages.currentPage().getParameters().get('jobNumber');
            List<Quote_Job__c > quoteJobList = new List<Quote_Job__c >();
            //Query Quote job to get account number
            quoteJobList = [SELECT id,JobNo__c,ParentQuote__r.Account__c from Quote_Job__c where JobNo__c != null AND JobNo__c =: jobNumber LIMIT 1];
            if(quoteJobList.size()>0){
                accId = quoteJobList[0].ParentQuote__r.Account__c;
            }
            if(accId != null){
                //Query customer loan to get the record 
                custloanList = [SELECT id,Name,AmountFinanced__c,NoteNumber__c,QuoteJobNumber__c,PaymentStatus__c,AmountBilled__c,Balance__c FROM CustomerLoan__c WHERE id =: loanId and QuoteJobNumber__c =:jobNumber limit 1];
                accList = [select id,Name,PostalCodeID__c,AddressID__c,PostalCodeID__r.Name,TelemarAccountNumber__c,SiteStreetName__c,SiteStreetNumber__c,SiteCity__c,SiteState__c,SitePostalCode__c ,MMBCustomerNumber__c
                           from account where id =: accid limit 1];
                if(custloanList.size() >0 && custloanList[0].NoteNumber__c != null){
                    //If any real time payment record exist in RTP Object
                    rtpList = [select id,name,NoteNumber__c,Last_Status__c,MMBPostStatus__c,TransID__c,JSONResponse__c,PaymentAmount__c,ApprovalCode__c,Card_Type__c,Card_Last_4__c,Card_Indicators__c 
                               from Real_Time_Payment__c where NoteNumber__c!=null and NoteNumber__c =: custloanList[0].NoteNumber__c limit 1];
                }
                if(accList != null && custloanList != null && accList.size() > 0 && custloanList.size() > 0){
                    zip = String.isNotBlank(accList[0].PostalCodeID__r.Name) ? accList[0].PostalCodeID__r.Name : '';
                    if(string.isNotBlank(accList[0].MMBCustomerNumber__c)){
                        inCustNo = accList[0].MMBCustomerNumber__c;
                        disCustNo = true;
                    }
                    if(string.isNotBlank(accList[0].TelemarAccountNumber__c)){
                        inTelemarNo = accList[0].TelemarAccountNumber__c;
                        renderTelemar = true;
                    }
                    if(string.isNotBlank(accList[0].Name)){
                        outCustName = accList[0].Name;
                    }
                    if(string.isNotBlank(accList[0].AddressID__c)){
                        outCustAddr = accList[0].SiteStreetNumber__c +' '+accList[0].SiteStreetName__c+','+ ' ' + accList[0].SiteCity__c + ', ' + accList[0].SiteState__c + ' ' + accList[0].SitePostalCode__c;
                    }
                    if(string.isNotBlank(custloanList[0].QuoteJobNumber__c)){
                        inJobNo = custloanList[0].QuoteJobNumber__c;
                        disJobNo = true;
                    }  
                    if( custloanList[0].Balance__c >0 && (string.isBlank(custloanList[0].PaymentStatus__c) || custloanList[0].PaymentStatus__c == 'Not Paid' || custloanList[0].PaymentStatus__c == 'Paid')) {
                        //if(outOutstandingBalance == ''){
                        outOutstandingBalance = string.valueof(custloanList[0].Balance__c);
                        inPayAmt = string.valueof(custloanList[0].Balance__c);
                        disPayAmt = true;
                    }
                    if(string.isNotBlank(custloanList[0].NoteNumber__c)){
                        noteNumber = custloanList[0].NoteNumber__c;
                    }
                    disPayMe = true;
                    isTrip = false;
                    isECP = false;
                    renderJobDetails = true;
                    Real_Time_Payment__c tempRTP = new Real_Time_Payment__c();
                    if(rtpList.size() == 0){
                        rtp = createRTPRecord();
                        if(rtp != null){
                            tempRTP = [select name from Real_Time_Payment__c where ID = :rtp.ID];
                            rtpName = tempRTP.Name;
                        }
                    }else{
                        if(rtpList != null && rtpList.size() > 0){
                            rtp = rtpList[0];
                            if(string.isNotBlank(rtp.Name)){
                                rtpName = rtp.Name;
                            }
                            if(rtp.TransID__c !=null){
                                renderComplete = true;
                                inTranJson = rtp.JSONResponse__c;
                                showButton(rtp.TransID__c,rtp.MMBPostStatus__c);
                            }
                        }
                    }
                    //Call OutFrame
                    if(rtp != null && string.isBlank(rtp.TransID__c)){
                        outframeSrc = outFrameCreation(rtpName,inPayAmt,inJobNo,outCustName,zip);
                        //outframeSrc = outframeSrc + '&customerRefNum=' + inCustNo;
                        renderGateway = true;
                    }
                }
            }
        }
        return null;    	
    } 
    
    public PageReference search() {
        Boolean isError = false;
        renderJobDetails = false;
        renderGateway = false;
        disPayMe = false;
        renderComplete = false;
        if (Utilities.isEmptyOrNull(inCustNo)) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Customer number must be entered.'));
            isError = true;
        }
        if (Utilities.isEmptyOrNull(inJobNo)) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Job number must be entered.'));
            isError = true;
        }
        if (isError) {
            return null;
        }
        isError = searchCustomer(inCustNo);
        if (isError) {
            return null; 
        }
        isError = searchJob(inJobNo);
        if (isError) {
            return null; 
        }
        if (jobResponse.BillToCustNo != inCustNo){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Customer number ' +  inCustNo + ' does not match Job ' + inJobNo + ' Bill To Customer No ' + jobResponse.BillToCustNo));
            return null;
        }
        renderJobDetails = true;
        outDepositPaid = Utilities.currency(jobResponse.PrepayAmount);
        if (inClient != MOBILETECH)
            outOutstandingBalance = Utilities.currency(jobResponse.OutstandingBalance);
        else outOutstandingBalance = '';
        //getDetails();
        if (!Utilities.isEmptyOrNull(parmCustNo) && !Utilities.isEmptyOrNull(parmJobNo) && !Utilities.isEmptyOrNull(parmPayAmt)){
            disPayMe = true;
            payme();
        }  
        return null;    	
    } 
    public PageReference payme() {
        Boolean isError = false;
        renderGateway = false;
        renderComplete = false;
        if(showPayData == 'RealTimePay'){
            if (Utilities.isEmptyOrNull(inPayAmt)) {
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Payment amount must be entered.'));
                isError = true;
            }
            
            if (Utilities.isEmptyOrNull(parmPayAmt)){
                if ((Decimal.valueOf(inPayAmt.replace(',','')) > jobResponse.OutstandingBalance) && (Decimal.valueOf(inPayAmt.replace(',','')) <> savePayment)){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.WARNING, 'Payment amount exceeds Installation Due - Hit button to confirm payment amount.'));
                    savePayment = Decimal.valueOf(inPayAmt.replace(',',''));
                    return null;
                }
            }
            
            try {
                Decimal decTest = Decimal.valueOf(inPayAmt.replace(',',''));
                if (decTest <= 0) {
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Payment amount must be greater than zero.'));
                    isError = true;
                }
            }
            catch (Exception e) {
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Payment amount entered is invalid.'));
                isError = true;
            }
            
            if (isError) {
                return null;
            }
        }
        if(string.isBlank(zip)){
            zip = cRes.ZipCode;
        }
        rtp = createRTPRecord();
        Real_Time_Payment__c tempRTP = [select name from Real_Time_Payment__c where ID = :rtp.ID];
        //Call OutFrame
        outframeSrc = outFrameCreation(tempRTP.name,String.valueOf(rtp.PaymentAmount__c),inJobNo,outCustName,zip);
        //outframeSrc = outframeSrc + '&customerRefNum=' + inCustNo;
        renderGateway = true;
        if (!Utilities.isEmptyOrNull(parmPayAmt)){
            disPayMe = true;
        }
        return null;    	
    } 
    
    public void postPayment() { 
        String payTypeID;
        inTranJson = EncodingUtil.urlDecode(inTranJson,'UTF-8');
        tran = (TranJson) JSON.deserialize(inTranJson, TranJson.class);
        String mmbComment;
        if (!Utilities.isEmptyOrNull(inClient)){
            mmbComment += ' Client:' + inClient;
        }
        if (tran.ccType == 'Visa') payTypeID = 'VI';
        else if (tran.ccType == 'American Express') payTypeID = 'AM';
        else if (tran.ccType == 'Discover') payTypeID = 'DI';
        else if (tran.ccType == 'Mastercard') payTypeID = 'MC';
        //Call Customer Loan MMB Method
        if(showPayData == 'CustLoan'){
            mmbComment = 'Notes Paid: ' + Userinfo.getUserName() + ' TranID: ' + tran.transId + ' App: ' + tran.approvalCode;
            postPaymentPhone(tran,inTranJson,mmbComment,payTypeID);
        }   
        if(showPayData == 'RealTimePay'){
            mmbComment = 'RTP ' + Userinfo.getUserName() + ' TranID:' + tran.transId + ' App:' + tran.approvalCode;
            postPaymentField(tran,inTranJson,mmbComment,payTypeID);
        }
    }
    public PageReference postPaymentField(TranJson tran,string inTranJson,string mmbComment,string payTypeID){
        MMBCustomerSiteSearchApi.ProfileFetchMMBRequest profileInfo = new  MMBCustomerSiteSearchApi.ProfileFetchMMBRequest();
        profileInfo.customerRefNum = (Utilities.isEmptyOrNull(tran.customerRefNum)?inCustNo:tran.customerRefNum);
        profileInfo.transactionRefNum = tran.transId;
        profileInfo.customerNumber = inCustNo;
        profileInfo.jobNumber = inJobNo;
        profileInfo.sendOption = 'N';
        if (isTrip){
            profileInfo.payReasonID = '8';
        }
        else if(inClient == Telemar){
            profileInfo.payReasonID = '7';
        }
        else if (inClient == MobileTech){
            profileInfo.payReasonID = '9';
        }
        else{
            profileInfo.payReasonID = '10';
        }
        //IF this is Telemar, then Prepay = YES otherwise NO
        profileInfo.prePayFlag = (Utilities.isEmptyOrNull(inTelemarNo) ? 'N' : 'Y');
        profileInfo.payTypeID = payTypeID;
        profileInfo.accountNo = profileInfo.payTypeID;
        profileInfo.transactionDate  = Datetime.now().format('yyyy-MM-dd');
        profileInfo.transactionAmount = Decimal.valueOf(inPayAmt.replace(',',''));
        system.debug('Tran Amt');
        system.debug(profileInfo.transactionAmount);
        profileInfo.comment = mmbComment;
        profileInfo.appSource = 'SFDC';
        profileInfo.telemarID = inTelemarNo;
        profileInfo.authCode = tran.approvalCode;
        MMBCustomerSiteSearchApi.ProfileFetchMMBResponse mmbResp = MMBCustomerSiteSearchApi.LookupProfileFetchMMB(profileInfo);
        system.debug(mmbResp);
        system.debug('Post:' + mmbResp.status );
        if ( mmbResp.status.startsWith('Success' )){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully posted to MMB.'));
        }
        else{ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Error posting to MMB - Please contact IT support - ' + mmbResp.status));
            }
        updateRTPRecord(tran,inTranJson,mmbResp.status,'');
        renderGateway = false;
        renderComplete = true;
        disPayMe = true;
        return null;
    }
    public void updateRTPRecord(TranJson tran,string inTranJson,string status,string prePayNo){
        rtp.TransID__c = tran.transId;
        rtp.ApprovalCode__c = tran.appCode;
        rtp.Card_Name__c = tran.name;
        rtp.Card_Indicators__c = cardIndicators(tran);
        rtp.Card_Last_4__c = tran.ccNumber.substring((tran.ccNumber.length()-4),tran.ccNumber.length());
        rtp.Card_Type__c = tran.ccType;
        rtp.Card_Exp__c = tran.expMonth +'/' + tran.expYear;
        rtp.MMBPostStatus__c = status;
        rtp.Cust_Ref_ID__c = tran.customerRefNum;
        rtp.JSONResponse__c = inTranJson;
        rtp.PrePayNumber__c = prePayNo;
        rtp.Last_Status__c = 'Posted';
        if(inPayAmt != null && rtp.PaymentAmount__c!= null && rtp.PaymentAmount__c != Decimal.valueOf(inPayAmt.replace(',',''))){
            rtp.PaymentAmount__c = Decimal.valueOf(inPayAmt.replace(',',''));
        }
        update rtp;
    }
    public PageReference postPaymentPhone(TranJson tran,string inTranJson,string mmbComment,string payTypeID){
        string prePayNo;
        try{ 
            string getFinanceReqBody = '';
            string mockResponse;
            string errorMockResponse;
            HttpRequest req = new HttpRequest();
            HttpResponse financeResponse = new HttpResponse();
            Http h = new Http();
            String endPoint = IntegrationSettings__c.getinstance().FlexFinanceEndPoint__c.trim();
            String userName = IntegrationSettings__c.getinstance().DPUsername__c.trim();
            String password = IntegrationSettings__c.getinstance().DPPassword__c.trim();
            System.debug('The tran is'+tran);
            //Request Creation for Real Time Payment Cust Loan
            RealTimePaymentController.RealTimePaymentRequest custLoanReq = new RealTimePaymentController.RealTimePaymentRequest();
            custLoanReq.note = Integer.valueOf(custloanList[0].NoteNumber__c);
            custLoanReq.paymentType = payTypeID;
            custLoanReq.acctNo = tran.ccNumber.substring((tran.ccNumber.length()-4),tran.ccNumber.length());
            custLoanReq.custName = tran.name; 
            custLoanReq.expDate = tran.expMonth +''+ tran.expYear;
            custLoanReq.oneTimePayment = Decimal.valueOf(inPayAmt.replace(',',''));
            custLoanReq.authCode = tran.approvalCode;
            custLoanReq.comment = mmbComment;//'CUSTOMER NAME ' + Userinfo.getUserName() + ' TranID:' + tran.transId + ' App:' + tran.approvalCode;
            custLoanReq.profileId = tran.customerRefNum;
            if(userDetail.EmployeeNumber__c != null && userDetail.EmployeeNumber__c.isNumeric()){
                custLoanReq.changeUser = Integer.valueOf(userDetail.EmployeeNumber__c);
            }
            getFinanceReqBody = JSON.serialize(custLoanReq);            
            //End Request Creation
            //Web Service Callout
            Blob headerValue = Blob.valueOf(userName + ':' + password);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('Content-Type', 'application/json');
            req.setEndpoint(endPoint.trim());
            req.setMethod('POST');
            System.debug('The request is'+getFinanceReqBody);
            if(getFinanceReqBody != null){
                req.setBody(getFinanceReqBody);
            }
            if(String.isNotBlank(req.getBody())){
                financeResponse = h.send(req);  
            }
            //Web Service Callout Ends
            //Response Handling
            System.debug('The response is'+financeResponse.getBody());
            if(financeResponse != null && String.isNotBlank(financeResponse.getBody())){
                //Success
                if(financeResponse.getStatusCode() == 200){
                    RealTimePaymentController.RealTimeSuccessResponse result = (RealTimePaymentController.RealTimeSuccessResponse) System.JSON.deserialize(financeResponse.getBody(), RealTimePaymentController.RealTimeSuccessResponse.class);
                    prePayNo = result.prePayNo;
                    mmbStatus = 'Success';
                    //Failure
                }else if(financeResponse.getStatusCode() == 500 || financeResponse.getStatusCode() == 400 || financeResponse.getStatusCode() == 404 || financeResponse.getStatusCode() == 401){
                    RealTimePaymentErrorResponseJ2A errList = (RealTimePaymentErrorResponseJ2A) System.JSON.deserialize(financeResponse.getBody(),RealTimePaymentErrorResponseJ2A.class);
                    if(errList.errors.size() >0){
                        mmbStatus = errList.errors[0].errorMessage;
                    }
                }
            }
            //Update Real Time Payment Record
            updateRTPRecord(tran,inTranJson,mmbStatus,prePayNo);
            //Show Messages on page
            showButton(tran.transId,mmbStatus);
            renderGateway = false;
            renderComplete = true;
            disPayMe = true;
        }catch(exception e){
            if(string.isNotBlank(tran.transId)){
                updateRTPRecord(tran,inTranJson,mmbStatus,prePayNo);
                showButton(tran.transId,mmbStatus);
                renderGateway = false;
                renderComplete = true;
                disPayMe = true;
            }
            ADTApplicationMonitor.log ('Finanance Payment Failed', e.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI); 
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please reach out to Administrator.'));
        }
        return null;
    }
    public void showButton(string transId,string status){
        if(transId != null){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Payment made Successfully.'));
            //If mmb was posted
            if (string.isNotBlank(status) && status.containsIgnoreCase('Success' )){
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully posted to MMB.'));
                retryMMBButton = false;
                mmbSuccessFlag = true;
                //To refersh the loan only if payment and mmb was successful from field sales
                if(matrixDept == false && mmbSuccessFlag){
                    CustomerLoansRLController.getExistingLoans(accId,true);
                }
            }
            else{
                retryMMBButton = true;
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Error posting to MMB - Please contact IT support - ' + status));
            }
        }  
    }
    public void callCustomerRefresh(){
        //To refersh the loan only if payment aand mmb was successful from field sales
        if(matrixDept == false && mmbSuccessFlag){
            CustomerLoansRLController.getExistingLoans(accId,true);
        }
    }
    public PageReference cancelRequest() {
        rtp.Last_Status__c = 'Cancelled';
        update rtp;
        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.WARNING, 'Request has been cancelled.'));
        renderCancel = true;
        renderGateway = false;
        return null;
    }
    
    public PageReference errorResponse() {
        List<String> errors = inError.split('\\|');
        String errDesc;
        System.debug(errors+inGatewayCode+inGatewayMsg);
        Real_Time_Payment_Failure__c rtpError = new Real_Time_Payment_Failure__c();
        rtpError.Real_Time_Payment__c = rtp.ID;
        rtpError.Error_Codes__c = inError;
        rtpError.Error_Msgs__c = '';
        for (String err : errors){
            errDesc = null;
            try {
                errDesc = Paymentech_Codes__c.getInstance(err).User_Error__c;
            }
            catch (Exception e) {
            } 
            if (Utilities.isEmptyOrNull(errDesc))
                errDesc = 'Unknown error';
            errDesc = err + ' : ' + errDesc + ' - ' + EncodingUtil.urlDecode(inGatewayMsg,'UTF-8');
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, errDesc));
            rtpError.Error_Msgs__c += errDesc + '\n';
        }
        insert rtpError;
        rtp.Last_Status__c = 'Error';
        update rtp;
        return null;
    }
    private Boolean searchCustomer(String custNO) {
        Boolean returnFlag = false;
        MMBCustomerSiteSearchApi.CustomerInfoRequest customerInfoReq = new MMBCustomerSiteSearchApi.CustomerInfoRequest();
        customerInfoReq.FirstName 	= ' '; 
        customerInfoReq.LastName 	= ' ';
        customerInfoReq.Address1 	= ' ';
        customerInfoReq.City 		= ' ' ;
        customerInfoReq.State 		= ' ';
        customerInfoReq.ZipCode 	= ' ';
        customerInfoReq.Phone 		= ' ';
        customerInfoReq.CustomerNo 	= custNO;
        MMBCustomerSiteSearchApi.LookupCustomerResponse lookcRes = MMBCustomerSiteSearchApi.LookupCustomer(customerInfoReq);
        if(lookcRes!=null && lookcRes.Success){
            cRes = lookcRes.customerInfo[0];
            outCustName = cRes.FirstName + ' ' + cRes.LastName;
            outCustAddr = cRes.Address1 + ' ' + cRes.City + ', ' + cRes.State + ' ' + cRes.ZipCode;
        }
        else{
            if(lookcRes!=null) {
                if (lookcRes.ResultSummaryMessage == 'NOTFOUND') {
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No customer records were found.'));
                    returnFlag = true;
                }
                else {
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error while searching for customer information. '+lookcRes.ResultSummaryMessage));
                    returnFlag = true;
                }
            }
            else {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Unable to reach MMB.'));
                returnFlag = true;
            }
        }
        return returnFlag; 
    } 
    
    private Boolean searchJob(String jobNO) {
        Boolean returnFlag = false;
        MMBCustomerSiteSearchApi.JobInfoRequest jobInfo = new MMBCustomerSiteSearchApi.JobInfoRequest();
        jobInfo.JobNo = jobNO;
        jobResponse = MMBCustomerSiteSearchApi.GetJob( jobInfo );
        system.debug('JobInfo:' + jobResponse);
        
        if(jobResponse!=null && jobResponse.Success){
        }
        else{
            if(jobResponse!=null) {
                if (jobResponse.ErrorMessage == 'NOTFOUND') {
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Job was not found were found.'));
                    returnFlag = true;
                }
                else {
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error while searching for job. '+jobResponse.ErrorMessage));
                    returnFlag = true;
                }
            }
            else {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Unable to reach MMB.'));
                returnFlag = true;
            }
        }
        return returnFlag;   
    } 
    
    private String cardIndicators(TranJson tran) {
        String cardInd = '';
        if (tran.ctiAffluentCard == 'Y')
            cardInd += 'Affluent;';
        if (tran.ctiCommercialCard == 'Y')
            cardInd += 'Commercial;';
        if (tran.ctiDurbinExemption == 'Y')
            cardInd += 'DurbinExcempt;';
        if (tran.ctiHealthcareCard == 'Y')
            cardInd += 'Healthcare;';
        if (tran.ctiLevel3Eligible == 'Y')
            cardInd += 'Level3;';
        if (tran.ctiPayrollCard == 'Y')
            cardInd += 'Payroll;';
        if (tran.ctiPrepaidCard == 'Y')
            cardInd += 'Prepaid;';
        if (tran.ctiPINlessDebitCard == 'Y')
            cardInd += 'Debit-Pinless;';
        if (tran.ctiSignatureDebitCard == 'Y')
            cardInd += 'Debit-Signature;';
        cardInd += tran.ctiIssuingCountry;
        if (Utilities.isEmptyOrNull(cardInd))
            cardInd = 'None';
        return cardInd; 
    }
    
    public with sharing class TranJson {
        private String transId ;
        private String appCode ;
        private String name ;
        private String paymentType ;
        private String ccNumber ;
        private String expMonth ;
        private String expYear ;
        private String ccType ;
        private String cardBrandSelected ;
        private String CVVMatch ;
        private String AVSMatch ;
        private String tokenId ;
        private String approvalCode ;
        private String transactionStart ;
        private String transactionEnd ;
        private String hostedSecureID ;
        private String sessionId ;
        private String status ;
        private String amount ;
        private String customer_id ;
        private String customer_email ;
        private String extToken ;
        private String customerRefNum ;
        private String ctiAffluentCard ;
        private String ctiCommercialCard ;
        private String ctiDurbinExemption ;
        private String ctiHealthcareCard ;
        private String ctiLevel3Eligible ;
        private String ctiPayrollCard ;
        private String ctiPrepaidCard ;
        private String ctiPINlessDebitCard ;
        private String ctiSignatureDebitCard ;
        private String ctiIssuingCountry ;
        private String profileProcStatus ;
        private String profileProcStatusMsg ;
        private String garbage;
    }
    //Outframe Creation
    public string outFrameCreation(string name,String paymentAmount,string inJobNo,string outCustName,string zipCode){
        string outFrame;
        outFrame = IntegrationSettings__c.getinstance().Paymentech_URL__c + '?';	
        outFrame = outFrame + 'hostedSecureID=' + IntegrationSettings__c.getinstance().Paymentech_HostedSecureID__c;
        outFrame = outFrame + '&action=buildForm' ;
        outFrame = outFrame + '&sessionId='+ name;
        outFrame = outFrame + '&hosted_tokenize=store_authorize' ;
        outFrame = outFrame + '&payment_type=' + (isECP? 'ECP' : 'Credit_Card') ;
        outFrame = outFrame + '&formType=0' ;
        outFrame = outFrame + '&allowed_types=American Express|Discover|MasterCard|Visa' ;
        outFrame = outFrame + '&trans_type=auth_capture' ;
        outFrame = outFrame + '&required=minimum' ;
        outFrame = outFrame + '&collectAddress=3' ;
        outFrame = outFrame + '&amount=' + paymentAmount ;
        outFrame = outFrame + '&orderId=' + name + '-' + inJobNo;
        outFrame = outFrame + '&name=' + outCustName; 
        outFrame = outFrame + '&cardIndicators=Y';
        outFrame = outFrame + '&zip=' + zipCode;  
        return outFrame;
    }
    public Real_Time_Payment__c createRTPRecord(){
        rtp = new Real_Time_Payment__c();        
        rtp.MMBCustomerNo__c = inCustNo;
        rtp.MMBJobNo__c = inJobNo;
        rtp.TelemarNo__c = inTelemarNo;
        if(inPayAmt !=null){
            rtp.PaymentAmount__c = Decimal.valueOf(inPayAmt.replace(',',''));
        }
        rtp.NoteNumber__c = noteNumber;
        rtp.Client__c = inClient;
        insert rtp;
        return rtp;
    }
    //MMB Request Schema for Cust Loan
    public class RealTimePaymentRequest{
        integer note;
        string paymentType;
        string acctNo;
        string custName;
        string expDate;
        Decimal oneTimePayment;
        string authCode;
        string comment;
        string profileId;
        integer changeUser;
    }
    public class RealTimeSuccessResponse{
        integer returnCode;
        string message;
        string prePayNo;
    }
}