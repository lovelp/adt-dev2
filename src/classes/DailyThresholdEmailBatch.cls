/************************************* MODIFICATION LOG ********************************************************************************************
* DailyThresholdEmailBatch
* 
* DESCRIPTION : Batch class that sends out an email once a day with updates on account and lead thresholds from the previous day  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*Erin McGee                       06/21/2013      - Original Version
*Jaydip Bhattacharya              06/25/2013      - For sprint#12, added queue monitoring 
* Jaydip Bhattacharya			  07/19/2013	  - Modifying this as per conversation with John Havelka. Removed usage of Daily_Thresholds__c and Daily_Threshold__c objects.                            
*                          
*/
global class DailyThresholdEmailBatch implements Database.Batchable<sObject>,Database.Stateful,Database.AllowsCallouts, Schedulable {
    
    global string emailBodyText='';
    global  string emailrecp;
    global  Datetime dt = DateTime.newInstance(system.today().addDays(-1).year(), system.today().addDays(-1).month(), system.today().addDays(-1).day());
    global  String dowtoday=dt.format('EEEE');
    private final List<Account> dailyThresholdDelete;
    public String query='SELECT Name from Monitor_Queues__c where Active__c=true order by Threshold_Comparator__c,Name';
           /*
            Daily_Thresholds__c thresholds=[SELECT Email_Addresses__c, Admin_Threshold_Friday__c, Admin_Threshold_Monday__c, Admin_Threshold_Saturday__c, Admin_Threshold_Sunday__c, Admin_Threshold_Thursday__c, Admin_Threshold_Tuesday__c, Admin_Threshold_Wednesday__c, 
        BUDCO_CA_Threshold_Friday__c, BUDCO_CA_Threshold_Monday__c, BUDCO_CA_Threshold_Saturday__c, BUDCO_CA_Threshold_Sunday__c, BUDCO_CA_Threshold_Thursday__c, BUDCO_CA_Threshold_Tuesday__c, BUDCO_CA_Threshold_Wednesday__c, 
        BUDCO_Not_CA_Threshold_Friday__c, BUDCO_Not_CA_Threshold_Monday__c, BUDCO_Not_CA_Threshold_Saturday__c, BUDCO_Not_CA_Threshold_Sunday__c, BUDCO_Not_CA_Threshold_Thursday__c, BUDCO_Not_CA_Threshold_Tuesday__c, BUDCO_Not_CA_Threshold_Wednesday__c, 
        Broadview_Threshold_Friday__c, Broadview_Threshold_Monday__c, Broadview_Threshold_Saturday__c, Broadview_Threshold_Sunday__c, Broadview_Threshold_Thursday__c, Broadview_Threshold_Tuesday__c, Broadview_Threshold_Wednesday__c, 
        Canadian_AS400_Threshold_Friday__c, Canadian_AS400_Threshold_Monday__c, Canadian_AS400_Threshold_Saturday__c, Canadian_AS400_Threshold_Sunday__c, Canadian_AS400_Threshold_Thursday__c, Canadian_AS400_Threshold_Tuesday__c, Canadian_AS400_Threshold_Wednesday__c, 
        Informix_Threshold_Friday__c, Informix_Threshold_Monday__c, Informix_Threshold_Saturday__c, Informix_Threshold_Sunday__c, Informix_Threshold_Thursday__c, Informix_Threshold_Tuesday__c, Informix_Threshold_Wednesday__c, 
        MMB_Threshold_Friday__c, MMB_Threshold_Monday__c, MMB_Threshold_Saturday__c, MMB_Threshold_Sunday__c, MMB_Threshold_Thursday__c, MMB_Threshold_Tuesday__c, MMB_Threshold_Wednesday__c, 
        RIF_Threshold_Friday__c, RIF_Threshold_Monday__c, RIF_Threshold_Saturday__c, RIF_Threshold_Sunday__c, RIF_Threshold_Thursday__c, RIF_Threshold_Tuesday__c, RIF_Threshold_Wednesday__c FROM Daily_Thresholds__c];
		*/
    /*
    global DailyThresholdEmailBatch() 
    {
        dailyThresholdDelete=new List<Account>();
        for(Account d: Database.query(query))
        {
            dailyThresholdDelete.add(d);
        }
    }
    */

    global void execute(SchedulableContext SC)
    {
        DailyThresholdEmailBatch z= new DailyThresholdEmailBatch();
        Database.executeBatch(z);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

           /*
            
            
            String[] SendTo = new String[]{};
            for(String email:thresholds.Email_Addresses__c.split(','))
            {
                SendTo.add(email);
            }
                if(Database.query(query).isEmpty())
        {
                        EmailMessageUtilities.SendEmailNotification(SendTo, 'Threshold Errors', 'No accounts or leads were added yesterday.');
                                            Daily_Threshold__c dailyThreshold= new Daily_Threshold__c(Admin__c=0, BUDCO_CA__c=0, BUDCO_Not_CA__c=0, Broadview__c=0, Canadian_AS400__c=0, Informix__c=0, MMB__c=0, RIF__c=0);
        insert dailyThreshold;

        }
        */
        
        return Database.getQueryLocator(query);
    
        
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) 
    {
        /*
        boolean sendAdminEmail=false;
        boolean sendBUDCOCAEmail=false;
        boolean sendBUDCONotCAEmail=false;
        boolean sendBroadviewEmail=false;
        boolean sendCanadaEmail=false;
        boolean sendInformixEmail=false;
        boolean sendMMBEmail=false;
        boolean sendRIFEmail=false;
        Decimal AdminThreshold=0;
        Decimal BUDCOCAThreshold=0;
        Decimal BUDCONOTCAThreshold=0;
        Decimal BroadviewThreshold=0;
        Decimal CanadianThreshold=0;
        Decimal InformixThreshold=0;
        Decimal MMBThreshold=0;
        Decimal RIFThreshold=0;
        DateTime yesterday=DateTime.now().addDays(-1);
        
        if (dowYesterday == 'Sunday')
        {
            AdminThreshold=thresholds.Admin_Threshold_Sunday__c;
            BUDCOCAThreshold = thresholds.BUDCO_CA_Threshold_Sunday__c;
            BUDCONOTCAThreshold = thresholds.BUDCO_Not_CA_Threshold_Sunday__c;
            BroadviewThreshold = thresholds.Broadview_Threshold_Sunday__c;
            CanadianThreshold = thresholds.Canadian_AS400_Threshold_Sunday__c;
            InformixThreshold = thresholds.Informix_Threshold_Sunday__c;
            MMBThreshold = thresholds.MMB_Threshold_Sunday__c;
            RIFThreshold = thresholds.RIF_Threshold_Sunday__c;
        }
        else if (dowYesterday == 'Monday')
        {
            AdminThreshold=thresholds.Admin_Threshold_Monday__c;
            BUDCOCAThreshold = thresholds.BUDCO_CA_Threshold_Monday__c;
            BUDCONOTCAThreshold = thresholds.BUDCO_Not_CA_Threshold_Monday__c;
            BroadviewThreshold = thresholds.Broadview_Threshold_Monday__c;
            CanadianThreshold = thresholds.Canadian_AS400_Threshold_Monday__c;
            InformixThreshold = thresholds.Informix_Threshold_Monday__c;
            MMBThreshold = thresholds.MMB_Threshold_Monday__c;
            RIFThreshold = thresholds.RIF_Threshold_Monday__c;
        }
        else if (dowYesterday == 'Tuesday')
        {
            AdminThreshold=thresholds.Admin_Threshold_Tuesday__c;
            BUDCOCAThreshold = thresholds.BUDCO_CA_Threshold_Tuesday__c;
            BUDCONOTCAThreshold = thresholds.BUDCO_Not_CA_Threshold_Tuesday__c;
            BroadviewThreshold = thresholds.Broadview_Threshold_Tuesday__c;
            CanadianThreshold = thresholds.Canadian_AS400_Threshold_Tuesday__c;
            InformixThreshold = thresholds.Informix_Threshold_Tuesday__c;
            MMBThreshold = thresholds.MMB_Threshold_Tuesday__c;
            RIFThreshold = thresholds.RIF_Threshold_Tuesday__c;
        }
        else if (dowYesterday == 'Wednesday')
        {
            AdminThreshold=thresholds.Admin_Threshold_Wednesday__c;
            BUDCOCAThreshold = thresholds.BUDCO_CA_Threshold_Wednesday__c;
            BUDCONOTCAThreshold = thresholds.BUDCO_Not_CA_Threshold_Wednesday__c;
            BroadviewThreshold = thresholds.Broadview_Threshold_Wednesday__c;
            CanadianThreshold = thresholds.Canadian_AS400_Threshold_Wednesday__c;
            InformixThreshold = thresholds.Informix_Threshold_Wednesday__c;
            MMBThreshold = thresholds.MMB_Threshold_Wednesday__c;
            RIFThreshold = thresholds.RIF_Threshold_Wednesday__c;
        }
        else if (dowYesterday == 'Thursday')
        {
            AdminThreshold=thresholds.Admin_Threshold_Thursday__c;
            BUDCOCAThreshold = thresholds.BUDCO_CA_Threshold_Thursday__c;
            BUDCONOTCAThreshold = thresholds.BUDCO_Not_CA_Threshold_Thursday__c;
            BroadviewThreshold = thresholds.Broadview_Threshold_Thursday__c;
            CanadianThreshold = thresholds.Canadian_AS400_Threshold_Thursday__c;
            InformixThreshold = thresholds.Informix_Threshold_Thursday__c;
            MMBThreshold = thresholds.MMB_Threshold_Thursday__c;
            RIFThreshold = thresholds.RIF_Threshold_Thursday__c;
        }
        else if (dowYesterday == 'Friday')
        {
            AdminThreshold=thresholds.Admin_Threshold_Friday__c;
            BUDCOCAThreshold = thresholds.BUDCO_CA_Threshold_Friday__c;
            BUDCONOTCAThreshold = thresholds.BUDCO_Not_CA_Threshold_Friday__c;
            BroadviewThreshold = thresholds.Broadview_Threshold_Friday__c;
            CanadianThreshold = thresholds.Canadian_AS400_Threshold_Friday__c;
            InformixThreshold = thresholds.Informix_Threshold_Friday__c;
            MMBThreshold = thresholds.MMB_Threshold_Friday__c;
            RIFThreshold = thresholds.RIF_Threshold_Friday__c;
        }
        else if (dowYesterday == 'Saturday')
        {
            AdminThreshold=thresholds.Admin_Threshold_Saturday__c;
            BUDCOCAThreshold = thresholds.BUDCO_CA_Threshold_Saturday__c;
            BUDCONOTCAThreshold = thresholds.BUDCO_Not_CA_Threshold_Saturday__c;
            BroadviewThreshold = thresholds.Broadview_Threshold_Saturday__c;
            CanadianThreshold = thresholds.Canadian_AS400_Threshold_Saturday__c;
            InformixThreshold = thresholds.Informix_Threshold_Saturday__c;
            MMBThreshold = thresholds.MMB_Threshold_Saturday__c;
            RIFThreshold = thresholds.RIF_Threshold_Saturday__c;
        }
        system.debug(AdminThreshold+' '+BUDCOCAThreshold+' '+BUDCONOTCAThreshold+' '+BroadviewThreshold+' '+CanadianThreshold+' '+InformixThreshold+' '+MMBThreshold+' '+RIFThreshold);
        for(Daily_Threshold__c daily:(List<Daily_Threshold__c>)(scope))
            {
                if(daily.Admin__c<AdminThreshold && AdminThreshold!=0)
                {
                    sendAdminEmail=true;
                    emailBodyText+='Admin threshold was not met. Only '+Integer.valueOf(daily.Admin__c)+' addresses of an expected minimum '+AdminThreshold+' were added.<br/><br/>';

                }
                if(daily.BUDCO_CA__c<BUDCOCAThreshold && BUDCOCAThreshold!=0)
                {
                    sendBUDCOCAEmail=true;
                    emailBodyText+='BUDCO CA threshold was not met. Only '+Integer.valueOf(daily.BUDCO_CA__c)+' leads of an expected minimum '+BUDCOCAThreshold+' were added.<br/><br/>';
                }
                if(daily.BUDCO_Not_CA__c<BUDCONOTCAThreshold && BUDCONOTCAThreshold!=0)
                {
                    sendBUDCONotCAEmail=true;
                    emailBodyText+='BUDCO Not CA threshold was not met. Only '+Integer.valueOf(daily.BUDCO_Not_CA__c)+' leads of an expected minimum '+BUDCONOTCAThreshold+' were added.<br/><br/>';
                }
                if(daily.Broadview__c<BroadviewThreshold && BroadviewThreshold!=0)
                {
                    sendBroadviewEmail=true;
                    emailBodyText+='Broadview threshold was not met. Only '+Integer.valueOf(daily.Broadview__c)+' addresses of an expected minimum '+BroadviewThreshold+' were added.<br/><br/>';
                }
                if(daily.Canadian_AS400__c<CanadianThreshold && CanadianThreshold!=0)
                {
                    sendCanadaEmail=true;
                    emailBodyText+='Canadian AS400 threshold was not met. Only '+Integer.valueOf(daily.Canadian_AS400__c)+' addresses of an expected minimum '+CanadianThreshold+' were added.<br/><br/>';
                }
                if(daily.Informix__c<InformixThreshold && InformixThreshold!=0)
                {
                    sendInformixEmail=true;
                    emailBodyText+='Informix threshold was not met. Only '+Integer.valueOf(daily.Informix__c)+' addresses of an expected minimum '+InformixThreshold+' were added.<br/><br/>';
                }
                if(daily.MMB__c<MMBThreshold && MMBThreshold!=0)
                {
                    sendMMBEmail=true;
                    emailBodyText+='MMB threshold was not met. Only '+Integer.valueOf(daily.MMB__c)+' addresses of an expected minimum '+MMBThreshold+' were added.<br/><br/>';
                }
                if(daily.RIF__c<RIFThreshold && RIFThreshold!=0)
                {
                    sendRIFEmail=true;
                    emailBodyText+='RIF threshold was not met. Only '+Integer.valueOf(daily.RIF__c)+' addresses of an expected minimum '+RIFThreshold+' were added.<br/><br/>';
                }
        }
        system.debug(emailBodyText);
        */
        
        //Jaydip B added below part 
        
             
		for (sObject sRec:scope){
            
            Monitor_Queues__c monrec=(Monitor_Queues__c)sRec;
            
            string strQ=monrec.Name;
            
            Monitor_Queues__c mnQry=Monitor_Queues__c.getInstance(strQ);
            
            if (strQ <> 'Email Recipients'){
            string dbQry=mnQry.Query__c;
            List<sObject> QResult = new List<sObject>();
            try {
            QResult = Database.query(dbQry);
            
            Integer thrsld;
            
            if (dowtoday=='Sunday'){
            	thrsld=Integer.valueof(mnQry.Sunday_Threshold__c);
            }
            else if(dowtoday=='Monday') {
            	thrsld=Integer.valueof(mnQry.Monday_Threshold__c);
            }
            else if(dowtoday=='Tuesday') {
            	thrsld=Integer.valueof(mnQry.Tuesday_Threshold__c);
            }
            else if(dowtoday=='Wednesday') {
            	thrsld=Integer.valueof(mnQry.Wednesday_Threshold__c);
            }
            else if(dowtoday=='Thursday') {
            	thrsld=Integer.valueof(mnQry.Thursday_Threshold__c);
            }
            else if(dowtoday=='Friday') {
            	thrsld=Integer.valueof(mnQry.Friday_Threshold__c);
            }
            else if(dowtoday=='Saturday') {
            	thrsld=Integer.valueof(mnQry.Saturday_Threshold__c);
            }
            
            
            if (thrsld==null){
            	thrsld=Integer.valueof(mnQry.Threshold_Limit__c);
            }
           
            
            
            if (mnQry.Threshold_Comparator__c=='>' && QResult.size()>thrsld){
                emailBodyText+='Threshold exceeded for '+strQ+'. Currently '+QResult.size() +' are queued, threshold size is '+ thrsld +'<br/><br/>';
            }
            else if(mnQry.Threshold_Comparator__c=='<' && QResult.size()<thrsld ) {
            	emailBodyText+=strQ  +' threshold was not met.'+thrsld+'  minimum expected. But only '+QResult.size()+' were added.<br/><br/>';
            }
            
            else if(mnQry.Threshold_Comparator__c=='R'  ) {
            	emailBodyText+=QResult.size() +' Record found with query: ' + dbQry +  '.<br/><br/>';
            }
                        
            }
            catch(Exception ex){
            	emailBodyText+='Error encountered while executing query: <b>' + dbQry +  '.</b> Error -'+ ex.getMessage() + '. <br/><br/>';
            }
            }
            else {
            	
            	emailrecp=mnQry.Query__c;
            }
        }
        
        
        
        //End of changes by Jaydip B
    
        
        
        
        
       
        //Daily_Threshold__c dailyThreshold= new Daily_Threshold__c(Admin__c=0, BUDCO_CA__c=0, BUDCO_Not_CA__c=0, Broadview__c=0, Canadian_AS400__c=0, Informix__c=0, MMB__c=0, RIF__c=0);
       // insert dailyThreshold; 

    }
    
    global void finish(Database.BatchableContext BC) 
    {
        //if(!dailyThresholdDelete.isEmpty())
        //{
           // delete dailyThresholdDelete;
            //dailyThresholdDelete.clear();
        //}
        
        
         if(emailBodyText!='')
        {
            emailBodyText='The following thresholds were not met yesterday, '+dt.format('MMMM dd, yyyy')+'.<br/><br/>'+emailBodyText;
            String[] SendTo = new String[]{};
            for(String email:emailrecp.split(','))
            {
                SendTo.add(email);
                 
            }
            EmailMessageUtilities.SendEmailNotification(SendTo, 'Threshold Errors', emailBodyText);
        }
        
    }
    
}