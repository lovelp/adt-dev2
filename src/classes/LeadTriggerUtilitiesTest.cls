/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : LeadTriggerUtilitiesTest is a test class for LeadTriggerUtilities.cls
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            02/29/2012          - Origininal Version
*
*                                                    
*/
@isTest
private class LeadTriggerUtilitiesTest 
{
    
    static testMethod void testprocessPostalCode()
    {
        Test.startTest();
        
        Lead L1 = new Lead();
        L1.LastName = 'TestLastName';
        L1.Company = 'TestCompany';
        L1.SiteStreet__c = '2251 Pimmit Drive';
        L1.SiteCity__c = 'Falls Church';        
        L1.SiteStateProvince__c = 'VA';
        L1.SitePostalCode__c = '221o2';
        L1.RecordTypeId = Utilities.getRecordType(RecordTypeName.USER_ENTERED_LEAD, 'Lead').Id;
        
        LeadTriggerUtilities.processPostalCode(L1);
        System.assert(L1.SitePostalCode__c == '221o2', 'The Expected Value was - 221o2');
        System.assert(L1.SiteCountryCode__c == 'US', 'The Expected Value was - US');
                    
        L1.SitePostalCode__c = '221 ONT';
        LeadTriggerUtilities.processPostalCode(L1);
        System.assert(L1.SitePostalCode__c == '221', 'The Expected Value was - 221');
        System.assert(L1.SitePostalCodeAddOn__c == 'ONT', 'The Expected Value was - ONT');
        System.assert(L1.SiteCountryCode__c == 'CA', 'The Expected Value was - CA');
        
        L1.SitePostalCode__c = '22    1';
        L1.SiteCountryCode__c = null;
        LeadTriggerUtilities.processPostalCode(L1);     
        System.assert(L1.SiteCountryCode__c == null, 'The Expected Value was - null');
        
        L1.SitePostalCode__c = '221o2-2345';
        LeadTriggerUtilities.processPostalCode(L1);     
        System.assert(L1.SiteCountryCode__c == 'US', 'The Expected Value was - US');
        System.assert(L1.SitePostalCode__c == '221o2', 'The Expected Value was - 221o2');
        System.assert(L1.SitePostalCodeAddOn__c == '2345', 'The Expected Value was - 2345');
        
        L1.SitePostalCode__c = '221o22345';
        L1.LeadSource = 'Telemar Rehash';
        LeadTriggerUtilities.processPostalCode(L1);     
        System.assert(L1.SiteCountryCode__c == 'US', 'The Expected Value was - US');
        System.assertEquals('221o2', L1.SitePostalCode__c, 'The Expected Value was - 221o2');
        System.assertEquals('2345', L1.SitePostalCodeAddOn__c, 'The Expected Value was - 2345');
        
        L1.SitePostalCode__c = '123456';
        L1.LeadSource = 'Telemar Rehash';
        LeadTriggerUtilities.processPostalCode(L1);     
        System.assert(L1.SiteCountryCode__c == 'CA', 'The Expected Value was - CA');
        System.assertEquals('123', L1.SitePostalCode__c, 'The Expected Value was - 123');
        System.assertEquals('456', L1.SitePostalCodeAddOn__c, 'The Expected Value was - 456');
        
        Test.stopTest();
    }
    
    static testMethod void testsetSiteAddress()
    {
        //hello
        Test.startTest();
        ErrorMessages__c eMes = new ErrorMessages__c();
        eMes.Name = 'Missing_Address';
        eMes.Message__c = 'Address missing.';
        insert eMes;
        Address__c newAddress;
        
        Lead L1 = new Lead();
        L1.LastName = 'TestLastName';
        L1.Company = 'TestCompany';
        L1.SiteStreet__c = '2251 Pimmit Drive';
        L1.SiteCity__c = 'Falls Church';        
        L1.SiteStateProvince__c = 'VA';
        L1.SitePostalCode__c = '221o21234';
        L1.SiteCountryCode__c = 'US';
        
        newAddress = LeadTriggerUtilities.setSiteAddress(L1);
        System.assert(newAddress.PostalCode__c == '221o21234', 'The Expected Value was - 221o21234');
        System.assert(newAddress.PostalCodeAddOn__c == null, 'The Expected Value was - NULL');
                
        L1.SiteStreet__c = null;
        L1.SiteCity__c = '';
        L1.SitePostalCode__c = '';
        L1.SiteCountryCode__c = '';
        newAddress = LeadTriggerUtilities.setSiteAddress(L1);
        System.assert(newAddress.Street__c == 'FAILED', 'There is an exception in setting the SiteAddress');
        
        Test.stopTest();
    }
    
    static testMethod void testsetSiteAddressIds()
    {
        Test.startTest();
        ErrorMessages__c eMes = new ErrorMessages__c();
        eMes.Name = 'Bad_Address';
        eMes.Message__c = 'There was an error finding address in salesforce. Insert failed.';
        insert eMes;
        map<string, Address__c> newAddresses = new map<string, Address__c>();
        list<Lead> newLeads = new list<Lead>();
        
        Lead L1 = new Lead();
        L1.LastName = 'TestLastName';
        L1.Company = 'TestCompany';
        L1.SiteStreet__c = '2251 Pimmit Drive';
        L1.SiteCity__c = 'Falls Church';        
        L1.SiteStateProvince__c = 'VA';
        L1.SitePostalCode__c = '22102';
        L1.SiteCountryCode__c = 'US';
        newLeads.add(L1);       
        
        Address__c newAddress1 = LeadTriggerUtilities.setSiteAddress(L1);
        newAddresses.put(newAddress1.OriginalAddress__c, newAddress1);
        
        Lead L2 = new Lead();
        L2.LastName = 'TestLastName';
        L2.Company = 'TestCompany';
        L2.SiteStreet__c = '1521 Spring Gate Dr';
        L2.SiteCity__c = 'Falls Church';        
        L2.SiteStateProvince__c = 'VA';
        L2.SitePostalCode__c = '22102';
        L2.SiteCountryCode__c = 'US';       
        newLeads.add(L2);   
        
        Address__c newAddress2 = LeadTriggerUtilities.setSiteAddress(L2);
        newAddresses.put(newAddress2.OriginalAddress__c, newAddress2);
        
        newAddresses = AddressHelper.upsertAddresses(newAddresses);         
        LeadTriggerUtilities.setSiteAddressIds(newAddresses, newLeads);
        
        System.assert(L1.AddressID__c != null, 'Address Id should not be null on Lead');
        System.assert(L2.AddressID__c != null, 'Address Id should not be null on Lead');
        
        
        L2.SiteCity__c = null;
        newLeads.add(L2);       
        LeadTriggerUtilities.setSiteAddressIds(newAddresses, newLeads);
        
        L2.SiteCity__c = 'TROY';
        newLeads.add(L2);       
        LeadTriggerUtilities.setSiteAddressIds(newAddresses, newLeads);
        
        Test.stopTest();
    }
    
    static testMethod void testsetPostalCodeLookup()
    {
        Test.startTest();
        ErrorMessages__c eMes = new ErrorMessages__c();
        eMes.Name = 'No_Zipcode_Match';
        eMes.Message__c = 'No zipcode found for assignment. Insert failed.';
        insert eMes;
        Postal_Codes__c p = new  Postal_Codes__c();
        p.Name = '221o2';
        p.BusinessID__c = '1100';
        insert p;
        
        list<Lead> newLeads = new list<Lead>();
        
        Lead L1 = new Lead();
                        
        L1.SiteStreet__c = '2251 Pimmit Drive';
        L1.SiteCity__c = 'Falls Church';        
        L1.SiteStateProvince__c = 'VA';
        L1.SitePostalCode__c = '221o2';
        L1.SiteCountryCode__c = 'US';
        L1.DispositionCode__c = 'Phone - Left Msg';
        L1.LastName = '';
        L1.Channel__c = 'Custom Home';
        L1.Business_Id__c = '1100 - Residential';
        newLeads.add(L1);   
        
        LeadTriggerUtilities.setPostalCodeLookup(newLeads);
        System.assert(L1.PostalCodeID__c != null, 'The Postal Code Lookup on Lead should not be Null');
        
        L1.SitePostalCode__c = null;
        LeadTriggerUtilities.setPostalCodeLookup(newLeads);
        System.assert(L1.PostalCodeID__c == null, 'The Postal Code Lookup on Lead should be Null');
        
        Test.stopTest();
        
    }
    
    static testMethod void testcreateDispositionOnLead()
    {
        
        List<Disposition__c> dispList;
        Lead L1;        
        TestHelperClass.createSalesRepUser();        
        TestHelperClass.createReferenceUserDataForTestClasses();
        user adminuser = TestHelperClass.createAdminUser();      
        User salesMgr = TestHelperClass.createManagerUser();
        System.runAs(adminuser) 
        {
            TestHelperClass.createReferenceDataForTestClasses();
            L1 = TestHelperClass.createLead(salesMgr, false, 'BUDCO');
			L1.NewMoverType__c = 'RL';
			insert L1;
        
	        dispList = [select Id, DispositionDate__c, DispositionType__c, DispositionDetail__c, Comments__c, TelemarAccountNumber__c, LeadID__r.Repeat_Disposition__c from Disposition__c where LeadID__c = : L1.Id];
	        system.debug('### 1 dispList='+dispList);
	        
	        Test.startTest();
	
	        L1.DispositionCode__c = 'Knocked';
	        L1.DispositionDetail__c = 'Knocked - Vacant';
	        
	        update L1;      
	        
	        dispList = [select Id, DispositionDate__c, DispositionType__c, DispositionDetail__c, Comments__c, TelemarAccountNumber__c, LeadID__r.Repeat_Disposition__c from Disposition__c where LeadID__c = : L1.Id];
	        system.debug('### 2 dispList='+dispList);
	        System.assert(dispList != null, 'List of dispositions should not be null');
	        System.assertEquals(1, dispList.size(), 'List of dispositions should have one item');
	        
	        Test.stopTest();
	        
	        system.debug('==> '+dispList);
        }
    }
    
    /*static testMethod void testsetSalesgenieLeadValidation()
    {
        Test.startTest();
        
        Lead L1;
        Lead L2;
        
        User salesMgr = TestHelperClass.createManagerUser();
        System.runAs(salesMgr) 
        {
            L1 = TestHelperClass.createLead(salesMgr,true, 'Salesgenie.com');
            
            try
            {
                L2 = TestHelperClass.createLead(salesMgr,true, 'Salesgenie.com');
            }
            Catch(Exception Ex)
            {
                System.assert(Ex.getMessage().contains('This Lead is already imported into Salesforce'));
            }           
        }       
        Test.stopTest();
    }*/
    
    static testMethod void testsetCompanyAndLastName()
    {
        Test.startTest();
        ErrorMessages__c eMes = new ErrorMessages__c();
        eMes.Name = 'LastName_Company_Required';
        eMes.Message__c = 'Either Last Name or Company is required for Lead insert';
        insert eMes;
        list<Lead> newLeads = new list<Lead>();
        
        Lead L1 = new Lead();
                        
        L1.LastName = '';
        L1.Company = 'Test Company';                
        LeadTriggerUtilities.setCompanyAndLastName(L1);
        System.debug('L1.LastName..:'+L1.LastName);
        System.assert(L1.LastName == 'Test Company', 'The expected value for Last Name is - Test Company');
        
        L1.LastName = 'Test Last Name';
        L1.Company = '';
        LeadTriggerUtilities.setCompanyAndLastName(L1);
        System.assert(L1.Company == 'Test Last Name', 'The expected value for Company is - Test Last Name');
        
        
        L1.LastName = '';
        L1.Company = '';
        LeadTriggerUtilities.setCompanyAndLastName(L1);
        
        Test.stopTest();
        
    }
    
    static testMethod void testCreateDispositionsTwoCINotes()
    {
        String dispDelimiter = '~';
        String dataDelimiter = '|';
        String note1 = 'Customer prefers morning appointments';
        String date1 = '08/28/2012';
        String note2 = 'Customer has large dog';
        String date2 = '08/25/2012';
        Id leadID = '00QV0000003xyYU';
        
        String input = dispDelimiter + note1 + dataDelimiter + date1 + 
                        dispDelimiter + note2 + dataDelimiter + date2;
        
        Test.startTest();
        
        List<Disposition__c> dispList = LeadTriggerUtilities.createDispositions(leadID, input);
        
    
        Test.stopTest();
        
        System.assertEquals(2, dispList.size());
        System.assertEquals(IntegrationConstants.DEFAULT_NSC_DISPOSITION, dispList[0].DispositionType__c);
        System.assertEquals('CI Note', dispList[0].DispositionDetail__c);
        System.assertEquals(note1, dispList[0].Comments__c);
        System.assertEquals(Date.parse(date1), dispList[0].DispositionDate__c.date());
        System.assertEquals(leadID, dispList[0].LeadID__c);
        
        System.assertEquals(IntegrationConstants.DEFAULT_NSC_DISPOSITION, dispList[1].DispositionType__c);
        System.assertEquals(IntegrationConstants.DISPOSITION_DETAIL_CI_NOTE, dispList[1].DispositionDetail__c);
        System.assertEquals(note2, dispList[1].Comments__c);
        System.assertEquals(Date.parse(date2), dispList[1].DispositionDate__c.date());
        System.assertEquals(leadID, dispList[1].LeadID__c);
        
        
        
    }
    
    static testMethod void testCreateDispositionsOneCINote()
    {
        String dispDelimiter = '~';
        String dataDelimiter = '|';
        String note1 = 'Customer prefers morning appointments';
        String date1 = '08/28/2012';
        Id leadID = '00QV0000003xyYU';
        
        String input = dispDelimiter + note1 + dataDelimiter + date1;
        
        Test.startTest();
        
        List<Disposition__c> dispList = LeadTriggerUtilities.createDispositions(leadID, input);
        
    
        Test.stopTest();
        
        System.assertEquals(1, dispList.size());
        System.assertEquals(IntegrationConstants.DEFAULT_NSC_DISPOSITION, dispList[0].DispositionType__c);
        System.assertEquals(IntegrationConstants.DISPOSITION_DETAIL_CI_NOTE, dispList[0].DispositionDetail__c);
        System.assertEquals(note1, dispList[0].Comments__c);
        System.assertEquals(Date.parse(date1), dispList[0].DispositionDate__c.date());
        System.assertEquals(leadID, dispList[0].LeadID__c);
        
        
    }
    
    static testMethod void testCreateDispositionsNullString()
    {
        Id leadID = '00QV0000003xyYU';
        
        String input = null;
        
        Test.startTest();
        
        List<Disposition__c> dispList = LeadTriggerUtilities.createDispositions(leadID, input);
        
    
        Test.stopTest();
        
        System.assertEquals(0, dispList.size());
    }
    
    static testMethod void testCreateDispositionsZeroLengthString()
    {
        Id leadID = '00QV0000003xyYU';
        
        String input = '';
        
        Test.startTest();
        
        List<Disposition__c> dispList = LeadTriggerUtilities.createDispositions(leadID, input);
        
    
        Test.stopTest();
        
        System.assertEquals(0, dispList.size());
    }
    
    static testMethod void testGetMostRecentDispositionNullString() {
        
        Id leadID = '00QV0000003xyYU';
        
        String input = null;
        
        Test.startTest();
        
        Disposition__c disp = LeadTriggerUtilities.getMostRecentDisposition(leadID, input);
        
    
        Test.stopTest();
        
        System.assert(disp != null, 'Should receive a non-null object');
        System.assert(disp.DispositionType__c == null, 'Disposition type should be null');
        System.assert(disp.DispositionDetail__c == null, 'Disposition detail should be null');
        System.assert(disp.Comments__c == null, 'Comments should be null');
        System.assert(disp.DispositionDate__c == null, 'Disposition date should be null');
        
    }
    
    static testMethod void testGetMostRecentDispositionZeroLengthString() {
        
        Id leadID = '00QV0000003xyYU';
        
        String input = null;
        
        Test.startTest();
        
        Disposition__c disp = LeadTriggerUtilities.getMostRecentDisposition(leadID, input);
        
    
        Test.stopTest();
        
        System.assert(disp != null, 'Should receive a non-null object');
        System.assert(disp.DispositionType__c == null, 'Disposition type should be null');
        System.assert(disp.DispositionDetail__c == null, 'Disposition detail should be null');
        System.assert(disp.Comments__c == null, 'Comments should be null');
        System.assert(disp.DispositionDate__c == null, 'Disposition date should be null');
        
    }
    
    static testMethod void testGetMostRecentDispositionOneCINote() {
        
        String dispDelimiter = '~';
        String dataDelimiter = '|';
        String note1 = 'Customer prefers morning appointments';
        String date1 = '08/28/2012';
        Id leadID = '00QV0000003xyYU';
        
        String input = dispDelimiter + note1 + dataDelimiter + date1;
        
        Test.startTest();
        
        Disposition__c disp = LeadTriggerUtilities.getMostRecentDisposition(leadID, input);
        
    
        Test.stopTest();
        
        System.assert(disp != null, 'Should receive a non-null object');
        System.assertEquals(IntegrationConstants.DEFAULT_NSC_DISPOSITION, disp.DispositionType__c);
        System.assertEquals(IntegrationConstants.DISPOSITION_DETAIL_CI_NOTE, disp.DispositionDetail__c);
        System.assertEquals(note1, disp.Comments__c);
        System.assertEquals(Date.parse(date1), disp.DispositionDate__c.date());
        System.assertEquals(leadID, disp.LeadID__c);
        
    }
    
    static testMethod void testGetMostRecentDispositionMostRecentFirst() {
        
        String dispDelimiter = '~';
        String dataDelimiter = '|';
        String note1 = 'Customer prefers morning appointments';
        String date1 = '08/28/2012';
        String note2 = 'Customer has large dog';
        String date2 = '08/25/2012';
        Id leadID = '00QV0000003xyYU';
        
        String input = dispDelimiter + note1 + dataDelimiter + date1 + 
                        dispDelimiter + note2 + dataDelimiter + date2;
        
        Test.startTest();
        
        Disposition__c disp = LeadTriggerUtilities.getMostRecentDisposition(leadID, input);
        
    
        Test.stopTest();
        
        System.assert(disp != null, 'Should receive a non-null object');
        System.assertEquals(IntegrationConstants.DEFAULT_NSC_DISPOSITION, disp.DispositionType__c);
        System.assertEquals(IntegrationConstants.DISPOSITION_DETAIL_CI_NOTE, disp.DispositionDetail__c);
        System.assertEquals(note1, disp.Comments__c);
        System.assertEquals(Date.parse(date1), disp.DispositionDate__c.date());
        System.assertEquals(leadID, disp.LeadID__c);
        
    }
    
    static testMethod void testGetMostRecentDispositionMostRecentLast() {
        
        String dispDelimiter = '~';
        String dataDelimiter = '|';
        String note1 = 'Customer has large dog';
        String date1 = '08/25/2012';
        String note2 = 'Customer prefers morning appointments';
        String date2 = '08/28/2012';
        Id leadID = '00QV0000003xyYU';
        
        String input = dispDelimiter + note1 + dataDelimiter + date1 + 
                        dispDelimiter + note2 + dataDelimiter + date2;
        
        Test.startTest();
        
        Disposition__c disp = LeadTriggerUtilities.getMostRecentDisposition(leadID, input);
        
    
        Test.stopTest();
        
        System.assert(disp != null, 'Should receive a non-null object');
        System.assertEquals(IntegrationConstants.DEFAULT_NSC_DISPOSITION, disp.DispositionType__c);
        System.assertEquals(IntegrationConstants.DISPOSITION_DETAIL_CI_NOTE, disp.DispositionDetail__c);
        System.assertEquals(note2, disp.Comments__c);
        System.assertEquals(Date.parse(date2), disp.DispositionDate__c.date());
        System.assertEquals(leadID, disp.LeadID__c);
        
    }
    
    static testMethod void testGetMostRecentDispositionSameDates() {
        
        String dispDelimiter = '~';
        String dataDelimiter = '|';
        String note1 = 'Customer has large dog';
        String date1 = '08/28/2012';
        String note2 = 'Customer prefers morning appointments';
        String date2 = '08/28/2012';
        Id leadID = '00QV0000003xyYU';
        
        String input = dispDelimiter + note1 + dataDelimiter + date1 + 
                        dispDelimiter + note2 + dataDelimiter + date2;
        
        Test.startTest();
        
        Disposition__c disp = LeadTriggerUtilities.getMostRecentDisposition(leadID, input);
        
    
        Test.stopTest();
        
        System.assert(disp != null, 'Should receive a non-null object');
        System.assertEquals(IntegrationConstants.DEFAULT_NSC_DISPOSITION, disp.DispositionType__c);
        System.assertEquals(IntegrationConstants.DISPOSITION_DETAIL_CI_NOTE, disp.DispositionDetail__c);
        System.assertEquals(note1, disp.Comments__c);
        System.assertEquals(Date.parse(date1), disp.DispositionDate__c.date());
        System.assertEquals(leadID, disp.LeadID__c);
        
    }
    
    static testMethod void testGetMostRecentDispositionInvalidDataFromExtract1() {
        
        String input = '~rep, BAIDYWI, ADD d the account for a video appt.  I m changing it back to resale so that we can get this completed.        |0';       
        Id leadID = '00QV0000003xyYU';
        
        Test.startTest();
        
        List<Disposition__c> dispList = null;
        try {
            dispList = LeadTriggerUtilities.createDispositions(leadID, input);
                
        }   
        catch (Exception e){
            System.assert(true, 'Exception should not occur but this one did: ' + e.getMessage());      
        }
        
        Test.stopTest();
        
        System.assertEquals(1, dispList.size());
        
        System.assert(dispList[0].DispositionDate__c == null);
        
        
    }
    
    static testMethod void testCreateDispositionsInvalidDataFromExtract2() {
        
        String input = '~Customer Instruction L/M TO CONFIRM 10/19/07 APT         |10/';        
        Id leadID = '00QV0000003xyYU';
        
        Test.startTest();
        
        List<Disposition__c> dispList = null;
        try {
            dispList = LeadTriggerUtilities.createDispositions(leadID, input);
                
        }   
        catch (Exception e){
            System.assert(true, 'Exception should not occur but this one did: ' + e.getMessage());      
        }
        
        Test.stopTest();
        
        System.assertEquals(1, dispList.size());
        
        System.assert(dispList[0].DispositionDate__c == null);

    
    }
    
}