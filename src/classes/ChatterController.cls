/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : ChatterController is a controller class for Chatter.page.
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Erin McGee      07/08/2013    - Original Version
*
*                           
*/
public with sharing class ChatterController
{
    public boolean displayPopup { get; set;}
    public Terms_and_Condition__c tc
    {
        get
        {
            return [Select Terms_and_Conditions_Text__c, Deployed__c From Terms_and_Condition__c ORDER BY CreatedDate LIMIT 1];
        }
    }

    public boolean isAccepted {get; set;}
    public User currentUser = [Select Id, Terms_and_Conditions_Accepted__c From User Where Id = : UserInfo.getUserID()];

    public ChatterController()
    {

    }

    public PageReference redirect()
    {
        if (currentUser.Terms_and_Conditions_Accepted__c!=null)
        {
            return new PageReference('/_ui/core/chatter/ui/ChatterPage');
        }
        else
        {
            if(tc!=null)
            {
                if(tc.Deployed__c)
                {
                    displayPopup = true;
                }
                else
                {
                    return new PageReference('/_ui/core/chatter/ui/ChatterPage');
                }
            }
            else
            {
                return new PageReference('/_ui/core/chatter/ui/ChatterPage');
            }
        }

        return null;
    }

    public PageReference save()
    {
        if (isAccepted)
        {
            currentUser.Terms_and_Conditions_Accepted__c = Date.today();
            update currentUser;
            return new PageReference('/_ui/core/chatter/ui/ChatterPage');
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please accept the Terms & Conditions, then save.'));
        }
        return null;

    }
}