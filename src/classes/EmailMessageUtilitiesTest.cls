@isTest(SeeAllData = false)
public class EmailMessageUtilitiesTest {
    
    public static Event e;
    public static Account a;
    
    static testmethod void createTestData(){
        //TestHelperClass.createReferenceDataForTestClasses();
        
        List<Wireless_Carrier__c> lstwless = new List<Wireless_Carrier__c> ();
        lstwless.add(new Wireless_Carrier__c (name='AT&T' ,SMS_Email_Service__c='test@txt.att.net', SMS_Enabled__c=true));
        lstwless.add(new Wireless_Carrier__c (name='Sprint' ,SMS_Email_Service__c='test@messaging.sprintpcs.com', SMS_Enabled__c=true));
        insert lstwless;
        
        list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
        rgvs.add(new ResaleGlobalVariables__c(Name='AcceptableDistanceForTelenavStopReport', value__c='0.125'));
        rgvs.add(new ResaleGlobalVariables__c(Name='AllowResiResaleAndResiDirect', value__c='false'));
        rgvs.add(new ResaleGlobalVariables__c(Name='CreateContactOnLeadConversion', value__c='false'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DataConversion', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DataRecastDisableAccountTrigger', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DaysBeforeReassignment', value__c='7'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DaysToRetainResidentialLeads', value__c='30'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DaysBeforeRIFVisibility', value__c='0'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DisqualifyEventTaskCode', value__c='TRV,'));
        rgvs.add(new ResaleGlobalVariables__c(Name='EnableManagerTownForInactiveManagers', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(Name='EventEndTimeOffset', value__c='60'));
        rgvs.add(new ResaleGlobalVariables__c(Name='EventObserverTimePeriod', value__c='24'));
        rgvs.add(new ResaleGlobalVariables__c(Name='EventRecTypeNameAllowed', value__c='Company Generated Appointment,Self Generated Appointment'));
        rgvs.add(new ResaleGlobalVariables__c(Name='EventStartTimeOffset', value__c='-30'));
        rgvs.add(new ResaleGlobalVariables__c(Name='GeocodeTimePeriod', value__c='36'));
        rgvs.add(new ResaleGlobalVariables__c(Name='GlobalResaleAdmin', value__c='TestGlobal ADMIN'));
        rgvs.add(new ResaleGlobalVariables__c(Name='GlobalNSCAdmin', value__c='Testing'));
        rgvs.add(new ResaleGlobalVariables__c(Name='GoogleGeoCodeDecodeKey', value__c='rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvs.add(new ResaleGlobalVariables__c(Name='GoogleGeocodeSchedulerInterval', value__c='1'));
        rgvs.add(new ResaleGlobalVariables__c(Name='InstallAppointmentTimeChangeThreshold', value__c='2'));
        rgvs.add(new ResaleGlobalVariables__c(Name='IntegrationUser', value__c='TestIntegration User')); 
        rgvs.add(new ResaleGlobalVariables__c(Name='IsTerritoryRealignmentBatchRunning', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(Name='RequestQueueMonitorInterval', value__c='2'));
        rgvs.add(new ResaleGlobalVariables__c(Name='stopTimeForTelenavStopReport', value__c='2'));
        rgvs.add(new ResaleGlobalVariables__c(Name='RIFAccountOwnerAlias', value__c='ADT RIF'));
        rgvs.add(new ResaleGlobalVariables__c(Name='EmailValidationRegex', value__c='.*@.*\\..*'));
        rgvs.add(new ResaleGlobalVariables__c(Name='SendAppointmentEmails', value__c='true'));
        rgvs.add(new ResaleGlobalVariables__c(Name='NoInstallUpdateDays', value__c='45'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DaysforRecentRIFIcon', value__c='90'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DaysToRetainResidentialBuilderLeads', value__c='730'));     
        rgvs.add(new ResaleGlobalVariables__c(Name='DevconPhoneSaleRole', value__c='Global_Admin'));
        rgvs.add(new ResaleGlobalVariables__c(Name='NewRehashHOARecordOwner', value__c='unitTestSalesRep1@testorg.com'));
        rgvs.add(new ResaleGlobalVariables__c(Name='UnlimitedHierarchyViewGroup', value__c='Extended Ownership Change Permissions'));
        rgvs.add(new ResaleGlobalVariables__c(Name='CPQ_Prefix', value__c='CF00Ne0000000hiqw'));
        rgvs.add(new ResaleGlobalVariables__c(Name='BypassQuoteSecurityProfiles', value__c='System Administrator'));   
        rgvs.add(new ResaleGlobalVariables__c(Name='ADTApplicationMonitor', value__c='1'));
        rgvs.add(new ResaleGlobalVariables__c(Name='ValidNewMoverType', value__c='RL,RM,RN,PN,PA,AP,PI,RC,CI,CA,SN,HA,HC'));
        rgvs.add(new ResaleGlobalVariables__c(Name='MMBAddressLength', value__c='30'));
        rgvs.add(new ResaleGlobalVariables__c(Name='MMBAccountNameLength',Value__c='30'));
        rgvs.add(new ResaleGlobalVariables__c(Name='MMBNameLength',Value__c='30'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DaysToPopulateDNISOnAccount',Value__c='1'));
        insert rgvs;
                
        List<user> users=new List<user>();
        
        Profile p = [select id from profile where name='System Administrator'];
        integer i= 23547876;
        User integrationUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='TestIntegration', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testinge'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(integrationUser);
        
        i= 7987345;
        User NewRehashUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='NewRehash', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1e@testorg.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(NewRehashUser);
        i= 2343578;
        User ResaleAdmin=new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='ADMIN',firstname='TestGlobal', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testinge'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(ResaleAdmin);
        
        INSERT users;
        
        a=testHelperClass.createAccountData();
        
        
        e=testHelperClass.createEvent(a,testHelperClass.createUser(34423422));
        e.TimeZoneId__c ='America/Los_Angeles';
        contact c=new contact();
        c.firstname='test';
        c.lastname='test';
        c.accountid=a.id;
        insert c;
        e.whoid=c.id;
        update e;
    
    }
    
    static testmethod void testEmailMessageUtilities(){ 
        createTestData();
        
        User u = [select id from user where profile.name = 'ADT NSC Sales Representative' limit 1];
        User rep = [Select id from user where profile.name = 'ADT NA Sales Representative' and IsActive = true limit 1 ];
        System.runas(rep){
            rep.SMS_Enabled__c = true;
            //rep.MobilePhone = '4085073115';
            update rep;
        }
        User Current = [select Id,SMS_Enabled__c, MobilePhone, DelegatedEmail__c from User where Id=:UserInfo.getUserId()];
        Current.SMS_Enabled__c = true;
        update Current;
        
        Test.startTest();
        System.runAs(Current){
        EmailMessageUtilities em = new EmailMessageUtilities();
        EmailMessageUtilities.SendAppointmentNotification(e.id,u.id);
            
        event eve = testHelperClass.createEvent(a,rep);        
        EmailMessageUtilities.NotifyUserBySMS(eve.id,true); 
        EmailMessageUtilities.NotifyUserBySMS(eve.id,false); 
        EmailMessageUtilities.isDelegateEmail(u);
        list<String> s = new list<String>(); 
        s.add('adb@adt.com');
            
        EmailMessageUtilities.SendEmailNotification(u.id,'Test','test test','https://test.com');
        EmailMessageUtilities.SendEmailNotification(u.id,'Test','test test',NULL);
        EmailMessageUtilities.SendEmailNotification(s,'test','test123');
        EmailMessageUtilities.SendEmailNotification(Current.id,'Test','test test','https://test.com',true);
        Test.stopTest();
        }
    }
        
}