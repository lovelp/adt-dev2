/************************************* MODIFICATION LOG ********************************************************************************************
* LeadDeleteProcessBatch
*
* DESCRIPTION : This batch manages the deletion of Lead records which have aged beyond their usefulness to the business 
*               More specifically, this process:
*               1) deletes Resi Salesgenie leads older than six months
*               2) deletes Resi New Mover (BUDCO) leads older than six months
*               3) deletes Resi Manual Import leads older than six months
*               4) deletes Resi Telemar Rehash leads older than six months
*               5) deletes SB Telemar Rehash leads older than six months
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*                               10/1/2012           - Original Version
* L.Alacon                      05/07/2012          - Added to soql the exclusion of Manual Import Builder leads, up to
*                                                     DaysToRetainResidentialBuilderLeads = 730 days. 
* Magdiel Herrera               9/22/2014           - Restriction to avoid deletion of business direct sales leads                                                  
*/

global with sharing class LeadDeleteProcessBatch implements Database.batchable<sObject>{
    
    //incoming query
    global final String query;
    
    //Constructor - set the local variable with the query string
    global LeadDeleteProcessBatch() {
         Integer days = Integer.valueof( ResaleGlobalVariables__c.getinstance('DaysToRetainResidentialLeads').value__c );
         Integer bldrdays = Integer.valueof( ResaleGlobalVariables__c.getinstance('DaysToRetainResidentialBuilderLeads').value__c );
         
        //this.query = 'Select Id from Lead Where CreatedDate < LAST_N_DAYS:'+ days +' and ( competitorcontractenddate__c = null or competitorcontractenddate__c < LAST_N_DAYS: '+ days +' ) and ((Business_Id__c like \'%1100%\' and (LeadSource=\'BUDCO\' OR LeadSource=\'Salesgenie.com\' OR LeadSource=\'Manual Import\')) OR ((Business_Id__c like \'%1100%\' OR Business_Id__c like \'%1200%\') AND LeadSource=\'Telemar Rehash\')  )';
        //this.query = 'Select Id from Lead Where(CreatedDate < LAST_N_DAYS:'+ days +' and( competitorcontractenddate__c = null or competitorcontractenddate__c < LAST_N_DAYS: '+ days +' ) and((Business_Id__c like \'%1100%\' and (LeadSource=\'BUDCO\' OR LeadSource=\'Salesgenie.com\' OR (LeadSource=\'Manual Import\' and Affiliation__c != \'Builder\') )) OR((Business_Id__c like \'%1100%\' OR Business_Id__c like \'%1200%\') AND LeadSource=\'Telemar Rehash\')  ) )OR(CreatedDate < LAST_N_DAYS:'+ bldrdays +' and  (competitorcontractenddate__c = null or competitorcontractenddate__c < LAST_N_DAYS: '+ bldrdays +' ) and LeadSource=\'Manual Import\'and Affiliation__c =\'Builder\' )';

        this.query = 'Select Id from Lead Where '+
                        '( (CreatedDate < LAST_N_DAYS:'+ days +' and '+
                            '( competitorcontractenddate__c = null or competitorcontractenddate__c < LAST_N_DAYS: '+ days +' ) and '+
                            '(  (Business_Id__c like \'%1100%\' and  '+
                                    '(LeadSource=\'BUDCO\' OR LeadSource=\'Salesgenie.com\' OR '+
                                        '(LeadSource=\'Manual Import\' and Affiliation__c != \'Builder\') '+
                                    ') '+
                                ') OR ( (Business_Id__c like \'%1100%\' OR Business_Id__c like \'%1200%\') AND LeadSource=\'Telemar Rehash\')  '+
                            ')  '+ 
                        ' )OR '+
                        ' (CreatedDate < LAST_N_DAYS:'+ bldrdays +' and  '+
                            '(competitorcontractenddate__c = null or competitorcontractenddate__c < LAST_N_DAYS: '+ bldrdays +' ) and  '+
                            ' LeadSource=\'Manual Import\'and Affiliation__c =\'Builder\' '+
                            ')'+
                        ') AND '+
                        '( NOT ( Business_Id__c like \'%1200%\' AND Channel__c = \'Business Direct Sales\') )';

        
    }
    // get Querylocator with the specitied query
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> leads) {
        List<Lead> leadstobedeleted = new List<Lead>();
        List<Id> leadIds = new List <Id> ();
        for(sObject s : leads)
        {
            Lead a = (Lead)s;
            leadIds.add(a.Id);
            leadstobedeleted.add(a);
        }

    System.debug('leads to be deleted '+leadIds);

      // passing the leads to deleteleads to delete the leads in StreetSheetItem__c, Disposition__c, Lead
     deleteLeads(leads,leadIds);

    }
    
    
     private static void deleteLeads(List<Lead> leads, List<Id> leadIds) {
        List<StreetSheetItem__c> ssitems = new List<StreetSheetItem__c>([
            select Id from StreetSheetItem__c
            where LeadID__c IN :leadIds
        ]);
        
        delete ssitems;
        
        List<disposition__c> disps = new List<disposition__c>([
            select Id from disposition__c
            where LeadID__c IN :leadIds
        ]);
        
        delete disps;
        
        delete leads;
    }
    
    global void finish(Database.BatchableContext bc) {

    }

}