/************************************* MODIFICATION LOG ********************************************************************************************
* DataRecastEventProcessor
*
* DESCRIPTION : Update events from phase 1
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* SahilGrover				  4/2/2012				- Original Version
*
*													
*/

global class DataRecastEventProcessor implements Database.Batchable<sObject>, Database.Stateful {

	public String query = 'Select Id,RecordTypeId from Event where RecordTypeId = null and isPrivate = false';

	global Database.QueryLocator start( Database.Batchablecontext bc ) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext bc, List<sObject> scope) {
		DataRecastHelper.processEvents((List<Event>)scope);
		update scope;
	}
	
	global void finish(Database.BatchableContext bc) {
		
	}

}