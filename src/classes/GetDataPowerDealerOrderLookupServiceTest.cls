/**
 Description- This test class used for GetDataPowerDealerOrderLookupService.
 Developer- TCS Developer
 Date- 3/15/2018
 */
@isTest
public class GetDataPowerDealerOrderLookupServiceTest {
    
    public static Account a;
    public static Account acc1;
    private static void testSetUp(){
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        rgvList.add(new ResaleGlobalVariables__c(name='BypassQuoteSecurityProfiles', value__c='System Administrator'));
        
        insert rgvList;
        IntegrationSettings__c integrationSetting= new IntegrationSettings__c(DPDealerLookupEndpointUsername__c='salesforcelb',
                                                                              DPDealerLookupEndpointPassword__c='tD$Hnj8s',
                                                                              DPDealerLookupEndpointTimeout__c=60000,
                                                                              DPDealerLookupEndpoint__c='https://dev.api.adt.com/adtutil/v1/orders/dealers/addresses'
                                                                             );
        insert integrationSetting;
        a = new Account();
        a= TestHelperClass.createAccountData();
        
    }
    static testMethod void testMethod1() {
        testSetUp();
        
        test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetDataPowerDealerCalloutMock');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        GetDataPowerDealerOrderLookupService dpDealerService = new GetDataPowerDealerOrderLookupService();
        GetDataPowerDealerOrderLookupService.GetDPDealerOrderLookupRes response = dpDealerService.getDealerOrderLookup(a.id);
        test.stopTest();
    }
    static testMethod void testMethod2() {
        testSetUp();
        
        test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetDataPowerDealerCalloutErrorMock');
        mock.setStatusCode(400);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        GetDataPowerDealerOrderLookupService dpDealerService = new GetDataPowerDealerOrderLookupService();
        GetDataPowerDealerOrderLookupService.GetDPDealerOrderLookupRes response = dpDealerService.getDealerOrderLookup(a.id);
        test.stopTest();
    }
    static testMethod void testMethod3() {
        testSetUp();
        
        test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetDataPowerDealerCalloutMockWithNoDealerInfo');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        GetDataPowerDealerOrderLookupService dpDealerService = new GetDataPowerDealerOrderLookupService();
        GetDataPowerDealerOrderLookupService.GetDPDealerOrderLookupRes response = dpDealerService.getDealerOrderLookup(a.id);
        test.stopTest();
    }
    
    static testMethod void testMethod5() {
        testSetUp();
        
        test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetDataPowerDealerCalloutMockWithNoDealerInfo');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        GetDataPowerDealerOrderLookupService dpDealerService = new GetDataPowerDealerOrderLookupService();
        GetDataPowerDealerOrderLookupService.GetDPDealerOrderLookupRes response = dpDealerService.getDealerOrderLookup(a.AddressID__c);
        GetDataPowerDealerOrderLookupService.compareWithExistingDealer(a.Id);
        test.stopTest();
    }
    
    static testMethod void testMethod4() {
        testSetUp();
        
        a.Data_Source__c = 'TEST';
        a.DP_Dealer_Name__c = 'test dealer';
        a.DP_Dealer_Number__c = '2134244';
        a.DealerQuoteStatus__c = 'test status';
        update a;
        
        test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetDataPowerDealerCalloutMock');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        GetDataPowerDealerOrderLookupService dpDealerService = new GetDataPowerDealerOrderLookupService();
        GetDataPowerDealerOrderLookupService.GetDPDealerOrderLookupRes response = dpDealerService.getDealerOrderLookup(a.id);
        test.stopTest();
    }
}