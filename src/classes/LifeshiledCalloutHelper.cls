/* LifeshiledCalloutHelper
*
* DESCRIPTION : Helper Class for LifeshiledCallout Class
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                        DATE              TICKET            REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Viraj Shah                   	10/09/2019       	HRM-11260         Original
* 
*/
public class LifeshiledCalloutHelper {

    public static final String SERVICENS = 'http://mastermindservice.adt.com/lookup';
	public static final String HSERVICENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
    public static final String LS_SEARCH_PREFIX = 'look';
	public static final String LS_SEARCH_HPREFIX = 'wsse';
    public static final String LS_SEARCH_SOAPNS = 'soapenv';
    public static final String LS_SEARCH_SOAPENV = 'http://schemas.xmlsoap.org/soap/envelope/';
    private static final String LS_ENDPOINT = 'http://mastermindservice.adt.com/lookup';
    private static final Integer LS_TIMEOUT = 200;
	
    public static string getLSEndpoint(){
		//return IntegrationSettings__c.getinstance().MMBSearchEndpoint__c;
		return LS_ENDPOINT;
	}
    
    public static Integer getLSTimeout(){
		//return IntegrationSettings__c.getinstance().MMBSearchCalloutTimeout__c.intValue();
		return LS_TIMEOUT;
	}
    	public static Dom.Document createEnvelopeHeader(){
		DOM.Document doc = new DOM.Document();
		DOM.Xmlnode envelope = doc.createRootElement('Envelope', LS_SEARCH_SOAPENV, LS_SEARCH_SOAPNS);
		envelope.setNamespace(LS_SEARCH_PREFIX, SERVICENS);
		envelope.setNamespace(LS_SEARCH_HPREFIX, HSERVICENS);
		return doc;
	}
}