global class CampaignBatchScheduler implements Schedulable{
    private final static ADTApplicationMonitor.CUSTOM_APP appContext = ADTApplicationMonitor.CUSTOM_APP.HERMES;
    
    global void execute(SchedulableContext ctx) {
        CampaignDefaults__c cd = CampaignDefaults__c.getInstance();
        integer tMins = Integer.valueOf(cd.NextCampaignBatchRun__c);         
        
        DateTime timenow = system.now().addMinutes(tMins);
        String seconds = String.valueOf(timenow.second()); //Execute at Zero Seconds
        String minutes = String.valueOf(timenow.minute()); //Execute at every 15th minute of hour       
        String hours = String.valueOf(timenow.hour()); // Execute Every Hour
        String sch = seconds + ' ' + minutes + ' * * * ?';
        
        system.debug('CRON:'+sch);
            
        String jobName = 'Campaign Batch Processor - ' + system.now();
        try{
            //database.executebatch(new CampaignBatchProcessor(),2000);
            scheduleCampaigns();
        }catch(exception ex){
            system.debug('Exception: '+ex);
            ADTApplicationMonitor.log(ex, 'CampaignBatchScheduler', 'execute', appContext);
        }
        system.schedule(jobName, sch, new CampaignBatchScheduler());
        
        if(ctx != null){
            system.abortJob(ctx.getTriggerId());
        }
    }
    
    global void scheduleCampaigns(){
        // Initialization
        List<Campaign> campaignsToSchedule = new List<Campaign>([select id, name, Type,Dialer_List_Output_Control__c, Status, Simulation__c, ParentId, SOQL__c, Primary_soql__c, DNIS__c, Email_Notification_User__c, Override_Active_Campaigns__c, HoldForReview__c, BatchSubmissionDate__c, BatchProcessedDate__c, ImmediateRun__c, RunScheduleTime__c, RunScheduleDay__c, OwnerId,CampaignMemberExpirationDays__c from campaign where IsActive = true AND (Status = 'Ready' OR Status = 'Queued') limit 1000]);
        List<Campaign> campaignJobsToInsert = new List<Campaign>();
        List<Campaign> errorCampaignsToUpdate = new List<Campaign>();
        List<Id> errorNotificationUserIdList = new List<Id>();
        List<Id> campaignIdList = new List<Id>();
        List<Id> campaignSimulationIdList = new List<Id>();
        List<Id> existingCampaignJobIds = new List<Id>();
        List<Id> campaignMasterIdLst = new List<Id>();
        List<Campaign> campaignsToUpdate = new List<Campaign>();
        List<Campaign> campaignMastersToUpdate = new List<Campaign>();
        
        Set<String> campaignJobNames = new Set<String>();
        Map<Id,Id> campaignIdParentIdMap = new Map<Id,Id>();
        
        // CampaignJob RecordType
        Recordtype campJobRT = new RecordType();
        campJobRT = [Select Id from RecordType where DeveloperName = 'CampaignJob' limit 1];
        
        // Custom setting defaults
        CampaignDefaults__c cd = CampaignDefaults__c.getInstance();
        integer tMins = Integer.valueOf(cd.NextCampaignBatchRun__c);
        integer bufferTime = Integer.valueOf(cd.BufferValueForCampaign__c);
        
        // Todays day of the week i.e monday or tuesday
        Date todaysDate = system.today();
        Datetime dt = DateTime.newInstance(todaysDate, Time.newInstance(0, 0, 0, 0));
        String dayOfWeek = dt.format('EEEE');
        for (Campaign camp : campaignsToSchedule) {
            if(camp.Type == 'Master')
                campaignMasterIdLst.add(camp.Id);
        }
        for (Campaign cJob : [Select Id,Name From Campaign Where CreatedDate = TODAY AND ParentId IN: campaignMasterIdLst AND Type = 'Job' Order By CreatedDate Desc]){
            campaignJobNames.add(cJob.Name);
        }
        for (Campaign cam : campaignsToSchedule) {
            if(cam.Primary_soql__c != Null && cam.SOQL__c != Null){
                if(cam.ParentId != null)
                    campaignIdParentIdMap.put(cam.Id,cam.ParentId);
                    
                if(cam.Simulation__c){
                    if(cam.ImmediateRun__c){
                        campaignSimulationIdList.add(cam.Id);
                    }else if(cam.RunScheduleDay__c != null && cam.RunScheduleDay__c.contains(dayOfWeek) && cam.RunScheduleTime__c != null){                     
                        for(String sTime : cam.RunScheduleTime__c.split(';')){
                            Integer min = Integer.valueOf(sTime.substring(sTime.indexOf(':')+1,sTime.indexOf(':')+3));
                            Integer hr = Integer.valueOf(sTime.substringBefore(':').replaceAll( '\\s+', ''));
                            if(sTime.containsIgnoreCase('PM') && hr != 12)
                                hr += 12;
                            if(sTime.containsIgnoreCase('AM') && hr == 12)
                                hr = 0;
                                
                            Datetime runDT = Datetime.newInstance(todaysDate, Time.newInstance(hr, min, 0, 0));
                            
                            // Compare the time
                            Long milliseconds = DateTime.now().getTime() - runDT.getTime();
                            Long seconds = milliseconds / 1000;
                            Long minutes = seconds / 60;
                            system.debug('DEV: Simulation minutes is '+ minutes);
                            if(minutes >= 0 && minutes <= tMins + bufferTime){
                                campaignSimulationIdList.add(cam.Id);
                            }
                        }
                    }
                }else{
                    if(cam.Type == 'Master' || cam.Type == 'One-Time'){
                        if(cam.ImmediateRun__c){
                            Campaign campaignJob = new Campaign();
                            campaignJob = cam.clone(false);
                            DateTime dtGmt = system.now();
                            campaignJob.name += ' - ' + dtGmt.format('yyyy-MM-dd h:mm a','America/New_York');
                            campaignJob.ParentId = cam.Id;
                            campaignJob.RecordTypeId = campJobRT.Id;
                            campaignJob.Type = 'Job';
                            campaignJob.status = 'Queued';
                            campaignJob.IsActive = true;
                            campaignJob.ImmediateRun__c = true;
                            campaignJob.RunScheduleDay__c = null;
                            campaignJob.RunScheduleTime__c = null;
                            campaignJob.ownerId = cam.ownerid;
                            campaignJob.Email_Notification_User__c = cam.Email_Notification_User__c;
                            campaignJob.Dialer_List_Output_Control__c=cam.Dialer_List_Output_Control__c;
                            campaignJob.CampaignMemberExpirationDays__c=cam.CampaignMemberExpirationDays__c;
                            campaignJobsToInsert.add(campaignJob);
                            campaignIdParentIdMap.put(campaignJob.Id,cam.Id);
                            
                            cam.BatchSubmissionDate__c = system.now();
                            campaignMastersToUpdate.add(cam);
                        }
                        else if(cam.RunScheduleDay__c != null && cam.RunScheduleDay__c.contains(dayOfWeek) && cam.RunScheduleTime__c != null){
                            for(String sTime : cam.RunScheduleTime__c.split(';')){
                                Integer min = Integer.valueOf(sTime.substring(sTime.indexOf(':')+1,sTime.indexOf(':')+3));
                                Integer hr = Integer.valueOf(sTime.substringBefore(':').replaceAll( '\\s+', ''));
                                if(sTime.containsIgnoreCase('PM') && hr != 12)
                                    hr += 12;
                                if(sTime.containsIgnoreCase('AM') && hr == 12)
                                    hr = 0;
                                    
                                Datetime runDT = Datetime.newInstance(todaysDate, Time.newInstance(hr, min, 0, 0));
                                system.debug('DEV: runScheduleDT: '+runDT);
                                
                                // Compare the time
                                Long milliseconds = DateTime.now().getTime() - runDT.getTime();
                                Long seconds = milliseconds / 1000;
                                Long minutes = seconds / 60;
                                system.debug('DEV: Comparison in minutes is '+ minutes);
                                if(minutes >= 0 && minutes <= tMins + bufferTime){
                                    String jobName = cam.Name + ' - ' + todaysDate + ' ' +sTime;
                                    if(jobName.contains('00:00:00'))
                                        jobName = jobName.replaceAll('00:00:00', '');
                                    if(campaignJobNames == null || (campaignJobNames != null && !campaignJobNames.contains(jobName))){
                                        Campaign campaignJob = new Campaign();
                                        campaignJob = cam.clone(false);
                                        campaignJob.name = jobName;
                                        campaignJob.ParentId = cam.Id;
                                        campaignJob.RecordTypeId = campJobRT.Id;
                                        campaignJob.Type = 'Job';
                                        campaignJob.status = 'Queued';
                                        campaignJob.IsActive = true;
                                        campaignJob.ImmediateRun__c = true;
                                        campaignJob.RunScheduleDay__c = null;
                                        campaignJob.RunScheduleTime__c = null;
                                        campaignJob.ownerId = cam.ownerid;
                                        campaignJob.Email_Notification_User__c = cam.Email_Notification_User__c;
                                        campaignJob.Dialer_List_Output_Control__c=cam.Dialer_List_Output_Control__c; 
                                        campaignJob.CampaignMemberExpirationDays__c=cam.CampaignMemberExpirationDays__c;
                                        campaignJobsToInsert.add(campaignJob);
                                        campaignIdParentIdMap.put(campaignJob.Id,cam.Id);
                                        
                                        cam.BatchSubmissionDate__c = system.now();  
                                        campaignMastersToUpdate.add(cam);
                                    }
                                }
                            }
                        }
                    }
                    if(cam.Type == 'Job' && cam.BatchProcessedDate__c == null){
                        existingCampaignJobIds.add(cam.Id);
                    }
                }
            }
            else{
                // Campaign aborted because there is no primary or secondary soql
                cam.Status = 'Aborted';
                errorCampaignsToUpdate.add(cam);
            }
        }
        
        // Insert the new campaign jobs
        if(campaignJobsToInsert.size() > 0){
            List<Id> savedCampaignJobIds = new List<Id>();
            Database.SaveResult[] srList = database.insert(campaignJobsToInsert,false);
            Boolean isSuccess = false;
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    savedCampaignJobIds.add(sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Campaign fields that affected this error: ' + err.getFields());
                    }
                }
            }

            //1. If there are any existing active campaign jobs which are not completed, schedule them first
            campaignIdList.addAll(existingCampaignJobIds);
            
            // 2. New campaign jobs are scheduled next
            campaignIdList.addAll(savedCampaignJobIds);
        }
        
        if(campaignIdList.size() > 0){
            for(Id cId :campaignIdList){
                try{
                    CampaignMemberBatchProcessor cmbp = new CampaignMemberBatchProcessor(cId);  
                                        
                    database.executeBatch(cmbp,integer.valueof(cd.Campaign_Member_Batch_Size__c));
                    Campaign camp = new Campaign();
                    camp.id = cid;
                    camp.BatchSubmissionDate__c = system.now();                    
                    campaignsToUpdate.add(camp);
                }catch(exception ex){
                    //batch not scheduled
                    // Campaign Job Aborted because of error in batch processor
                    Campaign camp = new Campaign();
                    camp.id = cid;
                    camp.status = 'Aborted';
                    campaignsToUpdate.add(camp);
                    
                    // Campaign Master Aborted because of error in batch processor
                    if(campaignIdParentIdMap.get(cid) != null){
                        Campaign campMaster = new Campaign();
                        campMaster.id = campaignIdParentIdMap.get(cid);
                        campMaster.status = 'Aborted';
                        campaignsToUpdate.add(campMaster);
                    }
                    system.debug('Exception: '+ex);
                }
            }
        }
        campaignsToUpdate.addAll(campaignMastersToUpdate);
        campaignsToUpdate.addAll(errorCampaignsToUpdate);
        
        // Send out email notification for aborted campaigns
        List<Messaging.singleEmailmessage> mailList = new List<Messaging.singleEmailmessage>();
        for(Campaign camp : errorCampaignsToUpdate){
            Messaging.singleEmailmessage Email = new Messaging.singleEmailmessage();
            if(camp.Simulation__c)
                email.setsubject('Campaign - '+ camp.name + ': Simulation aborted');
            else
                email.setsubject('Campaign - '+ camp.name + ': Run aborted');
            email.setPlainTextbody('Campaign does not have the Primary Soql and Secondary Soql configured.');
            if(camp.Email_Notification_User__c != null)
                email.setTargetObjectId(camp.Email_Notification_User__c);
            else
                email.setTargetObjectId(camp.OwnerId);
            email.saveAsActivity = false;
            mailList.add(Email);
        }
        system.debug('Error emails: '+ mailList.size());
        if(!mailList.isEmpty()) {
            try{
                Messaging.sendEmail(mailList);
            }
            catch (Exception ex) {
                System.debug('Unable to send aborted email: '+ ex.getStackTraceString());
            }
        }

        // 3. Simulation campaigns are scheduled last
        if(campaignSimulationIdList.size() > 0){
            for(Id simCId :campaignSimulationIdList){
                try{
                    CampaignMemberBatchProcessor cmbp = new CampaignMemberBatchProcessor(simCId);   
                    database.executeBatch(cmbp,integer.valueof(cd.Campaign_Member_Batch_Size__c));
                }catch(exception ex){
                    // Campaign Simulation Aborted because of error in batch processor
                    Campaign camp = new Campaign();
                    camp.id = simCId;
                    camp.status = 'Aborted';
                    campaignsToUpdate.add(camp);
                }
            }
        }
        
        if(campaignsToUpdate.size() > 0)
            database.update(campaignsToUpdate,false);
    }
}