/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : LeadTriggerUtilities.cls  has methods that perform common, often re-used functions in Lead Trigger
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                TICKET        REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            02/23/2012                        - Origininal Version
*
* Erin McGee                    06/27/2013                        - Added Repeat Disposition Functionality      
* Mike Tuckman                  03/22/2014                        - (Hermes) Added new fields Street2, StreetName, StreetNumber 
* Srinivas Yarramsetti          09/27/2018          HRM - 7486    - Remove extra spaces from address fields
* Nitin Gottiparthi             07/08/2019          HRM - 10370   - Added a method called sendLeadAssignmentEmail
*/
public without sharing class LeadTriggerUtilities{  
    private static final String DISPOSITION_DELIMITER = '~';
    private static final String DATA_DELIMITER = '|';
    private static final String ALTERNATE_DATA_DELIMITER = '&#124;';
    
    public static void processPostalCode(Lead NewLead){   
        Boolean validPostalCode = false;
        Boolean containsBlankSpace = NewLead.SitePostalCode__c.contains(' ');
        Boolean containsDash = NewLead.SitePostalCode__c.contains('-'); 
                
        if(!containsBlankSpace && !containsDash && NewLead.SitePostalCode__c.length() == 5){
            validPostalCode = true;
            NewLead.SiteCountryCode__c = 'US';              
        }else if(!containsBlankSpace && !containsDash && NewLead.SitePostalCode__c.length() == 6 && NewLead.LeadSource  == 'Salesgenie.com'){
            validPostalCode = true;            
            NewLead.SitePostalCodeAddOn__c = NewLead.SitePostalCode__c.substring(3,6); 
            NewLead.SitePostalCode__c = NewLead.SitePostalCode__c.substring(0,3); 
            NewLead.SiteCountryCode__c = 'CA';              
        }else if(!containsBlankSpace && containsDash && NewLead.SitePostalCode__c.length() == 10){
            list<String> PostalCodeSplitList = NewLead.SitePostalCode__c.split('-',0);
            if(PostalCodeSplitList != null && PostalCodeSplitList.size() == 2 && PostalCodeSplitList[0].length() == 5 && PostalCodeSplitList[1].length() == 4){                   
                validPostalCode = true;                 
                NewLead.SitePostalCode__c = PostalCodeSplitList[0].toUpperCase(); 
                NewLead.SitePostalCodeAddOn__c = PostalCodeSplitList[1].toUpperCase();                            
                NewLead.SiteCountryCode__c = 'US';              
            }else{
                validPostalCode = false;
            }
        }  
        else if(!containsBlankSpace && !containsDash && NewLead.SitePostalCode__c.length() == 9 && NewLead.LeadSource  == IntegrationConstants.DATA_SOURCE_TELEMAR_REHASH){      
            validPostalCode = true;
            String inputValue = NewLead.SitePostalCode__c;                 
            NewLead.SitePostalCode__c = inputValue.substring(0, 5); 
            NewLead.SitePostalCodeAddOn__c = inputValue.substring(5, 9);                            
            NewLead.SiteCountryCode__c = 'US';   
        }else if(!containsBlankSpace && !containsDash && NewLead.SitePostalCode__c.length() == 6 && NewLead.LeadSource  == IntegrationConstants.DATA_SOURCE_TELEMAR_REHASH){
            validPostalCode = true;
            String inputValue = NewLead.SitePostalCode__c;                 
            NewLead.SitePostalCode__c = inputValue.substring(0, 3); 
            NewLead.SitePostalCodeAddOn__c = inputValue.substring(3, 6);                            
            NewLead.SiteCountryCode__c = 'CA';
        }else if(containsBlankSpace && !containsDash){
            list<String> PostalCodeSplitList = NewLead.SitePostalCode__c.split(' ',0);
            if(PostalCodeSplitList != null && PostalCodeSplitList.size() == 2 && PostalCodeSplitList[0].length() == 3 && PostalCodeSplitList[1].length() == 3){
                validPostalCode = true;                 
                NewLead.SitePostalCode__c = PostalCodeSplitList[0].toUpperCase(); 
                NewLead.SitePostalCodeAddOn__c = PostalCodeSplitList[1].toUpperCase();                            
                NewLead.SiteCountryCode__c = 'CA';       
            }else{
                validPostalCode = false;
            }
        }
         
        if(!validPostalCode){
            NewLead.SitePostalCode__c.addError('Invalid Site Postal Code format.'+ '<BR>' + 'For US address, please enter a Postal Code in the format "12345" ' + '<BR>' + ' and Postal Code Add On (OPTIONAL) in the format "6789"' + '<BR>' + 'OR Postal Code in the format "12345-6789".' + '<BR>' + 'For Canadian address, please enter a Postal Code' + '<BR>' +'in the format "123 456".');
        }   
    }
    //HMR-7486
    public static Address__c setSiteAddress(Lead ld){
        String Error = 'FALSE';
        Address__c newAddress = new Address__c();
        newAddress.Street__c = Utilities.removeExtraSpaces(ld.SiteStreet__c);
        if(String.isNotBlank(ld.SiteCounty__c)){
            newAddress.Street2__c = Utilities.removeExtraSpaces(ld.SiteStreet2__c);
        }
        if(String.isNotBlank(ld.SiteStreetName__c)){
            newAddress.StreetName__c = Utilities.removeExtraSpaces(ld.SiteStreetName__c);
        }
        if(String.isNotBlank(ld.SiteStreetNumber__c)){
            newAddress.StreetNumber__c = Utilities.removeExtraSpaces(ld.SiteStreetNumber__c);
        }
        if(String.isNotBlank(ld.SiteCity__c)){
            newAddress.City__c = Utilities.removeExtraSpaces(ld.SiteCity__c);
        }
        if(String.isNotBlank(ld.SiteStateProvince__c)){
            newAddress.State__c = Utilities.removeExtraSpaces(ld.SiteStateProvince__c);
        }
        if(String.isNotBlank(ld.SiteCounty__c)){
            newAddress.County__c = Utilities.removeExtraSpaces(ld.SiteCounty__c);
        }
        if(String.isNotBlank(ld.SiteCountryCode__c)){
            newAddress.CountryCode__c = Utilities.removeExtraSpaces(ld.SiteCountryCode__c);
        }
        if(ld.IncomingLatitude__c != null){
            newAddress.Latitude__c = ld.IncomingLatitude__c;
        }   
        if(ld.IncomingLongitude__c != null){
            newAddress.Longitude__c = ld.IncomingLongitude__c;
        }
        if(String.isNotBlank(ld.SitePostalCode__c)){
            newAddress.PostalCode__c = ld.SitePostalCode__c;
        }
        newAddress.PostalCodeAddOn__c = ld.SitePostalCodeAddOn__c;  
        newAddress.Validated__c = ld.SiteValidated__c;
        newAddress.ValidationMsg__c = ld.SiteValidationMsg__c;
        
        try{
            newAddress.OriginalAddress__c = AddressHelper.getConcatenatedAddress(newAddress.Street__c, newAddress.Street2__c, newAddress.City__c, newAddress.State__c, newAddress.PostalCode__c);
            newAddress.StandardizedAddress__c = AddressHelper.getStandardizedAddress(newAddress.Street__c, newAddress.Street2__c, newAddress.City__c, newAddress.State__c, newAddress.PostalCode__c);        
        }catch(Exception Ex){
            ld.addError(ErrorMessages__c.getInstance('Missing_Address').Message__c);
            Error = 'TRUE';
        }
        if(Error == 'FALSE'){
            return newAddress;
        }else{
            newAddress.Street__c = 'FAILED';
            return newAddress;
        }
    }
       
    public static void setSiteAddressIds(Map<string, Address__c> newAddresses, list<Lead> newLeads){
        String origAddress = '';
        Boolean pass = true;
        System.Debug('newLeads==='+newLeads);
        for(Lead ld : newLeads){
            if(ld.SiteStreet__c != null && ld.SiteStreet__c != ''){
                pass = true;
                origAddress = 'BLANK';                  
                try{
                    origAddress = AddressHelper.getConcatenatedAddress(ld.SiteStreet__c, ld.SiteStreet2__c, ld.SiteCity__c, ld.SiteStateProvince__c, ld.SitePostalCode__c);
                }catch(Exception Ex){
                    pass = false;
                }
                System.Debug('origAddress======='+origAddress);
                System.Debug('newAddresses======='+newAddresses);
                if(pass){
                    if(newAddresses.get(origAddress) != null){
                        ld.AddressID__c = newAddresses.get(origAddress).id;                                         
                    }else{
                        ld.addError(ErrorMessages__c.getInstance('Bad_Address').Message__c);
                    }
                }
            }
        }
    }
    
    public static void setPostalCodeLookup(list<Lead> NewLeads){
        map<String, list<Lead>> allLeads = new map<String, list<Lead>>();
        list<Lead> leadsList;
        list<String> allZips = new list<String>();
        map<String, Id> allZipIds = new map<String, Id>();
        
        for(Lead ld : NewLeads){
            if(ld.Business_Id__c != null){
                // HRM-10450 Business Id Change
                String businessId = Channels.getFormatedBusinessId(ld.Business_Id__c,Channels.BIZID_OUTPUT.NUM);
                if(String.isBlank(businessId)){
                    businessId = '1100';
                }
                leadsList = allLeads.get(businessId + ';' + ((ld.SitePostalCode__c != null && ld.SitePostalCode__c != '') ? ld.SitePostalCode__c.toUpperCase() : null));
                
                if(leadsList == null){
                    leadsList = new list<Lead>();
                }
                
                leadsList.add(ld);
                allLeads.put(businessId + ';' + ((ld.SitePostalCode__c != null && ld.SitePostalCode__c != '') ? ld.SitePostalCode__c.toUpperCase() : null), leadsList);            
                
                if(ld.SitePostalCode__c != null && ld.SitePostalCode__c != ''){
                    allZips.add(ld.SitePostalCode__c.toUpperCase());  
                }
                if(ld.PostalCodeID__c != null){
                    ld.PostalCodeID__c = null;
                }
            }           
        }
              
        for(Postal_Codes__c pc : [select Id, Name, BusinessID__c from Postal_Codes__c where Name IN :allZips]){
            allZipIds.put(pc.BusinessId__c + ';' + pc.Name.toUpperCase(), pc.Id);
        }
        
        System.debug('The allzipids is'+allZipIds);       
        for(String key : allLeads.keySet()){
            System.Debug('key====='+key);
            for(Lead ld : allLeads.get(key)){                       
                if(allZipIds.get(key) == null){
                    ld.addError(ErrorMessages__c.getInstance('No_Zipcode_Match').Message__c);
                }else { 
                    ld.PostalCodeID__c = allZipIds.get(key);
                }
            }
        }
    }
    
    public static void createDispositionOnLead(List<Lead> NewLeads){
        list<disposition__c>  DispoList = new list<disposition__c>();
        List<Lead> updateRepeat=new List<Lead>();
        for(Lead ld : NewLeads){
            if( ld.DispositionCode__c != null &&  ld.DispositionCode__c != ''){
                disposition__c newDispo = new disposition__c();
                newDispo.LeadID__c = ld.Id;
                newDispo.OwnerId = ld.OwnerId;
                newDispo.DispositionType__c = ld.DispositionCode__c;
                newDispo.DispositionedBy__c = UserInfo.getUserId();         
                newDispo.DispositionDate__c = ld.DispositionDate__c != null ? ld.DispositionDate__c : DateTime.now();
                newDispo.Comments__c = ld.DispositionComments__c;       
                newDispo.DispositionDetail__c = ld.DispositionDetail__c;
                Lead upLead=new Lead(Id=ld.id, Repeat_Disposition__c=false);
                if(ld.Repeat_Disposition__c){
                    newDispo.DispositionDate__c = DateTime.now();
                    upLead.DispositionDate__c= DateTime.now();
                }
                DispoList.add(newDispo);
            }           
        }
        
        try{
            if(DispoList.size()>0){
                insert DispoList;
            }    
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
        }       
    }
    
    public static void SalesgenieLeadValidation(map<String, Lead> Salesgenie_LeadsMap){
        for(Lead ld : [Select Id, Name, OwnerId, SGCOM3__SG_infoGroupId__c  FROM Lead WHERE SGCOM3__SG_infoGroupId__c IN :Salesgenie_LeadsMap.keySet()]){
            Lead tempLead = Salesgenie_LeadsMap.get(ld.SGCOM3__SG_infoGroupId__c);
            if(tempLead != null){
                 tempLead.addError(ErrorMessages__c.getInstance('Salesgenie_Dupe_Message').Message__c); 
            }                   
        }       
    }
    
    public static void setCompanyAndLastName(Lead ld){
        if((ld.LastName == null || ld.LastName == '') && ld.Company != null && ld.Company != '' && ld.Company != 'N/A'){
            ld.LastName = ld.Company;
        }else if(ld.LastName != null && ld.LastName != '' && (ld.Company == null || ld.Company == '' || ld.Company == 'N/A')){
            if(ld.FirstName != null && ld.FirstName != ''){
                ld.Company = ld.FirstName + ' ' + ld.LastName;
            }else{
                ld.Company = ld.LastName;   
            }           
        }else if(ld.LastName == null || ld.LastName == '' || ld.Company == null || ld.Company == '' || ld.Company == 'N/A'){
            ld.addError(ErrorMessages__c.getInstance('LastName_Company_Required').Message__c);
        }
    }
    
    public static void updateAllProspectListTouched(set<id> leadIds){
        List<StreetSheetItem__c> allPLIs = new List<StreetSheetItem__c>();
        allPLIs = [select Id, Worked__c, Touched__c from StreetSheetItem__c where leadId__c in : leadIds];
        for(StreetSheetItem__c ssi : allPLIs){
            if(ssi.Touched__c != 1)
             ssi.Touched__c = ssi.Worked__c;
        }
        try{
            if(allPLIs.size() > 0)
                update allPLIs;
        } catch(Exception e){ }
    }  
    
    public static List<disposition__c> createDispositions(Id leadID, String input){
        List<disposition__c> dispositionList = new List<disposition__c>();
        
        if (input != null && input.length() > 0) {
            String[] dispositionsStr = input.substring(1).split(DISPOSITION_DELIMITER);
            for (String dispStr: dispositionsStr) {
                String[] dispData = dispStr.split(DATA_DELIMITER);
            
                // Data elements are delimited by | so the String split method should work but
                // the | is being received by this code as &#124; instead
                if (dispData.size() == 2) {
                    disposition__c d = new disposition__c();
                    d.DispositionType__c = IntegrationConstants.DEFAULT_NSC_DISPOSITION;
                    d.DispositionDetail__c = IntegrationConstants.DISPOSITION_DETAIL_CI_NOTE;
                    d.Comments__c = dispData[0];
                    d.DispositionDate__c = null;
                    try {
                        d.DispositionDate__c = Datetime.newInstanceGmt(Date.parse(dispData[1]), Time.newInstance(10,0,0,0));
                    } catch (Exception e) {
                        // no action; disposition date will be null
                    }   
                
                    d.LeadID__c = leadID;
                    dispositionList.add(d);
                }
                else {
                    // So if split does not produce an array containing 2 strings, search character by character
                    // for either | or &#124;
                    Integer splitPoint = 0;
                    String character = null;
                    for (Integer i = 0; i < dispStr.length(); i++) {
                        character = dispStr.substring(i, i+1);
                        if (character == DATA_DELIMITER || character == ALTERNATE_DATA_DELIMITER) {
                            splitPoint = i;
                            break;
                        }
                    }
                    if (splitPoint > 0) {
                        disposition__c d = new disposition__c();
                        d.DispositionType__c = IntegrationConstants.DEFAULT_NSC_DISPOSITION;
                        d.DispositionDetail__c = IntegrationConstants.DISPOSITION_DETAIL_CI_NOTE;
                        d.Comments__c = dispStr.substring(0, splitPoint);
                        d.DispositionDate__c = null;
                        try {
                            d.DispositionDate__c = Datetime.newInstanceGmt(Date.parse(dispStr.substring(splitPoint + 1)), Time.newInstance(10,0,0,0));      
                        } catch (Exception e) {
                            // no action; disposition date will be null
                        }
                        d.LeadID__c = leadID;
                        dispositionList.add(d);
                    }   
                }
            }
        }   
        return dispositionList;
    }
    
    public static disposition__c getMostRecentDisposition(Id leadID, String input) {
        disposition__c returnVal = new disposition__c();
        Datetime mostRecentDate = null;
        List<disposition__c> dispositionList = createDispositions(leadID, input);
        for (disposition__c d: dispositionList) {
            if (mostRecentDate == null || d.DispositionDate__c > mostRecentDate) {
                mostRecentDate = d.DispositionDate__c;
                returnVal = d;
            }
        }  
        return returnVal;
    }

    //Nitin added HRM-10370
    // Send email to lead owner if the profile is allowed to receive
    public static void sendLeadAssignmentEmail(List<lead> newLeads){
        datetime qryStart = datetime.now();
         system.debug('TimeEnd'+qryStart);
        list<id> userIds = new list<id>(); // list to store all lead owner ids
        list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
        map<id , String> getUserNameMap = new map<id , String>();// get userName wrto user id
        string allowedProfileList = Label.ChangeOwnerNotification;// access profile info from Custom Label
        for(lead l: newLeads){
            userIds.add(l.ownerid);
        }
        
        if(userids.size() > 0 && String.isNotBlank(allowedProfileList)){
            for(User u : [select id , name ,Profile.Name from user where id in :userIds And IsActive = true]){
                if(allowedProfileList.contains(u.Profile.Name)){
                    getUserNameMap.put(u.id, u.Name);
                }
            }
        }
        
        // collect all mail instances here and send at once
        for(lead l:newLeads){
            if(getUserNameMap.containsKey(l.OwnerId)){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(l.OwnerId);
                mail.setSaveAsActivity(false);
                mail.setSubject('New Lead Assignment' );
                String body = 'Hi '+ getUserNameMap.get(l.OwnerId)+ ','+ '<br/>'+ '<br/>'+'*** NEW LEAD ASSIGNMENT NOTIFICATION ***.';
                body+= '<br/>' + '<br/>' + 'The following lead has been assigned to you.';
                body+= '<br/>' + 'Lead Name: '+ l.LastName + '<br/>';
                body+= 'Address: '+ l.StandardizedAddress__c;
                body+='<br/>' + 'Company: '+l.Company ;
                body+='<br/>' + 'BusinessId: '+l.Business_Id__c ;
                body+='<br/>' + 'Channel: '+l.Channel__c ;
                body+='<br/>' + 'LeadManagementId: '+l.LeadManagementId__c;
                body+='<br/>' + 'Status: '+l.Status;
                body+='<br/>' + 'LastModifiedDate: '+l.LastModifiedDate;
                body+= '<br/>'+'<a href ="https://'+Url.getSalesforceBaseUrl().getHost()+'/'+l.Id+'">' +'Click on the link to access the lead directly'+'</a>';

                mail.setHtmlBody(body);
                mails.add(mail); // add all mails in master list
            }
        }
        // send emails from master list
        if(mails.size() > 0){
            Messaging.sendEmail(mails);
            system.debug('Mail size: '+mails.size());
        }
        datetime qryEnd = datetime.now();
        system.debug('TimeEnd'+qryEnd);
        system.debug('TimeforLeademailexection'+(qryEnd.getTime() - qryStart.getTime()) /1000);
    }
    
     public static List<Lead> getAllPossibleNowLeads(List<Lead> newLeadsList){
        List<Lead> possibleNowGryphonLeadsList = new List<Lead>();
        try{
            List<PossibleNowGryphonDispositons__mdt > possibleMetaDataList = [SELECT MasterLabel,ChannelValue__c,Disposition__c,SObjectValue__c,DispositionDetail__c FROM PossibleNowGryphonDispositons__mdt WHERE SObjectValue__c = 'lead'];
            //loop through every lead list
            for(Lead l : newLeadsList){
                // Loop through custom meta data for lead/account
                for(PossibleNowGryphonDispositons__mdt  possibleNowGryphonItem : possibleMetaDataList ){
                    //Match if lead business id matches with that on custom meta data
                    if(String.isNotBlank(l.Business_Id__c) && String.isNotBlank(possibleNowGryphonItem.ChannelValue__c) && 
                       possibleNowGryphonItem.ChannelValue__c.containsIgnoreCase(Channels.getFormatedBusinessId(l.Business_Id__c,Channels.BIZID_OUTPUT.SHORTSTR)) && 
                       String.isNotBlank(l.DispositionCode__c) && String.isNotBlank(possibleNowGryphonItem.Disposition__c) && l.DispositionCode__c.equalsIgnoreCase(possibleNowGryphonItem.Disposition__c)){
                           //if custom meta data details are empty create the request queue for ebr processing
                            if(String.isBlank(possibleNowGryphonItem.DispositionDetail__c)){
                               possibleNowGryphonLeadsList.add(l);
                            } 
                            //if details on custom meta data are not empty then it should match with what we have on lead/account
                            else if(String.isNotBlank(possibleNowGryphonItem.DispositionDetail__c) && String.isNotBlank(l.DispositionDetail__c) && possibleNowGryphonItem.DispositionDetail__c.containsIgnoreCase(l.DispositionDetail__c)){
                               possibleNowGryphonLeadsList.add(l);
                            }
                    }
                }
            }
        }catch(exception ex){
            System.debug('Exception while getting the custom metda data'+ex);
        }
        return possibleNowGryphonLeadsList;  
    }
}