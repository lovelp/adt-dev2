/************************************* MODIFICATION LOG ********************************************************************************************
* OpportunityCreateSalesAgreement
*
* DESCRIPTION : CTI Agent connection class to Salesforce NSC Calldata
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE        Ticket       REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Abhinav Pandey      07/20/2019     HRM-10379    - Sales agreement update opportunity on load of opportunity page.  
*                          
*/
public class OpportunityCreateSalesAgreement {
    List<opportunity> oppList = new List<opportunity>();
    String oppId = '';
    public OpportunityCreateSalesAgreement(Apexpages.StandardController stdcon){
        this.oppId = stdCon.getRecord().Id;
    }
    
    public PageReference init(){
        try{
            oppList = [select id,name,SalesAgreementId__c from opportunity where id =: this.oppId limit 1 ];
            System.debug('Opportunity list is'+oppList);
            if(oppList.size() > 0 && string.isNotBlank(oppList[0].SalesAgreementId__c)){
                ADT_SalesAgreementComponentController.initActions(oppList[0].id,'Lookup'); 
            } 
            System.debug('Opportunity list is after'+oppList);
        }catch(exception ex){
            System.debug('Exception occured'+ex);
        }
        return null;
    }
    
}