/*
 * Description: This is a test class for the following classes
                1. ManagerTownRealignmentScheduler
 * Created By: Shiva Pochamalla
 *
 * ---------------------------------------------------------------------------Changes below--------------------------------------------------------------------------------
 * 
 *
 */


@isTest
private class ManagerTownRealignmentSchedulerTest{
    public static void createTestData(){
        TestHelperClass.createReferenceDataForTestClasses();
        User u1=testHelperClass.createUser(234343);
        User u2=testHelperClass.createUser(345343);
        //create records
        ManagerTownRealignmentQueue__c mtrq=new ManagerTownRealignmentQueue__c(NewOwnerId__c=u2.id,OldOwnerId__c =u1.id,Channel__c='test',BusinessId__c='test',TownId__c='test');
        insert mtrq;
    }
    
    private static testmethod void testManagerTownRealignmentScheduler(){
        createTestData();
        test.startTest();
            
            ManagerTownRealignmentScheduler mtrs=new ManagerTownRealignmentScheduler();
            String chron = '0 0 21 * * ?';        
            system.schedule('Test managertown relign Sched', chron, mtrs);
        test.stopTest();
    }
}