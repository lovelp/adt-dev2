/************************************* MODIFICATION LOG ********************************************************************************************
 * ADTOneClickAPI
 *
 * DESCRIPTION : Serves as a class to be used for invocation of OneClickRestService and handling the API response
 *
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE              TICKET               REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Pankaj Sharma                 09/20/2018        HRM - 4050           - Original Version
 * Kalyani Pinisetti             02/7/2019         HRM - 9275           - Added additional field to the call
 * Siddarth Asokan               03/29/2019        ECOM - 967           - Modified request queue creation of One click API
 */

public class ADTOneClickAPI{
    /**
     * Wrapper class to store request of OneClick
     *
     **/
    public class OneClickRequestWrapper{
        public String name                       {get;set;}
        public String managementId               {get;set;}
        public String division                   {get;set;}
        public String source                     {get;set;}
        public String phone                      {get;set;}
        //Kalyani Added - HRM - 9275 
        public String postalCode                 {get;set;}
        public String dnis                       {get;set;}
        public String marketGeneric              {get;set;}
        public String businessSegment            {get;set;}
        public String promotions                 {get;set;}
        public String alreadySentToday           {get;set;}
        public String originalSubmissionDateTime {get;set;}
    }
     
   /**
    * This method constructs the JSON & performs callout
    * @Param Account Id & Audit Log Id
    * @ReturnType void
    **/
    public static void createOneClickReqQueue(Id accountId, Id aLogId){
        if(String.isNotBlank(aLogId)){
            // If audit log is provided use the audit log to get all the needed details - Webleads flow
            AuditLog__c aLog = [SELECT id,PromotionCode__c,OneClickMessageDivision__c,Account__c,Account__r.Name,Account__r.phone,Account__r.TelemarAccountNumber__c,Account__r.SitePostalCode__c,
                                CallData__c,CallData__r.DNIS__c,CallData__r.DNIS__r.oneClickBypass__c,CallData__r.DNIS__r.Marketing_Generic__c,CallData__r.DNIS__r.Business_segment__c,
                                CallData__r.DNIS__r.Promotions__c,CallData__r.DNIS__r.Promotions__r.Name FROM AuditLog__c WHERE Id=:aLogId];
            system.debug('Audit Log: '+aLog);
            
            String formattedPhoneNumber = (aLog.Account__c != null && aLog.Account__r.phone != null)? Utilities.getFormattedPhoneForWebleads(aLog.Account__r.phone) : '';
            // If the phone number format is wrong do not call 1 click
            if(aLog.Account__c != null && aLog.Account__r.TelemarAccountNumber__c != null && String.isNotBlank(formattedPhoneNumber)){
                OneClickRequestWrapper oneClickWrapper = new OneClickRequestWrapper();
                oneClickWrapper.name = aLog.Account__r.Name;
                oneClickWrapper.managementId = String.isNotBlank(aLog.Account__r.TelemarAccountNumber__c)? aLog.Account__r.TelemarAccountNumber__c : '';
                oneClickWrapper.division = aLog.OneClickMessageDivision__c != null? aLog.OneClickMessageDivision__c : '';
                oneClickWrapper.source = aLog.OneClickMessageDivision__c != null? aLog.OneClickMessageDivision__c : '';
                oneClickWrapper.phone =  formattedPhoneNumber;
                oneClickWrapper.postalCode = aLog.Account__r.SitePostalCode__c != null? aLog.Account__r.SitePostalCode__c : '';
                oneClickWrapper.dnis = aLog.PromotionCode__c != null? aLog.PromotionCode__c : '';
                oneClickWrapper.marketGeneric = '';
                oneClickWrapper.businessSegment = '';
                oneClickWrapper.promotions = '';
                // HRM - 9275 - Start
                if(aLog.CallData__c != null && aLog.CallData__r.DNIS__c != null){
                    if(String.isNotBlank(aLog.CallData__r.DNIS__r.Marketing_Generic__c)){
                        oneClickWrapper.marketGeneric = aLog.CallData__r.DNIS__r.Marketing_Generic__c;
                    }if(String.isNotBlank(aLog.CallData__r.DNIS__r.Business_segment__c)){
                        oneClickWrapper.businessSegment = aLog.CallData__r.DNIS__r.Business_segment__c;
                    }if(aLog.CallData__r.DNIS__r.Promotions__c != null && String.isNotBlank(aLog.CallData__r.DNIS__r.Promotions__r.Name)){  
                        oneClickWrapper.promotions = aLog.CallData__r.DNIS__r.Promotions__r.Name;
                    }
                }
            
                list<RequestQueue__c> existingReqQueue = new list<RequestQueue__c>();
                existingReqQueue = [Select Id,CreatedDate From RequestQueue__c Where CreatedDate >= LAST_N_DAYS:1 And MessageID__c =: Utilities.getFormattedPhoneForWebleads(String.valueOf(aLog.Account__r.phone)) 
                            And ServiceTransactionType__c = 'sendToOneClick' And RequestStatus__c = 'Success' order by CreatedDate ASC LIMIT 1];               
                system.debug('ExistingReqQueue: '+existingReqQueue);            
                oneClickWrapper.alreadySentToday = existingReqQueue.size() > 0? 'Y' : 'N';
                oneClickWrapper.originalSubmissionDateTime = existingReqQueue.size() > 0? String.ValueOf(existingReqQueue[0].CreatedDate): '';
                // HRM - 9275 - End 
            
                // Create JSON to store in service message
                String jsonString = JSON.serialize(oneClickWrapper);
                system.debug('One Click JSON: '+jsonString);
                  
                // Create Request queue
                RequestQueue__c rqObj = new RequestQueue__c();
                rqObj.AccountID__c = aLog.Account__c;
                rqObj.AuditLog__c = aLog.Id;
                rqObj.RequestStatus__c = (aLog.CallData__c != null && aLog.CallData__r.DNIS__c != null && aLog.CallData__r.DNIS__r.oneClickBypass__c)? 'Bypass':'Ready';
                rqObj.ServiceTransactionType__c = 'sendToOneClick';
                rqObj.MessageID__c = formattedPhoneNumber;
                rqObj.ServiceMessage__c = jsonString;
                rqObj.RequestDate__c = system.now();
                insert rqObj;
            }
        }else if(String.isNotBlank(accountId)){
            // If audit log is not provided use the account to get all the needed details - ECOM flow
          RequestQueue__c reqQueueToInsert = new RequestQueue__c();
          list<Account> accountList = new list<Account>();
            accountList =  [Select Id,Name,phone,TelemarAccountNumber__c,Business_Id__c,Channel__c,SitePostalCode__c from Account Where Id =: accountId AND TelemarAccountNumber__c != null];
            if(accountList.size() > 0){
                Account acc = accountList[0];
                String formattedPhoneNumber = acc.phone != null? Utilities.getFormattedPhoneForWebleads(acc.phone) : '';
                if(String.isNotBlank(formattedPhoneNumber)){
                    OneClickRequestWrapper oneClickWrapper = new OneClickRequestWrapper();
                    oneClickWrapper.name = acc.Name;
                    oneClickWrapper.managementId = String.isNotBlank(acc.TelemarAccountNumber__c)? acc.TelemarAccountNumber__c : '';
                    oneClickWrapper.division = 'ECommerce';
                    oneClickWrapper.source = 'ECommerce';
                    oneClickWrapper.phone =  formattedPhoneNumber;
                    oneClickWrapper.postalCode = acc.SitePostalCode__c != null? acc.SitePostalCode__c : '';
                    oneClickWrapper.dnis = '';
                    oneClickWrapper.marketGeneric = '';
                    oneClickWrapper.businessSegment = '';
                    oneClickWrapper.promotions = '';
                    // Since we do not have any call data to go with, query the latest ECOM call data
                    list<Call_Data__c> callDataList = new list<Call_Data__c>();
                    callDataList = [Select DNIS__r.Name,DNIS__r.oneClickBypass__c,DNIS__r.Marketing_Generic__c,DNIS__r.Business_segment__c,DNIS__r.Promotions__r.Name FROM Call_Data__c Where Account__c =: acc.Id And Partner_Id__c = 'ECOM' And DNIS__c != null order by CreatedDate DESC limit 1];
                    if(callDataList.size() > 0){
                        oneClickWrapper.dnis = callDataList[0].DNIS__r.Name;
                        if(String.isNotBlank(callDataList[0].DNIS__r.Marketing_Generic__c)){
                            oneClickWrapper.marketGeneric = callDataList[0].DNIS__r.Marketing_Generic__c;
                        }if(String.isNotBlank(callDataList[0].DNIS__r.Business_segment__c)){
                            oneClickWrapper.businessSegment = callDataList[0].DNIS__r.Business_segment__c;
                        }if(callDataList[0].DNIS__r.Promotions__c != null && String.isNotBlank(callDataList[0].DNIS__r.Promotions__r.Name)){  
                            oneClickWrapper.promotions = callDataList[0].DNIS__r.Promotions__r.Name;
                        }
                    }
                  
                    list<RequestQueue__c> existingReqQueue = new list<RequestQueue__c>();
                    existingReqQueue = [Select Id,CreatedDate From RequestQueue__c Where CreatedDate >= LAST_N_DAYS:1 And MessageID__c =:formattedPhoneNumber And ServiceTransactionType__c = 'sendToOneClick' And RequestStatus__c = 'Success' order by CreatedDate ASC LIMIT 1];          
                    oneClickWrapper.alreadySentToday = existingReqQueue.size() > 0? 'Y' : 'N';
                    oneClickWrapper.originalSubmissionDateTime = existingReqQueue.size() > 0? String.ValueOf(existingReqQueue[0].CreatedDate): '';
                    // Create JSON to store in service message
                    String jsonString = JSON.serialize(oneClickWrapper);
                    system.debug('One Click JSON: '+jsonString);
                    
                    // Create Request queue
                    RequestQueue__c rqObj = new RequestQueue__c();
                    rqObj.AccountID__c = acc.Id;
                    rqObj.RequestStatus__c = (callDataList.size() > 0 && callDataList[0].DNIS__r.oneClickBypass__c != null && callDataList[0].DNIS__r.oneClickBypass__c)? 'Bypass':'Ready';
                    rqObj.ServiceTransactionType__c = 'sendToOneClick';
                    rqObj.MessageID__c = formattedPhoneNumber;
                    rqObj.ServiceMessage__c = jsonString;
                    rqObj.RequestDate__c = system.now();
                    insert rqObj;
                }
            }
        }
    }
    
    /**
     * This method is used to make the callout to One Click
     * @Param jsonString
     * @ReturnType Void
     **/
     public static RequestQueue__c doOneClickCallOut(RequestQueue__c requeue){
        String userName = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPUsername__c:'abc';
        String password = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPPassword__c:'def';
        String endPointURL = (!Test.isRunningTest())? IntegrationSettings__c.getInstance().DPOneClickEndpoint__c:'http:abc.com';
        Decimal timeout = (!Test.isRunningTest())? IntegrationSettings__c.getInstance().DPEBRTimeout__c:6000;
        Blob headerValue = Blob.valueOf(userName + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        Httprequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type', 'application/json');
        request.setEndpoint(endPointURL);
        request.setTimeout(Integer.ValueOf(timeout));
        request.setMethod('POST');
        request.setBody(requeue.ServiceMessage__c);
        system.debug('One Click Request: '+request);
        try{
            // Send the request
            response = http.send(request);
        }catch(exception e){
            requeue.ErrorDetail__c = 'Exception: '+ e.getMessage();
        }
        system.debug('One Click Response: '+response);
        if(response != null && (response.getStatusCode() == 204 || response.getStatusCode() == 200)){
            // success response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_SUCCESS;
            requeue.ErrorDetail__c = null;
        }
        else{
            // error response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            requeue.ErrorDetail__c = String.isNotBlank(requeue.ErrorDetail__c)? requeue.ErrorDetail__c + '\n One Click Response: ' +response.getbody() : response.getbody();
        }
        return requeue;
    }
}