/************************************* MODIFICATION LOG ********************************************************************************************
 * TerritoryAssignmentTriggerHelper
 *
 * DESCRIPTION : Defines helper methods for use by TerritoryAssignmentTrigger
 *
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER						DATE				  REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 *            				 10/14/2011			- Original Version
 *
 *
 */
public with sharing class TerritoryAssignmentTriggerHelper
{

    ChangeAccountOwnership CAO = new ChangeAccountOwnership();

    public void relateTerritoryAssignmentToTerritory(List < TerritoryAssignment__c > TAs)
    {
        /*List<Id> pc = new List<Id>(); 
		for(TerritoryAssignment__c TA : TAs)
		{
			pc.add(TA.PostalCodeId__c);
		}
		List<Postal_Codes__c> pcs = [select Id, Name, BusinessId__c, IsTerritoryAssigned__c from Postal_Codes__c where Id in : pc];
		for(Postal_Codes__c pcc : pcs)
		{
			pcc.IsTerritoryAssigned__c = true;
		}
		update pcs;*/
    }

    public void AddTerritoryAssociation(List < TerritoryAssignment__c > AddedTerritories)
    {
        List < PostalCodeReassignmentQueue__c > PCQS = new List < PostalCodeReassignmentQueue__c > ();
        PostalCodeReassignmentQueue__c PCQ;
        for (TerritoryAssignment__c TA: AddedTerritories)
        {
            //add territories to a queue
            PCQ = new PostalCodeReassignmentQueue__c();
            PCQ.PostalCodeId__c = TA.PostalCodeID__c;
            PCQ.PostalCodeNewOnwer__c = TA.OwnerId;
            PCQ.TerritoryType__c = TA.TerritoryType__c;
            PCQS.add(PCQ);
        }
        insert PCQS;
    }

    public void RemoveTerritoryAssociation(List < TerritoryAssignment__c > DeletedTerritories)
    {
        List < Id > zipIds = new List < id > ();
        String postalCodeGA = '';
        Set < String > allDeletedPCTowns = new Set < String > ();
        map < String, String > allDeletedIdsAndUniqueTowns = new map < String, String > ();
        map < String, String > MTOwners = new map < String, String > ();

        //first get all distinct postal codes and get the towns and business ids associated to them
        for (TerritoryAssignment__c ta: DeletedTerritories)
        {
            zipIds.add(ta.PostalCodeId__c);
        }
        List < Postal_Codes__c > postalCodesAndTowns = [Select id, TownId__c, BusinessId__c, TownUniqueId__c from Postal_Codes__c where id in : zipIds];
        //get distinct towns and business ids from the deleted towns
        for (Postal_Codes__c pcts: postalCodesAndTowns)
        {
            allDeletedPCTowns.add(pcts.TownId__c);
            allDeletedIdsAndUniqueTowns.put(pcts.id, pcts.TownId__c + ';' + pcts.BusinessId__C);
        }
        List < ManagerTown__c > managerTowns = [select id, ManagerId__c, TownId__c, BusinessId__c, Type__c from ManagerTown__c where TownId__c in : allDeletedPCTowns];
        for (ManagerTown__c mt: managerTowns)
        {
            for (String mtt: mt.Type__c.split(';'))
            {
                MTOwners.put(mt.TownId__c + ';' + mt.BusinessId__c + ';' + mtt, mt.ManagerId__c);
            }
        }
        List < PostalCodeReassignmentQueue__c > PCQS = new List < PostalCodeReassignmentQueue__c > ();
        PostalCodeReassignmentQueue__c PCQ;
        for (TerritoryAssignment__c ta: DeletedTerritories)
        {
            PCQ = new PostalCodeReassignmentQueue__c();
            PCQ.PostalCodeId__c = ta.PostalCodeID__c;
            String managerID = MTOwners.get(allDeletedIdsAndUniqueTowns.get(ta.PostalCodeID__c) + ';' + Channels.getMappedChannel(ta.TerritoryType__c, true));
            if (managerID == null)
            {
                PCQ.PostalCodeNewOnwer__c = Utilities.getGlobalUnassignedUser();
                postalCodeGA += ta.PostalCode__c + ', ';
            }
            else
            {
                PCQ.PostalCodeNewOnwer__c = managerID;
            }
            PCQ.TerritoryType__c = TA.TerritoryType__c;
            PCQS.add(PCQ);
        }
        insert PCQS;
        if (postalCodeGA.contains(', '))
        {
            postalCodeGA = postalCodeGA.substring(0, (postalCodeGA.length() - 2));
           	if (ResaleGlobalVariables__c.getInstance('PCNewOwnerEmail') != null && (ResaleGlobalVariables__c.getInstance('PCNewOwnerEmail').value__c != '' && ResaleGlobalVariables__c.getInstance('PCNewOwnerEmail').value__c != null))
            {
                String[] toAddress = new String[] {ResaleGlobalVariables__c.getInstance('PCNewOwnerEmail').value__c};
                EmailMessageUtilities.SendEmailNotification(toAddress, 'Postal Code New Owners Set To Global Admin', 'During the Territory Assignment process, an Owner was not found for Postal Codes ' + postalCodeGA + '.  Global Admin now owns the Postal Codes.   Please check \'Manage Towns\' or \'Territories\' to ensure that zip code(s) are properly assigned.');
            }
        }
    }

    public void ChangeTerritoryAssociation(List < TerritoryAssignment__c > OldRecs, List < TerritoryAssignment__c > NewRecs)
    {
        Integer counter = 0;
        List < PostalCodeReassignmentQueue__c > PCQS = new List < PostalCodeReassignmentQueue__c > ();
        String postalCodeGA = '';
        PostalCodeReassignmentQueue__c PCQ;

        for (TerritoryAssignment__c oldTARecs: OldRecs)
        {
            if (oldTARecs.OwnerId != NewRecs[counter].OwnerId)
            {
                PCQ = new PostalCodeReassignmentQueue__c();
                PCQ.PostalCodeId__c = NewRecs[counter].PostalCodeID__c;
                if (NewRecs[counter].OwnerId == null)
                {
                    PCQ.PostalCodeNewOnwer__c = Utilities.getGlobalUnassignedUser();
                    postalCodeGA += NewRecs[counter].PostalCode__c + ', ';
                }
                else
                {
                    PCQ.PostalCodeNewOnwer__c = NewRecs[counter].OwnerId;
                }
                PCQ.TerritoryType__c = NewRecs[counter].TerritoryType__c;
                PCQS.add(PCQ);
            }
            counter += 1;
        }
        insert PCQS;
        if (postalCodeGA.contains(', '))
        {
            postalCodeGA = postalCodeGA.substring(0, (postalCodeGA.length() - 2));
            if (ResaleGlobalVariables__c.getInstance('PCNewOwnerEmail') != null && (ResaleGlobalVariables__c.getInstance('PCNewOwnerEmail').value__c != '' && ResaleGlobalVariables__c.getInstance('PCNewOwnerEmail').value__c != null))
            {
                String[] toAddress = new String[] {ResaleGlobalVariables__c.getInstance('PCNewOwnerEmail').value__c};
                EmailMessageUtilities.SendEmailNotification(toAddress, 'Postal Code New Owners Set To Global Admin', 'During the Territory Assignment process, an Owner was not found for Postal Codes ' + postalCodeGA + '.  Global Admin now owns the Postal Codes.   Please check \'Manage Towns\' or \'Territories\' to ensure that zip code(s) are properly assigned.');
            }
        }
    }

}