/************************************* MODIFICATION LOG ********************************************************************************************
* CaseTriggerHelper
*
* DESCRIPTION : Class contains logic used from triggers when DML operation are performed against Cases 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera           6/23/2014           - Original Version
* Magdiel Herrera           12/9/2014           - Refactor to support new business rules based on case complexity
*                                                   
*/

public class CaseTriggerHelper {
    
    private static ID CommissionErrorRT;
    private static ID QuoteRT;
    
    static {
        CommissionErrorRT = [SELECT Id, Name FROM RecordType WHERE Name = 'Callidus'].Id;
        QuoteRT = [SELECT Id, Name FROM RecordType WHERE Name = 'Hermes'].Id;
    }
    
    /**
     *  Run logic on case record updates
     *  
     *  @method ProcessCaseBeforeUpdate
     *
     *  @param  Map<Id, Case>   New case map containing values to commit to database
     *
     *  @param  Map<Id, Case>   Old case map containing values as they were originally
     * 
     */
    public static void ProcessCaseBeforeUpdate(Map<Id, Case> caseNewMap, Map<Id, Case> caseOldMap){     
        List<CaseShare> cShareList = new List<CaseShare>();              
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        Group callidusGroup;
        // Read callidus queue information
        try{
         callidusGroup = [SELECT Name, Type FROM Group WHERE Name = 'Callidus' AND Type = 'Queue' limit 1];
        }
        catch(Exception err){}
        // for each case on the update call
        for(Case newVal: caseNewMap.values()){
            Case oldVal = caseOldMap.get(newVal.Id);
            // Once a case is close then no further action is allowed
            if(oldVal.status == 'Closed'){
                newVal.addError('No further action is allowed on closed cases');
                continue;
            }
            // Commission Error Case
            if(newVal.RecordTypeId == CommissionErrorRT){
                
                //----------------------------------------------------------------
                //  Record consistency validations
                //----------------------------------------------------------------
                
                // Initial submitter not allowed to update status
                if( newVal.Status !=  oldVal.Status && UserInfo.getUserId() == oldVal.Commission_User__c ){
                    newVal.addError('Submitter is not allowed to update status on the case');
                    continue;
                }
                
                // Initial submitter not allowed to enter amount until MCA Needed status is set by CSI team
                if( oldVal.Status !=  'MCA Needed' && UserInfo.getUserId() == oldVal.Commission_User__c && newVal.Expected_Commission_Amount__c != null ){
                    newVal.addError('Submitter is not allowed to enter amount until MCA Needed status is set on the case');
                    continue;
                }
                
                // Once a case has been submitted to DOA approval cannot go back to MCA needed
                if( newVal.IsEscalated && newVal.Status !=  oldVal.Status && newVal.Status == 'MCA Needed'){
                    newVal.addError('Once a case has been submitted to DOA approval cannot go back to MCA needed');
                    continue;
                }
                
                
                //----------------------------------------------------------------
                //  Business rules and information flow
                //----------------------------------------------------------------
                
                // Updating this case to closed
                if( newVal.Status !=  oldVal.Status && newVal.Status == 'Closed'){
                    notifyCaseFinalDisposition(newVal);
                }
                
                // Taking this case from Callidus Queue
                if( newVal.OwnerId !=  oldVal.OwnerId && callidusGroup != null && callidusGroup.id == oldVal.OwnerId){
                    EmailMessageUtilities.SendEmailNotification(newVal.Commission_User__c, 'Your case '+oldVal.Subject+' is being worked', 'Your case is being worked, one of our team members will follow up with you.', sfdcBaseURL+'/'+oldVal.Id);
                }
                
                // MCA Needed for this case
                if( newVal.status != oldVal.Status && newVal.Status == 'MCA Needed'){
                    if( Utilities.isEmptyOrNull(newVal.Commission_User__c)){
                        newVal.addError('Unable to submit this Case for MCA. No initial Submitter found.');
                    }
                    else{
                        newVal.OwnerId = newVal.Commission_User__c;
                        notifySubmitterForMCA(newVal);
                    }
                }
                
                // Submitter adding Expected Commission Amount
                if( newVal.status == 'MCA Needed' && UserInfo.getUserId() == oldVal.Commission_User__c && newVal.Expected_Commission_Amount__c != null  ){
                    newVal.status = 'Escalated';
                    newVal.IsEscalated = true;
                    submitCaseToDOA(newVal.Id, UserInfo.getUserId());
                }
                
                // If case needs to escalate to the next approver after being escalated by the manager while in DOA then update assignee
                // No longer MCA needed
                /*if(newVal.IsEscalated == true && newVal.Approval_Levels_Reached__c != oldVal.Approval_Levels_Reached__c)
                {
                    NextAvailableAsignee(newVal);
                }*/
            }
        }
    }
    
    /**
     *  Process case records after an update call has completed 
     *
     */
    public static void ProcessCaseAfterUpdate(Map<Id, Case> caseNewMap, Map<Id, Case> caseOldMap){ }        
    
    /**
     *  Process case records before finally submitting them to database
     *
     *  @method ProcessCaseBeforeInsert
     *
     *  @param List<Case>
     *
     */
    public static void ProcessCaseBeforeInsert(List<Case> cList){
        for(Case c:cList){
            // Commission Error Case
            if(c.RecordTypeId == CommissionErrorRT){
                prepareCaseInfoToDOA(c);
            }
        }        
    }
    
    /**
     *  Process case records after record is saved to dabase and before is finally committed to database
     *  
     *  @method ProcessCaseAfterInsert
     *  
     *  @param List<Case>
     *
     */
    public static void ProcessCaseAfterInsert(List<Case> caseList){
    }
    
    private static void prepareCaseInfoToDOA(Case c){
        try{
            Map<String, User> uMap = RoleUtils.getRepApproverUserByLevel(c.Commission_User__c);
            c.Assignee_Role__c = RoleUtils.Manager;
            do{
                if( uMap.containsKey(c.Assignee_Role__c) )
                    c.Assignee__c = uMap.get(c.Assignee_Role__c).Id;
                if( c.Assignee__c == null && c.Assignee_Role__c == RoleUtils.C_Level){
                    c.AddError('Unable to locate an available asignee for this case.');
                }
                else if( c.Assignee__c == null ){
                    c.Assignee_Role__c = RoleUtils.getUpperApproverLevel(c.Assignee_Role__c);
                }
            }while (c.Assignee__c == null);
        }
        catch(Exception err){
            c.addError('Error while locating asignee for this case. '+err.getMessage()+' '+err.getStackTraceString());
        }
    }
    
    @future
    private static void submitCaseToDOA(Id cId, Id initialSubmitterId){
        try{
            // create the new approval request to submit
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Commisssion error initially submitted for review.');
            req.setObjectId(cId);
            // submit the approval request for processing
            Approval.ProcessResult result = Approval.process(req);          
        }
        catch(Exception err){
            // Notify case initial submitter
            String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
            EmailMessageUtilities.SendEmailNotification(initialSubmitterId, 'Unable to submit case to DOA ', err.getMessage() + ' ' + err.getStackTraceString(), sfdcBaseURL+'/'+cId);
        }
    }
    
    /**
     *  Let the initial submitter of the case that MCA is needed and the case was escalated
     *
     *  @method notifySubmitterForMCA
     *
     *  @param  Case
     *
     */
    private static void notifySubmitterForMCA(Case c){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        mail.setTargetObjectId(c.OwnerId);
        mail.setSenderDisplayName('Salesforce CSI Team Support');
        mail.setUseSignature(false);
        mail.setBccSender(false);
        mail.setSaveAsActivity(false);
        EmailTemplate et;
        String htmlBody;
        String subject;
        try{
            et = [SELECT id, HtmlValue, Subject FROM EmailTemplate WHERE DeveloperName = 'Case_Callidus_Case_MCA_Needed'];
            subject = et.Subject.replace('{!Case.CaseNumber}', c.CaseNumber);
            htmlBody = et.HtmlValue.replace('{!Case.CaseNumber}', c.CaseNumber);
            htmlBody = htmlBody.replace('{!Case.Subject}', c.Subject);
            htmlBody = htmlBody.replace('{!Case.Id}',  sfdcBaseURL+'/'+c.Id);
        }
        catch(Exception err){
            c.addError('Cannot send to MCA. Unable to locate communication email for submitter. Please contact your admin and let him know.');
        }
        if(et != null){
            //mail.setTemplateId(et.id);
            mail.setSubject(subject);
            mail.setHtmlBody(htmlBody);
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});        
        }
    }
    
    /**
     *  Sends out notification email to actors involved on a case final disposition
     *  
     *  @method notifyCaseFinalDisposition
     *  
     *  @param Case
     *
     */
    private static void notifyCaseFinalDisposition(Case c){
        
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        Set<Id> caseActorIds = new Set<Id>();
        String EMAIL_BODY = '';
        String CaseFinalDispo =  c.Commission_Error_Final_Disposition__c;
        
        // Case was solved without reaching approval process
        if( !c.IsEscalated ){
            caseActorIds.add(c.OwnerId);
            EMAIL_BODY = '<style> body { font-family:Arial, Helvetica, sans-serif; font-size:12px; } </style>'; 
            if( !Utilities.isEmptyOrNull(CaseFinalDispo) ){
            	EMAIL_BODY += '<hr/>'+CaseFinalDispo+'<hr/> <br/>';
            }
            else{
            	EMAIL_BODY += '';
            }
            EMAIL_BODY += 'Case Closed';
        }
        // Case was escalated to upper levels
        /*else{
            try{
                ProcessInstance pi = [SELECT Id, TargetObjectId, Status, ProcessDefinitionId, (SELECT Id, ProcessInstanceId, StepStatus, OriginalActorId, OriginalActor.Name, ActorId, Actor.Name,Actor.Email, Comments, StepNodeId, CreatedDate FROM Steps Order By CreatedDate Desc) FROM ProcessInstance WHERE TargetObjectId = :c.Id];
                if(pi.Steps!= null && !pi.Steps.isEmpty()){                    
                    EMAIL_BODY = '<style> body { font-family:Arial, Helvetica, sans-serif; font-size:12px; } </style>'; 
		            if( !Utilities.isEmptyOrNull(CaseFinalDispo) ){
		            	EMAIL_BODY += '<hr/>'+CaseFinalDispo+'<hr/> <br/>';
		            }
		            else{
		            	EMAIL_BODY += '';
		            }
                    EMAIL_BODY += '<table cellpadding="5" cellspacing="0" width="100%" style="border: 1px solid #ccc;font-family:Arial, Helvetica, sans-serif; font-size:12px;"><tr style="background-color: #EEEEEE; height: 25px;font-weight:bold"><td>Date</td><td>Status</td><td>Assigned To</td><td>Actual Approver</td><td>Comments</td></tr>';
                    for(ProcessInstanceStep step :pi.Steps){
                        caseActorIds.add(step.ActorId);
                        EMAIL_BODY += '<tr style="height: 20px;">';
                        EMAIL_BODY += '<td>'+step.CreatedDate+'</td><td style="color: '+( (step.StepStatus=='Approved' || step.StepStatus=='Started')?'green':'red' )+'">'+step.StepStatus+'</td><td>'+step.OriginalActor.Name+'</td><td>'+step.Actor.Name+'</td><td>'+step.Comments+'</td>';
                        EMAIL_BODY += '</tr>';
                    }
                    EMAIL_BODY += '</table>';
                    
                }
            }
            catch(Exception err){
            }
        }*/
        
        // Notify parties involved on case management in case of any
        for(Id uId: caseActorIds){
            EmailMessageUtilities.SendEmailNotification(uId, 'Final disposition on Commission Error '+c.CaseNumber, EMAIL_BODY, sfdcBaseURL+'/'+c.Id);
        }
        
        // Notify case initial submitter
        EmailMessageUtilities.SendEmailNotification(c.Commission_User__c, 'Final disposition on Commission Error '+c.CaseNumber, EMAIL_BODY, sfdcBaseURL+'/'+c.Id);
        
    }
    
    /**
     *  Assigns case to next user evaluating role hierarchy
     *  
     *  @method NextAvailableAsignee
     *  
     *  @param  Case
     *
     */
    /*public static void NextAvailableAsignee(Case newVal){
        Id currentAsignee = newVal.Assignee__c;
        RoleUtils.HierarchyLevel h = RoleUtils.getUserAsigneeHierarchy([SELECT Name, Business_Unit__c FROM User WHERE ID = :newVal.Commission_User__c]);                
        Map<String, User> OwnerManagerMap = RoleUtils.getRepApproverUserByLevel(newVal.Commission_User__c);
        String approverLevel = '';          
        
        for(String level: OwnerManagerMap.keySet())
        {
            User approverUser = OwnerManagerMap.get(level);
            if(approverUser.Id == currentAsignee)
                approverLevel = h.getNextUpperApprover(level);
        }
        
        if(OwnerManagerMap.containsKey(approverLevel))
        {
            newVal.Assignee__c = OwnerManagerMap.get(approverLevel).Id;
        }
        else
        {
            newVal.addError('Unable to determine assignee for '+approverLevel+'');
        }       
        
    }*/
    
    
}