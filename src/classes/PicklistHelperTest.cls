@isTest
private class PicklistHelperTest {
    
    static testMethod void testGetResultSizeValues() {
        
        Test.startTest();
        
        List<selectOption> l = PicklistHelper.getResultSizeValues();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 3 elements
        System.assertEquals(3, l.size(), 'Expect 3 elements in the list');
        //TODO
        // test the actual values returned
        
        Test.stopTest();
    
    }
    
    static testMethod void testGetDataSourceValues() {
        
        Test.startTest();
        
        List<selectOption> l = PicklistHelper.getDataSourceValues();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 15 elements
        //System.assertEquals(16, l.size(), 'Expect 16 elements in the list');
        //TODO
        // test the actual values returned
        
        Test.stopTest();
    
    }
    
    static testMethod void testGetAccountTypeValues() {
        
        Test.startTest();
        
        List<selectOption> l = PicklistHelper.getAccountTypeValues();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 5 elements
        System.assertEquals(5, l.size(), 'Expect 5 elements in the list');
        //TODO
        // test the actual values returned
        
        Test.stopTest();
    
    }
    
    static testMethod void testGetSiteStateValues() {
        
        Test.startTest();
        
        List<SelectOption> l = PicklistHelper.getSiteStateValues();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 65 elements
        System.assertEquals(65, l.size(), 'Expect 65 elements in the list');
        //TODO
        // test the actual values returned
        /*String prevLabel = '';
        for (SelectOption o : l) {
            if (prevLabel != 'All') {
                System.assert(o.getLabel() > prevLabel, 'The label ' + o.getLabel() + ' was preceeded by this one: ' + prevLabel);
            }
            prevLabel = o.getLabel();
            
        }*/
        // the above assertion no longer applies since Canadian provinces were added
        
        Test.stopTest();
    
    }
    
    static testMethod void testGetDiscoReasonValues() {
        
        Test.startTest();
        
        List<selectOption> l = PicklistHelper.getDiscoReasonValues();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains at least 102 elements
        // Note: it's possible that over time disco reasons will be added or removed
        System.assert(l.size() > 102, 'Expect at least 102 elements in the list');
        //TODO
        // test the actual values returned
        
        Test.stopTest();
    
    }
    
    static testMethod void testGetDispositionCodeValues() {
        
        Test.startTest();
        
        List<selectOption> l = PicklistHelper.getDispositionCodeValues();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains at least 23 elements
        // Note: it's possible that over time disposition code values will be added or removed
        System.assert(l.size() > 12, 'Expect at least 13 elements in the list');
        //TODO
        // test the actual values returned
        
        Test.stopTest();
    
    }
    
    static testMethod void testGetNewMoverTypeValues() {
        
        Test.startTest();
        
        List<selectOption> l = PicklistHelper.getNewMoverTypeValues();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 11 elements
        System.assertEquals(11, l.size(), 'Expect 11 elements in the list');
        //TODO
        // test the actual values returned
        
        Test.stopTest();
    
    }

}