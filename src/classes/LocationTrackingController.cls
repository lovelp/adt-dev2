/************************************* MODIFICATION LOG ********************************************************************************************
* LocationTrackingController
*
* DESCRIPTION : Used by the LocationTracking VisualForce page to obtain all dynamic data for display.  
*               It is a Controller in the SFDC MVC architecture.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					10/14/2011				- Original Version
*
*													
*/
public with sharing class LocationTrackingController {

	// Hold user entered data
	public String selectedMapType {get;set;}
	public String selectedRep {get;set;}
	public String selectedTeam {get;set;}
	public String selectedDate {get;set;}
	
	// Contain values for drop down lists
	public List<SelectOption> repList {get;set;}
	public List <SelectOption> teamList {get;set;}
	
	// Indicates if the map should be displayed
	public boolean bMapValid {get;set;}
	// Contains map data to be rendered
	public String mapDataPointsStr {get; set;}
	
	public List<LocationDataMessageProcessor.LocationData> tableData {get;set;}
	
	private List <LocationItem> locationHistoryList {get;set;}//Locations
	 
    public LocationTrackingController() {
  	
    	selectedMapType=LocationDataProvider.CURRENT_LOCATION_MAP_TYPE;//Initial value (find now)
    	bMapValid=false;
    	tableData= new List <LocationDataMessageProcessor.LocationData>();
    
    	teamList = new List <SelectOption>();
    	repList = new List <SelectOption>();
       	
    	locationHistoryList = new List <LocationItem>();
    	                  
		getReps();
		getTeams();
		

    }

		 
	private void getReps()
	{
        Map<Id, User> AllAvailableSalesRepsForManager = new Map<Id, User>();
        
    	//get the user id and the related towns that the user/manager is assigned to
		Id currentUserRoleId = UserInfo.getUserRoleId();
		
		List<Id> matrixedRoles = new List<Id>();
		matrixedRoles = Utilities.getMatrixedRolesForManager(currentUserRoleId);
		List<Id> allRoles = new List<Id>();
		allRoles.add(currentUserRoleId);
		if(matrixedRoles != null)
			allRoles.addAll(matrixedRoles);
		
		//get all subordinate roles for the user
		List<UserRole> subRoles = [select id, name from UserRole where parentRoleId =: allRoles];
		List<id> allSubordinateRoleIds = new List<Id>();
		for(UserRole ur: subRoles)
		{
			allSubordinateRoleIds.add(ur.id);
		}

		//get all salesreps for the manager
		List<User> allSubordinateUsers = [select id, name, IsActive from User where UserRoleId in : allSubordinateRoleIds];
		for(User u : allSubordinateUsers)
		{
			AllAvailableSalesRepsForManager.put(u.id, u);
		}


        SelectOption anOption;
        User u;
        String Label;
        repList.add(new SelectOption('NONE',''));
        for(Id uid : AllAvailableSalesRepsForManager.KeySet())
        {
        	u = AllAvailableSalesRepsForManager.get(uid);
        	if(u.IsActive)
        	{
				Label = u.Name;
        		anOption = new SelectOption(u.id, Label);
				repList.add(anOption);
        	}
        } 
	}
	
	private void getTeams() 
	{
    	teamList.add(new SelectOption('NONE',''));
		teamList.add(new SelectOption(UserInfo.getUserId(),'My Team'));
    }
    
    

	public PageReference process() {
		
		clear();
		if (selectedMapType == '0'){
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a View Type.'));
			bMapValid=false;
		}
		else {
			LocationDataMessageProcessor processor = LocationDataFactory.getProcessor(selectedMapType);
			if (processor == null) {
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The View Type (' + selectedMapType + ') is not recognized and cannot be processed.'));
				bMapValid=false;
			}
			if (processor.isValidInput(selectedRep, selectedTeam, selectedDate)) {
				LocationDataMessageParameters params = processor.buildParameters(selectedRep, selectedTeam, selectedDate);
				LocationDataProvider provider = LocationDataFactory.getProvider();
				locationHistoryList = provider.getLocationItems(params);
				bMapValid = processor.interpretResults(params, locationHistoryList);
				processor.prepareForMapping(locationHistoryList);
				tableData = processor.getLocationDataList(locationHistoryList);
				mapDataPointsStr = processor.getMapData(locationHistoryList);	
				System.debug('mapDataPointsStr...:'+mapDataPointsStr);	
			}
			else {
				bMapValid=false;
			}
				
		}
		return null;
		
	}
	
	private void clear(){
		tableData.clear();	
		locationHistoryList.clear();	
    	bMapValid=true;
	}
    

    public PageReference initMapLoad() {
        return null;
    }
    


}