/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : ChatterTriggerTest is a test class for PreventChatterFeedComments.trigger and PreventChatterFeedPosts.trigger
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            03/02/2012          - Origininal Version
*
*                                                    
*/
@isTest(SeeAllData = true)
public with sharing class ChatterTriggerTest
{
	static testmethod void ChatterTriggerPostTest()
    {
        CollaborationGroup postGroup = new CollaborationGroup(Name = 'Post', OwnerId = UserInfo.getUserId(), CollaborationType = 'Public');
        insert postGroup;
        Chatter_Group_Restriction__c cg = new Chatter_Group_Restriction__c(Name='Post',Restriction_Type__c='Cannot Post');
        insert cg;
        FeedItem post = new FeedItem();
        post.parentId = postGroup.Id;
        post.body = 'This Works @' + Userinfo.getName();
        insert post;

        FeedComment comment = new FeedComment(CommentBody = 'Comment', FeedItemId = post.Id);
        insert comment;

        User u = TestHelperClass.createManagerUser();

        system.runAs(u)
        {
            post = new FeedItem();
            post.body = 'This Does Not';
            post.parentId = postGroup.Id;
            try
            {
                insert post;
            }
            catch (Exception e)
            {
                //Assert Error Message
                System.assert(e.getMessage().contains('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Posting to this group is limited to group owners and managers.: []'),
                    e.getMessage());

                //Assert Status Code
                System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION',
                    e.getDmlStatusCode(0));
            }
        }
        // for ValidateChatterGroupName trigger coverage
        try{
	        Chatter_Group_Restriction__c cg1 = new Chatter_Group_Restriction__c(Name='Test',Restriction_Type__c='Cannot Post');
	        insert cg1;
        } catch(Exception e){
        	//Assert Error Message
                System.assert(e.getMessage().contains('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, The Chatter Group Name you entered does not exist. Please check the name of the group you are trying to enter and try again.: []'),
                    e.getMessage());
        }
        u = [SELECT Id, ChatterExcluded__c FROM User WHERE Id = :Userinfo.getUserId()];
        u.ChatterExcluded__c = true;
        update u;
        
        try {
        	post = new FeedItem(Body = 'Post Fails', parentId = postGroup.Id);
            insert post;
        }
        catch (Exception e){
         }
    }

    static testmethod void ChatterTriggerPostCommentTest()
    {
        CollaborationGroup postCommentGroup = new CollaborationGroup(Name = 'PostComment', OwnerId = UserInfo.getUserId(), CollaborationType = 'Public');
        insert postCommentGroup;
        Chatter_Group_Restriction__c cg = new Chatter_Group_Restriction__c(Name='PostComment',Restriction_Type__c='Cannot Post or Comment');
        insert cg;

        FeedItem post = new FeedItem();
        post.parentId = postCommentGroup.Id;
        post.body = 'This Works';
        insert post;

        FeedComment comment = new FeedComment(CommentBody = 'Comment @' + Userinfo.getName(), FeedItemId = post.Id);
        insert comment;

        User u = TestHelperClass.createManagerUser();

        system.runAs(u)
        {
            FeedItem post2 = new FeedItem();
            post2.body = 'This Does Not';
            post2.parentId = postCommentGroup.Id;
            try
            {
                insert post2;
            }
            catch (Exception e)
            {
                //Assert Error Message
                System.assert(e.getMessage().contains('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Posting to this group is limited to group owners and managers.: []'),
                    e.getMessage());

                //Assert Status Code
                System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION',
                    e.getDmlStatusCode(0));
            }

            try
            {
                comment = new FeedComment(CommentBody = 'Comment Fails', FeedItemId = post.Id);
                insert comment;
            }
            catch (Exception e)
            {
                //Assert Error Message
                System.assert(e.getMessage().contains('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Commenting to this group is limited to group owners and managers.: []'),
                    e.getMessage());

                //Assert Status Code
                System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION',
                    e.getDmlStatusCode(0));
            }


        }
        u = [SELECT Id, ChatterExcluded__c FROM User WHERE Id = :Userinfo.getUserId()];
        u.ChatterExcluded__c = true;
        update u;
        
        try {
        	comment = new FeedComment(CommentBody = 'Comment Fails', FeedItemId = post.Id);
            insert comment;
        }
        catch (Exception e){
         }
    }
}