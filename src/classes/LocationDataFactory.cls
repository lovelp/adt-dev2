/************************************* MODIFICATION LOG ********************************************************************************************
* LocationDataFactory
*
* DESCRIPTION : Defines factory methods for constructing the correct LocationDataProvider based on custom settings 
*               and the correct LocationDataMessageProcessor based on map type.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					10/14/2011				- Original Version
*
*													
*/
public class LocationDataFactory {
	
	private static final String TELENAV_TRACK = 'TeleNavTrack';
	private static final String MOCK_PROVIDER = 'MockProvider';
	
	public static LocationDataProvider getProvider() {
		
		LocationDataProvider ldp = null;
		
		IntegrationSettings__c settings = IntegrationSettings__c.getInstance();
		
		if (settings.LocationDataProvider__c == TELENAV_TRACK) {
			ldp = new TeleNavLocationDataProvider();
		}
		else if (settings.LocationDataProvider__c == MOCK_PROVIDER) {
			ldp = new MockLocationDataProvider();
		} 
		
		return ldp;
	}
	
	public static LocationDataMessageProcessor getProcessor(String mapType) {
		
		if (mapType == LocationDataProvider.CURRENT_LOCATION_MAP_TYPE) {
			return new CurrentLocationMessageProcessor();
		}
		
		if (mapType == LocationDataProvider.DAILY_LOCATION_MAP_TYPE)	{
			return new DailyLocationMessageProcessor();
		}
		
		if (mapType == LocationDataProvider.DISPOSITION_LOCATION_MAP_TYPE) {
			return new DispositionLocationMessageProcessor();
		}
		
		return null; 
	}

}