@isTest
public class HotLeadsControllerTest {
  @isTest
    public static void testHotLead(){
        
        user u = new User();
        u.id = UserInfo.getUserId();
        
        system.runAs(u){
        ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
        res.name = 'DataRecastDisableAccountTrigger';
        res.value__c = 'TRUE';
        insert res;
            
        Wireless_Carrier__c wirelesObj = new Wireless_Carrier__c();
        wirelesObj.Name = 'Verizon';
        wirelesObj.SMS_Email_Service__c = 'number@vtext.com';
        wirelesObj.SMS_Enabled__c = TRUE;
        insert wirelesObj;
        }
        
                
        Address__c addObj = ADTPartnerCustomerLookupTest.createAddr();
        Account acc = ADTPartnerCustomerLookupTest.CreateAccount(addObj);
        acc.Email__c = 'a@tcs.com';
        acc.OwnerId = u.id;
        acc.Channel__c = 'Home Health';
        acc.MMBOrderType__c = 'Test';
        update acc;
        
        TerritoryAssignment__c terriassgObj = new TerritoryAssignment__c();
        terriassgObj.PostalCodeID__c = acc.PostalCodeID__c;
        terriassgObj.TerritoryType__c ='Home Health';
        insert terriassgObj;
        
        ManagerTown__c managrTwnObj = new ManagerTown__c();
        managrTwnObj.ManagerID__c= u.Id;
        managrTwnObj.TownID__c = '1100';
        insert managrTwnObj ;
        
        ApexPages.currentPage().getParameters().put('usId',u.id);
        ApexPages.currentPage().getParameters().put('userEmail','a@tcs.com');
        ApexPages.currentPage().getParameters().put('ph','1111111111');
        ApexPages.StandardController sc = new Apexpages.StandardController(acc);
        HotLeadsController hotLead = new HotLeadsController(sc);
        hotLead.changeOwnership();
        
    }
    
    
    @isTest
    public static void testHotLead1(){
        
        user u = new User();
        u.id = UserInfo.getUserId();
        
        system.runAs(u){
        ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
        res.name = 'DataRecastDisableAccountTrigger';
        res.value__c = 'TRUE';
        insert res;
            
        Wireless_Carrier__c wirelesObj = new Wireless_Carrier__c();
        wirelesObj.Name = 'Verizon';
        wirelesObj.SMS_Email_Service__c = 'number@vtext.com';
        wirelesObj.SMS_Enabled__c = TRUE;
        insert wirelesObj;
        }
        
                
        Address__c addObj = ADTPartnerCustomerLookupTest.createAddr();
        Account acc = ADTPartnerCustomerLookupTest.CreateAccount(addObj);
        acc.Email__c = 'a@tcs.com';
        acc.OwnerId = u.id;
        acc.Channel__c = 'National Account Sales';
        acc.MMBOrderType__c = 'Test';
        update acc;
        
        TerritoryAssignment__c terriassgObj = new TerritoryAssignment__c();
        terriassgObj.PostalCodeID__c = acc.PostalCodeID__c;
        terriassgObj.TerritoryType__c ='Home Health';
        insert terriassgObj;
        
        ManagerTown__c managrTwnObj = new ManagerTown__c();
        managrTwnObj.ManagerID__c= u.Id;
        managrTwnObj.TownID__c = '1100';
        insert managrTwnObj ;
        
        ApexPages.currentPage().getParameters().put('usId',u.id);
        ApexPages.currentPage().getParameters().put('userEmail','a@tcs.com');
        ApexPages.currentPage().getParameters().put('ph','1111111111');
        ApexPages.StandardController sc = new Apexpages.StandardController(acc);
        HotLeadsController hotLead = new HotLeadsController(sc);
        hotLead.changeOwnership();
        
    }

}