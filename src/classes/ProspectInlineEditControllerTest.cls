/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ProspectInlineEditControllerTest {

    static testMethod void testGetItems() {
        StreetSheet__c ss = buildStreetSheet();
        
        test.setCurrentPage(Page.ProspectInlineEdit);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ss);
        ProspectInlineEditController pic;
        
        test.startTest();
        	pic = new ProspectInlineEditController(sc);
        test.stopTest();
        
        system.assertEquals(1, pic.leads.size());
        system.assertEquals(1, pic.accounts.size());
    }
    
    static testMethod void testDispAccount() {
        StreetSheet__c ss = buildStreetSheet();
        
        test.setCurrentPage(Page.ProspectInlineEdit);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ss);
        ProspectInlineEditController pic;
        
        Account a;
        
        String newDispCode = 'FAKE DISPOSITION';
        
        test.startTest();
        	pic = new ProspectInlineEditController(sc);
        	a = pic.accounts[0];
        	pic.dispAccountId = a.Id;
        	pic.accountDisp();
        	pic.dispAccount.DispositionCode__c = newDispCode;
        	pic.saveDisp();
        test.stopTest();
    	
    	Account aConfirm = [Select Id, DispositionCode__c from Account where Id = :a.Id];
    	system.assertEquals(newDispCode, aConfirm.DispositionCode__c);
    }
    
    static testMethod void testDispLead() {
        StreetSheet__c ss = buildStreetSheet();
        
        test.setCurrentPage(Page.ProspectInlineEdit);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ss);
        ProspectInlineEditController pic;
        
        Lead l;
        
        String newDispCode = 'FAKE DISPOSITION';
        
        test.startTest();
        	pic = new ProspectInlineEditController(sc);
        	l = pic.leads[0];
        	pic.dispLeadId = l.Id;
        	pic.leadDisp();
        	pic.dispLead.DispositionCode__c = newDispCode;
        	pic.saveDisp();
        test.stopTest();
    	
    	Lead lConfirm = [Select Id, DispositionCode__c from Lead where Id = :l.Id];
    	system.assertEquals(newDispCode, lConfirm.DispositionCode__c);
    }
    
    static testMethod void testDeleteSSItem() {
    	StreetSheet__c ss = buildStreetSheet();
        
        test.setCurrentPage(Page.ProspectInlineEdit);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ss);
        ProspectInlineEditController pic;
        
        test.startTest();
        	pic = new ProspectInlineEditController(sc);
        	pic.deleteItemId = pic.accounts[0].Id;
        	pic.deleteSSItem();
        	pic.deleteItemId = pic.leads[0].Id;
        	pic.deleteSSItem();
        test.stopTest();
        
        list<StreetSheetItem__c> ssitems = new list<StreetSheetItem__c>([Select Id from StreetSheetItem__c where StreetSheet__c = :ss.Id]);
        
        system.assertEquals(0, ssitems.size());
    }
    
    private static StreetSheet__c buildStreetSheet() {
    	StreetSheet__c ss;
    	String ssid;
    	Account a;
    	Lead l;
    	User u;
    	User current = [Select Id from User where Id = :UserInfo.getUserId()];
    	system.runas(current) {
    		u = TestHelperClass.createSalesRepUser();
    		a = TestHelperClass.createAccountData();
    		l = TestHelperClass.createLead(u);
    		ssid = StreetSheetManager.create(new set<String>{a.Id, l.Id});
    	}
    	ss = [Select Id from StreetSheet__c where Id = :ssid];
    	return ss;
    }
}