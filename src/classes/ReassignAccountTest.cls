@isTest
private class ReassignAccountTest{ 
    @isTest(SeeAllData=true) static void reassignAccount(){
        //Test.setMock(WebServiceMock.class, new ADTSalesMobilityFacadeMockImpl());
        RecordType rectypes = [Select Id From RecordType Where Name='Company Generated Appointment'];
        
        User salesRep = TestHelperClass.createSalesRepUserWithManager();
        UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
        User managerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId and isActive = true Limit 1];
        User adminUser = TestHelperClass.createAdminUser();
        Account a1;
        //User currentUser=[Select ID From User Where Id=:UserInfo.getUserID()];
        system.runas(adminUser){
            a1 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential',1);
            a1.OwnerId=managerUser.Id;
            a1.AccountStatus__c='INQ';
            update a1;
        }

        system.runas(managerUser){
            ApexPages.PageReference ref = Page.ReassignAccount;
            ref.getParameters().put('Id', a1.Id);
            Test.setCurrentPage(ref);
            Test.startTest();
            
            ReassignAccountController rl = new ReassignAccountController();
            rl.getSelectedSalesRep();
            rl.getallSalesreps();
            rl.setSelectedSalesRep(salesRep.Id);
            rl.Save();
            rl.Cancel();
            Test.stopTest();
            Event e= new Event();
            e.WhatId=a1.Id;
            e.StartDateTime=DateTime.now().addHours(2);
            e.EndDateTime=DateTime.now().addHours(4);
            e.RecordTypeId=rectypes.Id;
            insert e;
            
            rl = new ReassignAccountController();
            rl.getSelectedSalesRep();
            rl.getallSalesreps();
            rl.setSelectedSalesRep(salesRep.Id);
            rl.Cancel(); 
            
        }
    }
}