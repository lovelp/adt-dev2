/*
 * Description: This is a test class for the following classes
                1. GoogleGeoCodeSchedulable 
                2. GoogleGeoCodeBatch
 * Created By: Shiva Pochamalla
 *
 * ---------------------------------------------------------------------------Changes below--------------------------------------------------------------------------------
 * 
 *
 */


@isTest
private class GoogleGeoCodeSchedulableTest{
    public static void createTestData(){
        TestHelperClass.createReferenceDataForTestClasses();
        //create address for creating accounts
        Address__c addr = new Address__c();
        addr.Latitude__c = null;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
        insert addr;
    }
    
    private static testmethod void testGoogleGeoCodeSchedulable(){
        createTestData();
        test.startTest();
            GoogleGeoCodeSchedulable ggcs=new GoogleGeoCodeSchedulable();
            String chron = '0 0 21 * * ?';        
            system.schedule('Test Geo Code Sched', chron, ggcs);
        test.stopTest();
        
    }
}