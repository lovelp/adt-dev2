/*
 * Description: This is a test class for the following classes
 * 1.StreetSheetExpirationProcessor
 * Created By: Ravi Pochamalla
 *
 */

@isTest
private class StreetSheetExpirationProcessorTest{
public static void createTestData(){
        TestHelperClass.createReferenceDataForTestClasses();
    
    }
        
        
         static testMethod void testStreetSheetExpirationProcessor(){
         createTestData();
         test.startTest();
         StreetSheetExpirationProcessor ssep=new StreetSheetExpirationProcessor(); 
         String chron = '0 0 21 * * ?';        
         system.schedule('Test Street Exp', chron, ssep);
         test.stopTest();
        }
         
         
         }