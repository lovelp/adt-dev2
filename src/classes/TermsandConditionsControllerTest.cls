/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : TermsandConditionsControllerTest is a test class for TermsandConditionsController.cls
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Erin McGee      07/08/2013      - Original Version
*
*                           
*/
@isTest
private class TermsandConditionsControllerTest {
	
	@isTest static void TermsandConditionsControllerTest() 
	{
		Terms_and_Condition__c tc= new Terms_and_Condition__c();
		PageReference pageRef= Page.TermsandConditions;
		Test.setCurrentPageReference(pageRef);
		TermsandConditionsController tcc= new TermsandConditionsController();
		tcc.redirect();
		tc.Terms_and_Conditions_Text__c='Text';
		insert tc;
		tcc.redirect();
	}
	
}