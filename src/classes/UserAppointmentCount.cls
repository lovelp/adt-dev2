/************************************* MODIFICATION LOG ********************************************************************************************
* UserAppointmentCount
*
* DESCRIPTION : Class used to aggregate appointments by sales rep to be evaluated later either on reporting or in a round robin for MATRIX scheduler
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          TICKET        REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Anna Mounika         06/13/2016                  - Original Version
* Divya Rai            05/19/2017    HRM 5247      - Added by TCS to Schedule appointment daily for DLL 
*                          
*/

public class UserAppointmentCount {
    private static final String BUSINESSID_RESIDENTIAL = '1100';
    public static void setAppointmentUserAggregate( Id eventId, Boolean isInsert){
        setAppointmentUserAggregate(eventId, null, isInsert);
    }
    
    /**
        setAppointmentUserAggregate  Update the tracking of appointments for the user this event is assigned to
        
        @param  eventId     Id of the Event to track as part of the user historical
        
        @param  isInsert    Flag to identify this event as a new event been created instead of updated
    */
    public static void setAppointmentUserAggregate( Id eventId, Id ownerBeforeCancelled, Boolean isInsert){
        String ParentTerritory;
        String SubTerritory;

        // Holds the values of the event record all fields.
        Event eve = [select Id, WhatId, ShowAs, DurationInMinutes, Appointment_Type__c, StartDateTime, EndDateTime, TaskCode__c, ScheduleID__c, InstallTechnician__c, Status__c, Subject, OwnerID, OwnerWhenCanceled__c, IsReminderSet, ReminderDateTime, Description, RecordTypeId, WhoId from Event where Id = :eventId];
        Id EventOwnerId = (String.isBlank( ownerBeforeCancelled )? eve.OwnerID:ownerBeforeCancelled );
        
        // Reject any appointment not created on MATRIX
        if(String.isBlank(eve.Appointment_Type__c) ){
            return;
        }
                
        User AppointmentcountUser = [SELECT Name, Email, MobilePhone FROM User WHERE Id = :EventOwnerId];
        
        //Account Query to have the values of the Territory and sub territory.  
        Account a = [Select Id,TMSubTownID__c, TMTownID__c,Business_Id__c from account where id = :eve.WhatId];

        // Map account business id to schedule business id
        // HRM - 10450 Biz Id Changes
        String bId = Channels.getFormatedBusinessId(a.Business_Id__c, Channels.BIZID_OUTPUT.NUM);
        if(String.isBlank(bId)){
            bId = BUSINESSID_RESIDENTIAL;
        }
        //Querying the Scheduled User Terr object to find the unique record that has been scheduled for a combination of the teritorry and a user. In which this record helps to hold the UserAppointmentCount object History values.
        String schUsrQuery = 'SELECT Id, Order_Types__c, Quota__c, TMTownID__c, TMSubTownID__c FROM SchedUserTerr__c WHERE User__c = \''+EventOwnerId+'\' ';
        
        //Do not include business ID on express activation appointments use DLL for the town
        if( eve.Appointment_Type__c == 'In House Activation' ){
            schUsrQuery += ' AND TMTownID__c = \''+UserScheduleController.IN_HOUSE_ACTIVATION+'\' ';
        }else {
            schUsrQuery += ' AND BusinessId__c = \''+bId+'\' ';
            if (String.isNotBlank( a.TMTownID__c ) ){
                schUsrQuery += ' AND TMTownID__c = \''+a.TMTownID__c+'\' ';
                if(String.isNotBlank( a.TMSubTownID__c )){
                    schUsrQuery += ' AND TMSubTownID__c = \''+a.TMSubTownID__c+'\' ';
                }
            }
        }
        
        schUsrQuery += ' limit 1 ';
        
        SchedUserTerr__c schUsrObj;
        List<sObject> schObjList = Database.query(schUsrQuery);
        if( schObjList !=null && !schObjList.isEmpty() ){
            schUsrObj = (SchedUserTerr__c)schObjList[0];
        }
        
        if( schUsrObj == null ){
            system.debug('No schedule found to aggregate this appointment.');
            return;
        }
        
        //TerritoryAssignment__c ta = [select Id,name,Ownerid from TerritoryAssignment__c where OwnerId =: schUsrObj.User__c and TerritoryType__c LIKE  schUsrObj.Order_Types__c];
        
        //Querying the UserAppointmentCount Object to  check if there is an existing record, if it is there Update the appointment counts accordingly. If there is no record then create one.
        User_Appointment_Count__c userApptcount;
        String userApptcountQuery = 'SELECT Id, SchedUserTerr__r.TMTownID__c, SchedUserTerr__r.TMSubTownID__c, SchedUserTerr__r.BusinessId__c, Additional_Appointments_Cancelled__c,Additional_Appointments_Set__c,Additional_Appointments__c,CreatedById,CreatedDate,IsDeleted,LastModifiedById,Last_Appointment_Date_Time__c, Last_Appointment_Set_Date_Time__c,Name,Order_Appointment_Type__c,OwnerId,Period_Month__c,Period_Year__c,Period__c,Quota_Appointments_Cancelled__c,Quota_Appointments_Set__c,Quota_Appointments__c,SchedUserTerr__c,Sub_Territory__c,SystemModstamp,Territory__c,User__c FROM User_Appointment_Count__c ';
        userApptcountQuery += ' WHERE User__c = \''+EventOwnerId+'\' ';
        
        // Load aggregate used by round robin
        userApptcountQuery += ' AND SchedUserTerr__c = \''+schUsrObj.Id+'\' ';
        
        userApptcountQuery += ' AND Order_Appointment_Type__c =\''+eve.Appointment_Type__c+'\'';
        //Added by TCS HRM 5247
        if(eve.Appointment_Type__c == 'In House Activation' ){
            userApptcountQuery += ' AND Period_Date__c = '+eve.StartDateTime.day();
        }
        //End
        
        userApptcountQuery += ' AND Period_Month__c = '+eve.StartDateTime.month();
        userApptcountQuery += ' AND Period_Year__c = '+eve.StartDateTime.year();
        userApptcountQuery += ' limit 1';
        List<sObject> userAppointmentCountObjList = Database.query(userApptcountQuery);
        if( userAppointmentCountObjList !=null && !userAppointmentCountObjList.isEmpty() ){
            userApptcount = (User_Appointment_Count__c)userAppointmentCountObjList[0];
        }else {
            // no record found
            userApptcount = new User_Appointment_Count__c( Name = AppointmentcountUser.Name+'_'+eve.Appointment_Type__c );
            if( eve.Appointment_Type__c == 'In House Activation'){
                userApptcount.Name += '_'+schUsrObj.TMTownID__c;
            }else{
                userApptcount.Name += '_'+a.TMTownID__c;
                if(String.isNotBlank(a.TMSubTownID__c) ){
                    userApptcount.Name = userApptcount.Name + '_'+a.TMSubTownID__c;
                }
            }
            userApptcount.Order_Appointment_Type__c = eve.Appointment_Type__c;
            userApptcount.Period_Date__c = eve.StartDateTime.day(); //Added by TCS HRM 5247
            userApptcount.Period_Month__c = eve.StartDateTime.month();
            userApptcount.Period_Year__c =  eve.StartDateTime.year();
            userApptcount.User__c = EventOwnerId;
            userApptcount.Additional_Appointments_Cancelled__c = 0;
            userApptcount.Quota_Appointments_Cancelled__c = 0;
            userApptcount.Additional_Appointments_Set__c = 0;
            userApptcount.Quota_Appointments_Set__c = 0;
            userApptcount.SchedUserTerr__c = schUsrObj.Id;
        }

        // Logic to calculate the No of user appointments.
        if( isInsert ){
            // §  IF the (Quote Appointment Set Count - Quota Appointments Cancelled) is less than the user’s Quota Count from the SchedUserTerr object, increment the Quota Appointments Set count
            Integer UserAppointmentQuotaDiff = userApptcount.Quota_Appointments_Set__c.intValue() - userApptcount.Quota_Appointments_Cancelled__c.intValue();
            if ( UserAppointmentQuotaDiff < schUsrObj.Quota__c) {
                userApptcount.Quota_Appointments_Set__c = userApptcount.Quota_Appointments_Set__c +1;
            }
            // §  ELSE increment the Additional Appointments Set Count
            else {
                userApptcount.Additional_Appointments_Set__c = userApptcount.Additional_Appointments_Set__c + 1;
            }
            userApptcount.Last_Appointment_Date_Time__c = eve.StartDateTime;
            userApptcount.Last_Appointment_Set_Date_Time__c = DateTime.now();
        }else if ( eve.Status__c == EventManager.STATUS_CANCELED && eve.StartDateTime > DateTime.now() ){
            // is cancel before
            // §  IF the (Quote Appointment Set Count - Quota Appointments Cancelled) is less than or *equal* to the user’s Quota Count from the SchedUserTerr object, increment the Quota Appointments Cancelled count
            Integer UserCancelAppointmentQuotaDiff = userApptcount.Quota_Appointments_Set__c.intValue() - userApptcount.Quota_Appointments_Cancelled__c.intValue();
            if ( UserCancelAppointmentQuotaDiff <= schUsrObj.Quota__c) {
                userApptcount.Quota_Appointments_Cancelled__c = userApptcount.Quota_Appointments_Cancelled__c +1;
            }
            //§  ELSE increment the Additional Appointments Cancelled count
            else {
                userApptcount.Quota_Appointments_Cancelled__c = userApptcount.Additional_Appointments_Cancelled__c + 1;
            }
        }
        upsert userApptcount;
    }   
}