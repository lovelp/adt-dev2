/**
 Description- This test class used for GetServicePowerApptService.
 Developer- TCS Developer
 Date- 1/31/2018
 */
@isTest
private class GetServicePowerApptServiceTest {

    public static Account a;
    
    private static void testSetUp(){
        
            List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
            rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
            rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
            rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
            rgvList.add(new ResaleGlobalVariables__c(name='BypassQuoteSecurityProfiles', value__c='System Administrator'));
    
            insert rgvList;
            
            IntegrationSettings__c integrationSetting= new IntegrationSettings__c(GetInstallOffersSPUsername__c='salesforcelb',
                                                                                  GetInstallOffersSPPassword__c='766gdjdg',
                                                                                  GetInstallOffersSPTimeout__c=60000,
                                                                                  GetInstallOffersSPEndPoint__c='https://test.api.adt.com/adtsp/getapptoffersinstall/v1'
                                                                                  );
            insert integrationSetting;
            
            InstallAppointmentsSPConfig__c installAppt= new InstallAppointmentsSPConfig__c(SetupOwnerId=UserInfo.getOrganizationId(),
                                                                                           Appointment_Type__c='P',
                                                                                           DaysOfTheWeek__c ='YYYYYYY',
                                                                                           Importance__c = 9,
                                                                                           No_Of_Days__c = 30,
                                                                                           No_Of_Offers__c = 10,
                                                                                           Prom_Set__c='RS');
            insert installAppt;
        
            List<InstallAppointmentsStatusColor__c > installColor= new List<InstallAppointmentsStatusColor__c >();
            installColor.add(new InstallAppointmentsStatusColor__c (name = 'Green',EndDate__c = 3,StartDate__c=0));
            installColor.add(new InstallAppointmentsStatusColor__c (name = 'Orange',EndDate__c = 10,StartDate__c=4));
            installColor.add(new InstallAppointmentsStatusColor__c (name = 'Red',EndDate__c = 99,StartDate__c=11));
            insert installColor;
            
            a = new Account();
            a= TestHelperClass.createAccountData();
            
            Account acc= [select id,SitePostalCode__c, MMB_TownID__c,PostalCodeID__c, PostalCodeID__r.MMB_TownID__c from Account where id=: a.id];
            
            Postal_Codes__c pc= [select id , MMB_TownID__c from Postal_Codes__c where id=: acc.PostalCodeID__c];
            pc.MMB_TownID__c= '782';
            update pc;
            system.debug('SitePostalCode__c**'+acc.SitePostalCode__c+'MMB_TownID__c**'+acc.MMB_TownID__c+'pc**'+pc);
            
            
    }
    static testMethod void testMethod1() {
    
        	testSetUp();
        
            test.startTest();
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('GetServicePowerApptCalloutMock');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/json');
         

            // Set the mock callout mode

            Test.setMock(HttpCalloutMock.class, mock);
        
            //GetServicePowerApptService obj= new GetServicePowerApptService();
            //GetServicePowerApptService.GetServicePowerApptOffersInstallResponse response= obj.getOffers((string)a.id);
            GetServicePowerApptController objController = new GetServicePowerApptController();
            objController.getInstallAppointmentDateInfo((string)a.id);
            //Test.setMock(HttpCalloutMock.class, new GetServicePowerApptServiceMockGenerator()); 
            
            test.stopTest();
            
    }
    
    static testMethod void testMethod2() {
    
        	testSetUp();
        
            test.startTest();
        	
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('GetServicePowerApptCalloutErrorMock');
            mock.setStatusCode(400);
            mock.setHeader('Content-Type', 'application/json');
         

            // Set the mock callout mode

            Test.setMock(HttpCalloutMock.class, mock);
            //GetServicePowerApptService obj= new GetServicePowerApptService();
            //GetServicePowerApptService.GetServicePowerApptOffersInstallResponse response= obj.getOffers((string)a.id);
            GetServicePowerApptController objController = new GetServicePowerApptController();
            objController.getInstallAppointmentDateInfo((string)a.id);
            //Test.setMock(HttpCalloutMock.class, new GetServicePowerApptServiceMockGenerator()); 
            
            test.stopTest();
            
    }
    
}