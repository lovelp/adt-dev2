@isTest
global class MockHttpResponseDialerlistapi implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        // Only set a mock response for specific endpoints DM and PROD
        HttpResponse res = new HttpResponse();
        if( req.getEndpoint() == 'https://dailerendPoint.com/test'){
            res.setHeader('Content-Type', 'application/xml');
            res.setBody('<?xml version="1.0" encoding="UTF-8"?>');            
            res.setStatusCode(200);
        }
        return res;
    }
}