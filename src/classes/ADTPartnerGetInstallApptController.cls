public with sharing class ADTPartnerGetInstallApptController 
{
    public integer statusCode;
    
    public class getInstallAppointmentRequest
    {
        public string partnerId;
        public string callId;
        public string opportunityId;
        public string partnerRepName;
        public string startDate;
    }
    
     public class getInstallAppointmentResponse 
    {
        //public string opportunityId;
        public string message;
        public integer startIndex;
        public string startDate;
        public string promSet;
        public string postalCode;
        public integer numberOfOffers;
        public integer importance;
        public string endDate;
        public string days;
        public integer branchNumber;
        public string appointmentType;
        public List<jobRequests> jobRequests;
    }
    
    public class jobRequests
    {
        public string jobType;
        public string jobRequest;
        public List<extraSkills> extraSkills;
        public integer extraDuration;
    }
    
    public class extraSkills
    {
        public string skillId;
    }
  
    public ADTPartnerGetInstallApptController(){}    
    
    public getInstallAppointmentResponse parseJSONRequest(string jsonRequest)
    {
        getInstallAppointmentResponse installAppointmentResponse = new getInstallAppointmentResponse();
        getInstallAppointmentRequest installApptRequestWrapper = new getInstallAppointmentRequest();        
        try
        {
            // if blank json request
            if (string.isBlank(jsonRequest))
            {
                statusCode = 400;
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('400').Error_Message__c);                                         
            }
            system.debug('@@jsonRequest: '+jsonRequest);
            installApptRequestWrapper = (getInstallAppointmentRequest) system.JSON.deserialize(jsonRequest, getInstallAppointmentRequest.class);
            if (string.isBlank(installApptRequestWrapper.opportunityId) || string.isBlank(installApptRequestWrapper.partnerId) || string.isBlank(installApptRequestWrapper.callId) || string.isBlank(installApptRequestWrapper.partnerRepName))
            {
                //invalid input, missing required elements
                statusCode = 400;
                system.debug('invalid input, missing required elements');
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('400').Error_Message__c); 
            }
            
            // if provided, validate the start date, else set with today's date
            if (string.isBlank(installApptRequestWrapper.startDate))
            {
                installApptRequestWrapper.startDate = String.ValueOf(Date.Today()).remove(' 00:00:00');
            }
            else
            {
                string dateErrMsg = validateStartDate (installApptRequestWrapper.startDate);
                if (string.isNotBlank(dateErrMsg))
                {
                    //invalid start date
                    statusCode = 400;
                    return createErrorResponse (installApptRequestWrapper, dateErrMsg);
                }
            }
            
            // check if the callId exists in system
            Call_Data__c callData = getCallData (installApptRequestWrapper.callId);
            if (callData == null)
            {
                statusCode = 404;
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('404').Error_Message__c.replace('requestID','Call Id'));                
            }
            
            // check if opportunity id exists in system
            opportunity opp = getOpportunity (installApptRequestWrapper.opportunityId);
            if (opp == null)
            {
                statusCode = 404;
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('404').Error_Message__c.replace('requestID','Opportunity Id'));                
            }
            
            string accountId = validateCallAndOpportunity (callData, opp);
            if (string.isBlank(accountId))
            {
                // Opportunity ID and Call Id does not return the same account
                statusCode = 400;
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('appointmentCallOppIDError').Error_Message__c);                
            }            
           
            // you have a valid accountId, get the account details
            account acc = getAccount(accountId);  
            if (acc == null)
            {
                // if account is not returned, something went wrong
                statusCode = 500;
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('500').Error_Message__c);                
            }
            
            // now you have a valid account, check for the mandatory account fields
            if (string.isBlank(acc.SitePostalCode__c))
            {
                // postal code is blank
                statusCode = 404;
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('BlankPostalCode').Error_Message__c); 
            }                
            if (string.isBlank(acc.MMB_TownID__c) || acc.MMB_TownID__c == '999')
            {
                // Town ID is blank or default 999, send error
                statusCode = 404;
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('BlankMMBTown').Error_Message__c);
            }
            if (string.isBlank(acc.Business_Id__c))
            {
                // Business id is blank
                statusCode = 404;
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('BlankBusinessId').Error_Message__c); 
            }
            
            // get the package configurations based on 
            // 1. active = true
            // 2. business id = account business id            
            List<Job_Request__mdt> lstAllJobRequestMDT = [Select DeveloperName,MasterLabel, Duration__c, Skill1__c, Skill2__c, PartnerId__c From Job_Request__mdt Where BusinessId__c =: acc.Business_Id__c and Active__c =: true];
            if (lstAllJobRequestMDT == null || lstAllJobRequestMDT.isEmpty() )
            {
                // No package configuration found
                statusCode = 404;
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('NoInstallPackage').Error_Message__c);
            }
            List<Job_Request__mdt> lstJobRequestMDT = new List<Job_Request__mdt>();
            // now check how many packages are configured for the requesting partnerId, and process only those
            for (Job_Request__mdt jr : lstAllJobRequestMDT)
            {
                if (string.isNotBlank(string.valueOf(jr.PartnerId__c)) )
                {
                    list<string> lstPartnerId = string.valueOf(jr.PartnerId__c).toLowerCase().split(',');
                    set<string> setPartnerId = new set<string> ();
                    for (string str : lstPartnerId)
                    {
                        setPartnerId.add(str.trim());
                    }
                    if (setPartnerId.contains(installApptRequestWrapper.partnerId.toLowerCase().trim()))
                    {
                        lstJobRequestMDT.add(jr);
                    }
                }                
            }
            system.debug('SELECT Package'+lstJobRequestMDT);
            if (lstJobRequestMDT == null || lstJobRequestMDT.isEmpty())
            {
                // No package configuration found for the requesting PartnerId
                statusCode = 404;
                return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('NoInstallPackage').Error_Message__c);
            }
            
            // initial validations passed, create the response 
            statusCode = 200;
            
            return createInstallApptConfigResponse(installApptRequestWrapper.opportunityId, acc, lstJobRequestMDT, installApptRequestWrapper.startDate);
        }
        catch(Exception ex)
        {
            // something went wrong
            system.debug('@@ Exception Occured: ' + ex.getLineNumber() + ex.getMessage());
            ADTApplicationMonitor.log(ex, 'ADTPartnerGetInstallApptController', 'parseJSONRequest', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            statusCode = 500;
            return createErrorResponse (installApptRequestWrapper, PartnerAPIMessaging__c.getinstance('500').Error_Message__c);                              
        }
    }
        
    private getInstallAppointmentResponse createErrorResponse (getInstallAppointmentRequest installApptRequestWrapper, string errMessage)
    {
        getInstallAppointmentResponse installAppointmentResponse = new getInstallAppointmentResponse();
        //installAppointmentResponse.opportunityId = (installApptRequestWrapper.opportunityId != '' && installApptRequestWrapper.opportunityId != null )? installApptRequestWrapper.opportunityId : '';
        installAppointmentResponse.message = errMessage;
        installAppointmentResponse.startIndex = 0;
        installAppointmentResponse.startDate = '';
        installAppointmentResponse.promSet = '';
        installAppointmentResponse.postalCode = '';
        installAppointmentResponse.numberOfOffers = 0;
        installAppointmentResponse.importance = 0;
        installAppointmentResponse.endDate = '';
        installAppointmentResponse.days = '';
        installAppointmentResponse.branchNumber = 0;
        installAppointmentResponse.appointmentType = '';
        installAppointmentResponse.jobRequests= new List<jobRequests> ();
        return installAppointmentResponse;
    }
    
    private string validateCallAndOpportunity (Call_Data__c callData, opportunity opp)
    {
        string accountId = '';
        if (callData.Account__c != null && callData.Account__c == opp.AccountId)
        {
            accountId = opp.AccountId;
        }
        return accountId;
    }
    
    private string validateStartDate (string startDate)
    {
        try
        {
            // validate date format
            Pattern pDate = Pattern.compile('^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$');
            Matcher pmDate = pDate.matcher(startDate);
            if(!(pmDate.matches()))
            {
                return PartnerAPIMessaging__c.getinstance('invalidStartDate').Error_Message__c;
            }
            else
            {
                date stDate = Date.valueOf(startDate);
                // if past date
                if (stDate < date.today())
                {
                    return PartnerAPIMessaging__c.getinstance('pastStartDate').Error_Message__c;
                }                
            }
        }
        catch (Exception ex)
        {
            return PartnerAPIMessaging__c.getinstance('invalidStartDate').Error_Message__c;
        }
        return '';
    }
    
    private Call_Data__c getCallData (string callId)
    {
        List<Call_Data__c> lstCallData = [SELECT Id,Account__c FROM Call_Data__c WHERE Id =: callId];
        if (lstCallData != null && !lstCallData.isEmpty())
        {
            return lstCallData[0];
        }        
        return null;
    }
    
    private Opportunity getOpportunity (string opportunityId)
    {
        List<Opportunity> lstOpportunity = [SELECT id, AccountId from Opportunity where id =:opportunityId];
        if (lstOpportunity != null && !lstOpportunity.isEmpty())
        {
            return lstOpportunity[0];
        }        
        return null;
    }
    
    private account getAccount(string accountId)
    {
        List<Account> lstAccount = new List<Account> ();            
        lstAccount = [Select Business_Id__c, SitePostalCode__c, MMB_TownID__c from Account Where Id =: accountId];            
        if (lstAccount != null && !lstAccount.isEmpty())
        {
            return lstAccount[0];
        }        
        return null;
    }
    
    // create the SP install Appt request as DP response. DP will take this response and request SP for the Install Appt offers
    private getInstallAppointmentResponse createInstallApptConfigResponse (string opportunityId, account acc, List<Job_Request__mdt> lstJobRequestMDT, string startDate)
    {
        getInstallAppointmentResponse installAppointmentResponse = new getInstallAppointmentResponse();
        // get the install appt custom setting
        InstallAppointmentsSPConfig__c installApptSetting = InstallAppointmentsSPConfig__c.getOrgDefaults();
        
        //installAppointmentResponse.opportunityId = opportunityId;
        installAppointmentResponse.startIndex = 1;
        installAppointmentResponse.startDate = startDate;
        installAppointmentResponse.promSet = installApptSetting.Prom_Set__c;
        installAppointmentResponse.postalCode = acc.SitePostalCode__c;
        installAppointmentResponse.numberOfOffers = (installApptSetting.No_Of_Offers__c != null)? Integer.ValueOf(installApptSetting.No_Of_Offers__c):1;
        installAppointmentResponse.importance = (installApptSetting.Importance__c != null)? Integer.ValueOf(installApptSetting.Importance__c):9;
        integer dateFrame = (installApptSetting.No_Of_Days__c != null)? Integer.ValueOf(installApptSetting.No_Of_Days__c) : 30;
        installAppointmentResponse.endDate = String.ValueOf(date.valueOf(startDate) + dateFrame);
        installAppointmentResponse.days = installApptSetting.DaysOfTheWeek__c;
        installAppointmentResponse.branchNumber = integer.valueOf(acc.MMB_TownID__c);
        installAppointmentResponse.appointmentType = installApptSetting.Appointment_Type__c;
        
        List<jobRequests> lstJobRequest = new List<jobRequests> ();
        for (Job_Request__mdt jobRequestMDT : lstJobRequestMDT)
        {
            jobRequests objJobReq = new jobRequests();
            objJobReq.jobRequest = jobRequestMDT.MasterLabel;
            objJobReq.jobType = jobRequestMDT.Skill1__c.substring(0,4) + jobRequestMDT.Skill2__c.substring(0,2);
            objJobReq.extraDuration = Integer.ValueOf(jobRequestMDT.Duration__c);
            
            List<extraSkills> lstExtraSkills = new List<extraSkills> ();
            extraSkills skill1 = new extraSkills ();
            skill1.skillId = jobRequestMDT.Skill1__c;
            lstExtraSkills.add(skill1);
            
            extraSkills skill2 = new extraSkills ();
            skill2.skillId = jobRequestMDT.Skill2__c;
            lstExtraSkills.add(skill2);
            
            extraSkills skill3 = new extraSkills ();
            skill3.skillId = 'Branch-'+ acc.MMB_TownID__c;
            lstExtraSkills.add(skill3);
            
            objJobReq.extraSkills = lstExtraSkills;     
            lstJobRequest.add(objJobReq);
        }
        installAppointmentResponse.jobRequests = lstJobRequest;
        installAppointmentResponse.message = PartnerAPIMessaging__c.getinstance('installSuccess').Error_Message__c;
        system.debug('Created Response ##'+installAppointmentResponse);
        return installAppointmentResponse;
    }    
    
}