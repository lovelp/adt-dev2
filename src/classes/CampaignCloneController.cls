public class CampaignCloneController {
    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
    // add the instance for the variables being passed by id on the url
    private Campaign camp {get;set;}
    // Used by the vf page to go back in case of error
    public Id campId {get;set;}
    // set the id of the record that is created -- ONLY USED BY THE TEST CLASS
    public ID newRecordId {get;set;}

    // initialize the controller
    public CampaignCloneController(ApexPages.StandardController controller) {
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        camp = (Campaign)controller.getRecord();
        if(camp != null)
            campId = camp.Id;
    }

    // method called from the VF's action attribute to clone the Campaign Selection Criteria along with the Campaign Selection Criteria Items
    public PageReference cloneWithItems() {
         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Campaign newCamp;

         try {
             Recordtype CampRT = new RecordType();
             CampRT = [Select Id from RecordType where DeveloperName = 'CampaignMaster' limit 1];
             camp = [select Id, Name, Type, Status, ParentId, Simulation__c, SOQL__c, Primary_soql__c, Selection_Logic__c , DNIS__c, Email_Notification_User__c, Override_Active_Campaigns__c, HoldForReview__c, ImmediateRun__c, RunScheduleTime__c, RunScheduleDay__c, OwnerId, CampaignMemberExpirationDays__c, Dialer_List_Output_Control__c from Campaign where id = :camp.id];
             newCamp = camp.clone(false);
             newCamp.Status = 'Planned';
             newCamp.RecordtypeId = campRT.id;
             newCamp.name = newCamp.name.length() < 10? newCamp.name + '1': newCamp.name.substring(0,9) + '1';
             insert newCamp;

             // set the id of the new po created for testing
             newRecordId = newCamp.id;

             // copy over the line items
             List<Campaign_Selection_Criteria_item__c> items = new List<Campaign_Selection_Criteria_item__c>();
             for (Campaign_Selection_Criteria_item__c cscItem : [Select Id, Campaign__c , Field__c, ObjectsList__c , Selection_Comparison__c, Selection_Value__c, Sequence__c From Campaign_Selection_Criteria_item__c where Campaign__c = :camp.id]) {
                 Campaign_Selection_Criteria_item__c newCSCItem = cscItem.clone(false);
                 newCSCItem.Campaign__c = newCamp.id;
                 items.add(newCSCItem);
             }
             insert items;
             
         }catch (Exception e){
            // roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
         }
         return new PageReference('/'+newCamp.id);
    }
}