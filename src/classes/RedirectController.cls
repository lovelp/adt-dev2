/************************************* MODIFICATION LOG ********************************************************************************************
* RedirectController
*
* DESCRIPTION : Perform various redirect actions (generally for overriding standard buttons)
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                                             DATE                                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                                  1/30/2012                               - Original Version
* Magdiel Herrera                               6/30/2014                               - Added controlling logic for case
* Siju                                          6/10/2019                                HRM-10066: - OFAC Restrict                                                            
*/

public with sharing class RedirectController {
        
        private ApexPages.Standardcontroller stdcon;
        
        public String retURL {get; set;}
        public String saveNewURL {get; set;}
        public String rt {get; set;}
        public String rtname {get;set;}
        public String cancelURL {get; set;}
        public String ent {get; set;}
        public String confirmationToken {get; set;}
        public String accountID {get; set;}
        public String contactID {get; set;}
        public String leadID {get; set;}
        public Case caseObj {get;set;}
        public Boolean renderCaseTypeSection {get;set;}
        public String caseType {get;set;}
        public String appointmentType {get;set;}
        public List<SelectOption> caseTypeList {
          get{
            List<SelectOption> resList = new List<SelectOption>();
            resList.add(new SelectOption('Problem', 'Problem')); 
            resList.add(new SelectOption('Feature Request', 'Feature Request')); 
            resList.add(new SelectOption('Question', 'Question')); 
            return resList; 
          }
        }
        
        private Boolean editMode = false;
        
        
        public RedirectController(ApexPages.StandardController stdController) 
        {
                stdcon = stdController;
                renderCaseTypeSection = false;
                retURL = ApexPages.currentPage().getParameters().get('retURL');
                rt = ApexPages.currentPage().getParameters().get('RecordType');
                rtname = ApexPages.currentPage().getParameters().get('RecordTypeName');
                cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
                ent = ApexPages.currentPage().getParameters().get('ent');
                confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
                saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
                accountID = ApexPages.currentPage().getParameters().get('def_account_id');
                contactID = ApexPages.currentPage().getParameters().get('def_contact_id');
                leadID = ApexPages.currentPage().getParameters().get('def_lead_id');
                appointmentType = ApexPages.currentPage().getParameters().get('appointment_type'); 
        }
        
    public PageReference redirectEvent(){
        PageReference redirectPage;
        //get event record
        Event e = (Event)stdcon.getRecord();
        //HRM code to block SGL appointments for restricted town code or postal code

        Boolean blockSGLAppnt=false;
        Boolean blockMultiAppnt = false;
        if(String.isNotBlank(leadID)){
            Lead ld = [SELECT Id,  PostalCodeID__r.Town_Lookup__r.BlockSGLAppointment__c, PostalCodeID__r.BlockSGLAppointment__c FROM Lead WHERE id=:leadID];
            if(ld.PostalCodeID__r.Town_Lookup__r.BlockSGLAppointment__c || ld.PostalCodeID__r.BlockSGLAppointment__c){
                blockSGLAppnt = true;
            }
        }
        else if(String.isNotBlank(accountID)){
            Account acc = [SELECT id, PostalCodeID__r.Town_Lookup__r.BlockSGLAppointment__c,PostalCodeID__r.BlockSGLAppointment__c,OFACRestriction__c FROM Account WHERE Id=:accountID];
            if(acc.PostalCodeID__r.BlockSGLAppointment__c || acc.PostalCodeID__r.Town_Lookup__r.BlockSGLAppointment__c){
                 blockSGLAppnt = true;
            }
            
             //HRM-10066: - Restrict appointment if account is OFAC -Siju
            if(acc.OFACRestriction__c && FlexFiConfig__c.getinstance('CFG Disabled') != null && String.isNotBlank(FlexFiConfig__c.getinstance('CFG Disabled').Value__c) && FlexFiConfig__c.getinstance('CFG Disabled').Value__c.equalsIgnoreCase('false')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Appointments are blocked for this Account as it is marked as OFAC.'));
                return null;
            }
            list<Event> eve = [SELECT Id FROM EVENT WHERE WhatID = :accountID AND StartDateTime >: System.now() AND Status__c = :EVENTMANAGER.STATUS_SCHEDULED];
            if(eve.size()>0){
                blockMultiAppnt = true;
            }
        }
        if(blockSGLAppnt){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'SGL Appointments are blocked for this town or postal code.'));
            return null;
        }
        
        if(blockMultiAppnt){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There is already an active appointment on the Account.'));
            return null;
        }
        
        rt = e.RecordTypeId;
        if (rt == null && rtname != null)
        {
            //get rt from rtname
        try
            {
            RecordType rectype = [select Id from RecordType where SObjectType = 'Event' and (Name = :rtname or DeveloperName = :rtname) limit 1];
            rt = rectype.Id;
            } catch (Exception ex) {}
        }
        //get event object prefix
        String prefix = Schema.Sobjecttype.Event.getKeyPrefix();
        //no record type set, assume it's a salesforce-only event
        if (rt == null){
            redirectPage = new PageReference('/' + prefix + '/e');
                redirectPage.getParameters().put('nooverride','1');
                //return newPage;
            }
        else{
            //otherwise we should grab the record type map and check
            Map<Id,String> recmap = Utilities.getRecordTypesForObject('Event');
            rtname = recmap.get(rt);
            if (rtname == null || rtname.equalsIgnoreCase(RecordTypeName.SALESFORCE_ONLY_EVENT)){
            //return standard layout page
            if (e.Id == null || editMode) //new event page
            {
                redirectPage = new PageReference('/' + prefix + '/e');
                redirectPage.getParameters().put('nooverride','1');
                if (accountID != null && accountID.length() > 0) {
                    redirectPage.getParameters().put('what_id', accountID);
                    } 
                else if (leadID != null) {
                    redirectPage.getParameters().put('who_id', leadID);
                    }
                }
                else //view page (this section probably needs to change to check if user clicked 'edit' link or 'view' link)
                {
                    redirectPage = stdcon.view();
                    redirectPage.getParameters().put('nooverride','1');
                    }
                    }
                else
                    {
                    //return custom VF page
                    redirectPage = Page.ManageEvents;
                    redirectPage.getParameters().put('id',e.Id);
                    redirectPage.getParameters().put('RecordType', rt);
                    if (e.Id == null)
                        {
                            redirectPage.getParameters().put('mode','edit');
                        }
                        if( !String.isBlank(appointmentType) && appointmentType == 'DLL' ){
                        //validate if this account doesn't have an upcoming activation appointment
                        if( [SELECT count() FROM Event WHERE StartDateTime >= :DateTime.now() AND Status__c <> :EventManager.STATUS_CANCELED AND WhatId = :accountID AND ShowAs <> :EventManager.SHOW_AS_FREE AND Quote_Id__c <> null AND Appointment_Type__c <> null ] > 0 ){
                            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'This account has an upcoming DLL appointment, please cancel or reschedule appointment.') );
                            return null;
                            }
                        // Validate access rights
                        try{
                            User contextUser = [SELECT ManagerId, UserRole.Name FROM User WHERE Id = :UserInfo.getUserId()];
                            Account a = [SELECT OwnerId, Owner.ManagerId, Owner.UserRole.Name FROM Account WHERE Id = :accountID];
                            // This user is not the owner of this account but he's a DLL Manager
                            if( a.OwnerId != contextUser.Id ){
                                if( contextUser.UserRole.Name.equalsIgnoreCase('DLL Manager') ){
                                  if( a.Owner.ManagerId != contextUser.Id ){
                                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not the manager to this user, please change ownership of this account to your team. ') );
                                    return null;
                                    }
                                }
                            else{
                                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'You do not have access to add DLL appointments to this account. ') );
                                return null;
                                }
                                }
                            }
                            catch(Exception err){
                            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Cannot confirm access rights on this account record. '+err.getMessage()) );
                                return null;
                            }
                            // Validate quotes on this account
                            try{
                                Boolean quoteQualify = false;
                                for( scpq__SciQuote__c quote : [SELECT id, scpq__Status__c, (SELECT id, DLL__c FROM Quote_Jobs__r) FROM scpq__SciQuote__c WHERE account__c= :accountID ]){
                                    for(Quote_Job__c QJ:quote.Quote_Jobs__r   ){
                                        if( !String.isBlank( quote.scpq__Status__c ) && NSCHelper.isDLL(quote, QJ.DLL__c) ){
                                            quoteQualify = true;
                                        }
                                    }
                                        }
                                        if( !quoteQualify ){
                                            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Cannot find DLL quote on this account. ') );
                                            return null;
                                        }
                                    }
                                    catch(Exception err){
                                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Cannot find DLL quote on this account. '+err.getMessage()) );
                                        return null;
                                    }
                                    
                                    
                                    redirectPage.getParameters().put('appointment_type','DLL');
                                }
                        }
                }
                
                if (redirectPage != null)
                {
                        //put the initial parameters back
                        redirectPage.getParameters().put('retURL', retURL);
                        redirectPage.getParameters().put('RecordType', rt);
                        redirectPage.getParameters().put('cancelURL', cancelURL);
                        redirectPage.getParameters().put('ent', ent);
                        redirectPage.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
                        redirectPage.getParameters().put('save_new_url', saveNewURL);
                        redirectPage.getParameters().put('nooverride', '1');
                        redirectPage.getParameters().put('def_account_id', accountID);
                        redirectPage.getParameters().put('def_contact_id', contactID);
                        redirectPage.getParameters().put('def_lead_id', leadID);
                        
                        if (e.Id != null)
                                redirectPage.getParameters().put('id',e.Id);
                        
                        redirectPage.setRedirect(true);
                        return redirectPage;
                }
                
                ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.ERROR, 'No redirect page for event type: ' + rtname);
                ApexPages.addMessage(m);
                
                return null;
        }
        
        public PageReference redirectCase(){
            System.Debug('redirect case====');
                PageReference redirectPage;
                try{
                  
                  RecordType callidusRT = [SELECT Id, Name FROM RecordType WHERE Name = 'Callidus' limit 1];
                  RecordType hermesRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' and DeveloperName = 'Quoting' limit 1];
                  // Check standard controller for record type
                  if( Utilities.isEmptyOrNull(rt) ){
                    //get case record
                    Case c = (Case)stdcon.getRecord();                    
                    rt = c.RecordTypeId;
                  }
                  // check if no record type then default to callidus
                  if( Utilities.isEmptyOrNull(rt) ){
                    rt = callidusRT.Id;
                  }
                  
                  // Check for the RecordType of the case
                  if( ((String)callidusRT.Id).startsWith(rt) ){
                    
                    //get case object prefix
                    //String prefix = Schema.Sobjecttype.Case.getKeyPrefix();                    
                        //redirectPage = new PageReference('/' + prefix + '/e');
                        redirectPage = new PageReference('/apex/CallidusCaseNew');
                    
                        //put the initial parameters back
                        redirectPage.getParameters().put('retURL', retURL);
                        redirectPage.getParameters().put('RecordType', rt);
                        redirectPage.getParameters().put('cancelURL', cancelURL);
                        redirectPage.getParameters().put('ent', ent);
                        redirectPage.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
                        redirectPage.getParameters().put('save_new_url', saveNewURL);
                        redirectPage.getParameters().put('nooverride', '1');
                        redirectPage.getParameters().put('def_account_id', accountID);
                        redirectPage.getParameters().put('def_contact_id', contactID);
                        redirectPage.getParameters().put('def_lead_id', leadID);
                        return redirectPage;
                  }else if(!((String)hermesRT.Id).startsWith(rt)){
                    //get case object prefix
                    String prefix = Schema.Sobjecttype.Case.getKeyPrefix();                    
                        redirectPage = new PageReference('/' + prefix + '/e');

                        //put the initial parameters back
                        redirectPage.getParameters().put('retURL', retURL);
                        redirectPage.getParameters().put('def_account_id', accountID);
                        redirectPage.getParameters().put('RecordType', rt);                        
                        redirectPage.getParameters().put('ent', ent);                        
                        redirectPage.getParameters().put('def_contact_id', contactID);
                        redirectPage.getParameters().put('def_lead_id', leadID);
                        redirectPage.getParameters().put('nooverride', '1');                        
                        return redirectPage;
                  }
                  
                  
                  
                  // Check if the user is a Hermes user
                  Boolean isHermesUser = false;
                  caseObj = new Case( Status = 'New', Origin = 'Sales Rep', AccountId = accountID, Subject ='Question',Reason='New problem');
                  
                  //caseObj.Assignee__c = RoleUtils.getNextAvailableAsignee(userInfo.getUserId());
                  
                  try{
                    PermissionSetAssignment  psaObj = [SELECT PermissionSet.Name, PermissionSetId, Assignee.Id, AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :userInfo.getUserId() AND PermissionSet.Name = 'Hermes'];
                    
                    caseType = 'Hermes';
                    caseObj.Type = 'Hermes';
                    caseObj.RecordTypeId = hermesRT.Id;
                    isHermesUser = true;
                    renderCaseTypeSection = true;
                  }
                  catch(Exception err){}
                  
                  // If not a hermes user then the default type is just Question
                  if( !isHermesUser ){
                     caseType = 'Question';
                     caseObj.Type = 'Question';
                  }
                  
                  // Let the user decide what type it is for a hermes case                  
                  //return CreateRedirectCase(caseObj);
                }
                catch(Exception err){
                  ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Unexpected error while redirecting to case. \n'+err.getMessage()+'\n'+err.getStackTraceString()) );
                }
                
                return null;
        }
        
        public PageReference cancelCase(){
            if( !Utilities.isEmptyOrNull(accountID) )
              return new PageReference('/'+accountID);
            else
              return new PageReference('/home/home.jsp');  
        }
        
        public PageReference saveCase(){
            return CreateRedirectCase(caseObj);
        }
        
        public PageReference CreateRedirectCase(Case c){
            System.Debug('case'+c);
            Savepoint sp = Database.setSavepoint();
            
            try{
                  c.Type = caseType;
              insert c;
          
          /* Do not create an approval for a Hermes case
                // create the new approval request to submit
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setComments('Commisssion error initially submitted for review.');
                req.setObjectId(c.Id);
                // submit the approval request for processing
                Approval.ProcessResult result = Approval.process(req);
                // display if the reqeust was successful
                */

              return new PageReference('/'+c.Id+'/e?retURL=%2F'+c.Id);
            }
            catch(Exception err){
              // Make both the creating of the case and approval process transactional
              Database.rollback(sp);
                  ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to redirect page for case type: ' + c.Type + '\n'+err.getMessage()+'\n'+err.getStackTraceString())  );
            }
            
            return null;
        }
        
        public PageReference redirectEventEdit() {
                editMode = true;
                return redirectEvent();
        }
        
        public PageReference eventDeleteError() {
                return new PageReference('/apex/ShowError?Error=SG_EVENT_DEL');
        }
}