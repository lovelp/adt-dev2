/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : DOBinformationAction.cls has an invocable method that is called from process builder
* Due to the webservice callout exception which will happen if a webservice call is made after a DML,
* @future method is used.  
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Mounika Anna                  07/21/2017          - Origininal Version
* Siddarth Asokan               07/22/2017          - Added XSD format for DOB
*
*/
public class DOBinformationAction {
    @InvocableMethod(label='DobInfo' description='returns the values of MMBJob#,AccountName')
    public static void getQuoteinfo(list<Id> ids) {
        if(ids.size()>0)
            callMMBDob(ids[0]);
    }
    
    @future(Callout=true)
    public static void callMMBDob(Id quoteId){
        List<scpq__SciQuote__c> scquoteLst = [SELECT Account__r.Name, Account__r.DOB_encrypted__c, MMBJobNumber__c from scpq__SciQuote__c where id =: quoteId AND MMBJobNumber__c != null AND Account__r.Name != null AND (Account__r.Business_Id__c = '1100 - Residential' OR Account__r.Business_Id__c = '1100') AND Account__r.MMBOrderType__c != 'A1'];
        if(scquoteLst.size() > 0){
            for(scpq__SciQuote__c q: scquoteLst){
                if(q.Account__r.DOB_encrypted__c != null){
                    String[] dobEnclist = q.Account__r.DOB_encrypted__c.split('/') ;
                    
                    if(q.Account__r.DOB_encrypted__c.contains('/') && dobEnclist.size() == 3){
                        Integer dobMonth = Integer.valueOf(dobEnclist[0]);
                        Integer dobDay = Integer.valueOf(dobEnclist[1]);
                        Integer dobYear = Integer.valueOf(dobEnclist[2]);
        
                        date dobInDate = date.newInstance(dobYear,dobMonth,dobDay);
                        String dobXSD = String.valueOf(dobInDate);
                        dobXSD = dobXSD.replace(' 00:00:00', '');
                        
                        // Make consecutive requests for multiple MMBJobNumber__c
                        for(String jobNo: q.MMBJobNumber__c.split(',')){
                            // Set request params
                            MMBCustomerSiteSearchApi.ContractDOBMMBRequest contractDOBReq = new MMBCustomerSiteSearchApi.ContractDOBMMBRequest();
                            contractDOBReq.DOB                  = dobXSD;
                            contractDOBReq.ContractOwnerName    = q.Account__r.Name;
                            contractDOBReq.jobNo                = jobNo;
                            
                            MMBCustomerSiteSearchApi.contractDOBResponse contractDOBRes = new MMBCustomerSiteSearchApi.contractDOBResponse();
                            try{
                                contractDOBRes = MMBCustomerSiteSearchApi.setContractDOBMMB(contractDOBReq);
                            }
                            catch(exception ex){
                                // If timeout exception retry
                                contractDOBRes = MMBCustomerSiteSearchApi.setContractDOBMMB(contractDOBReq);
                            }
                        }
                    }
                }
            }
        }
    }
}