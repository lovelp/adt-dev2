@RestResource(urlmapping='/accountUpdate/*')
global class ADTPartnerAccountUpdateService 
{
	@HTTPPost
    global static void doPost ()
    {   
        datetime qryStart = datetime.now();
        system.debug('@@doPatch called');
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        system.debug('incoing req'+request.requestBody.toString());
        ADTPartnerAccountUpdateController accUpdateCntrl = new ADTPartnerAccountUpdateController ();
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(accUpdateCntrl.parseDPRequest (request.requestBody.toString())));
        response.statusCode = accUpdateCntrl.statusCode; 
        datetime qryEnd = datetime.now();
        system.debug('=========>'+(qryEnd.getTime() - qryStart.getTime()) /1000);
    }
}