/************************************* MODIFICATION LOG ********************************************************************************************
* IntegrationException
*
* DESCRIPTION : An exception class specifically for use in integrations
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner				 3/5/2012			- Original Version
*
*													
*/

public with sharing class IntegrationException extends Exception {

}