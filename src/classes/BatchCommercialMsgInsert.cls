global class BatchCommercialMsgInsert implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        String[] staticResources = System.Label.SandboxRefreshObjects.split(',');
        String query = 'SELECT Id, Name, Body FROM StaticResource WHERE Name IN :staticResources';
        //return results to process only if the current instance is a sandbox or a test is running
        Boolean sandbox;
        for(Organization o : [Select isSandbox from Organization limit 1]){
            sandbox = o.isSandbox;
        }
        system.debug('$$$' + query);
        if (Test.isRunningTest()){
        	query = 'SELECT Id, Name, Body FROM StaticResource WHERE Name = \'Affiliate\'';
        	return Database.getQueryLocator(query + ' limit 1');      
        } else if(sandbox == TRUE){
            //query = 'SELECT Id, Name, Body FROM StaticResource WHERE Name = \'Town\'';
        	return Database.getQueryLocator(query);
        } else {
            return Database.getQueryLocator(query + ' limit 0');
        }
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        list<StaticResource> listGroup = (list<StaticResource>)scope;
        list<SandboxRefreshSetting__mdt> fieldMappings = [SELECT MasterLabel, Source_Field__c, Field_Type__c, Target_Field__c, Target_Object__c, Field_Position__c   
                                              				FROM SandboxRefreshSetting__mdt 
                                                          	WHERE MasterLabel = :listGroup.get(0).Name];
        map<String, SandboxRefreshSetting__mdt> fieldMappingMap = new map<String, SandboxRefreshSetting__mdt>();
        String objectName = '';
        for (SandboxRefreshSetting__mdt fieldMapping : fieldMappings) {
            fieldMappingMap.put(fieldMapping.Source_Field__c.toLowercase(), fieldMapping);
            objectName = fieldMapping.Target_Object__c;
        }
        //fieldMappings = null;
        list<sObject> listToUpdate = new list<sObject>();
        if(!fieldMappingMap.isEmpty()){
        	//system.debug('$$' + fieldMappingMap);
            for(StaticResource g : listGroup){
                String body = g.Body.toString();
                //system.debug('$%$ - ' + body);
                //String[] csvFileLines = body.split('\n');
                String[] csvFileLines = safeSplit(body, '\n');
                for(Integer i = 0; i < csvFileLines.size(); i++){
                    csvFileLines[i] = escapeCsvLine(csvFileLines[i]);
                    String[] csvRecordData = resumeQuotes(csvFileLines[i].split(','));
                    //String[] csvRecordData = csvFileLines[i].trim().split(',');
                    system.debug('$%$ - ' + csvFileLines[i]);
                    system.debug('$%$ - ' + csvRecordData);
                    sObject sObj = Schema.getGlobalDescribe().get(objectName).newSObject();
                    
                    if(i != 0){
	                   for (SandboxRefreshSetting__mdt fieldMapping : fieldMappingMap.values()) {
				            Integer count = (Integer)fieldMapping.Field_Position__c;
				            system.debug('$$$' + count);
                           	system.debug('$$$' + csvRecordData[count]);
                           	system.debug('$$$' + fieldMapping.Target_Field__c.toLowercase());
				            Object obj = convertStringToObject(csvRecordData[count], fieldMapping.Field_Type__c);
				            sObj.put(fieldMapping.Target_Field__c.toLowercase(), obj);
				       }
	                   listToUpdate.add(sObj);
	               	} else{
	                   //header fetch
	                   for(Integer j = 0; j < csvRecordData.size(); j++){
	                       if(fieldMappingMap.containsKey(csvRecordData[j])){
	                           fieldMappingMap.get(csvRecordData[j].toLowercase()).Field_Position__c = j;
	                           
	                       }
	                   }
	               	}
                    
                }
            }
        }
        /*CommercialMessages__c comm = new CommercialMessages__c();
        comm.Name = csvRecordData.get(0);
        comm.Message__c = csvRecordData.get(1);*/
        if(!listToUpdate.isEmpty()){
            //Update all Groups that can be updated
            database.insert(listToUpdate, false);
        }
    }

    global void finish(Database.BatchableContext BC){
    	system.debug('***** BatchCommercialMsgInsert is complete.');    
    }
    public Object convertStringToObject(String val, String dataType){
        if(val == null || val == '' || val == '#N/A') return null;
        //system.debug('------'+val);
        if(dataType == 'String') return val;
        if(dataType == 'Decimal') return Decimal.valueOf(val).setScale(2);
        if(dataType == 'Boolean') return Boolean.valueOf(val);
        if(dataType == 'Date') return Date.valueOf(val);
        if(dataType == 'Percent') return Decimal.valueOf(val).setScale(4)*100;
        if(dataType == 'Email') {
            return val == '0' ? null : val;
        }
        if(dataType == 'DateTime') return DateTime.valueOf(val);
        return val;
    }
	public List<String> safeSplit(String inStr, String delim){
        Integer regexFindLimit = 100;
        Integer regexFindCount = 0;
        list<String> output = new list<String>();
        Matcher m = Pattern.compile(delim).matcher(inStr);
        Integer lastEnd = 0;
        while(!m.hitEnd()) {
            while(regexFindCount < regexFindLimit && !m.hitEnd()) {
                if(m.find()) {
                    output.add(inStr.substring(lastEnd, m.start()));  
                    lastEnd = m.end();
                } else {
                    output.add(inStr.substring(lastEnd));
                    lastEnd = inStr.length();
                }
                regexFindCount++;
            }
            // Note: Using region() to advance instead of substring() saves 
            // drastically on heap size. Nonetheless, we still must reset the 
            // (unmodified) input sequence to avoid a 'Regex too complicated' 
            // error.
            m.reset(inStr);        
            m.region(lastEnd, m.regionEnd());
            regexFindCount = 0;
        }
        return output;
	}
    public String escapeCsvLine(String csvLine){
        Integer startIndex;
        Integer endIndex;
        String prevLine = csvLine;
        while(csvLine.indexOf('"') > -1){
            if(startIndex == null){
                startIndex = csvLine.indexOf('"');
                csvLine = csvLine.substring(0, startIndex) + ':q:' + csvLine.substring(startIndex+1, csvLine.length());
            }else{
                if(endIndex == null){
                    endIndex = csvLine.indexOf('"');
                    csvLine = csvLine.substring(0, endIndex) + ':q:' + csvLine.substring(endIndex+1, csvLine.length());
                }
            }
            if(startIndex != null && endIndex != null){
                String sub = csvLine.substring(startIndex, endIndex);
                sub = sub.replaceAll(',', ':c:');
                csvLine = csvLine.substring(0, startIndex) + sub + csvLine.substring(endIndex, csvLine.length());
                startIndex = null;
                endIndex = null;
            }
        }
        //System.debug('prevLine:::'+prevLine);
        //System.debug('csvLine:::'+csvLine);
        return csvLine;
    }
    public String[] resumeQuotes(String[] csvLine){
        list<String> columns = new list<String>();
        for(String column : csvLine){
          columns.add(column.replaceAll(':q:', '').replaceAll(':c:', ','));
        }
        return columns;
    }
}