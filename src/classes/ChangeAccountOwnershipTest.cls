@isTest
private class ChangeAccountOwnershipTest {

    static testMethod void accountOwnershipWithZipAndUserTest() {
        User salesRep = TestHelperClass.createSalesRepUserWithManager();
        UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
        User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId and isactive=true Limit 1];
        User adminUser = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        Postal_Codes__c newPC1;
        System.runas(adminUser)
        {
            TestHelperClass.createReferenceDataForTestClasses();
            //create postal code.
            newPC1 = new Postal_Codes__c ( name = '221o2', businessId__c = '1100', TownId__c = '12345', Town__c = 'Unit Test Town 1', AreaId__c = '123', Area__c = 'Unit Test Area');
            insert newPC1;  
        }
        system.runas(managerUser)
        {
            Account a1 = TestHelperClass.createAccountData();
            Map<Id, Id> testMap = new Map<id, id>();
            testMap.put(newPC1.id, salesRep.id);
            Test.startTest();
            ChangeAccountOwnership cao = new ChangeAccountOwnership();
            cao.ChangeAccountOwnershipWithZipAndUser(testMap, null);
            Test.stopTest();
        }
        List<PostalCodeReassignmentQueue__c> PCRQ = [Select id from PostalCodeReassignmentQueue__c where PostalCodeId__c = : newPC1.id and PostalCodeNewOnwer__c = : salesRep.id];
        system.assertEquals(PCRQ.size(), 1);
    }  
    
    static testMethod void accountOwnershipWithListOfLeadsTest()
    {
        User salesRep = TestHelperClass.createSalesRepUserWithManager();
        UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
        User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId and isActive=true Limit 1];
        User adminUser = TestHelperClass.createAdminUser();
        TestHelperClass.createReferenceUserDataForTestClasses();
        TestHelperClass.createRehashOwnerForTestClasses();
        Postal_Codes__c newPC1;
        Lead l1;
        Lead l2;
        System.runas(adminUser)
        {
            TestHelperClass.createReferenceDataForTestClasses();
            ResaleGlobalVariables__c rgv =[Select id,Name,Value__c from ResaleGlobalVariables__c where Name=:'NewRehashHOARecordOwner'];
            rgv.Value__c='unitTestrehashRep1@testorg.com';
            update rgv;
            //create postal code.
            newPC1 = new Postal_Codes__c ( name = '22102', businessId__c = '1100', TownId__c = '12345', Town__c = 'Unit Test Town 1', AreaId__c = '123', Area__c = 'Unit Test Area');
            insert newPC1;
            ManagerTown__c MT = TestHelperClass.createManagerTown(ManagerUser, newPC1, Channels.TYPE_RESIDIRECT + ';' + Channels.TYPE_RESIRESALE);
            //Create a territory
            LIST<Territory__c> lstTerr=new LIST<Territory__c>();
            Territory__c terr = new Territory__c(name='Resi - Test', TerritoryType__c = 'Resi Direct Sales', /*BusinessId__c = '1100',*/ ownerid = salesRep.id);
            lstTerr.add(terr);
            Territory__c terrbld = new Territory__c(name='Builder - Test', TerritoryType__c = 'BUilder', /*BusinessId__c = '1100',*/ ownerid = salesRep.id);
            lstTerr.add(terrbld);
            
            database.Saveresult[] sr=database.insert(lstTerr,false);
            
            LIST<TerritoryAssignment__c>lsTA=new LIST<TerritoryAssignment__c>();
            TerritoryAssignment__c ta = new TerritoryAssignment__c(PostalCodeID__c = newPC1.id, TerritoryID__c = sr[0].getID(), TerritoryType__c = 'Resi Direct Sales', ownerid = salesRep.id);
            lsTA.add(ta);
            TerritoryAssignment__c tabld = new TerritoryAssignment__c(PostalCodeID__c = newPC1.id, TerritoryID__c = sr[1].getID(), TerritoryType__c = 'Builder', ownerid = salesRep.id);
            lsTA.add(tabld);
            insert lsTA;
        }    
        system.runas(managerUser)
        {
            LIST<Lead> lstLd=new LIST<Lead>();
            l1 = TestHelperClass.createLead(managerUser, false, 'BUDCO');
            l1.NewMoverType__c = 'RC';
            l1.Business_Id__c = '1100 - Residential';
            //l1.Affiliation__c == IntegrationConstants.AFFILIATION_USAA;
            lstLd.add(l1);
            l2 = TestHelperClass.createLead(managerUser, false, 'BUDCO');
            l2.LeadSource='Manual Import';
            l2.Business_Id__c = '1100 - Residential';
            l2.Affiliation__c=Channels.TERRITORYTYPE_BUILDER;
            lstLd.add(l2);
            
            
            database.Saveresult[] sr=database.insert(lstLd);
            List<Lead> l = new List<Lead>();
            l.add(l1);
            Test.startTest();
            //ChangeAccountOwnership.ChangeAccountOwnershipWithListOfLeads(l);
            ChangeAccountOwnership.ChangeAccountOwnershipWithListOfLeads(lstLd);
            Test.stopTest();
            //system.assertEquals(l1.ownerid, Utilities.getGlobalUnassignedUser()); 
        }       
    }    
}