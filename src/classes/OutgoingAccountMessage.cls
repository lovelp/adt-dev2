/************************************* MODIFICATION LOG ********************************************************************************************
* OutgoingAccountMessage
*
* DESCRIPTION : Creates and processes setAccount messages for Telemar Interface
*               Extends OutgoingMessage with message specific logic
*               Delegates to ADTSalesMobilityFacade (wsdl2apex code)
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  1/19/2012               - Original Version
*
*                                                   
*/

public with sharing class OutgoingAccountMessage extends OutgoingMessage {
    
    public Account a {get;set;}
    public Disposition__c d {get;set;}
    
    private ADTSalesMobilityFacade.setAccountRequest_element request;
    private ADTSalesMobilityFacade.setAccountResponse_element response;
    private ADTSalesMobilityFacade.Account_element acct;
    private ADTSalesMobilityFacade.AccountDSC_element acctDSC;
    private ADTSalesMobilityFacade.Disposition_element disposition;
    
    public OutgoingAccountMessage()
    {
        this.ServiceName = 'setAccount';
    }
    
    public override String createRequest()
    {
        request = new ADTSalesMobilityFacade.setAccountRequest_element();
        Id uid;
        if (a != null && a.OwnerId != null) uid = a.OwnerId;
        else uid = UserInfo.getUserId();
        
        User u = [Select Id, EmployeeNumber__c from User where ID = :uid];
        
        request.MessageID = getMessageId(uid);
        request.TransactionType = this.TransactionType;
        // intentionally using TelemarAccountNumber__c rather than the formuala field OutboundTelemarAccountNumber__c
        // since any SFDC Accounts sync'd with Telemar will have TelemarAccountNumber__c populated
        request.TelemarAccountNumber = (a.TelemarAccountNumber__c == null ? '' : a.TelemarAccountNumber__c);
        request.HREmployeeID = (u.EmployeeNumber__c == null ? '' : u.EmployeeNumber__c);
        if (TransactionType == IntegrationConstants.TRANSACTION_ACCOUNT || (!Utilities.isEmptyOrNull(TransactionType) && TransactionType.endsWith('-DSC')))
        {
            acct = new ADTSalesMobilityFacade.Account_element();
            acct.FirstName = truncateFirstName(a.FirstName__c);
            acct.LastName = truncateLastName(a.LastName__c);

            if (a != null && a.Business_Id__c != null && a.Business_Id__c.contains('1200')) {
                acct.BusinessName = truncateBusinessName(a.Name);
            } else {
                acct.BusinessName = null;
            }
            acct.PhoneNumber1 = truncatePhone(a.Phone);
            acct.PhoneNumber2 = truncatePhone(a.PhoneNumber2__c);
            acct.PhoneNumber3 = truncatePhone(a.PhoneNumber3__c);
            acct.PhoneNumber4 = truncatePhone(a.PhoneNumber4__c);
            acct.Email = a.Email__c;
            if(a.Channel__c == Channels.TYPE_CUSTOMHOMESALES && a.MMBOrderType__c == 'R1' ){
                acct.Comments=Channels.TYPE_RESIRESALE;
                acct.Comments=acct.comments.rightPad(30);
                acct.Comments=acct.comments+'CHS';

            }
            else
                acct.Comments = a.Channel__c;
            acct.ReferredBy = TextUtilities.removeDiacriticalMarks(a.ReferredBy__c);
            
            if( TransactionType.endsWith('-DSC') ){
                acctDSC = new ADTSalesMobilityFacade.AccountDSC_element();
                acctDSC.Location = 'SF';
                
                if((a.MMBBillingSystem__c =='Informix' || a.MMBBillingSystem__c =='INF'))
                    if(a.chargeBack__c.trim()=='Y' && a.InstallingDealer__c!=null)
                        acctDSC.SourceSystem=a.InstallingDealer__c;
                    else
                        acctDSC.SourceSystem='INF';
                else
                    acctDSC.SourceSystem='MMB';
                //acctDSC.SourceSystem = 'SF';
                /*if((a.MMBBillingSystem__c =='Informix' || a.MMBBillingSystem__c =='INF') && a.MMB_Relo_Customer__c && !String.isBlank(a.MMB_Relo_Customer_Number__c))
                    acctDSC.CustomerNumber = a.MMB_Relo_Customer_Number__c;
                else
                    acctDSC.CustomerNumber = a.MMBCustomerNumber__c;
                */
                if((a.MMBBillingSystem__c =='Informix' || a.MMBBillingSystem__c =='INF')){
                    if(a.MMB_Relo_Customer__c && !String.isBlank(a.MMB_Relo_Customer_Number__c))
                        acctDSC.CustomerNumber = a.MMB_Relo_Customer_Number__c;
                    else if (!String.isBlank(a.PreviousBillingCustomerNumber__c))
                        acctDSC.CustomerNumber = a.PreviousBillingCustomerNumber__c;
                }
                else
                    acctDSC.CustomerNumber = a.MMBCustomerNumber__c;
                acctDSC.Pic = 'Y';
                acctDSC.ReasonCode = 'CMV';
                acctDSC.DiscLevel = '1';
                acctDSC.SiteNumber = a.MMB_Relo_Site_Number__c;
                acctDSC.CTC = '000';
                if( a.MMBDisconnectDate__c != null ){
                    Time t = Time.newInstance(0, 0, 0, 0);
                    DateTime dt = DateTime.newInstance(a.MMBDisconnectDate__c, t);
                    acctDSC.EffectiveDate = dt.format('yyyy-MM-dd');
                }
                acctDSC.ExistingDISCO = 'N';
                acctDSC.InfoOnly = 'N';
            }
        }
        else
        {
            acct = null;
        }
        
        request.Account = acct;
        request.AccountDSC = acctDSC;
        
        disposition = null;
        if (TransactionType == IntegrationConstants.TRANSACTION_DISPOSITION_ONLY) {
            
            if (d != null)
            {
                disposition = new ADTSalesMobilityFacade.Disposition_element();
                disposition.DispositionCodeDesc = d.DispositionType__c;
                disposition.DispositionDateTime = d.DispositionDate__c;
                disposition.DispositionComment = '';
                if(!Utilities.isEmptyOrNull(d.DispositionDetail__c)) disposition.DispositionComment = truncateTextArea(d.DispositionDetail__c, 600);
                disposition.DispositionComment += truncateTextArea(d.Comments__c, 600);
                disposition.DispositionOffsetToGMT = System.now().format('Z');
            }
        }   
        
        request.Disposition = disposition;
        
        return system.Json.serialize(request);
    }
    
    public override void processResponse()
    {
        if (request == null && this.ServiceMessage != null) {
            request = (ADTSalesMobilityFacade.setAccountRequest_element)system.Json.deserialize(this.ServiceMessage, system.type.forName('ADTSalesMobilityFacade.setAccountRequest_element'));
        }
        else if (request == null) {
            createRequest();
        }
            
        ADTSalesMobilityFacade.SalesMobilitySOAP soap = new ADTSalesMobilityFacade.SalesMobilitySOAP();
        response = soap.setAccount(
            request.MessageID, 
            request.TransactionType, 
            request.TelemarAccountNumber, 
            request.HREmployeeID, 
            request.Account, 
            request.AccountDSC,
            request.Disposition
        );
        
        mid = response.MessageID;
        mstatus = response.MessageStatus;
        err = response.Error;
        
        if (mstatus != IntegrationConstants.MESSAGE_STATUS_OK && mstatus != IntegrationConstants.MESSAGE_STATUS_FAIL) {
            throw new IntegrationException('Invalid response from Telemar: MessageStatus of ' + mstatus + ' is invalid');
        }

        if (mstatus == IntegrationConstants.MESSAGE_STATUS_FAIL) {
            throw new IntegrationException (err);
        }
    }

}