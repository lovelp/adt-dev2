public class LC_MelissaDataTemplateController {
    
    public class Addresswrapper{
        @AuraEnabled public String title;
        @AuraEnabled public String street1;
        @AuraEnabled public String street2;
        @AuraEnabled public String city;
        @AuraEnabled public String state;
        @AuraEnabled public String postalcode;
        @AuraEnabled public String country;
    }
    
    public class MelissaResponseWrap{
        @AuraEnabled public String status;
        @AuraEnabled public String addressId;
        @AuraEnabled public String message;
        @AuraEnabled public Addresswrapper addrMelissa;
    }
    
    @AuraEnabled
    public static Address__c getAddressDetails(String recId, String addrsType){
        list<Opportunity> optylist = [SELECT Id, BillingAddress__c, ShippingAddress__c FROM Opportunity WHERE Id=:recId];
        String addrsId;
        if(addrsType == 'Shipping'){
            addrsId = optylist[0].ShippingAddress__c;
        }else if(addrsType == 'Billing'){
            addrsId = optylist[0].BillingAddress__c;
        }
        if(String.isNotBlank(addrsId)){
            return [SELECT Id,OriginalAddress__c,Validated__c ,Street__c,Street2__c,StreetNumber__c,StreetName__c,CHS__c,
                City__c,County__c,State__c,PostalCode__c,postalcodeAddOn__c,CountryCode__c,Latitude__c,Longitude__c FROM Address__c WHERE Id =:addrsId]; 
        }else{
            return null;
        }
          
    } 
   
    @AuraEnabled
    public Static MelissaResponseWrap addressValidation(String street1, String street2, String city, String state, String postalcode, Boolean overRideTrue){
        MelissaResponseWrap mrw = new MelissaResponseWrap();
        Addresswrapper adrmel = new Addresswrapper();
        try{            
            MelissaDataAPI.MelissaDataResults melissaResponse = new MelissaDataAPI.MelissaDataResults();
            String uniqueOriginalAddress = street1+'+';
            uniqueOriginalAddress += String.isNotBlank(street2)?street2+'+'+city+'+'+state+'+'+postalcode :city+'+'+state+'+'+postalcode;
            System.debug('uniqueOriginalAddress--'+uniqueOriginalAddress);
            List<Address__c> addrResponseList = [SELECT Id,OriginalAddress__c,Validated__c ,Street__c,Street2__c,StreetNumber__c,StreetName__c,CHS__c,
                                                 City__c,County__c,State__c,postalcode__c,postalcodeAddOn__c,CountryCode__c,
                                                 Latitude__c,Longitude__c FROM Address__c WHERE OriginalAddress__c =: uniqueOriginalAddress];
            //if no address exists
            if(addrResponseList.size() == 0){
                //melissa call
                melissaResponse = MelissaDataAPI.validateAddr(street1, street2 !=null ? street2: '', city, state, postalcode);
               System.debug('melissaResponse--'+melissaResponse);
                //To display on the page   
                adrmel.street1 =  melissaResponse.Addr1;
                adrmel.city = melissaResponse.City;
                adrmel.state = melissaResponse.State;
                adrmel.postalcode = melissaResponse.Zip;
                adrmel.country = melissaResponse.CountryCode;
                mrw.addrMelissa = adrmel;
                if(String.isNotBlank(melissaResponse.Suite)){
                     adrmel.street2 = melissaResponse.Suite;
                 }
                if(melissaResponse != null){
                    if(!overRideTrue){
                        for(String code : melissaResponse.ResultCodes){
                           if(code.containsIgnoreCase('AE')){
                               mrw.message = 'Error:'+code;
                               mrw.status = 'Error';
                               return mrw;
                           }
                        }
                    }
                  
                 uniqueOriginalAddress ='';
                 uniqueOriginalAddress = melissaResponse.Addr1+'+';                    
                 String address2 = melissaResponse.Addr2;
                 if(String.isNotBlank(melissaResponse.Suite)){
                     address2 = melissaResponse.Suite;
                 }
                 uniqueOriginalAddress +=String.isnotBlank(address2) ? address2 + '+' +melissaResponse.City+ '+' +melissaResponse.State+'+'+melissaResponse.Zip : melissaResponse.City+ '+'+ melissaResponse.State+ '+'+ melissaResponse.Zip;
                 System.debug('uniqueOriginalAddress--'+uniqueOriginalAddress);
                    //Query the address
                 addrResponseList = [SELECT Id,OriginalAddress__c,Street__c,Validated__c ,Street2__c,StreetNumber__c,StreetName__c,CHS__c,
                                            City__c,County__c,State__c,postalcode__c,postalcodeAddOn__c,CountryCode__c,
                                            Latitude__c,Longitude__c FROM Address__c WHERE OriginalAddress__c =: uniqueOriginalAddress];
                    System.debug('addrResponseList--'+addrResponseList);
                if(addrResponseList.size() == 0){
                    //Create Lead and Address with melissa data
                    mrw.addressId = createLeadAdress(melissaResponse,street1,street2,city,state,postalcode,overRideTrue);
                    mrw.status = 'Success';
                    mrw.addrMelissa = adrmel;
                    }
                    else{                           
                        //address exists after melissa validation so assign address addrResponseList[0] to the new lead
                        if(String.isBlank(addrResponseList[0].postalcode__c)||String.isBlank(addrResponseList[0].postalcodeAddOn__c) ||
                           addrResponseList[0].Latitude__c==NULL||addrResponseList[0].Longitude__c==NULL|| String.isBlank(addrResponseList[0].City__c) || 
                           String.isBlank(addrResponseList[0].State__c)|| String.isBlank(addrResponseList[0].CountryCode__c)|| 
                           String.isBlank(addrResponseList[0].County__c)){
                            melissaResponse = checkForMelissaData(addrResponseList[0]);
                            addrResponseList[0] = updateInvalidAddress(melissaResponse,addrResponseList[0]);
                        }
                        //To display on the page   
                        adrmel.street1 =  addrResponseList[0].Street__c;
                        adrmel.city = addrResponseList[0].City__c;
                        adrmel.state = addrResponseList[0].State__c;
                        adrmel.postalcode = addrResponseList[0].PostalCode__c;
                        adrmel.country = addrResponseList[0].CountryCode__c;
                        if(String.isNotBlank(addrResponseList[0].Street2__c)){
                            adrmel.street2 = addrResponseList[0].Street2__c;
                        }
                        mrw.addrMelissa = adrmel;
                        mrw.addressId = addrResponseList[0].Id;
                        mrw.status = 'Success';
                    }                            
                }
            }
            
            //address exists in the system
            else{
                //assign address addrResponseList[0] to the new lead
                if(String.isBlank(addrResponseList[0].PostalCode__c)||String.isBlank(addrResponseList[0].PostalCodeAddOn__c) ||addrResponseList[0].Latitude__c==NULL||addrResponseList[0].Longitude__c==NULL|| String.isBlank(addrResponseList[0].City__c) || String.isBlank(addrResponseList[0].State__c)|| String.isBlank(addrResponseList[0].CountryCode__c)|| String.isBlank(addrResponseList[0].County__c)){
                  melissaResponse = checkForMelissaData(addrResponseList[0]);
                  addrResponseList[0] = updateInvalidAddress(melissaResponse,addrResponseList[0]);
                }
                        //To display on the page   
                adrmel.street1 =  addrResponseList[0].Street__c;
                adrmel.city = addrResponseList[0].City__c;
                adrmel.state = addrResponseList[0].State__c;
                adrmel.postalcode = addrResponseList[0].PostalCode__c;
                adrmel.country = addrResponseList[0].CountryCode__c;
                if(String.isNotBlank(addrResponseList[0].Street2__c)){
                    adrmel.street2 = addrResponseList[0].Street2__c;
                }
                mrw.addrMelissa = adrmel;
                mrw.addressId = addrResponseList[0].Id;
                mrw.status = 'Success';
            }
            system.debug('mrw##'+mrw);
            return mrw;                                
        }catch(Exception ae){
            mrw.status = 'Error';
            mrw.message = ae.getMessage();
            return mrw;
        }
       
    }
    
    public static Id createLeadAdress(MelissaDataAPI.MelissaDataResults mdr,String street1, String street2, String city, String state, String postalcode, Boolean isOverRide){
        Boolean error = false;
        Boolean didOverride = isOverRide;
        Address__c addr = new Address__c();
        // Set Addr
        addr.Street__c = mdr != null ? mdr.Addr1 : street1;
        String address2 = mdr!=null? mdr.Addr2:street2 ;
        if (mdr!=null && String.isNotBlank(mdr.Suite)){
            address2 = mdr.Suite;
        }
        addr.Street2__c = String.isNotBlank(address2) ? address2 : String.isNotBlank(street2)? street2 :'';
        addr.StreetNumber__c = mdr != null ? mdr.StreetNumber: '';
        addr.StreetName__c = mdr != null ? mdr.StreetName : '';
        addr.City__c = mdr != null ? mdr.City :String.isNotBlank(city) ? city : '';
        addr.County__c = mdr != null ? mdr.County :'';
        addr.State__c = mdr != null ?  mdr.State :String.isNotBlank(state) ? state : '';
        addr.postalcode__c = (mdr != null && String.isNotBlank(mdr.Zip))? mdr.Zip : String.isNotBlank(postalcode) ? postalcode : '';
        addr.postalcodeAddOn__c = mdr != null ? mdr.Plus4 :'';
        addr.CountryCode__c = (mdr != null && String.isNotBlank(mdr.CountryCode)) ? mdr.CountryCode : '';
        addr.Latitude__c = mdr != null ? decimal.valueOf(mdr.Lat) : 0.00;
        addr.Longitude__c = mdr != null ? decimal.valueOf(mdr.Lon) : 0.00;
        addr.OriginalAddress__c = mdr != null ? mdr.Addr1+'+':street1+'+';
        if(mdr != null){
            addr.Validated__c = true;
            String errorFlag = '';
            String melissaAddress2 = mdr.Addr2;
            if (String.isNotBlank(mdr.Suite)){
                melissaAddress2 = mdr.Suite;
            }
            addr.OriginalAddress__c += String.isnotBlank(melissaAddress2) ? melissaAddress2 + '+' +mdr.City+ '+' +mdr.State+'+'+mdr.Zip : mdr.City+ '+'+ mdr.State+ '+'+ mdr.Zip;
            for (String code : mdr.ResultCodes){
                if (code.containsIgnoreCase('AE')){
                    errorFlag = 'Error:'+code;
                    break;
                }
            }     
        }else {
            addr.OriginalAddress__c += String.isnotBlank(street2) ? street2 + '+' +city+ '+' +state+'+'+postalcode : city+ '+'+ state+ '+'+ postalcode;
        }
        addr.StandardizedAddress__c = mdr != null ? mdr.Addr1+',' :street1 + ',';
        if(mdr != null){
            String meliAddress2 = mdr.Addr2;
            if (String.isNotBlank(mdr.Suite)){
                meliAddress2 = mdr.Suite;
            }
            addr.StandardizedAddress__c += String.isnotBlank(meliAddress2) ?meliAddress2 + ',' +mdr.City+ ',' +mdr.State+','+mdr.Zip : mdr.City+ ','+ mdr.State+ ','+ mdr.Zip;
        }else {
            addr.StandardizedAddress__c += String.isnotBlank(street2) ? street2 + ',' +city+ ',' +state+','+postalcode: city+ ','+ state+ ','+ postalcode;
        }
        //override codes
        if (error && didOverride) {
            addr.ValidationMsg__c = 'User Override';
            for (String code: mdr.ResultCodes) {
                addr.ValidationMsg__c += ':' + code.substring(0,4);
            }
        }
        addr.StandardizedAddress__c = addr.StandardizedAddress__c.toUpperCase();
        addr.OriginalAddress__c = addr.OriginalAddress__c.toUpperCase();         
        insert addr;
        return addr.Id;
    }
    
    // melissa data check
    public static MelissaDataAPI.MelissaDataResults checkForMelissaData(Address__c addr){
        MelissaDataAPI.MelissaDataResults melissaResponse = new MelissaDataAPI.MelissaDataResults();
        //check if reqAddr is null
        if(addr != null){
            melissaResponse = MelissaDataAPI.validateAddr(addr.Street__c, String.isNotBlank(addr.Street2__c)? addr.Street2__c : '', addr.City__c, addr.State__c,addr.postalcode__c);
        }
        return melissaResponse;
    }
    
    public static Address__c updateInvalidAddress(MelissaDataAPI.MelissaDataResults mdr, Address__c addr){
        Boolean error = false;
        Boolean didOverride = false;
        if(mdr != null){
            addr.postalcode__c = mdr.Zip;
            addr.City__c = mdr.City;
            addr.State__c = mdr.State;
            addr.CountryCode__c =mdr.CountryCode;
            addr.County__c =mdr.County;
            addr.Latitude__c=Decimal.valueof(mdr.Lat);
            addr.Longitude__c=Decimal.Valueof(mdr.Lon);
            addr.postalcodeAddOn__c=mdr.Plus4;
            addr.Validated__c = true;
            for (String code : mdr.ResultCodes){
                if (code.startsWith('AE')){
                    error = true;
                    didOverride = true;
                }
            }
            if (error && didOverride) {
                addr.ValidationMsg__c = 'User Override';
                for (String code: mdr.ResultCodes) {
                    addr.ValidationMsg__c += ':' + code.substring(0,4);
                }
            }
            update addr;
        }
        return addr;
    }  
    
}