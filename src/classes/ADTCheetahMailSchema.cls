/************************************* MODIFICATION LOG ********************************************************************************************
* ADTCheetahMailSchema
*
* DESCRIPTION : Schema for Cheetah API ADTCheetahMailSchema.
*
* Jitendra Kothari				10/10/2019			HRM-10967			Configurator Cheetahmail Requirement
*/
global class ADTCheetahMailSchema {
	global class CheetahMailReqWrapper{
		public String template{get;set;}
		public String customerEmailAddress{get;set;}
		public String heroImage{get;set;}
		public String protect{get;set;}
		public list<KeyProductDef> keyProduct{get;set;}
		public list<GroupProductDef> groupProduct{get;set;}
		public String tollFree{get;set;}
		public Boolean callNowButton{get;set;}
		public String person{get;set;}
	}
	global class KeyProductDef{
		public String keyProductName{get;set;}
	}
	global class GroupProductDef{
		public String groupProductName{get;set;}
	}
	public static void sendCheetahEmail(CustomerInterestConfig__c cic){
		if(String.isNotBlank(cic.Email__c) && String.isNotBlank(cic.Response__c)){
			RequestQueue__c rqToInsert = sendCheetahEmail(cic.Email__c, cic.Response__c);
			//rqToInsert = ADTCheetahMailSchema.doACKEmailCallOut(rqToInsert);
			if(cic.Id == null){
	        	insert cic;
	        }
	        rqToInsert.Customer_Web_Interest__c = cic.Id;
	        insert rqToInsert;
		}
	}
	public static RequestQueue__c sendCheetahEmail(String email, String emailDetails){
		RequestQueue__c rqToInsert = new RequestQueue__c();
		try{
                
        	if(String.isNotBlank(emailDetails)){
				list<String> splitedValues = emailDetails.split('\\,');
				system.debug('$$$' + splitedValues);
				ADTCheetahMailSchema.CheetahMailReqWrapper cmrw = new ADTCheetahMailSchema.CheetahMailReqWrapper();
				
				cmrw.CustomerEmailAddress = email;
				for(String pairs : splitedValues){
				    list<String> pair = pairs.split(':');
					
					if(pair.size() > 1) {
						String key = pair.get(0).trim().toLowerCase(); 
						String value = pair.get(1).trim(); 
						
						if(key == 'callnowbuttonflag'){
							Boolean val = Boolean.valueOf(value);
							cmrw.callNowButton = val;
						} else if (key == 'tollfree'){
							cmrw.tollFree = value;
						} else if (key == 'personalcode'){
							cmrw.person = value;
						} else if (key == 'templateid'){
							cmrw.template = value;
						} else if (key == 'heroimage'){
							cmrw.heroImage = value;
						} else if (key == 'protect'){
							cmrw.protect = value;
						} else if (key == 'keyproduct1' || key == 'keyproduct2'){
							if(cmrw.keyProduct == null){
								cmrw.keyProduct = new list<ADTCheetahMailSchema.KeyProductDef>();
							}
                            ADTCheetahMailSchema.KeyProductDef val = new ADTCheetahMailSchema.KeyProductDef();
							val.keyProductName = value;
							cmrw.KeyProduct.add(val);
						} else if (key == 'groupproduct1' || key == 'groupproduct2' || key == 'groupproduct3' || key == 'groupproduct4'){
							if(cmrw.groupProduct == null){
								cmrw.groupProduct = new list<ADTCheetahMailSchema.GroupProductDef>();
							}
                            ADTCheetahMailSchema.GroupProductDef val = new ADTCheetahMailSchema.GroupProductDef();
                            val.groupProductName = value;
							cmrw.groupProduct.add(val);
						}
					}
				}
				
				String parsedJson = JSON.serialize(cmrw);
	            rqToInsert = createCheetahMailRequestQueue(email, parsedJson);
        	}
        }catch(Exception e){
            system.debug('ACK Exception Error::'+e.getMessage()+e.getLineNumber());
            ADTApplicationMonitor.log(e,'ADTCheetahMailAPI','sendCheetahEmail',ADTApplicationMonitor.CUSTOM_APP.SERVICEPOWER);
        }
        return rqToInsert;
	}
	public static RequestQueue__c createCheetahMailRequestQueue(String email, String jsonString){
        /*String messageDivision = '';
        if(String.isNotBlank(aLog.LineOfBusiness__c)){
            if(aLog.LineOfBusiness__c.containsIgnoreCase('1100')){
                messageDivision = 'Resi';
            }else if(aLog.LineOfBusiness__c.containsIgnoreCase('1200')){
                messageDivision = 'Smb';
            }else if(aLog.LineOfBusiness__c.containsIgnoreCase('1300')){
                messageDivision = 'Comm';
            }   
        }*/
        //String emailSubject=(!String.isBlank(aLog.EmailSubject__c))?aLog.EmailSubject__c:'No Subject';
        // Create JSON to store in service message
        //String jsonString = JSON.serialize(new CheetahMailRequestWrapper(aLog.FormName__c,new NameWrapper(aLog.FirstName__c,aLog.LastName__c),new PhoneWrapper(Utilities.getFormattedPhoneForWebleads(aLog.PrimaryPhone__c)),aLog.Email__c,messageDivision,emailSubject,'false',aLog.PromotionCode__c,'L'));
        //system.debug('ACK JSON: '+ jsonString);
        // Create Request queue
        RequestQueue__c rq = new RequestQueue__c();
        
        rq.RequestStatus__c = 'Ready';
        rq.ServiceTransactionType__c = 'sendPCEmail';
        rq.MessageID__c = email;
        rq.ServiceMessage__c = jsonString;
        rq.RequestDate__c = System.now();
        rq.counter__c = 0;
        return rq;
    }
    
    /**
    * This method is used to make the callout to CheetahMailService
    * @Param RequestQueue__c
    * @ReturnType Void
    **/
    public static RequestQueue__c doACKEmailCallOut(RequestQueue__c requeue){
        String userName = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPUsername__c:'abc';
        String password = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPPassword__c:'def';
        String endPointURL = (!Test.isRunningTest())? IntegrationSettings__c.getInstance().DPConfigratorCheetahMailEndpoint__c:'http:abc.com';
        Decimal timeout = (!Test.isRunningTest())? IntegrationSettings__c.getInstance().DPEBRTimeout__c:6000;
        Blob headerValue = Blob.valueOf(userName + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        Httprequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type', 'application/json');
        request.setEndpoint(endPointURL);
        request.setTimeout(Integer.ValueOf(timeout));
        request.setMethod('POST');
        request.setBody(requeue.ServiceMessage__c);
        system.debug('ACK Request: '+request);
        try{
            // Send the request
            response = http.send(request);
        }catch(exception e){
            requeue.ErrorDetail__c = 'Exception: '+ e.getMessage();
        }
        system.debug('ACK Response: '+response);
        if(response != null && (response.getStatusCode() == 204 || response.getStatusCode() == 200)){
            // success response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_SUCCESS;
            requeue.ErrorDetail__c = null;
        }else if(response != null){
            // error response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            //update to hardcode
            requeue.ErrorDetail__c = String.isNotBlank(requeue.ErrorDetail__c)? requeue.ErrorDetail__c + '\n ACK Response: ' +response.getbody() : response.getbody();
        }else{
            //error response
        	requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            //update to hardcode
            requeue.ErrorDetail__c = String.isNotBlank(requeue.ErrorDetail__c) ? requeue.ErrorDetail__c + '\n ACK Response: Response is blank' : 'Response is blank';
        }
        /*ADTCheetahEmailServiceScema.sendProdConfigLeadEmailRequest_element cmrw = (ADTCheetahEmailServiceScema.sendProdConfigLeadEmailRequest_element)JSON.deserialize(requeue.ServiceMessage__c, ADTCheetahEmailServiceScema.sendProdConfigLeadEmailRequest_element.class);
				
        
        ADTCheetahEmailServiceScema.CPQUtilitiesSvcSOAP temp = new ADTCheetahEmailServiceScema.CPQUtilitiesSvcSOAP();
	            ADTCheetahEmailServiceScema.sendProdConfigLeadEmailResponse_element response = temp.sendProdConfigLeadEmail(cmrw.TemplateID, cmrw.CustomerEmailAddress, 
	            		cmrw.HeroImage, cmrw.Protect, cmrw.KeyProduct, cmrw.GroupProduct, cmrw.TollFree, cmrw.CallNowButtonFlag, cmrw.PersonalCode);
	            system.debug('$$$' + response);
        system.debug('ACK Response: '+response);
        if(response != null && response.ErrorNumber == '0'){
            // success response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_SUCCESS;
            requeue.ErrorDetail__c = null;
        }
        else{
            // error response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            requeue.ErrorDetail__c = String.isNotBlank(requeue.ErrorDetail__c)? requeue.ErrorDetail__c + '\n ACK Response: ' +response.ErrorMessage : response.ErrorMessage;
        }*/
        return requeue;
    }
}