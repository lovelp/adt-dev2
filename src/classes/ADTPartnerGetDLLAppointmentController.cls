public with sharing class ADTPartnerGetDLLAppointmentController {
    
    getDLLAppointmentResponse dllAppointmentResponse;
    public integer statusCode;
    public TimeZone customerTimeZone;
    public Event upcomingCustomerAppointment;
    public Time sameDayStartTime;
    public Map<Id, User> mapTerritoryUser;
    public Map<Id, List<SchedUserTerr__c>> mapSalesRepSchedule;
    public integer apptDuration;
    public Date apptStartDate;
    public boolean isSameDayAppointment;
    public ADTPartnerAppointmentHelper apptHelperController;
    public integer numSearchDays;
    
    /*public class getDLLAppointmentRequest
    {
        public getInstallAppointmentSlotsRequest getInstallAppointmentSlotsRequest;
    }*/
    
    public class getDLLAppointmentRequest
    {
        public string partnerId;
        public string callId;
        public string opportunityId;
        public string partnerRepName;
        public string orderNumber;
        public appointmentRequest appointmentRequest;
    }
    
    public class appointmentRequest
    {
        public string installType;
        public string startingDate;
    }
    
    /*public class getDLLAppointmentResponse 
    {
       public getInstallAppointmentSlotsResponse getInstallAppointmentSlotsResponse; 
    }*/
    
    public class getDLLAppointmentResponse 
    {
        public string opportunityId;
        public string orderNumber;
        public string message;
        public string installType;
        public List<installJobs> installJobs;
    }
    
    public class installJobs
    {
        public string jobNumber;
        public List<slots> slots;
        //public salesRep salesRep;
    }
    
    public class slots
    {
        // need to be changed in swagger, as of now using appointmentdate
        // public string date;
        public string appointmentDate;
        public List<appointmentTimes> appointmentTimes;        
    }
    
    public class appointmentTimes
    {
        public string startTime;
        public string endTime;
        public string offerId;
    }
    
    public ADTPartnerGetDLLAppointmentController ()
    {
        dllAppointmentResponse = new getDLLAppointmentResponse ();  
        statusCode = 200;
        customerTimeZone = UserInfo.getTimeZone();
        mapTerritoryUser = new Map<Id, User> ();
        mapSalesRepSchedule = new Map<Id, List<SchedUserTerr__c>> ();
        apptDuration = 0;
        apptStartDate = date.today ();
        isSameDayAppointment = false;
        apptHelperController = new ADTPartnerAppointmentHelper ();
        numSearchDays = 1;
    }
    
    public getDLLAppointmentResponse parseJSONRequest (string jsonRequest)
    {
        getDLLAppointmentRequest appointmenRequestWrapper = new getDLLAppointmentRequest (); 
        try
        {
            system.debug('User Customer TimeZone'+customerTimeZone);
            // if blank json request
            if (string.isBlank(jsonRequest))
            {
                createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('400').Error_Message__c);                                         
                statusCode = 400;
            }
            else
            {
                system.debug('@@jsonRequest: '+jsonRequest);
                appointmenRequestWrapper = (getDLLAppointmentRequest) system.JSON.deserialize(jsonRequest, getDLLAppointmentRequest.class);
                
                system.debug('@@appointmenRequestWrapper: '+appointmenRequestWrapper);
                if (string.isNotBlank(appointmenRequestWrapper.partnerId) && string.isNotBlank(appointmenRequestWrapper.callId) && string.isNotBlank(appointmenRequestWrapper.partnerRepName))
                {
                    if (appointmenRequestWrapper.appointmentRequest != null && string.isNotBlank(appointmenRequestWrapper.opportunityId) && string.isNotBlank(appointmenRequestWrapper.orderNumber))
                    {
                        if (string.isNotBlank(appointmenRequestWrapper.appointmentRequest.installType) && appointmenRequestWrapper.appointmentRequest.installType.equalsIgnoreCase('phoneActivation'))
                        {   
                            string accountId = apptHelperController.validateCallAndOpportunity (appointmenRequestWrapper.opportunityId, appointmenRequestWrapper.callId);
                            if (string.isNotBlank(accountId))
                            {
                                List<account> lstAccount = apptHelperController.getAccount (accountId);
                                //system.debug('@@ AccountList'+lstAccount);                    
                                if (lstAccount != null)
                                {
                                    List<scpq__SciQuote__c> lstQuote = apptHelperController.getQuote (appointmenRequestWrapper.orderNumber , lstAccount[0].id);
                                    system.debug('@@ Quote'+lstQuote);
                                    if (lstQuote != null)
                                    {
                                        if (string.valueOf(lstQuote[0].scpq__Status__c).equalsIgnoreCase('Submitted') || string.valueOf(lstQuote[0].scpq__Status__c).equalsIgnoreCase('Signed'))
                                        {
                                            system.debug('get the appt slots');
                                            getDLLAppointmentSlots (appointmenRequestWrapper, lstAccount[0], lstQuote[0]);  
                                        }
                                        else
                                        {
                                            // Order Number not in Signed or Submitted status
                                            createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('OrderStatusNotValid').Error_Message__c);                                         
                                            statusCode = 200;
                                        }                                        
                                    }
                                    else
                                    {
                                        // Order Number not found
                                        createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('404').Error_Message__c.replace('requestID','Order Number'));                                         
                                        statusCode = 404; 
                                    }                        
                                }
                                else 
                                {
                                    // Opportunity ID not found
                                    createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('404').Error_Message__c.replace('requestID','Opportunity ID'));                                       
                                    statusCode = 404; 
                                }
                            }
                            else
                            {
                                // Opportunity ID and Call Id does not return a common account
                                createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('appointmentCallOppIDError').Error_Message__c);                                       
                                statusCode = 400;
                            }                              
                        }
                        else
                        {
                            //string errMessage = 'Installation Type is not Phone Activation';
                            createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('NotPhoneActivation').Error_Message__c);
                            statusCode = 400;
                            //ADTApplicationMonitor.log ('ADTPartner Invalid Data Error', errMessage, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                        }
                        
                    }   
                    else
                    {
                        // Opportunity Id or Order Number missing
                        createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('400').Error_Message__c);                              
                        statusCode = 400; 
                    }
                }
                else
                { 
                    //invalid input, missing required elements
                    system.debug('invalid input, missing required elements');
                    createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('400').Error_Message__c);                              
                    statusCode = 400; 
                }
            }
        }
        catch (Exception ex)
        {
            // something went wrong
            system.debug('@@ Exception Occured: ' + ex.getLineNumber() + ex.getMessage());
            ADTApplicationMonitor.log(ex, 'ADTPartnerGetDLLAppointmentController', 'parseJSONRequest', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('500').Error_Message__c);                              
            statusCode = 500;
        }
        
        return dllAppointmentResponse;             
    }
    
    public void createErrorResponse (getDLLAppointmentRequest appointmenRequestWrapper, string errMessage)
    {
        //getInstallAppointmentSlotsResponse objResponse = new getInstallAppointmentSlotsResponse ();
        dllAppointmentResponse.message = errMessage;
        dllAppointmentResponse.opportunityId = (appointmenRequestWrapper.opportunityId != null ) ? appointmenRequestWrapper.opportunityId : '';    
        dllAppointmentResponse.orderNumber = (appointmenRequestWrapper.orderNumber != null ) ? appointmenRequestWrapper.orderNumber : '';
        dllAppointmentResponse.installType = (appointmenRequestWrapper.appointmentRequest != null) ? appointmenRequestWrapper.appointmentRequest.installType : '';
        appointmentTimes objTimes = new appointmentTimes ();
        objTimes.startTime = '';
        objTimes.endTime = '';
        objTimes.offerId = '';
        List<appointmentTimes> lstTimes = new List<appointmentTimes> ();
        lstTimes.add(objTimes);
        slots responseSlot = new slots ();
        responseSlot.appointmentDate = '';
        responseSlot.appointmentTimes = lstTimes;
        List<slots> lstSlots = new List<slots> ();
        //lstSlots.add(responseSlot);
        installJobs objInstallJobs = new installJobs ();
        objInstallJobs.jobNumber = '';
        objInstallJobs.slots = lstSlots;
        List<installJobs> lstInstallJobs = new List<installJobs> ();
        lstInstallJobs.add(objInstallJobs);
        dllAppointmentResponse.installJobs = lstInstallJobs; 
        //dllAppointmentResponse.getInstallAppointmentSlotsResponse = objResponse;
    }
    
    public void getDLLAppointmentSlots (getDLLAppointmentRequest appointmenRequestWrapper, Account acc, scpq__SciQuote__c quote)
    {
        try
        {
            system.debug('@@appointmenRequestWrapper: '+appointmenRequestWrapper);
            // default the req start date to next date, in case we get blank start date from Partner
            date reqStartDate = date.today().addDays(1);   
            
            //if (string.isNotBlank(appointmenRequestWrapper.appointmentRequest.startingDate))
            //{
                string errMessage = apptHelperController.validateDateTimeFormat (appointmenRequestWrapper.appointmentRequest.startingDate, '');
                
                if (string.isBlank(errMessage))
                {
                    // if startingDate is not blank, take that as req start date
                    if (string.isNotBlank(appointmenRequestWrapper.appointmentRequest.startingDate))
                    {
                        reqStartDate = date.valueOf(appointmenRequestWrapper.appointmentRequest.startingDate);
                    }
                    system.debug('@@reqStartDate: '+reqStartDate);
                                        
                    // check if the requested appt date is not a past date
                    if (!apptHelperController.pastDateApptStartDate(reqStartDate))
                    {
                        // validate if the DLL order, proceed only if true
                        if (apptHelperController.validateInHouseActivationOrder (acc.id, appointmenRequestWrapper.orderNumber))
                        {
                            apptDuration = SchedDefaults__c.getInstance('In House Activation').Duration__c.intValue();
                            numSearchDays = Integer.valueOf(PartnerSettings__c.getorgDefaults().DLLAppointmentDays__c);
                            isSameDayAppointment = apptHelperController.sameDayAppointment (reqStartDate);
                            System.debug('@@SameDayAppointment'+isSameDayAppointment);
                            customerTimeZone = apptHelperController.getcustomerTimeZone (acc);   
                            system.debug('@@customerTimeZone'+customerTimeZone);
                            // Search for the available appointment slots                    
                            searchAvailableSlot (appointmenRequestWrapper, acc, quote, reqStartDate);
                            
                        }
                        else
                        {
                            // Not a DLL order
                            createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('Not Activation Order').Error_Message__c);                             
                            statusCode = 200; 
                        }
                    }
                    else
                    {
                        // invalid start date
                        createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('pastStartDate').Error_Message__c);                             
                        statusCode = 400; 
                    }
                }
                else
                {
                    // invalid date format
                    createErrorResponse (appointmenRequestWrapper, errMessage);                             
                    statusCode = 400;
                }
            /*}
            else
            {
                // appointment start date missing
                createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('400').Error_Message__c);                              
                statusCode = 400; 
            }*/
        }
        catch (Exception ex)
        {
            // something went wrong
            ADTApplicationMonitor.log(ex, 'ADTPartnerGetDLLAppointmentController', 'getDLLAppointmentSlots', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
            createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('500').Error_Message__c);                             
            statusCode = 500;
        }
        
    }
    
    public void searchAvailableSlot (getDLLAppointmentRequest appointmenRequestWrapper, Account acc, scpq__SciQuote__c quote, Date reqStartDate)
    {   
        Map<Id, FieldSalesAppointmentController.AgentSchedule> mapSalesRepAvailableTime = new Map<Id, FieldSalesAppointmentController.AgentSchedule> ();
        
        integer dayCounter = 0;
        integer counter = 0;
                
        // check for am upcoming appointment for this account, proceed if no appointments
        string errString = apptHelperController.verifyUpcomingAppointments (acc, customerTimeZone);
        if (string.isBlank(errString))
        {
            map<date, set<time>> mapRestAvailableTimeSlots = new  map<date, set<time>>();
            while (counter < numSearchDays)
            {
                apptStartDate = reqStartDate.addDays(dayCounter);
                system.debug('@@apptStartDate: '+apptStartDate);
                // If same day appointment
                if( isSameDayAppointment )
                {
                    sameDayStartTime = apptHelperController.getSameDayStartTime (apptStartDate, customerTimeZone);
                }
                // get DLL reps and their schedule
                apptHelperController.getUserScheduleSetting (apptStartDate);
                
                mapTerritoryUser = apptHelperController.mapTerritoryUser;
                mapSalesRepSchedule = apptHelperController.mapSalesRepSchedule;
                system.debug('@@mapTerritoryUser: '+mapTerritoryUser);
                system.debug('@@mapSalesRepSchedule: '+mapSalesRepSchedule);
                
                mapSalesRepAvailableTime = apptHelperController.getUserScheduleByEvent( mapTerritoryUser, apptDuration, apptStartDate, customerTimeZone, sameDayStartTime );
                system.debug('@@getUserScheduleByEvent.mapSalesRepAvailableTime: '+mapSalesRepAvailableTime);
                
                mapSalesRepAvailableTime = apptHelperController.getUserBySchedule( mapSalesRepAvailableTime, mapSalesRepSchedule, apptDuration, apptStartDate, mapTerritoryUser,customerTimeZone  );
                system.debug('@@getUserBySchedule.mapSalesRepAvailableTime: '+mapSalesRepAvailableTime);
                
                FieldSalesAppointmentController.ActivationTechnicianSch ActivationTechnicianAvailableSchedule = new FieldSalesAppointmentController.ActivationTechnicianSch ();
                Map<String, Map<Id, FieldSalesAppointmentController.AgentSchedule>> SalesRepAvailableTimeMap = new Map<String, Map<Id, FieldSalesAppointmentController.AgentSchedule>>();
                
                Set<FieldSalesAppointmentController.ActivationTechnicianTime> SalesRepAvailableTimeSet = new Set<FieldSalesAppointmentController.ActivationTechnicianTime>();
                for(String aKey: mapSalesRepAvailableTime.keySet()){
                    // aKey = Id of the sales rep user
                    FieldSalesAppointmentController.AgentSchedule aschEl = mapSalesRepAvailableTime.get(aKey);
                    for( FieldSalesAppointmentController.AgentSlot asEl: aschEl.AgentAvailableSlotList){
                        system.debug('@@ ==> adding agent schedule...['+aKey+']'+asEl);
                        SalesRepAvailableTimeSet.add( new FieldSalesAppointmentController.ActivationTechnicianTime(asEl.startTime, asEl.endTime, apptStartDate) );
                    }
                }
                SalesRepAvailableTimeMap.put( apptStartDate.format(), mapSalesRepAvailableTime.clone());
                List<FieldSalesAppointmentController.ActivationTechnicianTime> SalesRepMergeAvailableTimeList = new List<FieldSalesAppointmentController.ActivationTechnicianTime> ();
                
                // new version using merge
                List<FieldSalesAppointmentController.ActivationTechnicianTime> tmpList = new List<FieldSalesAppointmentController.ActivationTechnicianTime>();
                tmpList.addAll(SalesRepAvailableTimeSet);
                Integer maxMerge = 0;
                do{
                    tmpList = apptHelperController.mergeOverlappingSchedule(tmpList, apptStartDate);
                    maxMerge++;
                } while( apptHelperController.isMergeNeeded(tmpList) && maxMerge < 5 );
                SalesRepMergeAvailableTimeList.addAll( tmpList );
                tmpList.clear();
                
                // continuation of merge using either
                SalesRepMergeAvailableTimeList.sort();
                Set<FieldSalesAppointmentController.ActivationTechnicianTime> SalesRepMergeAvailableTimeSet = new Set<FieldSalesAppointmentController.ActivationTechnicianTime>();
                SalesRepMergeAvailableTimeSet.addAll(SalesRepMergeAvailableTimeList);
                Map<String, FieldSalesAppointmentController.ActivationTechnicianSch> ActivationTechnicianSlotMap = new Map<String, FieldSalesAppointmentController.ActivationTechnicianSch> ();
                ActivationTechnicianSlotMap.put(apptStartDate.format(), new FieldSalesAppointmentController.ActivationTechnicianSch(apptStartDate, SalesRepMergeAvailableTimeSet) );
                system.debug('@@ActivationTechnicianSlotMap: '+ActivationTechnicianSlotMap);
                
                system.debug('@@SalesRepMergeAvailableTimeSet: '+SalesRepMergeAvailableTimeSet);
                
                //map<time,time> mapStartEndTimeSlots = new map<time,time> ();
                if (!SalesRepMergeAvailableTimeSet.isEmpty())
                {
                    set<time> lstTime = new set<time> ();
                    for (FieldSalesAppointmentController.ActivationTechnicianTime objAvailableTime : SalesRepMergeAvailableTimeSet)
                    {
                        time startTime = objAvailableTime.StartTime;
                        time endTime = objAvailableTime.EndTime;
                        system.debug('@@startTime: '+startTime + ' endTime: '+endTime);
                        DateTime stDateTime = Datetime.newInstanceGmt(Date.today(),startTime);
                        DateTime endDateTime = Datetime.newInstanceGmt(Date.today(),endTime);
                        //lstTime.add(startTime);
                        //while (startTime < endTime)
                        while(stDateTime < endDateTime )
                        { 
                            if (stDateTime.addMinutes(apptDuration) <= endDateTime)
                            {
                                lstTime.add(startTime);
                            }                            
                            startTime = startTime.addMinutes(apptDuration);
                            stDateTime = stDateTime.addMinutes(apptDuration);
                            
                        }
                        //lstTime.add(endTime);
                        system.debug('@@lstTime: '+lstTime);                        
                        
                        //mapStartEndTimeSlots.put(objAvailableTime.StartTime, objAvailableTime.EndTime);
                        //mapRestAvailableTimeSlots.put(apptStartDate, mapStartEndTimeSlots);
                    }
                    mapRestAvailableTimeSlots.put(apptStartDate, lstTime);
                    system.debug('@@mapRestAvailableTimeSlots: '+mapRestAvailableTimeSlots);
                    counter ++;
                    system.debug('@@counter : '+counter);
                } 
                
                dayCounter ++;
                system.debug('@@dayCounter : '+dayCounter);
            }
            
            //system.debug('@@mapStartEndTimeSlots: '+mapStartEndTimeSlots);
            //system.debug('@@mapRestAvailableTimeSlots: '+mapRestAvailableTimeSlots);
            List<slots> lstSlots = new List<slots> ();
            installJobs objInstallJobs = new installJobs ();
            List<installJobs> lstInstallJobs = new List<installJobs> ();
            
            objInstallJobs.jobNumber = quote.MMBJobNumber__c != null ? quote.MMBJobNumber__c : '' ;
            for (date schDate : mapRestAvailableTimeSlots.keySet())
            {
                slots responseSlot = new slots ();
                responseSlot.appointmentDate = string.valueOf(schDate) ;
                List<appointmentTimes> lstTimes = new List<appointmentTimes> ();
                for (time t : mapRestAvailableTimeSlots.get(schDate))
                {
                    appointmentTimes objTimes = new appointmentTimes ();
                    //responseSlot.startTime = string.valueOf(t);
                    objTimes.startTime = string.valueOf(t).substringBeforeLast(':'); 
                    objTimes.endTime = string.valueOf(t.addMinutes(apptDuration)).substringBeforeLast(':') ;
                    objTimes.offerId = responseSlot.appointmentDate.replace('-', '') + objTimes.startTime.replace(':', '') + objTimes.endTime.replace(':', '');
                    lstTimes.add(objTimes);
                }               
                responseSlot.appointmentTimes = lstTimes;
                lstSlots.add(responseSlot);
            }
            
            objInstallJobs.slots = lstSlots;
            lstInstallJobs.add(objInstallJobs);
            //getInstallAppointmentSlotsResponse objResponse = new getInstallAppointmentSlotsResponse ();
            dllAppointmentResponse.installJobs = lstInstallJobs;
            dllAppointmentResponse.opportunityId = appointmenRequestWrapper.opportunityId;    
            dllAppointmentResponse.orderNumber = appointmenRequestWrapper.orderNumber;
            dllAppointmentResponse.installType = appointmenRequestWrapper.appointmentRequest.installType;
            dllAppointmentResponse.message = PartnerAPIMessaging__c.getinstance('Get DLL Success').Error_Message__c;
            //dllAppointmentResponse.getInstallAppointmentSlotsResponse = objResponse;
            statusCode = 200;
            
        }
        else
        {
            system.debug('@@ Error: '+errString);
            createErrorResponse (appointmenRequestWrapper, errString);            
            statusCode = 200;
        }
    }
    
}