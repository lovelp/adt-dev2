public virtual class NSCPageControllerBase {

  private NSCComponentControllerBase componentController;
	
  public virtual NSCComponentControllerBase getComponentController() {
    return componentController;
  }

  public virtual void setComponentController(NSCComponentControllerBase compController) {
    componentController = compController;
  }
	
  public NSCPageControllerBase getThis() {
    return this;
  }
  
}