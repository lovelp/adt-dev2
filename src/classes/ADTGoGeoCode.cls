/************************************* MODIFICATION LOG ********************************************************************************************
* GoogleGeoCode
* 
* DESCRIPTION : Invokes the Google Geocode API using an HTTPRequest, processes the xml response, 
*               creates GoogleGeoCodeResults structure and handles any errors during processing.  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*               				10/14/2011			- Original Version
*
*													  
*													
*/

public class ADTGoGeoCode {
	
	private static final String googleBaseURL = 'https://maps.googleapis.com';
	private static final String googleGeocodeURL = '/maps/api/geocode/';
	private static final String googleTimeZoneURL = '/maps/api/timezone/';
	private static final String googleOutput = 'json';
	private static final String googleVersion = '3';
	private static final String googleClient = 'gme-adt';
	private static final String googleChannel = 'ADTGo';
	
	private static Boolean useADTLicense = true;
	
	private static DateTime dt;
	
	public class ADTGoGeoCodeRevResults {
		public String status;
		public String address;
        public String city;
        public String state;
        public String streetNumber;
        public String streetName;
		public String zipcode;
	}
	
	public class ADTGoTimeZoneResults {
		public Integer dstOffset;
		public Integer rawOffset;
		public String status;
		public String timeZoneId;
		public String timeZoneName;
		public Integer offset;
	}	
	
	public static ADTGoGeoCodeRevResults GeoCodeLatLon(Decimal lat, Decimal lon)
	{
		HttpRequest req = new HttpRequest();
		req.setEndpoint(getGoogleGeocodeURL(lat, lon));
     	req.setMethod('GET');	
     	System.debug('Google GeoCode Request……: ' + req);
     	Http http = new Http();
	    HTTPResponse res = http.send(req);
	    //system.debug(res.getBody());
	    return parseGeocodeResponse(res.getBody());
	    //system.debug(GGCAddress);
	}
	

	public static ADTGoTimeZoneResults TimeZoneLatLon(Decimal lat, Decimal lon)
	{
		HttpRequest req = new HttpRequest();
		req.setEndpoint(getGoogleTimeZoneURL(lat, lon));
     	req.setMethod('GET');	
     	System.debug('Google TimeZone Request……: ' + req);
     	Http http = new Http();
     	String body;
     	If (!Test.isRunningTest()) {
	    		HTTPResponse res = http.send(req);
	    		body = res.getBody();
     	}
	    	If (Test.isRunningTest()) {
	   		StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ADTGoResponse3' LIMIT 1];
			body = sr.Body.toString();
	    	}
	    //system.debug(res.getBody());
	    return parseTimeZoneResponse(body);
	    //system.debug(GGCAddress);
	}
	
	
	private static String getGoogleGeocodeURL(Decimal lat, Decimal lon)
	{
		String returnURL;
		if(useADTLicense)
		{
			returnURL = googleGeocodeURL + googleOutput + '?' + 'client=' + googleClient + '&sensor=true' + '&channel=' + googleChannel + '&latlng=' + lat + ',' + lon;
			String signature = getGoogleSignature(returnURL); 
			returnURL = googleBaseURL + returnURL + '&signature=' + signature;
		} 
		else
			returnURL = googleBaseURL + googleOutput + '?latlng=' + lat + ',' + lon + '&sensor=true';
			
		return returnURL;
	}
	
	private static String getGoogleTimeZoneURL(Decimal lat, Decimal lon)
	{
		String returnURL;
		dt = DateTime.now();
		Long tm = (dt.getTime())/1000;

		if(useADTLicense)
		{
			returnURL = googleTimeZoneURL + googleOutput + '?' + 'client=' + googleClient + '&sensor=true' + '&channel=' + 
				googleChannel + '&location=' + lat + ',' + lon + '&timestamp=' + String.valueOf(tm);
			String signature = getGoogleSignature(returnURL); 
			returnURL = googleBaseURL + returnURL + '&signature=' + signature;
		} 
		else
			returnURL = googleBaseURL + googleOutput + '?location=' + lat + ',' + lon + '&timestamp=' + tm.format() + '&sensor=true';
			
		return returnURL;
	}
	
	private static String getGoogleSignature(String returnURL)
	{
		String algorithmName = 'HMacSHA1';
		Blob mac = Crypto.generateMac(algorithmName,  Blob.valueOf(returnURL), EncodingUtil.base64Decode('rE5EevLmL7uzpwPOkxpw1RcZCFk='));
		String macUrl = EncodingUtil.base64Encode(mac);//EncodingUtil.urlEncode(EncodingUtil.base64Encode(mac), 'UTF-8');		
		String returnVal = macUrl.replace('+', '-').replace('/', '_');
		return String.valueOf(returnVal); 
	}

	
	public static ADTGoGeoCodeRevResults parseGeocodeResponse(String responseBody)
	{
		ADTGoGeoCodeRevResults result = new ADTGoGeoCodeRevResults();
		Jsonparser jp = JSON.createParser(responseBody);
		String tempValue;
		String tempValueShort;
		while (jp.nextValue() != null){
			if (jp.getCurrentName() == 'status'){
				result.status = jp.getText();
				if (result.status != 'OK'){
					break;
				}
			}
			else if (jp.getCurrentName() == 'formatted_address' && (result.address == null || result.address.length() == 0)){
				result.address = jp.getText();
				//break;
			}
			else if (jp.getCurrentName() == 'long_name'){
				tempValue = jp.getText();
			}
			else if (jp.getCurrentName() == 'short_name'){
				tempValueShort = jp.getText();
			}
			else if (jp.getCurrentName() == 'types') {
				jp.nextValue();
				if (jp.getText() == 'postal_code' && (result.zipcode == null || result.zipcode.length() == 0)) {
					result.zipcode = tempValue;
				}
				if (jp.getText() == 'street_number' && (result.streetNumber == null || result.streetNumber.length() == 0)) {
					result.streetNumber = tempValue;
				}
				if (jp.getText() == 'route' && (result.streetName == null || result.streetName.length() == 0)) {
					result.streetName = tempValue;
				}
				if (jp.getText() == 'locality' && (result.city == null || result.city.length() == 0)) {
					result.city = tempValue;
				}
				if (jp.getText() == 'administrative_area_level_1' && (result.state == null || result.state.length() == 0)) {
					result.state = tempValue;
				}
				//break;
			}
		}		
		system.debug(result);
		return result;
	}
	
	public static ADTGoTimeZoneResults parseTimeZoneResponse(String jsondata)
	{
		ADTGoTimeZoneResults result = (ADTGoTimeZoneResults) JSON.deserialize(jsondata, ADTGoTimeZoneResults.class);
		result.offset = (result.rawOffset/(60*60)) + (result.dstOffset/(60*60));
		
		system.debug(result);
		return result;
	}

}