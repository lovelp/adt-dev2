public class SciQuoteParameters {
    
    public String SalesAgentType   {get;set;}
    public String PreConfigID     {get;set;}
    public String RepName       {get;set;}
    public String RepUserName     {get;set;} //HRM-8351
    public String RepPhone       {get;set;}
    public String RepHrid       {get;set;}
    public String RepEmail       {get;set;}
    public String APMFlag       {get;set;}
    public string Department     {get;set;} //HRM-4732
    public string Restriction     {get;set;} //HRM-8262
    public string SubDepartment {get;set;}
    
    public SciQuoteParameters(ApexPages.StandardController sc){
        SalesAgentType = ApexPages.CurrentPage().getParameters().get('SalesAgentType');
        PreConfigID = ApexPages.CurrentPage().getParameters().get('PreConfigID');
        RepName = ApexPages.CurrentPage().getParameters().get('RepName');
        RepUserName = ApexPages.CurrentPage().getParameters().get('RepUserName');   //HRM-8351
        RepPhone = ApexPages.CurrentPage().getParameters().get('RepPhone');
        RepHrid =  ApexPages.CurrentPage().getParameters().get('RepHRID');
        RepEmail = ApexPages.CurrentPage().getParameters().get('RepEmail');
        APMFlag = ApexPages.currentPage().getParameters().get('APM');
        Department = ApexPages.currentPage().getParameters().get('Department');    //HRM-4732
        Restriction = ApexPages.currentPage().getParameters().get('Restriction');  //HRM-8262
        SubDepartment = ApexPages.currentPage().getParameters().get('SubDepartment');//HRM-10101
    }
}