@isTest(SeeAllData=false)
public class UserAppointmentCountTest {
    
     public static Event eve;
     public static Account a;
    
    static void createTestData(){
        TestHelperClass.createReferenceDataForTestClasses();
        
        List<user> users=new List<user>();
        
        Profile p = [select id from profile where name='System Administrator'];
        integer i= 23547875;
        User integrationUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='TestIntegration', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingu'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(integrationUser);
        
        i= 7987340;
        User NewRehashUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='NewRehash', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='unitTestSalesRepu@testorg.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(NewRehashUser);
        i= 2343579;
        User ResaleAdmin=new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='ADMIN',firstname='TestGlobal', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingu'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(ResaleAdmin);
        
        INSERT users;
        
       a=testHelperClass.createAccountData();
        
        
        eve=testHelperClass.createEvent(a,testHelperClass.createUser(32423402));
        eve.TimeZoneId__c ='America/Los_Angeles';
        contact c=new contact();
        c.firstname='test';
        c.lastname='test';
        c.accountid=a.id;
        insert c;
        eve.whoid=c.id;
        update eve;
  
    }
 
    static testmethod void testUserAppointmentCount(){
        createTestData();
        User u;
        User rep;
        Event e;
        Territory__c t;
        TerritoryAssignment__c ta;
        Address__c location, loc2;
        Id p;
        User current = [select Id from User where Id=:UserInfo.getUserId()];
        User nscUser = [select id from User where profile.name ='ADT NSC Sales Representative' and isActive= true limit 1];
        System.debug('@@@@@nscuser'+nscUser);
        //rep = [select id from User where profile.name ='ADT NA Sales Representative' and isActive= true limit 1];
        system.runas(current) {
            u = TestHelperClass.createUser(0);
            rep = TestHelperClass.createSalesRepUser();   
            System.debug('@@@rep'+rep);
            p = a.PostalCodeID__c;
            Postal_Codes__c pc = [select TMTownID__c,TMSubTownID__c from Postal_Codes__c where id =:p];
            System.debug('@@@@PC'+pc);
            pc.TMTownID__c ='121'; 
            update pc;
            e = TestHelperClass.createEvent(a, rep);
            e.Appointment_Type__c = 'New';
            e.RecordTypeId = [SELECT id, name,Developername,sobjecttype FROM RecordType WHERE sobjectType ='Event' and RecordType.Developername ='SelfGeneratedAppointment'].id;
            System.debug('@@@@@ID'+e.RecordTypeId);
            Update e;
            System.debug('@@@event using helper class'+e.id);
            System.debug('@@@Event user'+e.OwnerId);
            //t = TestHelperClass.createTerritory();
            //ta = TestHelperClass.createTerritoryAssignment(t,rep,pc);
            Territory2Type tt = new Territory2Type();
            tt = [select id from Territory2Type limit 1];
            Territory2Model tm = new Territory2Model();
            tm = [Select id,developername from Territory2Model limit 1];
            //Id=0MIc00000004lKbGAI, Name=SMB Town:121, ParentTerritory2Id=0MIc00000004k0WGAQ, Territory2ModelId=0MAc00000004CNKGA2, Territory2TypeId=0M5c0000000CaYNCA0
            Territory2 ter2 = new Territory2();
            ter2.Name = 'New SMB Town:121';
            ter2.Territory2ModelId = tm.id;
            ter2.Territory2TypeId = tt.id;
            ter2.DeveloperName = tm.DeveloperName;
            //ter2.ParentTerritory2Id =ta.id;
            insert ter2;
            System.debug('@@@@@@Ter2'+ter2.id);
            SchedUserTerr__c su = new SchedUserTerr__c();
            su.Order_Types__c = 'New';
            su.Quota__c = 100;
           // su.Territory__c = t.id;
            su.Territory_Association_ID__c= ter2.id;
            su.Name ='New SMB Town:121';
            su.Friday_Available_Start__c = 800;
            su.Friday_Available_End__c = 2300;
            su.Manager_Weighting__c = 0;
            su.Monday_Available_Start__c = 800;
            su.Monday_Available_Start__c = 2300;
            su.User__c = rep.Id;
            su.Tuesday_Available_Start__c = 800;
            su.Tuesday_Available_End__c =2300;
            su.Wednesday_Available_End__c =800;
            su.Wednesday_Available_Start__c = 2300;
            su.Thursday_Available_Start__c = 800;
            su.Thursday_Available_End__c = 2300;
            su.BusinessId__c = '1100';
            su.TMTownID__c = '121';
            insert su;
            
            System.debug('@@@@Su'+su.USer__c + '@@@@@Suterr'+su.Territory_Association_ID__c);
            TestHelperClass.setupTestIntegrationSettings(u);
         
            test.startTest();
                system.runas(nscUser) {
                    //UserAppointmentCount uc = new UserAppointmentCount();
                    UserAppointmentCount.setAppointmentUserAggregate(e.id,true);
                    Disposition__c d = new Disposition__c();
                    Insert d;
            test.stopTest();
                    
            }    
        }   
    }
}