/************************************* MODIFICATION LOG ********************************************************************************************
* 
* Name: LoanApplicationHistoryTriggerHelper
*
* DESCRIPTION : To add custom tracking of Loan Application (History)
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                                      DATE                        Ticket              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Siju Varghese                             06/15/2019      

********************************************************************************************************************************************************/

public class LoanApplicationHistoryTriggerHelper {
    public static void insertHistory(Map<Id, LoanApplication__c> newLoanApp, Map<Id, LoanApplication__c> oldLoanApp){
        try{ 
            //Loan Application List to store new values
            list<LoanApplication__c> newLoanAppList = new list<LoanApplication__c>();
            if(!newLoanApp.isEmpty()){
                newLoanAppList = newLoanApp.values();
            }
            list<ADTHistory__c> ADTHistoryList = new list<ADTHistory__c>();
            if(newLoanAppList.size() > 0){
                //Custom metadata to get fields to be tracked 
                List<ADT_Tracking__mdt> fieldlist = new list<ADT_Tracking__mdt>();
                fieldlist = [SELECT DeveloperName, Fields__c, Label FROM ADT_Tracking__mdt WHERE Label = 'LoanApplication__c'];
                if(fieldlist.size() > 0 && fieldlist[0] != null && String.isNotBlank(fieldlist[0].Fields__c)){
                    list<String> trackFieldsList = new list<String>();
                    trackFieldsList = fieldlist[0].Fields__c.split(',');
                    String objectName = fieldlist[0].Label;
                    
                    if(trackFieldsList.size() > 0){
                        //Schema Declaration to retrieve field labels
                        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
                        Schema.SObjectType loanApplicationSchema = schemaMap.get(objectName);
                        Map<String, Schema.SObjectField> fieldMap = loanApplicationSchema.getDescribe().fields.getMap();
                        for(LoanApplication__c loanApp: newLoanAppList){
                            LoanApplication__c oldLoanRec = oldLoanApp.get(loanApp.Id);
                            String changeStr = '';
                            String FldLabel;
                            String FromValue;
                            String toValue;
                            for(String fieldName: trackFieldsList){
                                fieldName = fieldName.trim();
                                changeStr = '';
                                if (oldLoanApp.get(loanApp.id).get(fieldName) != newLoanApp.get(loanApp.id).get(fieldName)){
                                    FldLabel = fieldMap.get(fieldName).getDescribe().getLabel();
                                    FromValue =  String.valueOf (oldLoanApp.get(loanApp.id).get(fieldName) == null?'Blank':oldLoanApp.get(loanApp.id).get(fieldName));
                                    toValue =  String.valueOf(newLoanApp.get(loanApp.id).get(fieldName) == null?'Blank':newLoanApp.get(loanApp.id).get(fieldName));
                                    changeStr = FldLabel + ' Changed from ' + FromValue + ' to ' + toValue ;

                                    // Create a new record for ADT History
                                    ADTHistory__c ADTHistory = new ADTHistory__c();
                                    ADTHistory.object__c = objectName;
                                    ADTHistory.Label__c = FldLabel;
                                    ADTHistory.RecordId__c = loanApp.id;
                                    ADTHistory.LoanApplication__c = loanApp.id;
                                    DateTime nowDT = System.now();
                                    ADTHistory.Description__c = changeStr;
                                    ADTHistory.Timestamp__c = DateTime.now();
                                    ADTHistory.User__c = UserInfo.getUserId();
                                    ADTHistory.NewValue__c = FromValue;
                                    ADTHistory.OldValue__c = toValue;
                                    ADTHistoryList.add(ADTHistory);
                                }
                            }
                        }
                    }
                }
            }
            System.debug('The list is'+ADTHistoryList);
            //Insert List if not empty
            if(ADTHistoryList.size() > 0){
                insert ADTHistoryList;
            } 
        }catch(exception e){
            ADTApplicationMonitor.log(e,'LoanApplicationHistoryTriggerHelper','insertHistory');
        }
    }
}