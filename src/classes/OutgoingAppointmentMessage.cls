/************************************* MODIFICATION LOG ********************************************************************************************
* OutgoingAppointmentMessage
*
* DESCRIPTION : Creates and processes setAppointment messages for Telemar Interface
*				Extends OutgoingMessage with message specific logic
*               Delegates to ADTSalesMobilityFacade (wsdl2apex code)
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					10/8/2012				- Original Version
* Mike Tuckman (001)			11/1/2013				- Support for Devcon and other extentions to Transaction Type (-xxx)
* Mike Tuckman (002)			02/22/2014				- (Hermes) Support for validated addresses
* Giribabu Geda					06/27/2018				- HRM-7313 Amplifinity feeding wrong leadsource to MMB	
* Siju Varghese                 1/04/2019               - HRM-8851 Canada Leads in Telemar are with 3 digit postal code
*/

public with sharing class OutgoingAppointmentMessage extends OutgoingMessage { 
	
	public Lead l {get;set;}
	public Account a {get;set;}
	public Event e {get;set;} 
	
	private ADTSalesMobilityFacade.setAppointmentRequest_element request;
	private ADTSalesMobilityFacade.setAppointmentResponse_element response;
	private ADTSalesMobilityFacade.PreSale_element presale;
	private ADTSalesMobilityFacade.Appointment_element appt;
	
	public String TelemarAccountNumber {get {return tan;}}
	public String TaskCode {get {return tCode;}}
	public String Dnis;
	public String Promotion;
	
	private String tan;
	private String tCode;
	
	public OutgoingAppointmentMessage()
	{
		this.ServiceName = 'setAppointment';
	}
	
	public override String createRequest()
	{
		request = new ADTSalesMobilityFacade.setAppointmentRequest_element();
		
		User userPerformingAction = [Select Id, EmployeeNumber__c, TimeZoneSidKey from User where ID = :UserInfo.getUserId()];
		
		User userBeingScheduled;
		String timeZoneOffset = '';
		if (userPerformingAction != null && userPerformingAction.Id == e.OwnerId) {
			userBeingScheduled = userPerformingAction;
			// get the current user's offset as of the time of the event
			// this handles time changes between daylight savings and standard time
			timeZoneOffset = e.StartDateTime.format('Z');
		} else {
			userBeingScheduled = [Select Id, EmployeeNumber__c, TimeZoneSidKey from User where ID = :e.OwnerId];

			if (userBeingScheduled.TimeZoneSidKey == userPerformingAction.TimeZoneSidKey) {
				// no difference in time zone so get the current user's offset as of the time of the event
				// this handles time changes between daylight savings and standard time
				timeZoneOffset = e.StartDateTime.format('Z');
			} else {
				// otherwise, use the user's current time zone identifier and the scheduled date to derive the offset
				timeZoneOffset = EventUtilities.deriveUserTimeZoneOffsetForDate(e.StartDateTime, userBeingScheduled.TimeZoneSidKey);
			}
			
		}
		
		
		request.MessageID = getMessageId(userPerformingAction.Id);
		request.TransactionType = this.TransactionType;
        
		// intentionally using the formula field OutboundTelemarAccountNumber__c rather than the TelemarAccountNumber__c
		// since the schedule action may be against a former RIF account that has a RifTelemarAccountNumber__c populated
		if (a != null && a.OutboundTelemarAccountNumber__c != null) {
			request.TelemarAccountNumber = a.OutboundTelemarAccountNumber__c;
		}
		else if (a != null && a.TelemarAccountNumber__c != null) {
			request.TelemarAccountNumber = a.TelemarAccountNumber__c;
		}
		else if (l != null && l.TelemarAccountNumber__c != null) {
			request.TelemarAccountNumber = l.TelemarAccountNumber__c;
		}
		request.HREmployeeID = (userPerformingAction.EmployeeNumber__c == null ? '' : userPerformingAction.EmployeeNumber__c);
		
		// Here we want to also check to see if the Lead source it's Builder, if it is we will set the LeadSource to Builder
		// in order to eventually sent it to Telemar.
		if (TransactionType != IntegrationConstants.TRANSACTION_CANCEL){		
			if (a != null) {
				request.LeadSource = a.Data_Source__c;
				if (a.Affiliation__c == 'Builder'){
					request.LeadSource = 'Builder';
				}
			} else if (l != null) {
				if (l.Affiliation__c == 'Builder'){
	                     request.LeadSource = 'Builder';
	                } else{
							request.LeadSource = l.LeadSource;
				}
			}
		}
		
		//if (TransactionType == IntegrationConstants.TRANSACTION_NEW) - 001
		if (TransactionType.startsWith(IntegrationConstants.TRANSACTION_NEW))
		{
			if (a != null) {
				buildPresaleFromAccount(a);
			} else if (l != null) {
				buildPresaleFromLead(l, TransactionType);
			}
		}
		else
		{
			presale = null;
		}
		
		request.PreSale = presale;
		
		appt = new ADTSalesMobilityFacade.Appointment_element();
		
		appt.OriginalTelemarScheduleID = (e.ScheduleID__c == null ? '' : e.ScheduleID__c);
		
		if (TransactionType != IntegrationConstants.TRANSACTION_CANCEL)
		{
			appt.AppointmentStartDateTime = e.StartDateTime;
			appt.AppointmentOffsetToGMT = timeZoneOffset;
			Double minutes = (e.EndDateTime.getTime() - e.StartDateTime.getTime()) / 1000 / 60;
			appt.Duration = String.valueof((Integer)minutes);
			appt.TaskCode = (e.TaskCode__c == null ? '' : e.TaskCode__c);
			appt.Notes = truncateTextArea(e.Description, 300);
			appt.HREmployeeID = (userBeingScheduled.EmployeeNumber__c == null ? '' : userBeingScheduled.EmployeeNumber__c);
		}
		request.Appointment = appt;
		//HRM-7313 && HRM-7272 Setting a default DNIS for amplifinity when a lead is converted and send the value in the request to Telemar.
		user u=[select id,rep_team__c from user where id= :UserInfo.getUserId()];
		if(String.isnotblank(u.Rep_Team__c) && u.Rep_Team__c.containsIgnoreCase('FIELDSALES')){
    		Map<String,String> partnersourcingMdtMap = new Map<String,String>();
            for(Partner_Sourcing__mdt partnerMDT: [Select DeveloperName,LeadSource__c,PartnerDNIS__c from Partner_Sourcing__mdt]){
                if(String.isNotBlank(partnerMDT.LeadSource__c))
                    partnersourcingMdtMap.put(partnerMDT.LeadSource__c,partnerMDT.PartnerDNIS__c);
            } 
		
			if(a!=null && String.isNotBlank(a.DNIS__C)){
				request.Dnis = a.DNIS__c;
			}else if(l!= null && String.isNotBlank(l.leadsource) && partnersourcingMdtMap!= null && partnersourcingMdtMap.containsKey(l.leadsource)){
				request.Dnis = partnersourcingMdtMap.get(l.leadsource);
			}
			else{
				request.Dnis = partnersourcingMdtMap.get('Field_Default_DNIS');
			}
		}else{
			request.Dnis = Dnis;
		}
		request.Promotion = Promotion;
		
		return system.Json.serialize(request);
	}
	
	public override void processResponse()
	{
		if (request == null && this.ServiceMessage != null) {
			request = (ADTSalesMobilityFacade.setAppointmentRequest_element)system.Json.deserialize(this.ServiceMessage, system.type.forName('ADTSalesMobilityFacade.setAppointmentRequest_element'));
		}
		else if (request == null) {
			createRequest();
		}
			
		system.debug(' @@ ==> Sending Request...'+request);	
		ADTSalesMobilityFacade.SalesMobilitySOAP soap = new ADTSalesMobilityFacade.SalesMobilitySOAP();
		response = soap.setAppointment(
			request.MessageID,  
			request.TransactionType, 
			request.TelemarAccountNumber, 
			request.HREmployeeID,
			request.LeadSource, 
			request.PreSale, 
			request.Appointment,
			request.Dnis,
			request.Promotion
		);
		
		mid = response.MessageID;
		mstatus = response.MessageStatus;
		err = response.Error;
		sid = response.TelemarScheduleID;
		tan = response.TelemarAccountNumber;
		tCode = response.TaskCode;
		
		System.debug('$$$ mid: ' + mid);
		System.debug('$$$ mstatus: ' + mstatus);
		System.debug('$$$ err: ' + err);
		System.debug('$$$ sid: ' + sid);
		System.debug('$$$ tan: ' + tan);
		System.debug('$$$ taskCode: ' + tCode);
		
		
		if (mstatus != IntegrationConstants.MESSAGE_STATUS_OK && mstatus != IntegrationConstants.MESSAGE_STATUS_FAIL) {
			throw new IntegrationException('Invalid response from Telemar: MessageStatus of ' + mstatus + ' is invalid');
		}
		
		if (mstatus == IntegrationConstants.MESSAGE_STATUS_FAIL) {
			throw new IntegrationException (err);
		}
		
	}
	
	private void buildPresaleFromLead(Lead l, String TransactionType) {
		presale = new ADTSalesMobilityFacade.PreSale_element();
		presale.FirstName = truncateFirstName(l.FirstName);
		presale.LastName = truncateLastName(l.LastName);
		if (l.Business_Id__c != null && l.Business_Id__c.contains('1200')) {
			presale.BusinessName = truncateBusinessName(l.Company);
		}
		
		//each address line is 30 chars in Telemar
		//divide first 120 chars of Site street into 4 address lines
		String[] alines = getAddressLines(l.SiteStreet__c);
		
		presale.AddressLine1 = alines[0];
		presale.AddressLine2 = alines[1];
		presale.AddressLine3 = alines[2];
		presale.AddressLine4 = alines[3];
		
		if (l.SiteValidated__c) {
			presale.AddressLine4 = '*MDO';
			presale.AddressLine2 = l.SiteStreet2__c;
		}
		
		presale.City = truncateSiteCity(l.SiteCity__c);
		presale.State = l.SiteStateProvince__c;
		presale.PostalCode = truncatePostalCode(l.SitePostalCode__c, l.SitePostalCodeAddOn__c);
		presale.CountryCode = l.SiteCountryCode__c;
		presale.PhoneNumber1 = truncatePhone(l.Phone);
		presale.Email = l.Email;
		presale.Comments = null;
		presale.QueriedSource = null;
		if( TransactionType !=null && TransactionType.endsWith('-NSC') ){
			presale.QueriedSource = l.QueriedSource__c;
		}		
		presale.ReferredBy = TextUtilities.removeDiacriticalMarks(l.ReferredBy__c);
		presale.Channel = l.Channel__c;
		
	}
	
	private void buildPresaleFromAccount(Account a) {
		presale = new ADTSalesMobilityFacade.PreSale_element();
		presale.FirstName = truncateFirstName(a.FirstName__c);
		presale.LastName = truncateLastName(a.LastName__c);
		if ((presale.FirstName == null || presale.FirstName.length() == 0) && 
			(presale.LastName == null || presale.LastName.length() == 0) &&
			(a.RifID__c != null && a.RifID__c.length() > 0)) {
			List<String> names;
			names = a.Name.split(',',2);
			if (names.size() == 2){
				presale.FirstName = truncateFirstName(names[1]);
				presale.LastName = truncateLastName(names[0].replace(',', ' '));
			}
			else {
				names = a.Name.split(' ',2);
				if (names.size() == 2){
					presale.FirstName = truncateFirstName(names[0]);
					presale.LastName = truncateLastName(names[1]);
				} 
				else {
					presale.LastName = truncateLastName(a.Name);					
				}
			}
		}
		if (a.Business_Id__c != null && a.Business_Id__c.contains('1200')) {
			presale.BusinessName = truncateBusinessName(a.Name);
		}
		
		//each address line is 30 chars in Telemar
		//divide first 120 chars of Site street into 4 address lines
		String[] alines = getAddressLines(a.SiteStreet__c);
		
		presale.AddressLine1 = alines[0];
		presale.AddressLine2 = alines[1];
		presale.AddressLine3 = alines[2];
		presale.AddressLine4 = alines[3];
		
		if (a.SiteValidated__c) {
			presale.AddressLine4 = '*MDO';
			presale.AddressLine2 = a.SiteStreet2__c;
		}
		
		presale.City = truncateSiteCity(a.SiteCity__c);
		presale.State = a.SiteState__c;
		presale.PostalCode = truncatePostalCode(a.SitePostalCode__c, a.SitePostalCodeAddOn__c);
		try{
		//Added Siju - http://jira.adt.com:8080/browse/HRM-8851
		if(((String.isNotBlank(a.ShippingCountry) && a.ShippingCountry.equalsIgnoreCase('CA')) || (String.isNotBlank(a.SiteCountryCode__c) && a.SiteCountryCode__c.equalsIgnoreCase('CA'))) && a.Data_Source__c.equalsIgnoreCase('WEB') && string.isNotBlank(a.CompletePostalCodeWeblead__c))
          presale.PostalCode = a.CompletePostalCodeWeblead__c;
		}
		catch(Exception ae){}
		    
		presale.CountryCode = a.SiteCountryCode__c;
		presale.PhoneNumber1 = truncatePhone(a.Phone);
		presale.Email = a.Email__c;
		presale.Comments = null;
		presale.QueriedSource = a.QueriedSource__c;
		presale.ReferredBy = TextUtilities.removeDiacriticalMarks(a.ReferredBy__c);
		presale.Channel = a.Channel__c;
	}
}