/************************************* MODIFICATION LOG ********************************************************************************************
* GoogleMapsTimeZone
* 
* DESCRIPTION : The Google Maps Time Zone API provides a simple interface to request the time zone for a location on the earth, 
				as well as that location's time offset from UTC..  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera              2/15/2016			- Original Version
*
*													  
*													
*/


public without sharing class GoogleMapsTimeZone {
	
	private static final String googleBaseURL = 'https://maps.googleapis.com/maps/api/timezone/';
	private static final String googleOutput = 'json';
	private static final String googleClient = 'gme-adt';
	private static final String googleChannel = 'Resale';
	private static Map<ID, GoogleTZResponse> mapGTZ;
	
	public static String TimeZoneURL; 
	
	public class GoogleMapsTimeZoneException extends Exception{}
	
	public class GoogleTZResponse {
	    public integer dstOffset { get; set; }
	    public integer rawOffset { get; set; }
	    public string status { get; set; }
	    public string timeZoneId { get; set; }
	    public string timeZoneName { get; set; }
	    public String error_message {get;set;}
	    public GoogleTZResponse () {
	    		dstOffset = 0;
	    		rawOffset = -18000;
	    		status = 'OK';
	    		timeZoneId = 'America/New_York';
	    		timeZoneName = 'Eastern Standard Time';
	    }
	}

	public static GoogleTZResponse getAddressTimeZone( Address__c addr){
		try{
			if (mapGTZ == null) mapGTZ = new Map<Id,GoogleTZResponse>();
			if (mapGTZ.containsKey(addr.ID)){
				system.debug('Found GTZ Match');
				return mapGTZ.get(addr.ID);
			}
			if (addr != null && (addr.Latitude__c == null || addr.Longitude__c == null)) {
				MelissaDataAPI.MelissaDataResults mdr = MelissaDataAPI.webValidateAddrObj(addr.id);
				system.debug('** Called MD :' + mdr);
				addr.Latitude__c = Decimal.valueOf(mdr.lat);
				addr.Longitude__c = Decimal.valueOf(mdr.lon);
			}
			HttpRequest req = new HttpRequest();
			system.debug('Addr Lat Lon ' + addr.Latitude__c + ':' + addr.Longitude__c);
			req.setEndpoint(getTimeZoneURL( addr.Latitude__c, addr.Longitude__c));
	     	req.setMethod('GET');	
	     	System.debug('Google Timezone Request……: ' + req);
	     	Http http = new Http();
		    HTTPResponse response = http.send(req);
		    String ResponseBody = response.getBody();
	        if (!(  response.getStatusCode() == 200 || response.getStatusCode() == 201 || response.getStatusCode() == 202 )) {
	            System.debug('[GoogleDistanceMatrix]::[getDistanceMatrix] Error Response Status:'+ response.getStatus() + ' Code:' + response.getStatusCode());
	            throw new IntegrationException(' StatusCode='+response.getStatusCode()+' Status='+response.getStatus() + ' Message='+ResponseBody);        
	        }	    
		    system.debug( ResponseBody );
		    GoogleTZResponse gtzr = parseJSONResponse( ResponseBody );
		    if (gtzr != null) {
		    		system.debug('Storing GTZ :' + addr.ID);
		    		mapGTZ.put(addr.ID, gtzr);
		    }
		    return  gtzr ;
		}
	    catch(Exception e) {
	    		return new GoogleTZResponse();
	    }
	    //system.debug(GGCAddress);
	}
	
	private static String getTimeZoneURL(decimal lat, decimal lon){
		String returnURL = '';
		TimeZoneURL = 'location='+lat+','+lon+'&timestamp='+(DateTime.now().getTime()/1000);
		String signature = getGoogleSignature(); 
		returnURL = googleBaseURL + googleOutput + '?' + TimeZoneURL + '&client=' + googleClient + '&channel=' + googleChannel + '&signature=' + signature;
		return returnURL;
	}

	private static String getGoogleSignature(){
		String BaseURL = '/maps/api/timezone/';
		String returnURL = BaseURL + googleOutput + '?' + TimeZoneURL + '&client=' + googleClient + '&channel=' + googleChannel;
		String urlEncodedURL = EncodingUtil.urlEncode(returnURL, 'UTF-8');
		String algorithmName = 'HMacSHA1';
		//Blob decodedKey = EncodingUtil.base64Decode('rE5EevLmL7uzpwPOkxpw1RcZCFk=');
		Blob decodedKey = EncodingUtil.base64Decode( ResaleGlobalVariables__c.getInstance('GoogleGeoCodeDecodeKey').value__c );
		Blob mac = Crypto.generateMac(algorithmName,  Blob.valueOf(returnURL), EncodingUtil.base64Decode('rE5EevLmL7uzpwPOkxpw1RcZCFk='));
		String macUrl = EncodingUtil.base64Encode(mac);//EncodingUtil.urlEncode(EncodingUtil.base64Encode(mac), 'UTF-8');		
		string returnVal = macUrl.replace('+', '-').replace('/', '_');
		return String.valueOf(returnVal); 
	}
	
	private static GoogleTZResponse parseJSONResponse( string JSONResponse){
		// Parse JSON response
		system.debug('parsing...'+JSONResponse);		
		GoogleTZResponse resTZ = (GoogleTZResponse) JSON.deserialize(JSONResponse, GoogleTZResponse.class);
		if( resTZ.status != 'OK' ){
			throw new GoogleMapsTimeZoneException('['+resTZ.status+'] '+resTZ.error_message);
		}	
		return 	resTZ;
	}

}