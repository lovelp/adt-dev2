@isTest
private class TilaPnoteControllerTest {
    
    private static LoanApplication__c LoanApplication;
    private static Account acct;
    private static void createTestData(){
    
    User current = [Select Id, Department from User where Id = :UserInfo.getUserId()];
    	list<FlexFiConfig__c> flexFIs = new list<FlexFiConfig__c>();
        
        FlexFiConfig__c fc = new FlexFiConfig__c();
        flexFIs.add(new FlexFiConfig__c(name='LoanType', value__c='test'));
        
        fc = new FlexFiConfig__c();
        fc.name='Phone Sales Department';
        fc.value__c = current.Department != null ? current.Department : 'test';
        flexFIs.add(fc);
        
        flexFIs.add(new FlexFiConfig__c(name='TransactionType', value__c='test'));
        flexFIs.add(new FlexFiConfig__c(name='autoBookFlg', value__c='test'));
        
        FlexFiConfig__c flf = new FlexFiConfig__c();
        flf.name = 'TilePnoteFailedAttempts';
        flf.value__c = '2';
        flexFIs.add(flf);
        
        insert flexFIs;
        
        list<FlexFiMessaging__c> flexMsgs = new list<FlexFiMessaging__c>();
        
        FlexFiMessaging__c msg = new FlexFiMessaging__c(Name = 'LoanSubmissionFailed', ErrorMessage__c = 'Application Submission Failed');
        flexMsgs.add(msg);
        
        FlexFiMessaging__c msg1 = new FlexFiMessaging__c(Name = 'LoanApplicationSubmitted', ErrorMessage__c = 'Application Submitted');
        flexMsgs.add( msg1);
        
        FlexFiMessaging__c msg2 = new FlexFiMessaging__c(Name = 'APPROVED:a', ErrorMessage__c = 'Application Approved');
        flexMsgs.add( msg2);
        
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'DecessionApplicationSubmitted', ErrorMessage__c = 'Loan Decision Application Submitted');
        flexMsgs.add( msg3);
        
        FlexFiMessaging__c msg4 = new FlexFiMessaging__c(Name = 'DecessionApplicationSubmittedAPPROVED', ErrorMessage__c = 'Loan Application Submitted & Approved');
        flexMsgs.add( msg4);
        
        insert flexMsgs;
        
        //Create Account
        //User u; 
        
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '4';
        insert resale;
        ResaleGlobalVariables__c InteUser = new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User');
        insert InteUser;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name='RIFAccountOwnerAlias'; 
        rgv.value__c='ADT RIF';
        insert rgv;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.CountryCode__c = 'US';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        acct = new Account(Name = 'Unit Test Account 2');
        acct.AddressID__c = addr.Id;
        
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';

          acct.FirstName__c='testFirstName';
          acct.LastName__c='testLastName';
          acct.Email__c = 'abc@gmail.com';
          acct.DOB_encrypted__c='11/11/1973';
          acct.Income__c = '1000';
          acct.Phone = '14785894';
          insert acct;
          
          Equifax_Log__c elog = new Equifax_Log__c(AccountID__c = acct.Id);
          elog.TransactionID__c = 'T001';
          insert elog;

        LoanApplication = new LoanApplication__c();
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c =DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.PreviousAddressLine1__c ='Prev Add Line 1';
        LoanApplication.PreviousAddressLine2__c ='Prev Add Line 2';
        LoanApplication.PreviousAddressPostalCd__c ='27518';
        LoanApplication.PreviousAddressStateCd__c ='NC';
        LoanApplication.PreviousAddressCountryCd__c='USA';
        //LoanApplication.SubmitLoanAccepted__c=true;
        LoanApplication.PreviousAddressLine1__c = '8952 Brook Rd';
        LoanApplication.PreviousAddressCity__c = 'McLean';
        LoanApplication.PreviousAddressStateCd__c = 'VA';
        LoanApplication.PreviousAddressPostalCd__c = '221o2';
        LoanApplication.PreviousAddressCountryCd__c = 'County123';
        LoanApplication.eConsentAcceptedDateTime__c = system.now();
        LoanApplication.Account__c = acct.id;
        LoanApplication.PaymentechProfileId__c = '123';
        LoanApplication.FirstName__c='testFirstName';
        LoanApplication.LastName__c='testLastName';
        LoanApplication.DOB__c='11/11/1973';
        LoanApplication.SSN__c='123456789';
        LoanApplication.AnnualIncome__c=1000;
        LoanApplication.BillingAddress__c =addr.id;
        LoanApplication.ShippingAddress__c =addr.id;
        insert LoanApplication;
    }

	private static testMethod void test1() {
	    createTestData();
	    system.debug('testdata'+LoanApplication);
	    Test.startTest();
	    ApexPages.currentPage().getParameters().put('id', String.valueOf(LoanApplication.Id));
	    TilaPnoteController tilPnotCont = new TilaPnoteController();
	    TilaPnoteController.displayAgreement('1234','10/10/1989',LoanApplication.id);
	    TilaPnoteController.eConsentAccepted(LoanApplication.id);
	    TilaPnoteController.termsAndConditionsOnly(LoanApplication.id);
	    TilaPnoteController.acceptAllTerms(LoanApplication.id);
	    TilaPnoteController.recurringPayment(LoanApplication.id);
	    
	    TilaPnoteController.displayAgreement('6789','11/11/1973',LoanApplication.id);
	    
	    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(LoanApplication);
	    TilaPnoteController tilPnotCont1 = new TilaPnoteController(sc);
	    Test.stopTest();
	}
    private static testMethod void test2() {
	    createTestData();
	    system.debug('testdata' + LoanApplication);
	    Test.startTest();
	    ApexPages.currentPage().getParameters().put('id', String.valueOf(LoanApplication.Id));
	    TilaPnoteController tilPnotCont = new TilaPnoteController();
	    TilaPnoteController.displayAgreement('1234','10.10.1989',LoanApplication.id);
	    Test.stopTest();
    }

}