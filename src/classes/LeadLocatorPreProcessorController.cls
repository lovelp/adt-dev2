/************************************* MODIFICATION LOG ********************************************************************************************
* LeadLocatorPreProcessorController
* 
DESCRIPTION : The Locator tool can be accessed multiple ways (link from an Account, link from a Lead, user entered address,
*             Locator tab, current location of the device) so this class determines appropriate latitude and longitude query string
*             parameters then forwards to the Locator page for subsequent processing.
*             It is a Controller in the SFDC MVC architecture. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*               				10/14/2011			- Original Version
*
* Shivkant Vadlamani			03/23/2012			- Incorporated Phase 2 changes including support for Leads 
*													  
*													
*/
public class LeadLocatorPreProcessorController 
{
	// holds the account Id supplied as a query string parameter
    public String mAid {get;set;}
    public Boolean isError {get;set;}
    
	public PageReference init()
    { 
    	String acctprefix = Schema.Sobjecttype.Account.getKeyPrefix();
		String leadprefix = Schema.Sobjecttype.Lead.getKeyPrefix();
    	
    	// look for query string parameters
        mAid = ApexPages.currentPage().getParameters().get('aid');    
        isError = false;
        
        Account a;
		Lead l;	   
         
        if (mAid != 'null' && mAid != null && mAid != '')
       	{
        	if (mAid.toLowerCase().startsWith(acctprefix.toLowerCase()))
			{
				// the start point is the location of the identified account
	    		a = SearchManager.getAccount(mAid); 
			}						
			else if(mAid.toLowerCase().startsWith(leadprefix.toLowerCase()))
			{
				// the start point is the location of the identified account
	    		l = SearchManager.getLead(mAid); 
			} 
            GoogleGeoCodeResults result;
            PageReference pageRef = new PageReference('/apex/LeadLocator');
            
            if(a != null && a.Latitude__c != null && a.Longitude__c != null && a.Latitude__c != 0 && a.Longitude__c != 0)
            {            	
	            pageRef.getParameters().put('lat', String.valueOf(a.Latitude__c)); 
	            pageRef.getParameters().put('lon', String.valueOf(a.Longitude__c));             
	            return pageRef;            	
            } 
            else if(l != null && l.Latitude__c != null && l.Longitude__c != null && l.Latitude__c != 0 && l.Longitude__c != 0)
            {            	
	            pageRef.getParameters().put('lat', String.valueOf(l.Latitude__c)); 
	            pageRef.getParameters().put('lon', String.valueOf(l.Longitude__c));         
	            return pageRef;            	
            }                 
          	else if(a != null && (a.Latitude__c == null || a.Longitude__c == null || (a.Latitude__c == 0 && a.Longitude__c == 0)))
           	{
           		if(a.SiteStreet__c != null && a.SiteCity__c != null && a.SiteCountryCode__c != null && a.SitePostalCode__c != null)
           		{
           			result = GoogleGeoCode.GeoCodeAddress(a.SiteStreet__c + ' ,' + a.SiteCity__c + ' ,' + a.SiteCountryCode__c + ' ,' + a.SitePostalCode__c);            	
           		}
            	
            	if(result!= null && result.AddressLat != null && result.AddressLon != null && result.AddressLat != '' && result.AddressLon != '')  
	            {  
	            	Address__c addr = [SELECT Id, Name, Latitude__c, Longitude__c 
	            							  FROM Address__c
	            							  WHERE Id = :a.AddressID__c];
	            	
	            	if(addr != null)
	            	{
	            		addr.Latitude__c = Decimal.valueOf(result.AddressLat); 
		            	addr.Longitude__c = Decimal.valueOf(result.AddressLon);    
		            	update addr;
	            	}	          	
	            	      	
		            pageRef.getParameters().put('lat', result.AddressLat); 
		            pageRef.getParameters().put('lon', result.AddressLon);            
		            return pageRef;
	            } 
            }
            else if(l != null && (l.Latitude__c == null || l.Longitude__c == null || (l.Latitude__c == 0 && l.Longitude__c == 0)))
            {
            	if(l.SiteStreet__c != null && l.SiteCity__c != null && l.SiteCountryCode__c != null && l.SitePostalCode__c != null)
           		{
            		result = GoogleGeoCode.GeoCodeAddress(l.SiteStreet__c + ' ,' + l.SiteCity__c + ' ,' + l.SiteCountryCode__c + ' ,' + l.SitePostalCode__c);
           		}
            	if(result!= null && result.AddressLat != null && result.AddressLon != null && result.AddressLat != '' && result.AddressLon != '')  
	            {   
	            	Address__c addr = [SELECT Id, Name, Latitude__c, Longitude__c 
	            							  FROM Address__c
	            							  WHERE Id = :l.AddressID__c];
	            	
	            	if(addr != null)
	            	{
	            		addr.Latitude__c = Decimal.valueOf(result.AddressLat); 
		            	addr.Longitude__c = Decimal.valueOf(result.AddressLon);    
		            	update addr;
	            	}
	            	         	
		            pageRef.getParameters().put('lat', result.AddressLat); 
		            pageRef.getParameters().put('lon', result.AddressLon);            
		            return pageRef;
	            } 	
            }   
            isError = true;                                 
       	}      
      return null;
    }
}