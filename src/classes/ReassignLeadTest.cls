@isTest
private class ReassignLeadTest{
	static testmethod void reassignLead(){
		TestHelperClass.createReferenceUserDataForTestClasses();
        RecordType rectypes = [Select Id From RecordType Where Name='Company Generated Appointment'];
		User HOAUser = TestHelperClass.createSalesRepUser();
        User salesRep = TestHelperClass.createSalesRepUserWithManager();
        UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
        User managerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId and isActive = true Limit 1];
        User adminUser = TestHelperClass.createAdminUser();
        Postal_Codes__c newPC1;
        Lead testLead;
        System.runas(adminUser){
        	TestHelperClass.createReferenceDataForTestClasses();
            //create postal code.
            newPC1 = new Postal_Codes__c(name = '22102', businessId__c = '1100', TownId__c = '12345', Town__c = 'Unit Test Town 1', AreaId__c = '123', Area__c = 'Unit Test Area');
            insert newPC1;
            ManagerTown__c MT = TestHelperClass.createManagerTown(ManagerUser, newPC1, Channels.TYPE_RESIDIRECT + ';' + Channels.TYPE_RESIRESALE);
            //Create a territory
            Territory__c terr = new Territory__c(name = 'Resi - Test', TerritoryType__c = 'Resi Direct Sales', /*BusinessId__c = '1100',*/ ownerid = salesRep.id);
            insert terr;
            TerritoryAssignment__c ta = new TerritoryAssignment__c(PostalCodeID__c = newPC1.id, TerritoryID__c = terr.id, TerritoryType__c = 'Resi Direct Sales', ownerid = salesRep.id);
            insert ta;
        }

        system.runas(managerUser){
            testLead = TestHelperClass.createLead(managerUser, false, 'BUDCO');
            testLead.NewMoverType__c = 'RL';
            testLead.ownerid = managerUser.Id;
            insert testLead;
            system.debug(testLead.Id);
            ApexPages.PageReference ref = Page.ReassignLead;
            ref.getParameters().put('Id', testLead.Id);
            
            Test.setCurrentPage(ref);
            Test.startTest();
            ReassignLeadController rl = new ReassignLeadController();
            rl.getSelectedSalesRep();
            rl.getallSalesreps();
            rl.setSelectedSalesRep(salesRep.Id);
            rl.Save();
            rl.Cancel();
            Test.stopTest();
        }
    }
}