/************************************* MODIFICATION LOG ********************************************************************************************
* ADTPartnerCreditCheckAPIController 
*
* DESCRIPTION : Supporting class for ADTPartnerCreditCheckAPI. Process the incoming JSON request and process response with creditscore.
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                    DATE           TICKET           REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*   Srinivas Yarramsetti       09/26/2017                      - Original Version
*   Varun Sinha                06/28/2019     HRM - 9939       - No Hit Credit Check,  Soft Credit Check
*   Abhinav Pandey            03/11/2019             HRM-10881         - Flex Fi Changes
*/

public with sharing class ADTPartnerCreditCheckAPIController{
    //Need it for Exception
    public class invalidDataException extends Exception {}
    //Parsed JSON String
    public dataPowerRequestInfo dataPowerRequestObj;
    public ADTFICOAPI.doConsumerScoreInfoResp ConsumerCheckResp {get;set;} 
    public Boolean isNoHit;
    public String Equifax_Credit_Score;
    public String EquifaxApprovalType;
    public String EquifaxRiskGrade;
    public String EquifaxRiskGradeDisplayValue;
    public String EquifaxCondition;
    public Boolean blockCreditCheck;
    public String ssnNumber4;
    public String messageStr;
    public Integer statusCode = 200; //Success
    public list<String> threepayterm;
    public string lastFourDigits;
    //Added for Soft to hard - 9939
    public Boolean doSoftCheck {get;set;}
    //Added to check for loan application feature
    public list<Equifax_Mapping__c> equifaxMappingList = null;
    public boolean loanAppAllowedFlag;
    public enum PAYMENT_VALUE {Annual, Quarterly, Monthly}
    // @ Primary Wrapper class to initialize JSON request
    public class dataPowerRequestInfo{
        public String opportunityId;
        public String partnerId;
        public String partnerRepName;
        public String callId;
        public customer customer;
    }
    
    public class customer{
        public String taxIdNumber;
        public String dateOfBirth;
        public previousAddress previousAddress;
    }
    
    public class previousAddress{
        public String addrLine1;
        public String addrLine2;
        public String city;
        public String state;
        public String postalCode;
        
    }
    
    public class creditRating{
        public String dateRetrieved;
        public String riskGrade;
        public String approvalType;
        public String paymentFrequency;
        public Integer depositPercent;
        public Boolean multiPayAllowed;
        public list<string> multiPayTerms;
        public Boolean easyPayRequired;
        public String message;
        public boolean loanAppAllowed;        
    }
    //Reponse wrapper
    public class responseWrapper{
        public String opportunityId;
        public String message;
        public creditRating creditRating;
    }
    
    //Child node for response wrapper
    public class paymentInfo{
        public String PaymentFreq; 
        public Decimal DepositPercent;
        public Boolean multipayallowed;
        public Boolean EasyPay;
        public list<string> multipayterm;
    }
    
    //Parser logic for parse JSON String to respective wrappers.
    public responseWrapper parseDPRequest(String jsonReq){
        responseWrapper responseString;
        try{
            dataPowerRequestObj = (dataPowerRequestInfo)JSON.deserializeStrict(jsonReq,dataPowerRequestInfo.class);
            system.debug(date.valueOf(dataPowerRequestObj.customer.dateOfBirth)); // Do not remove this debug. Check date value.
            system.debug(Integer.valueOf(dataPowerRequestObj.customer.taxIdNumber));// Do not remove this debug. Check SSN value.
            
            if(dataPowerRequestObj.customer.previousAddress != null){
                //system.debug(Integer.valueOf(dataPowerRequestObj.customer.previousAddress.postalCode));// Do not remove this debug. Check zip value.
                PartnerSettings__c pSettings = PartnerSettings__c.getorgDefaults();
                String[] validStateCodes = pSettings.ValidUSStateCodes__c.split(';');
                set<String> validStateSet = new set<String>();
                validStateSet.addAll(validStateCodes);
                if(dataPowerRequestObj.customer.previousAddress.state !=null && !validStateSet.contains(dataPowerRequestObj.customer.previousAddress.state)){
                    throw new invalidDataException('Invalid State code: '+dataPowerRequestObj.customer.previousAddress.state);
                }
                else if(dataPowerRequestObj.customer.previousAddress.postalCode != null && !validateZip(dataPowerRequestObj.customer.previousAddress.postalCode)){
                    throw new invalidDataException('Zip code is not valid: '+dataPowerRequestObj.customer.previousAddress.postalCode);
                }
            }
            responseString = dataPowerResponse(dataPowerRequestObj);
        }
        catch (Exception ae){
            //Any kind of exception is handled with Exception message
            ADTApplicationMonitor.log(ae, 'ADTPartnerCreditCheckAPIController', 'parseDPRequest', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
            statusCode = 400;
            responseWrapper resObj = new responseWrapper();
            creditRating cr = new creditRating();
            cr.Message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
            resObj.creditRating = cr;
            responseString = resObj;
        }
        return responseString;
    }
    
    /********************************************************************************************
     * Description: Query account and create a response
     * Return Type: responsseWrapper
     * ******************************************************************************************/
    private responseWrapper dataPowerResponse(dataPowerRequestInfo dprequestObject){
        responseWrapper resObj = new responseWrapper();
        creditRating cr = new creditRating();
        Boolean prevAddLogic = false;
        boolean noHit = false;
        loanAppAllowedFlag = false;
        Account oppParAccnt = null;
        try{
            //Check for valid previous address.
            if(dprequestObject.customer != null && dprequestObject.customer.previousAddress != null && (!String.isBlank(dprequestObject.customer.previousAddress.addrLine1) || !String.isBlank(dprequestObject.customer.previousAddress.addrLine2) || !String.isBlank(dprequestObject.customer.previousAddress.city)
                || !String.isBlank(dprequestObject.customer.previousAddress.state) || !String.isBlank(dprequestObject.customer.previousAddress.postalCode))){
                if(String.isBlank(dprequestObject.customer.previousAddress.city) || String.isBlank(dprequestObject.customer.previousAddress.state) || String.isBlank(dprequestObject.customer.previousAddress.postalCode)){
                        prevAddLogic = true;
                    }    
                }
            if(!prevAddLogic){
                list<Opportunity> opplist = [SELECT id, AccountId from Opportunity where id =:dprequestObject.opportunityId];
                
                if(opplist.size() > 0 && opplist[0].AccountId!=null){   
                    //9939
                    oppParAccnt = getAccountInfo(opplist[0].AccountId);
                    
                    system.debug('## Checking account before update'+oppParAccnt);         
                    //Added for To check whether to make a hit again or not
                    equifaxMappingList = [Select HitCode__c, HitCodeResponse__c, NoHit__c, RiskGrade__c, RiskGradeDisplayValue__c, ApprovalType__c, CreditScoreStartValue__c, CreditScoreEndValue__c from Equifax_Mapping__c Where RiskGrade__c != null And ApprovalType__c != null];
                    for(Equifax_Mapping__c eMap : equifaxMappingList){
                        if(eMap.NoHit__c && String.isNotBlank(oppParAccnt.EquifaxRiskGrade__c) && eMap.RiskGrade__c == oppParAccnt.EquifaxRiskGrade__c && String.isNotBlank(oppParAccnt.EquifaxApprovalType__c) && eMap.ApprovalType__c == oppParAccnt.EquifaxApprovalType__c){
                           noHit = true;
                           break;
                        }
                    }
                    
                    //Check if credit score is the old one. And hit equifax for new credit score.
                    if(oppParAccnt.Equifax_Last_Check_DateTime__c == null || oppParAccnt.Equifax_Last_Check_DateTime__c.date().daysBetween(Date.today()) > Integer.valueOf(Equifax__c.getinstance('Days to allow additional check').value__c) || noHit){
                        GetConsumerScoreInformation(oppParAccnt, dprequestObject.customer);
                        messageStr = EquifaxRiskGradeDisplayValue;
                    }
                    else{
                        messageStr = equifaxRiskGDVal(oppParAccnt);
                    }
                    //Query Account again to get updated account informtion after equifax
                    oppParAccnt = getAccountInfo(opplist[0].AccountId);
                    //Added to check partner configuration
                    if(String.isNotBlank(dprequestObject.partnerId)){
                        System.debug('The partner id is'+dprequestObject.partnerId);
                        // Partner Settings
                        for (PartnerConfiguration__c pConfig : [Select CreditCheckApi__c,PartnerID__c From PartnerConfiguration__c Where PartnerID__c =: dprequestObject.partnerId And CreditCheckApi__c != null limit 1]){
                            if(pConfig.CreditCheckApi__c.contains('Loan Application')){
                                LoanApplicationDataAccess dataAccess = new LoanApplicationDataAccess();
                                string message = dataAccess.getLoanApplicationAllowedMessage(oppParAccnt,true,'');//HRM-10881 Added true boolean
                                if(message == 'false'){
                                    loanAppAllowedFlag = true;
                                }
                            }
                        }
                    }
                    system.debug('## Checking updated account'+oppParAccnt);
                    // Create a response for the request
                    resObj.opportunityId = opplist[0].id;
                    cr.dateRetrieved = oppParAccnt.Equifax_Last_Check_DateTime__c != null? oppParAccnt.Equifax_Last_Check_DateTime__c.format('yyyy-MM-dd').substring(0,10):'';
                    cr.RiskGrade = oppParAccnt.EquifaxRiskGrade__c!=null?oppParAccnt.EquifaxRiskGrade__c:'';
                    cr.ApprovalType = oppParAccnt.EquifaxApprovalType__c!=null?oppParAccnt.EquifaxApprovalType__c:'';
                    //Added New loanAppAllowedFlag in response
                    cr.loanAppAllowed = loanAppAllowedFlag;
                    paymentInfo paymentInfo = processPaymentIfo(oppParAccnt);
                    if(paymentInfo !=null){
                        cr.PaymentFrequency = paymentInfo.PaymentFreq != null? paymentInfo.PaymentFreq:'' ;
                        cr.depositPercent = paymentInfo.DepositPercent!=null? paymentInfo.DepositPercent.intValue():0;
                        cr.multiPayAllowed = paymentInfo.multipayallowed;
                        cr.multiPayTerms = new list<String>(paymentInfo.multipayterm);
                        cr.easyPayRequired = paymentInfo.EasyPay;
                        cr.message = messageStr;
                    }
                    else{
                        cr.PaymentFrequency = '';
                        cr.depositPercent = 0;
                        cr.message = messageStr;
                    }
                    resObj.creditRating = cr;
                    system.debug('JsonResponse'+resObj);
                    return resObj;
                }                              
                else{
                    cr.Message = PartnerAPIMessaging__c.getinstance('404').Error_Message__c.replace('requestID','Opportunity ID');
                    resObj.opportunityId = dprequestObject.opportunityId;
                    resObj.creditRating = cr;
                    statusCode = 404;
                    return resObj;
                }
            }
            else{
                statusCode = 400; 
                cr.Message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                resObj.opportunityId = dprequestObject.opportunityId;
                resObj.creditRating = cr;
                return resObj;
            }
        }
        catch(Exception ae){
            //Any kind of exception is handled with Exception message
            ADTApplicationMonitor.log(ae, 'ADTPartnerCreditCheckAPIController', 'dataPowerResponse', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
            statusCode = 400;
            cr.Message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
            resObj.opportunityId = dprequestObject.opportunityId;
            resObj.creditRating = cr;
        }
        return resObj;
    }
    
    /********************************************************************************************
     * Description: Core method to parse and call equifax.
     * Return Type: paymentInfo
     * ******************************************************************************************/
     
    public void GetConsumerScoreInformation(Account acnt, customer custInfo){
        // Credit Check
        try{    
            ADTFICOGateway adtfg = new ADTFICOGateway();
            ADTFICOGateway.CCInfo cInf = new ADTFICOGateway.CCInfo();
            cInf.address1 = acnt.SiteStreet__c;
            cInf.address2 = acnt.SiteStreet2__c != null ? acnt.SiteStreet2__c : '';
            cInf.city = acnt.SiteCity__c;
            cInf.state = acnt.SiteState__c;
            cInf.zip = acnt.SitePostalCode__c;
            lastFourDigits = custInfo.taxIdNumber.length() >= 4 ? custInfo.taxIdNumber.substring(custInfo.taxIdNumber.length()-4,custInfo.taxIdNumber.length()) : '';
            system.debug('##'+lastFourDigits);
            cInf.ssn4No = lastFourDigits;
            ssnNumber4 = lastFourDigits;
            cInf.dob = Date.valueOf(custInfo.dateOfBirth);
            
            cInf.prev_address1 = custInfo.previousAddress !=null && custInfo.previousAddress.addrLine1  != null ? custInfo.previousAddress.addrLine1:'';
            cInf.prev_address2 =  custInfo.previousAddress !=null && custInfo.previousAddress.addrLine2 != null ? custInfo.previousAddress.addrLine2:'';
            cInf.prev_city =  custInfo.previousAddress !=null && custInfo.previousAddress.city != null ? custInfo.previousAddress.city:'';
            cInf.prev_state =  custInfo.previousAddress !=null && custInfo.previousAddress.state != null ? custInfo.previousAddress.state:'';
            cInf.prev_zip =  custInfo.previousAddress !=null && custInfo.previousAddress.postalCode != null ? custInfo.previousAddress.postalCode.substring(0,5):'';
            
            cInf.partnerId = dataPowerRequestObj.partnerId;
            cInf.repName = dataPowerRequestObj.partnerRepName;
            ADTFICOGateway ficoScoringSrv = new ADTFICOGateway(acnt, cInf ); 
            ConsumerCheckResp = ficoScoringSrv.ConsumerScoreInfo(); 
            if( ConsumerCheckResp != null ){
                String noHitRiskGrade = '';
                String noHitRiskGradeDisplayValue = '';
                String noHitApprovalType = '';
                this.isNoHit = false;
                this.Equifax_Credit_Score = ConsumerCheckResp.ccIcCreditScore;
                if(String.isNotBlank(ConsumerCheckResp.ccIcHitCode)){
                    if(equifaxMappingList == null){
                        equifaxMappingList = [Select HitCode__c, HitCodeResponse__c, NoHit__c, RiskGrade__c, RiskGradeDisplayValue__c, ApprovalType__c, CreditScoreStartValue__c, CreditScoreEndValue__c from Equifax_Mapping__c Where RiskGrade__c != null And ApprovalType__c != null];
                    }
                    for(Equifax_Mapping__c eMap : equifaxMappingList){
                        if(eMap.HitCode__c == ConsumerCheckResp.ccIcHitCode){
                            //Perfect hit - hitscore 1 or 6
                            if(ConsumerCheckResp.ccIcHitCode == '1' || ConsumerCheckResp.ccIcHitCode == '6'){
                                if((String.isNotBlank(ConsumerCheckResp.ccIcCreditScore) && String.isNotBlank(eMap.CreditScoreStartValue__c) && String.isNotBlank(eMap.CreditScoreEndValue__c) && Integer.valueOf(ConsumerCheckResp.ccIcCreditScore) >= Integer.valueOf(eMap.CreditScoreStartValue__c) && Integer.valueOf (ConsumerCheckResp.ccIcCreditScore) <= Integer.valueOf(eMap.CreditScoreEndValue__c)) || 
                                    (String.isNotBlank(ConsumerCheckResp.ccIcRejectCode) && ConsumerCheckResp.ccIcRejectCode == eMap.HitCodeResponse__c)){
                                    this.EquifaxApprovalType = eMap.ApprovalType__c;
                                    this.EquifaxRiskGrade = eMap.RiskGrade__c;
                                    if(String.isNotBlank(eMap.RiskGradeDisplayValue__c)){
                                        this.EquifaxRiskGradeDisplayValue = eMap.RiskGradeDisplayValue__c;
                                    }
                                }
                            }else{
                                if(eMap.NoHit__c){
                                    this.isNoHit = true;
                                    noHitRiskGrade = eMap.RiskGrade__c;
                                    noHitRiskGradeDisplayValue = eMap.RiskGradeDisplayValue__c;
                                    noHitApprovalType = eMap.ApprovalType__c;
                                }
                                this.EquifaxApprovalType = eMap.ApprovalType__c;
                                this.EquifaxRiskGrade = eMap.RiskGrade__c;
                                if(String.isNotBlank(eMap.RiskGradeDisplayValue__c)){
                                    this.EquifaxRiskGradeDisplayValue = eMap.RiskGradeDisplayValue__c;
                                }
                            }
                        }
                    }               
                }
                //9939 -  To update previous address
                acnt.PreviousStreet__c =  custInfo.previousAddress !=null && custInfo.previousAddress.addrLine1  != null ? custInfo.previousAddress.addrLine1:'';
                acnt.PreviousStreet2__c = custInfo.previousAddress !=null && custInfo.previousAddress.addrLine2 != null ? custInfo.previousAddress.addrLine2:'';
                acnt.PreviousCity__c =  custInfo.previousAddress !=null && custInfo.previousAddress.city != null ? custInfo.previousAddress.city:'';
                acnt.PreviousState__c = custInfo.previousAddress !=null && custInfo.previousAddress.state != null ? custInfo.previousAddress.state:'';
                acnt.PreviousZip__c = custInfo.previousAddress !=null && custInfo.previousAddress.postalCode != null ? custInfo.previousAddress.postalCode.substring(0,5):'';
                //No Hit to equifax log
                if( isNoHit ){
                    //Set the No Hit Values
                    EquifaxApprovalType = noHitApprovalType;
                    EquifaxRiskGrade = noHitRiskGrade;
                    EquifaxRiskGradeDisplayValue = noHitRiskGradeDisplayValue;
                    blockCreditCheck = true;
                    acceptCreditCondition(acnt);
                }
                else {
                    List<Equifax_Conditions__c> equifaxList = [SELECT Name, Payment_Frequency__c, Three_Pay_Allowed__c, EasyPay_Required__c, Deposit_Required_Percentage__c, Customer_Message__c, Agent_Quoting_Message__c, Agent_Message__c FROM Equifax_Conditions__c WHERE Name = :EquifaxApprovalType];
                    if( equifaxList != null && !equifaxList.isEmpty() ){                    
                        blockCreditCheck = true;
                        acceptCreditCondition(acnt);
                    }
                }
            }
        }
        catch(Exception ae){
            this.blockCreditCheck = true;
            this.EquifaxApprovalType = Equifax__c.getinstance('Failure Condition Code').value__c;
            this.EquifaxRiskGrade = Equifax__c.getinstance('Failure Risk Grade').value__c;
            this.EquifaxRiskGradeDisplayValue = Equifax__c.getinstance('Failure Risk Grade Display Value').value__c;
            //Equifax_Credit_Score = '6.50';
            ADTApplicationMonitor.log(ae, 'ADTPartnerCreditCheckAPIController', 'GetConsumerScoreInformation', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
            this.acceptCreditCondition(acnt);
        }
    }
    
     /********************************************************************************************
     * Description: Method to update account
     * Return Type: void
     * ******************************************************************************************/
    //Method to update account after response from Equifax log.
    private void acceptCreditCondition(Account a){
        try{
            if (this.blockCreditCheck){
                a.Equifax_Last_Check_DateTime__c = DateTime.now();
            }
            a.Equifax_Credit_Score__c = this.Equifax_Credit_Score;
            a.EquifaxApprovalType__c = this.EquifaxApprovalType;
            a.EquifaxRiskGrade__c = this.EquifaxRiskGrade;
            a.Equifax_Last_Four__c = ssnNumber4;
            a.Equifax_Full_SSN_Used__c = true;
            a.Customer_Refused_to_provide_DOB__c = false;
            a.DOB_encrypted__c = Date.valueOf(dataPowerRequestObj.customer.dateOfBirth).format();
            a.Equifax_Last_Four__c = lastFourDigits;
            // Populate the JSON field on the account for field sales only.
            CPQIntegrationDataGenerator.IntegrationDataSource iSource = new CPQIntegrationDataGenerator.IntegrationDataSource(a.Id);                
            iSource.equifaxApprovalType = a.EquifaxApprovalType__c;
            iSource.equifaxRiskGrade = a.EquifaxRiskGrade__c;
            a.CPQIntegrationData__c = CPQIntegrationDataGenerator.getCPQIntegrationJSON( iSource );
            
            update a;
        }
        catch( Exception ae){
            ADTApplicationMonitor.log(ae, 'ADTPartnerCreditCheckAPIController', 'acceptCreditCondition', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        }
    }
    
    /********************************************************************************************
     * Description: Method to create payment wrapper.
     * Return Type: paymentInfo
     * ******************************************************************************************/
    private paymentInfo processPaymentIfo(Account acc){
        paymentInfo creditInfo = new paymentInfo();
        if(String.isNotBlank(acc.EquifaxApprovalType__c)){
            Equifax_Conditions__c equiCond = Equifax_Conditions__c.getValues(acc.EquifaxApprovalType__c);
            if(equiCond.Payment_Frequency__c == 'Q')
                creditInfo.PaymentFreq = String.valueOf(PAYMENT_VALUE.Quarterly);
            else if(equiCond.Payment_Frequency__c == 'A')
                creditInfo.PaymentFreq = String.valueOf(PAYMENT_VALUE.Annual);
            else
                creditInfo.PaymentFreq = String.valueOf(PAYMENT_VALUE.Monthly);
            creditInfo.DepositPercent = equiCond.Deposit_Required_Percentage__c;
            creditInfo.multipayallowed = equiCond.Three_Pay_Allowed__c;
            if(equiCond.Three_Pay_Allowed__c){
                threepayterm = new list<String>();
                threepayterm.add('3');
                creditInfo.multipayterm = threepayterm;
            }
            
            creditInfo.EasyPay = equiCond.EasyPay_Required__c;
            return creditInfo;
        }
        else{
            return null;
        }
    }
    
    /********************************************************************************************
     * Description: Method to get riskgrade display value.
     * Return Type: String
     * ******************************************************************************************/
    private String equifaxRiskGDVal(Account acc){
        if(acc != null){
            if (String.isNotBlank(acc.EquifaxRiskGrade__c) && acc.EquifaxRiskGrade__c == Equifax__c.getinstance('Default Risk Grade').value__c) {
                return Equifax__c.getinstance('Default Risk Grade Display Value').value__c; 
            }else if(String.isNotBlank(acc.EquifaxRiskGrade__c) && acc != null  && !(acc.EquifaxRiskGrade__c == Equifax__c.getinstance('Default Risk Grade').value__c || acc.EquifaxRiskGrade__c == Equifax__c.getinstance('Default Approved Risk Grade').value__c)){
                list<Equifax_Mapping__c> equifaxMappingList = [SELECT RiskGradeDisplayValue__c FROM Equifax_Mapping__c WHERE RiskGrade__c = : acc.EquifaxRiskGrade__c AND RiskGradeDisplayValue__c != null LIMIT 1];
                if(equifaxMappingList != null && equifaxMappingList.size()>0){
                    return equifaxMappingList[0].RiskGradeDisplayValue__c;
                }
            }
        }
        return '';
    }
    
    //Valid zip Ex: 123456789 or 12345-6789 or 12345
    private Boolean validateZip(String postalcode){
        Boolean isValid;
        String regexZip = '^[0-9]{5}[0-9]{4}|[0-9]{5}([-][0-9]{4})|[0-9]{5}?$'; //RegExp Format: 123456789 or 12345-6789 or 12345
        Pattern MyPattern = Pattern.compile(regexZip);
        // Then instantiate a new Matcher object "MyMatcher"
        Matcher MyMatcher = MyPattern.matcher(postalcode);
        isValid = MyMatcher.matches();
        return isValid;
    }
    
    public Account getAccountInfo(string accId){
        Account oppParAccnt = null;
        if(accId != null){
            oppParAccnt = [SELECT id, Name, FirstName__c, LastName__c, DOB_encrypted__c, Customer_Refused_to_provide_DOB__c,Channel__c,MMBCustomerNumber__c,MMBSiteNumber__c,Informix_Addon__c,
                           Equifax_Last_Check_DateTime__c,MMBOrderType__c,PostalCodeID__c, Equifax_Credit_Score__c, TelemarAccountNumber__c,ADTEmployee__c,MMB_Multisite__c,MMBContractNumber__c,
                           EquifaxApprovalType__c, EquifaxRiskGrade__c,Business_Id__c, Equifax_Last_Four__c, Equifax_Full_SSN_Used__c,MMBBillingSystem__c,MMB_Relo_Customer__c,MMBSystemNumber__c,
                           SiteStreet__c, SiteStreet2__c, SiteCity__c, SiteState__c, SitePostalCode__c, SiteCounty__c,Email__c,AddressID__c,TMTownID__c,MMB_Disco_Site__c,Inactive_Contract__c,
                           Informix_Conversion__c,MMB_Relo_Customer_Number__c,MMB_Past_Due_Balance__c,MMBSiteResponse__c,HOA__c,OFACRestriction__c,PostalCodeID__r.name,Informix_Multisite__c,
                           PreviousBillingCustomerNumber__c,PostalCodeID__r.Town_Lookup__c,PostalCodeID__r.Town_Lookup__r.name
                           FROM Account WHERE Id = :accId FOR UPDATE];
        }
        return oppParAccnt;
    }
}