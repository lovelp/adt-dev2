@isTest
private class AccountSearchParametersTest {
		
	static testMethod void testSetAndGet() {
		
		String[] DATASOURCE = new String[]{'DataSource'};
		
		String[] getStringArray = null;
		
		Test.startTest();
		AccountSearchParameters a = new AccountSearchParameters();
		
		a.dataSource = DATASOURCE;
		
		getStringArray = a.dataSource;
		// get value should not be null
		System.assert(getStringArray != null);
		// both arrays should be the same size
		System.assertEquals(DATASOURCE.size(), getStringArray.size(), 'Both arrays should be the same size');
		Integer i = 0;
		for (String getValue: getStringArray) {
			// each element of get array should match the source
			System.assertEquals(DATASOURCE[i], getStringArray[i], 'Each element of get array should match the source');
		}	
			
		Test.stopTest();
		
	}

}