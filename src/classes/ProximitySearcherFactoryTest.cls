@isTest
private class ProximitySearcherFactoryTest
{
    static testMethod void testGetSearcher()
    {
        User usr1 = TestHelperClass.createSalesRepUser();
        User usr2 = TestHelperClass.createSalesExecUser();
        
        ProximitySearcher searcher;
        
        Test.startTest();       
        
        System.runAs(usr1) 
    	{
    		searcher = ProximitySearcherFactory.getSearcher(true, false);
    		
    	}
        
        System.assertEquals('FinancialHierarchyProximitySearcher', searcher.getName());    
            
                
        System.runAs(usr2) 
    	{
    		searcher = ProximitySearcherFactory.getSearcher(false, false);    		
    	}  
    	
    	System.assertEquals('RoleHierarchyProximitySearcher', searcher.getName());   
        
        Test.stopTest();
        
    }
}