@isTest
private class LocationDataFactoryTest {
	
	static testMethod void testGetProvider() {
		
		Test.startTest();
		
		LocationDataProvider ldp = LocationDataFactory.getProvider();
		
		// should get an object back from the factory
		System.assert(ldp != null, 'Factory should return a subclass of LocationDataProvider');
		
		//TODO 
		// ideally should test what class is returned but since Apex doesn't support reflection,
		// this would require a getName() method in the LocationDataProvider abstract class
		// result would be production code created strictly for testing purposes
		
		//TODO
		// condition to test the return of MockLocationDataProvider 
		 
		Test.stopTest();
		
	}
	
	static testMethod void testGetProcessor() {
		
		Test.startTest();
		
		LocationDataMessageProcessor ldmp = LocationDataFactory.getProcessor(LocationDataProvider.CURRENT_LOCATION_MAP_TYPE);
		// should get an object back from the factory
		System.assert(ldmp != null, 'Factory should not return null');
		System.assertEquals('CurrentLocationMessageProcessor', ldmp.getName(), 'Factory should return a CurrentLocationMessageProcessor object');
		
		ldmp = LocationDataFactory.getProcessor(LocationDataProvider.DAILY_LOCATION_MAP_TYPE);
		// should get an object back from the factory
		System.assert(ldmp != null, 'Factory should not return null');
		System.assertEquals('DailyLocationMessageProcessor', ldmp.getName(), 'Factory should return a DailyLocationMessageProcessor object');
		
		ldmp = LocationDataFactory.getProcessor(LocationDataProvider.DISPOSITION_LOCATION_MAP_TYPE);
		// should get an object back from the factory
		System.assert(ldmp != null, 'Factory should not return null');
		System.assertEquals('DispositionLocationMessageProcessor', ldmp.getName(), 'Factory should return a DispositionLocationMessageProcessor object');
		
		Test.stopTest();
		
	}
	

}