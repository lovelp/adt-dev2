/************************************* MODIFICATION LOG ********************************************************************************************
* MelissaDataApi
* 
DESCRIPTION : Calls out to MelissaData to verify and parse address

*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Mike Tuckman                  4/12/14             - Original Version
* Mike Tuckman                  4/27/17             - Updated to use V3 Personator service
*                                                   
*/


public with sharing class MelissaDataAPI {

    // To be moved to Custom Setting
    private static String AddrValidateEndpoint = 'https://addresscheck.melissadata.net/v2/SOAP/Service.svc';
    private static String AddrValidateEndpointV3 = 'https://personator.melissadata.net/v3/WEB/ContactVerify/doContactVerify?';
    private static String mdAct = 'Check';
    private static String mdCols = 'DeliveryIndicator,CountyName,GrpGeocode,Plus4,GrpAddressDetails,GrpParsedAddress,Suite';
    private static String GeocodeEndpoint = 'https://geocoder.melissadata.net/v2/SOAP/Service.svc';
    private static String mdVersion = getVersion();
    private static String MelissaCustID = getID();
    private static Map<String, String> retCodes;
    private static Map<ID, MelissaDataResults> mapMdr;
    private final static ADTApplicationMonitor.CUSTOM_APP appContext = ADTApplicationMonitor.CUSTOM_APP.MELISSA;
     
    public class MelissaDataResults {
        public List<String> ResultCodes;
        public String AddrKey;
        public String Addr1;
        public String Addr2;
        public String Suite;
        public String City;
        public String State;
        public String Zip;
        public String Plus4;
        public String StreetName;
        public String StreetNumber;
        public String County;
        public String CountryCode;
        public String Lat;
        public String Lon;
        public String UTC;
        public String DeliveryIndicator;
        public String UrbanizationName;
        }
    
    private static String getVersion() {
       String version;
       try {
            if (ResaleGlobalVariables__c.getInstance('MelissaDataVersion').value__c != null)
                version = ResaleGlobalVariables__c.getInstance('MelissaDataVersion').value__c;
            else
                version = 'V2';
        }
        catch (Exception ex) {
            version = 'V2';
        }
        return version;
    }
    
    private static String getID() {
       String id;
       try {
            if (ResaleGlobalVariables__c.getInstance('MelissaDataCustID').value__c != null)
                id = ResaleGlobalVariables__c.getInstance('MelissaDataCustID').value__c;
            else
                id = '98900100';
        }
        catch (Exception ex) {
            id = '98900100';
        }
        return id;
    }
    
    
    public static MelissaDataResults validateAddr (String addr1, String addr2, String city, String state, String zip){
        System.debug('inside melissa data class validate addr');
        MelissaDataResults resMD;
        
        try {
        
            //if (mdVersion == 'V3') {
            		System.debug('v3 check');
                resMD = checkAddrV3 (addr1, addr2, city, state, zip);
            
            //} else {
            	//System.debug('non v3 check');
                /*resMD = checkAddr (addr1, addr2, city, state, zip);
                if (resMD != null){
                    retrieveGeo(resMD);
                }*/
            //}
        
        return resMD;
        }
        catch (Exception ex) {
            ADTApplicationMonitor.log(ex, 'MelissaDataAPI', 'validateAddr', appContext);
            return null;
        }
        
    }
        
    public static MelissaDataResults updateAddrObj (ID addressID){
        
        Address__c addr = [select Street__c, Street2__c, City__c, State__c, PostalCode__c, PostalCodeAddOn__c, County__c, Latitude__c, Longitude__c, CountryCode__c from Address__c where Address__c.Id = :addressID];
        
        MelissaDataResults resMD = validateAddr (addr.Street__c, addr.Street2__c, addr.City__c, addr.State__c, addr.PostalCode__c);
		
        if (resMD != null){
            addr.City__c = resMD.City;// Added by mounika Anna -- 6903
            addr.State__c = resMD.State;// Added by mounika Anna -- 6903
            addr.County__c = resMD.County;
            addr.Latitude__c = decimal.valueOf(resMD.Lat);
            addr.Longitude__c = decimal.valueOf(resMD.Lon);
            addr.PostalCodeAddOn__c = resMD.Plus4;
            if(String.isNotBlank(resMD.CountryCode)){
            	if(resMD.CountryCode.contains('US'))
            	addr.CountryCode__c = 'US';
        		else
        		addr.CountryCode__c = resMD.CountryCode;
            }
            //addr.CountryCode__c = String.isNotBlank(resMD.CountryCode)?resMD.CountryCode:'';
            update addr;
        }
        
        return resMD;
        
    }
        
    public static MelissaDataResults webValidateAddrObj (ID addressID){
    	
    		if (mapMdr == null) mapMdr = new Map<ID, MelissaDataResults>();
    		if (mapMdr.containsKey(addressID)) {
    			system.debug('Found MD Match');
    			return mapMdr.get(addressID);
    		}
        
        Address__c addr = [select Street__c, Street2__c, City__c, State__c, PostalCode__c, PostalCodeAddOn__c, County__c, Latitude__c, Longitude__c, CountryCode__c from Address__c where Address__c.Id = :addressID];
        
        MelissaDataResults resMD = validateAddr (addr.Street__c, addr.Street2__c, addr.City__c, addr.State__c, addr.PostalCode__c);
		
        if (resMD != null){
        		system.debug('Storing MDR :' + addressID);
        		mapMdr.put(addressID, resMD);
        }
        
        return resMD;
        
    }
            
    /*private static MelissaDataResults checkAddr (String addr1, String addr2, String city, String state, String zip){
        
        HttpRequest req = new HttpRequest();

        String url = AddrValidateEndpoint;
        req.setEndpoint(url);
        req.setMethod('POST');
                
        String xmlreq = 
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:MelissaDataAddressCheckService" xmlns:urn1="urn:mdWebServiceAddress">'+
            '<soapenv:Header/>'+
            '<soapenv:Body>'+
            '<urn:doAddressCheck>'+
            '<urn:Request>'+
            '<urn1:TransmissionReference></urn1:TransmissionReference>'+
            '<urn1:CustomerID>' + MelissaCustID + '</urn1:CustomerID>'+
            '<urn1:OptAddressParsed>true</urn1:OptAddressParsed>'+
            '<urn1:Record>'+
            '<urn1:RecordID></urn1:RecordID>'+
            '<urn1:Company></urn1:Company>'+
            '<urn1:LastName></urn1:LastName>'+
            '<urn1:Urbanization></urn1:Urbanization>'+
            '<urn1:AddressLine1>' + addr1 + '</urn1:AddressLine1>'+
            '<urn1:AddressLine2>' + addr2 + '</urn1:AddressLine2>'+
            '<urn1:Suite></urn1:Suite>'+
            '<urn1:City>' + city + '</urn1:City>'+
            '<urn1:State>' + state + '</urn1:State>'+
            '<urn1:Zip>' + zip + '</urn1:Zip>'+
            '<urn1:Plus4></urn1:Plus4>'+
            '<urn1:Country></urn1:Country>'+
            '</urn1:Record>'+
            '</urn:Request>'+
            '</urn:doAddressCheck>'+
            '</soapenv:Body>'+
            '</soapenv:Envelope>';
		*/
        //req.setHeader('Accept', '*/*');
        /*req.setHeader('Accept-Encoding', 'gzip,deflate');
        req.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        req.setHeader('SOAPAction', 'urn:MelissaDataAddressCheckService/IService/doAddressCheck');
        req.setHeader('Content-Length', xmlreq.length().format());
        req.setBody(xmlreq);
        System.debug('Request...:' + req.getBody());
        
        Httpresponse res;
        
        if (!Test.isRunningTest()) {
            Http http = new Http();
            res = http.send(req);           
        }
        else
            res = fakeAddrRes();
        
        MelissaDataResults resMD = parseResultsAddr(res);
        
        System.debug('Results' + resMD);        
        
        return resMD;
    }*/
    
    private static MelissaDataResults checkAddrV3 (String addr1, String addr2, String city, String state, String zip){
        
        HttpRequest req = new HttpRequest();

        String url = AddrValidateEndpointV3;
        url += 'id=' + MelissaCustID;
        url += '&format=json';
        url += '&act=' + mdAct;
        url += '&cols=' + mdCols;
        if (!Utilities.isEmptyOrNull(addr1)) url += '&a1=' + EncodingUtil.urlEncode(addr1, 'UTF-8');
        if (!Utilities.isEmptyOrNull(addr2)) url += '&a2=' + EncodingUtil.urlEncode(addr2, 'UTF-8');
        if (!Utilities.isEmptyOrNull(city)) url += '&city=' + EncodingUtil.urlEncode(city, 'UTF-8');
        if (!Utilities.isEmptyOrNull(state)) url += '&state=' + EncodingUtil.urlEncode(state, 'UTF-8');
        if (!Utilities.isEmptyOrNull(zip)) url += '&postal=' + EncodingUtil.urlEncode(zip, 'UTF-8');
        //url = EncodingUtil.urlEncode(url, 'UTF-8')
        //url = url.replace(' ','+');
        
        req.setEndpoint(url);
        req.setMethod('GET');
        
        System.debug('Request URL...:' + url);
        
        Httpresponse res;
        
        if (!Test.isRunningTest()) {
            Http http = new Http();
            res = http.send(req);           
        }
        else
            res = fakeAddrResV3();
        
        MelissaDataResults resMD = parseResultsAddrV3(res);
        
        System.debug('Results' + resMD);        
        
        return resMD;
    }
    
    
    
    /*private static MelissaDataResults parseResultsAddr(Httpresponse res){

        MelissaDataResults mdata = new MelissaDataResults();

        String ns1 = 'urn:MelissaDataAddressCheckService';
        String ns2 = 'urn:mdWebServiceAddress';
        String soap = 'http://schemas.xmlsoap.org/soap/envelope/';
        
        String body = res.getBody();
        System.debug('Body:' + body);   
                
        Dom.Document doc = res.getBodyDocument();
        System.debug('Doc:' + doc); 
        
        Dom.XMLNode row = doc.getRootElement().getChildElement('Body',soap).getChildElement('doAddressCheckResponse',ns1).getChildElement('doAddressCheckResult', ns1).getChildElement('Record', ns2);
        String CodeString = row.getChildElement('Results',ns2).getText(); 
        row = row.getChildElement('Address', ns2);
        mdata.Addr1 = nonNull(row.getChildElement('Address1',ns2).getText()); 
        if(nonNull(row.getChildElement('Urbanization',ns2).getChildElement('Name',ns2).getText())!=null){
            mdata.Addr2 = nonNull(row.getChildElement('Urbanization',ns2).getChildElement('Name',ns2).getText());
            System.debug('Urbanization'+nonNull(row.getChildElement('Urbanization',ns2).getChildElement('Name',ns2).getText()));
        }
        else{
            mdata.Addr2 = nonNull(row.getChildElement('Address2',ns2).getText());
            System.debug('Urbanization response'+mdata.Addr2);
        }
        
        mdata.Suite = nonNull(row.getChildElement('Suite',ns2).getText()); 
        mdata.City = nonNull(row.getChildElement('City',ns2).getChildElement('Name',ns2).getText()); 
        mdata.State = nonNull(row.getChildElement('State',ns2).getChildElement('Abbreviation',ns2).getText()); 
        mdata.Zip = nonNull(row.getChildElement('Zip',ns2).getText()); 
        mdata.Plus4 = nonNull(row.getChildElement('Plus4',ns2).getText()); 
        mdata.AddrKey = nonNull(row.getChildElement('AddressKey',ns2).getText()); 
        row = row.getChildElement('Parsed', ns2);
        mdata.StreetName = nonNull(row.getChildElement('StreetName',ns2).getText() + ' ' + row.getChildElement('Suffix',ns2).getText());
        mdata.StreetNumber = nonNull(row.getChildElement('AddressRange',ns2).getText()); 
        
        List<String> Codes = CodeString.split(',');
        mdata.ResultCodes = new List<String>();
        
        for (String code: Codes){
            if (MelissaDataErrors__c.getInstance(code) != null)
                mdata.ResultCodes.add(code + ':' + MelissaDataErrors__c.getInstance(code).Message__c);
            else
                mdata.ResultCodes.add(code + ':' + code + '-Unknown Code');
        }
        mdata.ResultCodes.add('AS00' + ':' + mdVersion);
                
        return mdata;
        
    }*/
    
    private static MelissaDataResults parseResultsAddrV3(Httpresponse res){

        MelissaDataResults mdata = new MelissaDataResults();
        
        String CodeString;
        
        JSONParser parser = JSON.createParser(res.getBody());
        
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == 'AddressLine1') {
                    parser.nextToken();
                    mdata.Addr1 = parser.getText();
                }
                if (parser.getText() == 'AddressLine2') {
                    parser.nextToken();
                    mdata.Addr2 = parser.getText();
                }
                if (parser.getText() == 'Suite') {
                    parser.nextToken();
                    mdata.Suite = parser.getText();
                }
                if (parser.getText() == 'City') {
                    parser.nextToken();
                    mdata.City = parser.getText();
                }
                if (parser.getText() == 'State') {
                    parser.nextToken();
                    mdata.State = parser.getText();
                }
                if (parser.getText() == 'PostalCode') {
                    parser.nextToken();
                    mdata.Zip = parser.getText();
                }
                if (parser.getText() == 'Plus4') {
                    parser.nextToken();
                    mdata.Plus4 = parser.getText();
                }
                if (parser.getText() == 'AddressKey') {
                    parser.nextToken();
                    mdata.AddrKey = parser.getText();
                }
                if (parser.getText() == 'AddressStreetName') {
                    parser.nextToken();
                    mdata.StreetName = parser.getText();
                }
                if (parser.getText() == 'AddressStreetSuffix') {
                    parser.nextToken();
                    mdata.StreetName += ' ' + parser.getText();
                }
                if (parser.getText() == 'AddressHouseNumber') {
                    parser.nextToken();
                    mdata.StreetNumber = parser.getText();
                }
                if (parser.getText() == 'Latitude') {
                    parser.nextToken();
                    mdata.Lat = parser.getText();
                    system.debug('Lat');
                    system.debug(mdata.Lat);
                    if (mdata.Lat == '' || mdata.Lat == ' ' || mdata.Lat == null) mdata.Lat = '0';
                }
                if (parser.getText() == 'Longitude') {
                    parser.nextToken();
                    mdata.Lon = parser.getText();
                    system.debug('Lon');
                    system.debug(mdata.Lon);
                    if (mdata.Lon == '' || mdata.Lon == ' ' || mdata.Lon == null) mdata.Lon = '0';
                }
                if (parser.getText() == 'CountyName') {
                    parser.nextToken();
                    mdata.County = parser.getText();
                }
                if(parser.getText() == 'CountryCode'){
                	parser.nextToken();
                    mdata.CountryCode = parser.getText();
                }
                if (parser.getText() == 'UTC') {
                    parser.nextToken();
                    mdata.UTC = parser.getText();
                }
                if (parser.getText() == 'UrbanizationName') {
                    parser.nextToken();
                    mdata.UrbanizationName = parser.getText();
                    if (!Utilities.isEmptyOrNull(mdata.UrbanizationName))
                        mdata.Addr2 = mdata.UrbanizationName;
                }
                if (parser.getText() == 'DeliveryIndicator') {
                    parser.nextToken();
                    mdata.DeliveryIndicator = parser.getText();
                }
                if (parser.getText() == 'Results') {
                    parser.nextToken();
                    CodeString = parser.getText();
                }
            }
        }
        
        List<String> Codes = CodeString.split(',');
        mdata.ResultCodes = new List<String>();
        
        for (String code: Codes){
            if (MelissaDataErrors__c.getInstance(code) != null)
                mdata.ResultCodes.add(code + ':' + MelissaDataErrors__c.getInstance(code).Message__c);
            else
                mdata.ResultCodes.add(code + ':' + code + '-Unknown Code');
        }
        mdata.ResultCodes.add('AS00' + ': version ' + mdVersion);
                
        return mdata;
        
    }

    /*private static void retrieveGeo (MelissaDataResults resMD){
        
        HttpRequest req = new HttpRequest();

        String url = GeocodeEndpoint;
        req.setEndpoint(url);
        req.setMethod('POST');
        
        String xmlreq = 
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:MelissaDataGeoCodeService" xmlns:urn1="urn:mdWebServiceGeoCode">' +
            '<soapenv:Header/>' +
            '<soapenv:Body>' +
            '<urn:doGeoCode>' +
            '<urn:Request>' +
            '<urn1:CustomerID>' + MelissaCustID + '</urn1:CustomerID>' +
            '<urn1:Record>' +
            '<urn1:AddressKey>' + resMD.AddrKey + '</urn1:AddressKey>' +
            '</urn1:Record>' +
            '</urn:Request>' +
            '</urn:doGeoCode>' +
            '</soapenv:Body>' +
            '</soapenv:Envelope>';
		*/
        //req.setHeader('Accept', '*/*');
        /*req.setHeader('Accept-Encoding', 'gzip,deflate');
        req.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        req.setHeader('SOAPAction', 'urn:MelissaDataGeoCodeService/IService/doGeoCode');
        req.setHeader('Content-Length', xmlreq.length().format());
        req.setBody(xmlreq);
        System.debug('Request...:' + req.getBody());

        Httpresponse res;

        if (!Test.isRunningTest()) {
            Http http = new Http();
            res = http.send(req);           
        }
        else        
            res = fakeGeoRes();
                
        parseResultsGeo(res, resMD);
        
        System.debug('Results Geo' + resMD);        
        
        return;
    }
    
    private static void parseResultsGeo(Httpresponse res, MelissaDataResults mdata){


        String ns1 = 'urn:MelissaDataGeoCodeService';
        String ns2 = 'urn:mdWebServiceGeoCode';
        String soap = 'http://schemas.xmlsoap.org/soap/envelope/';
        
        String body = res.getBody();
        System.debug('Body:' + body);   
                
        Dom.Document doc = res.getBodyDocument();
        System.debug('Doc:' + doc); 
        
        Dom.XMLNode row = doc.getRootElement().getChildElement('Body',soap).getChildElement('doGeoCodeResponse',ns1).getChildElement('doGeoCodeResult', ns1).getChildElement('Record', ns2);
        String CodeString = row.getChildElement('Results',ns2).getText(); 
        row = row.getChildElement('Address', ns2);
        mdata.County = nonNull(row.getChildElement('County',ns2).getChildElement('Name',ns2).getText()); 
        mdata.Lat = nonNull(row.getChildElement('Latitude',ns2).getText()); 
        mdata.Lon = nonNull(row.getChildElement('Longitude',ns2).getText()); 
        
        return;
        
    }*/

   /* private static String nonNull(String str) {
        If (str == 'Null'){
            return '';
        }
        else return str;
    }
    
    private static Httpresponse fakeAddrRes(){
        
        Httpresponse res = new Httpresponse();
        
        res.setBody(
            '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">            ' +
            '   <s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ' +
            '      <doAddressCheckResponse xmlns="urn:MelissaDataAddressCheckService">   ' +
            '         <doAddressCheckResult>                                             ' +
            '            <Version xmlns="urn:mdWebServiceAddress">2.0.62</Version>       ' +
            '            <TransmissionReference xmlns="urn:mdWebServiceAddress"/>        ' +
            '            <Results xmlns="urn:mdWebServiceAddress"></Results>             ' +
            '            <TotalRecords xmlns="urn:mdWebServiceAddress">1</TotalRecords>  ' +
            '            <Record xmlns="urn:mdWebServiceAddress">                        ' +
            '               <RecordID/>                                                  ' +
            '               <Results>AC03,AE10</Results>                                 ' +
            '               <Address>                                                    ' +
            '                  <Company></Company>                                       ' +
            '                  <Urbanization>                                            ' +
            '                     <Name></Name>                                          ' +
            '                  </Urbanization>                                           ' +
            '                  <Address1>3252 Marbon Rd</Address1>                       ' +
            '                  <Address2></Address2>                                     ' +
            '                  <Suite></Suite>                                           ' +
            '                  <PrivateMailBox></PrivateMailBox>                         ' +
            '                  <City>                                                    ' +
            '                     <Name>Jacksonville</Name>                              ' +
            '                     <Abbreviation>Jacksonville</Abbreviation>              ' +
            '                  </City>                                                   ' +
            '                  <State>                                                   ' +
            '                     <Name>Florida</Name>                                   ' +
            '                     <Abbreviation>FL</Abbreviation>                        ' +
            '                  </State>                                                  ' +
            '                  <Zip>32223</Zip>                                          ' +
            '                  <Plus4>3218</Plus4>                                       ' +
            '                  <CarrierRoute>R077</CarrierRoute>                         ' +
            '                  <DeliveryPointCode>52</DeliveryPointCode>                 ' +
            '                  <DeliveryPointCheckDigit>7</DeliveryPointCheckDigit>      ' +
            '                  <CongressionalDistrict>04</CongressionalDistrict>         ' +
            '                  <Type>                                                    ' +
            '                     <Address>                                              ' +
            '                        <Code>S</Code>                                      ' +
            '                        <Description>Street</Description>                   ' +
            '                     </Address>                                             ' +
            '                     <Zip>                                                  ' +
            '                        <Code></Code>                                       ' +
            '                        <Description>Standard ZIP Code</Description>        ' +
            '                     </Zip>                                                 ' +
            '                  </Type>                                                   ' +
            '                  <Country>                                                 ' +
            '                     <Abbreviation>US</Abbreviation>                        ' +
            '                     <Name>United States of America</Name>                  ' +
            '                  </Country>                                                ' +
            '                  <AddressKey>32223321852</AddressKey>                      ' +
            '                  <Parsed>                                                  ' +
            '                     <StreetName>Marbon</StreetName>                        ' +
            '                     <AddressRange>3252</AddressRange>                      ' +
            '                     <Suffix>Rd</Suffix>                                    ' +
            '                     <Direction>                                            ' +
            '                        <Post></Post>                                       ' +
            '                        <Pre></Pre>                                         ' +
            '                     </Direction>                                           ' +
            '                     <Suite>                                                ' +
            '                        <Range></Range>                                     ' +
            '                        <Name></Name>                                       ' +
            '                     </Suite>                                               ' +
            '                     <PrivateMailbox>                                       ' +
            '                        <Name></Name>                                       ' +
            '                        <Range></Range>                                     ' +
            '                     </PrivateMailbox>                                      ' +
            '                     <Garbage></Garbage>                                    ' +
            '                     <RouteService></RouteService>                          ' +
            '                     <LockBox></LockBox>                                    ' +
            '                     <DeliveryInstallation></DeliveryInstallation>          ' +
            '                  </Parsed>                                                 ' +
            '               </Address>                                                   ' +
            '            </Record>                                                       ' +
            '         </doAddressCheckResult>                                            ' +
            '      </doAddressCheckResponse>                                             ' +
            '   </s:Body>                                                                ' +
            '</s:Envelope>                                                               ');
        return res;

    }*/

    private static Httpresponse fakeAddrResV3(){
        
        Httpresponse res = new Httpresponse();
        
        res.setBody(
            '{"Records":[{"AddressDeliveryInstallation":" ","AddressExtras":" ","AddressHouseNumber":"11011","AddressKey":"32223665111","AddressLine1":"11011 San Jose Blvd",' +
            '"AddressLine2":" ","AddressLockBox":" ","AddressPostDirection":" ","AddressPreDirection":" ","AddressPrivateMailboxName":" ","AddressPrivateMailboxRange":" ",' +
            '"AddressRouteService":" ","AddressStreetName":"San Jose","AddressStreetSuffix":"Blvd","AddressSuiteName":" ","AddressSuiteNumber":" ","AddressTypeCode":"S",' +
            '"CarrierRoute":"C050","City":"Jacksonville","CityAbbreviation":"Jacksonville","CompanyName":" ","CountryCode":"US","CountryName":"United States of America",' +
            '"CountyName":"Duval","DeliveryIndicator":"U","DeliveryPointCheckDigit":"8","DeliveryPointCode":"11","EmailAddress":" ","Latitude":"30.174800","Longitude":"-81.627881",' +
            '"MelissaAddressKey":" ","MelissaAddressKeyBase":" ","NameFull":" ","PhoneNumber":" ","Plus4":"6651","PostalCode":"32223","RecordExtras":" ","RecordID":"1",' +
            '"Reserved":" ","Results":"AC02,AC03,AE10,GS01","State":"FL","StateName":"Florida","Suite":" ","UTC":"-05:00","UrbanizationName":" "}],"TotalRecords":"1",' +
            '"TransmissionReference":" ","TransmissionResults":" ","Version":"5.1.5"}');
            
        return res;

    }

    /*private static Httpresponse fakeGeoRes(){
        
        Httpresponse res = new Httpresponse();
        
        res.setBody(    
            '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">   ' +
            '   <s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
            '      <doGeoCodeResponse xmlns="urn:MelissaDataGeoCodeService">    ' +
            '         <doGeoCodeResult>                                         ' +
            '            <Version xmlns="urn:mdWebServiceGeoCode">2.0.56</Version>' +
            '            <TransmissionReference xmlns="urn:mdWebServiceGeoCode"></TransmissionReference>' +
            '            <Results xmlns="urn:mdWebServiceGeoCode"></Results>    ' +
            '            <TotalRecords xmlns="urn:mdWebServiceGeoCode">1</TotalRecords>' +
            '            <Record xmlns="urn:mdWebServiceGeoCode">               ' +
            '               <RecordID/>                                         ' +
            '               <Results>GS01</Results>                             ' +
            '               <Address>                                           ' +
            '                  <County>                                         ' +
            '                     <Name>Duval</Name>                            ' +
            '                     <Fips>12031</Fips>                            ' +
            '                  </County>                                        ' +
            '                  <Latitude>30.150164</Latitude>                   ' +
            '                  <Longitude>-81.626664</Longitude>                ' +
            '                  <Place>                                          ' +
            '                     <Code>1235000</Code>                          ' +
            '                     <Name>Jacksonville</Name>                     ' +
            '                  </Place>                                         ' +
            '                  <TimeZone>                                       ' +
            '                     <Name>Eastern Time</Name>                     ' +
            '                     <Code>05</Code>                               ' +
            '                  </TimeZone>                                      ' +
            '                  <CBSA>                                           ' +
            '                     <Code>27260</Code>                            ' +
            '                     <Title>Jacksonville, FL</Title>               ' +
            '                     <Level>Metropolitan Statistical Area</Level>  ' +
            '                     <CBSADivisionCode></CBSADivisionCode>         ' +
            '                     <CBSADivisionTitle></CBSADivisionTitle>       ' +
            '                     <CBSADivisionLevel></CBSADivisionLevel>       ' +
            '                  </CBSA>                                          ' +
            '                  <Census>                                         ' +
            '                     <Block>2005</Block>                           ' +
            '                     <Tract>016803</Tract>                         ' +
            '                  </Census>                                        ' +
            '               </Address>                                          ' +
            '            </Record>                                              ' +
            '         </doGeoCodeResult>                                        ' +
            '      </doGeoCodeResponse>                                         ' +
            '   </s:Body>                                                       ' +
            '</s:Envelope>                                                      ');
        return res;
    }*/

}