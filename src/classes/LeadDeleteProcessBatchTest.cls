/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LeadDeleteProcessBatchTest {

    static testMethod void myUnitTest() {
        User u;
        Lead l;
        List <Lead> leadList = new List<Lead>();

        system.runas(TestHelperClass.createAdminUser()) {
            
        TestHelperClass.createReferenceDataForTestClasses();        
        TestHelperClass.createReferenceUserDataForTestClasses();
        }

        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
           ResaleGlobalVariables__c rgb = new ResaleGlobalVariables__c();
           rgb = [ select id, name , value__c from ResaleGlobalVariables__c where name = 'DaysToRetainResidentialLeads' ];
           rgb.value__c = '1';
           update rgb;
           
           u = TestHelperClass.createSalesRepUser();
           
           Lead l1 = TestHelperClass.createLead(u, false, 'BUDCO' );
           l1.LastName = 'testbudco1';
           l1.competitorcontractenddate__c= System.Today() - 1;
           l1.Business_Id__c = '1100';
           l1.NewMoverType__c = 'RL';
           l1.LeadSource = 'BUDCO';
           leadList.add(l1);
           //insert l;
            
           Lead l2 = TestHelperClass.createLead(u, false, 'BUDCO' );
           l2.LastName = 'testbudco2';
           l2.NewMoverType__c = 'RL';
           leadList.add(l2);
           //insert l;
           
           Lead l3 = TestHelperClass.createLead(u, false, 'Salesgenie.com' );
           l3.LastName = 'testSalesgenie1';
           l3.SGCOM3__SG_infoGroupId__c = '12211';
           leadList.add(l3);
           //insert l;
           Lead l4 = TestHelperClass.createLead(u, false, 'Salesgenie.com' );
           l4.LastName = 'testSalesgenie2';
           l4.SGCOM3__SG_infoGroupId__c = '12001';
           leadList.add(l4);
           //insert l;
           
           Lead l5 = TestHelperClass.createLead(u, false, 'Manual Import' );
           l5.LastName = 'Manual_Import1';
           leadList.add(l5);
           //insert l;
           Lead l6 = TestHelperClass.createLead(u, false, 'Manual Import' );
           l6.LastName = 'Manual_Import2';
           leadList.add(l6);
           //insert l;
            insert leadList;
            //NONE OF THE LEADS WILL BE DELETED BECAUSE WE WILL NOT BE
            //ABLE TO MATCH THE CONDITION IN SOQL QUREY UNLESS WE FIND A
            //WAY TO EDIT CREATED DATE. 
            /*test.startTest();
            LeadDeleteProcessBatch lbatch = new LeadDeleteProcessBatch();
            
           
            Database.executebatch(new LeadDeleteProcessBatch(), 1000);
            List<Lead> leads1 = [select Id from lead where id in :leadList ];
            test.stopTest();*/
            
            test.startTest();
            LeadDeleteProcessBatch lbatch = new LeadDeleteProcessBatch();
            Database.QueryLocator ql = lbatch.start(null);
            lbatch.execute(null,leadList);
            lbatch.Finish(null);
            test.stopTest();
            
           
           
        }
    }
    
    
    static testMethod void LeadDeleteProcessorTest () {
    TestHelperClass.createReferenceDataForTestClasses();  
    Test.startTest(); 
    LeadDeleteProcessor lp = new LeadDeleteProcessor();
    ResaleGlobalVariables__c rgb = new ResaleGlobalVariables__c();
    rgb.name = 'DaysToRetainResidentialLeads';
    rgb.value__c = '1';
    insert rgb;

        String chron = '0 0 23 * * ?';
             
        System.schedule('Test Schedule', chron, lp);
        Test.stopTest();     
    }
}