/************************************* MODIFICATION LOG ********************************************************************************************
* MapItem
*
* DESCRIPTION : Data structure representing a point to be displayed on a map.
*               Capable to created a representation of itself as a String for use by VisualForce pages.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*             				 10/14/2012			- Original Version
*
*													
*/
public with sharing class MapItem {

	//private static final String DATA_DELIMITER = '^'; //Renamed to DD to reduce code line length and clarity
	private static final String DD = '^';
	
	private static final Set<String> badChars = new Set<String>{'[', ']'};
	private static RecordType resaleAcctRecType = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account'); 
    private static RecordType standardAcctRecType = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account');
    private static RecordType rifAcctRecType = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account');
    private static Integer RIFRecentDays = Integer.valueof( ResaleGlobalVariables__c.getinstance('DaysforRecentRIFIcon').value__c );
	
    public String rId {get;set;}
    public String createdFrom {get;set;}
    public String rPhone{get;set;}
    public String rStreet{get;set;}
    public String rCity{get;set;}
    public String rState{get;set;}
    public String rPostalCode{get;set;}
    public string rPostalCodeAddOn{get;set;}
    public String rCountry{get;set;}
    public String rAddress{get;set;}
    public String rName{get;set;}
    public String rOwnerId{get;set;}
    public Decimal rLat{get;set;} 
    public Decimal rLon{get;set;}
    public String mDataSource {get;set;}
    public String mTenureDate {get;set;}
    public Decimal mANSCSold {get;set;}
    public String mSystemProfileCode {get;set;}
    public String mServiceProfileCode {get;set;}
    public String mDiscoReason {get;set;}
    public String mDiscoDate {get;set;}
    public String mDispoDate {get;set;}
    public String mDisposition {get;set;}
    public String mDispositionDetail {get;set;}
    public String mNewMoverType {get;set;}
    public String mType {get;set;}
    public String mOwnerName {get;set;}
    public String mActiveSite {get;set;}
    public String mIsNewMover {get;set;}
    public String mUnAssigned {get;set;}
    public String mRecordCategory {get;set;}
    public String mChannel {get;set;}
    public Decimal rDistance{get;set;}
    public Boolean rIsStartingPoint {get;set;}//false by default
    public Account rAcct {get;set;}
    public Lead rLead {get;set;}   
    
    public MapItem(Decimal latitude, Decimal longitude) {
    	
    	rLat = latitude;
    	rLon = longitude;
    	createdFrom = 'lat-lon';
    }
    
    public MapItem(Account a, Decimal distance) {
    	
    	this(a);
    	rDistance = distance;
    }
    
   
    public MapItem(Account a)
    {    	
        createdFrom = 'account';              
        rAcct = a;        
        rLat = a.Latitude__c; 
        rLon = a.Longitude__c; 
        rId = a.Id;
        mANSCSold = 0;
        
		if(a.OwnerId == UserInfo.getUserId())
		{
			if(a.RecordTypeId == resaleAcctRecType.Id)
			{
				mRecordCategory = 'My Prospects';
			}
			else if(a.RecordTypeId == standardAcctRecType.Id)
			{
				// default to My Prospects, unless determine some other value is more applicable
				mRecordCategory = 'My Prospects';
				if(a.SalesAppointmentDate__c  != null && a.SalesAppointmentDate__c > System.now()) {
					mRecordCategory = 'My Sales Appointments';
				}	
				else if(a.SalesAppointmentDate__c  != null && a.SalesAppointmentDate__c <= System.now() && (a.ScheduledInstallDate__c == null || (a.ScheduledInstallDate__c != null && a.ScheduledInstallDate__c <= System.now()))) {
					mRecordCategory = 'My Unsold Sales Appointment';
				}
				else if(a.ScheduledInstallDate__c != null && a.ScheduledInstallDate__c > System.now()) {				
					mRecordCategory = 'My Install Appointments';
				}				
			} 
			else if(a.RecordTypeId == rifAcctRecType.Id)
			{
				if (a.LastActivityDate__c > System.now().addDays(-RIFRecentDays) ){
				mRecordCategory = 'RIF';
			}				
				else {
					mRecordCategory = 'RIFGrey';					
				}
			}				
		}	
		else
		{	
			if(a.RecordTypeId == rifAcctRecType.Id)
			{
				if (a.LastActivityDate__c > System.now().addDays(-RIFRecentDays) ){
				mRecordCategory = 'RIF';
			}
			else {
					mRecordCategory = 'RIFGrey';					
				}
			}
			else {
				set<String> invalidValues = new set<String>();
            	invalidValues.addAll(IntegrationConstants.STATUS_SOLD_VALUES);
            	invalidValues.addAll(IntegrationConstants.STATUS_CLOSED_VALUES);	
            	if(!invalidValues.contains(a.AccountStatus__c)) {
					mRecordCategory = 'Other Accounts Available';
            	}
			}     		
		} 
		//Overwrite marker for hoa records
		if( HOAHelper.isHOARecord(a) ){
			mRecordCategory = 'Home Owner Association';
		}
		       
    }
    
    public MapItem(Lead l, Decimal distance)
    {
    	this(l);
    	rDistance = distance;
    }
    
    public MapItem(Lead l)
    {        
        createdFrom = 'lead';             
        rLead = l;    
        rLat = l.Latitude__c; 
        rLon = l.Longitude__c; 	
        rId = l.Id;    
    }   
                                                         
    public String getCleanName(String n){
      String theName='';
      

      try{
            theName = String.escapeSingleQuotes(n+'');  
            theName = theName.trim().replaceAll('[&();:#@"!]+','');
			theName = theName.trim().replaceAll('[-~$%^*_+=?]+','');
			theName = theName.trim().replaceAll('[|\n\r{}]+','');
        
            for(String theChar: badChars)
            {
                theName = theName.replace(theChar,'');    
            }
            
            if(theName.length()>20){
            	theName=theName.substring(0,20);
            }
        }
        catch(Exception e){ 
        	theName='-';
        }
        return theName; 
      
    }
                                                           
    public String getCleanAddress(){
        
        //Make one String with Street, City, State, Zip, Country... as long as each of the elements exist.
        String theAddress = rAddress;
        
        try{
        	if(theAddress.length()>0 && rCity.length()>0){ theAddress += ', ';}
            if(rCity.length()>0){ theAddress += rCity ;}
        }
        catch(Exception e){ }
        try{
        	if(theAddress.length()>0 && rState.length()>0){ theAddress += ', ';}
            if(rState.length()>0){ theAddress += rState ;}
        }
        catch(Exception e){ }
        try{
        	if(theAddress.length()>0 && rPostalCode.length()>0){ theAddress += ', ';}
            if(rPostalCode.length()>0){ theAddress += rPostalCode;}
        }
        catch(Exception e){ }
        try{
        	if(theAddress.length()>0 && rPostalCodeAddOn.length()>0){ theAddress += ' ';}
            if(rPostalCodeAddOn.length()>0){ theAddress += rPostalCodeAddOn;}
        }
        catch(Exception e){ }
        try{
        	if(theAddress.length()>0 && rCountry.length()>0){ theAddress += ', ';}
            if(rCountry.length()>0){ theAddress += rCountry;} 
        }
        catch(Exception e){ }
        try{
            theAddress = String.escapeSingleQuotes(theAddress+'');  
            theAddress = theAddress.trim().replaceAll('[&();:#@"!]+','');
			theAddress = theAddress.trim().replaceAll('[-~$%^*_+=?]+','');
			theAddress = theAddress.trim().replaceAll('[|\n\r{}]+','');
        
            for(String theChar: badChars)
            {
                theAddress = theAddress.replace(theChar,'');    
            }           
        }
        catch(Exception e){ theAddress='-';}
        return theAddress;      
    }
    
    
	public String getIsStartingPointStr(){
		if(rIsStartingPoint == null || !rIsStartingPoint){
			return 'false';
		}
		else{
			return 'true';
		}
	}
	
	public String asString() {
		if(createdFrom == 'account')
		{
			buildFromAccount(rAcct);
		}
		else if(createdFrom == 'lead')
		{
			buildFromLead(rLead);
		}
		// Changed code from individual lines for each concatonation to combined lines
		String itemString ='id='+rId+DD+'name='+getCleanName(rName)+DD+'address='+String.escapeSingleQuotes(getCleanAddress())+DD+'phone=';
        if (rPhone == null) {
        	itemString += '';	
        } else {
        	itemString += rPhone;
        }
        itemString += DD+'active='+mActiveSite+DD+'newmover='+mIsNewMover+DD+'datasource='+mDataSource+DD+'discoreason=';
        if (mDiscoReason == null) {
        	itemString += '';
        } else {
        	itemString += mDiscoReason;
        }
        itemString += DD+'discodate=';
        if (mDiscoDate == null) {
        	itemString += '';
        } else {
        	itemString += mDiscoDate;
        }
        itemString += DD+'type='+mType+DD+'rep='+mOwnerName+DD+'distance='+rDistance+DD+'disposition='+mDisposition+DD+
        'dispositiondetail='+mDispositionDetail+DD+'dispodate='+mDispoDate+DD+'newmovertype='+mNewMoverType+DD+'lat='+rLat+
        DD+'lon='+rLon+DD+'unassigned='+mUnAssigned+DD+'recordCategory='+mRecordCategory+DD+'channel='+mChannel+
        DD+'tenuredate='+mTenureDate+DD+'anscsold='+mANSCSold+DD+'systemprofilecode='+mSystemProfileCode+DD+'serviceprofilecode='+mServiceProfileCode;
        
        //System.debug('itemString....: ' + itemString);
        return itemString;
	}
	
	public Boolean isRevenueInForce() {
		
		Boolean returnValue = false;
		if (mRecordCategory == 'RIF' || mRecordCategory == 'RIFGrey') {
			returnValue = true;
		}
		return returnValue;
	}
	
	public Boolean isOutOfService() {
		
		Boolean returnValue = false;
		if (createdFrom == 'account' && rAcct != null && 
				(rAcct.Channel__c == Channels.TYPE_RESIRESALE || rAcct.Channel__c == Channels.TYPE_SBRESALE)) {
			returnValue = true;
		}
		return returnValue;
	}
	
	private void buildFromAccount(Account a)
	{	
		rName = a.Name;
		mANSCSold = 0; 		       
        
		if(a.InService__c == true)
			mActiveSite = 'true';
		else
			mActiveSite = 'false';
			
		if(a.NewMover__c == true)
			mIsNewMover = 'true';
		else
			mIsNewMover = 'false';
			
		if(a.UnassignedLead__c == true)
			mUnAssigned = 'true';
		else
			mUnAssigned = 'false';
		
		if(a.Channel__c != null && a.Channel__c != '')
			mChannel = a.Channel__c;
		else
			mChannel = '';
			
		rAddress = '';
		String tempStreet = a.SiteStreet__c;
        if(tempStreet != null && tempStreet.length()>0){ rAddress = tempStreet; }
        
        rCity = a.SiteCity__c;        
        rState = a.SiteState__c;
        rPostalCode = a.SitePostalCode__c; 
        rPostalCodeAddOn = a.SitePostalCodeAddOn__c;
        
        if(a.SiteCountryCode__c != null)       
        	rCountry = a.SiteCountryCode__c;
        else
        	rCountry = '';
        	
        if(a.Phone != null)
        	rphone = a.Phone;
        else
        	rphone = '';
       
       	mOwnerName = a.Owner.Name;
		
		if(a.Data_Source__c != null && a.Data_Source__c != '')
			mDataSource = a.Data_Source__c;
		else
			mDataSource = '';
		
		if(a.DisconnectReason__c != null && a.DisconnectReason__c != '')
			mDiscoReason = a.DisconnectReason__c;
		else
			mDiscoReason = '';
		
		if(a.DispositionCode__c != null && a.DispositionCode__c != '')
			mDisposition = a.DispositionCode__c;
		else
			mDisposition = '';
		
		if(a.DispositionDetail__c != null && a.DispositionDetail__c != '')	
			mDispositionDetail = a.DispositionDetail__c;
		else
			mDispositionDetail = '';
			
		Date tempDate = a.DisconnectDate__c;		
		if(tempDate != null)
			mDiscoDate = tempDate.format();
		else
			mDiscoDate = '';
			
		DateTime tempDateTime = a.DispositionDate__c;		
		if(tempDateTime != null)
			mDispoDate = tempDateTime.format('M/d/yyyy');
		else
			mDispoDate = '';
		
		Date myTdate = a.TenureDate__c;
		if(myTdate != null)
			mTenureDate = myTdate.format();
		else
			mTenureDate = '';
		
		if (a.ANSCSold__c != null)
			mANSCSold = a.ANSCSold__c;
		else
			mANSCSold = 0.0;
		
		if(a.SystemProfileCode__c != null && a.SystemProfileCode__c != '')
			mSystemProfileCode = a.SystemProfileCode__c;
		else
			mSystemProfileCode = '';
		
		if(a.ServiceProfileCode__c != null && a.ServiceProfileCode__c != '')
			mServiceProfileCode = a.ServiceProfileCode__c;
		else
			mServiceProfileCode = '';
		
		///////////////////////////////////////////	
		if(a.NewMoverType__c != null && a.NewMoverType__c != '')
			mNewMoverType = a.NewMoverType__c;
		else
			mNewMoverType = '';
		
		if(a.Type != null && a.Type != '')	
			mType = a.Type;
		else
			mType = '';
			
		system.debug('^^^^^^^^');
		system.debug(a.OwnerId);
		system.debug(UserInfo.getUserId());
		if(a.OwnerId == UserInfo.getUserId())
		{
			if(a.RecordTypeId == resaleAcctRecType.Id)
			{
				mRecordCategory = 'My Prospects';
			}
			else if(a.RecordTypeId == standardAcctRecType.Id)
			{
				if(a.SalesAppointmentDate__c  != null && a.SalesAppointmentDate__c > System.now())
					mRecordCategory = 'My Sales Appointments';
				else if(a.SalesAppointmentDate__c  != null && a.SalesAppointmentDate__c <= System.now() && (a.ScheduledInstallDate__c == null || (a.ScheduledInstallDate__c != null && a.ScheduledInstallDate__c <= System.now())))
					mRecordCategory = 'My Unsold Sales Appointment';
				else if(a.ScheduledInstallDate__c != null && a.ScheduledInstallDate__c > System.now())				
					mRecordCategory = 'My Install Appointments';				
			}
			else if(a.RecordTypeId == rifAcctRecType.Id)
			{
				if (a.LastActivityDate__c > System.now().addDays(-RIFRecentDays) ){
				mRecordCategory = 'RIF';
			}			
				else {
					mRecordCategory = 'RIFGrey';					
				}
			}			
		}	
		else
		{
			if(a.RecordTypeId == rifAcctRecType.Id)
			{
				if (a.LastActivityDate__c > System.now().addDays(-RIFRecentDays) ){
				mRecordCategory = 'RIF';
			}
			else {
					mRecordCategory = 'RIFGrey';					
				}
			}
			else {
				set<String> invalidValues = new set<String>();
            	invalidValues.addAll(IntegrationConstants.STATUS_SOLD_VALUES);
            	invalidValues.addAll(IntegrationConstants.STATUS_CLOSED_VALUES);	
            	if(!invalidValues.contains(a.AccountStatus__c)) {
					mRecordCategory = 'Other Accounts Available';
            	}
			}
		}
		//Overwrite marker for hoa records
		if( HOAHelper.isHOARecord(a) ){
			mRecordCategory = 'Home Owner Association';
		}
				 
	}
	
	private void buildFromLead(Lead l)
	{
		rName = l.Name;        
        
		if(l.NewMover__c == true)
			mIsNewMover = 'true';
		else
			mIsNewMover = 'false';
		
		rAddress = '';
		String tempStreet = l.SiteStreet__c;
        if(tempStreet != null && tempStreet.length()>0){ rAddress = tempStreet; }
        
        rCity = l.SiteCity__c;
        rState = l.SiteStateProvince__c;
        rPostalCode = l.SitePostalCode__c;
        rPostalCodeAddOn = l.SitePostalCodeAddOn__c;
        
        if(l.SiteCountryCode__c != null)
        	rCountry = l.SiteCountryCode__c;
        else
        	rCountry = '';
        	
        if(l.Phone != null)
        	rphone = l.phone;
        else
        	rphone = '';
        	 
        if(l.Owner.Name != null)
			mOwnerName = l.Owner.Name;		
		
		if(l.LeadSource != null && l.LeadSource != '')
			mDataSource = l.LeadSource;
		else
			mDataSource = '';
			
		if(l.DispositionCode__c != null && l.DispositionCode__c != '')
			mDisposition = l.DispositionCode__c;
		else
			mDisposition = '';
			
		if(l.DispositionDetail__c != null && l.DispositionDetail__c != '')
			mDispositionDetail = l.DispositionDetail__c;
		else
			mDispositionDetail = '';
		
		if(l.Channel__c != null && l.Channel__c != '')
			mChannel = l.Channel__c;
		else
			mChannel = '';
		
		if(mChannel.contains('SB') && l.Company != null && l.Company != '')
		{
			rName = l.Company;
		}
		
		Date tempDate = l.DisconnectDate__c;		
		if(tempDate != null)
			mDiscoDate = tempDate.format();
		else
			mDiscoDate = '';
			
		DateTime tempDateTime = l.DispositionDate__c;		
		if(tempDateTime != null)
			mDispoDate = tempDateTime.format('M/d/yyyy'); 
		else
			mDispoDate = '';
			
		if(l.NewMoverType__c != null && l.NewMoverType__c != '')		
			mNewMoverType = l.NewMoverType__c;
		else
			mNewMoverType = '';
			
		if(l.Type__c != null && l.Type__c != '')
			mType = l.Type__c;
		else
			mType = '';

		if(l.UnassignedLead__c == true)
			mUnAssigned = 'true';
		else
			mUnAssigned = 'false';
		
		if(l.OwnerId == UserInfo.getUserId())
			mRecordCategory = 'My Prospects';
		else
			mRecordCategory = 'Other Leads Available';
	}
}