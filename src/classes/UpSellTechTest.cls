@isTest
public class UpSellTechTest {

   /* public class MockTechUpSellDao extends TechUpSellDao{
        
        public override String getExistingProductCatalog(Account acc){           
            return null;
        }
        
        public override String getAllProductCatalogNames(){
            return null;
        }
        
        public override String getAllProductCatalogQuantity(){
            return null;
        }        
    } */
    public static Account mockTechUpSellAccountProductDetails(){
        Account acc = TestHelperClass.createAccountData();
        ProductCatalog__c pCats = new ProductCatalog__c();
        pCats.ProductName__c ='Test Product';
        pCats.Product_Notes__c = 'Test Notes';
        pCats.Product_Quantity__c = '2';
        pCats.ProductAccount__c = acc.id;
        insert pCats;       
        return acc;
    }
    
    public static Account mockTechUpSellAccount(){
        return TestHelperClass.createAccountData();
    }
    public static TechUpSellProductController makePageControllerWithProductAccountData(){
        ApexPages.StandardController standardController = new ApexPages.StandardController(mockTechUpSellAccountProductDetails());
        return new TechUpSellProductController(standardController);
    }
    public static TechUpSellProductController makePageControllerWithAccountData(){
        ApexPages.StandardController standardController = new ApexPages.StandardController(mockTechUpSellAccount());
        return new TechUpSellProductController(standardController);
    }
    public static void createResaleAvoidAccounttriggerData(){
        list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
        rgvs.add(new ResaleGlobalVariables__c(Name='DataRecastDisableAccountTrigger', value__c='TRUE'));
        insert rgvs;
    }
    
    @isTest
    private static void existingProductCatalogTest(){
        createResaleAvoidAccounttriggerData();
        TechUpSellProductController controller = makePageControllerWithProductAccountData();
        TechUpSellDao daoObj = new TechUpSellDao();
    }
    
    @isTest
    private static void nonExistingProductCatalogTest(){
        createResaleAvoidAccounttriggerData();
        TechUpSellProductController controller = makePageControllerWithAccountData();
        TechUpSellDao daoObj = new TechUpSellDao();
        controller.setTechUpSellDao(daoObj);
    } 
    
    @isTest
    private static void saveProductCatalogTest(){  
        createResaleAvoidAccounttriggerData();      
        TechUpSellProductController controller = makePageControllerWithAccountData();
        TechUpSellDao daoObj = new TechUpSellDao();
        TechUpSellProductController.saveCatalog(controller.acc.id,'[{"pQuantity":"2","pNotes":"Test","pName":"Product A","pId":""}]');
    } 
    
    

}