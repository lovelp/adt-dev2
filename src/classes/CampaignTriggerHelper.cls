/************************************* MODIFICATION LOG ********************************************************************************************
* CampaignTriggerHelper
*
* DESCRIPTION : Class contains logic used from triggers when DML operation are performed against Campaigns
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan               5/26/2017           - Original Version
*                                                   
*/

public class CampaignTriggerHelper{
    /**
     *  Process campaign records before finally submitting them to database
     *
     *  @method ProcessCampaignBeforeInsert
     *
     *  @param List<Campaign>
     *
     */
    public static void ProcessCampaignBeforeInsert(List<Campaign> campList){
        validateCampaign(true,campList,null);
    }
    
    /**
     *  Run logic on campaign record updates
     *  
     *  @method ProcessCampaignBeforeUpdate
     *
     *  @param  Map<Id, Campaign>   New campaign map containing values to commit to database
     *
     *  @param  Map<Id, Campaign>   Old campaign map containing values as they were originally
     * 
     */
    public static void ProcessCampaignBeforeUpdate(Map<Id, Campaign> campaignNewMap, Map<Id, Campaign> campaignOldMap){  
        validateCampaign(false,campaignNewMap.values(),campaignOldMap);
        
    }
    
    /*
     *  Campaign Validation
     */     
     public static void validateCampaign(Boolean isInsert, List<Campaign> campList,Map<Id, Campaign> campaignOldMap){
         Set<String> campaignNameSet = new Set<String>();
         List<String> campaignNameLst = new List<String>();
         for(Campaign camp: campList){
             campaignNameLst.add(camp.Name);
         }
         
         for(Campaign camp: [Select Id, Name from Campaign Where Type = 'Master' AND Name IN:campaignNameLst]){
             campaignNameSet.add(camp.Name);
         }
         
         for(Campaign c: campList){
            if(String.isNotBlank(c.name) && c.name.length() > 10 && c.type != 'Job')
                c.addError('Campaign name cannot be more than 10 characters');
            if(!c.ImmediateRun__c && (c.RunScheduleTime__c == null || c.RunScheduleDay__c == null))
                c.addError('Enter Run Schedule Day & Time or select immediate run');
            if(c.ImmediateRun__c && (c.RunScheduleTime__c != null || c.RunScheduleDay__c != null))
                c.addError('Cannot enter Run Schedule Day/Time if immediate run is selected');
            if(!c.HoldForReview__c && c.Dialer_List_Output_Control__c == null)
                c.addError('Dialer List Output Control needs to be configured');
            if(c.Type == 'One-Time'){               
                if(c.RunScheduleTime__c != null && c.RunScheduleDay__c != null && (c.RunScheduleTime__c.contains(';') || c.RunScheduleDay__c.contains(';')))
                    c.addError('Cannot have more than one Run Schedule for Day on One Time Campaigns.');
            }
            
            // Check for unique name for campaign
            if(campaignNameSet.size() > 0 && campaignNameSet.contains(c.name)){
                if(isInsert || (!isInsert && c.Name != campaignOldMap.get(c.id).Name)){
                    c.addError('A clone for this campaign already exists. Please rename campaign: '+ c.Name + ' to continue.');
                }
            }
        }
    }
}