/************************************* MODIFICATION LOG ********************************************************************************************
* StreetSheetSelectorController
*
* DESCRIPTION : Controller for prospect list selector component used for adding leads / accounts to street sheets.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					4/25/2012				- Original Version
*
*													
*/


public with sharing class StreetSheetSelectorController {

	private list<StreetSheet__c> sslist;
	
	public String ssid {get;set;}
	public list<selectOption> options {get;set;}
	public String[] itemIds {get;set;}
	public String returl {get;set;}

	public StreetSheetSelectorController() {
		sslist = new list<StreetSheet__c>([
			select Id, Name from StreetSheet__c where OwnerId = :UserInfo.getUserId()
		]);
		options = new list<selectOption>();
		for (StreetSheet__c s : sslist) {
			selectOption so = new selectOption(s.Id, s.Name);
			options.add(so);	
		}
		itemIds = new String[0];
	}
	
	public PageReference addToStreetSheet() {
		
		try {
			Set<String> itemIdSet = new Set<String>(itemIds);
			StreetSheetManager.addItems(ssid, itemIdSet);
		} catch (Exception ex) {
			ApexPages.addMessages(ex);
		}
		
		return new PageReference('/' + ssid);
	}
	
	public PageReference cancel() {
		if (returl != null) {
			return new PageReference(returl);
		}
		String acctprefix = Schema.Sobjecttype.Account.getKeyPrefix();
		String leadprefix = Schema.Sobjecttype.Lead.getKeyPrefix();
		if (itemIds.size() > 1) {
			if (itemIds[0] != null && itemIds[0].startsWith(acctprefix)) {
				return new PageReference('/' + acctprefix);
			} else if (itemIds[0] != null && itemIds[0].startsWith(leadprefix)) {
				return new PageReference('/' + leadprefix);
			}
		} else if (itemIds.size() > 0) {
			return new PageReference('/' + itemIds[0]);
		}
		return new PageReference('/' + acctprefix);
	}
}