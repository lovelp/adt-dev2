/************************************* MODIFICATION LOG ********************************************************************************************
* AccountAppointmentCreation
*
* DESCRIPTION : Controller for the visualforce page handling account appointment creation
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* EJ Bantz                      10/23/2019          - HRM-11053 Home health
*/

public class AccountAppointmentCreation {
    public AccountAppointmentCreation(){
        
    }
    //HRM 8349
    public static string accountAvailable(Lead l){
        string isEventAvaiable;
        try{
            List<account> accList = new List<account>();
            string accLink;
            datetime myDateTime = datetime.now();

            // HRM-11053
            if(String.isNotBlank(l.channel__c) && l.channel__c == Channels.TYPE_HOMEHEALTH){
                // Only search for existing home health accounts
                accList = [SELECT id,TelemarAccountNumber__c,Name,Email__c,Phone,
                            (SELECT id 
                                FROM events 
                                WHERE StartDateTime >: myDateTime 
                                AND Status__c <> :EventManager.STATUS_CANCELED limit 1)
                            FROM account 
                            WHERE addressId__c =: l.AddressID__c 
                            AND Channel__c = 'Home Health' 
                            ORDER BY SalesAppointmentDate__c,email__c DESC 
                            LIMIT 49999];
            } else {
                // Only Search for non home health accounts
                accList = [SELECT id,TelemarAccountNumber__c,Name,Email__c,Phone,
                            (SELECT id 
                                FROM events 
                                WHERE StartDateTime >: myDateTime 
                                AND Status__c <> :EventManager.STATUS_CANCELED limit 1)
                            FROM account 
                            WHERE addressId__c =: l.AddressID__c 
                            AND Channel__c != 'Home Health' 
                            ORDER BY SalesAppointmentDate__c,email__c DESC 
                            LIMIT 49999];
            }

            if(accList.size()>0){
                for(Account acc:accList){
                    if(acc.events!=null && !acc.events.isEmpty() && ((acc.Email__c!=null && l.Email!=null && l.Email == acc.Email__c) || ( acc.Name!=null && l.Name!=null && l.Name == acc.Name)) ){
                        isEventAvaiable = 'An account already exists for this address with an upcoming appointment for Telemar Number -'+acc.TelemarAccountNumber__c+'. Please reach out to your manager.';
                        break;
                    }
                    else if(acc.events!=null && !acc.events.isEmpty()){
                        isEventAvaiable = 'An account already exists for this address with an upcoming appointment for Telemar Number -'+acc.TelemarAccountNumber__c+'. Please reach out to your manager.';
                        break;
                    }
                    else if((acc.Email__c!=null && l.Email!=null && l.Email == acc.Email__c) || ( acc.Name!=null && l.Name!=null && l.Name == acc.Name)) {
                        isEventAvaiable = 'An account already exists for this address with matching contact details (Telemar Number -'+acc.TelemarAccountNumber__c+'). Please reach out to your manager.';
                        break;
                    }
                    else{
                        isEventAvaiable = 'Account Can Be Created';
                    }
                }
            }else{
                isEventAvaiable = 'Account Can Be Created';  
            }
        }
        catch(exception e){
            ADTApplicationMonitor.log(e,null);
        }
        return isEventAvaiable;
    }
    
}