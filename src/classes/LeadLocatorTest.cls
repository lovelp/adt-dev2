@isTest
private class LeadLocatorTest 
{
    
    private static void createRefData()
    {
        System.runAs(TestHelperClass.createAdminUser())
        {
            TestHelperClass.createReferenceDataForTestClasses();   
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
    }
    
    static testMethod void testGetDispositionOptions() {
        
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        Test.startTest();
        
        List<SelectOption> l = llc.getDispositionOptions();
        
        Test.stopTest();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains at least 23 elements
        // Note: it's possible that over time disposition code values will be added or removed
        System.assert(l.size() > 1, 'Expect more than 1 elements in the list');
        //TODO
        // test the actual values returned
            
            
    }
    
    
    static testMethod void testGetDataSourceOptions() {
        
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        Test.startTest();
        
        List<SelectOption> l = llc.getDataSourceOptions();
        
        Test.stopTest();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 8 elements
        System.assert(l.size() > 1, 'Expect more than 1 elements in the list');
        //TODO
        // test the actual values returned
            
            
    }
    
    static testMethod void testGetDiscoReasonOptions() {
        
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        Test.startTest();
        
        List<SelectOption> l = llc.getDiscoReasonOptions();
        
        Test.stopTest();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains at least 102 elements
        // Note: it's possible that over time disco reasons will be added or removed
        System.assert(l.size() > 1, 'Expect more than 1 elements in the list');
        //TODO
        // test the actual values returned
            
            
    }
    
    static testMethod void testGetNewMoverOptions() {
        
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        Test.startTest();
        
        List<SelectOption> l = llc.getNewMoverOptions();
        
        Test.stopTest();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 3 elements
        System.assert(l.size() > 1, 'Expect more than 1 elements in the list');
        // test the actual values returned
            
            
    }
    
    static testMethod void testGetRepAssignedOptionsWithTeam() {
        
        User manager = TestHelperClass.createManagerWithTeam();
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        Test.startTest();
        
        List<SelectOption> l;
        System.runAs(manager) {
            l = llc.getRepAssignedOptions();
        }
        
        Test.stopTest();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 2 elements
        System.assert(l.size() > 1, 'Expect more than 1 elements in the list');
        
        
    }
    
    
    static testMethod void testGetDistancesManager() {
        
        User manager = TestHelperClass.createManagerWithTeam();
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        llc.init();
        Test.startTest();
        
        List<SelectOption> l;
        System.runAs(manager) {
            llc.init();
            l = llc.getDistances();
        }
        
        Test.stopTest();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 6 elements
        System.assert(l.size() > 1, 'Expect more than 1 elements in the list');
            
    }
    
    static testMethod void testGetDistancesSalesRep() {
        
        User salesRep = TestHelperClass.createSalesRepUser();
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        llc.init();
        Test.startTest();
        
        List<SelectOption> l;
        System.runAs(salesRep) {
            llc.init();
            l = llc.getDistances();
        }
        
        Test.stopTest();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 5 elements
        System.assert(l.size() > 1, 'Expect more than 1 elements in the list');
            
    }
    
    static testMethod void testGetLeadNumsAsMgr() {
        
        User salesRep = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :TestHelperClass.createSalesRepUserWithManager().Id];
                                 
        User salesMgr = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :Utilities.getManagersForRole(salesRep.UserRoleId)[0].Id];
                                 
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        List<SelectOption> l;
        
        Test.startTest();   
        
        System.runAs(salesMgr) 
        {
            llc.init();
            l = llc.getLeadNums();
        }
        
        Test.stopTest();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 5 elements
        System.assert(l.size() > 1, 'Expect more than 1 elements in the list');         
    }
    
    static testMethod void testGetLeadNumsAsRep() {
        
        User salesRep = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :TestHelperClass.createSalesRepUserWithManager().Id];
                                 
        User salesMgr = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :Utilities.getManagersForRole(salesRep.UserRoleId)[0].Id];
                                 
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        List<SelectOption> l;
        
        Test.startTest();   
        
        System.runAs(salesRep) 
        {
            llc.init();
            l = llc.getLeadNums();
        }
        
        Test.stopTest();
        
        // expect a non-null list
        System.assert(l != null, 'Expect to receive a non-null list');
        // that contains 3 elements
        System.assert(l.size() > 1, 'Expect more than 1 elements in the list');     
    }
    
    
    static testMethod void testInitQueryStringParamPresentSalesRep() {
        CreateRefData();
        TestHelperClass.createSalesRepUser(); 
        User salesRep = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :TestHelperClass.createSalesRepUserWithManager().Id];
                                 
        User salesMgr = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :Utilities.getManagersForRole(salesRep.UserRoleId)[0].Id];
        
        Account a1;
        Account a2;     
        
        System.runAs(salesMgr) 
        {           
            String id = TestHelperClass.inferPostalCodeID('22039', '1100');
            
            //Create Addresses first
            Address__c addr1 = new Address__c();
            addr1.Latitude__c = 38.72686000000000;
            addr1.Longitude__c = -77.25470100000000;
            addr1.Street__c = '8507 Oak Pointe Way';
            addr1.City__c = 'Fairfax Station';
            addr1.State__c = 'VA';
            addr1.PostalCode__c = '22039';
            addr1.OriginalAddress__c = '8507 Oak Pointe Way, Fairfax Station, VA 22039';            
            insert addr1;
            
            Lead l = new Lead();
            l.LastName = 'William';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '8507 Oak Pointe Way';
            l.SiteCity__c = 'Fairfax Station';      
            l.SiteStateProvince__c = 'VA';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '22039';
            l.PostalCodeID__c = null;
            l.AddressID__c = addr1.Id;
            l.Business_Id__c = null;            
            insert l;
            
            a1 = new Account();
            a1.AddressID__c = addr1.Id;
            a1.Name = 'Unit Test Account 2';
            a1.LeadStatus__c = 'Active';
            a1.UnassignedLead__c = false;
            a1.InService__c = true;
            a1.Business_Id__c = '1100 - Residential';
            a1.ShippingPostalCode = '22039';                
            insert a1;                  
        }
                
        System.runAs(salesRep) 
        {
            String id = TestHelperClass.inferPostalCodeID('22039', '1100');
            
            //Create Addresses first
            Address__c addr2 = new Address__c();
            addr2.Latitude__c = 38.72686000000000;
            addr2.Longitude__c = -77.25470100000000;
            addr2.Street__c = '8506 Oak Pointe Way';
            addr2.City__c = 'Fairfax Station';
            addr2.State__c = 'VA';
            addr2.PostalCode__c = '22039';
            addr2.OriginalAddress__c = '8506 Oak Pointe Way, Fairfax Station, VA 22039';            
            insert addr2;
            
            Lead l = new Lead();
            l.LastName = 'Wallace';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '8506 Oak Pointe Way';
            l.SiteCity__c = 'Fairfax Station';      
            l.SiteStateProvince__c = 'VA';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '22039';
            l.PostalCodeID__c = null;
            l.AddressID__c = addr2.Id;
            l.Business_Id__c = null;            
            insert l;
            
            a2 = new Account();
            a2.AddressID__c = addr2.Id;
            a2.Name = 'Unit Test Account 3';
            a2.LeadStatus__c = 'Active';
            a2.UnassignedLead__c = false;
            a2.InService__c = true;
            a2.Channel__c = 'Resi Direct Sales';
            a2.Business_Id__c = '1100 - Residential';
            a2.ShippingPostalCode = '22039';            
            insert a2;                          
        }       
                
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator?aid=' + a2.Id);
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        
        Test.startTest();   
        
        System.runAs(salesRep) {
            llc.init();
            llc.searcher.SearchType='First Density Increment';
            llc.searchNearby();
            llc.searcher.SearchType='Second Density Increment';
            llc.searchNearby();
        }
        
        Test.stopTest();
        
        System.assertEquals(true, llc.mBShowBackLink, 'Indicator for back link should be true');
        System.assertEquals(false, llc.mDisplayAccountSelectLists, 'Indicator for account select lists should be false');
        System.assertEquals('5', llc.distance, 'Distance should be 5');
        System.assert(llc.locations != null, 'List of MapItems should not be null');
        
        //System.assertEquals(3, llc.locations.size(), 'List of MapItems should contain 4 items');
        System.assert(llc.mapDataPointsStr != null, 'Mapping data as a string should be present');
        
        LeadLocatorMetrics__c[] metrics = [select NumberOfLeadsReturned__c from LeadLocatorMetrics__c where LeadLocatorInvokedBy__c = :salesRep.Id];
        
        System.assertEquals(1, metrics.size(), 'One LeadLocatorMetrics__c record should exist');
        //COMMENTED FOR DEPLOYMENT
        //System.assertEquals(3, metrics[0].NumberOfLeadsReturned__c, 'LeadLocatorMetrics__c records should indicate 3 results returned');
        
        
    }
    
    static testMethod void testInitQueryStringParamPresentManager() {
        CreateRefData();
        TestHelperClass.createSalesRepUser(); 
        User salesRep = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :TestHelperClass.createSalesRepUserWithManager().Id];
                                 
        User salesMgr = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :Utilities.getManagersForRole(salesRep.UserRoleId)[0].Id];
        
        Account a1;
        Account a2;     
        
        System.runAs(salesMgr) 
        {           
            String id = TestHelperClass.inferPostalCodeID('53202', '1100');  
            
            //Create Addresses first
            Address__c addr1 = new Address__c();
            addr1.Latitude__c = 43.0401814;
            addr1.Longitude__c = -87.9029031;
            addr1.Street__c = '700 East Mason Street';
            addr1.City__c = 'Milwaukee';
            addr1.State__c = 'WI';
            addr1.PostalCode__c = '53202';
            addr1.OriginalAddress__c = '700 East Mason Street, Milwaukee, WI 53202';            
            insert addr1;
            
            Lead l = new Lead();
            l.LastName = 'William';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '700 East Mason Street';
            l.SiteCity__c = 'Milwaukee';        
            l.SiteStateProvince__c = 'WI';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '53202';
            l.PostalCodeID__c = null;
            l.AddressID__c = addr1.Id;
            l.Business_Id__c = null;            
            insert l;
            
            a1 = new Account();
            a1.AddressID__c = addr1.Id;
            a1.Name = 'Unit Test Account 2';
            a1.LeadStatus__c = 'Active';
            a1.UnassignedLead__c = false;
            a1.InService__c = true;
            a1.Business_Id__c = '1100 - Residential';
            a1.ShippingPostalCode = '53202';                
            insert a1;                  
        }
                
        System.runAs(salesRep) 
        {
            String id = TestHelperClass.inferPostalCodeID('53202', '1100');
            
            //Create Addresses first
            Address__c addr2 = new Address__c();
            addr2.Latitude__c = 43.0402537;
            addr2.Longitude__c = -87.9016188;
            addr2.Street__c = '800 East Mason Street';
            addr2.City__c = 'Milwaukee';
            addr2.State__c = 'WI';
            addr2.PostalCode__c = '53202';
            addr2.OriginalAddress__c = '800 East Mason Street, Milwaukee, WI 53202';            
            insert addr2;
            
            Lead l = new Lead();
            l.LastName = 'Wallace';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '800 East Mason Street';
            l.SiteCity__c = 'Milwaukee';        
            l.SiteStateProvince__c = 'WI';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '53202';
            l.PostalCodeID__c = null;
            l.AddressID__c = addr2.Id;
            l.Business_Id__c = null;            
            insert l;
            
            a2 = new Account();
            a2.AddressID__c = addr2.Id;
            a2.Name = 'Unit Test Account';
            a2.LeadStatus__c = 'Active';
            a2.UnassignedLead__c = false;
            a2.InService__c = true;
            a2.Business_Id__c = '1100 - Residential';
            a2.ShippingPostalCode = '53202';            
            insert a2;                          
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator?aid=' + a1.Id);
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        
        Test.startTest();
        
        System.runAs(salesMgr) {
            llc.init();
        }
        
        Test.stopTest();
        System.assertEquals(true, llc.mBShowBackLink, 'Indicator for back link should be true');
        System.assertEquals(true, llc.mDisplayAccountSelectLists, 'Indicator for account select lists should be true');
        System.assertEquals('10', llc.distance, 'Distance should be 10');
        System.assert(llc.locations != null, 'List of MapItems should not be null');
        //System.assertEquals(3, llc.locations.size(), 'List of MapItems should contain 3 items');
        System.assert(llc.mapDataPointsStr != null, 'Mapping data as a string should be present');
        
        LeadLocatorMetrics__c[] metrics = [select NumberOfLeadsReturned__c from LeadLocatorMetrics__c where LeadLocatorInvokedBy__c = :salesMgr.Id];
        
        System.assertEquals(1, metrics.size(), 'One LeadLocatorMetrics__c record should exist');
        //COMMENTED FOR DEPLOYMENT
        //System.assert(metrics[0].NumberOfLeadsReturned__c >= 3, 'LeadLocatorMetrics__c records should indicate 3 results returned');
        
        
    }
    
     static testMethod void testInitQueryStringWithCloverLeafAsManager() {
        CreateRefData();
        TestHelperClass.createSalesRepUser(); 
        User salesRep = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :TestHelperClass.createSalesRepUserWithManager().Id];
                                 
        User salesMgr = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :Utilities.getManagersForRole(salesRep.UserRoleId)[0].Id];
        
        Account a1;
        Account a2;     
        
        System.runAs(salesMgr) 
        {           
            String id = TestHelperClass.inferPostalCodeID('53202', '1100');  
            
            //Create Addresses first
            Address__c addr1 = new Address__c();
            addr1.Latitude__c = 43.0401814;
            addr1.Longitude__c = -87.9029031;
            addr1.Street__c = '700 East Mason Street';
            addr1.City__c = 'Milwaukee';
            addr1.State__c = 'WI';
            addr1.PostalCode__c = '53202';
            addr1.OriginalAddress__c = '700 East Mason Street, Milwaukee, WI 53202';            
            insert addr1;
            
            Lead l = new Lead();
            l.LastName = 'William';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '700 East Mason Street';
            l.SiteCity__c = 'Milwaukee';        
            l.SiteStateProvince__c = 'WI';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '53202';
            l.PostalCodeID__c = null;
            l.AddressID__c = addr1.Id;
            l.Business_Id__c = null;            
            insert l;
            
            a1 = new Account();
            a1.AddressID__c = addr1.Id;
            a1.Name = 'Unit Test Account 2';
            a1.LeadStatus__c = 'Active';
            a1.UnassignedLead__c = false;
            a1.InService__c = true;
            a1.Business_Id__c = '1100 - Residential';
            a1.ShippingPostalCode = '53202';                
            insert a1;                  
        }
                
        System.runAs(salesRep) 
        {
            String id = TestHelperClass.inferPostalCodeID('53202', '1100');
            
            //Create Addresses first
            Address__c addr2 = new Address__c();
            addr2.Latitude__c = 43.0402537;
            addr2.Longitude__c = -87.9016188;
            addr2.Street__c = '800 East Mason Street';
            addr2.City__c = 'Milwaukee';
            addr2.State__c = 'WI';
            addr2.PostalCode__c = '53202';
            addr2.OriginalAddress__c = '800 East Mason Street, Milwaukee, WI 53202';            
            insert addr2;
            
            Lead l = new Lead();
            l.LastName = 'Wallace';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '800 East Mason Street';
            l.SiteCity__c = 'Milwaukee';        
            l.SiteStateProvince__c = 'WI';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '53202';
            l.PostalCodeID__c = null;
            l.AddressID__c = addr2.Id;
            l.Business_Id__c = null;            
            insert l;
            
            a2 = new Account();
            a2.AddressID__c = addr2.Id;
            a2.Name = 'Unit Test Account';
            a2.LeadStatus__c = 'Active';
            a2.UnassignedLead__c = false;
            a2.InService__c = true;
            a2.Business_Id__c = '1100 - Residential';
            a2.ShippingPostalCode = '53202';            
            insert a2;                          
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator?aid=' + a2.Id + '&clf=true');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        
        Test.startTest();
        
        System.runAs(salesRep) {
            llc.init();
        }
        
        Test.stopTest();
        System.assertEquals(true, llc.mBShowBackLink, 'Indicator for back link should be true');
        System.assertEquals(false, llc.mDisplayAccountSelectLists, 'Indicator for account select lists should be true');
        System.assertEquals('1', llc.distance, 'Distance should be 1');
        System.assert(llc.locations != null, 'List of MapItems should not be null');
        //System.assertEquals(2, llc.locations.size(), 'List of MapItems should contain 2 items');
        System.assert(llc.mapDataPointsStr != null, 'Mapping data as a string should be present');          
        llc.backToAccount();
    }
    
    
    static testMethod void testSaveStreetSheet() {
        CreateRefData();
        User salesRep = TestHelperClass.createSalesRepUser();       
        Account a1;
        Account a2;
        
        System.runAs(salesRep) {
            a1 = TestHelperClass.createAccountData();
            a2 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator?aid=' + a1.Id);
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        
        System.runAs(salesRep) {
            llc.init();
        }
        
        // simulate user action to select both accounts
        llc.ssiIds = a1.Id + ',' + a2.Id;
        
        Test.startTest();
        
        PageReference newRef = null;
        System.runAs(salesRep) {
            newRef = llc.SaveStreetSheet();
        }
        Test.stopTest();
        
        System.assert(newRef != null, 'Should be navigated to the Street Sheet page');
        
        
        List<StreetSheet__c> createdSS = [Select id from StreetSheet__c where createdbyId =: salesRep.id];
        System.assertEquals(1, createdSS.size(), 'This user should have one Street Sheet record');
        
        //if the assert succeeds, there should be atleast 1 item in the createdSS list
        StreetSheet__c firstSS = createdSS[0];
        List<StreetSheetItem__c> createdSSI = [Select id from StreetSheetItem__c where StreetSheet__c =: firstSS.Id];
        System.assertEquals(2, createdSSI.size(), 'This user should have two Street Sheet Item records');
        
        //Integer actualStreetSheetCount = database.Countquery('select count() from StreetSheet__c where CreatedById = \'' + salesRep.Id + '\'');
        //Integer actualStreetSheetItemCount = database.Countquery('select count() from StreetSheetItem__c where CreatedById = \'' + salesRep.Id + '\'');
        
        //System.assertEquals(1, actualStreetSheetCount, 'This user should have one Street Sheet record');
        //System.assertEquals(2, actualStreetSheetItemCount, 'This user should have two Street Sheet Item records');
        
    }
    
    static testMethod void testProcessAddressEntered() {
        CreateRefData();
        TestHelperClass.createSalesRepUser(); 
        User manager = TestHelperClass.createManagerUser();     
        Account a1;
        Account a2;
        
        System.runAs(manager) {
            a1 = TestHelperClass.createAccountData();
            a2 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        
        System.runAs(manager) {
            llc.init();
        }
        
        // simulate the user entering an address
        llc.addressEntered = 'McLean+VA';
        
        Test.startTest();
        
        System.runAs(manager) {
            PageReference newRef = llc.processAddressEntered();
            System.assert(newRef.getUrl().contains('McLean+VA'), 'Expect to be navigated to the Lead Locator Pre Processor page for the address');
            
        }
        
        Test.stopTest();
        
        
    }
    
    static testMethod void testGoBackToRecord() {
        CreateRefData();
        TestHelperClass.createSalesRepUser(); 
        User manager = TestHelperClass.createManagerUser();     
        Account a;
        
        System.runAs(manager) {
            a = TestHelperClass.createAccountData();
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator');
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        
        MapItem mi = new MapItem(a);
        llc.startingPoint = mi;
        
        Test.startTest();
        
        PageReference newRef = llc.goBackToRecord();
        
        System.assert(newRef.getUrl().contains(a.Id), 'Expect the URL to contain the account id');
        
        Test.stopTest();
    }
   

    static testMethod void testProcessAddress() {
        CreateRefData();
        TestHelperClass.createSalesRepUser(); 
        User salesRep = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :TestHelperClass.createSalesRepUserWithManager().Id];
        
        User salesMgr = [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId 
                                 FROM User
                                 WHERE Id = :Utilities.getManagersForRole(salesRep.UserRoleId)[0].Id];
        
        Account a1;
        Account a2; 
        Address__c addr1;   
        
        System.runAs(salesMgr) 
        {           
            String id = TestHelperClass.inferPostalCodeID('53202', '1100');  
            
            //Create Addresses first
            addr1 = new Address__c();
            addr1.Latitude__c = 43.0401814;
            addr1.Longitude__c = -87.9029031;
            addr1.Street__c = '700 East Mason Street';
            addr1.City__c = 'Milwaukee';
            addr1.State__c = 'WI';
            addr1.PostalCode__c = '53202';
            addr1.OriginalAddress__c = '700 East Mason Street, Milwaukee, WI 53202';            
            insert addr1;
            
            Lead l = new Lead();
            l.LastName = 'William';
            l.Company = 'Brave Heart';
            l.DispositionCode__c = 'Phone - No Answer';
            l.Channel__c = 'Resi Direct Sales';
            l.SiteStreet__c = '700 East Mason Street';
            l.SiteCity__c = 'Milwaukee';        
            l.SiteStateProvince__c = 'WI';
            l.SiteCountryCode__c = 'US';
            l.SitePostalCode__c = '53202';
            l.PostalCodeID__c = null;
            l.AddressID__c = addr1.Id;
            l.Business_Id__c = null;            
            insert l;
            
            a1 = new Account();
            a1.AddressID__c = addr1.Id;
            a1.Name = 'Unit Test Account 2';
            a1.LeadStatus__c = 'Active';
            a1.UnassignedLead__c = false;
            a1.InService__c = true;
            a1.Business_Id__c = '1100 - Residential';
            a1.ShippingPostalCode = '53202';                
            insert a1;      
            
            Address__c addr2 = new Address__c();
            addr2.Latitude__c = 43.0402537;
            addr2.Longitude__c = -87.9016188;
            addr2.Street__c = '800 East Mason Street';
            addr2.City__c = 'Milwaukee';
            addr2.State__c = 'WI';
            addr2.PostalCode__c = '53202';
            addr2.OriginalAddress__c = '800 East Mason Street, Milwaukee, WI 53202';            
            insert addr2;
            
            a2 = new Account();
            a2.AddressID__c = addr2.Id;
            a2.Name = 'Unit Test Account';
            a2.LeadStatus__c = 'Active';
            a2.UnassignedLead__c = false;
            a2.InService__c = true;
            a2.Business_Id__c = '1100 - Residential';
            a2.ShippingPostalCode = '53202';            
            insert a2;              
        }
                
        ApexPages.PageReference ref = new PageReference('/apex/LeadLocator?addr=700 East Mason Street, Milwaukee, WI 53202&lat=' + addr1.Latitude__c + '&lon=' + addr1.Longitude__c);
        test.setCurrentPageReference(ref);
        LeadLocatorController llc = new LeadLocatorController();
        
        Test.startTest();
        
        System.runAs(salesMgr) {
            llc.init();
        }
        
        Test.stopTest();
        
        System.assert(llc.startingPoint != null, 'starting point should not be null');
        System.assertEquals('700 East Mason Street, Milwaukee, WI 53202', llc.startingPoint.rAddress);
        
    }
    
    
}