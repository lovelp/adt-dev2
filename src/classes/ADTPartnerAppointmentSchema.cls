public with sharing class ADTPartnerAppointmentSchema {
    
    //Request
    public class AppointmentSlotrequest{
        public String partnerId;    //Partner001
        public String callId;   //a1G1300000Dzv45
        public String opportunityId;    //0061300001O8exI
        public String partnerRepName;   //John Smith
        public AppointmentRequest appointmentRequest;
    }
    
    public class AppointmentRequest{
        public String existingID;   //00U1B00000iNBKCUA4
        public String startingDate; //2017-10-10
        public String type; //N1
        public String appointmenttime; // null for get appnt. Required for Set appnt
    }
    
    //Response
    public class AppointmentSlotReponse {
        public String callId;
        public String opportunityId;
        public String message;
        public ExistingAppointment existingAppointment;
        public List<AvailableSlots> slots;
        public String errorMessage;
        public Integer errorStatusCode;
    }
    public class ExistingAppointment {
        public String id;   //00U1B00000iNBKCUA4
        public String appointmentDateTime;  //2017-10-24T19:00:00-0400
        public SalesRep salesRep;
    }
    public class SalesRep {
        public String name; //Jone Jones
        public String email;    //jjones@adt.com
        public String phone;    //904-111-2323
    }

    public class AvailableSlots{
        public String appointmentAvailableDate;
        public List<String> appointmentAvailableTimes;
    }
    
    /******************************
     * Set Appointment response Schema
     * *******************************/
     
     public class setAppntResponse{
         public Id opportunityID;
         public setAppointment setAppointment;
         public alternateSlots alternateSlots;
     }
     
     public class setAppointment{
         public boolean success;
         public String message;
         public id id;
         public String startDateTime;
         public String endDateTime;
         public SalesRep SalesRep;
     }
     
     public class alternateSlots{
        public String appointmentAvailableDate;
        public List<String> appointmentAvailableTimes;
     }
}