/************************************* MODIFICATION LOG ********************************************************************************************
* 
* TechUpSellDaoTest
*
* DESCRIPTION :
* Test Coverage for LoanApplicationController, LoanApplicationDataAccess class
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE             Ticket         REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* 
* Jitendra Kothari            12/26/2018            -    - Test Coverage
*/
@isTest (SeeAllData = false)
private class LoanApplicationControllerTest {

    static LoanApplication__c lApp;
    static Account acct;
    static testMethod void testLoanAppCtrl() {
        createTestData();
        Test.startTest();
        Test.setCurrentPageReference(Page.LoanApplicationPage);
        System.currentPageReference().getParameters().put('agentType', 'Field');
        System.currentPageReference().getParameters().put('loanId', lApp.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(acct);
        LoanApplicationController ctrl = new LoanApplicationController(controller);
        
        Boolean flag = ctrl.getViewStateField();
        system.assertEquals(true, flag);
        
        flag = ctrl.getViewStatePhone();
        system.assertEquals(false, flag);
        
        flag = ctrl.getAccountHasActiveApp();
        system.assertEquals(false, flag);
        
        flag = ctrl.getAccountHasApproApp();
        system.assertEquals(false, flag);
        
        LoanApplicationDataAccess dataObject = new LoanApplicationDataAccess();
        
        ctrl.globalLoanId = lApp.Id;
        String inTranJson = '{"transId":"1","appCode":"1","name":"1","paymentType":"1","ccNumber":"123443211234","expMonth":"11","expYear":"2019",' + 
                            '"ccType":"Mastercard","cardBrandSelected":"1","CVVMatch":"1","AVSMatch":"1","tokenId":"1","approvalCode":"1",' + 
                            '"transactionStart":"1","transactionEnd":"1","hostedSecureID":"1","sessionId":"1","status":"1","amount":"100.00",' + 
                            '"customer_id":"1","customer_email":"test@test.com","extToken":"1","customerRefNum":"1","ctiAffluentCard":"1",' + 
                            '"ctiCommercialCard":"1","ctiDurbinExemption":"1","ctiHealthcareCard":"Y","ctiLevel3Eligible":"Y",' + 
                            '"ctiPayrollCard":"Y","ctiPrepaidCard":"Y","ctiPINlessDebitCard":"Y","ctiSignatureDebitCard":"Y",'+
                            '"ctiIssuingCountry":"1","profileProcStatus":"1","profileProcStatusMsg":"1","garbage":"1"}';
        ctrl.transactionDetails = inTranJson;
        ctrl.postPayment();
        
        String existingLoanDetails = LoanApplicationController.getFullLoanDetails(lApp.Id);
        
        //flag = LoanApplicationController.updateLoanDetails(existingLoanDetails);
        //system.assertEquals(true, flag);
        
        LoanApplicationController.getPaymentTechUrl(lApp.Id);
        
        String shippingAddress = dataObject.getAccountAddress(acct);
        //LoanApplicationController.editNewLoanApplication(acct.Id, shippingAddress, shippingAddress, shippingAddress, lApp.Id);
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanDecisionResponse());
        
        LoanApplicationController.submitToZoot(lApp.Id, '123', '123456789');
        
        String result = LoanApplicationController.deactivateApplication(lApp.Id);
        system.assertEquals('Deactivated', result);
        LoanApplicationPaymentController paymentPhoneObject = new LoanApplicationPaymentController();
        paymentPhoneObject.transactionDetails = inTranJson;
        paymentPhoneObject.postPayment();
        LoanApplicationController.saveNewLoanApplication(acct.Id, shippingAddress, shippingAddress, '{}','1000',true,'10/10/1990' );
        LoanApplicationController.editNewLoanApplication(acct.Id, shippingAddress, shippingAddress, shippingAddress,lApp.Id,'1000',true,'10/10/1990'  );
        
        Test.stopTest();
    }
     static testMethod void testLoanAppCtrl1() {
        createTestData1();
        Test.startTest();
        Test.setCurrentPageReference(Page.LoanApplicationPage);
        System.currentPageReference().getParameters().put('agentType', 'Field');
        ApexPages.StandardController controller = new ApexPages.StandardController(acct);
        LoanApplicationController ctrl = new LoanApplicationController(controller);
        
        Boolean flag = ctrl.getViewStateField();
        system.assertEquals(true, flag);
        
        flag = ctrl.getViewStatePhone();
        system.assertEquals(false, flag);
        
        flag = ctrl.getAccountHasActiveApp();
        
        
        flag = ctrl.getAccountHasApproApp();
        system.assertEquals(false, flag);
        
        
        //flag = LoanApplicationController.updateLoanDetails(existingLoanDetails);
        //system.assertEquals(true, flag);
        
        LoanApplicationDataAccess dataObject = new LoanApplicationDataAccess();
        String shippingAddress = dataObject.getAccountAddress(acct);
        LoanApplicationController.saveNewLoanApplication(acct.Id, shippingAddress, shippingAddress, shippingAddress,'1000',true,'10/10/1990' );
        //LoanApplicationController.editNewLoanApplication(acct.Id, shippingAddress, shippingAddress, shippingAddress,lApp.Id,'1000',true,'10/10/1990'  );
        
        Test.stopTest();
    }
    
    public static void createTestData1(){
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootLoanDecisionEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c = 120;
        insert is;

        list<FlexFiConfig__c> flexConfigs = new list<FlexFiConfig__c>();
        FlexFiConfig__c fc1 = new FlexFiConfig__c(value__c='test');
        fc1.name='LoanType';
        flexConfigs.add(fc1);
        
        FlexFiConfig__c fc2 = new FlexFiConfig__c(value__c='test');
        fc2.name='TransactionType';
        flexConfigs.add(fc2);
        
        FlexFiConfig__c fc3 = new FlexFiConfig__c(value__c='test');
        fc3.name='autoBookFlg';
        flexConfigs.add(fc3);
        
        insert flexConfigs;
        
        list<FlexFiMessaging__c> flexMsgs = new list<FlexFiMessaging__c>();
        FlexFiMessaging__c msg1 = new FlexFiMessaging__c(Name = 'FlexFiRiskGradeNotValid');
        msg1.ErrorMessage__c = 'Credit is Run but Risk Grade is Not Approved for Loan Application. Please Proceed to ADT Financing.';
        flexMsgs.add(msg1);
        
        FlexFiMessaging__c msg2 = new FlexFiMessaging__c(Name = 'DOBError');
        msg2.ErrorMessage__c = 'Account Does Not Have a Date Of Birth. Provide Date Of Birth Or Proceed to ADT Financing.';
        flexMsgs.add(msg2);
        
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'NoEmailError');
        msg3.ErrorMessage__c = 'Account Does Not Have an Email. Provide an Email Or Proceed to ADT Financing.';
        flexMsgs.add(msg3);
        
        FlexFiMessaging__c msg4 = new FlexFiMessaging__c(Name = 'TnCAcceptedByMessage');
        msg4.ErrorMessage__c = 'Terms & Condition Accepted by';
        flexMsgs.add(msg4);
        
        insert flexMsgs;
        
        map<String, String> EquifaxSettings = new map<String, String>
                        {
                          'Order Types For Credit Check'=>'N1;R1'
                        , 'Order Types For Credit Check - Renters'=>'N1;R1'
                        , 'Failure Risk Grade'=>'X'
                        , 'Failure Condition Code'=>'APPROV'
                        , 'Default Risk Grade'=>'W'
                        , 'Default Condition Code'=>'CAE1'
                        , 'Employee Risk Grade'=>'U'
                        , 'Employee Condition Code'=>'CME3'     
                        //, 'Days to be considered out of date'=>'90'
                        , 'Days to allow additional check'=>'1'
                        ,'FlexFiApprovalRiskGrade' => 'A;B;C;X'};
        List<Equifax__c> EquifaxSettingList = new List<Equifax__c>(); 
        for( String sN: EquifaxSettings.keySet()){
            Equifax__c eS = new Equifax__c( name = sN );
            eS.value__c = EquifaxSettings.get(sN);
            EquifaxSettingList.add( eS );
        }
        insert EquifaxSettingList;
        //Create Account
        User u; 
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '4';
        insert resale;
        ResaleGlobalVariables__c InteUser = new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User');
        Insert     InteUser;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name='RIFAccountOwnerAlias'; 
        rgv.value__c='ADT RIF';
        insert rgv;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        
        ResaleGlobalVariables__c rgv6 = new ResaleGlobalVariables__c();
        rgv6.name = 'Alternate Pricing Model Order Types';
        rgv6.value__c = 'N1;N2;N3;N4;R1;R2;R3;R4;A1;A2;A3;A4';
        insert rgv6;
        
        List<Postal_Codes__c> pcs = new List<Postal_Codes__c>();
        Postal_Codes__c pc = new Postal_Codes__c (name='221o2', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100', APM_PostalCode__c = true);
        pcs.add(pc);
        /*Postal_Codes__c pc1 = new Postal_Codes__c (name='221o3', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100');
        pcs.add(pc1);*/
        insert pcs;
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        acct = new Account(Name = 'Unit Test Account 2');
        acct.AddressID__c = addr.Id;
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '221o2';
        acct.Email__c = 'abc@gmail.com';
        acct.DOB_encrypted__c = '12/12/1900';
        acct.Equifax_Last_Check_DateTime__c = system.now().addDays(-5);
        acct.EquifaxRiskGrade__c = 'A';
        acct.MMBOrderType__c = 'R1';
        acct.PostalCodeId__c = pc.Id;
        insert acct;
       
    }
    
    public static void createTestData(){
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootLoanDecisionEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c = 120;
        insert is;

        list<FlexFiConfig__c> flexConfigs = new list<FlexFiConfig__c>();
        FlexFiConfig__c fc1 = new FlexFiConfig__c(value__c='test');
        fc1.name='LoanType';
        flexConfigs.add(fc1);
        
        FlexFiConfig__c fc2 = new FlexFiConfig__c(value__c='test');
        fc2.name='TransactionType';
        flexConfigs.add(fc2);
        
        FlexFiConfig__c fc3 = new FlexFiConfig__c(value__c='test');
        fc3.name='autoBookFlg';
        flexConfigs.add(fc3);
        
        insert flexConfigs;
        
        list<FlexFiMessaging__c> flexMsgs = new list<FlexFiMessaging__c>();
        FlexFiMessaging__c msg1 = new FlexFiMessaging__c(Name = 'FlexFiRiskGradeNotValid');
        msg1.ErrorMessage__c = 'Credit is Run but Risk Grade is Not Approved for Loan Application. Please Proceed to ADT Financing.';
        flexMsgs.add(msg1);
        
        FlexFiMessaging__c msg2 = new FlexFiMessaging__c(Name = 'DOBError');
        msg2.ErrorMessage__c = 'Account Does Not Have a Date Of Birth. Provide Date Of Birth Or Proceed to ADT Financing.';
        flexMsgs.add(msg2);
        
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'NoEmailError');
        msg3.ErrorMessage__c = 'Account Does Not Have an Email. Provide an Email Or Proceed to ADT Financing.';
        flexMsgs.add(msg3);
        
        FlexFiMessaging__c msg4 = new FlexFiMessaging__c(Name = 'TnCAcceptedByMessage');
        msg4.ErrorMessage__c = 'Terms & Condition Accepted by';
        flexMsgs.add(msg4);
        
        insert flexMsgs;
        
        map<String, String> EquifaxSettings = new map<String, String>
                        {
                          'Order Types For Credit Check'=>'N1;R1'
                        , 'Order Types For Credit Check - Renters'=>'N1;R1'
                        , 'Failure Risk Grade'=>'X'
                        , 'Failure Condition Code'=>'APPROV'
                        , 'Default Risk Grade'=>'W'
                        , 'Default Condition Code'=>'CAE1'
                        , 'Employee Risk Grade'=>'U'
                        , 'Employee Condition Code'=>'CME3'     
                        //, 'Days to be considered out of date'=>'90'
                        , 'Days to allow additional check'=>'1'
                        ,'FlexFiApprovalRiskGrade' => 'A;B;C;X'};
        List<Equifax__c> EquifaxSettingList = new List<Equifax__c>(); 
        for( String sN: EquifaxSettings.keySet()){
            Equifax__c eS = new Equifax__c( name = sN );
            eS.value__c = EquifaxSettings.get(sN);
            EquifaxSettingList.add( eS );
        }
        insert EquifaxSettingList;
        //Create Account
        User u; 
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '4';
        insert resale;
        ResaleGlobalVariables__c InteUser = new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User');
        Insert     InteUser;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name='RIFAccountOwnerAlias'; 
        rgv.value__c='ADT RIF';
        insert rgv;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        
        ResaleGlobalVariables__c rgv6 = new ResaleGlobalVariables__c();
        rgv6.name = 'Alternate Pricing Model Order Types';
        rgv6.value__c = 'N1;N2;N3;N4;R1;R2;R3;R4;A1;A2;A3;A4';
        insert rgv6;
        
        List<Postal_Codes__c> pcs = new List<Postal_Codes__c>();
        Postal_Codes__c pc = new Postal_Codes__c (name='221o2', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100', APM_PostalCode__c = true);
        pcs.add(pc);
        /*Postal_Codes__c pc1 = new Postal_Codes__c (name='221o3', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100');
        pcs.add(pc1);*/
        insert pcs;
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        acct = new Account(Name = 'Unit Test Account 2');
        acct.AddressID__c = addr.Id;
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '221o2';
        acct.Email__c = 'abc@gmail.com';
        acct.DOB_encrypted__c = '12/12/1900';
        acct.Equifax_Last_Check_DateTime__c = system.now().addDays(-5);
        acct.EquifaxRiskGrade__c = 'A';
        acct.MMBOrderType__c = 'R1';
        acct.PostalCodeId__c = pc.Id;
        insert acct;
        
        lApp = new LoanApplication__c();
        lApp.Account__c = acct.id;
        lApp.BillingAddress__c =addr.id;
        lApp.ShippingAddress__c =addr.id;
        lApp.PreviousAddressLine1__c ='Prev Add Line 1';
        lApp.PreviousAddressLine2__c ='Prev Add Line 2';
        lApp.PreviousAddressPostalCd__c ='27518';
        lApp.PreviousAddressStateCd__c ='NC';
        lApp.PreviousAddressCountryCd__c='USA';
        
       
        insert lApp;
    }
}