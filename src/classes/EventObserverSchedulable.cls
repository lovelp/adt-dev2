/************************************* MODIFICATION LOG ********************************************************************************************
* EventObserverSchedulable
* 
* DESCRIPTION : A schedulable class that initiates the batch class EventObserverBatch with a scope parameter of 1.
*				Since the batch class will initiate a callout and SFDC permits only one callout per batch.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* SHIVKANT VADLAMANI     		4/5/2012			- Original Version  
*													
*/

global class EventObserverSchedulable implements Schedulable 
{
	global void execute(SchedulableContext sc) 
	{		
		// scope parameter is 1 in order to ensure that the batch attempts only 1 callout
		Database.executeBatch( new EventObserverBatch(), 1);			
	}

}