global class CallCampaignMemberBatchProcessor {
    webService static Id callBatchclass(String campaignId) {
        CampaignMemberBatchProcessor cmbp = new CampaignMemberBatchProcessor(campaignId); 
        CampaignDefaults__c cd = CampaignDefaults__c.getorgDefaults();
        database.executeBatch(cmbp,integer.valueof(cd.Campaign_Member_Batch_Size__c));
        campaign cp=new campaign();
        cp.id=campaignid;
        cp.BatchSubmissionDate__c=system.now();
        cp.BatchProcessedDate__c=null;
        update cp;
        return null;
    }
}