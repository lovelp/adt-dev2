/************************************* MODIFICATION LOG ********************************************************************************************
* LeadAssignmentBatchProcessor
*
* DESCRIPTION : Reassigns leads to previous owner if certain conditions are met
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					3/12/2012				- Original Version
*
*													
*/

global class LeadAssignmentBatchProcessor implements Database.Batchable<sObject>, Database.Stateful {
	
	public static String DaysBeforeReassignment = String.escapeSingleQuotes(ResaleGlobalVariables__c.getinstance('DaysBeforeReassignment').value__c);
	
	public String query = 'SELECT Id, OwnerId, PreviousOwner__c, Other_Disposition_Date__c, DateAssigned__c, RecordType.DeveloperName ' +
						  'FROM Lead ' +
						  'WHERE PreviousOwner__c <> null ' +
						  	   ' AND PreviousOwnerActive__c = \'true\' ' +
						  	   ' AND isConverted = false ' +
						  	   ' AND ( DateAssigned__c < LAST_N_DAYS:' + DaysBeforeReassignment + ' OR DateAssigned__c = null ) ' +
						  	   ' AND ( Other_Disposition_Date__c < LAST_N_DAYS:' + DaysBeforeReassignment + ' OR Other_Disposition_Date__c = null ) ' +
						  	   ' AND ( NOT ( Business_Id__c like \'%1200%\' AND Channel__c = \'Business Direct Sales\') )';

	global Database.QueryLocator start( Database.Batchablecontext bc ) {
		system.debug('query = ' + query);
		return Database.getQueryLocator(query);
	}
	
	
	global void execute(Database.BatchableContext bc, List<sObject> scope) {
		
		List<Lead> accts = (List<Lead>)scope;
		for (Lead l : accts) {
			if(l.PreviousOwner__c!=null)
			{
				l.OwnerId = l.PreviousOwner__c;
				//Scenario 9 -Previous Owner set to null in Apex class LeadAssignmentBatchProcessor
				l.PreviousOwner__c=null;
				//End Scenario 9
			}
		}
		update accts;
	}
	
	global void finish(Database.BatchableContext bc) {
		
	}
}