@IsTest
public class RealTimePaymentErrorResponseJ2ATest {
    
    static testMethod void testParse() {
        String json = '{\"errors\": [{'+
            '\"status\": \"500\",'+
            '\"errorCode\": \"-1\",'+
            '\"errorMessage\": \"Internal Processing Error, please contact the Production Support team\"'+
            '}]}';
        RealTimePaymentErrorResponseJ2A obj = RealTimePaymentErrorResponseJ2A.parse(json);
        System.assert(obj != null);
    }
}