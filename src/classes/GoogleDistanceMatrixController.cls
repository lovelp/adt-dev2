/************************************* MODIFICATION LOG ********************************************************************************************
* GoogleDistanceMatrixController
*
* DESCRIPTION : Receives an account and generates a distance matrix using reps with access to this account's territory.
*               - Evaluating schedules for each assigned to this account's territory
*               - Evaluating travel distance between any starting default location or previous appointment
*               - Treats all times on the customer's timezone               
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera               1/27/2016               - Original Version
*
*                                                   
*/
public class GoogleDistanceMatrixController {

   
}