/************************************* MODIFICATION LOG ********************************************************************************************
* ProximitySearcherFactory
*
* DESCRIPTION : Provides a factory method for constructing the correct concrete subclass of ProximitySearcher based
*               characteristics of the user performing a Locator or Cloverleaf function
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani			3/23/2012			- Original Version
*
*													
*/

public with sharing class ProximitySearcherFactory 
{
	 public static ProximitySearcher getSearcher(Boolean isSalesRep, Boolean isManager) 
	 {
	 	if( isSalesRep ||  isManager)
	 	{
	 		 return new FinancialHierarchyProximitySearcher();
	 	}
        else
        {
        	 return new RoleHierarchyProximitySearcher();
        }
     }
}