public class AccountInfo{

    public static String accountid;
    public static Account acc;
    
    public static Account getAccount(){
        Account acc = new Account();
        if(String.isNotBlank(accountId)){
            acc = [SELECT id,PhoneNumber2__c,OFACRestriction__c,Profile_RentOwn__c,Data_Source__c, Lead_Origin__c ,HotLeadAssignedDateTime__c,TelemarAccountNumber__c,owner.email,owner.name,createddate,CreatedById,SiteStreet__c,SiteStreet2__c,SiteCity__c, SiteState__c, SitePostalCode__c,SitePostalCodeAddOn__c,HOA__c,Rep_User__c,Rep_Phone__c,Rep_Name__c,Rep_HRID__c,PostalCodeID__c,PostalCodeID__r.Name,PostalCodeID__r.Town_Lookup__r.name, TMSubTownID__c,ResaleTown__c,TMTownID__c, Business_Id__c, Email__c, name, FirstName__c, LastName__c, ResaleTownNumber__c, AddressID__r.StandardizedAddress__c,AddressID__c, AddressID__r.PostalCode__c,AddressID__r.State__c, Phone, PhoneNumber3__c, CustomerID__c, DisconnectDate__c, LeadSourceNo__c, QueriedSource__c, GenericMedia__c, TelemarLeadSource__c, Chargeback__c, 
                              MMBBillingSystem__c, AccountStatus__c,LastActivityDate__c,MMB_Relo_Customer__c, MMBContractNumber__c, MMBSystemNumber__c,PostalCodeID__r.BusinessID__c, MMB_Disco_Site__c, MMB_Multisite__c, MMB_Past_Due_Balance__c, Channel__c, MMBCustomerNumber__c, MMBSiteNumber__c,MMB_Relo_Site_Number__c, MMB_Relo_Customer_Number__c, MMB_Relo_Billing_System__c, MMBOrderType__c, CPQIntegrationData__c,MMBContractStartDate__c,MMBDisconnectDate__c,
                              MMB_Disco_Count__c,informix_CTC__c,Previous_Billing_Site_Number__c, PreviousBillingCustomerNumber__c, EquifaxApprovalType__c, Equifax_Credit_Score__c, Equifax_Last_Check_DateTime__c, EquifaxRiskGrade__c,Informix_Reinstate__c,Informix_Reinstate_Reason__c,Informix_Relocation__c,Informix_Relocation_Reason__c,Informix_Addon__c,Informix_Addon_Reason__c,Informix_Conversion__c,Informix_Conversion_Reason__c,Informix_Multisite__c,Informix_Multisite_Reason__c,
                              dealer_phone__c,SalesAppointmentDate__c,Rep_User__r.email,TransitionDate1__c,TransitionDate2__c,TransitionDate3__c,TransitionDate4__c,ScheduledInstallDate__c,dealer_name__c,installingDealer__c,OrderTypeChangeText__c,ADTEmployee__c ,MMBCustomerResponse__c,MMBSiteResponse__c, Inactive_Contract__c,DOB_encrypted__c,Customer_Refused_to_provide_DOB__c
                       FROM Account 
                       WHERE id=:accountId];
        }
        return acc;
    
    }
}