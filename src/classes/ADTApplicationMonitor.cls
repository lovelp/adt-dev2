/************************************* MODIFICATION LOG ********************************************************************************************
* ADTApplicationMonitor
*
* DESCRIPTION : Used at the application and business logic layer to closely monitor and report on unexpected or not treated behavior
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera				01/21/2016			  - Original version                                                  
* Siddarth Asokan               05/20/2019            - Created a new log method
*/

public without sharing class ADTApplicationMonitor {
	
	public enum CUSTOM_APP {STANDARD, MATRIX, HERMES, MELISSA, PARTNER, SERVICEPOWER,FLEXFI,COMMERCIAL}
	
	private static List<String> logDetail;
	private static String app;
	private static DateTime startMonitorTime;
	private static DateTime endMonitorTime;
	
	static {
		logDetail = new List<String>();
		logDetail.add('\n Application Monitor started at '+DateTime.now()+'\n');
		startMonitorTime = DateTime.now();
	}
	
	private static void Savelog(ADTLog__c logObj) {
		if( logObj !=null ){
			try{
				if( !utilities.isEmptyOrNull(app) ){
					logDetail.add('Caught by: '+app);
				}
				logObj.Reported_By__c = UserInfo.getUserId();
				endMonitorTime = DateTime.now();
				logObj.Message_Description__c = '';				
				for(String str: logDetail){
					logObj.Message_Description__c += str ;
					logObj.Message_Description__c += '\n';
				}
				logObj.Message_Description__c += '\n Application Monitor ended at '+DateTime.now()+'\n';
				// Time in milliseconds
				logObj.Time__c = endMonitorTime.getTime() - startMonitorTime.getTime();
				ResaleGlobalVariables__c monitorSetting = ResaleGlobalVariables__c.getinstance('ADTApplicationMonitor');
				if( monitorSetting != null && monitorSetting.value__c == '1'){
					insert logObj;
				}
				else{
					system.debug(logObj);
				}				
			}
			catch(Exception err){
				system.debug(logObj);
				system.debug(err + '\n' + err.getStackTraceString());
				// notify by email ?
			}
		}
	}

	public static void log(Exception ex) {
		log(ex, null);
	}
	
	public static void log(Exception ex, ADTLog__c logObj) {
		if( ex != null ){
			if(logObj == null){
				logObj = new ADTLog__c(name=ex.getTypeName());
			}
			logObj.Date_Time__c = DateTime.now();			
			logObj.Message__c = ex.getMessage();
			logObj.Type__c = 'Error';
			logDetail.add('Organization: '+UserInfo.getOrganizationName());
			logDetail.add('Cause: '+ex.getCause());
			logDetail.add('Line Number: '+ex.getLineNumber());
			logDetail.add('Stack Trace: '+ex.getStackTraceString());
			Savelog(logObj);
		}
	}
	
	public static void log(Exception ex, String ApexClassStr, String ApexClassMethodStr) {
			logDetail.add('Apex Class: '+ApexClassStr);
			logDetail.add('Apex Class Method: '+ApexClassMethodStr);
			log(ex, null);
	}
	
	public static void log(Exception ex, String ApexClassStr, String ApexClassMethodStr, CUSTOM_APP appParam) {
			logDetail.add('Apex Class: '+ApexClassStr);
			logDetail.add('Apex Class Method: '+ApexClassMethodStr);
			ADTLog__c logObj = new ADTLog__c(name=ex.getTypeName());
			logObj.Custom_App__c = 	appParam.name();
			log(ex, logObj);
	}
	
	public static void log(String appParam, String msg, boolean commitTrace) {
		ADTLog__c logObj = new ADTLog__c(name=appParam);
		logDetail.add(msg);
		if( commitTrace ){
			Savelog(logObj);
		}
	}
	
	public static void log(String msg, boolean commitTrace) {
		logDetail.add(msg);
		if( commitTrace ){
			Savelog(null);
		}
	}
	
	public static void log(String msg) {
		log(msg, false);
	}
    
    // TCS - added as part of HRM# 6019 RedVentures- AccountUpdate API
    public static void log(String appParam, String msg, boolean commitTrace, CUSTOM_APP customApp) {
        logDetail.clear();
		ADTLog__c logObj = new ADTLog__c(name=appParam);
        logObj.Custom_App__c = 	customApp.name();
        logObj.Date_Time__c = DateTime.now();
        logObj.Message__c = 'Callout Error';
		logDetail.add(msg);
		if( commitTrace ){
			Savelog(logObj);
		}
	}
    // End
    
    // HRM - 9708
    public static void log(String appParam, String msg, String detailedMsg, boolean commitTrace, CUSTOM_APP customApp) {
        logDetail.clear();
		ADTLog__c logObj = new ADTLog__c(name=appParam);
        logObj.Custom_App__c = 	customApp.name();
        logObj.Date_Time__c = DateTime.now();
        logObj.Message__c = msg;
		logDetail.add(detailedMsg);
		if(commitTrace){
			Savelog(logObj);
		}
	}
}