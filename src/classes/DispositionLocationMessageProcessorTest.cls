@isTest
private class DispositionLocationMessageProcessorTest {
	
	private static void createRefData()
	{
		System.runAs(TestHelperClass.createAdminUser())
		{
			TestHelperClass.createReferenceDataForTestClasses();
		}
		TestHelperClass.createReferenceUserDataForTestClasses();
	}
	
	static testMethod void testIsValidInputReturnsFalse() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		Datetime dateParam = Datetime.now();
		Datetime futureDateParam = dateParam.addDays(1);
		
		Test.startTest();
		
		DispositionLocationMessageProcessor dlmp = new DispositionLocationMessageProcessor();
		
		Boolean returnVal = dlmp.isValidInput(null, null, null);
		System.assert(!returnVal, '1. All null input should be invalid');
		System.assert(ApexPages.hasMessages(), '1. The page should have a message');
		
		returnVal = dlmp.isValidInput(LocationDataMessageProcessor.NONE, LocationDataMessageProcessor.NONE, null);
		System.assert(!returnVal, '2. None as all inputs should be invalid');
		System.assert(ApexPages.hasMessages(), '2. The page should have a message');
		
		returnVal = dlmp.isValidInput(salesRep.Id, null, null);
		System.assert(!returnVal, '3. Null date should be invalid');
		System.assert(ApexPages.hasMessages(), '2. The page should have a message');
		
		returnVal = dlmp.isValidInput(salesRep.Id, null, '');
		System.assert(!returnVal, '4. Empty string for date should be invalid');
		System.assert(ApexPages.hasMessages(), '4. The page should have a message');
		
		returnVal = dlmp.isValidInput(salesRep.Id, null, '01/01/2000');
		System.assert(!returnVal, '5. Date more than 250 days ago should be invalid');
		System.assert(ApexPages.hasMessages(), '5. The page should have a message');
		
		returnVal = dlmp.isValidInput(salesRep.Id, null, futureDateParam.format('MM/dd/yyyy'));
		System.assert(!returnVal, '6. Date in the future (' + futureDateParam.format('MM/dd/yyyy') + ') should be invalid');
		System.assert(ApexPages.hasMessages(), '6. The page should have a message');
		
		returnVal = dlmp.isValidInput(salesRep.Id, null, dateParam.format('MM/dd/yyyy'));
		System.assert(!returnVal, '7. String for user and current date but without disposition data should be invalid');
		System.assert(ApexPages.hasMessages(), '7. The page should have a message');
		
		Test.stopTest();
		
	}
	
	static testMethod void testIsValidInputReturnsTrue() {
		CreateRefData();

		User salesRep = TestHelperClass.createSalesRepUser();
		Datetime dateParam = Datetime.now();
		
		System.runAs(salesRep) {
			Account a = TestHelperClass.createAccountData();
			
			a.DispositionCode__c = 'ML - Mailed Letter';
			//a.DispositionDate__c = Datetime.now();
			a.DispositionDate__c = dateParam;
			
			update a;
		}
		
		Test.startTest();
		
		DispositionLocationMessageProcessor dlmp = new DispositionLocationMessageProcessor();
		
		Boolean returnVal = dlmp.isValidInput(salesRep.Id, null, dateParam.format('MM/dd/yyyy'));
		System.assert(returnVal, 'String for user and current date and one recent disposition update should be valid');
		System.assert(!ApexPages.hasMessages(), 'The page should not have a message');
		
		Test.stopTest();
		
	}
	
	static testMethod void testPrepareForMapping() {
		CreateRefData();
		
		User salesRep = TestHelperClass.createSalesRepUser();
		Datetime dateParam = Datetime.now();
		
		Datetime dispositionTime;
		
		System.runAs(salesRep) {
			Account a = TestHelperClass.createAccountData();
			
			a.DispositionCode__c = 'ML - Mailed Letter';
			dispositionTime = Datetime.now();
			a.DispositionDate__c = dispositionTime;
			
			update a;
			
			
		}
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		// one breadcrumb before the disposition time
		LocationItem li1 = new LocationItem();
		li1.pDateTime = dispositionTime.addMinutes(-15);
		li1.rUser = salesRep;
		li1.pLatitude = '37.925924';
		li1.pLongitude = '-76.229562';
		
		itemsList.add(li1);
		
		// one breadcrumb after the disposition time; this one is closer timewise 
		// and should be matched up with the disposition
		LocationItem li2 = new LocationItem();
		li2.pDateTime = Datetime.now();
		li2.rUser = salesRep;
		li2.pLatitude = '38.925924';
		li2.pLongitude = '-77.229562';
		
		itemsList.add(li2);
		
		Test.startTest();
		
		DispositionLocationMessageProcessor dlmp = new DispositionLocationMessageProcessor();
		
		if (dlmp.isValidInput(salesRep.Id, null, dateParam.format('MM/dd/yyyy'))) {
			dlmp.prepareForMapping(itemsList);
			List<LocationDataMessageProcessor.LocationData> tableData = dlmp.getLocationDataList(itemsList);
			System.assert(tableData != null, 'Expect to a non-null list for table data');
			System.assertEquals(1, tableData.size(), 'Expect the list of table data to have one element');
			String mapDataPointsStr = dlmp.getMapData(itemsList);
			System.assert(mapDataPointsStr != null, 'Expect this string ' + mapDataPointsStr + ' to contain data for mapping');
			System.assert(mapDataPointsStr.contains(li2.pLatitude), 'The data for mapping should include the latitude');
			System.assert(mapDataPointsStr.contains(li2.pLongitude), 'The data for mapping should include the longitude');	
		}
		else {
			System.assert(false, 'Input should be valid but was not');
		}
		
		
		Test.stopTest();
		
	}
	
	static testMethod void testPrepareForMappingSoldDispositions() {
		CreateRefData();
		
		User salesRep = TestHelperClass.createSalesRepUser();
		Datetime dateParam = Datetime.now();
		
		Account a1;
		Account a2;
		Account a1After;
		Account a2After;
		System.runAs(salesRep) {
			a1 = TestHelperClass.createAccountData();
			
			a1.DispositionCode__c = 'SD - Sold';
			
			update a1;
			
			a1After = [select DispositionDate__c from Account where Id = :a1.Id];
			
			a2 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
			
			a2.DispositionCode__c = 'SA - Sold / Activated';
			
			update a2;
			
			a2After = [select DispositionDate__c from Account where Id = :a1.Id];
			
		}
		Datetime timeA1 = a1After.DispositionDate__c;
		Datetime timeA2 = a2After.DispositionDate__c;
		
		List<LocationItem> itemsList = new List<LocationItem>();
		
		// one breadcrumb before the first disposition time
		LocationItem li1 = new LocationItem();
		li1.pDateTime = timeA1;
		li1.rUser = salesRep;
		li1.pLatitude = '37.925924';
		li1.pLongitude = '-76.229562';
		
		itemsList.add(li1);
		
		// one breadcrumb before the second disposition time; this one is closer timewise 
		// and should be matched up with the second disposition
		LocationItem li2 = new LocationItem();
		li2.pDateTime = timeA2.addSeconds(2);
		li2.rUser = salesRep;
		li2.pLatitude = '38.925924';
		li2.pLongitude = '-77.229562';
		
		itemsList.add(li2);
		
		// one breadcrumb after the second disposition time but no closer timewise
		LocationItem li3 = new LocationItem();
		li3.pDateTime = timeA2.addSeconds(7);
		li3.rUser = salesRep;
		li3.pLatitude = '39.925924';
		li3.pLongitude = '-78.229562';
		
		itemsList.add(li2);
		
		Test.startTest();
		
		DispositionLocationMessageProcessor dlmp = new DispositionLocationMessageProcessor();
		
		if (dlmp.isValidInput(salesRep.Id, null, dateParam.format('MM/dd/yyyy'))) {
			dlmp.prepareForMapping(itemsList);
			List<LocationDataMessageProcessor.LocationData> tableData = dlmp.getLocationDataList(itemsList);
			System.assert(tableData != null, 'Expect to a non-null list for table data');
			System.assertEquals(2, tableData.size(), 'Expect the list of table data to have one element');
			String mapDataPointsStr = dlmp.getMapData(itemsList);
			System.assert(mapDataPointsStr != null, 'Expect this string ' + mapDataPointsStr + ' to contain data for mapping');
			System.Debug('mapDataPointsStr==='+mapDataPointsStr);
			System.assert(mapDataPointsStr.contains(li1.pLatitude), 'The data for mapping should include the latitude for the first disposition');
			System.assert(mapDataPointsStr.contains(li1.pLongitude), 'The data for mapping should include the longitude for the first disposition');
			//System.assert(mapDataPointsStr.contains(li2.pLatitude), 'The data for mapping should include the latitude for the second disposition');
			//System.assert(mapDataPointsStr.contains(li2.pLongitude), 'The data for mapping should include the longitude for the second disposition');
			//System.assert(!mapDataPointsStr.contains(li3.pLatitude), 'The data for mapping should not include the latitude for the third breadcrumb');
			//System.assert(!mapDataPointsStr.contains(li3.pLongitude), 'The data for mapping should not include the longitude for the third breadcrumb');	
		}
		else {
			System.assert(false, 'Input should be valid but was not');
		}
		
		
		Test.stopTest();
		
	}

}