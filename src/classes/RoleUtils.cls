/************************************* MODIFICATION LOG ********************************************************************************************
* RoleUtils
*
* DESCRIPTION : Extends UserRole hierarchy 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera           06/23/2014              - Original Version
* Magdiel Herrera           09/04/2014              - Added support for approver role label dependency upon business unit
* Siddarth Asokan           02/28/2017              - Added HierarchyLevelGlobal which combines Resi & SMB Hierarchy for field
* Siddarth Asokan           05/30/2018              - Added HierarchyLevelCRD which is similar to HierarchyLevelNSC
* Siddarth Asokan           09/28/2018              - Added HierarchyLevelMatrix which combines HierarchyLevelNSC & HierarchyLevelCRD
*/

public class RoleUtils {
    
    //---------------------------------------------------------------------------------------------------
    //  Inner class used for handling of custom errors
    //---------------------------------------------------------------------------------------------------
    public class RoleUtilsException extends Exception {}
    
    //---------------------------------------------------------------------------------------------------
    //  Inner class used for encapsulating commonly shared information across multiple hierarchy types
    //---------------------------------------------------------------------------------------------------
    public abstract class HierarchyLevel {
        public virtual String getFirstApprover(){
            return MANAGER;
        }
        public virtual String getNextUpperApprover(String approverStr){
            return '';
        }
        public virtual String getNextLowerApprover(String approverStr){
            return '';
        }
        public virtual String getLastApprover(){
            return '';
        }
        public virtual Boolean isDirectAssigneeLevel(String approverLevel, Boolean hasUsers){
            return false;
        }
        public virtual Boolean isTopHierarchyLevel(String approverLevel){
            return true;
        }
        public virtual Boolean isLevelInBranch(String currLevel){
            return true;
        }
    }

    //---------------------------------------------------------------------------------------------------
    //  Inner class used for define a hierarchy for Matrix
    //---------------------------------------------------------------------------------------------------
    public class HierarchyLevelMatrix extends HierarchyLevel {
        public RoleHierarchy__mdt approvalLevelMTD;
        public HierarchyLevelMatrix(User uObj){
            if(roleMetaDataMap != null && roleMetaDataMap.size()>0){
                for(String key: roleMetaDataMap.keySet()){
                    if(String.isNotBlank(uObj.Rep_Team__c) && uObj.Rep_Team__c.equalsIgnoreCase(key))
                        approvalLevelMTD = roleMetaDataMap.get(key);
                }
            }
        }
        
        public override String getFirstApprover(){
            if(approvalLevelMTD != null) return approvalLevelMTD.Level2__c;
            return null;
        }
        public override String getNextLowerApprover(String approverLevel){
            
            String resVal = '';
            if(String.isBlank(approverLevel) || approvalLevelMTD == null){ 
                if(!Test.isRunningTest()){
                throw new RoleUtilsException('Unable to determine level for '+approverLevel);
                }
            }
            else if(approverLevel.equalsIgnoreCase(approvalLevelMTD.Level1__c)) resVal = '';
            else if(approverLevel.equalsIgnoreCase(approvalLevelMTD.Level4__c)) resVal = approvalLevelMTD.Level3__c;
            else if(approverLevel.equalsIgnoreCase(approvalLevelMTD.Level3__c)) resVal = approvalLevelMTD.Level2__c;
            else if(approverLevel.equalsIgnoreCase(approvalLevelMTD.Level2__c)) resVal = approvalLevelMTD.Level1__c;
            else{
                throw new RoleUtilsException('Unable to determine level for '+approverLevel);
            }
            return resVal;
        }
        public override String getNextUpperApprover(String approverLevel){
            String resVal = '';
            if(String.isBlank(approverLevel) || approvalLevelMTD == null){ 
                if(!Test.isRunningTest()){
                throw new RoleUtilsException('Unable to determine level for '+approverLevel);
                }
            }
            else if(approverLevel.equalsIgnoreCase(approvalLevelMTD.Level4__c)) resVal = '';
            else if(approverLevel.equalsIgnoreCase(approvalLevelMTD.Level3__c)) resVal = approvalLevelMTD.Level4__c;
            else if(approverLevel.equalsIgnoreCase(approvalLevelMTD.Level2__c)) resVal = approvalLevelMTD.Level3__c;
            else if(approverLevel.equalsIgnoreCase(approvalLevelMTD.Level1__c)) resVal = approvalLevelMTD.Level2__c;
            else{
                throw new RoleUtilsException('Unable to determine level for '+approverLevel);
            }
            return resVal;
        }
        public override String getLastApprover(){
            if(approvalLevelMTD != null) return approvalLevelMTD.Level4__c;
            return null;
        }
        public override Boolean isDirectAssigneeLevel(String approverLevel, Boolean hasUsers){
            return true && hasUsers;
        }
        public override Boolean isTopHierarchyLevel(String approverLevel){
            if(approvalLevelMTD != null) 
                return (approverLevel == approvalLevelMTD.Level4__c);
            return false;
        }
        public override Boolean isLevelInBranch(String currLevel){
            if(String.isBlank(currLevel) || approvalLevelMTD == null){
                return false;
            }
            return ( currLevel.equalsIgnoreCase(approvalLevelMTD.Level4__c) || currLevel.equalsIgnoreCase(approvalLevelMTD.Level3__c) || currLevel.equalsIgnoreCase(approvalLevelMTD.Level2__c) || currLevel.equalsIgnoreCase(approvalLevelMTD.Level1__c));
        }
    }
    
    //---------------------------------------------------------------------------------------------------
    //  Inner class used for define a hierarchy for Resi & SMB combined
    //---------------------------------------------------------------------------------------------------
    public class HierarchyLevelGlobal extends HierarchyLevel {
        public void sendEmailNotificationToApprovers(Map<String, User> listOfApprovers,List<DOA_Request__c> doaReqList){
            System.debug('### doa request'+doaReqList+'## approver list'+listOfApprovers);
            EmailTemplate templateId = [SELECT Id FROM EmailTemplate where name = 'DOA Notification'];  
            String allLowerApprover='';
            if(doaReqList != null && listOfApprovers != null){          
                for(DOA_Request__c doaR : doaReqList){
                    String salesRepId = doaR.Sales_Rep__c;
                    salesRepId = salesRepId.subString(0,salesRepId.length()-3);
                    allLowerApprover = getLowerApprovalLevels(doaR.DOAApproverLevel__c);
                    System.debug('###rr'+allLowerApprover);
                    for(String approvalLevel : allLowerApprover.split(';')){
                        if(listOfApprovers.get(approvalLevel) != null && listOfApprovers.get(approvalLevel).isActive == true){
                            sendEmailNotification(templateId.Id,listOfApprovers.get(approvalLevel).Id,doaR.Id);
                        }                   
                    }
                    //Send email notification to the Sales Rep
                    sendEmailNotification(templateId.Id,salesRepId,doaR.Id);
                }
            }
        }   
    
        public void sendEmailNotification(Id templateId,Id approvalLevelId,Id doaRequestId){
            try{
                System.debug('inside email notification method');
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                User user = [select email, firstName, lastName from User where id = :approvalLevelId];
                Contact tempContact = new Contact(email = user.email, firstName = user.firstName, lastName = user.lastName);
                insert tempContact;
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTemplateId(templateId);
                email.setSaveAsActivity(false);
                email.setTargetObjectId(tempContact.id);
                email.setWhatId(doaRequestId);
                email.setSenderDisplayName('DOA Notification'); 
                mails.add(email);      
                Messaging.sendEmail(mails); 
                delete tempContact;
            }
           catch (exception e){
                System.debug('Cannot Send Email');
            }
        }
    
        public String getLowerApprovalLevels(String approverLevel){
            String resVal = '';
            if(Utilities.isEmptyOrNull(approverLevel)){ 
                throw new RoleUtilsException('Unable to determine level for '+approverLevel);
            }
            else if(approverLevel.equalsIgnoreCase(REGIONAL_VICE_PRESIDENT))    resVal = AREA_GENERAL_MANAGER+';'+MANAGER;
            else if(approverLevel.equalsIgnoreCase(AREA_GENERAL_MANAGER))       resVal = MANAGER;
            else if(approverLevel.equalsIgnoreCase(MANAGER))                    resVal = '';
            else                                                                resVal = '';
            return resVal;
        }
        public override String getNextLowerApprover(String approverLevel){
            String resVal = '';
            if(Utilities.isEmptyOrNull(approverLevel)){ 
                throw new RoleUtilsException('Unable to determine level for '+approverLevel);
            }
            else if(approverLevel.equalsIgnoreCase(REGIONAL_VICE_PRESIDENT))    resVal = AREA_GENERAL_MANAGER;
            else if(approverLevel.equalsIgnoreCase(AREA_GENERAL_MANAGER))       resVal = MANAGER;
            else if(approverLevel.equalsIgnoreCase(MANAGER))                    resVal = REP;
            else if(approverLevel.equalsIgnoreCase(REP))                        resVal = '';
            else throw new RoleUtilsException('Unable to determine level for '+approverLevel);
            
            return resVal;
        }
        public override String getNextUpperApprover(String approverLevel){
            String resVal = '';
            if(String.isBlank(approverLevel)){ 
                throw new RoleUtilsException('Unable to determine level for '+approverLevel);
            }
            else if(approverLevel.equalsIgnoreCase(REGIONAL_VICE_PRESIDENT))    resVal = '';
            else if(approverLevel.equalsIgnoreCase(AREA_GENERAL_MANAGER))       resVal = REGIONAL_VICE_PRESIDENT;
            else if(approverLevel.equalsIgnoreCase(MANAGER))                    resVal = AREA_GENERAL_MANAGER;
            else if(approverLevel.equalsIgnoreCase(REP))                        resVal = MANAGER;
            else throw new RoleUtilsException('Unable to determine level for '+approverLevel);
            return resVal;
        }
        public override String getLastApprover(){
            return REGIONAL_VICE_PRESIDENT;
        }
        public override Boolean isDirectAssigneeLevel(String approverLevel, Boolean hasUsers){
            Boolean resVal = false;
            if( (approverLevel == MANAGER || approverLevel == AREA_GENERAL_MANAGER) && hasUsers) resVal = true;
            return resVal;
        }
        public override Boolean isTopHierarchyLevel(String approverLevel){          
            return (approverLevel == REGIONAL_VICE_PRESIDENT) ;
        }
        public override Boolean isLevelInBranch(String currLevel){
            if( Utilities.isEmptyOrNull(currLevel)){
                return false;
            }
            return ( currLevel.equalsIgnoreCase(REGIONAL_VICE_PRESIDENT) || currLevel.equalsIgnoreCase(AREA_GENERAL_MANAGER) || currLevel.equalsIgnoreCase(MANAGER));
        }
    }
    
    //---------------------------------------------------------------------------------------------------
    //  Inner class used for encapsulating each node's behavior on a hierarchy tree structure for roles
    //---------------------------------------------------------------------------------------------------
    public class RoleNodeWrapper {
        
        public UserRole                 UserRoleObj     { get; set; }
        public List<RoleNodeWrapper>    ChildNodes      { get; set; }        
        public List<User>               RoleUserList    { get; set; }
        
        public Boolean isLeafNode {
            get {
                return !hasChildren;
            }
        }        
        public Boolean hasChildren {
            get{
                return ChildNodes != null && !ChildNodes.isEmpty();              
            }
        }
        public Boolean hasUsers {
            get {
                return (RoleUserList != null && !RoleUserList.isEmpty());
            }
        }        
        public String RoleName {
            get {
                return UserRoleObj.Name;
            }
        }
        public Id RoleId {
            get {
                return UserRoleObj.Id;
            }
        }
        public String ParentRoleId {
            get{
                return UserRoleObj.ParentRoleId;
            }
        }
        
        public RoleNodeWrapper(){
            RoleUserList = new List<User>();
            ChildNodes = new List<RoleNodeWrapper>();
        }
    }   
    
    //---------------------------------------------------------------------------------------------------
    //  Enum type for restrict values defining a business type passed on when creating a hierarchy
    //---------------------------------------------------------------------------------------------------
    public Enum HierarchyBusinessUnit { SB, Resi }
    
    //---------------------------------------------------------------------------------------------------
    //  Hierarchies used by the utility tool
    //---------------------------------------------------------------------------------------------------
    public static String REP                        = 'Rep';
    public static String MANAGER                    = 'Sales Manager';
    public static String AREA_MANAGER               = 'Area Sales Manager';
    public static String REGIONAL_MANAGER           = 'Regional Sales Director';
    public static String VICE_PRESIDENT             = 'Vice President';
    public static String C_LEVEL                    = 'Direct Reports to CEO';
    public static String AREA_GENERAL_MANAGER       = 'Area General Manager'; 
    public static String REGIONAL_GENERAL_MANAGER   = 'Regional General Manager'; 
    public static String REGIONAL_VICE_PRESIDENT    = 'Regional Vice President';
    
    //---------------------------------------------------------------------------------------------------
    //  Used internally for handling a type of hierarchy specific to that of the business Id on the user
    //---------------------------------------------------------------------------------------------------
    @TestVisible private static HierarchyLevel HierarchyApproverLevel{get;set;} 
    
    //---------------------------------------------------------------------------------------------------
    //  Map to hold user by approver role instead of by user role   
    //---------------------------------------------------------------------------------------------------
    @TestVisible private static Map<String, User> userByManagerLevelMap{get;set;}
    
    //---------------------------------------------------------------------------------------------------
    //  Map to hold roles with Id as the key
    //---------------------------------------------------------------------------------------------------
    @TestVisible private static Map <Id, UserRole> roleUsersMap{get;set;}

    //---------------------------------------------------------------------------------------------------
    //  Map to hold child roles with parentRoleId as the key
    //---------------------------------------------------------------------------------------------------
    @TestVisible private static Map <Id, List<Id>> parentChildRoleMap{get;set;}
    
    //---------------------------------------------------------------------------------------------------
    //  Map to hold users under each role
    //---------------------------------------------------------------------------------------------------
    @TestVisible private static Map<Id, List<Id>>  usersByRoleMap{get;set;}

    //---------------------------------------------------------------------------------------------------
    //  List holds all subordinates
    //---------------------------------------------------------------------------------------------------
    private static List<User> allSubordinates {get; set;}
        
    //---------------------------------------------------------------------------------------------------
    //  UserRole SObject prefix
    //---------------------------------------------------------------------------------------------------
    private static String roleSObjectPrefix {get;set;}    

    //---------------------------------------------------------------------------------------------------
    //  User SObject prefix
    //---------------------------------------------------------------------------------------------------
    private static String userSObjectPrefix {get;set;}    

    private static Map<String, String> roleHierarchyLabel {get;set;}
    
    //---------------------------------------------------------------------------------------------------
    //  Keep track of every user's manager by level
    //---------------------------------------------------------------------------------------------------
    private static Map<String, String> LevelManagerMap {get;set;}
    
    //---------------------------------------------------------------------------------------------------
    //  Global JSON generator
    //---------------------------------------------------------------------------------------------------
    private static JSONGenerator gen {get; set;}
    
    //---------------------------------------------------------------------------------------------------
    //  Map to hold the Custom MetaData for 
    //---------------------------------------------------------------------------------------------------
    private static Map<String, RoleHierarchy__mdt> roleMetaDataMap {get; set;}
    
    /**
     *  Static initialization block where all data structures and information needed is loaded for further use
     */
    static {
        
        initSObjectDescribeInfo();
        
        initNodeInfo();
    }
    
    /**
     *  Init key prefix static reference for UserRole and User
     */
    private static void initSObjectDescribeInfo() {
        roleSObjectPrefix = Schema.Sobjecttype.UserRole.getKeyPrefix();
        userSObjectPrefix = Schema.Sobjecttype.User.getKeyPrefix();
    }
    
    /**
     *  Load information about roles and related users to buil the hierarchy
     */
    private static void initNodeInfo() {
        
        userByManagerLevelMap = new Map<String, User>();
        roleHierarchyLabel = new Map<String, String>();
        LevelManagerMap = new Map<String, String>(); 
        
        // Create a blank list
        allSubordinates = new List<User>();
        // Get the profiles from custom label
        List<String> profileNamesList = new List<String>();
        if(String.isNotBlank(label.RoleUtilsProfileQuery)) 
            profileNamesList = label.RoleUtilsProfileQuery.split(',');
        else
            profileNamesList.add('System Administrator');
            
        // Get role to users mapping in a map with key as role id       
        roleUsersMap = new Map<Id, UserRole>([SELECT Id, Name, parentRoleId, 
                                                (SELECT id, name, UserRoleId, Approver_Level__c, email, IsActive, DelegatedEmail__c, ManagerId, Rep_Team__c
                                                FROM users WHERE IsActive = true AND ProfileId IN ( SELECT ID FROM Profile WHERE Profile.Name IN :profileNamesList)) 
                                              FROM UserRole ORDER BY parentRoleId]);
        
        // Get the meta data type custom settings
        roleMetaDataMap = new Map<String, RoleHierarchy__mdt>();
        for(RoleHierarchy__mdt roleMDT : [SELECT DeveloperName,Level1__c,Level2__c,Level3__c,Level4__c FROM RoleHierarchy__mdt Where Active__c = True]){
            roleMetaDataMap.put(roleMDT.DeveloperName,roleMDT);
        }
        
        // populate parent role - child roles map
        parentChildRoleMap = new Map <Id, List<Id>>();        
        for (UserRole r : roleUsersMap.values()) {
            List<Id> tempList = new List<Id>();
            if (parentChildRoleMap.containsKey(r.parentRoleId)){
                tempList = parentChildRoleMap.get(r.parentRoleId);
            }
            tempList.add(r.Id);
            parentChildRoleMap.put(r.parentRoleId, tempList);
        }
    } 
    
    public static RoleNodeWrapper getParentNodeOfUser (Id userId) {
        return createNode(userId, true);
    }
    
    public static RoleNodeWrapper getRootNodeWithManagersByUser(Id userId){
        RoleNodeWrapper resultNode;
        RoleNodeWrapper n = new RoleNodeWrapper();
        User uObj;
        
        try{
            uObj = [select id, name, UserRoleId, DelegatedEmail__c from user WHERE ID = :userId ];
        }
        catch(Exception err){
            throw new RoleUtilsException('User could not be found.');
        }
        
        if( Utilities.isEmptyOrNull(uObj.UserRoleId) )
            throw new RoleUtilsException('User does not have a role.');
            
        n.RoleUserList.add(uObj);
        n.UserRoleObj = roleUsersMap.get(uObj.UserRoleId);
        
        if( !Utilities.isEmptyOrNull(n.UserRoleObj.parentRoleId)  )
            resultNode = createNode(n.UserRoleObj.parentRoleId, n);
        else    
            resultNode = n;
        return resultNode;  
    }
    
    public static Map<String, User> getRepApproverUserByLevel(Id userId){
        // Allow only for user Ids
        if( !isUser(userId) ) throw new RoleUtilsException('Expecting user Id. Received '+userId);
        // Read User record
        User uObj;
        try{
            uObj = [select id, name, UserRoleId, Business_Unit__c, DelegatedEmail__c, Profile.Name, Rep_Team__c, ManagerID from user WHERE ID = :userId ];
        }
        catch(Exception err){
            throw new RoleUtilsException('User could not be found.');
        }
        return getRepApproverUserByLevel(uObj);
    }

    public static Map<String, User> getRepApproverUserByLevel(User uObj){
        if( Utilities.isEmptyOrNull(uObj.UserRoleId) ) throw new RoleUtilsException('User '+uObj.name+' ['+uObj.Id+'] does not have role assigned.');
        userByManagerLevelMap = new Map<String, User>();
        HierarchyApproverLevel = getUserAsigneeHierarchy(uObj);
        // Start from the rep user and his direct manager
        LevelManagerMap.put(HierarchyApproverLevel.getFirstApprover(), uObj.ManagerID);
        createApprover(HierarchyApproverLevel.getFirstApprover(), roleUsersMap.get(uObj.UserRoleId).parentRoleId);
        return userByManagerLevelMap;
    }
    
    public static Map<String, User> getRepApproverUserByLevel(Id userId, HierarchyBusinessUnit roleBusinessUnit){
        
        // Allow only for user Ids
        if( !isUser(userId) ) throw new RoleUtilsException('Expecting user Id. Received '+userId);
        
        userByManagerLevelMap = new Map<String, User>();
        
        // Read User record
        User uObj;
        String managerRoleId;
        try{
            uObj = [select id, name, UserRoleId, UserRole.Name, DistrictNumber__c, Business_Unit__c, DelegatedEmail__c, Profile.Name, ManagerID from user WHERE ID = :userId ];
            
            String searchStr = '%'+roleBusinessUnit.name()+'%'+uObj.DistrictNumber__c+'%Mgr%';
            
            // search for manager
            for(UserRole ur: [Select ParentRoleId, Name, DeveloperName From UserRole Where Name like :searchStr ]){
                managerRoleId = ur.Id;
            }
            
            if( Utilities.isEmptyOrNull(managerRoleId) ){
                if(!Test.isRunningTest()){
                throw new RoleUtilsException('Manager could not be located.');
                }
            }
            
        }
        catch(RoleUtilsException rExceptionErr){
            throw rExceptionErr;
        }
        catch(Exception err){
            throw new RoleUtilsException('User could not be found.');
        }
        
        HierarchyApproverLevel = getUserAsigneeHierarchy(roleBusinessUnit);
        
        // Start from the rep user and his direct manager
        LevelManagerMap.put(HierarchyApproverLevel.getFirstApprover(), uObj.ManagerID);
        createApprover(HierarchyApproverLevel.getFirstApprover(), managerRoleId);
        
        return userByManagerLevelMap;
    }
    
    public static void createApprover(String approverLevel, String roleId){
        
        Id parentRole;
        User uObj;
        if(roleUsersMap != null  && roleUsersMap.size() > 0 && roleUsersMap.get(roleId) != null){
            parentRole = roleUsersMap.get(roleId).parentRoleId;
                
            for(User u:roleUsersMap.get(roleId).users){
                // If any user has configuration at this level and that setup belongs to the current hierarchy type then use it
                if( String.isNotBlank(u.Approver_Level__c) && HierarchyApproverLevel.isLevelInBranch(u.Approver_Level__c) ){
                    approverLevel = u.Approver_Level__c;
                    uObj = u;
                }
            }
        }
        
        // If configuration was found at this level while traversing then use it, take any user otherwise (except for top levels)
        if( uObj != null ){
            system.debug('uobj is not null');
            userByManagerLevelMap.put(approverLevel, uObj);
            approverLevel = HierarchyApproverLevel.getNextUpperApprover(approverLevel);
        }
        else if (roleUsersMap.get(roleId)!= null && HierarchyApproverLevel.isDirectAssigneeLevel(approverLevel, !roleUsersMap.get(roleId).users.isEmpty())) {
            uObj = roleUsersMap.get(roleId).users[0];
            if(HierarchyApproverLevel instanceof HierarchyLevelMatrix){
                for(User u:roleUsersMap.get(roleId).users){
                    if( u.Id == LevelManagerMap.get(approverLevel)){
                        uObj = u;
                    }
                }
                userByManagerLevelMap.put(approverLevel, uObj);
            }
            approverLevel = HierarchyApproverLevel.getNextUpperApprover(approverLevel);             
        }
                
        if(parentRole != null && String.isNotBlank(approverLevel) ){
            // If no user was found on this iteration keep moving until stop condition reached otherwise save the manager for parent iteration
            if( uObj != null ){
                LevelManagerMap.put(approverLevel, uObj.ManagerID);
            }
            createApprover(approverLevel, parentRole);
        }
    } 
    
    public static HierarchyLevel getUserAsigneeHierarchy(User uObj){
        
        HierarchyLevel resVal;
        
        if( uObj == null)
            throw new RoleUtilsException('Expecting user received '+uObj);
        Boolean allowedDepartment = false;
        if(roleMetaDataMap == null || (roleMetaDataMap != null && roleMetaDataMap.size()==0)){
            system.debug('roleMetaDataMap is null');
            initNodeInfo();
        }
        if(roleMetaDataMap != null && roleMetaDataMap.size()>0){
            for(String key: roleMetaDataMap.keySet()){
                if(String.isNotBlank(uObj.Rep_Team__c) && uObj.Rep_Team__c.equalsIgnoreCase(key))
                    allowedDepartment = true;
            }
        }
		
		if(allowedDepartment){
            // For all matrix users
            resVal = new HierarchyLevelMatrix(uObj);
        }else if((String.isNotBlank(uObj.Business_Unit__c) && (uObj.Business_Unit__c.contains('Resi') || uObj.Business_Unit__c.contains('SB')) || uObj.Business_Unit__c == Channels.TYPE_CUSTOMHOMESALES || uObj.Business_Unit__c == Channels.TYPE_HOMEHEALTH)){
            // Field Users
            resVal = new HierarchyLevelGlobal();
        }
        else{
            throw new RoleUtilsException('Unable to determine a hierarchy for this user');
        }
        
        return resVal;
    }
    
    public static HierarchyLevel getUserAsigneeHierarchy(HierarchyBusinessUnit bu){
        HierarchyLevel resVal;
        if( bu ==  HierarchyBusinessUnit.SB || bu ==  HierarchyBusinessUnit.Resi){
            resVal = new HierarchyLevelGlobal();
        }
        else {
            throw new RoleUtilsException('Unable to determine a hierarchy for this user.');
        }        
        return resVal;
    }
    
    public static String getUpperApproverLevel(String levelStr){
        if(Utilities.isEmptyOrNull(levelStr)) 
            throw new RoleUtilsException('Unable to determine level for '+levelStr);
        if(levelStr.equalsIgnoreCase(C_Level))          return '';
        if(levelStr.equalsIgnoreCase(Vice_President))   return C_Level; 
        if(levelStr.equalsIgnoreCase(Regional_Manager)) return Vice_President;
        if(levelStr.equalsIgnoreCase(Area_Manager))     return Regional_Manager;
        if(levelStr.equalsIgnoreCase(Manager))          return Area_Manager;
        if(!Test.isRunningTest()){
            throw new RoleUtilsException('Unable to determine level for '+levelStr); 
        }
        return null;
    }
    
    public static Id getNextAvailableAsignee(Id userId){
        Id asignee;
        
        // From the user read the role hierarchy one node per level until root is reached
        RoleNodeWrapper rootNode = getRootNodeWithManagersByUser(userId);
        system.debug('Root Node: '+ rootNode);
        Boolean repFound = false;
        while( !repFound ){
            
            // Set our pointer on the manager
            if( rootNode.hasUsers ){
                asignee = rootNode.RoleUserList[0].Id;
            }
            else{
                // keep searching until a manager is found
                while( asignee == null &&  rootNode.hasChildren ){
                    rootNode = rootNode.ChildNodes[0]; 
                    if( rootNode.hasUsers )
                        asignee = rootNode.RoleUserList[0].Id; // If more than one manager take first on the list 
                }
            }
            
            // This user doesn't have active managers in the role hierarchy
            if(asignee == userId)
                break;
            
            // From the manager keep going levels down moving the pointers until the subordinate is reached
            if( asignee != null && rootNode.hasChildren ){
                RoleNodeWrapper subordinateNode = rootNode.ChildNodes[0];
                
                // Find the user return manager
                if( subordinateNode.hasUsers ){
                    for(User u: subordinateNode.RoleUserList){
                        if(u.Id == userId)
                            repFound = true;
                    }
                }
                
                // move the manager pointer 
                rootNode = subordinateNode;
            }
            
            
        }
        return asignee;
    }
    
    public static RoleNodeWrapper getRootNodeWithFullTree(){
        for(UserRole ur: [SELECT Id, Name, parentRoleId FROM UserRole WHERE parentRoleId = null limit 1]){
            return createNode(ur.Id, true);
        }
        return null;
    }
    
    /**
     * Return a JSON string from the hierarchy to be used by third party applications
     * @param Id        UserOrRoleId        From where to start building the tree
     * @param Integer   traversalDirection  If greather than 0 then it returns the entire hierarchy, else just the path to the root
     * @return String   JSON of the hierarchy returned  
     */
    public static String getTreeJSON(Id userOrRoleId, Integer traversalDirection) {
        gen = JSON.createGenerator(true);
        RoleNodeWrapper node;
        if(isUser(userOrRoleId)){
            Map<String, User> uRoleLabelMap = getRepApproverUserByLevel(userOrRoleId); 
            for(String sKey: uRoleLabelMap.keySet()){
                roleHierarchyLabel.put(uRoleLabelMap.get(sKey).Id, sKey);
            }
        }
        
        if(traversalDirection > 0)
         node = createNode(userOrRoleId, true);
        else
         node = getRootNodeWithManagersByUser(userOrRoleId);  
        gen.writeStartArray();
            convertNodeToJSON(node);
        gen.writeEndArray();
        return gen.getAsString();
    }
    
    public static String ApproverLevelStr(String userId){
        String resVal = '';
        if(roleHierarchyLabel!= null && !roleHierarchyLabel.isEmpty() && roleHierarchyLabel.containsKey(userId)){
            resVal = roleHierarchyLabel.get(userId);
        }
        return resVal;
    } 
    
    /**
     *  Create a JSON structure from a tree
     *  @method convertNodeToJSON
     *  @param objRNW Root node reference
     */
    private static void convertNodeToJSON(RoleNodeWrapper objRNW){
        gen.writeStartObject();
        
            gen.writeStringField('nodeLabel', objRNW.RoleName);
            gen.writeStringField('nodeId', objRNW.RoleId);
            
            // Child nodes
            gen.writeFieldName('children');
            gen.writeStartArray();
            if (objRNW.hasUsers || objRNW.hasChildren)
            {
                    if (objRNW.hasUsers)
                    {
                        for (User u : objRNW.RoleUserList)
                        {
                            gen.writeStartObject();
                                gen.writeStringField('nodeLabel', u.Name);
                                gen.writeStringField('nodeId', u.Id);
                                gen.writeStringField('nodeRole', ApproverLevelStr(u.Id));
                            gen.WriteEndObject();
                        }
                    }
                    if (objRNW.hasChildren)
                    {
                        for (RoleNodeWrapper r : objRNW.ChildNodes)
                        {
                            convertNodeToJSON(r);
                        }
                    }
            }
            gen.writeEndArray();
            
        gen.writeEndObject();
    }
    
    /**
     *  Create a user role tree node based on roleId traversing roles up
     *  @param ID               Role ID
     *  @param RoleNodeWrapper  Child node to current role
     *  @return RoleNodeWrapper Reference to the root node of the tree 
     */    
    public static RoleNodeWrapper createNode(Id roleId, RoleNodeWrapper nChild){
        RoleNodeWrapper nParent = new RoleNodeWrapper();
        Id parentRole = roleUsersMap.get(roleId).parentRoleId;
         
        nParent.UserRoleObj = roleUsersMap.get(roleId);
        nParent.RoleUserList.addAll(nParent.UserRoleObj.users);
        nParent.ChildNodes.add(nChild);
        
        if(parentRole == null){
            return nParent;
        }
        
        return createNode(parentRole, nParent);        
        
    }
    
    /**
     *  Create a user role tree node based on roleId or userId traversing roles down
     *  @param ID User or Role ID
     *  @return RoleNodeWrapper Reference to the root node of the tree 
     */
    @TestVisible
    private static RoleNodeWrapper createNode(Id objId, Boolean includeNodeUsers) {
        RoleNodeWrapper n = new RoleNodeWrapper();
        User uObj;
        Id roleId;
        system.debug('calling create node');
        if (isRole(objId)) { // If this node is created from a role ID
            roleId = objId;
        }
        else { // This node needs to be created from a user ID, get the user info and his role
            uObj = [select id, name, UserRoleId, DelegatedEmail__c from user WHERE ID = :objId ];
            roleId = uObj.UserRoleId;
        }
        
        n.UserRoleObj = roleUsersMap.get(roleId);
        if(n.UserRoleObj.users != null && !n.UserRoleObj.users.isEmpty() && includeNodeUsers){
            n.RoleUserList.addAll(n.UserRoleObj.users);
        }
        if(includeNodeUsers == false && !isRole(objId)){
            for(User u: n.UserRoleObj.users){
                if(u.Id == objId) n.RoleUserList.add(u);
            }
        }
            
        
        // If node is created from a role then update subordinates
        system.debug('===>> isRole='+isRole(objId)+' objId='+objId);
        if(isRole(objId)){
            allSubordinates.addAll(n.RoleUserList);         
        }
        
        // This role has child roles
        if (parentChildRoleMap.containsKey(roleId)){
            
            List<RoleNodeWrapper> lst = new List<RoleNodeWrapper>();
            
            // Recursive call to create tree structure below this node
            for (Id rId : parentChildRoleMap.get(roleId)) {
                lst.add(createNode(rId, true));
            }
            
            n.ChildNodes = lst;
            
        }
        
        return n;
    }
    
    public static List<User> getAllSubordinates(Id userId){
        RoleNodeWrapper rNode = createNode(userId, true);
        return allSubordinates;
    }
    
    /**
     *  Check the object type of objId and return 'true' if it's of Role type
     *  @method isRole
     *  @param Id objId
     *  @return Boolean True if it's a role Id, false otherwise
     */    
    public static Boolean isRole (Id objId) {
        return (objId!=null)?((String)objId).startsWith(roleSObjectPrefix):false;
    }
    
    /**
     *  Check the object type of objId and return 'true' if it's of User type
     *  @method isUser
     *  @param Id objId
     *  @return Boolean True if it's a user Id, false otherwise 
     */    
    public static Boolean isUser (Id objId) {
        return (objId!=null)?((String)objId).startsWith(userSObjectPrefix):false;
    }
    
    
 }