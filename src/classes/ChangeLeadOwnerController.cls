/************************************* MODIFICATION LOG ********************************************************************************************
* ChangeLeadOwnerController
*
* DESCRIPTION : Users may initiate a change of ownership for lead(s) from a list view.
*               This class supports the VisualForce page where the user selects the new owner.
*               It is a Controller Extension in the SFDC MVC architecture.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                 DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover              03/27/2012          - Original Version
* Siddarth Asokan           05/20/2019          - Added ChangeOwnershipWithListOfLeads method
* Siddarth Asokan           07/26/2019          - Updated ChangeOwnershipWithListOfLeads method to support National
*/

public with sharing class ChangeLeadOwnerController {
    public Integer NumberOfRecords {get; set;}
    private list<Lead> selectedLeads {get; set;}
    public list<Lead> selectedLeadDetails {get; set;}
    public map<Boolean,List<Lead>> mapLead{get;set;}
    public static map<Id,String>leadIdOwnerMap{get;set;}
    private map<Id, User> AllAvailableSalesRepsForManager = new map<Id, User>();
    public Boolean isLeadAssignmentAllowed{get;set;}
    
    //selected salesrep for assignment
    String selectedSalesRep = null;
    //getter for salesrep selected
    public String getSelectedSalesRep() {
        return selectedSalesRep;
    }
    //setter for salesrep selected
    public void setSelectedSalesRep(String ssr){ 
        this.selectedSalesRep = ssr; 
    }   
    
    //Constructor
    public ChangeLeadOwnerController(ApexPages.StandardSetController controller){
        //Check if Lead Assignment is allowed
        isLeadAssignmentAllowed = false;
        String allowedProfiles = label.AllowedProfilesToRunAssignments;
        for(user loggedInUser : [Select Id, Profile.Name from User Where Id =: UserInfo.getUserId()]){
            if(String.isNotBlank(allowedProfiles) && allowedProfiles.contains(loggedInUser.Profile.Name)){
                isLeadAssignmentAllowed = true;
            }
        }
        selectedLeads = controller.getSelected();
        System.debug('### Selected Leads: '+selectedLeads);
        NumberOfRecords = selectedLeads.size();
        if(NumberOfRecords > 0){
            BuildListOfAvailableReps();
            BuildListOfLeadsSelected();
        }
    }
    
    public Integer getListSizeForFalse(){
        return (mapLead != null && mapLead.size() > 0)? mapLead.get(false).size() : 0;
    }
    
    public Integer getListSizeForTrue(){
        return (mapLead != null && mapLead.size() > 0)? mapLead.get(true).size() : 0;
    }
    
    public pageReference Validate(){
        if(selectedLeads.size() == 0){
            return new pageReference('/apex/ShowError?Error=ANS');
        }else if ( ProfileHelper.isSalesRep()){
            return new pageReference('/apex/ShowError?Error=SCA');
        }
        return null;
    }
    
    private void BuildListOfAvailableReps(){
        //get the user id and the related towns that the user/manager is assigned to
        Id currentUserRoleId = UserInfo.getUserRoleId();
        
        List<Id> matrixedRoles = new List<Id>();
        matrixedRoles = Utilities.getMatrixedRolesForManager(currentUserRoleId);
        List<Id> allRoles = new List<Id>();
        allRoles.add(currentUserRoleId);
        if(matrixedRoles != null){
            allRoles.addAll(matrixedRoles);
        }
        //get all subordinate roles for the user
        List<UserRole> subRoles = [select id, name from UserRole where parentRoleId =: allRoles];
        List<id> allSubordinateRoleIds = new List<Id>();
        for(UserRole ur: subRoles){
            allSubordinateRoleIds.add(ur.id);
        }

        //get all salesreps for the manager
        List<User> allSubordinateUsers = [select id, name, IsActive from User where UserRoleId in : allSubordinateRoleIds];
        for(User u : allSubordinateUsers){
            AllAvailableSalesRepsForManager.put(u.id, u);
        }
    }
    
    public List<SelectOption> getallSalesreps(){
        List<SelectOption> SalesReps = new List<SelectOption>(); 
        SelectOption anOption;
        User u;
        String Label;
        for(Id uid : AllAvailableSalesRepsForManager.KeySet()){
            u = AllAvailableSalesRepsForManager.get(uid);
            if(u.IsActive){
                Label = u.Name;
                anOption = new SelectOption(u.id, Label);
                SalesReps.add(anOption);
            }
        } 
        label = 'Me (' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ')';
        anOption = new SelectOption(UserInfo.getUserId(), label);
        SalesReps.add(anOption);        
        return SalesReps;
    }   
    
    private void BuildListOfLeadsSelected(){
        Set<id> leadIds = new Set<id>();
        selectedLeadDetails = new List<Lead>(); 
        for(Lead led : selectedLeads){
            leadIds.add(led.id);
        }
        
        for(List<Lead> allQueriedLeads : [select id, Name, SiteStreet__c, SiteCity__c, SiteStateProvince__c, SitePostalCode__c, Owner.Name from Lead where Id in : leadIds]){
            for(Lead queriedLead : allQueriedLeads){
                selectedLeadDetails.add(queriedLead);
            }
        }
    }
    
    public pageReference Save(){
        List<Lead> updatedLeads = new List<Lead>();
        if(this.selectedSalesRep == null || this.selectedSalesRep == ''){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a salesrep to change the ownership.');
            ApexPages.addMessage(myMsg);
            return null;
        }else{          
            for(Lead led : selectedLeadDetails){
                led.OwnerId = this.selectedSalesRep;
                //led.ManuallyAssigned__c = true;
                led.AssignedBy__c = UserInfo.getUserId();
                led.DateAssigned__c = DateTime.now();
                led.PreviousOwner__c = null;
                led.NewLead__c = true;
                led.UnassignedLead__c = this.selectedSalesRep == UserInfo.getUserId()?true:false;
                updatedLeads.add(led);
            }
            update updatedLeads;
        }
        return new pageReference('/' + schema.Sobjecttype.Lead.getKeyPrefix() );
    }
    
    // Call this method on page load
    public void onLoad(){
        if(isLeadAssignmentAllowed){
            // Only call the method if lead assignment is allowed
            mapLead = ChangeOwnershipWithListOfLeads(selectedLeads,true);
        }
    }
    
    // Returns a map of success leads & failed leads
    public static map<Boolean,list<Lead>> ChangeOwnershipWithListOfLeads(list<Lead> Leads, Boolean doUpdate){
        leadIdOwnerMap = new map<Id,String>();
        list<Lead> leadList = new list<Lead>();
        list<Lead> successLeads = new list<Lead>();
        list<Lead> errorLeads = new list<Lead>();
        list<Lead> sameOwnerLeads = new list<Lead>();
        map<Boolean,list<Lead>> returnMap = new map <Boolean,list<Lead>>();
        // HRM-10369 National Owner Assignment
        String defaultNationalOwnerId;
        if(ResaleGlobalVariables__c.getinstance('DefaultNationalOwnerUsername') != null){
            String defaultNationalUsername = ResaleGlobalVariables__c.getinstance('DefaultNationalOwnerUsername').value__c;
            for(User NationalOwner: [Select Id from User where UserName =: defaultNationalUsername limit 1]){
                defaultNationalOwnerId = NationalOwner.Id;
            }
        }
        if(doUpdate){
            // If the method is called from the UI
            set<Id> leadIdSet = new set<Id>();
            for(Lead l : Leads){
                // For all other owner assignments
                leadIdSet.add(l.id);
            }
            if(leadIdSet.size() > 0){
                leadList = [SELECT Id,Name,Phone,Email,AssignOwner__c,StandardizedAddress__c,Business_Id__c,Channel__c,Affiliation__c,OwnerId,Owner.Name,postalcodeId__c FROM Lead WHERE Id IN: leadIdSet]; 
            }
        }else{
            // If the method is called from the trigger
            map<String,String> usernameOwnerIdMap = new map<String,String>();
            set<String> usernameSet = new set<String>();
            for(Lead l: Leads){
                // Get the owner username if provided at the time of insert and and put it in set
                if(String.isNotBlank(l.OwnerUsername__c)){
                    usernameSet.add(l.OwnerUsername__c);
                }
            }
            
            if(usernameSet.size() > 0){
                // Get the active owner based on the set of usernames
                for(User u: [Select Id,UserName from User where Username IN :usernameSet And IsActive = true]){
                    usernameOwnerIdMap.put(u.Username,u.Id);
                }
            }
            
            for(Lead finalLead : Leads){
                if(String.isNotBlank(finalLead.OwnerUsername__c) && usernameOwnerIdMap.size() > 0 && usernameOwnerIdMap.containsKey(finalLead.OwnerUsername__c) && finalLead.OwnerId != usernameOwnerIdMap.get(finalLead.OwnerUsername__c)){
                    // If the Owner linked to the username is still active, use it
                    finalLead.OwnerId = usernameOwnerIdMap.get(finalLead.OwnerUsername__c);
                }else if(String.isNotBlank(finalLead.channel__c) && finalLead.channel__c == Channels.TYPE_NATIONAL && String.isNotBlank(defaultNationalOwnerId) && finalLead.OwnerId != defaultNationalOwnerId){
                    // HRM-10369 Assign the National Owner
                    // Assigning the default National Owner here since we do not need to run the below code
                    finalLead.OwnerId = defaultNationalOwnerId;
                }else{
                    // If Owner usename is not provided or if the user is deactivated
                    leadList.add(finalLead);
                }
            }
        }
        if(leadList.size() > 0){
            list<Id>changedOwnerIdList = new list<Id>();
            map<Id,String> changedOwnerIdMap = new map<Id,String>();
            list<Id> PostalCodeIds = new list<Id>();
            map<String, String> PCOwners = new map<String, String>();
            set<String> allTowns = new set<String>();
            map<Id, String> PCTownsMap = new map<Id, String>();
            map<String, String> MTOwners = new map<String, String>();
            list<Id> ownerIdList = new list<Id> ();
            map <Id,String> ownerID_USAAMap = new map <Id,String>();
            String GlobalUnassingnedUser = Utilities.getGlobalUnassignedUser();
            //get all postal codes from accounts + postal code and channels
            for(Lead led : leadList){
                if(led.postalcodeId__c != null){
                    PostalCodeIds.add(led.postalcodeId__c);
                }
            }
            if(PostalCodeIds.size() > 0){
                //get all towns related to the postal codes
                for(Postal_Codes__c pcs : [Select Id, TownId__c, BusinessId__c, TownUniqueId__c from Postal_Codes__c where Id in : PostalCodeIds]){
                    allTowns.add(pcs.TownId__c);
                    PCTownsMap.put(pcs.id, pcs.TownId__c);
                }
                //check of the postal codes are assigned
                for(TerritoryAssignment__c TA : [Select Id, PostalCodeId__c, OwnerId, TerritoryType__c from TerritoryAssignment__c where PostalCodeId__c in : PostalCodeIds AND Owner.isActive = True]){
                    PCOwners.put(TA.PostalCodeId__c + ';' + TA.TerritoryType__c, TA.OwnerId);
                    ownerIdList.add(TA.OwnerId);            
                }
                // Manager Towns
                for(ManagerTown__c MT : [Select id, ManagerId__c, TownId__c, BusinessId__c, Type__c from ManagerTown__c where TownId__c in : allTowns]){
                    for(String mtts : MT.Type__c.split(';')){
                        MTOwners.put(MT.TownId__c + ';' + MT.BusinessId__c + ';' + mtts, MT.ManagerId__c);
                    }
                }
                // USAA
                for (user owners : [Select Id, qualification__c from user where Id in : ownerIdList And IsActive = true]){
                    ownerID_USAAMap.put(owners.Id,owners.qualification__c);
                }
            }
            for(Lead led : leadList){
                system.debug('lead Id: '+led.Id);
                String oldLeadOwner = led.OwnerId;
                String BussId = '';
                if(String.isNotBlank(led.Business_Id__c)){
                    BussId = Channels.getFormatedBusinessId(led.Business_Id__c,Channels.BIZID_OUTPUT.NUM);
                } 
                if(String.isNotBlank(led.Business_Id__c) && led.Business_Id__c.contains('1300') &&  String.isNotBlank(led.channel__c) && led.channel__c == Channels.TYPE_NATIONAL && String.isNotBlank(defaultNationalOwnerId)){
                    // HRM-10369 Assign the National Owner
                    led.OwnerId = defaultNationalOwnerId;
                    led.DateAssigned__c = DateTime.now();
                    led.UnassignedLead__c = false;
                    led.NewLead__c = true; 
                }else if (led.Affiliation__c == 'Builder'){
                    String TnId = PCTownsMap.get(led.postalCodeID__c);
                    string mgrId;
                    if(TnId != null && MTOwners.containsKey(TnID+';1100;Resi Direct Sales')){
                        mgrId = MTOwners.get(TnID+';1100;Resi Direct Sales');
                    }
                    if(PCOwners.containsKey(led.PostalCodeId__c + ';' + Channels.TERRITORYTYPE_BUILDER)){
                        led.OwnerId = PCOwners.get(led.PostalCodeId__c + ';' + Channels.TERRITORYTYPE_BUILDER);
                        led.DateAssigned__c = DateTime.now();
                        led.UnassignedLead__c = false;
                        led.NewLead__c = true;
                        System.debug('led.OwnerId1: '+led.OwnerId);
                    }else if(String.isNotBlank(mgrId)){
                        led.OwnerId = mgrId;
                        led.DateAssigned__c = DateTime.now();
                        led.UnassignedLead__c = false;
                        led.NewLead__c = true;
                        System.debug('led.OwnerId2: '+led.OwnerId);
                    }
                }else if(PCOwners.get(led.PostalCodeId__c + ';' + led.Channel__c) != null){
                    system.debug('Inside the PCOwner');
                    String ownerId = PCOwners.get(led.PostalCodeId__c + ';' + led.Channel__c);
                    led.DateAssigned__c = DateTime.now();
                    led.UnassignedLead__c = false;
                    led.NewLead__c = true;
                    if ((led.Affiliation__c == IntegrationConstants.AFFILIATION_USAA && ownerID_USAAMap.size() > 0 && ownerID_USAAMap.get(ownerId) == IntegrationConstants.AFFILIATION_USAA) || (led.Affiliation__c != IntegrationConstants.AFFILIATION_USAA) ) {
                        led.OwnerId = PCOwners.get(led.PostalCodeId__c + ';' + led.Channel__c);
                        System.debug('Lead Owner Id3: '+led.OwnerId);
                    }else if (MTOwners.get(PCTownsMap.get(led.PostalCodeId__c) + ';' + BussId + ';' + led.Channel__c) != null){
                        led.OwnerId = MTOwners.get(PCTownsMap.get(led.PostalCodeId__c) + ';' + BussId + ';' + led.Channel__c);
                        System.debug('Lead Owner Id4:'+led.OwnerId);
                    }else{
                        led.OwnerId = GlobalUnassingnedUser;
                        System.debug('GlobalUnassingnedUser is the Owner 1');
                    }
                }else if (MTOwners.get(PCTownsMap.get(led.PostalCodeId__c) + ';' + BussId + ';' + led.Channel__c) != null){
                    led.OwnerId = MTOwners.get(PCTownsMap.get(led.PostalCodeId__c) + ';' + BussId + ';' + led.Channel__c);
                    led.DateAssigned__c = DateTime.now();
                    led.UnassignedLead__c = true;       
                    led.NewLead__c = true; 
                    System.debug('led.OwnerId5: '+led.OwnerId);
                }else{
                    led.OwnerId = GlobalUnassingnedUser;
                    led.DateAssigned__c = DateTime.now();
                    led.UnassignedLead__c = true;
                    led.NewLead__c = true;
                    System.debug('GlobalUnassingnedUser is the Owner 2');
                }
                changedOwnerIdList.add(led.OwnerId);
                changedOwnerIdMap.put(led.Id,led.OwnerId);
                // Create a map of success and failures to display in UI
                if(led.OwnerId == GlobalUnassingnedUser){
                    // If owner is global admin error it out and not do a update if called from UI
                    errorLeads.add(led);
                }else if(led.OwnerId == oldLeadOwner){
                    // Add to the final map to show in UI but not do an update since owner is the same
                    sameOwnerLeads.add(led);
                }else{
                    successLeads.add(led);
                }
                // Reset values after assignment
                led.AssignOwner__c = false;
                led.OwnerUsername__c = null;
            }

            if(doUpdate){
                // Only process the below for manual updates
                if(successLeads.size() > 0){
                    // Update the success records
                    database.update(successLeads,false);
                }
                if(changedOwnerIdList.size() > 0){
                    //Adding New Owner in a map to display in UI
                    map<Id,String> ownerIdNameMap = new map<Id,String>();
                    for(User u: [Select Id, name From user Where Id IN: changedOwnerIdList]){
                        ownerIdNameMap.put(u.Id,u.Name);
                    }
                    for(String lId: changedOwnerIdMap.keySet()){
                        leadIdOwnerMap.put(lId,ownerIdNameMap.get(changedOwnerIdMap.get(lId)));
                    }
                }
            }
        }
        // Before returning the final map add the same owner leads to the success list
        successLeads.addAll(sameOwnerLeads);
        returnMap.put(false,errorLeads);
        returnMap.put(true,successLeads);
        system.debug('Final Map: '+ returnMap);
        return returnMap;
    }
}