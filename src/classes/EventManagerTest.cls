/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EventManagerTest {

    static testMethod void testCreate() {
        Event e;
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
        }
        
        e.InstallTechnician__c = '';
        for (Integer i=0; i < 256; i++) {
            e.InstallTechnician__c = e.InstallTechnician__c + 'b';
        }
        
        String err;
        
        test.startTest();
        
        try {
            EventManager.createEvent(e);
            System.assert(false, 'This should not be successful');
        } catch (Exception ex){
            System.assert(ex != null, 'Expect an exception');
        }
        test.stopTest();
        
        
    }
    
    static testMethod void testCreateEvent() {
        Event e;
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
        }
        
        test.startTest();
            EventManager.createEvent(e);
        test.stopTest();
        
        system.assertNotEquals(null, e.Id);
    }
    
    static testMethod void testCreateEventWithAccount() {
        Event e;
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
        }
        
        test.startTest();
            EventManager.createEvent(e, a);
        test.stopTest();
        
        system.assertNotEquals(null, e.Id);
    }
    
    static testMethod void testUpdateEvent() {
        Event e;
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, true);
        }
        
        test.startTest();
            try {
                EventManager.updateEvent(e, a);
                system.assert(true);
            } catch (Exception ex) {
                system.assert(false, ex.getMessage());
            }
        test.stopTest();
    }
    
    static testMethod void testUpdateEventWithAccount() {
        Event e;
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, true);
        }
        
        test.startTest();
            try {
                EventManager.updateEvent(e, a);
                system.assert(true);
            } catch (Exception ex) {
                system.assert(false, ex.getMessage());
            }
        test.stopTest();
    }
    
    public void testCreateSGEventAndConvertLead() {
        Event e;
        User u;
        Lead l;
        Account a;
        String dispcomments = 'Test copy to account';
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createUser(0);
            a = TestHelperClass.createAccountData();    //just doing this so it creates the postal code stuff
            l = TestHelperClass.createLead(current, false, '');
            l.DispositionComments__c = dispcomments;
            l.PostalCode = a.ShippingPostalCode;
            l.Business_Id__c = a.Business_Id__c;
            l.AddressID__c = a.AddressID__c;
            l.SiteStreet__c = a.SiteStreet__c;
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.BusinessID__c = l.Business_Id__c.Contains('1100') ? '1100' : '1200';
            pc.Name = l.PostalCode;
            insert pc;
            l.PostalCodeID__c = TestHelperClass.inferPostalCodeID( l.PostalCode , l.Business_Id__c.Contains('1100') ? '1100' : '1200' );
            l.OwnerId = u.Id;
            insert l;
            e = TestHelperClass.createEvent(l, current, false);
        }
      
      
       StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
       mock.setStaticResource('HttpStaticfakeResp');
       mock.setStatusCode(200);
       mock.setHeader('Content-Type', 'application/xml');
       Test.setMock(HttpCalloutMock.class, mock);
       HTTPResponse res =null;
       
        String err = null;
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                EventManager.createSGAndConvertLead(e, l);
            }
        test.stopTest();
        
        system.assertEquals(null, err);
        system.assertNotEquals(null, e.Id);
    }
    
    static testMethod void testCreateAppointmentForAccount() {
        Event e;
        User u;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
            a.OwnerId = u.Id;
            a.Rep_User__c = u.id;
            update a;
            a = [
                Select Id, Name, FirstName__c, LastName__c, SiteStreet__c, SiteCity__c, SiteState__c,
                    SiteCountryCode__c, SitePostalCode__c, SitePostalCodeAddOn__c, Phone, Email__c,
                    DispositionComments__c, QueriedSource__c, ReferredBy__c, TelemarAccountNumber__c,
                    Type, AccountStatus__c,Rep_User__r.Profile.Name, OutboundTelemarAccountNumber__c
                From Account
                where Id = :a.Id
            ];
        }
        
        String err;
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                err = EventManager.createAppointmentForAccount(e, a);
            }
        test.stopTest();
        
        //system.assertEquals(null, err);
        system.assertNotEquals(null, e.Id);
    }
  
    static testMethod void testCreateAppointmentForAccountStatusCAV() {
        Event e;
        User u;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
            a.OwnerId = u.Id;
            a.Rep_User__c = u.id;
            a.AccountStatus__c = IntegrationConstants.STATUS_CAV;
            update a;
            a = [
                Select Id, Name, FirstName__c, LastName__c, SiteStreet__c, SiteCity__c, SiteState__c,
                    SiteCountryCode__c, SitePostalCode__c, SitePostalCodeAddOn__c, Phone, Email__c,
                    DispositionComments__c, QueriedSource__c, ReferredBy__c, TelemarAccountNumber__c,
                    Type, AccountStatus__c,Rep_User__r.Profile.Name, OutboundTelemarAccountNumber__c
                From Account
                where Id = :a.Id
            ];
        }
        
        String err;
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                err = EventManager.createAppointmentForAccount(e, a);
            }
        test.stopTest();
        
        a = [Select Id, AccountStatus__c from Account where Id = :a.Id];
        
        //system.assertEquals(null, err);
        system.assertNotEquals(null, e.Id);
        //system.assertEquals(null,a.AccountStatus__c);
    }
  
    static testMethod void testUpdateAppointment() {
        Event e;
        User u;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            a.OwnerId = u.Id;
            update a;
            e = TestHelperClass.createEvent(a, u);          
        }
        
        String err;
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                err = EventManager.updateAppointment(e, a);
            }
        test.stopTest();
        
        system.assertEquals(null, err);
    }
    
    static testMethod void testCancelAppointment() {
        Event e;
        User u;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            a.OwnerId = u.Id;
            a.Rep_User__c = u.id;
            update a;
            e = TestHelperClass.createEvent(a, u);
            a.SalesAppointmentDate__c = e.StartDateTime;
            update a;
        }
        
        String err;
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                // Set our custom mock implementation to simulate a webservice response on a test execution context
                Test.setMock(WebServiceMock.class, new ADTSalesMobilityFacadeMockImpl());
                err = EventManager.cancelAppointment(e, a);
                err = EventManager.cancelActivationAppointment(e,a);
            }
        test.stopTest();
        
        /*system.assertEquals(null, err);
        system.assertEquals('Canceled', e.Status__c);
        system.assertEquals(null, a.SalesAppointmentDate__c);
        system.assertNotEquals(null, a.SalesApptCancelledFlag__c);*/
    }
    
    static testMethod void testCreateAdminAppointment() {
        Event e;
        User u;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
            a.OwnerId = u.Id;
            update a;
        }
        
        String err;
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                err = EventManager.createAdminAppointment(e, new List<User>{u});
            }
        test.stopTest();
        
        system.assertEquals(null, err);
        system.assertNotEquals(null, e.Id);
    }
    
    static testMethod void testCreateAdminAppointmentMultiple() {
        Event e;
        User u1;
        User u2;
        User mgr;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            mgr = TestHelperClass.createManagerUser();
            u1 = TestHelperClass.createSalesRepUser();
            u1.Username = 'fakeuser@unittest.com';
            u1.EmployeeNumber__c = '1234fake';
            update u1;
            u2 = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, mgr, false);
        }
        
        String err;
        test.startTest();
            err = EventManager.createAdminAppointment(e, new List<User>{u1, u2});
        test.stopTest();
        
        system.assertEquals(null, err);
    }
    
    static testMethod void testUpdateAdminAppointment() {
        Event e;
        User u;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            a.OwnerId = u.Id;
            a.Rep_User__c = u.id;
            update a;
            e = TestHelperClass.createEvent(a, u);          
        }
        
        String err;
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                err = EventManager.updateAdminAppointment(e, u);
        }
        test.stopTest();
        
        system.assertEquals(null, err);
    }
    
    static testMethod void testCancelAdminAppointment() {
        Event e;
        User u;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            u.EmployeeNumber = '123';
            update u;
            a = TestHelperClass.createAccountData();
            a.OwnerId = u.Id;
            a.Rep_User__c = u.id;
            update a;
            e = TestHelperClass.createEvent(a, u, false);
            e.PostalCode__c = '44131';
            e.Description = 'Test Event';
            e.RecordTypeId = Utilities.getRecordType( RecordTypeName.ADMIN_TIME, 'Event').Id;
            insert e;
        }
        
        String err;
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                            // Set our custom mock implementation to simulate a webservice response on a test execution context
                Test.setMock(WebServiceMock.class, new ADTSalesMobilityFacadeMockImpl());
                err = EventManager.cancelAdminAppointment(e);
            }
        test.stopTest();
        
        system.assertEquals(null, err);
    }
    
    static testMethod void testValidateManagerAdminEvent() {
        
        Event e;
        Event ue;
        User rep;
        User mgr;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            rep = TestHelperClass.createSalesRepUserWithManager();
            mgr = [Select Id from User where username='unitTestSalesMgr2@testorg.com' limit 1];
            a = TestHelperClass.createAccountData();
            ue = TestHelperClass.createEvent(a, rep, false);
            ue.StartDateTime = system.now().addHours(-2);
            ue.EndDateTime = system.now().addHours(-1);
            insert ue;
            e = TestHelperClass.createEvent(a, mgr, false);
            e.RecordTypeId = Utilities.getRecordType( RecordTypeName.ADMIN_TIME , 'Event').Id;
            insert e;
        }
        
        String err;
        test.startTest();
            system.runas(mgr) {
                err = EventManager.validateEvent(e, new List<User>{rep});
            }
        test.stopTest();
        
        system.assertEquals(null, err);
    }
    
    static testMethod void testValidateRepAdminEvent() {
        
        Event e;
        Event ue;
        User rep;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            rep = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            ue = TestHelperClass.createEvent(a, rep, false);
            ue.StartDateTime = system.now().addHours(-2);
            ue.EndDateTime = system.now().addMinutes(-90);
            ue.CreatedById = rep.Id;
            insert ue;
            e = TestHelperClass.createEvent(a, rep, false);
            e.RecordTypeId = Utilities.getRecordType( RecordTypeName.ADMIN_TIME , 'Event').Id;
            //e.CreatedById = rep.Id;
            //insert e;
        }
        
        system.debug('e = ' + e);
        
        String err;
        test.startTest();
            system.runas(rep) {
                err = EventManager.validateEvent(e);
            }
        test.stopTest();
        
        system.assertEquals(null, err);
    }
    
    static testMethod void testValidateSGEvent() {
        
        Event e;
        Event ue;
        User rep;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            rep = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            ue = TestHelperClass.createEvent(a, rep, false);
            ue.StartDateTime = system.now().addHours(-4);
            ue.EndDateTime = ue.StartDateTime.addHours(1);
            //insert ue;
            e = TestHelperClass.createEvent(a, rep, false);
            e.EndDateTime = e.StartDateTime.addHours(3);
            e.RecordTypeId = Utilities.getRecordType( RecordTypeName.SELF_GENERATED_APPOINTMENT , 'Event').Id;
            insert e;
        }
        
        String err;
        test.startTest();
            system.runas(rep) {
                err = EventManager.validateEvent(e);
            }
        test.stopTest();
        
        system.assertEquals(null, err);
    }
    
    static testMethod void test2HourAdminTime() {
        Event e;
        User rep;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            rep = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, rep, false);
            e.RecordTypeId = Utilities.getRecordType( RecordTypeName.ADMIN_TIME , 'Event').Id;
            e.StartDateTime = DateTime.now();
            e.EndDateTime = e.StartDateTime.addHours(3);
        }
        
        String err;
        test.startTest();
            system.runas(rep) {
                err = EventManager.validateEvent(e);
            }
        test.stopTest();
        
        system.assertNotEquals(null, err);
    }
    
    static testMethod void testDeleteEvent() {
        Event e;
        User rep;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            rep = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, rep);
        }
        
        String err;
        test.startTest();
            EventManager.deleteEvent(e);
        test.stopTest();
        
        system.assertEquals(null, err);
    }
    
    static testMethod void testError() {
        Event e;
        Event e2;
        User u;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u, false);
            e2 = TestHelperClass.createEvent(a, u);
        }
        
        String err;
        String err2;
        test.startTest();
            e.TaskCode__c = '1234';
            e.InstallTechnician__c = '';
            for (Integer i=0; i < 256; i++) {
                e.InstallTechnician__c = e.InstallTechnician__c + 'b';
            }
            err = EventManager.createAppointmentForAccount(e, a);
            e2.TaskCode__c = '1234';
            e2.InstallTechnician__c = '';
            for (Integer i=0; i < 256; i++) {
                e2.InstallTechnician__c = e2.InstallTechnician__c + 'b';
            }
            // Set our custom mock implementation to simulate a webservice response on a test execution context
            Test.setMock(WebServiceMock.class, new ADTSalesMobilityFacadeMockImpl());
            err2 = EventManager.updateAppointment(e2, a);
        test.stopTest();
        
        system.assertNotEquals(null, err);
        system.assertNotEquals(null, err2);
    }
   
    static testMethod void testSetObservedDuration()
    {
        Event e;
        User u;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) 
        {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u);
        }
        
        test.startTest();
            long duration = 20; 
            EventManager.setObservedDuration(e, duration, false);
            System.assertEquals( e.ObservedDuration__c, EventManager.LESS_THAN_30_MINS);
            
            duration = 40;
            EventManager.setObservedDuration(e, duration, false);
            System.assertEquals( e.ObservedDuration__c, EventManager.BETWEEN_30_AND_60_MINS);
            
            duration = 80;
            EventManager.setObservedDuration(e, duration, false);
            System.assertEquals( e.ObservedDuration__c, EventManager.BETWEEN_60_AND_90_MINS);
            
            duration = 100;
            EventManager.setObservedDuration(e, duration, false);
            System.assertEquals( e.ObservedDuration__c, EventManager.MORE_THAN_90_MINS);
            
            duration = null;
            EventManager.setObservedDuration(e, duration, false);
            System.assertEquals( e.ObservedDuration__c, EventManager.NOT_AT_LOCATION);
            
            duration = 80;
            EventManager.setObservedDuration(e, duration, true);
            System.assertEquals( e.ObservedDuration__c, EventManager.DATA_NOT_AVAILABLE);
            
        test.stopTest();                
    }
    
    static testMethod void testSetOnTimeIndicator()
    {
        Event e;
        User u;
        Account a;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) 
        {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            e = TestHelperClass.createEvent(a, u);
        }
        
        test.startTest();
            long delay = 2; 
            EventManager.setOnTimeIndicator(e, delay, false);
            System.assertEquals( e.OnTimeIndicator__c, EventManager.ONTIME);
            
            delay = 8;
            EventManager.setOnTimeIndicator(e, delay, false);
            System.assertEquals( e.OnTimeIndicator__c, EventManager.LATE_BY_5_TO_10_MINS);
            
            delay = 15;
            EventManager.setOnTimeIndicator(e, delay, false);
            System.assertEquals( e.OnTimeIndicator__c, EventManager.LATE_BY_10_TO_20_MINS);
            
            delay = 30;
            EventManager.setOnTimeIndicator(e, delay, false);
            System.assertEquals( e.OnTimeIndicator__c, EventManager.LATE_BY_MORE_THAN_20_MINS);
            
            delay = null;
            EventManager.setOnTimeIndicator(e, delay, false);
            System.assertEquals( e.OnTimeIndicator__c, EventManager.NOT_AT_LOCATION);
            
            delay = 30;
            EventManager.setOnTimeIndicator(e, delay, true);
            System.assertEquals( e.OnTimeIndicator__c, EventManager.DATA_NOT_AVAILABLE);
            
        test.stopTest();                
    }
    
    static testMethod void testDeleteException() {
        String err;
        test.startTest();
            err = EventManager.deleteEvent(null);
        test.stopTest();
        
        system.assertNotEquals(null, err);
    }
    
    static testMethod void testGetEvent() {
        User u;
        Event e;
        User current = [Select Id from User where Id = :UserInfo.getUserID()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            e = TestHelperClass.createEvent(u);
        }
        
        Event evt;
        test.startTest();
            evt = EventManager.getEvent(e.Id);
        test.stopTest();
        
        system.assertEquals(e.Id, evt.Id);
    }
    
    static testMethod void testUpdateObservedValues() {
        Account a1, a2;
        User u;
        Event e;
        Address__c addr1, addr2;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            a1 = TestHelperClass.createAccountData();
            u = TestHelperClass.createUser(0);
            e = TestHelperClass.createEvent(a1, u);
            addr1 = [Select Id, PostalCode__c, Latitude__c, Longitude__c from Address__c where Id = :a1.AddressID__c];
            // 37.3731 -121.999
            addr1.Latitude__c = 37.3732;
            addr1.Longitude__c = -122;
            update addr1;
            
            addr2 = addr1.clone(false);
            addr2.Latitude__c = 37.3733;
            addr2.Longitude__c = -122.001;
            addr2.OriginalAddress__c = 'Fake Address';
            addr2.Street__c = 'fake street';
            insert addr2;
            a2 = TestHelperClass.createAccountData(addr2, a1.PostalCodeID__c, true);
        }
        
        test.startTest();
            system.runas(u) {
                IntegrationSettings__c is = IntegrationSettings__c.getInstance();
                is.LocationDataProvider__c = 'MockProvider';
                if (is.Id == null) {
                    insert is;
                }
                EventManager.updateObservedValues(new list<Event>{e});
           }
        test.stopTest();
        
        Event econfirm = [Select Id, ObservedDuration__c from Event where Id = :e.Id];
        system.assertEquals( EventManager.LESS_THAN_30_MINS, econfirm.ObservedDuration__c);
    }
    
    static testMethod void testCancelEvent()
    {
        Event e1;
        Event e2;
        RecordType admintime = Utilities.getRecordType(RecordTypeName.ADMIN_TIME, 'Event');
        RecordType sg = Utilities.getRecordType(RecordTypeName.SELF_GENERATED_APPOINTMENT, 'Event');
        Account a;
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            a.OwnerId = u.Id;
            a.Rep_User__c = u.id;
            update a;
            e1 = TestHelperClass.createEvent(a, u, false);
            e1.RecordTypeId = admintime.Id;
            insert e1;
            e2 = TestHelperClass.createEvent(a, u, false);
            e2.RecordTypeId = sg.Id;
            insert e2;
        }
        
        List<Id> eids = new List<Id>{e1.Id, e2.Id};
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                Test.setMock(WebServiceMock.class, new ADTSalesMobilityFacadeMockImpl());
                EventManager.cancelEvent(e1);
                EventManager.cancelEvent(e2);
            }
        test.stopTest();
        
        List<Event> econfirm = new List<Event>([select Id, Status__c from Event where Id IN :eids]);
        
        
    }

    static testMethod void testGetInstallAppointmentForAccount()
    {
        Event e1;
        RecordType install = Utilities.getRecordType(RecordTypeName.INSTALL_APPOINTMENT, 'Event');
        Account a;
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        Event evt;
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            a.OwnerId = u.Id;
            update a;
            e1 = TestHelperClass.createEvent(a, u, false);
            e1.RecordTypeId = install.Id;
            insert e1;
        }
        
        TestHelperClass.setupTestIntegrationSettings(u);
        test.startTest();
            system.runas(u) {
                evt = EventManager.getInstallAppointmentForAccount(a.Id);
                EventManager.syncEventAccountOwner(new List<Account>{a}) ;

            }
        test.stopTest();
        
        System.assert(evt != null, 'An event record should exist');
    }
    
    
    static testMethod void testgetAcctCityState() 
    {
        
        String sTempCityState = null;
        String err = 'error';
        String subCityState = null;
        Account a1, a2;
        User u;
        Event e;
        Address__c location, loc2;
        User current = [select Id from User where Id=:UserInfo.getUserId()];
        
        system.runAs(current) {        
            a1 = TestHelperClass.createAccountData();
            u = TestHelperClass.createUser(0);
            e = TestHelperClass.createEvent(a1, u);
            location = [Select Id, City__c, State__c from Address__c where Id = :a1.AddressID__c];
            location.City__c = 'Plantation';
            location.State__c = 'FL';
            subCityState = ' - ' + location.City__c + ', ' + location.State__c;
            update location;
            
            
            loc2 = location.clone(false);
            loc2.City__c = 'Plantation';
            loc2.State__c = 'FL';
            loc2.OriginalAddress__c = 'Test Address';
            loc2.Street__c = 'test street';
            insert loc2;
            a2 = TestHelperClass.createAccountData(loc2, a1.PostalCodeID__c, true);

        }
            
        test.startTest();
        system.runAs(u){
            sTempCityState = EventManager.getAcctCityState(a2.Id);
            if (sTempCityState == null){
                system.assert(null, err);
            }
        }
        test.stopTest();
        system.assertEquals(sTempCityState, subCityState);
    }

}