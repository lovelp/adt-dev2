/************************************* MODIFICATION LOG ********************************************************************************************
* CPQMarketingReportController
*
* DESCRIPTION : Controller for the visualforce page handling custom CPQ reporting for marketing 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel herrera             	04/17/2015			- Original Version
*
*													
*/

public class CPQMarketingReportController {

	//---------------------------------------------------------------------------------------------------
	//	Custom wrapper to encapsulate report rows at a single level for rendering
	//---------------------------------------------------------------------------------------------------
	public class  QuoteInfoRow {
		public Integer rowClass {get;set;}
		public DateTime CreatedDate {get;set;}
		public DateTime SubmittedDate {get;set;}
		public String getCreatedDateStr() {
			return (CreatedDate!=null)?CreatedDate.format():'';
		}
		public String getSubmittedDateStr() {
			return (SubmittedDate!=null)?SubmittedDate.format():'';
		}
		public String LeadOrigin {get;set;}
		public String QueriedSource {get;set;}
		public String TelemarLeadSource {get;set;}
		public String GenericMedia {get;set;}
		public String CustomerNumber {get;set;}
		public String DataSource {get;set;}
		public String CampaignStr {get;set;}
		public String ResiChangeover {get;set;} 
		public String QuoteName {get;set;}
		public String QuoteNumber {get;set;}
		public String Status {get;set;}
		public String PackageStr {get;set;}
		public String SalesType {get;set;}
		public String PromoCode {get;set;}
		public String Territory {get;set;}
		public String PhoneFieldStatus {get;set;}
		public String County {get;set;}
		public String City  {get;set;}
		public String State {get;set;}
		public String ZipCode {get;set;}
		public String RepDistrict {get;set;}
		public String TelemarAccountNumber {get;set;}
		public String DNIS {get;set;}
		public Decimal BasePrice {get;set;}
		public Decimal AddOnPrice {get;set;}
		public Decimal OtherAddOnPrice {get;set;}
		public Decimal CorpDiscount {get;set;}
		public Decimal DOADiscount {get;set;}
		public Decimal InstallPrice {get;set;}
		public Decimal BaseMonitoring {get;set;}
		public Decimal QSPPrice {get;set;}
		public Decimal AdditionalMonitoring {get;set;}
		public Decimal AdditionalQSP {get;set;}
		public Decimal CorpDiscountMonthly {get;set;}
		public Decimal MonthlyDOA {get;set;}
		public Decimal QSPDOA {get;set;}
		public Decimal FinalRMR {get;set;}
		public QuoteInfoRow(){
			BasePrice = 0.0;
			AddOnPrice = 0.0;
			OtherAddOnPrice = 0.0;
			CorpDiscount = 0.0;
			DOADiscount = 0.0;
			InstallPrice = 0.0;
			BaseMonitoring = 0.0;
			QSPPrice = 0.0;
			AdditionalMonitoring = 0.0;
			AdditionalQSP = 0.0;
			CorpDiscountMonthly = 0.0;
			MonthlyDOA = 0.0;
			QSPDOA = 0.0;
			FinalRMR = 0.0;
		}
	}
	
	//---------------------------------------------------------------------------------------------------
	//	Report Filters
	//---------------------------------------------------------------------------------------------------
	public String startDate {get;set;}
	public String endDate {get;set;}
	public String filterBy {get;set;}
	
	//---------------------------------------------------------------------------------------------------
	//	Report Details
	//---------------------------------------------------------------------------------------------------
	public Integer ReportRows {get;set;} 
	public Boolean renderDataRows {get;set;}
	
	//---------------------------------------------------------------------------------------------------
	//	Constructor
	//---------------------------------------------------------------------------------------------------
	public CPQMarketingReportController(){
		startDate = Apexpages.currentPage().getParameters().get('sd');
		endDate = Apexpages.currentPage().getParameters().get('ed');
		String filterByParam = Apexpages.currentPage().getParameters().get('f');
		filterBy = (Utilities.isEmptyOrNull(filterByParam))?'CreatedDate':filterByParam;
		renderDataRows = false;
		ReportRows = 0;
	}
	
	/**
	 *	Lazy load request for rendering reports rows in read only mode
	 *	@method	getquoteList		Called from a repeat placed on the page which renders when requested from an action 
	 *	@return List<QuoteInfoRow>	Rows produced for this report 
	 */ 
	public List<QuoteInfoRow> getquoteList() {
			List<QuoteInfoRow> resVal = new List<QuoteInfoRow>(); 
			ReportRows = 0;
			QuoteInfoRow temp = new QuoteInfoRow(); 
			try{
				// Package discounts
				Map<String, Quote_Activity__c> qaMap = new Map<String, Quote_Activity__c>(); 
				Set<String> quoteIds = new Set<String>();  

				// Dynamic SOQL definition
				String soqlJobWhereStr =  ' WHERE '+filterBy+' >= '+startDate+' AND '+filterBy+' <= '+endDate+' AND ParentQuote__r.scpq__QuoteId__c IN :quoteIds ';
				String soqlActivityWhereStr =  ' WHERE PackageID__c <> null AND (QSP_Discount__c > 0 OR ANSC_Discount__c > 0 OR ADSC_Discount__c > 0) ';
				String soqlActivityStr =  'SELECT CreatedDate, PackageID__c, Comments__c, QSP_Discount__c, ANSC_Discount__c, Quote__r.scpq__QuoteId__c FROM Quote_Activity__c ' + soqlActivityWhereStr;
				String soqlJobStr = 'SELECT CreatedDate, Sale_Type__c, ANSCBaseQSP__c, ANSCFinalRMR__c, ANSCCorpDiscount__c, ANSCAdditionalQSP__c, ANSCAdditionalMonitoring__c, ANSCBaseMonitoring__c, ADSCOtherCorpDiscount__c, ADSCInstallPrice__c, ADSCCorpDiscount__c, ADSCOtherAddOn__c, ADSCAddOn__c, ADSCBase__c, PromotionID__c, PackageID__c, ParentQuote__r.CreatedBy.DistrictNumber__c, ParentQuote__r.Order_Source__c, ParentQuote__r.Sales_Type__c, ParentQuote__r.scpq__Status__c, ParentQuote__r.Account__r.DNIS__c, ParentQuote__r.Account__r.GenericMedia__c,ParentQuote__r.Account__r.TelemarLeadSource__c,ParentQuote__r.Account__r.QueriedSource__c,ParentQuote__r.Account__r.TransitionDate3__c, ParentQuote__r.Account__r.Lead_Origin__c,ParentQuote__r.Account__r.Data_Source__c, ParentQuote__r.Account__r.TelemarAccountNumber__c, ParentQuote__r.Account__r.PostalCodeID__r.Name, ParentQuote__r.Account__r.AddressID__r.State__c, ParentQuote__r.Account__r.AddressID__r.City__c,ParentQuote__r.Account__r.AddressID__r.County__c,ParentQuote__r.Account__r.PostalCodeID__r.TownId__c, ParentQuote__r.scpq__QuoteId__c, ParentQuote__r.Account__r.Channel__c, ParentQuote__r.MMBCustomerNumber__c, ParentQuote__r.Name, (SELECT CreatedDate, PackageID__c FROM Quote_Job_Lines__r) FROM Quote_Job__c ' + soqlJobWhereStr;
				soqlJobStr += ' Order By CreatedDate desc limit 10000';				
				
				// Read package discounts from activity messages under the quote for each qualified for these filters
				for( sObject qaObj:Database.query( soqlActivityStr ) ){
					Quote_Activity__c qa = (Quote_Activity__c)qaObj;
					if( !Utilities.isEmptyOrNull(qa.PackageID__c) ){
						qaMap.put(qa.PackageID__c+';'+qa.Quote__r.scpq__QuoteId__c, qa);
						quoteIds.add(qa.Quote__r.scpq__QuoteId__c);
					}
				}
				
				// Used for coloring row groups by date
				QuoteInfoRow prevRow;
				
				// Generate report from the job and grabbing info as needed from parent quote/account
				for( sObject qJobObj:Database.query( soqlJobStr ) ){
					QuoteInfoRow qInf = new QuoteInfoRow();
					Quote_Job__c qJob = (Quote_Job__c)qJobObj;
					if ( qJob==null || qJob.CreatedDate == null || qJob.Quote_Job_Lines__r.isEmpty()){
						continue;
					}
					for( Quote_Job_Line__c qjl: qJob.Quote_Job_Lines__r){
						// If the package is missing from the job line attempt to use the one at the job level
						String PackageID = '';
						if( Utilities.isEmptyOrNull(qjl.PackageID__c) ){
							PackageID = (Utilities.isEmptyOrNull(qJob.PackageID__c))?'':qJob.PackageID__c;
						}
						else{
							PackageID = qjl.PackageID__c;
						}
						
						// Package ID
						qInf.PackageStr = PackageID;
						
						String msgID = PackageID + ';' + qJob.ParentQuote__r.scpq__QuoteId__c;
						if( qaMap.containsKey( msgID ) ){
							Quote_Activity__c qa = qaMap.get( msgID ); 
							//[(ANSCAdditionalQSP__c + ANSCBaseQSP__c)*ParentQuote__r.QSPDiscount__c]
							decimal opr1 = (qJob.ANSCAdditionalQSP__c!=null)?qJob.ANSCAdditionalQSP__c:0;
							decimal opr2 = (qJob.ANSCBaseQSP__c!=null)?qJob.ANSCBaseQSP__c:0;
							decimal opr3 = (qa.QSP_Discount__c!=null)?qa.QSP_Discount__c:0;
							qInf.QSPDOA	= (opr1 + opr2) * opr3/100;
							//[(ANSCBaseMonitoring__c+ ANSCAdditionalMonitoring__c)*ParentQuote__r.MonitorPercent__c]
							opr1 = (qJob.ANSCBaseMonitoring__c!=null)?qJob.ANSCBaseMonitoring__c:0;
							opr2 = (qJob.ANSCAdditionalMonitoring__c!=null)?qJob.ANSCAdditionalMonitoring__c:0;
							opr3 = (qa.ANSC_Discount__c!=null)?qa.ANSC_Discount__c:0;
							qInf.MonthlyDOA	= (opr1 + opr2) * opr3/100;
						}
						// color grouping by date
						if(prevRow==null){
							qInf.rowClass = 1;
						}
						else if ( prevRow!=null && prevRow.CreatedDate.day() != qJob.CreatedDate.day() ) {
							qInf.rowClass = prevRow.rowClass * (-1);
						}
						else {
							qInf.rowClass = prevRow.rowClass;
						}
						// report fields and stuff
						qInf.CreatedDate = qJob.CreatedDate;
						prevRow = qInf;
						qInf.DataSource = qJob.ParentQuote__r.Account__r.Data_Source__c;
						qInf.SubmittedDate = qJob.ParentQuote__r.Account__r.TransitionDate3__c;
						qInf.LeadOrigin = qJob.ParentQuote__r.Account__r.Lead_Origin__c;
						qInf.QueriedSource = qJob.ParentQuote__r.Account__r.QueriedSource__c;
						qInf.TelemarLeadSource = qJob.ParentQuote__r.Account__r.TelemarLeadSource__c;
						qInf.GenericMedia = qJob.ParentQuote__r.Account__r.GenericMedia__c;
						qInf.CustomerNumber  = qJob.ParentQuote__r.MMBCustomerNumber__c;
						qInf.CampaignStr = qJob.ParentQuote__r.Account__r.Channel__c;
						qInf.QuoteName = qJob.ParentQuote__r.Name;
						qInf.QuoteNumber = qJob.ParentQuote__r.scpq__QuoteId__c;
						qInf.Status = qJob.ParentQuote__r.scpq__Status__c;
						qInf.SalesType = qJob.ParentQuote__r.Sales_Type__c;
						qInf.PromoCode = qJob.PromotionID__c;
						qInf.Territory = qJob.ParentQuote__r.Account__r.PostalCodeID__r.TownId__c;
						qInf.PhoneFieldStatus = qJob.ParentQuote__r.Order_Source__c;
						qInf.County = qJob.ParentQuote__r.Account__r.AddressID__r.County__c;
						qInf.City = qJob.ParentQuote__r.Account__r.AddressID__r.City__c;
						qInf.State = qJob.ParentQuote__r.Account__r.AddressID__r.State__c;
						qInf.ZipCode = qJob.ParentQuote__r.Account__r.PostalCodeID__r.Name;
						qInf.RepDistrict = qJob.ParentQuote__r.CreatedBy.DistrictNumber__c;
						qInf.BasePrice = qJob.ADSCBase__c;
						qInf.AddOnPrice = qJob.ADSCAddOn__c;
						qInf.ResiChangeover = qJob.Sale_Type__c; 
						qInf.OtherAddOnPrice = qJob.ADSCOtherAddOn__c;
						qInf.CorpDiscount = qJob.ADSCCorpDiscount__c;						
						qInf.InstallPrice = qJob.ADSCInstallPrice__c;
						qInf.DOADiscount = qJob.ADSCOtherCorpDiscount__c;
						qInf.BaseMonitoring = qJob.ANSCBaseMonitoring__c;
						qInf.QSPPrice = qJob.ANSCBaseQSP__c;
						qInf.AdditionalMonitoring = qJob.ANSCAdditionalMonitoring__c;
						qInf.AdditionalQSP = qJob.ANSCAdditionalQSP__c;
						qInf.CorpDiscountMonthly = qJob.ANSCCorpDiscount__c;
						qInf.FinalRMR = qJob.ANSCFinalRMR__c;
						qInf.TelemarAccountNumber = qJob.ParentQuote__r.Account__r.TelemarAccountNumber__c;
						qInf.DNIS = qJob.ParentQuote__r.Account__r.DNIS__c;
					}
					resVal.add( qInf );
					if( resVal.size() == 10000 ){
						ReportRows = resVal.size();
						apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Report size close to limit. Some information is not shown. Consider filtering by an smaller time frame. Currently allowed size is 10,000 records.'));
						break;
					}
				}
			}
			catch(Exception err){
				apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, err.getMessage() + '<br/>' + err.getStackTraceString()));
			}
			
			ReportRows = resVal.size();
			
			return resVal;
	}
	
	/**
	 *	Action called from the page to render report from a lazy load request
	 *	@method	LoadQuoteReportData
	 *	@return PageReference
	 */ 
	public PageReference LoadQuoteReportData(){
		renderDataRows = true;
		return null;
	}
	
}