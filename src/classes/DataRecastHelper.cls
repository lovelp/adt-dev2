/************************************* MODIFICATION LOG ********************************************************************************************
* DataRecastHelper
*
* DESCRIPTION : Helper class for data recasting batches
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					3/20/2012				- Original Version
*
*													
*/

public with sharing class DataRecastHelper {
	/*
	public class DispInfo {
		public String dispCode;
		public String dispDetail;
		public DispInfo(String dispCode, String dispDetail) {
			this.dispCode = dispCode;
			this.dispDetail = dispDetail;
		}
	}
	*/
	public static void processEvents(List<Event> events) {
		RecordType sfonly = Utilities.getRecordType( RecordTypeName.SALESFORCE_ONLY_EVENT, 'Event');
		for (Event e : events) {
			if (e.RecordTypeID == null) {
				e.RecordTypeId = sfonly.Id;
			}
		}
	}

	public static void processAccounts(List<Account> accounts) {

		for (Account a : accounts) {
			
			// set channel
			if (a.Channel__c == null && a.Business_Id__c != null) {	// assume phase 1 records have a null channel value
				if ( a.Business_Id__c.contains('1100') ) {
					a.Channel__c = Channels.TYPE_RESIRESALE;
				}
				else if ( a.Business_Id__c.contains('1200') ) {
					a.Channel__c = Channels.TYPE_SBRESALE;
				}
			}
			
			// set processing type
			if ( a.ProcessingType__c == null) {	// assume phase 1 records will have a null processing type
				a.ProcessingType__c = IntegrationConstants.PROCESSING_TYPE_FIELD;
			}
			
			if ( a.TransitionDate1__c == null) {
				a.TransitionDate1__c = a.CreatedDate.date();
			}
			
			if (a.BillingSystem__c == null && a.LeadExternalID__c != null) {
				a.BillingSystem__c = a.LeadExternalID__c.substring(0,1);
			}
		}
	}
	
	public static void processAddresses(List<Address__c> addresses) {
		for (Address__c a : addresses) {
			if (a.CountryCode__c == null) {
				a.CountryCode__c = 'US';
			}
		}
	}
	
}