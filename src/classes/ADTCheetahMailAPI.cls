/************************************* MODIFICATION LOG ********************************************************************************************
* ADTEBRAPI
*
* DESCRIPTION : Serves as a class to be used for invocation of CheetahMail Service and handling the API response
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE              TICKET               REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Parameswaran Iyer             9/17/2018         HRM - 4050           - Original Version
* Nishanth Mandala              8/28/2019         HRM - 10662          - To have 'Comm' as MessageDivision for Commercial 
*/

public class ADTCheetahMailAPI {
   /**
    * Wrapper class to store Name details
    **/
    public class NameWrapper{
        public String firstName{get;set;}
        public String lastName{get;set;}
        
        public NameWrapper(String firstName,String lastName){
            this.firstName=firstName;
            this.lastName=lastName;
        }
    }
    
   /**
    * Wrapper class to store phone details
    **/
    public class PhoneWrapper{
        public String phone{get;set;}
        public PhoneWrapper(String phone){
            this.phone=phone;
        }
    }
    
   /**
    * Wrapper class to store request of CheetahMailService
    **/
    public class CheetahMailRequestWrapper{
        public String formName{get;set;}
        public NameWrapper Name{get;set;}
        public PhoneWrapper phones{get;set;}
        public String email{get;set;}
        public String messageDivision{get;set;}
        public String emailSubject{get;set;} 
        public String discount{get;set;} 
        public String promotioncode{get;set;} 
        public String messageType{get;set;}
        
        public CheetahMailRequestWrapper(){}
        
        public CheetahMailRequestWrapper(String formName,NameWrapper Name,PhoneWrapper phones,String email, String messageDivision,String emailSubject,String discount,String promotioncode,String messageType){
            this.formName=formName;
            this.Name=Name;
            this.phones=phones;
            this.email=email;
            this.messageDivision=messageDivision;
            this.emailSubject=emailSubject;
            this.discount=discount;
            this.promotioncode=promotioncode;
            this.messageType=messageType;
        }
    }
    
   /**
    * This method constructs the JSON & performs callout
    * @Param Id reqId
    * @ReturnType PageReference
    **/
    public PageReference sendToACKEmail(AuditLog__c aLog){
        try{
            RequestQueue__c rqToInsert = new RequestQueue__c();
            rqToInsert = createCheetahMailRequestQueue(aLog);
            insert rqToInsert;
        }catch(Exception e){
            system.debug('EBR Exception Error::'+e.getMessage()+e.getLineNumber());
            ADTApplicationMonitor.log(e,'ADTCheetahMailAPI','sendToACKEmail',ADTApplicationMonitor.CUSTOM_APP.SERVICEPOWER);
        }
        return null;
    }
    
    //For bulk processing
    public static void sendToACKEmail(list<AuditLog__c> audloglist){
        try{
            list<RequestQueue__c> reqlist = new list<RequestQueue__c>();
            for(AuditLog__c aLog:audloglist){
                RequestQueue__c rqToInsert = new RequestQueue__c();
                rqToInsert = createCheetahMailRequestQueue(aLog);
                reqlist.add(rqToInsert);
            }
            insert reqlist;
        }catch(Exception e){
            
            ADTApplicationMonitor.log(e,'ADTCheetahMailAPI','sendToACKEmail',ADTApplicationMonitor.CUSTOM_APP.SERVICEPOWER);
        }
    }
    
   /**
    * This method creates the Cheetah mail request queue
    * @Param AuditLog__c
    * @ReturnType RequestQueue__c
    **/
    public static RequestQueue__c createCheetahMailRequestQueue(AuditLog__c aLog){
        String messageDivision = '';
        if(String.isNotBlank(aLog.LineOfBusiness__c)){
            if(aLog.LineOfBusiness__c.containsIgnoreCase('1100')){
                messageDivision = 'Resi';
            }else if(aLog.LineOfBusiness__c.containsIgnoreCase('1200')){
                messageDivision = 'Smb';
            // HRM - 10662  - to pass Comm value as MessageDivision (Cheetah Mail) for Commercial type    
            }else if(aLog.LineOfBusiness__c.containsIgnoreCase('1300')){
                messageDivision = 'Comm';
            }   
        }
        String emailSubject=(!String.isBlank(aLog.EmailSubject__c))?aLog.EmailSubject__c:'No Subject';
        // Create JSON to store in service message
        String jsonString = JSON.serialize(new CheetahMailRequestWrapper(aLog.FormName__c,new NameWrapper(aLog.FirstName__c,aLog.LastName__c),new PhoneWrapper(Utilities.getFormattedPhoneForWebleads(aLog.PrimaryPhone__c)),aLog.Email__c,messageDivision,emailSubject,'false',aLog.PromotionCode__c,'L'));
        system.debug('ACK JSON: '+ jsonString);
        // Create Request queue
        RequestQueue__c rq = new RequestQueue__c();
        rq.AccountID__c = aLog.Account__c;
        rq.AuditLog__c = aLog.Id;
        
        list<DNIS__c> dnisRec = new list<DNIS__c>();
        dnisRec = [select ACKBypass__c from DNIS__c Where name =: aLog.PromotionCode__c And ACKBypass__c = true limit 1];
        rq.RequestStatus__c = (dnisRec.size() > 0)? 'Bypass':'Ready';

        rq.ServiceTransactionType__c = 'sendACKEmail';
        rq.MessageID__c = aLog.email__c;
        rq.ServiceMessage__c = jsonString;
        rq.RequestDate__c = System.now();
        rq.counter__c = 0;
        return rq;
    }
    
   /**
    * This method is used to make the callout to CheetahMailService
    * @Param RequestQueue__c
    * @ReturnType Void
    **/
    public static RequestQueue__c doACKEmailCallOut(RequestQueue__c requeue){
        String userName = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPUsername__c:'abc';
        String password = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPPassword__c:'def';
        String endPointURL = (!Test.isRunningTest())? IntegrationSettings__c.getInstance().DPCheetahMailEndpoint__c:'http:abc.com';
        Decimal timeout = (!Test.isRunningTest())? IntegrationSettings__c.getInstance().DPEBRTimeout__c:6000;
        Blob headerValue = Blob.valueOf(userName + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        Httprequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type', 'application/json');
        request.setEndpoint(endPointURL);
        request.setTimeout(Integer.ValueOf(timeout));
        request.setMethod('POST');
        request.setBody(requeue.ServiceMessage__c);
        system.debug('ACK Request: '+request);
        try{
            // Send the request
            response = http.send(request);
        }catch(exception e){
            requeue.ErrorDetail__c = 'Exception: '+ e.getMessage();
        }
        system.debug('ACK Response: '+response);
        if(response != null && (response.getStatusCode() == 204 || response.getStatusCode() == 200)){
            // success response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_SUCCESS;
            requeue.ErrorDetail__c = null;
        }else if(response != null){
            // error response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            //update to hardcode
            requeue.ErrorDetail__c = String.isNotBlank(requeue.ErrorDetail__c)? requeue.ErrorDetail__c + '\n ACK Response: ' +response.getbody() : response.getbody();
        }else{
            //error response
        	requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            //update to hardcode
            requeue.ErrorDetail__c = String.isNotBlank(requeue.ErrorDetail__c) ? requeue.ErrorDetail__c + '\n ACK Response: Response is blank' : 'Response is blank';
        }
        return requeue;
    }
}