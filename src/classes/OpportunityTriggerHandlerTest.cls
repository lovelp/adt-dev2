/************************************* MODIFICATION LOG ********************************************************************************************
 * OpportunityTriggerHandlerTest
 *
 * DESCRIPTION : Handler class for OpportunityTrigger and OpportunityTriggerHandler
 *               
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                        DATE              	TICKET            REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Jitendra Kothari                 07/24/2019         	HRM-         	CD1 - Link disposition to Opportunities as well
 * Jitendra Kothari					09/19/2019			HRM-10677		Pass Closed Lost Status Back to Sales Pilot
 */
@isTest
private class OpportunityTriggerHandlerTest {
    
    @TestSetup
    private static void createTestData() {

        //Create Account
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        
        List<Postal_Codes__c> pcs = new List<Postal_Codes__c>();
        Postal_Codes__c pc = new Postal_Codes__c (name='221o2', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100', APM_PostalCode__c = true);
        pcs.add(pc);
        insert pcs;
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        Account acct = new Account(Name = 'Unit Test Account i');
        acct.AddressID__c = addr.Id;
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '221o2';
        acct.Email__c = 'abc@gmail.com';
        acct.DOB_encrypted__c = '12/12/1900';
        acct.Equifax_Last_Check_DateTime__c = system.now().addDays(-5);
        acct.EquifaxRiskGrade__c = 'A';
        acct.MMBOrderType__c = 'R1';
        acct.PostalCodeId__c = pc.Id;
        insert acct;
        
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.SalesPointCalloutTimeout__c = 60000;
        is.SalesPilotURL__c = 'https://salespilottest.protection1.com/Sales/SalesAgreement/SalesAgreement/[SalesAgreementId]/Info';
        is.CreateSalesAgreementURL__c = 'https://salespilottest.protection1.com/SDFC/createSalesAgreement';
        is.LookupSalesAgreementURL__c = 'https://salespilottest.protection1.com/SDFC/lookupSalesAgreement';
        is.UpdateSalesAgreementURL__c ='https://salespilottest.protection1.com/SDFC/updateSalesAgreement';
        insert is;
    }
    
    static testMethod void testOppUpdate() {
        Account acct = [SELECT Id FROM Account WHERE CreatedDate = TODAY AND Name = 'Unit Test Account i' order by createddate desc limit 1];
        Opportunity opp = New Opportunity(AccountId = acct.Id);
        opp.Name = 'TestOpp1';
        opp.StageName = 'Closed Lost';
        opp.CloseDate = Date.today();
        opp.DispositionCode__c = 'Lost';
        opp.Disposition_Detail__c = 'Duplicate';
        opp.SalesAgreementId__c = '123456789';
        
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new SalesAgreementUpdateMock());
        insert opp; 
        
        list<Disposition__c> disp = [SELECT Id, AccountID__c, DispositionedBy__c, DispositionType__c, DispositionDetail__c
                                        FROM Disposition__c WHERE OpportunityID__c = :opp.Id];
        system.assertEquals(disp.isEmpty(), false);
        system.assertEquals(disp.size(), 1);
        //system.assertEquals(disp.get(0).AccountID__c, opp.AccountId);
        system.assertEquals(disp.get(0).DispositionedBy__c, Userinfo.getUserId());
        system.assertEquals(disp.get(0).DispositionType__c, 'Lost');
        system.assertEquals(disp.get(0).DispositionDetail__c, 'Duplicate');
        
        opp.DispositionCode__c = 'Won';
        opp.Disposition_Detail__c = 'Duplicate';
        update opp;
        
        disp = [SELECT Id, AccountID__c, DispositionedBy__c, DispositionType__c, DispositionDetail__c
                                        FROM Disposition__c WHERE OpportunityID__c = :opp.Id];
        system.assertEquals(disp.isEmpty(), false);
        system.assertEquals(disp.size(), 2);
        //system.assertEquals(disp.get(1).AccountID__c, opp.AccountId);
        system.assertEquals(disp.get(1).DispositionedBy__c, Userinfo.getUserId());
        system.assertEquals(disp.get(1).DispositionType__c, 'Won');
        system.assertEquals(disp.get(1).DispositionDetail__c, 'Duplicate');
        
        opp.DispositionCode__c = 'Won';
        opp.Disposition_Detail__c = 'Pricing';
        update opp;
        
        Test.stopTest();
        disp = [SELECT Id, AccountID__c, DispositionedBy__c, DispositionType__c, DispositionDetail__c
                                        FROM Disposition__c WHERE OpportunityID__c = :opp.Id];
        system.assertEquals(disp.isEmpty(), false);
        system.assertEquals(disp.size(), 3);
        //system.assertEquals(disp.get(2).AccountID__c, opp.AccountId);
        system.assertEquals(disp.get(2).DispositionedBy__c, Userinfo.getUserId());
        system.assertEquals(disp.get(2).DispositionType__c, 'Won');
        system.assertEquals(disp.get(2).DispositionDetail__c, 'Pricing');
    }
    //Start HRM-10677
    //Added to test values passed to Sales Pilot integration
    static testMethod void testSyncFieldsToSalesPilot() {
        Account acct = [SELECT Id FROM Account WHERE CreatedDate = TODAY AND Name = 'Unit Test Account i' order by createddate desc limit 1];
       
        //Create Opportunity
        Opportunity opp = New Opportunity(AccountId = acct.Id);
        opp.Name = 'TestOpp1';
        opp.StageName = 'Needs Analysis';
        opp.CloseDate = Date.today();
        opp.DispositionCode__c = 'Lost';
        opp.Disposition_Detail__c = 'Duplicate';
        opp.SalesAgreementId__c = '890112393';
        
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new SalesAgreementUpdateMock());
        insert opp; 
        
        //Update to Closed Lost
        opp.StageName = 'Closed Lost';
        //Send Opportunity Status to Sales Pilot
        update opp;
               
        Test.stopTest();
        
        //Assert the Request and Response
        opp = [Select UpdateSalesAgreementRequestJSON__c,UpdateSalesAgreementResponseJSON__c From Opportunity Where Id=:opp.Id];
        
        //Assert Request - Closed Lost
        SalesAgreementSchema.SalesAgreementUpdateRequest req = new SalesAgreementSchema.SalesAgreementUpdateRequest();
        req = (SalesAgreementSchema.SalesAgreementUpdateRequest) System.JSON.deserialize(opp.UpdateSalesAgreementRequestJSON__c, SalesAgreementSchema.SalesAgreementUpdateRequest.class);
        system.assertEquals(req.status, 'Lost');
        
        //Assert Response -
        SalesAgreementSchema.SalesAgreementUpdateResponse result = new SalesAgreementSchema.SalesAgreementUpdateResponse();
        result = (SalesAgreementSchema.SalesAgreementUpdateResponse) System.JSON.deserialize(opp.UpdateSalesAgreementResponseJSON__c, SalesAgreementSchema.SalesAgreementUpdateResponse.class);
        system.assertEquals(result.status, true);
    }
    //End HRM-10677
    
}