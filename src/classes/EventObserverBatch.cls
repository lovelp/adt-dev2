/************************************* MODIFICATION LOG ********************************************************************************************
* EventObserverBatch
* 
* DESCRIPTION : Batch class that is responsible for invoking the TeleNav Track getStopReport web service for an Event and 
*               updating the observed start and end times based on observed stop measurements.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* SHIVKANT VADLAMANI            4/1/2012            - Original Version                                                
*                                                   
*/

global class EventObserverBatch implements Database.batchable<sObject>, Database.AllowsCallouts
{
   global String query = 'SELECT Id, StartDateTime, EndDateTime, CreatedDate, WhatId, DevicePhoneNumber__c, ObservedStartDateTime__c, ObservedEndDateTime__c, ObservedDuration__c, OnTimeIndicator__c, OwnerId FROM Event WHERE WhatId <> null AND DevicePhoneNumber__c <> null AND ObservedDuration__c = null AND Status__c <> \'Canceled\'';
    
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {       
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, list<sObject> Appointments)
    {           
        list<Event> eventsList = (list<Event>)Appointments; 
        
        if(eventsList != null && eventsList.size() > 0)
        {
            EventManager.updateObservedValues(eventsList);
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {   
    }
    
}