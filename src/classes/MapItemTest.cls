@isTest
private class MapItemTest {
	
	
	static testMethod void testConstructFromAccount() {
		
		Account a = TestHelperClass.createAccountData();
		
		Test.startTest();
		
		
		MapItem mi = new MapItem(a);
		mi.asString();
		System.assert(a.Id == mi.rId);
		System.assertEquals(a.Latitude__c, mi.rLat);
		System.assertEquals(a.Longitude__c, mi.rLon);
		System.assert(a.OwnerId == mi.rOwnerId);
		System.assertEquals('true', mi.mActiveSite);
		System.assertEquals('account', mi.createdFrom);
		System.assertEquals('false', mi.mIsNewMover);
		System.assertEquals('', mi.rPhone);
		System.assertEquals('', mi.rCountry);
		System.assertEquals(a.Owner.Name, mi.mOwnerName);
		//System.assert(mi.mDataSource == 'TEST');
		System.assertEquals('', mi.mDiscoReason);
		System.assertEquals(a.DispositionCode__c, mi.mDisposition);
		System.assert(mi.mDiscoDate == '');
		System.assert(mi.mDispoDate == '');
		
		a.InService__c = false;
		a.NewMover__c = true;
		a.Data_Source__c = 'ADMIN';
		Date inputDate = Date.today();
		a.DisconnectDate__c = inputDate;
		Datetime inputDatetime = Datetime.now();
		a.DispositionDate__c = inputDatetime;
		a.Name = '12345678901234567890123';
		a.Type = 'Discontinuance';
		a.Industry = 'Other';
		a.UnassignedLead__c = true;
		a.OwnerId = UserInfo.getUserId();
		a.ScheduledInstallDate__c = System.today() + 1 ;		
		
		MapItem mi2 = new MapItem(a);
		mi2.asString();
		System.assertEquals('false', mi2.mActiveSite);
		System.assertEquals('true', mi2.mIsNewMover);
		System.assertEquals('ADMIN', mi2.mDataSource);
		System.assertEquals('12345678901234567890123', mi2.rName);
		System.assertEquals(inputDate.format(), mi2.mDiscoDate);
		System.assertEquals(inputDatetime.format('M/d/yyyy'), mi2.mDispoDate);
		System.assertEquals(a.Type, mi2.mType);
		//System.assertEquals('My Install Appointments', mi2.mRecordCategory);
		
		a.SalesAppointmentDate__c = System.today() + 1 ;
		a.ScheduledInstallDate__c = System.today() - 1 ;	
		MapItem mi3 = new MapItem(a);
		mi3.asString();
		//System.assertEquals('My Sales Appointments', mi3.mRecordCategory);
		
		String converted = mi2.asString();
		System.assert(!converted.contains('12345678901234567890123'));
		System.assert(converted.contains('12345678901234567890'));

		a.Type = 'Resale';
		MapItem mi4 = new MapItem(a, 2.0);
		mi4.asString();
	//	System.assertEquals('My Prospects', mi4.mRecordCategory);
	
		Test.stopTest(); 
		
		
	}
	
	static testMethod void testConstructFromResaleAccount() {
		
		Account a = TestHelperClass.createAccountData();
		a.Channel__c = 'SB Direct Sales';
		a.Data_Source__c = '';
		a.Phone ='7035551212';
		a.DisconnectReason__c = 'Reason';
		a.DispositionDetail__c = 'Detail';
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account').Id;
		a.OwnerId = UserInfo.getUserId();
		
		Test.startTest();
		
		MapItem mi = new MapItem(a);
		mi.asString();
		
		Test.stopTest();
		System.assert(a.Id == mi.rId);
		System.assertEquals(a.Latitude__c, mi.rLat);
		System.assertEquals(a.Longitude__c, mi.rLon);
		System.assertEquals('true', mi.mActiveSite);
		System.assertEquals('account', mi.createdFrom);
		System.assertEquals('false', mi.mIsNewMover);
		System.assertEquals('7035551212', mi.rPhone);
		System.assertEquals('', mi.rCountry);
		System.assertEquals('', mi.mDataSource);
		System.assertEquals('Reason', mi.mDiscoReason);
		System.assertEquals(a.DispositionCode__c, mi.mDisposition);
		System.assertEquals('Detail', a.DispositionDetail__c);
		System.assert(mi.mDiscoDate == '');
		System.assert(mi.mDispoDate == '');
		System.assertEquals('My Prospects', mi.mRecordCategory);
			
	}
	
	static testMethod void testConstructFromRIFAccountCurrentUserOwner() {
		
		Account a = TestHelperClass.createAccountData();
		a.Channel__c = 'SB Direct Sales';
		a.Data_Source__c = '';
		a.Phone = null;
		a.DispositionCode__c = null;
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account').Id;
		a.OwnerId = UserInfo.getUserId();
		a.DisconnectDate__c = null;
		a.NewMoverType__c = 'Relo';
		
		Test.startTest();
		
		MapItem mi = new MapItem(a);
		mi.asString();
		
		Test.stopTest();
		System.assertEquals('RIFGrey', mi.mRecordCategory);
		System.assertEquals('', mi.rPhone);
		System.assertEquals('', mi.mDiscoDate);
		System.assertEquals('Relo', mi.mNewMoverType);
			
	}
	
	static testMethod void testConstructFromRIFAccountNotCurrentUserOwner() {
		
		Account a = TestHelperClass.createAccountData();
		a.Channel__c = 'SB Direct Sales';
		a.Data_Source__c = '';
		a.Phone ='7035551212';
		a.DisconnectReason__c = 'Reason';
		a.DispositionDetail__c = 'Detail';
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account').Id;
		
		Test.startTest();
		
		MapItem mi = new MapItem(a);
		mi.asString();
		
		Test.stopTest();
		System.assertEquals('RIFGrey', mi.mRecordCategory);
			
	}
	
	static testMethod void testConstructFromStandardAsSalesAppt() {
		
		Account a = TestHelperClass.createAccountData();
		a.Channel__c = 'SB Direct Sales';
		a.Data_Source__c = '';
		a.Phone ='7035551212';
		a.DisconnectReason__c = 'Reason';
		a.DispositionDetail__c = 'Detail';
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account').Id;
		a.OwnerId = UserInfo.getUserId();
		a.SalesAppointmentDate__c = Datetime.now();
		a.SalesAppointmentDate__c = a.SalesAppointmentDate__c.addDays(1);
		
		Test.startTest();
		
		MapItem mi = new MapItem(a);
		mi.asString();
		
		Test.stopTest();
		
		System.assertEquals('My Sales Appointments', mi.mRecordCategory);
		
			
	}
	
	static testMethod void testConstructFromStandardAsUnsoldSalesAppt() {
		
		Account a = TestHelperClass.createAccountData();
		a.Channel__c = 'SB Direct Sales';
		a.Data_Source__c = '';
		a.Phone ='7035551212';
		a.DisconnectReason__c = 'Reason';
		a.DispositionDetail__c = 'Detail';
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account').Id;
		a.OwnerId = UserInfo.getUserId();
		a.SalesAppointmentDate__c = Datetime.now();
		a.SalesAppointmentDate__c = a.SalesAppointmentDate__c.addDays(-1);
		a.ScheduledInstallDate__c = null;
		
		Test.startTest();
		
		MapItem mi = new MapItem(a);
		mi.asString();
		
		Test.stopTest();
		
		System.assertEquals('My Unsold Sales Appointment', mi.mRecordCategory);
			
	}
	
	static testMethod void testConstructFromStandardAsInstallAppt() {
		
		Account a = TestHelperClass.createAccountData();
		a.Channel__c = 'SB Direct Sales';
		a.Data_Source__c = '';
		a.Phone ='7035551212';
		a.DisconnectReason__c = 'Reason';
		a.DispositionDetail__c = 'Detail';
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account').Id;
		a.OwnerId = UserInfo.getUserId();
		a.ScheduledInstallDate__c = Datetime.now();
		a.ScheduledInstallDate__c = a.ScheduledInstallDate__c.addDays(1);
		a.SalesAppointmentDate__c = a.ScheduledInstallDate__c.addDays(-5);
		
		Test.startTest();
		
		MapItem mi = new MapItem(a);
		mi.asString();
		
		Test.stopTest();
		
		System.assertEquals('My Install Appointments', mi.mRecordCategory);
		
			
	}
	
	static testMethod void testGetIsStartingPointStr() {
		
		Test.startTest();
		
		MapItem mi = new MapItem(0,0);
		
		System.assertEquals('false', mi.getIsStartingPointStr());
		
		mi.rIsStartingPoint = false;
		
		System.assertEquals('false', mi.getIsStartingPointStr());
		
		mi.rIsStartingPoint = true;
		
		System.assertEquals('true', mi.getIsStartingPointStr());
		
		
		Test.stopTest();
	}
	
	static testMethod void testLeadMapItem()
	{
		User current = [select Id from User where Id=:UserInfo.getUserId()];
		Lead l;
		system.runas(current) {
			User u = TestHelperClass.createSalesRepUser();
			l = TestHelperClass.createLead(u);
			l.Channel__c = null;
		}
		test.startTest();
			MapItem mi = new MapItem(l, 1);
			mi.asString();
		test.stoptest();
		
		
		System.assert(l.Id == mi.rId);
		System.assertEquals(l.Latitude__c, mi.rLat);
		System.assertEquals(l.Longitude__c, mi.rLon);
		System.assertEquals('lead', mi.createdFrom);
		System.assertEquals('false', mi.mIsNewMover);
		System.assertEquals('', mi.rPhone);
		System.assertEquals('US', mi.rCountry);
		System.assertEquals(l.Owner.Name, mi.mOwnerName);
		System.assert(mi.mDataSource == '');
		System.assertEquals('', mi.mChannel);
		System.assertEquals('Other Leads Available', mi.mRecordCategory);
		
	}
	
	static testMethod void testLeadMapItemCurrentUserOwner()
	{
		User current = [select Id from User where Id=:UserInfo.getUserId()];
		Lead l;
		system.runas(current) {
			User u = TestHelperClass.createSalesRepUser();
			l = TestHelperClass.createLead(u);
			l.NewMover__c = true;
			l.OwnerId = UserInfo.getUserId();
			l.Phone = '7035551212';
			l.LeadSource = 'SOURCE';
			l.DispositionCode__c = null;
			l.DispositionDetail__c = 'DETAIL';
			l.Channel__c = 'SB Direct Sales';
			l.Company = 'COMPANY';
			l.DispositionDate__c = Datetime.now();
			l.DisconnectDate__c = Date.today();
			l.NewMoverType__c = 'Relo';
			l.Type__c = 'TYPE';
			l.UnassignedLead__c = true;
		}
		test.startTest();
			MapItem mi = new MapItem(l, 1);
			mi.asString();
		test.stoptest();
		
		System.assertEquals('My Prospects', mi.mRecordCategory);
		System.assertEquals('true', mi.mIsNewMover);
		System.assertEquals('7035551212', mi.rPhone);
		System.assertEquals('SOURCE', mi.mDataSource);
		System.assertEquals('', mi.mDisposition);
		System.assertEquals('DETAIL', mi.mDispositionDetail);
		System.assertEquals('SB Direct Sales', mi.mChannel);
		System.assertEquals('COMPANY', mi.rName);
		System.assertNotEquals('', mi.mDispoDate);
		System.assertNotEquals('', mi.mDiscoDate);
		System.assertEquals('Relo', mi.mNewMoverType);
		System.assertEquals('TYPE', mi.mType);
		System.assertEquals('true', mi.mUnAssigned);
		
	}
	
	
	static testMethod void testIsRevenueInForceLead() {
		
		User current = [select Id from User where Id=:UserInfo.getUserId()];
		Lead l;
		system.runas(current) {
			User u = TestHelperClass.createSalesRepUser();
			l = TestHelperClass.createLead(u);
		}
		Test.startTest();
			MapItem mi = new MapItem(l, 1);
			System.assert(!mi.isRevenueInForce());
		Test.stoptest();
		
	}
	
	static testMethod void testIsOutOfServiceLead() {
		
		User current = [select Id from User where Id=:UserInfo.getUserId()];
		Lead l;
		system.runas(current) {
			User u = TestHelperClass.createSalesRepUser();
			l = TestHelperClass.createLead(u);
		}
		Test.startTest();
			MapItem mi = new MapItem(l, 1);
			System.assert(!mi.isOutOfService());
		Test.stoptest();
		
	}
	
	static testMethod void testIsRevenueInForceStandardAccount() {
		
		Account a = TestHelperClass.createAccountData();
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account').Id;
		
		Test.startTest();
			MapItem mi = new MapItem(a, 1);
			System.assert(!mi.isRevenueInForce());
		Test.stoptest();
		
	}
	
	static testMethod void testIsOutOfServiceStandardAccount() {
		
		Account a = TestHelperClass.createAccountData();
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account').Id;
		Test.startTest();
			MapItem mi = new MapItem(a, 1);
			System.assert(!mi.isOutOfService());
		Test.stoptest();
		
	}
	
	static testMethod void testIsRevenueInForceResaleAccount() {
		
		Account a = TestHelperClass.createAccountData();
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account').Id;
		
		Test.startTest();
			MapItem mi = new MapItem(a, 1);
			System.assert(!mi.isRevenueInForce());
		Test.stoptest();
		
	}
	
	static testMethod void testIsOutOfServiceResaleAccount() {
		
		Account a = TestHelperClass.createAccountData();
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account').Id;
		a.Channel__c = Channels.TYPE_RESIRESALE;
		Test.startTest();
			MapItem mi = new MapItem(a, 1);
			System.assert(mi.isOutOfService());
		Test.stoptest();
		
	}
	
	static testMethod void testIsOutOfServiceStandardAccountResiResaleChannel() {
		
		Account a = TestHelperClass.createAccountData();
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account').Id;
		a.Channel__c = Channels.TYPE_RESIRESALE;
		Test.startTest();
			MapItem mi = new MapItem(a, 1);
			System.assert(mi.isOutOfService());
		Test.stoptest();
		
	}
	
	static testMethod void testIsOutOfServiceStandardAccountSBResaleChannel() {
		
		Account a = TestHelperClass.createAccountData();
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account').Id;
		a.Channel__c = Channels.TYPE_SBRESALE;
		Test.startTest();
			MapItem mi = new MapItem(a, 1);
			System.assert(mi.isOutOfService());
		Test.stoptest();
		
	}
	
	static testMethod void testIsRevenueInForceRIFAccount() {
		
		Account a = TestHelperClass.createAccountData();
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account').Id;
		
		Test.startTest();
			MapItem mi = new MapItem(a, 1);
			System.assert(mi.isRevenueInForce());
		Test.stoptest();
		
	}
	
	static testMethod void testIsOutOfServiceRIFAccount() {
		
		Account a = TestHelperClass.createAccountData();
		a.RecordTypeId = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account').Id;
		Test.startTest();
			MapItem mi = new MapItem(a, 1);
			System.assert(!mi.isOutOfService());
		Test.stoptest();
		
	}
	
	static testMethod void testConstructExtraRIFAccountFields() {
		
		Account a = TestHelperClass.createAccountData();
		Date inputDate = Date.today();
		
		a.TenureDate__c = date.today();
		a.ANSCSold__c = 543.21;
		a.SystemProfileCode__c = '77';
		a.ServiceProfileCode__c = '100';
		
		Test.startTest();
		
		MapItem mi = new MapItem(a);
		mi.asString();
		Test.stopTest();
		
		
		System.assertEquals(inputDate.format(), mi.mTenureDate);
		System.assertEquals(543.21, mi.mANSCSold);
		System.assertEquals('77', mi.mSystemProfileCode);
		System.assertEquals('100',mi.mServiceProfileCode);
		
	}
	
}