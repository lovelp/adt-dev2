/************************************* MODIFICATION LOG ********************************************************************************************
* PicklistHelper
*
* DESCRIPTION : Determines picklist values needed to render the ManageStreetSheet VisualForce page.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*            				 10/14/2011			- Original Version
*
*													
*/

public with sharing class PicklistHelper {
	
	private static final String UNITED_STATES = 'US';
	private static final String DATA_SOURCE_FIELD = 'DataSource';
	private static final String TYPE_FIELD = 'Type';
	
	public static final String ALL = 'All';
	
	
	// Determine picklist values for row limits
	public static List<selectOption> getResultSizeValues(){
	 	
    	List<SelectOption> options = new List<SelectOption>();	
      	
      	 // Find all the values in the custom setting  
        Map<String, StreetSheetQueryLimit__c> queryLimits = StreetSheetQueryLimit__c.getAll();
    
        // Create the Select Options  
        for (String limitName : queryLimits.keySet()) {
            options.add(new SelectOption(limitName, limitName));
        }
      			      
        return options;     
     }
     
 	// Determine picklist values for Data Source
   	public static List<SelectOption> getLeadDataSourceValues(){
    	Schema.DescribeFieldResult F = Lead.LeadSource.getDescribe();
                
      	List<Schema.PicklistEntry> dataSourcePicklist = F.getPicklistValues();
      	
        Map<String, String> optionsMap = new Map<String, String>(); 
        List<String> labelList = new List<String>();       
      	for(Schema.PicklistEntry plo: dataSourcePicklist){	
			if (plo.isActive()) {
				// put active and valid options into a map using the label as the key
				optionsMap.put(plo.getLabel(), plo.getValue());
				// also put the label into a list
				labelList.add(plo.getLabel());
			}
		}
		
		// Sort the list of labels
		labelList.sort(); 
		
		// Then iterate over the sorted list
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(ALL, ALL));
		for(String label : labelList) {
			// and create the SelectOption objects
			options.add(new SelectOption(optionsMap.get(label), label));	
		}
			
		return options;
               
     }
	
 	// Determine picklist values for Data Source
   	public static List<SelectOption> getDataSourceValues(){
    	Schema.DescribeFieldResult F = Account.Data_Source__c.getDescribe();
                
      	List<Schema.PicklistEntry> dataSourcePicklist = F.getPicklistValues();
      	
        Map<String, String> optionsMap = new Map<String, String>(); 
        List<String> labelList = new List<String>();       
      	for(Schema.PicklistEntry plo: dataSourcePicklist){	
			if (plo.isActive()) {
				// put active and valid options into a map using the label as the key
				optionsMap.put(plo.getLabel(), plo.getValue());
				// also put the label into a list
				labelList.add(plo.getLabel());
			}
		}
		
		// Sort the list of labels
		labelList.sort(); 
		
		// Then iterate over the sorted list
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(ALL, ALL));
		for(String label : labelList) {
			// and create the SelectOption objects
			options.add(new SelectOption(optionsMap.get(label), label));	
		}
		
		
		return options;
               
     }
     
   	public static List<SelectOption> getTerritoryTypeValues(){
    	Schema.DescribeFieldResult F = Territory__c.TerritoryType__c.getDescribe();
                
      	List<Schema.PicklistEntry> dataSourcePicklist = F.getPicklistValues();
      	
        Map<String, String> optionsMap = new Map<String, String>(); 
        List<String> labelList = new List<String>();       
      	for(Schema.PicklistEntry plo: dataSourcePicklist){	
			if (plo.isActive()) {
				// put active and valid options into a map using the label as the key
				optionsMap.put(plo.getLabel(), plo.getValue());
				// also put the label into a list
				labelList.add(plo.getLabel());
			}
		}
		// Sort the list of labels
		//labelList.sort(); 
		
		// Then iterate over the sorted list
		List<SelectOption> options = new List<SelectOption>();
		//options.add(new SelectOption(ALL, ALL));
		for(String label : labelList) {
			// and create the SelectOption objects
			options.add(new SelectOption(optionsMap.get(label), label));	
		}
		
		return options;
               
     }     

	public static List<SelectOption> getLeadTypeValues() {
    	Schema.DescribeFieldResult F = Lead.Type__c.getDescribe();
                
      	List<Schema.PicklistEntry> typePicklist = F.getPicklistValues();
        
        Map<String, String> optionsMap = new Map<String, String>(); 
        List<String> labelList = new List<String>();        
      	for(Schema.PicklistEntry plo: typePicklist){
      		if (plo.isActive()) {
				// put active and valid options into a map using the label as the key
				optionsMap.put(plo.getLabel(), plo.getValue());
				// also put the label into a list
				labelList.add(plo.getLabel());
			}
		}
		// Sort the list of labels
		labelList.sort(); 
		
		// Then iterate over the sorted list
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(ALL, ALL));
		for(String label : labelList) {
			// and create the SelectOption objects
			options.add(new SelectOption(optionsMap.get(label), label));	
		}
		
		return options;
	}
    
    // Determine picklist values for Account Type 
   	public static List<SelectOption> getAccountTypeValues(){
    	Schema.DescribeFieldResult F = Account.Type.getDescribe();
                
      	List<Schema.PicklistEntry> typePicklist = F.getPicklistValues();
        
        Map<String, String> optionsMap = new Map<String, String>(); 
        List<String> labelList = new List<String>();        
      	for(Schema.PicklistEntry plo: typePicklist){
      		if (plo.isActive() && validOption(plo, TYPE_FIELD)) {
				// put active and valid options into a map using the label as the key
				optionsMap.put(plo.getLabel(), plo.getValue());
				// also put the label into a list
				labelList.add(plo.getLabel());
			}
		}
		// Sort the list of labels
		labelList.sort(); 
		
		// Then iterate over the sorted list
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(ALL, ALL));
		for(String label : labelList) {
			// and create the SelectOption objects
			options.add(new SelectOption(optionsMap.get(label), label));	
		}
		
		return options;
               
    }
    
    // Determine picklist values for New Mover Type
    public static List<SelectOption> getNewMoverTypeValues() {
    	
    	List<SelectOption> items = new List<SelectOption>();
        Schema.DescribeFieldResult F = Account.NewMoverType__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        
        
        items.add(new SelectOption('All','All'));
    
        for(integer k =0; k< P.size(); k++)
        {
            if(P[k].isActive())
            {
               items.add(new SelectOption(P[k].getLabel(),P[k].getLabel()));
            }
        }   
        
        return items; 
    	
    }
    
    
    // Determine picklist values for Disposition Code
    public static List<SelectOption> getLeadDispositionCodeValues() {
    	
    	List<SelectOption> items = new List<SelectOption>();
        Schema.DescribeFieldResult F = Lead.DispositionCode__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        
        items.add(new SelectOption('All','All'));
    
        for(integer k =0; k< P.size(); k++)
        {
            if(P[k].isActive())
            {
               items.add(new SelectOption(P[k].getLabel(),P[k].getLabel()));
            }
        }   
        
        return items;
    	
    }
    
    // Determine picklist values for Disposition Code
    public static List<SelectOption> getDispositionCodeValues() {
    	
    	List<SelectOption> items = new List<SelectOption>();
        Schema.DescribeFieldResult F = Account.DispositionCode__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        
        items.add(new SelectOption('All','All'));
    
        for(integer k =0; k< P.size(); k++)
        {
            if(P[k].isActive())
            {
               items.add(new SelectOption(P[k].getLabel(),P[k].getLabel()));
            }
        }   
        
        return items;
    	
    }
    
    // Determine picklist values for Site States    
    public static List<SelectOption> getSiteStateValues() {
    
    	List<SelectOption> options = new List<SelectOption>();	
      	
      	Map<String, SiteStates__c> allStatesMap = SiteStates__c.getAll();

        // Filter states that belong to US  
        Map<String, SiteStates__c> statesMap = new Map<String, SiteStates__c>();
        for(SiteStates__c state : allStatesMap.values()) {
        	// Added Canada Provinces
            //if (state.CountryCode__c == UNITED_STATES) {
                statesMap.put(state.StateName__c, state);
            //}
        }
        
        // Sort the states based on their names  
        List<String> stateNamesList = new List<String>();
        stateNamesList.addAll(statesMap.keySet());
        stateNamesList.sort();
        
        // Generate the Select Options based on the final sorted list  
    	options.add(new SelectOption(ALL, ALL));
        for (String stateName : stateNamesList) {
            SiteStates__c state = statesMap.get(stateName);
            options.add(new SelectOption(state.StateCode__c, state.StateName__c));
        }

        return options;     
    		
    }
        
    
    // Determine picklist values for Disco Reasons
    public static List <SelectOption> getDiscoReasonValues(){
		Schema.DescribeFieldResult R = Account.DisconnectReason__c.getDescribe();
		List<Schema.PicklistEntry> disconnectPicklist = R.getPicklistValues();
		
		Map<String, String> optionsMap = new Map<String, String>(); 
        List<String> labelList = new List<String>(); 
		for(Schema.PicklistEntry plo: disconnectPicklist){
			if (plo.isActive()) {
				// put active and valid options into a map using the label as the key
				optionsMap.put(plo.getLabel(), plo.getValue());
				// also put the label into a list
				labelList.add(plo.getLabel());
			}
		}
		// Sort the list of labels
		labelList.sort(); 
		
		// Then iterate over the sorted list
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(ALL, ALL));
		for(String label : labelList) {
			// and create the SelectOption objects
			options.add(new SelectOption(optionsMap.get(label), label));	
		}
		
		return options;
	}
	
	private static Boolean validOption(PicklistEntry e, String fieldType) {
    	
        
        Map<String, ReferenceData__c> allRefDataMap = ReferenceData__c.getAll();

        // Filter values that belong to the requested field type  
        Set<String> refDataSet = new Set<String>();
        for(ReferenceData__c rData : allRefDataMap.values()) {
            if (rData.Field__c == fieldType) {
                refDataSet.add(rData.Value__c);
            }
        }
               
        return refDataSet.contains(e.getValue());
    	
	}

}