global class ADTPartnerAccUpdateNoDataResGenerator implements HttpCalloutMock
{
    global HTTPResponse respond(HTTPRequest req) 
    {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        // Create a fake response for a customer lookup web service call
        String xmlBodyStr = '';
        xmlBodyStr='<NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/">'
            +'   <NS1:Body>'
            +'      <NS2:Fault xmlns:NS2="http://mastermindservice.adt.com/lookup">'            
            +'              <NS2:faultstring>NOTFOUND</NS2:faultstring>'
            +'      </NS2:Fault>'
            +'  </NS1:Body>'
            +'</NS1:Envelope>';
        res.setBody(xmlBodyStr);
            
        res.setStatusCode(200);
        return res;
    }
}