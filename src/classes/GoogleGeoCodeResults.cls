/************************************* MODIFICATION LOG ********************************************************************************************
* GoogleGeoCodeResults
* 
* DESCRIPTION : Date structure representing the response from the Google Geocode API. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*               				10/14/2011			- Original Version
*
*													  
*													
*/

public with sharing class GoogleGeoCodeResults {

	public String Status;
	public String AddressType;
	public String FormattedAddress;
	public String AddressStreetNumber;
	public String AddressRoute;
	public String AddressLocality;
	public String AddressPostalCode;
	public String AddressCountry;
	public String AddressLat;
	public String AddressLon;
	public String ExactMatch;

}