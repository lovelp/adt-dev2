/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DataRecastAccountTaskCodeAssignmentTest {
    static testMethod void dataRecastAccountTaskCodeAssignTest() {
        User admin = TestHelperClass.createUser(1000);
        System.runAs(admin){          
            TestHelperClass.createReferenceDataForTestClasses();

            // Create DataConversionUser
            Profile p1 = [select id from profile where name='ADT Integration User']; 
            User dataConversionUser = new User(alias = 'utsm1', email='unitTestIntegration1@testorg.com', 
                emailencodingkey='UTF-8', lastname='TestIntegration User', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p1.Id, EmployeeNumber__c = 'T987654123',
                timezonesidkey='America/Los_Angeles', username='unitTestIntegration1@testorg.com',
                StartDateInCurrentJob__c = Date.today());
            insert dataConversionUser;

            // Create GlobalUnassignedUser
            Profile sysAdmin = [select id from profile where name='System Administrator']; 
            User globalUnassignedUser = new User(alias = 'utsm1', email='unitTestGlobalUnassignUser1@testorg.com', 
                emailencodingkey='UTF-8', lastname='TestGlobal ADMIN', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = sysAdmin.Id, EmployeeNumber__c = 'T987612345',
                timezonesidkey='America/Los_Angeles', username='unitTestGlobalUnassignUser1@testorg.com',
                StartDateInCurrentJob__c = Date.today());
            insert globalUnassignedUser;

            List<ResaleGlobalVariables__c> globalList = new List<ResaleGlobalVariables__c>();
            globalList = [Select Id,name from ResaleGlobalVariables__c];
            system.debug(globalList);
            
            RecordType accountRT = [Select Id, DeveloperName from RecordType Where SobjectType = 'Account' and DeveloperName = 'Standard'];
            RecordType eventRT = [Select Id, DeveloperName from RecordType Where SobjectType = 'Event' and DeveloperName = 'CompanyGeneratedAppointment'];
            
            Account a = new Account();
            a.Name = 'Test Acc'; 
            a.TaskCode__c = 'ADA';
            a.Business_Id__c = '1100';
            a.RecordTypeId = accountRT.Id;
            insert a;
            
            // Insert 2 events with the first events ending time greater than the 2nd one
            Event e1 = new Event();
            e1.WhatId = a.Id;
            e1.TaskCode__c = 'ADA';
            e1.RecordTypeId = eventRT.Id;
            e1.StartDateTime = System.now();
            e1.EndDateTime = System.now() + 2;
            e1.Subject = 'Test Subject1';
            insert e1;

            Event e2 = new Event();
            e2.WhatId = a.Id;
            e2.TaskCode__c = 'ADA';
            e2.RecordTypeId = eventRT.Id;
            e2.StartDateTime = System.now();
            e2.EndDateTime = System.now() + 1;
            e2.Subject = 'Test Subject2';
            insert e2;

            Test.startTest();
                DataRecastAccountTaskCodeAssignment dataRecast = new DataRecastAccountTaskCodeAssignment();
                Database.executeBatch(dataRecast);
            Test.stopTest();
        }
    }
}