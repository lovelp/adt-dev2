/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class ADTEWCConsentAPITest {
    
    static testMethod void myUnitTest() {
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        insert rgvList;
        
        TelemarDecommission__c t=new TelemarDecommission__c(name='WebLeads',value__c='TRUE');
        insert t;
        List<Postal_Codes__c> pList=TestHelperClass.createPostalCodes();
        pList.get(0).Name='12345';
        pList.get(0).BusinessID__c='1100';
        update pList.get(0);
        list<Equifax__c> eqlist = new list<Equifax__c>();
        eqlist.add(new Equifax__c(name = 'Default Condition Code', value__c='CAE1'));
        eqlist.add(new Equifax__c(name = ' Default Risk Grade', value__c='W'));
        insert eqlist;
        Lead_Convert__c lc = new Lead_Convert__c();
        lc.Name = 'SGL';
        lc.Description__c = 'Self Generated Lead';
        lc.SourceNo__c = '022';
        insert lc;
        ResaleGlobalVariables__c rg = new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger',value__c='TRUE');
        insert rg;
        
        ADTWebLeadsAPI.DataPowerRequestInfo dataPowerReqInfoWrapInstance= new ADTWebLeadsAPI.DataPowerRequestInfo();
        dataPowerReqInfoWrapInstance.data=new ADTWebLeadsAPI.DataInfo();
        dataPowerReqInfoWrapInstance.data.formName= 'abc';
        dataPowerReqInfoWrapInstance.data.firstName = 'Iam';
        dataPowerReqInfoWrapInstance.data.firstName = 'TheBoss';
        dataPowerReqInfoWrapInstance.data.promotionCode= 'abc';
        dataPowerReqInfoWrapInstance.data.messageDivision= 'resi';
        dataPowerReqInfoWrapInstance.data.messageType= 'L';
        dataPowerReqInfoWrapInstance.data.emailSubject= 'abc';
        dataPowerReqInfoWrapInstance.data.businessName= 'abc';
        //dataPowerReqInfoWrapInstance.data.streetAddress= 'abc';
        //dataPowerReqInfoWrapInstance.data.city= 'abc';
        dataPowerReqInfoWrapInstance.data.stateCode= 'CO';
        dataPowerReqInfoWrapInstance.data.countryCode= 'US';
        dataPowerReqInfoWrapInstance.data.zipCode= '12345';
        dataPowerReqInfoWrapInstance.data.primaryPhoneNumber= '5615620780';
        //dataPowerReqInfoWrapInstance.data.secondaryPhoneNumber= '1234567890';
        dataPowerReqInfoWrapInstance.data.emailAddress= 'abc@tcs.com';
        //dataPowerReqInfoWrapInstance.data.ownershipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.sourceIP= '10.43.32.18';
        dataPowerReqInfoWrapInstance.data.createDateTime= '11223';
        dataPowerReqInfoWrapInstance.data.udf = new List<ADTWebLeadsAPI.UDFParamsInfo>();
        ADTWebLeadsAPI.UDFParamsInfo udfParamsInfoInstance=new ADTWebLeadsAPI.UDFParamsInfo();
        udfParamsInfoInstance.tag='Request_Message_Format';
        udfParamsInfoInstance.value='RQ20051015';
        dataPowerReqInfoWrapInstance.data.udf.add(udfParamsInfoInstance);
        
        String myJSON = JSON.serialize(dataPowerReqInfoWrapInstance);
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/api/createWebLead';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(myJSON); 
        
        RestContext.request = req;
        RestContext.response= res;
        ADTWebLeadsAPI.dopostMethod();
        list<id> audilid = new list<id>();
        for(Auditlog__c ad:[SELECT id from AuditLog__c]){
            audilid.add(ad.id);
        }
        Messaging.SendEmailResult[] sendem = ADTWebLeadsController.sendMassEmail(audilid);
        
        ADTEWCConsentAPI.addEWCWrapper objAddEWCWrap = new ADTEWCConsentAPI.addEWCWrapper(1, 2, 'Test 1', 'Test 2', 'Test 3', 'Test 4');
        ADTEWCConsentAPI.sendTextWrapper objSendTextWrap = new ADTEWCConsentAPI.sendTextWrapper(1,2, 'Test 1', 'Test 2', 'Test 3', 'Test 4', 'Test 5', 'Test 6', 'Test 7');
        ADTEWCConsentAPI.updateWrapper objUpdateWrap = new ADTEWCConsentAPI.updateWrapper(1,2, 'Test 1', 'Test 2', 'Test 3', false, true, 'Test 6');
        
        Map<Id, ID> mapAccAudit = new Map<Id, ID>();
        ADTEWCConsentAPI.sendText(mapAccAudit);
        
        Account acc = new Account();
        acc.name = 'test';
        acc.Business_Id__c = '1100 - Residential';
        acc.EquifaxApprovalType__c = 'CAE1';
        acc.EquifaxRiskGrade__c = 'Y';
        acc.Equifax_Last_Check_DateTime__c = system.today()-2;
        acc.MMBLookup__c = true;
        acc.LastMMBLookupRun__c =  System.now();
        acc.TelemarAccountNumber__c = '1233';
        insert acc;

        
        ADTEWCConsentAPI.sendToEWC(acc.id ,mapAccAudit.get(acc.Id));
    }
}