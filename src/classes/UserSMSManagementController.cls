public without sharing class UserSMSManagementController {
    
    public User u {get;set;}
    public Boolean SMSOption {get;set;}
    public Id UserId {get;set;}
    
    public UserSMSManagementController(ApexPages.StandardController sc){
        UserId = sc.getId();
        loadUserInfo();
        SMSOption = u.SMS_Enabled__c;
    }
    
    private void loadUserInfo(){
        u = [SELECT SMS_Enabled__c, MobilePhone, SMS_Enabled_Last_Updated__c FROM User WHERE Id = :UserId];
    }
    
    public PageReference saveSMSOption(){
        try{
            // refresh info on this user
            loadUserInfo();
            if( SMSOption && String.isBlank(u.MobilePhone) ){
                SMSOption = false;
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error, 'Unable to submit your request. You need to setup a mobile phone number.'));
                return null;
            }
            if( SMSOption !=  u.SMS_Enabled__c){
                u.SMS_Enabled_Last_Updated__c = DateTime.now();
                u.SMS_Enabled__c = SMSOption;
                update u;
                // If enabling this then notify user of the change
                if( u.SMS_Enabled__c ){
                    EmailMessageUtilities.NotifyUserBySMS( u.Id, true );
                }
                String smsActionMsg = ( u.SMS_Enabled__c )?'ENABLED':'DISABLED';
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Confirm, 'We\'ve received your request and <strong style="color:red">'+smsActionMsg+'</strong> sms messages to your user.'));
            }
            else{
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info, 'No changes requested.'));
            }
        }
        catch(Exception err){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error, 'Cannot save your selection. '+err.getMessage()));
        }
        return null;
    }
    
}