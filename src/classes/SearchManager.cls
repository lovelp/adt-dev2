/************************************* MODIFICATION LOG ********************************************************************************************
* SearchManager
*
* DESCRIPTION : Retrieves records from the Account, Lead, Street Sheet, and Street Sheet Item objects.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2011			- Original Version
*JaydipB					 4/29/2013			- Added fields RecordType.Name fro Sprint#9 Story#118
*JaydipB					 6/5/2013			- Added a new method to select Resale Accounts.		
*MikeT						 6/12/2014			- Added Sold_By_System__c to all methods that retrieve account											
*/

public with sharing class SearchManager {
	
	private static String selectString; 
    private static String whereString; 
    private static String orderByString;
    private static String limitString;
    private static List <SearchItem> searchResultList; 
    
    private enum SObjectType {ACCOUNT, LEAD}
    
    private static final String TIME_ZONE = '-05:00'; 
	
	public static List <SearchItem> findActiveAccounts(AccountSearchParameters parameters) {
	
		searchResultList = new List<SearchItem>();
	
		selectString = 'Select ID, Name, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, DisconnectDate__c, DisconnectReason__c, Data_Source__c, TenureDate__c, ANSCSold__c, SystemProfileCode__c, ServiceProfileCode__c,Type, DispositionDate__c, InService__c, Phone, NewMoverNewPhone__c, DispositionCode__c, Business_ID__c '; 
    	selectString += ' from Account '; 
    	
    	// All accounts returned must be Active and be assigned
    	whereString = getWhereString(parameters);
   	
    	// no order by
    	orderByString = ' ';
    	
    	limitString = ' LIMIT ' + parameters.queryRowLimit;
    	
    	processAccountQuery();
	
		return searchResultList;													
	}
  	
  	public static List<SearchItem> findLeads(AccountSearchParameters parameters) {
  		
		searchResultList = new List<SearchItem>();
		
		selectString = 'Select ID, Name, SiteStreet__c, SiteCity__c, SiteStateProvince__c, SitePostalCode__c, SitePostalCodeAddOn__c, LeadSource, Phone, NewMoverNewPhone__c, DispositionCode__c, Company, Business_ID__c, Type__c, Channel__c ';
		selectString += ' from Lead ';
		
		whereString = getWhereString(parameters, SObjectType.LEAD);
   	
    	// no order by
    	orderByString = ' ';
    	
    	limitString = ' LIMIT ' + parameters.queryRowLimit;
		
		//same limit string as account
		
		//processLeadQuery(parameters.queryRowLimit);
		processLeadQuery();
  		
  		return searchResultList;
  	}
  	
  	public static List <SearchItem> findStreetSheetItems(String streetSheetID, String orderBy) {
  		
  		selectString = 'Select ID, AccountID__c, LeadID__c, AccountName__c, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, DisconnectDate__c, DisconnectReason__c, Data_Source__c, Type__c, Latitude__c, Longitude__c, Business_ID__c, Phone__c, InService__c, DispositionCode__c '; 
    	selectString += ' from StreetSheetItem__c';
    	
    	whereString = ' where StreetSheet__c = \'' + String.escapeSingleQuotes(streetSheetID) + '\'';
    	orderByString = ' order by ' + orderBy;
    	
    	processStreetSheetItemQuery();
	
		return searchResultList;									
  	}
  	
  	public static List <SearchItem> findStreetSheetItems(String streetSheetID) {
	
		return findStreetSheetItems(streetSheetID, 'AccountName__c');									
  	}
  	
  	public static List <SearchItem> findStreetSheetItemsForMapping(String streetSheetID) {
	
		return findStreetSheetItems(streetSheetID, 'SortCode__c');									
  	}
  	
  	//originally wanted this to be more generic, but salesforce doesn't support generic type methods
  	public static void redistributeResults(List<SearchItem> accounts, List<SearchItem> leads, Integer reslimit) {
  		
  		Integer numleads = leads.size();
  		Integer numaccts = accounts.size();
  		Integer dist = resLimit / 2;
  		if (numAccts + numleads > resLimit) { //no processing needed if combined size of lists is smaller than max
  			if (numaccts > dist && numleads > dist) {	//too many accounts and leads
  				//remove from both lists until each equals dist
  				removeItemsFromBack(accounts, dist);
  				removeItemsFromBack(leads, dist);
  			}
  			else if (numaccts > dist && numleads < dist) {	//too many accounts
  				//remove from accounts until numaccts + numleads = resLimit
  				Integer desiredSize = resLimit - numleads;
  				removeItemsFromBack(accounts, desiredSize);
  			}
  			else if (numaccts < dist && numleads > dist) {	//too many leads
  				//remove from leads until numaccts + numleads = resLimit
  				Integer desiredSize = resLimit - numaccts;
  				removeItemsFromBack(leads, desiredSize);
  			}
  		}
  	}
  	
  	private static void removeItemsFromBack(List<SearchItem> l, Integer desiredSize) {
  		
  		for (Integer i=l.size()-1; i>=desiredSize; i--) {
  			l.remove(i);
  		}
  	}
  	
  	private static void processAccountQuery(){
  		
  		searchResultList = new List<SearchItem>();
  		
  		String fullSOQL = selectString +  whereString +  orderByString +  limitString;
  		Account[] accountResults = database.query(fullSOQL);
   	
		Integer i = 0;
		while (i < accountResults.size()) {
			searchResultList.add(new SearchItem(accountResults[i], false));		
			i++;
		}
  	}
  	
  	private static void processLeadQuery()
  	{
  		if (searchResultList == null)
  		{
  			searchResultList = new List<SearchItem>();
  		}
  		
  		String fullSOQL = selectString +  whereString +  orderByString +  limitString;
  		Lead[] leadResults = database.query(fullSOQL);
  		
  		for (Lead l : leadResults) {
  			searchResultList.add(new SearchItem(l, false));
  		}
  	}
  	
  	private static void processStreetSheetItemQuery(){
  		
  		searchResultList = new List<SearchItem>();
  		
  		String fullSOQL = selectString + whereString + orderByString;
   		StreetSheetItem__c[] streetSheetItemResults = database.query(fullSOQL);
   	
		Integer i = 0;
		while (i < streetSheetItemResults.size()) {
			searchResultList.add(new SearchItem(streetSheetItemResults[i]));		
			i++;
		}
  	}
  	
  	
	public static List <SearchItem> getAccounts(Set <String> accountIdSet) {
	
		selectString = 'Select ID, Name, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, ' +
						'Business_ID__c, DisconnectDate__c, Data_Source__c, Type, Phone, TenureDate__c, ANSCSold__c, SystemProfileCode__c, ServiceProfileCode__c,DispositionCode__c, DisconnectReason__c '
						; 
    	selectString += ' from Account '; 
    	
    	whereString = '';
    	// Construct where clause
    	if (!accountIdSet.isEmpty()) {
    		
    		Integer countOfCriteria=0;
    		whereString = ' WHERE ID IN (';
    		for (String s: accountIdSet){
				if (countOfCriteria!=0) {
					whereString += ', ';
				}	
				whereString += '\''+s+'\'';
				countOfCriteria++;
			}
			whereString += ') ';	
    	}
   	
    	orderByString = '';
    	limitString = '';
    	
    	processAccountQuery();
	
		return searchResultList;													
	} 
	
	
	public static Account getAccount(String id){
     	Account a; 
        try{
          	if(id != null)
            {
              a = [Select  OwnerId,  Id, Name, Latitude__c,Longitude__c,Phone, Type, ProcessingType__c, AddressID__c, Channel__c, LastActivityDate__c,  
                           Data_Source__c, TenureDate__c, ANSCSold__c, SystemProfileCode__c, ServiceProfileCode__c, DisconnectReason__c, DisconnectDate__c, InService__c, NewMover__c, NewMoverRecordID__C, SalesAppointmentDate__c,
                           NewMoverType__c, DispositionDate__c, DispositionCode__c, SiteStreet__C, SiteCity__c, SiteState__c, SitePostalCode__c, SiteCountryCode__c, TelemarOrderType__c,
                           NewMoverNewPhone__c, DateAssigned__c, Owner.Name, DispositionDetail__c, UnassignedLead__c, ScheduledInstallDate__c, RecordTypeId, AccountStatus__c, DNIS__c
		                   from Account 
		                   where id = :EncodingUtil.urlEncode(id,'UTF-8')
		                   limit 1];
            }            
        } catch(Exception e){ 
            a = null;
         }
         return a; 
    }
    
    public static Lead getLead(String id)
    {
    	Lead l;
    	try{
          	if(id != null)
            {
              l = [Select Id, Name, Latitude__c,Longitude__c, Phone, Owner.Name, UnassignedLead__c, LeadSource, AddressID__c, Channel__c,
						  NewMoverType__c, NewMover__c, DisconnectDate__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, DateAssigned__c, 
						  SiteStreet__C, SiteCity__c, SitePostalCode__c, SiteStateProvince__c, SiteCountryCode__c, Type__c
                     	  from Lead 
                     	  where id = :EncodingUtil.urlEncode(id,'UTF-8')
                          limit 1];
            }            
        } catch(Exception e){ 
            l = null;
         }
         return l;     	
    }
    
    public static Account getStandardAccountByTelemarAccountNumber(String accountNumber){
     	Account a; 
		RecordType rtype=Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account');       
		try {
          	if(accountNumber != null)
            {
              a = [Select  Name, FirstName__c, LastName__c, Spouse__c, ShippingStreet, ShippingStreet2__c, ShippingCity, ShippingState, 
              		ShippingPostalCode, ShippingCountry, SiteCrossStreet__c, SiteSubdivision__c, IncomingCounty__c, Phone, 
              		PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email__c, TelemarAccountNumber__c, PromotionCode__c, 
              		PromotionDescription__c, 
              		PromoterID__c, AccountStatus__c, TelemarAssignedRepName__c, 
              		DispositionCode__c, DispositionDetail__c, DispositionDate__c, DispositionEmployeeNumber__c, 
              		DispositionEmployeeName__c, DispositionComments__c, 
              		TelemarLeadSource__c, GenericMedia__c, QueriedSource__c, OutboundIndicator__c, IncomingLatitude__c, 
              		IncomingLongitude__c, Business_Id__c, Channel__c, CustomerID__c, SystemType__c, ADSCSold__c, ANSCSold__c, 
					PaymentType__c, ManuallyAssigned__c, DeviceList__c, LeadExternalID__c, AccountProfileInformation__c, InService__c, 
					NewLead__c, Data_Source__c, Type, ProcessingType__c, LeadStatus__c, ArchivedReason__c,
					SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SiteCounty__c, SiteCountryCode__c,
					Latitude__c, Longitude__c, ScheduledInstallDate__c, SalesAppointmentDate__c, TransitionDate1__c,
					TransitionDate2__c, TransitionDate3__c,  TransitionDate4__c, TransitionDate5__c, Sold_By_System__c, TelemarOrderType__c,
					RecordTypeId, OwnerId, Affiliation__c, SoldDate__c, BillingSystem__c, ReferredBy__c, TaskCode__c, SalesApptCancelledFlag__c, DNIS__c
                     from Account 
                     where TelemarAccountNumber__c = :EncodingUtil.urlEncode(accountNumber,'UTF-8')
                     and RecordTypeID=:rtype.Id
                     limit 1];
            }
            
        } catch(Exception e){ 
            a = null;
        }
        return a; 
    }
    
    
    public static Account getAccountByTelemarAccountNumber(String accountNumber){
     	Account a; 
		       
		try {
          	if(String.isNotBlank(accountNumber))
            {
              a = [Select  Name, FirstName__c, LastName__c, Spouse__c, ShippingStreet, ShippingStreet2__c, ShippingCity, ShippingState, 
              		ShippingPostalCode, ShippingCountry, SiteCrossStreet__c, SiteSubdivision__c, IncomingCounty__c, Phone, 
              		PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email__c, TelemarAccountNumber__c, PromotionCode__c, 
              		PromotionDescription__c, AddressID__c,
              		PromoterID__c, AccountStatus__c, TelemarAssignedRepName__c, 
              		DispositionCode__c, DispositionDetail__c, DispositionDate__c, DispositionEmployeeNumber__c, 
              		DispositionEmployeeName__c, DispositionComments__c, 
              		TelemarLeadSource__c, GenericMedia__c, QueriedSource__c, OutboundIndicator__c, IncomingLatitude__c, 
              		IncomingLongitude__c, Business_Id__c, Channel__c, CustomerID__c, SystemType__c, ADSCSold__c, ANSCSold__c, 
					PaymentType__c, ManuallyAssigned__c, DeviceList__c, LeadExternalID__c, AccountProfileInformation__c, InService__c, 
					NewLead__c, Data_Source__c, Type, ProcessingType__c, LeadStatus__c, ArchivedReason__c,
					SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SiteCounty__c, SiteCountryCode__c,
					Latitude__c, Longitude__c, ScheduledInstallDate__c, SalesAppointmentDate__c, TransitionDate1__c,
					TransitionDate2__c, TransitionDate3__c,  TransitionDate4__c, TransitionDate5__c, Sold_By_System__c, TelemarOrderType__c,
					RecordTypeId, OwnerId, Affiliation__c, SoldDate__c, BillingSystem__c, ReferredBy__c, TaskCode__c, SalesApptCancelledFlag__c, DNIS__c
                     from Account 
                     where TelemarAccountNumber__c = :EncodingUtil.urlEncode(accountNumber,'UTF-8')
                     limit 1 FOR UPDATE];
            }
            
        } catch(Exception e){ 
            a = null;
        }
        return a; 
    }
    
   /* Implementation of the method initially deployed with Sprint 4/5 code
      Reverted to Sprint 3 code following lengthy (1 min+) response times causing web service calls from 
      Telemar to time out
      SOQL query using RifTelemarAccountNumber was in place to support setProspect calls before
      an install appointment resulting from an add-on appointment but that functionality was de-scoped
      and thus, no reason to retain
    public static Account getAccountByTelemarAccountNumber(String accountNumber){
     	Account a = null; 
        
        if(accountNumber != null)
        {
            try {
            	// First attempt to find an Account with RifTelemarAccountNumber populated
            	// this should be a RIF account from GDW
              	a = [Select  Name, FirstName__c, LastName__c, Spouse__c, ShippingStreet, ShippingStreet2__c, ShippingCity, ShippingState, 
              		ShippingPostalCode, ShippingCountry, SiteCrossStreet__c, SiteSubdivision__c, IncomingCounty__c, Phone, 
              		PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email__c, TelemarAccountNumber__c, PromotionCode__c, 
              		PromotionDescription__c, 
              		PromoterID__c, AccountStatus__c, TelemarAssignedRepName__c, 
              		DispositionCode__c, DispositionDetail__c, DispositionDate__c, DispositionEmployeeNumber__c, 
              		DispositionEmployeeName__c, DispositionComments__c, 
              		TelemarLeadSource__c, GenericMedia__c, QueriedSource__c, OutboundIndicator__c, IncomingLatitude__c, 
              		IncomingLongitude__c, Business_Id__c, Channel__c, CustomerID__c, SystemType__c, ADSCSold__c, ANSCSold__c, 
					PaymentType__c, ManuallyAssigned__c, DeviceList__c, LeadExternalID__c, AccountProfileInformation__c, InService__c, 
					NewLead__c, Data_Source__c, Type, ProcessingType__c, LeadStatus__c, ArchivedReason__c,
					SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SiteCounty__c, SiteCountryCode__c,
					Latitude__c, Longitude__c, ScheduledInstallDate__c, SalesAppointmentDate__c, TransitionDate1__c,
					TransitionDate2__c, TransitionDate3__c,  TransitionDate4__c, TransitionDate5__c,
					RecordTypeId, OwnerId, Affiliation__c, SoldDate__c, BillingSystem__c, ReferredBy__c, TaskCode__c
                     from Account 
                     where RifTelemarAccountNumber__c = :EncodingUtil.urlEncode(accountNumber,'UTF-8')
                     limit 1];
            } catch(Exception e){ 
            	a = null;
        	}	              
            
            if (a == null) {
            	try {	
           			// Then attempt to find an Account with TelemarAccountNumber populated
            		// this should be a standard account from Telemar   
             		a = [Select  Name, FirstName__c, LastName__c, Spouse__c, ShippingStreet, ShippingStreet2__c, ShippingCity, ShippingState, 
              			ShippingPostalCode, ShippingCountry, SiteCrossStreet__c, SiteSubdivision__c, IncomingCounty__c, Phone, 
              			PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email__c, TelemarAccountNumber__c, PromotionCode__c, 
              			PromotionDescription__c, 
              			PromoterID__c, AccountStatus__c, TelemarAssignedRepName__c, 
              			DispositionCode__c, DispositionDetail__c, DispositionDate__c, DispositionEmployeeNumber__c, 
              			DispositionEmployeeName__c, DispositionComments__c, 
              			TelemarLeadSource__c, GenericMedia__c, QueriedSource__c, OutboundIndicator__c, IncomingLatitude__c, 
              			IncomingLongitude__c, Business_Id__c, Channel__c, CustomerID__c, SystemType__c, ADSCSold__c, ANSCSold__c, 
						PaymentType__c, ManuallyAssigned__c, DeviceList__c, LeadExternalID__c, AccountProfileInformation__c, InService__c, 
						NewLead__c, Data_Source__c, Type, ProcessingType__c, LeadStatus__c, ArchivedReason__c,
						SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SiteCounty__c, SiteCountryCode__c,
						Latitude__c, Longitude__c, ScheduledInstallDate__c, SalesAppointmentDate__c, TransitionDate1__c,
						TransitionDate2__c, TransitionDate3__c,  TransitionDate4__c, TransitionDate5__c,
						RecordTypeId, OwnerId, Affiliation__c, SoldDate__c, BillingSystem__c, ReferredBy__c, TaskCode__c
                     	from Account 
                     	where TelemarAccountNumber__c = :EncodingUtil.urlEncode(accountNumber,'UTF-8')
                     	limit 1];    	
              	} catch(Exception e){ 
            		a = null;
        		}      
            }
        }       
        return a; 
    }
  */  
    
    public static Account getAccountByLeadExternalID(String leadExternalID){
     	Account a; 
       
        try {
          	if(leadExternalID != null)
            {
              a = [Select  Name, FirstName__c, LastName__c, Spouse__c, ShippingStreet, ShippingStreet2__c, ShippingCity, ShippingState, 
              		ShippingPostalCode, ShippingCountry, SiteCrossStreet__c, SiteSubdivision__c, IncomingCounty__c, Phone, 
              		PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email__c, TelemarAccountNumber__c, PromotionCode__c, 
              		PromotionDescription__c, 
              		PromoterID__c, AccountStatus__c, TelemarAssignedRepName__c, 
              		DispositionCode__c, DispositionDetail__c, DispositionDate__c, DispositionEmployeeNumber__c, 
              		DispositionEmployeeName__c, DispositionComments__c, TelemarLeadSource__c, 
              		GenericMedia__c, QueriedSource__c, OutboundIndicator__c, IncomingLatitude__c, IncomingLongitude__c, 
              		Business_Id__c, Channel__c, CustomerID__c, SystemType__c, ADSCSold__c, ANSCSold__c, PaymentType__c, 
					ManuallyAssigned__c, DeviceList__c, LeadExternalID__c, AccountProfileInformation__c, InService__c, SoldDate__c, 
					NewLead__c, Data_Source__c, Type, ProcessingType__c, LeadStatus__c, ArchivedReason__c, TransitionDate1__c,
					TransitionDate2__c, TransitionDate3__c,  TransitionDate4__c, TransitionDate5__c, Sold_By_System__c, TelemarOrderType__c,
					ScheduledInstallDate__c, SalesAppointmentDate__c, RecordTypeId, OwnerId, Affiliation__c, 
					BillingSystem__c, ReferredBy__c, TaskCode__c, DNIS__c
                     from Account 
                     where LeadExternalID__c = :EncodingUtil.urlEncode(leadExternalID,'UTF-8')
                          limit 1];
            }
            
        } catch(Exception e){ 
            a = null;
        }
        return a; 
    }   
    
    
    //Jaydip B created below method to select Resale Accounts for Sprin#11.
     public static Account getresaleAccount(String leadExternalID,String accountNumber){
     	Account a; 
         RecordType rtype=Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account');
        try {
          	if(leadExternalID != null && accountNumber==null )
            {
              a = [Select  Name, FirstName__c, LastName__c, Spouse__c, ShippingStreet, ShippingStreet2__c, ShippingCity, ShippingState, 
              		ShippingPostalCode, ShippingCountry, SiteCrossStreet__c, SiteSubdivision__c, IncomingCounty__c, Phone, 
              		PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email__c, TelemarAccountNumber__c, PromotionCode__c, 
              		PromotionDescription__c, 
              		PromoterID__c, AccountStatus__c, TelemarAssignedRepName__c, 
              		DispositionCode__c, DispositionDetail__c, DispositionDate__c, DispositionEmployeeNumber__c, 
              		DispositionEmployeeName__c, DispositionComments__c, TelemarLeadSource__c, 
              		GenericMedia__c, QueriedSource__c, OutboundIndicator__c, IncomingLatitude__c, IncomingLongitude__c, 
              		Business_Id__c, Channel__c, CustomerID__c, SystemType__c, ADSCSold__c, ANSCSold__c, PaymentType__c, 
					ManuallyAssigned__c, DeviceList__c, LeadExternalID__c, AccountProfileInformation__c, InService__c, SoldDate__c, 
					NewLead__c, Data_Source__c, Type, ProcessingType__c, LeadStatus__c, ArchivedReason__c, TransitionDate1__c,
					TransitionDate2__c, TransitionDate3__c,  TransitionDate4__c, TransitionDate5__c, Sold_By_System__c, TelemarOrderType__c,
					ScheduledInstallDate__c, SalesAppointmentDate__c, RecordTypeId, OwnerId, Affiliation__c, 
					BillingSystem__c, ReferredBy__c, TaskCode__c, DNIS__c
                     from Account 
                     where LeadExternalID__c = :EncodingUtil.urlEncode(leadExternalID,'UTF-8')
                     and RecordTypeID=:rtype.ID
                     limit 1];
            }
            else if (accountNumber!=null){
            	a = [Select  Name, FirstName__c, LastName__c, Spouse__c, ShippingStreet, ShippingStreet2__c, ShippingCity, ShippingState, 
              		ShippingPostalCode, ShippingCountry, SiteCrossStreet__c, SiteSubdivision__c, IncomingCounty__c, Phone, 
              		PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email__c, TelemarAccountNumber__c, PromotionCode__c, 
              		PromotionDescription__c, 
              		PromoterID__c, AccountStatus__c, TelemarAssignedRepName__c, 
              		DispositionCode__c, DispositionDetail__c, DispositionDate__c, DispositionEmployeeNumber__c, 
              		DispositionEmployeeName__c, DispositionComments__c, TelemarLeadSource__c, 
              		GenericMedia__c, QueriedSource__c, OutboundIndicator__c, IncomingLatitude__c, IncomingLongitude__c, 
              		Business_Id__c, Channel__c, CustomerID__c, SystemType__c, ADSCSold__c, ANSCSold__c, PaymentType__c, 
					ManuallyAssigned__c, DeviceList__c, LeadExternalID__c, AccountProfileInformation__c, InService__c, SoldDate__c, 
					NewLead__c, Data_Source__c, Type, ProcessingType__c, LeadStatus__c, ArchivedReason__c, TransitionDate1__c,
					TransitionDate2__c, TransitionDate3__c,  TransitionDate4__c, TransitionDate5__c, Sold_By_System__c, TelemarOrderType__c,
					ScheduledInstallDate__c, SalesAppointmentDate__c, RecordTypeId, OwnerId, Affiliation__c, 
					BillingSystem__c, ReferredBy__c, TaskCode__c, DNIS__c
                     from Account 
                     where TelemarAccountNumber__c = :EncodingUtil.urlEncode(accountNumber,'UTF-8')
                     and RecordTypeID=:rtype.ID
                     limit 1];
            	
            }
            
            
            
        } catch(Exception e){ 
            a = null;
        }
        return a; 
    }   
    
    public static Account getAccount(String billingSystem, String customerID){
     	Account a; 
        try {
          	if(billingSystem != null && customerID != null)
            {
              a = [Select  Name, FirstName__c, LastName__c, Spouse__c, ShippingStreet, ShippingStreet2__c, ShippingCity, ShippingState, LastActivityDate__c,
              		ShippingPostalCode, ShippingCountry, SiteCrossStreet__c, SiteSubdivision__c, IncomingCounty__c, Phone, 
              		PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email__c, TelemarAccountNumber__c, PromotionCode__c, 
              		PromotionDescription__c, PromoterID__c, AccountStatus__c, TelemarAssignedRepName__c, 
              		DispositionCode__c, DispositionDetail__c, DispositionDate__c, DispositionEmployeeNumber__c, 
              		DispositionEmployeeName__c, DispositionComments__c, 
              		TelemarLeadSource__c, GenericMedia__c, QueriedSource__c, OutboundIndicator__c, IncomingLatitude__c, 
              		IncomingLongitude__c, Business_Id__c, Channel__c, CustomerID__c, SystemType__c, ADSCSold__c, ANSCSold__c, 
					PaymentType__c, ManuallyAssigned__c, DeviceList__c, LeadExternalID__c, AccountProfileInformation__c, InService__c, 
					NewLead__c, Data_Source__c, Type, ProcessingType__c, LeadStatus__c, ArchivedReason__c,
					SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SiteCounty__c, SiteCountryCode__c,
					Latitude__c, Longitude__c, ScheduledInstallDate__c, SalesAppointmentDate__c, TransitionDate1__c,
					TransitionDate2__c, TransitionDate3__c,  TransitionDate4__c, TransitionDate5__c, Sold_By_System__c, TelemarOrderType__c,
					RecordTypeId, OwnerId, Affiliation__c, SoldDate__c, BillingSystem__c, ReferredBy__c, TaskCode__c, DNIS__c
                     from Account 
                     where BillingSystem__c = :EncodingUtil.urlEncode(billingSystem,'UTF-8')
                     and CustomerID__c = :EncodingUtil.urlEncode(customerID, 'UTF-8')
                     limit 1];
            }
            
        } catch(Exception e){ 
            a = null;
        }
        return a; 
    }
    
    public static Account getAccount(String billingSystem, String customerID, String accountType){
     	Account a;
     	List<Account> acctList = new List<Account>(); 
        try {
          	if(billingSystem != null && customerID != null && accountType != null)
            {
              	acctList = [Select  Name, FirstName__c, LastName__c, Spouse__c, ShippingStreet, ShippingStreet2__c, ShippingCity, ShippingState, LastActivityDate__c,
              		ShippingPostalCode, ShippingCountry, SiteCrossStreet__c, SiteSubdivision__c, IncomingCounty__c, Phone, 
              		PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c, Email__c, TelemarAccountNumber__c, PromotionCode__c, 
              		PromotionDescription__c, PromoterID__c, AccountStatus__c, TelemarAssignedRepName__c, 
              		DispositionCode__c, DispositionDetail__c, DispositionDate__c, DispositionEmployeeNumber__c, 
              		DispositionEmployeeName__c, DispositionComments__c, 
              		TelemarLeadSource__c, GenericMedia__c, QueriedSource__c, OutboundIndicator__c, IncomingLatitude__c, 
              		IncomingLongitude__c, Business_Id__c, Channel__c, CustomerID__c, SystemType__c, ADSCSold__c, ANSCSold__c, 
					PaymentType__c, ManuallyAssigned__c, DeviceList__c, LeadExternalID__c, AccountProfileInformation__c, InService__c, 
					NewLead__c, Data_Source__c, Type, ProcessingType__c, LeadStatus__c, ArchivedReason__c,
					SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SiteCounty__c, SiteCountryCode__c,
					Latitude__c, Longitude__c, ScheduledInstallDate__c, SalesAppointmentDate__c, TransitionDate1__c,
					TransitionDate2__c, TransitionDate3__c,  TransitionDate4__c, TransitionDate5__c, Sold_By_System__c, TelemarOrderType__c,
					RecordTypeId, OwnerId, Affiliation__c, SoldDate__c, BillingSystem__c, ReferredBy__c, TaskCode__c, DNIS__c
                     from Account 
                     where BillingSystem__c = :EncodingUtil.urlEncode(billingSystem,'UTF-8')
                     and CustomerID__c = :EncodingUtil.urlEncode(customerID, 'UTF-8')
                     and Type = :accountType];
                     
              	if (acctList.size() == 1) {
            		a = acctList[0];
              	}
            }
            
        } catch(Exception e){ 
            a = null;
        }
        return a; 
    }   
    
    private static String getWhereString(AccountSearchParameters parameters)
    {
    	return getWhereString(parameters, SObjectType.ACCOUNT);
    }
    
	private static String getWhereString(AccountSearchParameters parameters, SObjectType sob)
	{
		//common fields
		String wherestr = ' WHERE ';
		
		//do this before common so I don't have to worry about whether or not I need to put "AND" at the beginning of each wherestr addition
		if (sob == SObjectType.ACCOUNT)
		{
			wherestr += ' LeadStatus__c = \'Active\' ';
		}
		else if (sob == SObjectType.LEAD)
		{
			//wherestr += ' Status = \'Open\' ';
			// need an initial condition so I don't have to worry about 'AND' in all other conditions
			wherestr += ' isConverted = FALSE ';
		}
    	
    	// Any accounts which are already items in this Street Sheet should be excluded from the search
    	if (parameters.accountIdSet != null && !parameters.accountIdSet.isEmpty()) {
    		
    		Integer countOfCriteria=0;
    		wherestr += ' AND ID NOT IN (';
    		for (String s: parameters.accountIdSet){
				if (countOfCriteria!=0) {
					wherestr += ', ';
				}	
				wherestr += '\''+ String.escapeSingleQuotes(s)+'\'';
				countOfCriteria++;
			}
			wherestr += ') ';	
    	}
    	
    	
    	// Match on Data Source if supplied
    	if (parameters.dataSource != null && parameters.dataSource.size() != 0 && !Utilities.contains(parameters.dataSource, PicklistHelper.ALL)) {
    		
    		wherestr += ' AND ';
    		if (sob == SObjectType.ACCOUNT)
    		{
    			wherestr += ' Data_Source__c ';
    		}
    		else if (sob == SObjectType.LEAD)
    		{
    			wherestr += ' LeadSource ';
    		}
    		
    		/*if ( Utilities.contains(parameters.dataSource, PicklistHelper.ALL) ) {
    			wherestr += ' <> NULL ';
    		}
    		else {*/
	    		wherestr += ' IN (';
	    		
	    		for (Integer i=0; i < parameters.dataSource.size(); i++) {
					if (i != 0) {
						wherestr += ', ';
					}	
					wherestr += '\'' + String.escapeSingleQuotes(parameters.dataSource[i]) + '\'';
				}
				wherestr += ') ';
    		//}
    	}
    	
    	// Match on Lead Type if supplied
    	if (parameters.leadType != null && parameters.leadType.size() != 0 && !Utilities.contains(parameters.leadType, PicklistHelper.ALL)) {
    		
    		if (sob == SObjectType.ACCOUNT) {
    			wherestr += ' AND Type ';
    		}
    		else {
    			wherestr += ' AND Type__c ';
    		}
    		
    		/*if ( Utilities.contains(parameters.leadType, PicklistHelper.ALL) ) {
    			wherestr += ' <> NULL ';
    		}
    		else {*/
	    		wherestr += ' IN (';
	    		
	    		for (Integer i=0; i < parameters.leadType.size(); i++) {
					if (i != 0) {
						wherestr += ', ';
					}	
					wherestr += '\'' + String.escapeSingleQuotes(parameters.leadType[i]) + '\'';
				}
				wherestr += ') ';
    		//}
    	}
	    		
    	// Match on Non-worked Lead Indicator if supplied
    	if (parameters.nonWorkedLeadIndicator) {
    		
    		wherestr += ' AND NewLead__c = ' + parameters.nonWorkedLeadIndicator;
    		
    	}
	    	
    	// Match on New Mover Indicator if supplied
    	if (parameters.newMoverIndicator) {
    		
    		wherestr += ' AND NewMover__c = ' + parameters.newMoverIndicator;
    		
    	}
    	
    	// Match on Site Street if supplied
    	if (parameters.siteStreet != null && parameters.siteStreet.length() != 0) {   		
    		
    		wherestr += ' AND SiteStreet__c LIKE ' + '\'' + '%' + String.escapeSingleQuotes(parameters.siteStreet) + '%' + '\'';
    	}
    	
    	// Match on Site City if supplied
    	if (parameters.siteCity != null && parameters.siteCity.length() != 0) {   		
    		
    		wherestr += ' AND SiteCity__c = ' + '\'' + String.escapeSingleQuotes(parameters.siteCity) + '\'';
    	}
    	
    	// Match on Site State if supplied
    	if (parameters.siteState != null && parameters.siteState.size() != 0 && !Utilities.contains(parameters.siteState, PicklistHelper.ALL)) {
    		
    		wherestr += ' AND ';
    		if (sob == SObjectType.ACCOUNT)
    		{
    			wherestr += ' SiteState__c ';
    		}
    		else if (sob == SObjectType.LEAD)
    		{
    			wherestr += ' SiteStateProvince__c ';
    		}
    		
    		/*if ( Utilities.contains(parameters.siteState, PicklistHelper.ALL) ) {
    			wherestr += ' <> NULL ';
    		}
    		else {*/
	    		wherestr += ' IN (';
	    		
	    		for (Integer i=0; i < parameters.siteState.size(); i++) {
					if (i != 0) {
						wherestr += ', ';
					}	
					wherestr += '\'' + String.escapeSingleQuotes(parameters.siteState[i]) + '\'';
				}
				wherestr += ') ';
    		//}
    	}
    	
    	// Match on Site Postal Code if supplied
    	if (parameters.sitePostalCode != null && parameters.sitePostalCode.length() != 0) {   		
    		
    		wherestr += ' AND SitePostalCode__c = ' + '\'' + String.escapeSingleQuotes(parameters.sitePostalCode) + '\'';
    	}
    	
    	if ( parameters.sitePostalCodeAddOn != null && parameters.sitePostalCodeAddOn.length() != 0 ) {
    		wherestr += ' AND SitePostalCodeAddOn__c = ' + '\'' + String.escapeSingleQuotes(parameters.sitePostalCodeAddOn) + '\'';
    	}
    	
    	// Match on not null phone fields if Phone Available Indicator supplied
    	if (parameters.phoneAvailableIndicator) {
    		if (sob == SObjectType.ACCOUNT)
    		{
    			wherestr += ' AND (Phone != null OR PhoneNumber2__c != null OR PhoneNumber3__c != null OR PhoneNumber4__c != null OR NewMoverNewPhone__c != null)';
    		}
    		else if (sob == SObjectType.LEAD)
    		{
    			wherestr += ' AND (Phone != null OR NewMoverNewPhone__c != null ) ';
    		}
    	}
    	
    	// Match on Date Assigned if supplied
    	if (parameters.dateAssignedAfter != null) {
    		// Format the date as required, appending the TIME_ZONE literal
    		// This approach is necessary since the date format mask 'yyyy-MM-dd\'T\'HH:mm:ssZ' returns 
    		// a value 2011-01-01T00:00:00-0500 whereas the required format is 2011-01-01T00:00:00-05:00
    		wherestr += ' AND DateAssigned__c >= ' + String.escapeSingleQuotes(parameters.dateAssignedAfter.format('yyyy-MM-dd\'T\'HH:mm:ss') + TIME_ZONE);
    		
    	}
    	if (parameters.dateAssignedBefore != null) {
    		// Format the date as required, appending the TIME_ZONE literal
    		// This approach is necessary since the date format mask 'yyyy-MM-dd\'T\'HH:mm:ssZ' returns 
    		// a value 2011-01-01T00:00:00-0500 whereas the required format is 2011-01-01T00:00:00-05:00
    		wherestr += ' AND DateAssigned__c <= ' + String.escapeSingleQuotes(parameters.dateAssignedBefore.format('yyyy-MM-dd\'T\'HH:mm:ss') + TIME_ZONE);
    		
    	}
    	
    	if (parameters.newMoverDateAfter != null) {
    		wherestr += ' AND NewMoverDate__c >= ' + String.escapeSingleQuotes(Utilities.dateToSoql(parameters.newMoverDateAfter));
    	}
    	
    	if (parameters.newMoverDateBefore != null) {
    		wherestr += ' AND NewMoverDate__c <= ' + String.escapeSingleQuotes(Utilities.dateToSoql(parameters.newMoverDateBefore));
    	}
    	
    	if (parameters.dispositionCode != null && parameters.dispositionCode.size() > 0 && !Utilities.contains(parameters.dispositionCode, PicklistHelper.ALL)) {
    		wherestr += ' AND DispositionCode__c ';
    		
    		/*if (Utilities.contains(parameters.dispositionCode, PicklistHelper.ALL)) {
    			wherestr += ' <> NULL ';
    		}
    		else {*/
	    		wherestr += ' IN (';
	    		String dlist = '';
	    		for (String s : parameters.dispositionCode) {
	    			dlist += '\'' + String.escapeSingleQuotes(s) + '\',';
	    		}
	    		//remove extra comma
	    		dlist = dlist.subString(0, dlist.length() - 1);
	    		
	    		wherestr += dlist + ') ';
    		//}
    	}
	    	
		//unique account fields
		if (sob == SObjectType.ACCOUNT)
		{
			wherestr += ' AND UnassignedLead__c = false ';
			
			if (parameters.salesAppointmentDateAfter != null) {
				wherestr += ' AND DateAssigned__c >= ' + String.escapeSingleQuotes(parameters.salesAppointmentDateAfter.format('yyyy-MM-dd\'T\'HH:mm:ss') + TIME_ZONE);
			}
			
			if (parameters.salesAppointmentDateBefore != null) {
				wherestr += ' AND DateAssigned__c <= ' + String.escapeSingleQuotes(parameters.salesAppointmentDateBefore.format('yyyy-MM-dd\'T\'HH:mm:ss') + TIME_ZONE);
			}
	    	
	    	if (parameters.phoneSale != null && parameters.phoneSale) {
	    		wherestr += ' AND ProcessingType__c = \'NSC\' AND Data_Source__c = \'Telemar\'';
	    	}
	    	
	    	// Match on Disco Date if supplied
	   	
	    	if (parameters.discoDateAfter != null) {
	    		wherestr += ' AND DisconnectDate__c >= ' + String.escapeSingleQuotes(Utilities.dateToSoql(parameters.discoDateAfter));
	    		
	    	}
	    	if (parameters.discoDateBefore != null) {
	    		wherestr += ' AND DisconnectDate__c <= ' + String.escapeSingleQuotes(Utilities.dateToSoql(parameters.discoDateBefore));
	    		
	    	}
	    	
	    	// Match on Disco Reason if supplied
	    	if (parameters.discoReason != null && parameters.discoReason.size() != 0 && !Utilities.contains(parameters.discoReason, PicklistHelper.ALL)) {
	    		
	    		wherestr += ' AND DisconnectReason__c ';
	    		
	    		/*if (Utilities.contains(parameters.discoReason, PicklistHelper.ALL)) {
	    			wherestr += ' <> NULL ';
	    		}
	    		else {*/
	    			wherestr += ' IN (';
		    		for (Integer i=0; i < parameters.discoReason.size(); i++) {
						if (i != 0) {
							wherestr += ', ';
						}	
						wherestr += '\'' + String.escapeSingleQuotes(parameters.discoReason[i]) + '\'';
					}
					wherestr += ') ';
	    		//}
	    	}
	    
	    	// Match on Active Site Indicator if supplied
	    	if (parameters.activeSiteIndicator) {
	    		
	    		wherestr += ' AND InService__c = ' + parameters.activeSiteIndicator;
	    		
	    	}
	    	
	    	if (parameters.manuallyAssigned) {
	    		wherestr += ' AND ManuallyAssigned__c = ' + parameters.manuallyAssigned;
	    	}
		}
		//unique lead fields
		else if (sob == SObjectType.LEAD)
		{
			
		}
		
		return wherestr;
	}
	
	public static Lead getLeadByTelemarAccountNumber(String accountNumber){
     	Lead l; 
        try {
          	if(accountNumber != null)
            {
              l = [Select  Id 
                     from Lead 
                     where TelemarAccountNumber__c = :EncodingUtil.urlEncode(accountNumber,'UTF-8')
                     limit 1];
            }
            
        } catch(Exception e){ 
            l = null;
        }
        return l; 
    }

}