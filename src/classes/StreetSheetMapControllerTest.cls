@isTest
private class StreetSheetMapControllerTest {
	
	static testMethod void testConstructorNoQueryStringParam() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
        
        System.runAs(salesRep) {    
        	Account a = TestHelperClass.createAccountData();
        }	
		
		Test.startTest();
		System.runAs(salesRep) {
			ApexPages.PageReference ref = new PageReference('/apex/StreetSheetMap');
	    	Test.setCurrentPageReference(ref);
	    	
	    	StreetSheetMapController ssmc = new StreetSheetMapController();
	    	
	    	System.assert(ssmc.streetSheetName == null, 'Without a street sheet id, should have no name either');
	    	System.assertEquals(0, ssmc.noGeocodeItemsList.size(), 'Without a street sheet id, no items should have geocode issues');
		
		}
		
		Test.stopTest();
		
	
	}
	
	static testMethod void testConstructorQueryStringParam() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
        
        StreetSheet__c ss;
        System.runAs(salesRep) {    
        	//Account a1 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
        	
        	Account a2 = TestHelperClass.createAccountData();
        	
        	Account a3 = createBadGeocodeAccount();
        	
        	ss = new StreetSheet__c();
			ss.Name = 'Unit Test Street Sheet';
		
			insert ss;

		
			StreetSheetItem__c ssi2 = new StreetSheetItem__c();
			ssi2.AccountID__c = a2.Id;
			ssi2.StreetSheet__c = ss.Id;
		
			insert ssi2;
			
			StreetSheetItem__c ssi3 = new StreetSheetItem__c();
			ssi3.AccountID__c = a3.Id;
			ssi3.StreetSheet__c = ss.Id;
		
			insert ssi3;
			
			
        }	
		
		Test.startTest();
		System.runAs(salesRep) {
			ApexPages.PageReference ref = new PageReference('/apex/StreetSheetMap?ssid=' + ss.Id);
	    	Test.setCurrentPageReference(ref);
	    	
	    	StreetSheetMapController ssmc = new StreetSheetMapController();
	    	
	    	System.assert(ssmc.streetSheetName != null, 'With a street sheet id, should have the name of the sheet');
	    	
	    	System.assert(ssmc.mapDataPointsStr.length() > 0, 'Expect the string of map data to be created');
	    	System.assertEquals(1, ssmc.noGeocodeItemsList.size(), 'Expect one item without geocoding');
	    	
	    	// page calls initMapLoad() as part of the window.onLoad event so do that explicitly to simulate behavior
	    	PageReference newRef = ssmc.initMapLoad();
	    	System.assert(newRef == null, 'Expect the page reference to be null meaning no navigation');
		
		}
		
		Test.stopTest();
		
	
	}
	
	private static Account createBadGeocodeAccount()
	{
		//Create Addresses first
		Address__c addr = new Address__c();
		addr.Latitude__c = 0;
		addr.Longitude__c = 0;
		addr.Street__c = '8280 Greensboro Dr';
		addr.State__c = 'VA';
		addr.PostalCode__c = '221o2';
		addr.OriginalAddress__c = '8280 Greensboro Dr, McLean, VA 221o2';
		
		insert addr;
		
		
		Account acct = new Account();
		acct.AddressID__c = addr.Id;
		acct.Name = 'No Geocode';
		acct.LeadStatus__c = 'Active';
		acct.UnassignedLead__c = false;
		acct.Business_Id__c = '1100 - Residential';
		
		insert acct;
		return acct;
	}
	

}