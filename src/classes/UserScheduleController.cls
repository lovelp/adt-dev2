/************************************* MODIFICATION LOG ********************************************************************************************
* UserScheduleController
*
* DESCRIPTION : Apex Controller to manage user schedule by town and Business ID or Line of Business               
*
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shiva Pochamalla                                   - Original Version
* Anna Mounika                  9/28/2016            - Removed Territory Management 2.0 and started to use Town+Subtown from the Postal Code table
* Magdiel Herrera               9/28/2016            - Removed town selection moved this to the admin interface
*
*/


public class UserScheduleController {
    
    public class UserScheduleControllerException extends Exception {}
    
    public static String IN_HOUSE_ACTIVATION = 'DLL';
    
    public String Userid;
    public String UserName {get;set;}
    public String selectedRecordId {get;set;}
    public String newTerrId {get;set;}
    public String newTown {get;set;}
    public String deletedScheduleId {get;set;}
    public String recIdtoUpdate {get;set;}
    public String Town {get;set;}
    public String Subtown {get;set;}
    public String BusinessID {get;set;}
    public Boolean showSaveAddress {get;set;}
    public Boolean showActionButtons {get;set;}
    public Boolean showSave {get;set;}
    public Boolean showAddressValidation {get;set;}
    public Boolean isNewScheduleValid {get;set;}
    public Boolean isHomeActivation {get;set;}
    public Boolean renderActionConfirmPanel {
        get {
            return (usd != null && UserScheduleData.SetupAction.NEW_SCHEDULE == usd.initAction );
        }
    }
    
    public addressLookup adlookup {get;set;}
    public UserScheduleData usd {get;set;}
    
    private MelissaDataAPI.MelissaDataResults mdr;
    private String originalAddress;

    private SchedUserTerr__c selectedScheduledTerr;
    public User currentUser;
    
    public List<SchedUserTerr__c> schedTerrList {get;set;}
    public List<Postal_Codes__c> postalcodeTownList {get;set;}
    public List<SchedDefaults__c> scheduleDefaults {get;set;}
    public List<SchedUserTerr__c> tobeDeleted {get;set;}

    public List<SelectOption> SelectableTime {
        get{
            SelectableTime = new List<SelectOption>();
            SelectableTime.add(new SelectOption('0','-select-'));        
            integer i=800;
            String timeVal=String.valueof(i);
            SelectableTime.add(new SelectOption(String.valueof(i),getTimeString(decimal.valueof(i)) ));
            while( i< 2300 ){
                i=i+30;
                timeVal=String.valueof(i);
                SelectableTime.add(new SelectOption(timeVal,getTimeString(decimal.valueof(i)) ));
                i=i+70;
                timeVal=String.valueof(i);
                SelectableTime.add(new SelectOption(timeVal,getTimeString(decimal.valueof(i)) ));
            }
            return SelectableTime;
        }
        set;
    }
    
    public Class addressLookup{
        public String street{get;set;}
        public String street2{get;set;}
        public String City{get;set;}
        public String State{get;set;}
        public String postalCode{get;set;}
        public addressLookup(String st,String st1,String ct,String ste,String pscode){
            street = st;
            Street2 = st1;
            city = ct;
            state = ste;
            postalCode = pscode;
        }
        public addressLookup(){}
    }

    public class allTowns {
        public String townName;
        public String subTownName;
        public allTowns(String Town,String SubTown){
            if (SubTown!=null)
                townName = 'Town:'+Town +'Sub:'+SubTown;  
            else
                townName = 'Town : '+Town ;
        }
    }
    
    /**
     *  @Constructor 
     */
    public userScheduleController(){
        
        // defaults 
        isHomeActivation= false;
        
        // initialize view state variables for later use
        tobeDeleted = new List<SchedUserTerr__c>();
        isNewScheduleValid = false;
        
        // settings shown at the top of the page, each order type has a different set of hours
        scheduleDefaults = [select id,Duration__c,name from SchedDefaults__c];
        
        // this execution context's user 
        String loggedinUserName = userinfo.getName();
        user loggedinUser = [select id,profile.name from user where id=:userInfo.getUserid()];
        
        // check if this user is assigned to a super user permission set
        List<PermissionSet> ps = [select id,name,(SELECT AssigneeId FROM Assignments) from permissionset where name='NSC_Advanced_User'];
        set<id> superIds = new set<id>();
        if(ps.size()>0){
            List<PermissionSetAssignment> users=ps[0].assignments;
            for(PermissionSetAssignment a:users){
                superIds.add(a.assigneeId);
            }
        }
        
        // By default use the logged in user
        Userid = Userinfo.getUserId();
        UserName = Userinfo.getFirstName()+' '+UserInfo.getLastName();
        
        try{
            // this URL parameter contains all information sent for schedule setup in JSON format
            String Userdata = ApexPages.currentPage().getParameters().get('usD');
            System.debug('Userdata'+Userdata);
            // No paramters sent to this page
            if( String.isBlank(Userdata)  ){
                throw new UserScheduleControllerException('No parameters received.');
            }
            
            // Parse the information into the schedule data object
            try{
                usd = (UserScheduleData)Json.deserialize(Userdata,UserScheduleData.class);
                if( usd.Town == IN_HOUSE_ACTIVATION ){
                    isHomeActivation = true;
                }
            }
            catch(Exception parseErr){
                throw new UserScheduleControllerException('Error while parsing information sent to this page. Invalid <strong>UserScheduleData</strong> type.');
            }
            
            // All information is parsed correctly from the parameters sent to this page
            if( usd != null ){
                
                // UserID is required, use logged in user instead if not sent
                if( !String.isBlank( usd.userID ) ){
                    Userid = usd.userID;
                }
                
	            currentUser=[select id,firstname,lastname,manager.name,profile.name,Business_Unit__c from User where id=:Userid];
	            Town = usd.Town;
	            Subtown = usd.Subtown;
	            if( isHomeActivation ){
                     BusinessID = '';
                     usd.BusinessID = '';
	            }
	            else if(usd.BusinessID!=null){
	                 BusinessID = usd.BusinessID;
	            }
	            else if(currentUser.Business_Unit__c!= null && currentUser.Business_Unit__c.contains('Resi')){
	                 BusinessID = '1100';
	            }
	            
	            username = currentUser.firstname+' '+currentuser.lastname;
	            
	            if(currentUser.manager.name==loggedinUserName || loggedinUser.profile.name=='System Administrator' || loggedinUser.profile.name=='ADT NA Sales Admin' || superIds.contains(loggedinUser.id)){
	                showActionButtons = true;
	                
	                // Validate this request is valid
	                String dynamicSchedQuery = 'SELECT id, name, BusinessId__c FROM SchedUserTerr__c WHERE User__c = :userId AND TMTownID__c != null ';
	                if( !String.isBlank(Town) ){
	                    dynamicSchedQuery += ' AND TMTownID__c = :Town ';
	                    if( !String.isBlank(Subtown) ){
	                       dynamicSchedQuery += ' AND TMSubTownID__c = :Subtown ';
	                    }
	                    if( !String.isBlank(BusinessID) ){
                           dynamicSchedQuery += ' AND BusinessId__c = :BusinessID ';
	                    }
                        List<sObject> sobjList = Database.query( dynamicSchedQuery );
                        if( sobjList != null && sobjList.size() > 0 ){
                            isNewScheduleValid = false;
                        }
                        else {
                            isNewScheduleValid = true;
                        }
	                }
	            }
            }
            
        }catch(exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() ));    
        }
        
        if(userId != loggedinUser.id && currentUser.manager.name!=loggedinUserName){
            showSave=false;
        }else{
            showSave=true;
        }
        adlookup = new AddressLookup();
        schedTerrList = getUpdatedUserRepSchedule();
    }
    
    /**
     *  Action called from the page to delete an schedule for this user
     *  
     *  @method deleteSchedule
     *
     */
    public pageReference deleteSchedule(){
        for(SchedUserTerr__c st:schedTerrList){
            if(deletedScheduleId==st.id){
                delete st;
                break;
            }
        }
        schedTerrList = getUpdatedUserRepSchedule();
        return null;
    }
    
    public pageReference deleteSchedules(){
        if(tobeDeleted.size()>0){
            delete tobedeleted;
            schedTerrList=getUpdatedUserRepSchedule();
        }
        return null;
    }
    
    public List<SchedUserTerr__c> getUpdatedUserRepSchedule(){
        List<SchedUserTerr__c> schdList = new List<SchedUserTerr__c>();
        for(SchedUserTerr__c schdItem :[SELECT id,name,Monday_Available_Start__c,Monday_Available_End__c,Tuesday_Available_Start__c, Tuesday_Available_End__c, Wednesday_Available_Start__c,Wednesday_Available_End__c,Thursday_Available_Start__c,Thursday_Available_End__c,Friday_Available_Start__c,Friday_Available_End__c,Saturday_Available_Start__c,Saturday_Available_End__c,Sunday_Available_Start__c,Sunday_Available_End__c,TMTownID__c,TMSubTownID__c, Default_Starting_Address__c,Default_Starting_Address__r.StandardizedAddress__c,Order_Types__c,BusinessId__c FROM SchedUserTerr__c WHERE TMTownID__c!=null and User__c=:UserId ]){
            if(  !String.isBlank( schdItem.TMTownID__c ) && schdItem.TMTownID__c.equalsIgnoreCase(IN_HOUSE_ACTIVATION) ){
                isHomeActivation = true;
            }
            schdList.add(schdItem);
        }
        return schdList;
    }
    
    public pageReference saveList(){
        for(SchedUserTerr__c st:schedTerrList){
            if(st.id==recIdtoUpdate){
                List<String> ordertypes=new List<String>();
                if(st.order_types__c!=null)
                    ordertypes=st.Order_Types__c.split(';');
                List<SchedDefaults__c> sdList=[select id,name,Duration__c from SchedDefaults__c where name IN:ordertypes];
                Decimal minTime=0;
                if(sdList.size()>0){
                    List<decimal> durationList=new List<decimal>();
                    for(schedDefaults__c sd:sdList){
                        durationList.add(sd.Duration__c);
                    
                    }
                    if(durationList.size()>0){
                        durationList.sort();
                        minTime=durationList[0];
                    }
                }
                
                minTime=minTime*100;

                /*------------------------------------------------------------------------------check if agent selected end time less than start time--------------------------------------------------------------*/
               
                if(integer.valueof(st.Sunday_Available_End__c)<integer.valueof(st.Sunday_Available_Start__c)){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Sunday start time cannot be greater than end time for '+st.name));    
                    return null;
                }
                if(integer.valueof(st.Monday_Available_End__c)<integer.valueof(st.Monday_Available_Start__c)){
                    
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Monday start time cannot be greater than end time for '+st.name));    
                    return null;
                }
                if(integer.valueof(st.Tuesday_Available_End__c)<integer.valueof(st.Tuesday_Available_Start__c)){
    
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Tuesday Start time cannot be greater than end time for '+st.name));    
                    return null;
                }
                if(integer.valueof(st.Wednesday_Available_End__c)<integer.valueof(st.Wednesday_Available_Start__c)){
    
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Wednesday Start time cannot be greater than end time for '+st.name));    
                    return null;
                }
                if(integer.valueof(st.Thursday_Available_End__c)<integer.valueof(st.Thursday_Available_Start__c)){
    
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Thursday Start time cannot be greater than end time for '+st.name));    
                    return null;
                }
                if(integer.valueof(st.Friday_Available_End__c)<integer.valueof(st.Friday_Available_Start__c)){
    
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Friday Start time cannot be greater than end time for '+st.name));    
                    return null;
                }
                if(integer.valueof(st.Saturday_Available_End__c)<integer.valueof(st.Saturday_Available_Start__c)){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Saturday Start time cannot be greater than end time for '+st.name));    
                    return null;
                }
                /*------------------------------------------------------------------------------end check if agent selected end time less than start time--------------------------------------------------------------*/
                update st;
                return null;
            }
            
        }

        try{
            update schedTerrList;
        }catch(exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, e.getMessage()));
        }
        return null;
    }
    
    public pageReference cancel(){
        schedTerrList = getUpdatedUserRepSchedule();
        showAddressValidation = false;
        return null;
    }
    
    public pageReference addNewTownSchedule(){
        // Validate we have enough information to setup and schedule for this user
        if( usd == null || (String.isBlank(usd.BusinessID) && !isHomeActivation) || String.isBlank(usd.Town) ){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error, 'Not enough information to create a new schedule.'));
            return null;
        }
        
        // Insert a new schedule
        try {
	        SchedUserTerr__c newSchedule = new SchedUserTerr__c();
	        newSchedule.TMTownID__c = usd.Town;
	        newSchedule.TMSubTownId__c = usd.SubTown; 
	        newSchedule.User__c = userid;
            newSchedule.Quota__c = Integer.valueOf(ResaleGlobalVariables__c.getinstance('ScheduleUserQuotaValue').value__c); 
	        newSchedule.BusinessId__c = usd.BusinessID;
	        if( isHomeActivation ){
                newSchedule.Name = UserName +'_DLL';
	        }
	        else if(usd.BusinessID.contains('1100')){
                if(!String.isBlank(usd.SubTown))
                    newSchedule.Name = UserName +' Resi Town:'+ usd.Town+' SubTown:'+usd.SubTown;
                else
                    newSchedule.Name = UserName +' Resi Town:'+ usd.Town;
            }
            else{
                if(!String.isBlank(usd.SubTown))
                    newSchedule.Name = UserName +' SMB Town:'+ usd.Town+' SubTown:'+usd.SubTown;
                else
                    newSchedule.Name = UserName +' SMB Town:'+ usd.Town;    
            }
	        insert newSchedule;
	        
	        // clear the variable we use to store the action sent to this page
	        usd = null;
	        
	        // Redirect the user to this page in view mode
	        PageReference pr = new PageReference('/apex/UserSchedule');
            UserScheduleData schData = new UserScheduleData(userid);
            schData.setSetupAction( UserScheduleData.SetupAction.VIEW );
	        String JSONScheduleSetupData = JSON.serialize(schData);
	        pr.getParameters().put('usD', JSONScheduleSetupData );
	        pr.setRedirect(true);
	        return pr;
        }
        catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
        
        schedTerrList = getUpdatedUserRepSchedule();
        return null;
    }
    
    public pageReference showAddressValidation(){
        selectedScheduledTerr= [SELECT id,Default_Starting_Address__r.StandardizedAddress__c,Default_Starting_Address__r.Street__c,Default_Starting_Address__r.Street2__c,Default_Starting_Address__r.City__c,Default_Starting_Address__r.State__c,Default_Starting_Address__r.PostalCode__c,Default_Starting_Address__c,Order_Types__c FROM SchedUserTerr__c WHERE id=:selectedRecordId];
        if(selectedScheduledTerr.Default_Starting_Address__c!=null){
            adlookup =new addressLookup(selectedScheduledTerr.Default_Starting_Address__r.Street__c,
                                        selectedScheduledTerr.Default_Starting_Address__r.Street2__c,
                                        selectedScheduledTerr.Default_Starting_Address__r.City__c,
                                        selectedScheduledTerr.Default_Starting_Address__r.State__c,
                                        selectedScheduledTerr.Default_Starting_Address__r.PostalCode__c);
        }else{
            adlookup =new addressLookup();
        }
        showAddressValidation=true;
        showSaveAddress=false;
        return null;
    }
    
    public pageReference verifyAddress(){
        
        mdr = MelissaDataAPI.validateAddr(adlookup.street, adlookup.street2, adlookup.city, adlookup.state, adlookup.postalCode);

        Set<String> mErrCodes = new Set<String>();
        for (String code : mdr.ResultCodes){                
            if ( code.startsWith('AE') ){
               
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, code));
                showSaveAddress=false;
                return null;
            }
        }
        //Use any previous validation in case of confirm
        if ( mdr == null ) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, 'Cannot confirm an empty address.'));
            showSaveAddress=false;
            return null;
        }
        adlookup =new addressLookup(mdr.Addr1, mdr.Addr2, mdr.city, mdr.state, mdr.zip);
        mdr.Addr2=mdr.Addr2.trim();
        //system.debug('address before'+mdr.Addr2+'after');
        if(!Utilities.isEmptyOrNull(mdr.Addr2))
            originalAddress=AddressHelper.getConcatenatedAddress(mdr.Addr1, mdr.Addr2, mdr.city, mdr.state, mdr.zip);
        else
            originalAddress=AddressHelper.getConcatenatedAddress(mdr.Addr1,mdr.city, mdr.state, mdr.zip);
        showSaveAddress=true;
        return null;
    }
    
    public pageReference saveValidatedAddress(){
        List<Address__c> addr=[select id from Address__c where originalAddress__c=:originalAddress];
        
        if(addr.size()>0){
            selectedScheduledTerr.Default_Starting_Address__c=addr[0].id;
            
        }else{
            Address__c newAddress=new Address__c();
            newAddress.Street__c = mdr.Addr1;
            newAddress.Street2__c = mdr.Addr2;
            newAddress.StreetName__c = mdr.StreetName;
            newAddress.StreetNumber__c = mdr.StreetNumber;
            newAddress.City__c = mdr.City;
            newAddress.State__c = mdr.State;
            newAddress.County__c = mdr.County;
            //newAddress.CountryCode__c = ld.SiteCountryCode__c;
            newAddress.Latitude__c = decimal.valueOf(mdr.Lat);
            newAddress.Longitude__c = decimal.valueOf(mdr.Lon);
            newAddress.PostalCode__c = mdr.Zip;
            newAddress.PostalCodeAddOn__c = mdr.Plus4;  
            newAddress.Validated__c = true;
            //newAddress.ValidationMsg__c = ld.SiteValidationMsg__c;
            newAddress.OriginalAddress__c = AddressHelper.getConcatenatedAddress(newAddress.Street__c, newAddress.Street2__c, newAddress.City__c, newAddress.State__c, newAddress.PostalCode__c);
            newAddress.StandardizedAddress__c = AddressHelper.getStandardizedAddress(newAddress.Street__c, newAddress.Street2__c, newAddress.City__c, newAddress.State__c, newAddress.PostalCode__c);            
            insert newAddress;
            /*
            Map<String, Address__c> newaddressMap=new Map<String, Address__c>();
            newaddressMap.put(newAddress.OriginalAddress__c,newAddress);
            newaddressMap=AddressHelper.upsertAddresses(newaddressMap);
            */
            selectedScheduledTerr.Default_Starting_Address__c=newAddress.id;
        }
        update selectedScheduledTerr;
        showAddressValidation=false;
        schedTerrList=getUpdatedUserRepSchedule();
        return null;
    }
    
    public Boolean getshowschedTerrList(){
        if(schedTerrList.size()>0)
            return true;
        else 
            return false;
    }
    
    public String getTimeString(Decimal timeVal){
        Integer AvailableStartTimeHr = ((decimal)(timeVal)/100).intValue();
        Integer AvailableStartTimeMin = (((decimal)(timeVal)/100 - AvailableStartTimeHr) * 100).intValue();
        system.debug(AvailableStartTimeHr +'Hr '+AvailableStartTimeMin+' min');
        Time timevalue=Time.newInstance(AvailableStartTimeHr , AvailableStartTimeMin, 0, 0);
        String timeString=datetime.newinstance(date.today(),timevalue).format().replace(Date.today().format(), '').trim();
        return timeString;
    }
    
    
}