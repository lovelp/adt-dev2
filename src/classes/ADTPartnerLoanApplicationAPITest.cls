@isTest (SeeAllData = false)

public class ADTPartnerLoanApplicationAPITest {
static LoanApplication__c lApp;
    static Account acct;
    public static opportunity opp;
    public static Call_Data__c cd;
    // Positive scenario
    static testMethod void testDoPostMethod()  {
        createTestData();
        // Set the request body
        ADTPartnerLoanAppAPISchema.LoanApplicationRequestMessage req = new ADTPartnerLoanAppAPISchema.LoanApplicationRequestMessage();
        ADTPartnerLoanAppAPISchema.LoanApplicationRequest loanAppReq = new ADTPartnerLoanAppAPISchema.LoanApplicationRequest();
        ADTPartnerLoanAppAPISchema.Customer cust = new ADTPartnerLoanAppAPISchema.Customer();
        ADTPartnerLoanAppAPISchema.PaymentInfo payInfo = new ADTPartnerLoanAppAPISchema.PaymentInfo();
        ADTPartnerLoanAppAPISchema.BillingAddress prevAddr = new ADTPartnerLoanAppAPISchema.BillingAddress();
        prevAddr.addrLine1 = '101 Main Street';
        prevAddr.city = 'New York';
        prevAddr.state = 'NY';
        prevAddr.postalCode = '10010';
        cust.previousAddress = prevAddr;
        ADTPartnerLoanAppAPISchema.BillingAddress billAddr = new ADTPartnerLoanAppAPISchema.BillingAddress();
        billAddr.addrLine1 = '101 Main Street';
        billAddr.city = 'New York';
        billAddr.state = 'NY';
        billAddr.postalCode = '10010';
        cust.billingAddress = billAddr;
        payInfo.maskedCardNumber = 'XXXXXXXXXXXX1347';
        payInfo.transactionNumber = '5927465B0F93D5513DFC6D21374A1E5E0C275434';
        payInfo.refNumber = '94746649';
        payInfo.brand = 'visa';
        payInfo.cardExpiry = '2019/10/10';
        cust.paymentInfo = payInfo;
        cust.annualIncome = 200;
        cust.taxIdNumber = '123561111';
        cust.dateOfBirth = '1990-01-01';
        cust.creditCardCVV = '123';
        loanAppReq.customer = cust;
        loanAppReq.opportunityId = opp.Id;
        //loanAppReq.callId = cd.id;
        //loanAppReq.opportunityId = '006c000000Jxn2F';//dummy opp
        loanAppReq.partnerId = 'Partner001';
        loanAppReq.partnerRepName = 'partnerRepName';
        req.loanApplicationRequest = loanAppReq;
        String rBody = JSON.serialize(req);
        system.debug('@@Request Body: '+rBody);        
        RestRequest request = new RestRequest();
        request.requestUri ='https://cs13.salesforce.com/services/apexrest/LoanApplication';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(rBody);
        RestResponse res1 = new RestResponse();
        RestContext.request = request;
        RestContext.response= res1;
        Test.startTest();
        // Set the Mock class for MMB Lookup
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanDecisionResponse());
        ADTPartnerLoanApplicationAPI.doPost();
        String Resp =  '{"errors":[{"status":"401","errorCode":"-2","errorMessage":"Authorization failed"}]}';
        ADTPartnerLoanApplicationAPI.desZootError(Resp);   
        Test.stopTest();
    }
    //Negative scenarios
    static testMethod void testDoPostMethod1()  {
        createTestData();
        Address__c addr1 = new Address__c();
        addr1.Latitude__c = 38.94686000000000;
        addr1.Longitude__c = -77.25470100000000;
        addr1.Street__c = '101 Main Street';
        addr1.City__c = 'New York';
        addr1.State__c = 'NY';
        addr1.PostalCode__c = '10010';
        addr1.County__c = 'US';
        addr1.OriginalAddress__c = '101 Main Street+New York+NY+10010';
        insert addr1;        
        
        // Set the request body
        ADTPartnerLoanAppAPISchema.LoanApplicationRequestMessage req = new ADTPartnerLoanAppAPISchema.LoanApplicationRequestMessage();
        ADTPartnerLoanAppAPISchema.LoanApplicationRequest loanAppReq = new ADTPartnerLoanAppAPISchema.LoanApplicationRequest();
        ADTPartnerLoanAppAPISchema.Customer cust = new ADTPartnerLoanAppAPISchema.Customer();
        ADTPartnerLoanAppAPISchema.PaymentInfo payInfo = new ADTPartnerLoanAppAPISchema.PaymentInfo();
        ADTPartnerLoanAppAPISchema.BillingAddress prevAddr = new ADTPartnerLoanAppAPISchema.BillingAddress();
        /*prevAddr.addrLine1 = '101 Main Street';
        prevAddr.city = 'New York';
        prevAddr.state = 'NY';
        prevAddr.postalCode = '10010';*/
        cust.previousAddress = prevAddr;
        ADTPartnerLoanAppAPISchema.BillingAddress billAddr = new ADTPartnerLoanAppAPISchema.BillingAddress();
        /*billAddr.addrLine1 = '101 Main Street';
        billAddr.city = 'New York';
        billAddr.state = 'NY';
        billAddr.postalCode = '10010';*/
        cust.billingAddress = billAddr;
        //payInfo.maskedCardNumber = 'XXXXXXXXXXXX1347';
        payInfo.transactionNumber = '5927465B0F93D5513DFC6D21374A1E5E0C275434';
        //payInfo.refNumber = '94746649';
        cust.paymentInfo = payInfo;
        //cust.annualIncome = 200;
        //cust.taxIdNumber = '123561111';
        //cust.dateOfBirth = '1990-01-01';
        //cust.creditCardCVV = '123';
        loanAppReq.customer = cust;
        loanAppReq.opportunityId = opp.Id;
        //loanAppReq.callId = cd.id;
        //loanAppReq.opportunityId = '006c000000Jxn2F';//dummy opp
        loanAppReq.partnerId = 'Partner001';
        loanAppReq.partnerRepName = 'partnerRepName';
        req.loanApplicationRequest = loanAppReq;
        String rBody = JSON.serialize(req);
        system.debug('@@Request Body: '+rBody);        
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://cs13.salesforce.com/services/apexrest/LoanApplication';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(rBody);
        RestResponse res1 = new RestResponse();
        RestContext.request = request;
        RestContext.response= res1;
        Test.startTest();
        // Set the Mock class for MMB Lookup
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanDecisionResponse());
        //request.requestBody = Blob.valueof('{"Blank" : "Blank"}');
        ADTPartnerLoanApplicationAPI.doPost();
        
        request.requestBody = Blob.valueof(rBody);
        ADTPartnerLoanApplicationAPI.doPost();
        
        cust.billingAddress = null;
        prevAddr.addrLine1 = '101 Main Street';
        prevAddr.city = 'New York';
        prevAddr.state = 'NY';
        prevAddr.postalCode = '10010';
        cust.previousAddress = prevAddr;
        
        payInfo.maskedCardNumber = 'XXXXXXXXXXXX1347';
        payInfo.transactionNumber = '5927465B0F93D5513DFC6D21374A1E5E0C275434';
        payInfo.refNumber = '94746649';
        cust.paymentInfo = payInfo;
        cust.annualIncome = 200;
        cust.taxIdNumber = '123561111';
        cust.dateOfBirth = '1990-01-01';
        cust.creditCardCVV = '123';
        rBody = JSON.serialize(req);
        request.requestBody = Blob.valueof(rBody);
        ADTPartnerLoanApplicationAPI.doPost();
        Test.stopTest();
    }
    public static void createTestData(){
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootLoanDecisionEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c = 120;
        is.ZootAPIRetryCounter__c = 2;
        //is.EncryptedDPKey__c = 'y7HFCa6RxyPGlrkACgiFU7m7k0RSXldDE/DRCZ0lAVAvs5GPwPisYCwApnCNw9VuNGrQ65bmXymijPMIQI6GNw==';
        //is.KeytoDecrypt__c = 'dT6y2Va49Xwnc9B+ALdEuqu6vEZ/6hokleIuOodHdJw=';
        insert is;
        list<FlexFiConfig__c> flexConfigs = new list<FlexFiConfig__c>();
        flexConfigs.add(new FlexFiConfig__c(value__c='test', name='LoanType'));
        flexConfigs.add(new FlexFiConfig__c(value__c='test', name='TransactionType'));
        flexConfigs.add(new FlexFiConfig__c(value__c='test', name='autoBookFlg'));
        flexConfigs.add(new FlexFiConfig__c(Name = 'Phone Sales Department', value__c = 'test'));
        flexConfigs.add(new FlexFiConfig__c(Name = 'OFACCode', value__c = 'test'));
        flexConfigs.add(new FlexFiConfig__c(Name = 'loanApplicationExpirationDays', value__c = '60'));
        insert flexConfigs;
        
        list<FlexFiMessaging__c> flexMsgs = new list<FlexFiMessaging__c>();
        FlexFiMessaging__c msg1 = new FlexFiMessaging__c(Name = 'FlexFiRiskGradeNotValid');
        msg1.ErrorMessage__c = 'Credit is Run but Risk Grade is Not Approved for Loan Application. Please Proceed to ADT Financing.';
        flexMsgs.add(msg1);
        
        FlexFiMessaging__c msg2 = new FlexFiMessaging__c(Name = 'DOBError');
        msg2.ErrorMessage__c = 'Account Does Not Have a Date Of Birth. Provide Date Of Birth Or Proceed to ADT Financing.';
        flexMsgs.add(msg2);
        
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'NoEmailError');
        msg3.ErrorMessage__c = 'Account Does Not Have an Email. Provide an Email Or Proceed to ADT Financing.';
        flexMsgs.add(msg3);
        
        FlexFiMessaging__c msg4 = new FlexFiMessaging__c(Name = 'TnCAcceptedByMessage');
        msg4.ErrorMessage__c = 'Terms & Condition Accepted by';
        flexMsgs.add(msg4);
        
        flexMsgs.add(new FlexFiMessaging__c(Name = 'LoanSubmissionFailed', ErrorMessage__c = 'Test'));
        flexMsgs.add(new FlexFiMessaging__c(Name = 'DecisionApplicationSubmitted', ErrorMessage__c = 'Test'));
        
        insert flexMsgs;
        
        map<String, String> EquifaxSettings = new map<String, String>
                        {
                          'Order Types For Credit Check'=>'N1;R1'
                        , 'Order Types For Credit Check - Renters'=>'N1;R1'
                        , 'Failure Risk Grade'=>'X'
                        , 'Failure Condition Code'=>'APPROV'
                        //, 'Default Risk Grade'=>'W'
                        //, 'Default Condition Code'=>'CAE1'
                        , 'Employee Risk Grade'=>'U'
                        , 'Employee Condition Code'=>'CME3'     
                        //, 'Days to be considered out of date'=>'90'
                        , 'Days to allow additional check'=>'1'
                        ,'FlexFiApprovalRiskGrade' => 'A;B;C;X'};
        List<Equifax__c> EquifaxSettingList = new List<Equifax__c>(); 
        for( String sN: EquifaxSettings.keySet()){
            Equifax__c eS = new Equifax__c( name = sN );
            eS.value__c = EquifaxSettings.get(sN);
            EquifaxSettingList.add( eS );
        }
        insert EquifaxSettingList;
        //Create Account
        //User u; 
        //User current = [Select Id from User where Id = :UserInfo.getUserId()];
        list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
        rgvs.add(new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(name='IntegrationUser', value__c='Integration User'));
        rgvs.add(new ResaleGlobalVariables__c(name='RIFAccountOwnerAlias', value__c='ADT RIF'));
        rgvs.add(new ResaleGlobalVariables__c(name='DataConversion', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(name='AccountUpdateAPI_AllowedOrderTypes', value__c='N1;N4;R1;R3;A1'));
        //rgvs.add(new ResaleGlobalVariables__c(name='NewRehashHOARecordOwner', value__c= u.username));
        rgvs.add(new ResaleGlobalVariables__c(Name='GlobalResaleAdmin', value__c='Testing'));
        rgvs.add(new ResaleGlobalVariables__c(Name='Alternate Pricing Model Order Types', value__c='N1;N2;N3;N4;R1;R2;R3;R4;A1;A2;A3;A4'));
        rgvs.add(new ResaleGlobalVariables__c(Name='P1RententionPhNumber', value__c='1800-000-0000'));
        rgvs.add(new ResaleGlobalVariables__c(Name='P1HOAPhNumber', value__c='1800-000-0000'));
        rgvs.add(new ResaleGlobalVariables__c(Name='HOATeamPhone', value__c='1800-000-0000'));
        rgvs.add(new ResaleGlobalVariables__c(Name='MelissaDataVersion', value__c='V2'));
        rgvs.add(new ResaleGlobalVariables__c(Name='MelissaDataCustID', value__c='98900100'));
        insert rgvs;
        
        List<Postal_Codes__c> pcs = new List<Postal_Codes__c>();
        Postal_Codes__c pc = new Postal_Codes__c (name='221o2', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100', APM_PostalCode__c = true);
        pcs.add(pc);
        /*Postal_Codes__c pc1 = new Postal_Codes__c (name='221o3', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100');
        pcs.add(pc1);*/
        insert pcs;
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        /*Address__c addr1 = new Address__c();
        addr1.Latitude__c = 38.94686000000000;
        addr1.Longitude__c = -77.25470100000000;
        addr1.Street__c = '101 Main Street';
        addr1.City__c = 'New York';
        addr1.State__c = 'NY';
        addr1.PostalCode__c = '10010';
        addr1.County__c = 'US';
        addr1.OriginalAddress__c = '101 Main Street+New York+NY+10010';
        insert addr1;*/
        
        acct = new Account(Name = 'Unit Test Account 2');
        acct.AddressID__c = addr.Id;
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '221o2';
        acct.Email__c = 'abc@gmail.com';
        acct.DOB_encrypted__c = '12/12/1900';
        acct.Equifax_Last_Check_DateTime__c = system.now();
        acct.EquifaxRiskGrade__c = 'A';
        acct.EquifaxApprovalType__c = 'APPROV';
        acct.MMBOrderType__c = 'R1';
        acct.PostalCodeId__c = pc.Id;
        insert acct;
        
        
        //Opportunity opp;
        opp = New Opportunity();
        opp.AccountId = acct.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp;
        
        // create call data record
        cd = new Call_Data__c (name = 'TestCallData', Account__c = acct.id, Contact_Type__c = 'Inbound Call');
        insert cd;
        
        lApp = new LoanApplication__c();
        lApp.Account__c = acct.id;
        lApp.BillingAddress__c =addr.id;
        lApp.ShippingAddress__c =addr.id;
        lApp.SubmitLoanTerms_ConditionAcceptedDate__c =DateTime.parse('12/05/2018 11:46 AM');
        lApp.PreviousAddressLine1__c ='Prev Add Line 1';
        lApp.PreviousAddressLine2__c ='Prev Add Line 2';
        lApp.PreviousAddressPostalCd__c ='27518';
        lApp.PreviousAddressStateCd__c ='NC';
        lApp.PreviousAddressCountryCd__c='USA';
       
        insert lApp;
    }
}