/**************************** MODIFICATION LOG **************************************************************
 *
 * DESCRIPTION : SciQuoteTriggerHelper.cls has methods that govern behaviour of inserted and updated SciQuote records
 *
 *-----------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE               TICKET               REASON
 *-----------------------------------------------------------------------------------------------------------
 * Mike Tuckman                  11/20/2013                              - Original Version
 * Abhinav Pandey                10/28/1028         HRM-8009             - Credit ByPass Process
 * Srini Yarramsetti             01/03/2018         HRM-8447             - Soft to Hard credit
 * Siddarth Asokan               03/29/2019         ECOM-967             - EBR & One click call out for ECOM orders
 * Siju Varghese                 06/10/2019         HRM-10173            - Ability to Negotiate Price
 * Siddarth Asokan               09/16/2019         HRM-9983             - Creating Request Queue for DOA & Credit Bypass instead of @future
 */
 
public without sharing class SciQuoteTriggerHelper {
    //-------------------------------------------------------------------------------------------------------
    //ProcessSciQuoteBeforeInsert()
    //-------------------------------------------------------------------------------------------------------
    public static void ProcessSciQuoteBeforeInsert(List<scpq__SciQuote__c> NewSciQuotes){
        for(scpq__SciQuote__c NewSciQuote : NewSciQuotes){
            if (NewSciQuote.Account__c == null) {
                Opportunity opp = [select AccountId from Opportunity where id = :NewSciQuote.scpq__OpportunityId__c];
                if (opp != null){
                    NewSciQuote.Account__c = opp.AccountId;
                    Account act = [select ID, MMBCustomerNumber__c, MMBOrderType__c, MMBSiteNumber__c, Rep_User__c from Account where ID = :opp.AccountId];
                    if (act != null){
                        NewSciQuote.MMBOutCustomerNumber__c = act.MMBCustomerNumber__c;
                        NewSciQuote.MMBOutOrderType__c = act.MMBOrderType__c;
                        NewSciQuote.MMBOutSiteNumber__c = act.MMBSiteNumber__c;
                        NewSciQuote.Rep_User__c = act.Rep_User__c;
                    }
                }
            }
            
            // New jobs attributes
            if(NewSciQuote.Job_Details__c != null && NewSciQuote.Job_Details__c.length() > 0){
                SciQuoteTriggerUtilities.doQuoteJobsAttributes(NewSciQuote);
            }
        }
    } 
     
    //-------------------------------------------------------------------------------------------------------
    //ProcessSciQuotesAfterInsert()
    //-------------------------------------------------------------------------------------------------------
    public static void ProcessSciQuotesAfterInsert(List<scpq__SciQuote__c> NewSciQuotes){               
        // Status values from CPQ - Created, Paid, Signed, Submitted, Scheduled
        list<Account> updAcct = new list<Account>();
        list<disposition__c> dispAdd = new list<disposition__c>();
        list<Quote_Activity__c> qaAdds = new list<Quote_Activity__c>();
        list<SciQuoteTriggerUtilities.QuoteJob> qJobList = new list<SciQuoteTriggerUtilities.QuoteJob>(); 
        list<scpq__SciQuote__c> doaQuotes = new list<scpq__SciQuote__c>();
        
        for(scpq__SciQuote__c NewSciQuote : NewSciQuotes){
            Account a = [select OwnerId, TelemarAccountNumber__c, TransitionDate3__c, Sold_By_System__c, AccountStatus__c, MMBCustomerNumber__c, MMBSiteNumber__c, TelemarOrderType__c, Business_Id__c from Account where Id = :NewSciQuote.Account__c];
            User u = [select EmployeeNumber__c, Name, ManagerID, Manager.DelegatedEmail__c, Business_Unit__c, Profile.Name, DelegatedEmail__c from User where ID = :a.OwnerId];
            
            SciQuoteTriggerUtilities.dispoQuote(NewSciQuote, null, updAcct, dispAdd);
            
            if (NewSciQuote.Activity_Data__c != null && NewSciQuote.Activity_Data__c.length() > 0){
                SciQuoteTriggerUtilities.doActivity(NewSciQuote, qaAdds, u, a);
            }
            // Quote has Jobs. Add them for the first time after having this record's id to build relationship
            if (NewSciQuote.Job_Details__c != null && NewSciQuote.Job_Details__c.length() > 0){
                SciQuoteTriggerUtilities.doQuoteJob(NewSciQuote, qJobList, u);
            }
            //HRM 11480 Check if any quotes have DOALockID and DOAApprovalLevel  
            if (String.isNotBlank(NewSciQuote.DOALockID__c) && String.isNotBlank(NewSciQuote.DOAApprovalLevel__c)){
                doaQuotes.add(NewSciQuote);
            }
        }
        
        if(updAcct.size() > 0){
            update updAcct;
        }
        if(dispAdd.size() > 0){
            insert dispAdd;
        }
        if(qaAdds.size() > 0){
            insert qaAdds;
        }
        if(!qJobList.isEmpty()){
            // Save Jobs
            SciQuoteTriggerUtilities.saveQuoteJob(qJobList);
        }
        if(doaQuotes.size() > 0){
            // Create Request Queue for DOA
            DOARequestUtils.createRequestQueueForDOA(doaQuotes, false);
        }
    }
      
    //-------------------------------------------------------------------------------------------------------
    //ProcessSciQuotesBeforeUpdate()
    //-------------------------------------------------------------------------------------------------------
    public static void ProcessSciQuotesBeforeUpdate(Map<Id, scpq__SciQuote__c> NewSciQuotes, Map<Id, scpq__SciQuote__c> OldSciQuotes){ 
        for(scpq__SciQuote__c NewSciQuote : NewSciQuotes.values()){  
            scpq__SciQuote__c OldSciQuote = OldSciQuotes.get(NewSciQuote.Id);
            if(NewSciQuote.Rep_User__c != null){
                Opportunity opp = [select AccountId,Account.Rep_User__c, OwnerId from Opportunity where id = :NewSciQuote.scpq__OpportunityId__c];
                if(opp != null && NewSciQuote.Rep_User__c!= opp.Account.Rep_User__c){
                    NewSciQuote.Rep_User__c = opp.Account.Rep_User__c;
                }
            }
            
            if (NewSciQuote.Account__c == null) {
                Opportunity opp = [select AccountId, OwnerId from Opportunity where id = :NewSciQuote.scpq__OpportunityId__c];
                if (opp != null){
                    NewSciQuote.Account__c = opp.AccountId;
                }
            }

            // New jobs attributes
            if(NewSciQuote.Job_Details__c != null && NewSciQuote.Job_Details__c.length() > 0 && NewSciQuote.Job_Details__c != OldSciQuote.Job_Details__c){
                SciQuoteTriggerUtilities.doQuoteJobsAttributes(NewSciQuote);
            }
            
            // Added by TCS -- HRM 423
            if(OldSciQuote != null && (NewSciQuote.scpq__Status__c != OldSciQuote.scpq__Status__c && NewSciQuote.scpq__Status__c =='Signed')){
                NewSciQuote.Signed_Date_Time__c= System.now();
            }
            //TCS changes ends
            
            // Clearing out the DOA Approval Status if the DOA lock id has changed
            if((NewSciQuote.DOALock__c != OldSciQuote.DOALock__c) || (NewSciQuote.DOALockID__c != OldSciQuote.DOALockID__c) || (NewSciQuote.DOALockID__c != null && OldSciQuote.DOALockID__c == null)){              
                NewSciQuote.DOAApprovalStatus__c  = '';
            }
            
            if(NewSciQuote.scpq__Status__c == 'Paid'){
                NewSciQuote.QuotePaidOn__c = System.now();
            }
        }
    }
    
    //-------------------------------------------------------------------------------------------------------
    //ProcessSciQuotesAfterUpdate()
    //-------------------------------------------------------------------------------------------------------
    public static void ProcessSciQuotesAfterUpdate(Map<Id, scpq__SciQuote__c> NewSciQuotes, Map<Id, scpq__SciQuote__c> OldSciQuotes){               
        // Status values from CPQ - Created, Paid, Signed, Submitted, Scheduled
        //HRM-10173 - Siju Added
        SciQuotePriceChangeHandler.ProcessSciQuoteAfterUpdate(NewSciQuotes,OldSciQuotes);
        
        list<Account> updAcct = new list<Account>();
        list<disposition__c> dispAdd = new list<disposition__c>();
        list<Quote_Activity__c> qaAdds = new list<Quote_Activity__c>();
        list<SciQuoteTriggerUtilities.QuoteJob> qJobList = new list<SciQuoteTriggerUtilities.QuoteJob>(); 
        list<scpq__SciQuote__c> doaQuotes = new list<scpq__SciQuote__c>();
        list<scpq__SciQuote__c> creditByPassDOAList = new list<scpq__SciQuote__c>();        
        map<Id,Id> accQuoteMap = new map<Id,Id>();
        list<Id> creditCheckAccountIdList = new list<Id>(); //HRM-8447 Soft to Hard credit
        map<Id,Id> ecomQuoteIdAcctIdMap = new map<Id,Id>(); //ECOM-967 EBR call for ECOM
    
        for(scpq__SciQuote__c NewSciQuote : NewSciQuotes.values()){
            scpq__SciQuote__c OldSciQuote = OldSciQuotes.get(NewSciQuote.Id);
            Account a = [select OwnerId, TelemarAccountNumber__c, TransitionDate3__c, Sold_By_System__c, AccountStatus__c, MMBCustomerNumber__c, MMBSiteNumber__c, TelemarOrderType__c, Business_Id__c 
                         from Account where Id = :NewSciQuote.Account__c];
            User u = [select EmployeeNumber__c, Name, ManagerID, Manager.DelegatedEmail__c, Business_Unit__c, Profile.Name, DelegatedEmail__c from User where ID = :a.OwnerId];
            
            if (OldSciQuote == null || NewSciQuote.scpq__Status__c != OldSciQuote.scpq__Status__c){
                SciQuoteTriggerUtilities.dispoQuote(NewSciQuote, OldSciQuote, updAcct, dispAdd);
            }
            
            if ((NewSciQuote.scpq__Status__c != OldSciQuote.scpq__Status__c) || ((NewSciQuote.Activity_Type__c != null && OldSciQuote.Activity_Type__c == null) || (NewSciQuote.Activity_Type__c != OldSciQuote.Activity_Type__c))){
                SciQuoteTriggerUtilities.doActivity(NewSciQuote, qaAdds, u, a);
            }
            
            // New jobs/lines added
            if(NewSciQuote.Job_Details__c != null && NewSciQuote.Job_Details__c.length() > 0 && NewSciQuote.Job_Details__c != OldSciQuote.Job_Details__c){
                SciQuoteTriggerUtilities.doQuoteJob(NewSciQuote, qJobList, u);
            }
            //HRM - 8009
            if(NewSciQuote.CreditBypassRequested__c && !OldSciQuote.CreditBypassRequested__c){    
                creditByPassDOAList.add(NewSciQuote);      
            }
            //HRM 11480 Check if any quotes after update have DOALockID changed - could be added, changed to new lock ID, or removed by CPQ
            if(String.isNotBlank(NewSciQuote.DOAApprovalLevel__c) && ((NewSciQuote.DOALock__c != OldSciQuote.DOALock__c) || (NewSciQuote.DOALockID__c != OldSciQuote.DOALockID__c) || 
                (NewSciQuote.DOALockID__c != null && OldSciQuote.DOALockID__c == null))){
                doaQuotes.add(NewSciQuote);
            }
            //HRM-8447 Soft to Hard credit
            if(label.CreditCheckConversionFlag == 'true' && NewSciQuote.scpq__Status__c == 'Paid' && NewSciQuote.scpq__Status__c != OldSciQuote.scpq__Status__c){
                creditCheckAccountIdList.add(NewSciQuote.Account__c);
            }
            //ECOM-967 Check and call EBR
            if(NewSciQuote.Source__c == 'ECOM' && NewSciQuote.scpq__Status__c == 'Paid' && NewSciQuote.scpq__Status__c != OldSciQuote.scpq__Status__c){
                ecomQuoteIdAcctIdMap.put(NewSciQuote.Id,NewSciQuote.Account__c);
            }
        }
        
        if(updAcct.size() > 0){
            update updAcct;
        }
        if(dispAdd.size() > 0){
            insert dispAdd;
        }
        if(qaAdds.size() > 0){
            insert qaAdds;
        }
        if(!qJobList.isEmpty()){
            // Save Jobs
            SciQuoteTriggerUtilities.insertQuoteJobOnUpdate(qJobList, NewSciQuotes);
        }     
        if(creditByPassDOAList.size() > 0){
            // Create Request Queue for Credit Bypass
            DOARequestUtils.createRequestQueueForDOA(creditByPassDOAList,true);
        } 
        if(!doaQuotes.isEmpty()){
            // Create Request Queue for DOA
            DOARequestUtils.createRequestQueueForDOA(doaQuotes,false);
        }
        if(creditCheckAccountIdList.size() > 0){
            // HRM-8447 Check to see if hard credit needs to be done and do it in @Future method
            SciQuoteTriggerUtilities.checkAndPerformHardCreditCheck(creditCheckAccountIdList);
        }
        if(ecomQuoteIdAcctIdMap.size() > 0){
            // ECOM-967 Check quote Job & Do callout to EBR & then to One click
            SciQuoteTriggerUtilities.checkAndCallEBROneClick(qJobList, ecomQuoteIdAcctIdMap);
        }
    }
}