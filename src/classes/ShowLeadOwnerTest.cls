/*
 * Description: This is a test class for the following classes
 * 1.ShowLeadOwner
 * Created By: Ravi Pochamalla
 *
 */

@isTest
private class ShowLeadOwnerTest{

    static testMethod void testShowLeadOwner(){   
        User u;
        Lead l;
        List <Lead> leadList = new List<Lead>();

        system.runas(TestHelperClass.createAdminUser()) {
        	TestHelperClass.createReferenceDataForTestClasses();        
        	TestHelperClass.createReferenceUserDataForTestClasses();
        	ErrorMessages__c err = new ErrorMessages__c();
        	err.Name ='Bad_Address';
        	err.Message__c='There was an error finding address in salesforce. Insert failed.';
        	insert err;
        }
        
        User current = [select Id from User where Id = :UserInfo.getUserId()];
            system.runas(current) {
                ResaleGlobalVariables__c rgb = new ResaleGlobalVariables__c();
                rgb = [ select id, name , value__c from ResaleGlobalVariables__c where name = 'DaysToRetainResidentialLeads' ];
                rgb.value__c = '0';
                update rgb;
            
                u = TestHelperClass.createSalesRepUser();
                l = TestHelperClass.createLead(u, false, 'BUDCO' );
				l.NewMoverType__c = 'RL';
                l.LastName = 'testbudco1';
                leadList.add(l);
                insert leadList;
                l = TestHelperClass.createLead(u, false, 'BUDCO' );
                l.LastName = 'testbudco2';
				l.NewMoverType__c = 'RL';
                leadList.add(l);
                insert l;
        
                
            String dataConversion = ResaleGlobalVariables__c.getinstance('IntegrationUser').value__c; 
            ApexPages.StandardController sc = new ApexPages.StandardController(l);
            test.setCurrentPageReference(Page.ShowLeadOwner);
            test.startTest();
            ShowLeadOwner slo = new ShowLeadOwner(sc);
            test.stopTest();
            }
    }
}