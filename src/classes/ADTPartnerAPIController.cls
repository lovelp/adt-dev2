/************************************* MODIFICATION LOG ************************************************************
* ADTPartnerAPIController
*
* DESCRIPTION : Defines logic to handle the method calls from the integration service. 
*-------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          TICKET        REASON
*-------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan      09/01/2017                  Original Version
* Siddarth Asokan      09/09/2017                  getCreditRating 
* Abhinav Pandey       09/09/2017                  getservicability
* Ravi Pochamalla      09/27/2017                  getOpportunityId modified with lead conversion
* Mounika Anna         02/22/2018    HRM-6599      Change the ordertype value from R3 to R2
* Harsha vardhan       24/06/2019    HRM-9905      Serviceability - Added Rep Department field
* Srinivas Yarramsetti 07/31/2019    optyRecordTypeId  Added utility class for opportunity record type
*/
public with sharing class ADTPartnerAPIController {
    public String oppId {get;set;}
    public Account acc {get;set;}
    public static String partnerId;

    //Method to return the Opportunity Id 
    public String getOpportunityId(ADTPartnerCustomerLookupSchema.CustomerLookupRequest req){
        try{
            // Set the static partnerId so that it can be used in getservicability
            partnerId = req.partnerID;
            if(String.isBlank(req.callID) ?  false : true){
                Call_Data__c callData = [Select id,DNIS__r.name, Account__c, Lead__c, AccountLastActivityDate__c, lead__r.PostalCodeID__c, Lead__r.IsConverted,Lead__r.ConvertedAccountId,lead__r.addressId__c, lead__r.addressId__r.PostalCode__c, lead__r.AddressId__r.CountryCode__c FROM Call_Data__c WHERE id =: req.callID limit 1];
                // If OppId is blank or null
                if((String.isBlank(req.opportunityID) || req.opportunityID == null ) ?  true : false ){ 
                    system.debug('test oppID...'+req.opportunityID);            
                    if(String.isBlank(callData.Account__c)){
                        if(callData.Lead__r.IsConverted){
                            Account acc = [Select Id,LastActivityDate__c,MMBLookup__c,LastMMBLookupRun__c,name,Business_Id__c,(select Id,name from opportunities) from account where Id =: callData.lead__r.ConvertedAccountId limit 1];
                            acc.MMBLookup__c = true;
                            acc.LastMMBLookupRun__c =  System.now();
                            update acc;
                            if(acc.opportunities.size()>0){
                                for(Opportunity opp : acc.opportunities){
                                    AccountInfo.accountid = acc.id;
                                    callData.Lead__c = null;
                                    callData.Account__c = acc.id;
                                    callData.AccountLastActivityDate__c = acc.LastActivityDate__c;
                                    update callData;
                                    return opp.Id;
                                    break;
                                }
                            }else{
                                // create opportunity
                                opportunity o = new opportunity();
                                o.Name = acc.Name;
                                o.closeDate = date.today();
                                o.StageName = 'Prospecting';
                                o.AccountId = acc.id;
                                o.RecordTypeId = Utilities.optyRecordTypeId(acc);
                                insert o; 
                                //update calldata after opportunity creation
                                callData.Lead__c = null;
                                callData.Account__c = acc.id;
                                callData.AccountLastActivityDate__c = acc.LastActivityDate__c;
                                update callData;
                                AccountInfo.accountid = callData.Account__c;
                                return o.id;
                            }
                        }else{
                            system.debug('into loop of lead conversion');
                            Map<String, String> OppId = convertLeadtoAcc(callData.Lead__c);
                            Account acct = new Account();
                            acct = [Select Id, LastActivityDate__c, DNIS__c from Account Where Id =:OppId.get('AccId') limit 1];
                            acct.DNIS__c = String.ValueOf(callData.DNIS__r.name);
                            update acct;
                            callData.Lead__c = null;
                            callData.Account__c = OppId.get('AccId');
                            callData.AccountLastActivityDate__c = acct.LastActivityDate__c;
                            update callData;
                            return OppId.get('OppId');
                        }
                    }else{
                        // Account present in call data
                        Account acc = [Select Id,name,LastActivityDate__c,Business_Id__c,(select Id,name from opportunities) from account where Id =:callData.Account__c limit 1];
                        acc.MMBLookup__c = true;
                        acc.LastMMBLookupRun__c = System.now();
                        update acc;
                        callData.Lead__c = null;
                        callData.AccountLastActivityDate__c = acc.LastActivityDate__c;
                        update callData;
                        if(acc.opportunities.size()>0){
                            for(Opportunity opp : acc.opportunities)
                            {
                               AccountInfo.accountid = callData.Account__c;
                               return opp.Id;
                               break;
                            }
                        }else{
                            opportunity o = new opportunity();
                            o.Name = acc.Name;
                            o.closeDate = date.today();
                            o.StageName = 'Prospecting';
                            o.AccountId = acc.id;
                            o.RecordTypeId = Utilities.optyRecordTypeId(acc);
                            insert o;                            
                            AccountInfo.accountid = callData.Account__c;                           
                            return o.id;   // return the opportunity ID
                        }
                        system.debug('test oppID. inside else....'+acc);
                    }
                }else{
                    // Opportunity Id is present in request
                    Opportunity opp = [SELECT id, AccountId, Account.LastActivityDate__c, Account.SiteCountryCode__c, Account.PostalCodeID__c, Account.AddressID__c, Account.addressId__r.PostalCode__c from Opportunity Where id =: req.opportunityID limit 1]; 
                    // HRM-6640 Weblead Accounts
                    try{
                        // If address Id is blank populate it from the callData's lead
                        if(opp.Account.AddressID__c == null && callData.lead__r.AddressId__c != null){
                            Account accAddr = new Account();
                            accAddr.Id = opp.AccountId;
                            accAddr.AddressID__c = callData.lead__r.AddressId__c;
                            // Populating the postal code ID by populating the shipping postal code and country
                            accAddr.ShippingPostalCode = callData.lead__r.AddressId__r.PostalCode__c;
                            accAddr.ShippingCountry = callData.lead__r.AddressId__r.CountryCode__c;
                            update accAddr;
                        }
                        // If postal Code Id on account is blank & if Address has a postal code we populate the postal code Id on account
                        else if(opp.Account.PostalCodeID__c == null && opp.Account.AddressID__c != null && String.isNotBlank(opp.Account.addressId__r.PostalCode__c) && String.isNotBlank(opp.Account.SiteCountryCode__c)){
                            // Populating the shipping postalcode & shipping country popualtes the postal code ID on account
                            Account accWithPostal = new Account();
                            accWithPostal.Id = opp.AccountId;
                            accWithPostal.ShippingPostalCode = opp.Account.addressId__r.PostalCode__c;
                            accWithPostal.ShippingCountry = opp.Account.SiteCountryCode__c;
                            update accWithPostal;
                        }
                    }catch(Exception e){
                        system.debug('error....: '+e.getStackTraceString());
                    }
                    // HRM-6640 Weblead Accounts  
                    callData.Account__c = opp.AccountId;
                    callData.AccountLastActivityDate__c = opp.Account.LastActivityDate__c;
                    // Delete lead once converted to account
                    if(!callData.Lead__r.IsConverted){
                        list<id> leadids = new list<id>{callData.lead__c};
                        deleteLead(leadids);
                    }
                    callData.Lead__c = null;
                    AccountInfo.accountid  = opp.AccountId;
                    update callData;
                    return req.opportunityID;
                }
            }
        }catch(Exception e){}
        
        return String.isNotBlank(this.oppId)? this.oppId : '';  
    }
    
    public static void deleteLead(List<id> leadIdsToDelete){
        try{
            list<Lead> leadList = [select id from lead where Id IN:leadIdsToDelete];
            delete leadList;           
        }catch(Exception e){}
    }
    
    // Lead conversion process
    public static map<String,String> convertLeadtoAcc(id leadId){   
        // Create Account Request (Dummy) to reuse existing logic
        Call_Data__c callData;
        String dnis;
        String promotion;
        try{
            callData = [Select id,DNIS__r.name,DNIS__r.promotions__r.PromotionCode__c from call_data__c where Lead__c=:leadId limit 1];
            dnis = (String.isBlank(String.ValueOf(callData.DNIS__r.name)) ? '' : String.ValueOf(callData.DNIS__r.name));
            promotion = (String.isBlank(String.ValueOf(callData.DNIS__r.promotions__r.PromotionCode__c)) ? '' : String.ValueOf(callData.DNIS__r.promotions__r.PromotionCode__c));
            system.debug('test dnis calldata...'+String.ValueOf(callData.DNIS__r.name));
        }catch(Exception ex){}
        TelemarGateway.AccountRequest ar = new TelemarGateway.AccountRequest();
        ar.Dnis = (String.isBlank(dnis) ? '' : dnis); 
        ar.Promotion = (String.isBlank(promotion) ? '' : promotion);
        ar.Submitter = UserInfo.getUserId();
        
        LeadInfo.leadId = leadId;
        Lead l = LeadInfo.getLeadDetails();
        String leadFirstName = l.FirstName;
        String leadLastName = l.LastName;
        String leadownerid = l.ownerid;
        if(String.isBlank(l.Business_Id__c)){
            l.Business_Id__c = '1100 - Residential';
            update l;
        }
        // Sync with Telemar with creation of telemar number
        TelemarGateway.ScheduleResult res = TelemarGateway.createAccount(l, ar);  
        system.debug('response telemar ...'+res);
        
        if (String.isNotBlank(res.AccountNumber) ) {
            l.TelemarAccountNumber__c = (res.AccountNumber.length() > 10 ? res.AccountNumber.substring(0,10) : res.AccountNumber);
            update l;
        }
        
        // Lead Conversion
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(leadId);        
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        Account acc = new Account(Id = lcr.getAccountId(),Email__c = l.email, ProcessingType__c = 'User',rep_user__c=leadownerid, FirstName__c = leadFirstName, LastName__c = leadLastName, EquifaxApprovalType__c = Equifax__c.getinstance('Default Condition Code').value__c, EquifaxRiskGrade__c = Equifax__c.getinstance('Default Risk Grade').value__c, MMBLookup__c = true , LastMMBLookupRun__c =  System.now(), DNIS_Modified_Date__c = System.now(), DNIS_Modifier_Name__c = UserInfo.getUserId());
        update acc;       
        AccountInfo.accountid  = lcr.getAccountId();
        Map<String, String> IdsMap = new Map<String, String>{'OppId'=> lcr.getOpportunityId(), 'AccId'=>lcr.getAccountId()};
        return IdsMap;
    }
 
    // Method to return the credit check details
    public ADTPartnerCustomerLookupSchema.CreditRating getCreditRating(){
        this.acc = new Account();
        this.acc = Accountinfo.getAccount();
        system.debug('Account inside get credit rating: '+ acc);
        
        // Updated changes for Credit Rating Start
        ADTPartnerCustomerLookupSchema.CreditRating creditInfo = new ADTPartnerCustomerLookupSchema.CreditRating();
        Equifax_Conditions__c equiCond = Equifax_Conditions__c.getValues(acc.EquifaxApprovalType__c);
        
        creditInfo.dateRetrieved = (String.ValueOf(acc.Equifax_Last_Check_DateTime__c) == null ? '' : String.ValueOf((acc.Equifax_Last_Check_DateTime__c).date()));
        creditInfo.riskGrade = acc.EquifaxRiskGrade__c;
        creditInfo.approvalType = acc.EquifaxApprovalType__c;
        for(Equifax_Mapping__c eMap: [Select RiskGradeDisplayValue__c From Equifax_Mapping__c Where ApprovalType__c =: acc.EquifaxApprovalType__c And RiskGrade__c =: acc.EquifaxRiskGrade__c Limit 1]){
            creditInfo.message = eMap.RiskGradeDisplayValue__c.replaceALL('\n',' ');
        }   
        if(String.isBlank(creditInfo.message))
            creditInfo.message = equiCond.Agent_Message__c.replaceAll('\n',' ');                   
             
        if(equiCond.Payment_Frequency__c == 'Q')
            creditInfo.paymentFrequency = ADTPartnerCustomerLookupSchema.PAYMENT_VALUE.Quarterly;
        else if(equiCond.Payment_Frequency__c == 'A')
            creditInfo.paymentFrequency = ADTPartnerCustomerLookupSchema.PAYMENT_VALUE.Annual;
        else
            creditInfo.paymentFrequency = ADTPartnerCustomerLookupSchema.PAYMENT_VALUE.Monthly;
        
        if(equiCond.Deposit_Required_Percentage__c != null)
            creditInfo.depositPercent = Integer.valueOf(equiCond.Deposit_Required_Percentage__c);
        else
            creditInfo.depositPercent = 0;
            
        creditInfo.multiPayAllowed = equiCond.Three_Pay_Allowed__c ? True:False;
        
        list<String> threepayterm = new list<String>();
        if(equiCond.Three_Pay_Allowed__c){           
            threepayterm.add('3');
            creditInfo.multiPayTerms = threepayterm;
        }else{
            creditInfo.multiPayTerms = threepayterm;
        }
        creditInfo.easyPayRequired = equiCond.EasyPay_Required__c ? True:False;
        
        return creditInfo;
    }
    
    // Method to return the list of servicability restrictions
    public List<ADTPartnerCustomerLookupSchema.Serviceability> makeServiceabilityCall(){
        List<ADTPartnerCustomerLookupSchema.Serviceability> serviceabilityList = new List<ADTPartnerCustomerLookupSchema.Serviceability>();
        this.acc = new Account();
        this.acc = Accountinfo.getAccount();
        serviceabilityList = getservicability(this.acc);
        return serviceabilityList;
    }
    
    // Method to return Telemar Acc number 
    public String getleadManagementId(){
        String telemarAccNo = String.isnotBlank(acc.TelemarAccountNumber__c) ? acc.TelemarAccountNumber__c : '';
        return telemarAccNo;
    }
    
    public List<ADTPartnerCustomerLookupSchema.Serviceability> getservicability(Account acc){
        system.debug('Account inside getservicability: '+ acc);
        List<ADTPartnerCustomerLookupSchema.Serviceability> servList = new list<ADTPartnerCustomerLookupSchema.Serviceability>();
        ADTPartnerCustomerLookupSchema.Serviceability serviceabilityObj;
        Map<String,ADTPartnerCustomerLookupSchema.Serviceability> serviceabilityMap = new Map<String,ADTPartnerCustomerLookupSchema.Serviceability>();
        String rvServiceabilityOrderTypes = ResaleGlobalVariables__c.getInstance('RvServiceabilityOrderTypes').value__c;
        Map<String,String> orderTypeDescriptionMap = new Map<String,String>();
        orderTypeDescriptionMap = getRvOrderTypeDescription(rvServiceabilityOrderTypes);
        if(String.isNotBlank(rvServiceabilityOrderTypes)){
            for(String orderType : rvServiceabilityOrderTypes.split(';')){
                serviceabilityObj = new ADTPartnerCustomerLookupSchema.Serviceability();
                serviceabilityObj.orderType = orderType;
                String des = orderTypeDescriptionMap.get(orderType);
                serviceabilityObj.description = des;
                serviceabilityObj.orderAllowed = true;
                serviceabilityObj.appointmentAllowed = true;
                serviceabilityObj.orderMessage = '';
                serviceabilityObj.appointmentMessage = '';
                serviceabilityMap.put(orderType,serviceabilityObj);
            }
        }
        System.debug('### serviceability Map is'+serviceabilityMap);
        servList = buildServiceabilityRulesList(acc,serviceabilityMap);
        return servList;
    }
        
    private List<ADTPartnerCustomerLookupSchema.Serviceability> buildServiceabilityRulesList(Account a,Map<String,ADTPartnerCustomerLookupSchema.Serviceability> serviceabilityMap){
        Boolean isPostalCodeCheck;
        List<ADTPartnerCustomerLookupSchema.Serviceability> serviceabilityRulesList = new list<ADTPartnerCustomerLookupSchema.Serviceability>();
        //HRM9905
        User u = [Select Id, Rep_Team__c From User Where Id =: UserInfo.getUserID()];
        for(Serviceability__c serviceabilityObj : [SELECT Business_ID__c,Active__c,Order_Type_for_Phone__c,Order_Type__c,PostalCode__r.name,Reason__c,
                                                  Sales_Appointment__c,
                                                  Channel__c,State__c,TM_TownId__c, TM_Sub_TownID__c, Town__r.name FROM Serviceability__c 
                                                  where Active__c = true and Start_Date__c <=: System.Today() and End_Date__c >=: System.Today() AND Rep_Department__c INCLUDES (: u.Rep_Team__c)]){  
                                                      System.debug('Account-----'+a);
                                                      System.debug('serviceabilityObj-----'+serviceabilityObj);
                                                      System.debug('----postalcode----------'+a.PostalCodeID__r.name);
            if(a.PostalCodeID__r.name == null ){
                isPostalCodeCheck = false;
            }
            else if((serviceabilityObj.PostalCode__r.name !=null && (serviceabilityObj.PostalCode__r.name != a.PostalCodeID__r.name)) 
                  ||(serviceabilityObj.Town__r.name!=null && (serviceabilityObj.Town__r.name != a.PostalCodeID__r.Town_Lookup__r.name))
                  ||(serviceabilityObj.TM_TownId__c!=null && (serviceabilityObj.TM_TownId__c != a.TMTownID__c))
                  ||(serviceabilityObj.TM_Sub_TownID__c!=null && (serviceabilityObj.TM_Sub_TownID__c != a.TMSubTownID__c))){
                isPostalCodeCheck = false;
            }
            else {
                isPostalCodeCheck = true;
            }
            //  HRM 9905 - part of code optimization while code review
            if(!(a != null && (serviceabilityObj.Business_Id__c != null && String.isNotBlank(a.Business_ID__c) && (!serviceabilityObj.Business_ID__c.contains(a.Business_ID__c)))
                   || (serviceabilityObj.Channel__c != null && String.isNotBlank(a.Channel__c) && (!serviceabilityObj.Channel__c.contains(a.Channel__c)))
                   || (serviceabilityObj.State__c != null && String.isNotBlank(a.SiteState__c) && (serviceabilityObj.State__c != a.SiteState__c))
                   || !isPostalCodeCheck)) {    
                  // Checks if rule blocks Quoting in phone sales and update the map
                 if(String.isNotBlank(serviceabilityObj.Order_Type_for_Phone__c)) {
                    for(String str : serviceabilityObj.Order_Type_for_Phone__c.split(';')){
                        serviceabilityMap.get(str).orderAllowed = false;
                        //update the reason text and append if any text exists;
                        if(String.isNotBlank(serviceabilityObj.Reason__c) && String.isBlank(serviceabilityMap.get(str).orderMessage)){
                            serviceabilityMap.get(str).orderMessage = serviceabilityObj.Reason__c;
                        }
                    }
                 }
                 // Checks if rule blocks Sales Appointment in phone sales and update the map
                 if(String.isnotBlank(serviceabilityObj.Sales_Appointment__c)){
                    for(String str : serviceabilityObj.Sales_Appointment__c.split(';')){
                        serviceabilityMap.get(str).appointmentAllowed = false;
                        //update the reason text and append if any text exists;
                        if(String.isNotBlank(serviceabilityObj.Reason__c) && String.isBlank(serviceabilityMap.get(str).appointmentMessage)){
                            serviceabilityMap.get(str).appointmentMessage = serviceabilityObj.Reason__c;
                        }
                    }
                 }
            }
        }
        
        //Applying the partner rules
        if(String.isNotBlank(partnerId)){
            for(PartnerConfiguration__c rvConfig : [Select Id, PartnerMessage__c,AllowedOrderType__c from PartnerConfiguration__c where PartnerID__c =: partnerId]){
                String partnerDefaultMessage = rvConfig.PartnerMessage__c;
                if(String.isnotBlank(rvConfig.AllowedOrderType__c)){
                    for(String ss : serviceabilityMap.keySet()){
                        if(!rvConfig.AllowedOrderType__c.contains(serviceabilityMap.get(ss).orderType)){
                            if(String.isNotBlank(partnerDefaultMessage)){
                                if(String.isBlank(serviceabilityMap.get(ss).orderMessage)){
                                    serviceabilityMap.get(ss).orderMessage = partnerDefaultMessage;
                                    serviceabilityMap.get(ss).orderAllowed = false;
                                }
                                if(String.isBlank(serviceabilityMap.get(ss).appointmentMessage)){
                                    serviceabilityMap.get(ss).appointmentMessage = partnerDefaultMessage;
                                    serviceabilityMap.get(ss).appointmentAllowed = false;
                                }
                            }
                        }
                    }
                }
            }
        }
        //iterate over the map and send the list
        for(String k : serviceabilityMap.keySet()){
            serviceabilityRulesList.add(serviceabilityMap.get(k));
        }
        return serviceabilityRulesList;
    }
        
    private Map<String,String> getRvOrderTypeDescription(String orderTypes){
        Map<String,String> orderTypeDescriptionMap = new Map<String,String>();
        for(String orderType: orderTypes.split(';')){
            if(orderType.equalsIgnoreCase('n1')){
                orderTypeDescriptionMap.put(orderType,'New Customer and Site');
            }
            if(orderType.equalsIgnoreCase('n2')){
                orderTypeDescriptionMap.put(orderType,'Relocation New Site');
            }
            if(orderType.equalsIgnoreCase('n3')){
                orderTypeDescriptionMap.put(orderType,'Multi Site');
            }
            if(orderType.equalsIgnoreCase('n4')){
                orderTypeDescriptionMap.put(orderType,'Conversion');
            }
            if(orderType.equalsIgnoreCase('a1')){
                orderTypeDescriptionMap.put(orderType,'Add-On');
            }
            if(orderType.equalsIgnoreCase('r1')){
                orderTypeDescriptionMap.put(orderType,'Resale');
            }
             //Defect :1978 --- HRM-6599 Change ordertype value R3 to r2 -- Mounika Anna
            if(orderType.equalsIgnoreCase('r2')){
                orderTypeDescriptionMap.put(orderType,'Reinstatement');
            }
            if(orderType.equalsIgnoreCase('r3')){
                orderTypeDescriptionMap.put(orderType,'Resale Relocation');
            }//end of the change
            if(orderType.equalsIgnoreCase('r4')){
                orderTypeDescriptionMap.put(orderType,'Resale Multi Site');
            }
        }
        return orderTypeDescriptionMap;
    }
}