/************************************* MODIFICATION LOG ********************************************************************************************
* UserScheduleAdminController
*
* DESCRIPTION : Apex Controller to manage user schedule by town setup on the Postal Code custom object               
*
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera               1/27/2016               - Original Version
*
*                                                   
*/
public class UserScheduleAdminController {
        
    // URL Parameter usD = {}
    public static String IN_HOUSE_ACTIVATION = 'DLL';
    public static List<String> supportedProfiles = new List<String>{'ADT NA Sales Manager','ADT NA Sales Representative','ADT NSC DLL Sales Representative','ADT Commercial Representative','ADT Commercial Sales Manager'};
    public class TownSubtownSearchItem {
        public String Town;
        public String SubTown;
        public String BusinessID;
        public TownSubtownSearchItem(){
            Town = '';
            SubTown = '';
            BusinessID = '';
        }
        public TownSubtownSearchItem( Postal_Codes__c pc){
            Town = pc.TMTownID__c;
            SubTown = ( String.isBlank(pc.TMSubTownID__c)?'':pc.TMSubTownID__c );
            BusinessID = (!String.isBlank(pc.BusinessID__c)?pc.BusinessID__c:'');
        }
        public TownSubtownSearchItem( Postal_Code_Aggregate__c pc){
            Town = pc.TMTownID__c;
            SubTown = ( String.isBlank(pc.TMSubTownID__c)?'':pc.TMSubTownID__c );
            BusinessID = (!String.isBlank(pc.BusinessID__c)?pc.BusinessID__c:'');
        }
    }
    
    public String SelectedUserId {get;set;}
    public String SelectedTown {get;set;}
    public String SelectedSubtown {get;set;}
    public String SelectedAction {get;set;} 
    public String SelectedBusinessId {get;set;}
    public String JSONScheduleSetupData {get;set;}
    public Boolean isSuccess {get;set;}
    
    public String InHouseActivationCode {
        get{
            return IN_HOUSE_ACTIVATION;
        }
    }
    public Boolean renderRebuildAction {
        get{
            return [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name.equalsIgnoreCase('System Administrator');
        }
    }
    
    public PageReference SetupUserSchedule(){
        isSuccess = true;
        //PageReference pr = new PageReference('/apex/UserSchedule');
        UserScheduleData schData = new UserScheduleData(SelectedUserId, SelectedBusinessId, SelectedTown, SelectedSubtown);
        if( !String.isBlank(SelectedAction) ){
            if( SelectedAction.equalsIgnoreCase('view') ){
                schData.setSetupAction( UserScheduleData.SetupAction.VIEW );
            }
            else if( SelectedAction.equalsIgnoreCase('add') ){
                schData.setSetupAction( UserScheduleData.SetupAction.NEW_SCHEDULE );
            }
            else {
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Unsupported action <strong>'+SelectedAction+'</strong> '));
                isSuccess = false;
                return null;
            }
        }
        JSONScheduleSetupData = JSON.serialize(schData);
        //pr.getParameters().put('usD', JSONScheduleSetupData );
        return null;
    }
    
    public PageReference RefreshAdminView(){
        PageReference pr = new PageReference('/apex/UserScheduleAdmin');
        pr.setRedirect(true);
        return pr;
    }
    
    @RemoteAction
    public static List<User> getUserList(){
        List<User> resList = new List<User>();
        for(User u: [SELECT id, name, UserRoleId, UserRole.Name  FROM user WHERE isActive = true AND profile.name IN :supportedProfiles]){
            resList.add(u);
        }
        return resList;
    }
    
    @RemoteAction
    public static Boolean UserHasSchedule(String uID){
        return ( [SELECT count() FROM SchedUserTerr__c Where User__c = :uID AND TMTownID__c <> ''] > 0 );
    }
    
    @RemoteAction @ReadOnly 
    public static List<TownSubtownSearchItem> getTownList(){
        Map<String, TownSubtownSearchItem> TownSubtownMap = new Map<String, TownSubtownSearchItem>();
        for(Postal_Codes__c pc: [SELECT BusinessID__c, TMTownID__c, TMSubTownID__c FROM Postal_Codes__c WHERE TMTownID__c <> '' AND BusinessID__c <> '' Order By TMTownID__c, TMSubTownID__c]){
            TownSubtownMap.put( (pc.BusinessID__c+';'+pc.TMTownID__c+';'+pc.TMSubTownID__c), new TownSubtownSearchItem(pc) );
        }
        return TownSubtownMap.values();
    }
    
    @RemoteAction
    public static void rebuildAggregateEntry(String aggregateEntryList ){
        List< Object > objList = (List< Object >) JSON.deserializeUntyped(aggregateEntryList);
        List<Postal_Code_Aggregate__c> newPCAggregateList = new List<Postal_Code_Aggregate__c>();
        for( Object obj: objList ){
            Map<String, Object> mObj = (Map<String, Object> )obj;
            String t = (String)mObj.get('town');
            String st = (String)mObj.get('stown');
            String bid = (String)mObj.get('businessid');
            if( String.isBlank( t ) || String.isBlank( bid ) )
              continue;
            Postal_Code_Aggregate__c newPCAggregate = new Postal_Code_Aggregate__c();
            newPCAggregate.TMTownID__c = t;
            newPCAggregate.TMSubTownId__c = st;
            newPCAggregate.BusinessId__c = bid;
            newPCAggregateList.add( newPCAggregate );
        }
        Database.SaveResult[] srList = Database.insert( newPCAggregateList , false);
    }
    
    @RemoteAction
    public static void clearAggregateEntry(Boolean AllOrNone){
        List<Postal_Code_Aggregate__c> pcAggrList = [SELECT Id, Name FROM Postal_Code_Aggregate__c];
        //Database.DeleteResult[] drList = Database.delete(pcAggrList, AllOrNone);
        delete pcAggrList;
        //return [SELECT count() FROM Postal_Code_Aggregate__c] > 0;
    }
    
    @RemoteAction @ReadOnly 
    public static List<TownSubtownSearchItem> getTownAggregateList(){
        Map<String, TownSubtownSearchItem> TownSubtownMap = new Map<String, TownSubtownSearchItem>();
        for(Postal_Code_Aggregate__c pc: [SELECT BusinessID__c, TMTownID__c, TMSubTownID__c FROM Postal_Code_Aggregate__c WHERE TMTownID__c <> '' AND BusinessID__c <> '' Order By TMTownID__c, TMSubTownID__c]){
            TownSubtownMap.put( (pc.BusinessID__c+';'+pc.TMTownID__c+';'+pc.TMSubTownID__c), new TownSubtownSearchItem(pc) );
        }
        List<TownSubtownSearchItem> resL = new List<TownSubtownSearchItem>(); 
        resL.addAll(TownSubtownMap.values());

        // Add virtual territory used by In-House Activation technicians
        TownSubtownSearchItem vTerr = new TownSubtownSearchItem();
        vTerr.Town = IN_HOUSE_ACTIVATION; // In-House Activation
        vTerr.BusinessID = '1100';
        resL.add(vTerr);
        return resL;
    }
    
    @RemoteAction 
    public static List<SchedUserTerr__c> getUserScheduleByTown(String town, String subtown, String bId){
        return [SELECT Name, Order_Types__c, User__c, User__r.Name, User__r.UserName, User__r.Phone, User__r.email FROM SchedUserTerr__c WHERE TMTownID__c = :town AND TMSubTownID__c = :subtown AND BusinessId__c = :bId];
    }
    
    @RemoteAction 
    public static List<SchedUserTerr__c> getInHouseTechnicianSchedule(String techCode){
        return [SELECT Name, Order_Types__c, User__c, User__r.Name, User__r.UserName, User__r.Phone, User__r.email FROM SchedUserTerr__c WHERE TMTownID__c = :techCode];
    }
    
    @RemoteAction 
    public static List<SchedUserTerr__c> getUserScheduleByUser(String uName){
        return [SELECT Name, Order_Types__c, User__c, User__r.Name, User__r.UserName, User__r.Phone, User__r.email FROM SchedUserTerr__c WHERE User__r.UserName = :uName];
    }
    
}