/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class RepCoverageController_test {
	
	private static void createTestData(){
		integer i=12312334;
        profile p =[select id from profile where Name='ADT NA Sales Representative'];
        
        User u =new user(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='Testrep', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingrepcoverage'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        insert u;
        List<Postal_Code_Aggregate__c> listPca =new List<Postal_Code_Aggregate__c>();
        
        Postal_Code_Aggregate__c Pca = new Postal_Code_Aggregate__c(BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C='111');
        listPca.add(Pca);
 		Postal_Code_Aggregate__c Pca1 = new Postal_Code_Aggregate__c(BUSINESSID__C='1200',TMSUBTOWNID__C='14',TMTOWNID__C='112');        
        listPca.add(Pca1);
        insert listPca;
        Postal_Codes__c pc = new Postal_Codes__c(Name='06010',Area__c='NH/ME/CT/Albany',AreaID__c='100',BusinessID__c='1100',District__c='Central New England',District_Id__c='106',MMB_TownID__c='39',Region__c='NorthEast',Region_Id__c='090',TMSubTownID__c='TER8',TMTownID__c='111');
        insert pc;
        Postal_Codes__c pc1 = new Postal_Codes__c(Name='06010',Area__c='NH/ME/CT/Albany',AreaID__c='100',BusinessID__c='1200',District__c='Central New England',District_Id__c='106',MMB_TownID__c='39',Region__c='NorthEast',Region_Id__c='090',TMSubTownID__c='TER8',TMTownID__c='111');
        insert pc1;
        SchedUserTerr__c schusr = new SchedUserTerr__c(Name='Resi Town:111 Sub:TER8',FRIDAY_AVAILABLE_END__C=0,FRIDAY_AVAILABLE_START__C=0,MONDAY_AVAILABLE_END__C=1800,
        MONDAY_AVAILABLE_START__C=1000,ORDER_TYPES__C='New;Resale;Relocation;Add-On;Conversion;Custom Home',SATURDAY_AVAILABLE_END__C=0,SATURDAY_AVAILABLE_START__C=0,SUNDAY_AVAILABLE_END__C=0,
        SUNDAY_AVAILABLE_START__C=0,TERRITORY_ASSOCIATION_ID__C='0MI130000008QCRGA2',THURSDAY_AVAILABLE_END__C=0,THURSDAY_AVAILABLE_START__C=0,
        TUESDAY_AVAILABLE_END__C=0,TUESDAY_AVAILABLE_START__C=0,USER__C=u.Id,WEDNESDAY_AVAILABLE_END__C=2100,WEDNESDAY_AVAILABLE_START__C=900,
        BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C ='111');
        insert schusr;
	}

    static testMethod void ShowallTerritorieswithrepCoverage() {
        createTestData();
        
        PageReference pageRef = Page.RepCoverage;
        Test.setCurrentPage(pageRef);
        RepCoverageController repCoverage = new RepCoverageController();
        test.startTest();
        repCoverage.getLineofBusiness();
        repCoverage.showRepCoverageforAllTerritories();
        test.stopTest();
    }
    static testMethod void ShowallTerritorieswithNorepCoverage() {
        createTestData();
        
        PageReference pageRef = Page.RepCoverage;
        Test.setCurrentPage(pageRef);
        RepCoverageController repCoverage = new RepCoverageController();
        
        test.startTest();
        
        repCoverage.getLineofBusiness();
        repCoverage.showAllTerritorieswithNoRepCoverage();
        
        test.stopTest();
    }
    static testMethod void showRepCoverageByTerritory() {
        createTestData();
        PageReference pageRef = Page.RepCoverage;
        
        Test.setCurrentPage(pageRef);
        Test.startTest();
        RepCoverageController repCoverage = new RepCoverageController();
        repCoverage.getLineofBusiness();
        repCoverage.searchPostalCode='';
        repCoverage.SelectedBid='Resi';       
        repCoverage.searchTown ='Resi Town:111';        
        repCoverage.selectedTown =repCoverage.searchTown;
        RepCoverageController.findTownID(repCoverage.searchTown,'Resi');
        repCoverage.showRepCoverageByTerritory(); 
        
        repCoverage.SelectedBid='Resi'; 
        repCoverage.searchsubTown ='Resi Town:111 Sub:TER17';
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        RepCoverageController.findsubtownID(repCoverage.searchsubTown,'Resi');
        repCoverage.showRepCoverageByTerritory();
        
        repCoverage.SelectedBid='Resi'; 
        repCoverage.searchTown ='Resi Town:111';
        repCoverage.searchsubTown ='Resi Town:111 Sub:TER17';
        repCoverage.selectedTown =repCoverage.searchTown;
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        RepCoverageController.findTownID(repCoverage.searchTown,'Resi');
        RepCoverageController.findsubtownID(repCoverage.searchsubTown,'Resi');
        repCoverage.showRepCoverageByTerritory();
        
        test.stopTest(); 
    }
    
    static testMethod void showRepCoverageByTerritory_Postalcode() {
        createTestData();
        PageReference pageRef = Page.RepCoverage;
        
        Test.setCurrentPage(pageRef);
        RepCoverageController repCoverage = new RepCoverageController();
        test.startTest();
        repCoverage.getLineofBusiness();
        repCoverage.searchPostalCode='06010';
        repCoverage.SelectedBid='Resi';       
        repCoverage.searchTown ='';      
        repCoverage.searchsubTown ='';
        repCoverage.selectedTown =repCoverage.searchTown;
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        repCoverage.selectedPostalCode =repCoverage.searchPostalCode;
        RepCoverageController.findPostalcode(repCoverage.selectedPostalCode,'Resi');
        RepCoverageController.findPostalcode('1200','SMB');
        repCoverage.showRepCoverageByTerritory();
        ApexPages.currentPage().getParameters().put('bid', '111');
        ApexPages.currentPage().getParameters().put('terrid', '0MI130000008QCRGA2');
        repCoverage.getsearchTerritoryUsers();
        test.stopTest();
       
    }
    
    
}