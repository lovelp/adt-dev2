/************************************* MODIFICATION LOG ********************************************************************************************
* DIYEmailRest
*
* DESCRIPTION : Defines web services to enable Canopy Account Maintenance
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Mike Tuckman               2/28/2016          - Original Version
*
*                                                   
*/

@RestResource(urlMapping='/DIYEmail/*')
global with sharing class DIYEmailRest {

    global class DIYEmailPostReturn {
        public String status;
        public String message;
        public Id id;
    }

    /*
    @HttpDelete
    global static void doDelete() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String accountId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Canopy_Account__c account = [SELECT Id FROM Canopy_Account__c WHERE Id = :accountId];
        delete account;
    }
    */
  
    /*@HttpGet
    global static CanopyAccountGetReturn doGet() {
        CanopyAccountGetReturn actRet = new CanopyAccountRest.CanopyAccountGetReturn();
        actRet.status = 'false';
        try{
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;
            String mobile;
            String email;
            String deviceid;
            if (req.requestURI.lastIndexOf('/mobile/') > -1){
                mobile = req.requestURI.substring(req.requestURI.lastIndexOf('/mobile/')+8);
            }
            else if (req.requestURI.lastIndexOf('/email/') >-1){
                email = req.requestURI.substring(req.requestURI.lastIndexOf('/email/')+7);
            }
            else if (req.requestURI.lastIndexOf('/deviceid/') >-1){
                deviceid = req.requestURI.substring(req.requestURI.lastIndexOf('/deviceid/')+10);
            }
            else {
                throw new CanopyException('Search Parameter not correct');
            }
            Canopy_Account__c acct = null;
            if (mobile != null && mobile.length() > 0){
                acct = CanopyAccountUtilities.getAccountbyMobile(mobile);
            }
            else if (deviceid != null && deviceid.length() > 0){
                acct = CanopyAccountUtilities.getAccountbyDeviceID(deviceid);
            }
            else if (email != null && email.length() > 0){
                acct = CanopyAccountUtilities.getAccountbyEmail(email);
            }
            if (acct == null){
                throw new CanopyException ('Account not found');
            }
            else {
                actRet.status = 'true';
                actRet.mobile = acct.Mobile_Phone__c;
                actRet.email = acct.Email__c;
                actRet.name = acct.Name;
                actRet.deviceID = acct.Device_ID__c;
                actRet.deviceType = acct.Device_Type__c;
                actRet.carrier = acct.Carrier__c;
                actRet.id = acct.Id;
            }
        } catch (Exception e) {
            actRet.message = e.getMessage();
        }
        return actRet;
    }*/
  
    @HttpPost
    global static DIYEmailPostReturn doPost(String email, String ipaddr) {
        DIYEmailPostReturn emailRet = new DIYEmailRest.DIYEmailPostReturn();
        emailRet.status = 'false';
        try {
            DIY_Email__c diyemail = new DIY_Email__c();
            diyemail.Email__c = email;
            diyemail.IP_Addr__c = ipaddr;
            insert diyemail;
            emailRet.id = diyemail.id;
            emailRet.status = 'true';
            emailRet.message = 'Email Saved';
        }
        catch (Exception e){
            emailRet.message = e.getMessage();
        }
        return emailRet;
    }
}