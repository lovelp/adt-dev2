@isTest
private class MapUtilitiesTest {
	
	
	static testMethod void testGetBoundingBox() {
		
		Account a = TestHelperClass.createAccountData();
		
		Account fullA1 = [select OwnerId,  Id, Name, Latitude__c,Longitude__c,Phone, Type, Industry, Data_Source__c, DisconnectReason__c, DisconnectDate__c, InService__c, NewMover__c, NewMoverRecordID__c, NewMoverType__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, SiteStreet__C, SiteCity__c, SiteState__c, SitePostalCode__c, NewMoverNewPhone__c, DateAssigned__c,UnassignedLead__c, Owner.Name, SalesAppointmentDate__c, ScheduledInstallDate__c, SiteCountryCode__c, Channel__c, RecordTypeId from Account where Id = :a.Id];
		
		MapItem mi = new MapItem(fullA1);
		
		Test.startTest(); 
		
		List<Decimal> boxList = MapUtilities.getBoundingBox(mi, 5);
		/*System.assertEquals(29.8452410998552821997105643994211 , boxList[0]);
		System.assertEquals(-77.3477359056414946089037118567533, boxList[1]);
		System.assertEquals(39.0192189001447178002894356005789, boxList[2]);
		System.assertEquals(-77.1616660943585053910962881432467, boxList[3]);*/
		
		Test.stopTest();
		
	}
	
	static testMethod void testGetBoundingBoxNoLatLong() {
		
		Account a = TestHelperClass.createAccountData();
		
		MapItem mi = new MapItem(a);
		
		Test.startTest();
		
		List<Decimal> boxList = MapUtilities.getBoundingBox(mi, 5);
		System.assertEquals(1, boxList[0], 'Lat min should be 1');
		System.assertEquals(1, boxList[1], 'Long min should be 1');
		System.assertEquals(0, boxList[2], 'Lat max should be 0');
		System.assertEquals(0, boxList[3], 'Long max should be 0');
		
		Test.stopTest();
		
	}
	
	static testMethod void testGetDistance() {
		
		Account a1 = TestHelperClass.createAccountData();
		Account a2 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
		
		Account fullA1 = [select OwnerId,  Id, Name, Latitude__c,Longitude__c,Phone, Type, Industry, Data_Source__c, DisconnectReason__c, DisconnectDate__c, InService__c, NewMover__c, NewMoverRecordID__c, NewMoverType__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, SiteStreet__C, SiteCity__c, SiteState__c, SitePostalCode__c, NewMoverNewPhone__c, DateAssigned__c, UnassignedLead__c, Owner.Name, SalesAppointmentDate__c, ScheduledInstallDate__c, SiteCountryCode__c, Channel__c, RecordTypeId  from Account where Id = :a1.Id];
		Account fullA2 = [select OwnerId,  Id, Name, Latitude__c,Longitude__c,Phone, Type, Industry, Data_Source__c, DisconnectReason__c, DisconnectDate__c, InService__c, NewMover__c, NewMoverRecordID__c, NewMoverType__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, SiteStreet__C, SiteCity__c, SiteState__c, SitePostalCode__c, NewMoverNewPhone__c, DateAssigned__c, UnassignedLead__c, Owner.Name, SalesAppointmentDate__c, ScheduledInstallDate__c, SiteCountryCode__c, Channel__c, RecordTypeId  from Account where Id = :a2.Id];
		
		MapItem mi1 = new MapItem(fullA1);
		MapItem mi2 = new MapItem(fullA2);
		
		Test.startTest();
		
		Decimal distance = MapUtilities.getDistance(mi1, mi2);
		//System.assertEquals(1.98, distance);
		
		Test.stopTest();
		
		
	}
	
	static testMethod void testGetDistanceSamePoint() {
		
		Account a1 = TestHelperClass.createAccountData();
	
		Account fullA1 = [select OwnerId,  Id, Name, Latitude__c,Longitude__c,Phone, Type, Industry, Data_Source__c, DisconnectReason__c, DisconnectDate__c, InService__c, NewMover__c, NewMoverRecordID__c, NewMoverType__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, SiteStreet__C, SiteCity__c, SiteState__c, SitePostalCode__c, NewMoverNewPhone__c, DateAssigned__c, UnassignedLead__c, Owner.Name, SalesAppointmentDate__c, ScheduledInstallDate__c , SiteCountryCode__c, Channel__c, RecordTypeId from Account where Id = :a1.Id];
		
		MapItem mi1 = new MapItem(fullA1);
		MapItem mi2 = new MapItem(fullA1);
		
		Test.startTest();
		
		Decimal distance = MapUtilities.getDistance(mi1, mi2);
		System.assertEquals(0, distance);
		
		Test.stopTest();
		
		
	}
	
	static testMethod void testGetDistanceNoLatLong() {
		
		Account a1 = TestHelperClass.createAccountData();
		Account a2 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
	
		MapItem mi1 = new MapItem(a1);
		MapItem mi2 = new MapItem(a2);
		
		Test.startTest();
		
		Decimal distance = MapUtilities.getDistance(mi1, mi2);
		System.assertEquals(0, distance);
		
		Test.stopTest();
		
		
	}
	
	static testMethod void testGetDistanceLatAndLong() {
		
		Account a1 = TestHelperClass.createAccountData();
		Account a2 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
		
		Account fullA1 = [select OwnerId,  Id, Name, Latitude__c,Longitude__c,Phone, Type, Industry, Data_Source__c, DisconnectReason__c, DisconnectDate__c, InService__c, NewMover__c, NewMoverRecordID__c, NewMoverType__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, SiteStreet__C, SiteCity__c, SiteState__c, SitePostalCode__c, NewMoverNewPhone__c, DateAssigned__c,  UnassignedLead__c,  Owner.Name, SalesAppointmentDate__c, ScheduledInstallDate__c, SiteCountryCode__c, Channel__c  from Account where Id = :a1.Id];
		Account fullA2 = [select OwnerId,  Id, Name, Latitude__c,Longitude__c,Phone, Type, Industry, Data_Source__c, DisconnectReason__c, DisconnectDate__c, InService__c, NewMover__c, NewMoverRecordID__c, NewMoverType__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, SiteStreet__C, SiteCity__c, SiteState__c, SitePostalCode__c, NewMoverNewPhone__c, DateAssigned__c,  UnassignedLead__c,  Owner.Name, SalesAppointmentDate__c, ScheduledInstallDate__c, SiteCountryCode__c, Channel__c  from Account where Id = :a2.Id];
	
		
		Test.startTest();
		
		Decimal distance = MapUtilities.getDistance(fullA1.Latitude__c, fullA1.Longitude__c, fullA2.Latitude__c, fullA2.Longitude__c);
		//System.assertEquals(1.98, distance);
		
		Test.stopTest();
		
		
	}
	
	static testMethod void testGetDistanceSpecificLatLong() {
		
		/*
		Account a1 = TestHelperClass.createAccountData();
		Account a2 = TestHelperClass.createAccountData(Date.today(), '1100 - Residential');
		
		Account fullA1 = [select OwnerId,  Id, Name, Latitude__c,Longitude__c,Phone, Type, Industry, Data_Source__c, DisconnectReason__c, DisconnectDate__c, InService__c, NewMover__c, NewMoverRecordID__c, NewMoverType__c, DispositionDate__c, DispositionCode__c, SiteStreet__C, SiteCity__c, SiteState__c, SitePostalCode__c, NewMoverNewPhone__c, DateAssigned__c, Owner.Name from Account where Id = :a1.Id];
		Account fullA2 = [select OwnerId,  Id, Name, Latitude__c,Longitude__c,Phone, Type, Industry, Data_Source__c, DisconnectReason__c, DisconnectDate__c, InService__c, NewMover__c, NewMoverRecordID__c, NewMoverType__c, DispositionDate__c, DispositionCode__c, SiteStreet__C, SiteCity__c, SiteState__c, SitePostalCode__c, NewMoverNewPhone__c, DateAssigned__c, Owner.Name from Account where Id = :a2.Id];
		*/
		MapItem mi1 = new MapItem(33.345864, -111.973203);
		MapItem mi2 = new MapItem(33.345864, -111.973203);
		
		Test.startTest();
		
		Decimal distance = MapUtilities.getDistance(mi1, mi2);
		System.assertEquals(0, distance);
		
		Test.stopTest();
		
		
	}

}