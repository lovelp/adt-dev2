@isTest
private class DistanceMapTest 
{	

	static testMethod void testDistanceMap()
	{		
		String id = TestHelperClass.inferPostalCodeID('53202', '1100');
		
		//Create Addresses first
		Address__c addr1 = new Address__c();
		addr1.Latitude__c = 43.0401814;
		addr1.Longitude__c = -87.9029031;
		addr1.Street__c = '700 East Mason Street';
		addr1.City__c = 'Milwaukee';
		addr1.State__c = 'WI';
		addr1.PostalCode__c = '53202';
		addr1.OriginalAddress__c = '700 East Mason Street, Milwaukee, WI 53202';			
		insert addr1;
		
		//Create Addresses first
		Address__c addr2 = new Address__c();
		addr2.Latitude__c = 43.0402537;
		addr2.Longitude__c = -87.9016188;
		addr2.Street__c = '800 East Mason Street';
		addr2.City__c = 'Milwaukee';
		addr2.State__c = 'WI';
		addr2.PostalCode__c = '53202';
		addr2.OriginalAddress__c = '800 East Mason Street, Milwaukee, WI 53202';			
		insert addr2;
		
		Lead L1 = new Lead();
		L1.LastName = 'William';
		L1.Company = 'Brave Heart';
		L1.DispositionCode__c = 'Phone - No Answer';
		L1.Channel__c = 'Resi Direct Sales';
		L1.SiteStreet__c = '700 East Mason Street';
		L1.SiteCity__c = 'Milwaukee';		
		L1.SiteStateProvince__c = 'WI';
		L1.SiteCountryCode__c = 'US';
		L1.SitePostalCode__c = '53202';
		L1.PostalCodeID__c = null;
		L1.AddressID__c = addr1.Id;
		L1.Business_Id__c = null;			
		insert L1;
		
		Lead L2 = new Lead();
		L2.LastName = 'Wallace';
		L2.Company = 'Brave Heart';
		L2.DispositionCode__c = 'Phone - No Answer';
		L2.Channel__c = 'Resi Direct Sales';
		L2.SiteStreet__c = '800 East Mason Street';
		L2.SiteCity__c = 'Milwaukee';		
		L2.SiteStateProvince__c = 'WI';
		L2.SiteCountryCode__c = 'US';
		L2.SitePostalCode__c = '53202';
		L2.PostalCodeID__c = null;
		L2.AddressID__c = addr2.Id;
		L2.Business_Id__c = null;			
		insert L2;
		
		Account a1 = new Account();
		a1.AddressID__c = addr1.Id;
		a1.Name = 'Unit Test Account 2';
		a1.LeadStatus__c = 'Active';
		a1.UnassignedLead__c = false;
		a1.InService__c = true;
		a1.Business_Id__c = '1100 - Residential';
		a1.ShippingPostalCode = '53202';				
		insert a1;		    		
		
		Account a2 = new Account();
		a2.AddressID__c = addr2.Id;
		a2.Name = 'Unit Test Account';
		a2.LeadStatus__c = 'Active';
		a2.UnassignedLead__c = false;
		a2.InService__c = true;
		a2.Business_Id__c = '1100 - Residential';
		a2.ShippingPostalCode = '53202';			
		insert a2;							
    			
		Test.startTest();
		
		List<Account> AcctList = [SELECT  OwnerId, Id, Name, Latitude__c,Longitude__c, Phone, Type, InService__c, NewMover__c, NewMoverType__c, Data_Source__c, 
										  DisconnectReason__c, DisconnectDate__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, UnassignedLead__c, SiteStreet__c, SiteCity__c, 
										  SiteState__c, SitePostalCode__c, Owner.Name, SalesAppointmentDate__c, ScheduledInstallDate__c, DateAssigned__c, SiteCountryCode__c, Channel__c,
										  RecordTypeId, LastActivityDate__c  
										  FROM Account 
										  WHERE LeadStatus__c = 'Active' AND ( Id = :a1.Id OR Id = :a2.Id)]; 
		
		List<Lead> LeadsList = [SELECT  Id, Name, OwnerId, Latitude__c, Longitude__c, NewMover__c, SiteStreet__c, SiteCity__c, SiteStateProvince__c, SitePostalCode__c, 
										SiteCountryCode__c, Phone, Owner.Name, LeadSource, DispositionCode__c, DispositionDetail__c, Channel__c, DisconnectDate__c,
										DispositionDate__c, NewMoverType__c, Type__c, UnassignedLead__c 
										FROM Lead 
										WHERE Id = :L1.Id OR Id = :L2.Id]; 										
										 		
		MapItem mi = new MapItem(AcctList[0]);
							
		DistanceMap d1 = new DistanceMap(AcctList, LeadsList, 10.0, mi);
		List<MapItem> mapItemList = d1.getProspectsList(2.0, 100, 100);
		
		Test.stopTest();
		
		System.assert(mapItemList != null, 'returned list should not be null');
		System.assertEquals(2, mapItemList.size(), 'returned list should contain two items');
	}
	
	static testMethod void testDistanceMapResaleAccounts()
	{		
		String id = TestHelperClass.inferPostalCodeID('53202', '1100');
		
		//Create Addresses first
		Address__c addr1 = new Address__c();
		addr1.Latitude__c = 43.0401814;
		addr1.Longitude__c = -87.9029031;
		addr1.Street__c = '700 East Mason Street';
		addr1.City__c = 'Milwaukee';
		addr1.State__c = 'WI';
		addr1.PostalCode__c = '53202';
		addr1.OriginalAddress__c = '700 East Mason Street, Milwaukee, WI 53202';			
		insert addr1;
		
		//Create Addresses first
		Address__c addr2 = new Address__c();
		addr2.Latitude__c = 43.0402537;
		addr2.Longitude__c = -87.9016188;
		addr2.Street__c = '800 East Mason Street';
		addr2.City__c = 'Milwaukee';
		addr2.State__c = 'WI';
		addr2.PostalCode__c = '53202';
		addr2.OriginalAddress__c = '800 East Mason Street, Milwaukee, WI 53202';			
		insert addr2;
		
		Account a1 = new Account();
		a1.AddressID__c = addr1.Id;
		a1.Name = 'Unit Test Account 2';
		a1.LeadStatus__c = 'Active';
		a1.UnassignedLead__c = false;
		a1.InService__c = true;
		a1.Business_Id__c = '1100 - Residential';
		a1.ShippingPostalCode = '53202';
		a1.Channel__c = Channels.TYPE_RESIRESALE;
		a1.RecordTypeId = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account').Id;				
		insert a1;		    		
		
		Account a2 = new Account();
		a2.AddressID__c = addr2.Id;
		a2.Name = 'Unit Test Account';
		a2.LeadStatus__c = 'Active';
		a2.UnassignedLead__c = false;
		a2.InService__c = true;
		a2.Business_Id__c = '1200 - Small Business';
		a2.ShippingPostalCode = '53202';
		a2.Channel__c = Channels.TYPE_SBRESALE;
		a2.RecordTypeId = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account').Id;			
		insert a2;							
    			
		List<Account> AcctList = [SELECT  OwnerId, Id, Name, Latitude__c,Longitude__c, Phone, Type, InService__c, NewMover__c, NewMoverType__c, Data_Source__c,
										  DisconnectReason__c, DisconnectDate__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, UnassignedLead__c, SiteStreet__c, SiteCity__c, 
										  SiteState__c, SitePostalCode__c, Owner.Name, SalesAppointmentDate__c, ScheduledInstallDate__c, DateAssigned__c, SiteCountryCode__c, Channel__c,
										  RecordTypeId, LastActivityDate__c  
										  FROM Account 
										  WHERE LeadStatus__c = 'Active' AND ( Id = :a1.Id OR Id = :a2.Id)]; 
										 		
		MapItem mi = new MapItem(AcctList[0]);
							
		DistanceMap d1 = new DistanceMap(AcctList, null, 10.0, mi);
		Integer OOS_PERCENT_1 = 50;
		Integer RIF_PERCENT_1 = 0;
		Integer OOS_PERCENT_2 = 0;
		Integer RIF_PERCENT_2 = 100;
		
		Test.startTest();
		
		List<MapItem> mapItemList = d1.getProspectsList(2.0, OOS_PERCENT_1, RIF_PERCENT_1);
		
		System.assert(mapItemList != null, 'returned list should not be null');
		System.assertEquals(1, mapItemList.size(), 'returned list should contain one item');
		
		
		mapItemList = d1.getProspectsList(2.0, OOS_PERCENT_2, RIF_PERCENT_2);
		
		System.assert(mapItemList != null, 'returned list should not be null');
		System.assertEquals(0, mapItemList.size(), 'returned list should contain no items');
		
		Test.stopTest();
			
	}

	static testMethod void testDistanceMapRIFAccounts()
	{		
		String id = TestHelperClass.inferPostalCodeID('53202', '1100');
		
		//Create Addresses first
		Address__c addr1 = new Address__c();
		addr1.Latitude__c = 43.0401814;
		addr1.Longitude__c = -87.9029031;
		addr1.Street__c = '700 East Mason Street';
		addr1.City__c = 'Milwaukee';
		addr1.State__c = 'WI';
		addr1.PostalCode__c = '53202';
		addr1.OriginalAddress__c = '700 East Mason Street, Milwaukee, WI 53202';			
		insert addr1;
		
		//Create Addresses first
		Address__c addr2 = new Address__c();
		addr2.Latitude__c = 43.0402537;
		addr2.Longitude__c = -87.9016188;
		addr2.Street__c = '800 East Mason Street';
		addr2.City__c = 'Milwaukee';
		addr2.State__c = 'WI';
		addr2.PostalCode__c = '53202';
		addr2.OriginalAddress__c = '800 East Mason Street, Milwaukee, WI 53202';			
		insert addr2;
		
		Account a1 = new Account();
		a1.AddressID__c = addr1.Id;
		a1.Name = 'Unit Test Account 2';
		a1.LeadStatus__c = 'Active';
		a1.UnassignedLead__c = false;
		a1.InService__c = true;
		a1.Business_Id__c = '1100 - Residential';
		a1.ShippingPostalCode = '53202';
		a1.RecordTypeId = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account').Id;				
		insert a1;		    		
		
		Account a2 = new Account();
		a2.AddressID__c = addr2.Id;
		a2.Name = 'Unit Test Account';
		a2.LeadStatus__c = 'Active';
		a2.UnassignedLead__c = false;
		a2.InService__c = true;
		a2.Business_Id__c = '1100 - Residential';
		a2.ShippingPostalCode = '53202';
		a2.RecordTypeId = Utilities.getRecordType(RecordTypeName.RIF_ACCOUNT, 'Account').Id;			
		insert a2;							
    			
		List<Account> AcctList = [SELECT  OwnerId, Id, Name, Latitude__c,Longitude__c, Phone, Type, InService__c, NewMover__c, NewMoverType__c, Data_Source__c, 
										  DisconnectReason__c, DisconnectDate__c, DispositionDate__c, DispositionCode__c, DispositionDetail__c, UnassignedLead__c, SiteStreet__c, SiteCity__c, 
										  SiteState__c, SitePostalCode__c, Owner.Name, SalesAppointmentDate__c, ScheduledInstallDate__c, DateAssigned__c, SiteCountryCode__c, Channel__c,
										  RecordTypeId, LastActivityDate__c  
										  FROM Account 
										  WHERE LeadStatus__c = 'Active' AND ( Id = :a1.Id OR Id = :a2.Id)]; 
										 		
		MapItem mi = new MapItem(AcctList[0]);
							
		DistanceMap d1 = new DistanceMap(AcctList, null, 10.0, mi);
		Integer OOS_PERCENT_1 = 0;
		Integer RIF_PERCENT_1 = 50;
		Integer OOS_PERCENT_2 = 100;
		Integer RIF_PERCENT_2 = 0;
		
		Test.startTest();
		
		List<MapItem> mapItemList = d1.getProspectsList(2.0, OOS_PERCENT_1, RIF_PERCENT_1);
		
		System.assert(mapItemList != null, 'returned list should not be null');
		System.assertEquals(1, mapItemList.size(), 'returned list should contain one item');
		
		
		mapItemList = d1.getProspectsList(2.0, OOS_PERCENT_2, RIF_PERCENT_2);
		
		System.assert(mapItemList != null, 'returned list should not be null');
		System.assertEquals(0, mapItemList.size(), 'returned list should contain no items');
		
		Test.stopTest();
		
		
	}
	
	static testMethod void testGetProspectsList()
	{		
		String id = TestHelperClass.inferPostalCodeID('53202', '1100');
		
		//Create Address first
		Address__c addr1 = new Address__c();
		addr1.Latitude__c = 43.0401814;
		addr1.Longitude__c = -87.9029031;
		addr1.Street__c = '700 East Mason Street';
		addr1.City__c = 'Milwaukee';
		addr1.State__c = 'WI';
		addr1.PostalCode__c = '53202';
		addr1.OriginalAddress__c = '700 East Mason Street, Milwaukee, WI 53202';			
		insert addr1;
		
		Account a1 = new Account();
		a1.AddressID__c = addr1.Id;
		a1.Name = 'Unit Test Account 2';
		a1.LeadStatus__c = 'Active';
		a1.UnassignedLead__c = false;
		a1.InService__c = true;
		a1.Business_Id__c = '1100 - Residential';
		a1.ShippingPostalCode = '53202';
		a1.Channel__c = Channels.TYPE_RESIRESALE;
		a1.RecordTypeId = Utilities.getRecordType(RecordTypeName.ADT_NA_RESALE, 'Account').Id;				
		insert a1;		    		
										 		
		MapItem mi1 = new MapItem(a1, .04);
		MapItem mi2 = new MapItem(a1, .04);
		MapItem mi3 = new MapItem(a1, .05);
		MapItem mi4 = new MapItem(a1, .06);
		MapItem mi5 = new MapItem(a1, .06);
		MapItem mi6 = new MapItem(a1, .07);
		MapItem mi7 = new MapItem(a1, .07);
		MapItem mi8 = new MapItem(a1, .07);
		MapItem mi9 = new MapItem(a1, .07);
		MapItem mi10 = new MapItem(a1, .07);
		MapItem mi11 = new MapItem(a1, .08);
		MapItem mi12 = new MapItem(a1, .11);
		MapItem mi13 = new MapItem(a1, .27);
		MapItem mi14 = new MapItem(a1, .31);
		MapItem mi15 = new MapItem(a1, .35);
		
		List<MapItem> l04 = new List<MapItem>();
		l04.add(mi1);
		l04.add(mi2);
		
		List<MapItem> l05 = new List<MapItem>();
		l05.add(mi3);
		
		List<MapItem> l06 = new List<MapItem>();
		l06.add(mi4);
		l06.add(mi5);
		
		List<MapItem> l07 = new List<MapItem>();
		l07.add(mi6);
		l07.add(mi7);
		l07.add(mi8);
		l07.add(mi9);
		l07.add(mi10);
		
		List<MapItem> l08 = new List<MapItem>();
		l08.add(mi11);
		
		List<MapItem> l11 = new List<MapItem>();
		l11.add(mi12);
		
		List<MapItem> l27 = new List<MapItem>();
		l27.add(mi13);
		
		List<MapItem> l31 = new List<MapItem>();
		l31.add(mi14);
		
		List<MapItem> l35 = new List<MapItem>();
		l35.add(mi15);
		
		Map<Decimal, List<MapItem>> distLocationMap = new Map<Decimal, List<MapItem>>();
		distLocationMap.put(.04, l04);
		distLocationMap.put(.05, l05);
		distLocationMap.put(.06, l06);
		distLocationMap.put(.07, l07);
		distLocationMap.put(.08, l08);
		distLocationMap.put(.11, l11);
		distLocationMap.put(.27, l27);
		distLocationMap.put(.31, l31);
		distLocationMap.put(.35, l35);
		
		 
							
		DistanceMap d1 = new DistanceMap(null, null, 10.0, null);
		d1.distLocationMap = distLocationMap;
		Integer OOS_PERCENT_1 = 20;
		Integer RIF_PERCENT_1 = 0;
		
		
		Test.startTest();
		
		List<MapItem> mapItemList = d1.getProspectsList(50, OOS_PERCENT_1, RIF_PERCENT_1);
		
		System.assert(mapItemList != null, 'returned list should not be null');
		System.assertEquals(10, mapItemList.size(), 'returned list should contain one item');
		
		Test.stopTest();
			
	}
	
	
	
}