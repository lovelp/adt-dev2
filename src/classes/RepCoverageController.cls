/************************************* MODIFICATION LOG ********************************************************************************************
* RepCoverageController
*
* DESCRIPTION : Controller to display the Repcoverage for terrioties by Order types
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*                            09/12/2016             - Original Version
*
*                                                   
*/
public class RepCoverageController {
    public string SelectedBid {get;set;}
    public String searchTown {get; set;}
    public String selectedTown {get; set;}
    public String searchsubTown {get;set;}
    public String searchPostalCode {get;set;}
    public String selectedSubTown {get;set;}
    public String selectedPostalCode {get;set;}
    public transient list<allTerritoryUsers> allterrList {get;set;}
    public Map<Id,String> monUsrMap {get;set;}
    public Map<Id,String> tueUsrMap {get;set;}
    public Map<Id,String> wedUsrMap {get;set;}
    public Map<Id,String> thuUsrMap {get;set;}
    public Map<Id,String> friUsrMap {get;set;}
    public Map<Id,String> satUsrMap {get;set;}
    public Map<Id,String> sunUsrMap {get;set;}
    
    public RepCoverageController(){
            
        SelectedBid ='';
        selectedTown ='';
        selectedPostalCode ='';
        selectedSubTown= ''; 
        allterrList = new list<allTerritoryUsers>();
        monUsrMap = new Map<Id,String>();
        tueUsrMap = new Map<Id,String>();
        wedUsrMap = new Map<Id,String>();
        thuUsrMap = new Map<Id,String>();
        friUsrMap = new Map<Id,String>();
        satUsrMap = new Map<Id,String>();
        sunUsrMap = new Map<Id,String>();
    }
    
    Public list<SelectOption>  getLineofBusiness(){
    
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Resi','Resi'));
        options.add(new SelectOption('SMB','SMB'));
        return options;
    
    }
    //Method to autofill the town search field
    @remoteAction
    public static list<String> findTownID(string searchTown,string busId){
        system.Debug('searchTown=='+searchTown);
        List<String> StrTowns = new List<String>();
        String strSearch = '%'+SearchTown+'%';
        for(territory2 terr :[SELECT id,Name FROM Territory2 where Name Like :strSearch and (parentTerritory2Id!=null and parentTerritory2.Name=:busId) and territory2Model.DeveloperName=:'Sales_Appt' order by Name ASC limit 10000]){
                StrTowns.add(terr.Name);
        }
        return StrTowns;
    }
    //Method to autofill the sub-town search field
    @remoteAction
    public static list<String> findsubtownID(string searchSubTown,string busId){
        system.Debug('======'+busId);
        List<String> StrSubTowns = new List<String>();
        String strSearch = '%'+searchsubTown+'%';
        for(territory2 terr :[SELECT id, Name, parentTerritory2Id, parentTerritory2.Name FROM Territory2 WHERE Name LIKE :strSearch and territory2Model.DeveloperName=:'Sales_Appt' AND  parentTerritory2Id != NULL AND parentTerritory2.Name !=:busId AND parentTerritory2.Name like:busId+'%' order by Name ASC limit 10000]){
            
            StrSubTowns.add(terr.Name);
        }
        return StrSubTowns;
    }
    //Method to autofill the Postalcodesearch field
    @remoteAction
    public static list<String> findPostalcode(string searchPostalCode,string busId){
        List<String> strPostalCodes = new List<String>();
        String strSearch = searchPostalCode+'%';
        String strQuery ='Select id,Name from Postal_Codes__c where Name like \''+strSearch+'\'';
        if(busId=='Resi'){
           strQuery =strQuery+ ' and BusinessID__c=\'1100\' order by Name Asc';
        }else if(busId=='SMB'){
            strQuery =strQuery+ ' and BusinessID__c=\'1200\' order by Name Asc';
        }
        for(Postal_Codes__c pcId :database.Query(strQuery)){
            strPostalCodes.add(pcId.Name);
        }
        return strPostalCodes;
    }
    
           
    public pagereference showRepCoverageByTerritory(){
        allterrList = new list<allTerritoryUsers>();
        Map<String,List<SchedUserTerr__c>> mapSchusrTerr =New Map<String,List<SchedUserTerr__c>>();
        Map<String,String> MapAccandterrids = new Map<String,String>();
        List<String> lstpostalcodes = new List<String>();
        Map<id,Account> mapAccs ;
        Map<ID,Territory2> mapTerritory = new MAp<Id,Territory2>();
        
        string strterrName='';
        String StrSubTerrName='';
        String strName = SelectedBid+'%';
        if(!String.isBlank(selectedTown) && String.isBlank(selectedSubTown)){             
             mapTerritory = New Map<ID,Territory2>([SELECT Id,Name FROM Territory2 where (Name like : selectedTown+'%') and territory2Model.DeveloperName=:'Sales_Appt' limit 10000]);
        }else if(!String.isBlank(selectedSubTown) && String.isBlank(selectedTown)) {            
            mapTerritory = New Map<ID,Territory2>([SELECT Id,Name FROM Territory2 where (Name=:selectedSubTown) and territory2Model.DeveloperName=:'Sales_Appt' limit 10000]);
        }else if(!String.isBlank(selectedTown) && !String.isBlank(selectedSubTown)){            
            mapTerritory = New Map<ID,Territory2>([SELECT Id,Name FROM Territory2 where (Name like : selectedTown OR Name=:selectedSubTown) and territory2Model.DeveloperName=:'Sales_Appt' limit 10000]);
        }else if(!String.isBlank(selectedPostalCode)){
            String strQuery ='Select id,Name,TMSubTownID__c,TMTownID__c from Postal_Codes__c where Name = \''+selectedPostalCode+'\'';
            if(SelectedBid=='Resi'){
               strQuery =strQuery+ ' and BusinessID__c=\'1100\' limit 1';
            }else if(SelectedBid=='SMB'){
                strQuery =strQuery+ ' and BusinessID__c=\'1200\' limit 1';
            }
            for(Postal_Codes__c postcode : Database.query(strQuery)){
                system.Debug('**postcode ======'+postcode );
                if(postcode.TMTownID__c!=null && postcode.TMSubTownID__c!=null){
                    if(SelectedBid=='Resi'){
                        strterrName = 'Resi Town:'+postcode.TMTownID__c;                        
                        StrSubTerrName= 'Resi Town:'+postcode.TMTownID__c+' '+'Sub:'+postcode.TMSubTownID__c;
                    }else if(SelectedBid=='SMB'){
                        strterrName = 'SMB Town:'+postcode.TMTownID__c; 
                        StrSubTerrName= 'SMB Town:'+postcode.TMTownID__c+' '+'Sub:'+postcode.TMSubTownID__c; 
                    }
                                         
                    mapTerritory = New Map<ID,Territory2>([SELECT Id,Name FROM Territory2 where Name=:StrSubTerrName and territory2Model.DeveloperName=:'Sales_Appt' limit 10000]);
                }else if(postcode.TMTownID__c!=null && postcode.TMSubTownID__c==null){
                    if(SelectedBid=='Resi'){
                        strterrName = 'Resi Town:'+postcode.TMTownID__c;                        
                    }else if(SelectedBid=='SMB'){
                        strterrName = 'SMB Town:'+postcode.TMTownID__c; 
                    }
                    
                     mapTerritory = New Map<ID,Territory2>([SELECT Id,Name FROM Territory2 where Name=: strterrName and territory2Model.DeveloperName=:'Sales_Appt' limit 10000]);
                }
            }
        }else{
            mapTerritory = New Map<ID,Territory2>([SELECT Id,Name FROM Territory2 where territory2Model.Name=:'Sales Appt' and Name like : strName limit 10000]);
            //ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info, 'Please enter data in One of filter fields to apply filters') );
        }
                
        
               
        for(SchedUserTerr__c schusr : [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,Town_ID__c,Sub_Town_ID__c,Territory_Association_ID__c from SchedUserTerr__c where Territory_Association_ID__c IN : mapTerritory.keyset() and user__r.isActive=true limit 10000]){
            List<SchedUserTerr__c> lstterrusers = mapSchusrTerr.get(schusr.Territory_Association_ID__c);
            if (lstterrusers == null) {
                lstterrusers = new List<SchedUserTerr__c>();
                mapSchusrTerr.put(schusr.Territory_Association_ID__c, lstterrusers);
            }
            lstterrusers.add(schusr);
        } 
         
        
        String territoryModel;String PostalCode;string townId;string subTownId;
        
        for(String str : mapTerritory.keySet()){
            if(mapSchusrTerr.get(str)!=null && mapSchusrTerr.get(str).Size()>0){
                String NewRepName='';
                String ResaleRepName='';
                String RelocationRepName='';
                String AddOnRepName='';
                String ConversionRepName='';
                String CustomHomeRepName=''; 
                String ordType ='New;Resale;Relocation;Add-On;Conversion;Custom Home';
                for(SchedUserTerr__c schterr : mapSchusrTerr.get(str)){                    
                    if(schterr.Order_Types__c !=null){
                        townId =schterr.Town_ID__c;
                        subTownId =schterr.Sub_Town_ID__c;
                        List<String> Types = schterr.Order_Types__c.split(';');
                        String usrName = schterr.User__r.FirstName+' '+ schterr.User__r.LastName;
                        for(string Type : Types){
                             if(Type =='New'){
                                 NewRepName +=usrName+';';
                             }if(Type =='Resale'){
                                 ResaleRepName +=usrName+';';
                             }if(Type =='Relocation'){
                                 RelocationRepName +=usrName+';';
                             }if(Type =='Add-On'){
                                 AddOnRepName +=usrName+';';
                             }if(Type =='Conversion'){
                                 ConversionRepName +=usrName+';';
                             }if(Type=='Custom Home'){
                                CustomHomeRepName +=usrName+';';
                             }
                        }
                    }
                }
                allTerritoryUsers terr = new allTerritoryUsers(mapTerritory.get(str).Name,townId,subTownId,null,
                                                    ordType,NewRepName,ResaleRepName,RelocationRepName,AddOnRepName,ConversionRepName,CustomHomeRepName,str);
                    System.Debug('terr ====='+terr );
                    allterrList.add(terr);
            }else{
                String townid1=null;
                String subtownid1=null;
                if(mapTerritory.get(str).Name !=null && mapTerritory.get(str).Name.Contains('Town:') && !mapTerritory.get(str).Name.Contains('Sub:')){
                    townid1=mapTerritory.get(str).Name.Remove(mapTerritory.get(str).Name.subString(0, mapTerritory.get(str).Name.LastIndexOf(':')+1));
                }if(mapTerritory.get(str).Name.Contains('Sub:')){                    
                    townid1= mapTerritory.get(str).Name.substringBetween(':', 'Sub:');
                    subtownid1=mapTerritory.get(str).Name.Remove(mapTerritory.get(str).Name.subString(0, mapTerritory.get(str).Name.LastIndexOf(':')+1));
                }
                allTerritoryUsers terr = new allTerritoryUsers(mapTerritory.get(str).Name,townid1,subtownid1 ,null,null,
                                                                null,null,null,null,null,null,null);
                allterrList.add(terr);
            }
            
        }
        selectedTown ='';
        selectedPostalCode ='';
        selectedSubTown= ''; 
        
        return null;
        
   }
   
   public pagereference showRepCoverageforAllTerritories(){
        allterrList = new list<allTerritoryUsers>();
        Map<String,List<SchedUserTerr__c>> mapSchusrTerr =New Map<String,List<SchedUserTerr__c>>();
        Map<String,String> MapAccandterrids = new Map<String,String>();
        List<String> lstpostalcodes = new List<String>();
        Map<ID,Territory2> mapTerritory = New Map<ID,Territory2>([SELECT Id,Name FROM Territory2 where territory2Model.Name=:'Sales Appt' limit 10000]);
                
        String strName = SelectedBid+'%';
               
        for(SchedUserTerr__c schusr : [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,Town_ID__c,Sub_Town_ID__c,Territory_Association_ID__c from SchedUserTerr__c where Territory_Association_ID__c IN : mapTerritory.keyset() and user__r.isActive=true limit 10000]){
            List<SchedUserTerr__c> lstterrusers = mapSchusrTerr.get(schusr.Territory_Association_ID__c);
            if (lstterrusers == null) {
                lstterrusers = new List<SchedUserTerr__c>();
                mapSchusrTerr.put(schusr.Territory_Association_ID__c, lstterrusers);
            }
            lstterrusers.add(schusr);
        } 
        String territoryModel;String PostalCode;string townId;string subTownId;
        for(String str : mapTerritory.keySet()){
            if(mapSchusrTerr.get(str)!=null && mapSchusrTerr.get(str).Size()>0){
                String NewRepName='';
                String ResaleRepName='';
                String RelocationRepName='';
                String AddOnRepName='';
                String ConversionRepName='';
                String CustomHomeRepName='';  
                String ordType ='New;Resale;Relocation;Add-On;Conversion;Custom Home';
                for(SchedUserTerr__c schterr : mapSchusrTerr.get(str)){                    
                    if(schterr.Order_Types__c !=null){
                        townId =schterr.Town_ID__c;
                        subTownId =schterr.Sub_Town_ID__c;
                        List<String> Types = schterr.Order_Types__c.split(';');
                        String usrName = schterr.User__r.FirstName+' '+ schterr.User__r.LastName;
                        for(string Type : Types){
                             if(Type =='New'){
                                 NewRepName +=usrName+';';
                             }if(Type =='Resale'){
                                 ResaleRepName +=usrName+';';
                             }if(Type =='Relocation'){
                                 RelocationRepName +=usrName+';';
                             }if(Type =='Add-On'){
                                 AddOnRepName +=usrName+';';
                             }if(Type =='Conversion'){
                                 ConversionRepName +=usrName+';';
                             }if(Type=='Custom Home'){
                                CustomHomeRepName +=usrName+';';
                             }
                        }
                    }
                }
                allTerritoryUsers terr = new allTerritoryUsers(mapTerritory.get(str).Name,townId,subTownId,null,
                                                    ordType,NewRepName,ResaleRepName,RelocationRepName,AddOnRepName,ConversionRepName,CustomHomeRepName,str);
                allterrList.add(terr);
            }else {
                String townid1=null;
                String subtownid1=null;
                if(mapTerritory.get(str).Name !=null && mapTerritory.get(str).Name.Contains('Town:') && !mapTerritory.get(str).Name.Contains('Sub:')){
                    townid1=mapTerritory.get(str).Name.Remove(mapTerritory.get(str).Name.subString(0, mapTerritory.get(str).Name.LastIndexOf(':')+1));
                }if(mapTerritory.get(str).Name.Contains('Sub:')){                    
                    townid1= mapTerritory.get(str).Name.substringBetween(':', 'Sub:');
                    subtownid1=mapTerritory.get(str).Name.Remove(mapTerritory.get(str).Name.subString(0, mapTerritory.get(str).Name.LastIndexOf(':')+1));
                }
                allTerritoryUsers terr = new allTerritoryUsers(mapTerritory.get(str).Name,townid1,subtownid1 ,null,null,
                                                                null,null,null,null,null,null,null);
                allterrList.add(terr);
            }
            
        }
        
        return null;
        
   }
   
   public pagereference showAllTerritorieswithNoRepCoverage(){        
        allterrList = new list<allTerritoryUsers>();
        Map<String,List<SchedUserTerr__c>> mapSchusrTerr =New Map<String,List<SchedUserTerr__c>>();
        Map<String,String> MapAccandterrids = new Map<String,String>();
        List<String> lstpostalcodes = new List<String>();
        Map<id,Account> mapAccs ;
                
        Map<ID,Territory2> mapTerritory = New Map<ID,Territory2>([SELECT Id,Name FROM Territory2 where territory2Model.Name=:'Sales Appt' limit 10000]);
        
        system.Debug('====='+mapTerritory );        
        String strName = SelectedBid+'%';
               
        for(SchedUserTerr__c schusr : [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,Town_ID__c,Sub_Town_ID__c,Territory_Association_ID__c from SchedUserTerr__c where Territory_Association_ID__c IN : mapTerritory.keyset() and user__r.isActive=true limit 10000]){
            List<SchedUserTerr__c> lstterrusers = mapSchusrTerr.get(schusr.Territory_Association_ID__c);
            if (lstterrusers == null) {
                lstterrusers = new List<SchedUserTerr__c>();
                mapSchusrTerr.put(schusr.Territory_Association_ID__c, lstterrusers);
            }
            lstterrusers.add(schusr);
        } 
        
        for(String str : mapTerritory.keySet()){
            if(mapSchusrTerr.get(str)==null){
                String townid=null;
                String subtownid=null;
                if(mapTerritory.get(str).Name !=null && mapTerritory.get(str).Name.Contains('Town:') && !mapTerritory.get(str).Name.Contains('Sub:')){
                    townid=mapTerritory.get(str).Name.Remove(mapTerritory.get(str).Name.subString(0, mapTerritory.get(str).Name.LastIndexOf(':')+1));
                }if(mapTerritory.get(str).Name.Contains('Sub:')){                    
                    townid= mapTerritory.get(str).Name.substringBetween(':', 'Sub:');
                    subtownid=mapTerritory.get(str).Name.Remove(mapTerritory.get(str).Name.subString(0, mapTerritory.get(str).Name.LastIndexOf(':')+1));
                }
                allTerritoryUsers terr = new allTerritoryUsers(mapTerritory.get(str).Name,townid,subtownid ,null,null,
                                                                null,null,null,null,null,null,null);
                allterrList.add(terr);
            }            
        }
        
        return null;
        
   }
   public List<SchedUserTerr__c> getsearchTerritoryUsers(){
        //List<UserTerritory2Association> territoryAssociations=[select id,userId from UserTerritory2Association where isactive=true and territory2id=:selectedTerrId];
        Map<Decimal,String> timeMap = new Map<Decimal,String>{100=>'1:00 AM',200=>'2:00 AM',300=>'3:00 AM',400=>'4:00 AM',500=>'5:00 AM',600=>'6:00 AM',700=>'7:00 AM',
                            800=>'8:00 AM',900=>'9:00 AM',1000=>'10:00 AM',1100=>'11:00 AM',1200=>'12:00 AM',1300=>'1:00 PM',1400=>'2:00 PM',1500=>'3:00 PM',1600=>'4:00 PM',
                            1700=>'5:00 PM',1800=>'6:00 PM',1900=>'7:00 PM',2000=>'8:00 PM',2100=>'9:00 PM',2200=>'10:00 PM',2300=>'11:00 PM',2400=>'12:00 PM',115=>'1:15 AM',
                            215=>'2:15 AM',315=>'3:15 AM',415=>'4:15 AM',515=>'5:15 AM',615=>'6:15 AM',715=>'7:15 AM',815=>'8:15 AM',915=>'9:15 AM',1015=>'10:15 AM',
                            1115=>'11:15 AM',1215=>'12:15 AM',1315=>'1:15 PM',1415=>'2:15 PM',1515=>'3:15 PM',1615=>'4:15 PM',1715=>'5:15 PM',1815=>'6:15 PM',1915=>'7:15 PM',
                            2015=>'8:15 PM',2115=>'9:15 PM',2215=>'10:15 PM',2315=>'11:15 PM',2415=>'12:15 PM',130=>'1:30 AM',230=>'2:30 AM',330=>'3:30 AM',430=>'4:30 AM',
                            530=>'5:30 AM',630=>'6:30 AM',730=>'7:30 AM',830=>'8:30 AM',930=>'9:30 AM',1030=>'10:30 AM',1130=>'11:30 AM',1230=>'12:30 AM',1330=>'1:30 PM',
                            1430=>'2:30 PM',1530=>'3:30 PM',1630=>'4:30 PM',1730=>'5:30 PM',1830=>'6:30 PM',1930=>'7:30 PM',2030=>'8:30 PM',2130=>'9:30 PM',2230=>'10:30 PM',
                            2330=>'11:30 PM',2430=>'12:30 PM'};
        List<SchedUserTerr__c> terrUserSchedules = new List<SchedUserTerr__c>();        
        String selectedTerrId =ApexPages.currentPage().getParameters().get('terrid');
        String bId=ApexPages.currentPage().getParameters().get('bId');
        terrUserSchedules=[select id,user__c,user__r.Name,Order_Types__c,Sub_Town_ID__c,Town_ID__c,Monday_Available_Start__c,Monday_Available_End__c,
                                                   Tuesday_Available_Start__c, Tuesday_Available_End__c,
                                                   Wednesday_Available_Start__c,Wednesday_Available_End__c,
                                                   Thursday_Available_Start__c,Thursday_Available_End__c,
                                                   Friday_Available_Start__c,Friday_Available_End__c,
                                                   Saturday_Available_Start__c,Saturday_Available_End__c,
                                                   Sunday_Available_Start__c,Sunday_Available_End__c
                           From SchedUserTerr__c Where Territory_Association_ID__c=:selectedTerrId and user__r.isactive=true and Order_Types__c!=null and Name like: '%'+bId+'%' limit 10000];
        for (SchedUserTerr__c ut:terrUserSchedules){
            String repNotAvailable = 'Not Available';
            if (ut.Monday_Available_Start__c > 0 && ut.Monday_Available_End__c > 0)
                monUsrMap.put(ut.user__c,timeMap.get(ut.Monday_Available_Start__c)+' to '+timeMap.get(ut.Monday_Available_End__c));
            else
                monUsrMap.put(ut.user__c, repNotAvailable);
            if (ut.Tuesday_Available_Start__c > 0  && ut.Tuesday_Available_End__c > 0)
                tueUsrMap.put(ut.user__c,timeMap.get(ut.Tuesday_Available_Start__c)+' to '+timeMap.get(ut.Tuesday_Available_End__c));
            else
                tueUsrMap.put(ut.user__c, repNotAvailable);
            if (ut.Wednesday_Available_Start__c > 0 && ut.Wednesday_Available_End__c > 0)
                wedUsrMap.put(ut.user__c,timeMap.get(ut.Wednesday_Available_Start__c)+' to '+timeMap.get(ut.Wednesday_Available_End__c));
            else
                wedUsrMap.put(ut.user__c, repNotAvailable);
            if (ut.Thursday_Available_Start__c > 0 && ut.Thursday_Available_End__c > 0)
                thuUsrMap.put(ut.user__c,timeMap.get(ut.Thursday_Available_Start__c)+' to '+timeMap.get(ut.Thursday_Available_End__c));
            else
                thuUsrMap.put(ut.user__c, repNotAvailable);
            if (ut.Friday_Available_Start__c > 0 && ut.Friday_Available_End__c > 0)
                friUsrMap.put(ut.user__c,timeMap.get(ut.Friday_Available_Start__c)+' to '+timeMap.get(ut.Friday_Available_End__c));
            else
                friUsrMap.put(ut.user__c, repNotAvailable);
            if (ut.Saturday_Available_Start__c > 0 && ut.Saturday_Available_End__c > 0)
                satUsrMap.put(ut.user__c,timeMap.get(ut.Saturday_Available_Start__c)+' to '+timeMap.get(ut.Saturday_Available_End__c));
            else
                satUsrMap.put(ut.user__c, repNotAvailable);
            if (ut.Sunday_Available_Start__c > 0 && ut.Sunday_Available_End__c > 0)
                sunUsrMap.put(ut.user__c,timeMap.get(ut.Sunday_Available_Start__c)+' to '+timeMap.get(ut.Sunday_Available_End__c));
            else
                sunUsrMap.put(ut.user__c, repNotAvailable);
        }
        return terrUserSchedules;
    }
   
   public class allTerritoryUsers{
        public String territoryModel{get; set;}
        public String PostalCode{get; set;}
        public String OrderType{get; set;}
        public string townId{get; set;}
        public string subTownId{get; set;}
        public String TerritoryAssignedRep{get; set;}
        public String NewOrderType{get; set;}
        public String ResaleOrderType{get; set;}
        public String RelocationOrderType{get; set;}
        public String AddOnOrderType{get; set;}
        public String ConversionOrderType{get; set;}
        public String CustomHomeOrderType{get; set;}
        public String TerritoryID{get;set;}
        
        public allTerritoryUsers(String territory,string tId,string subTId,String PCode,String OType,
                                    string NewName,string Resale,string Relocation,
                                    string AddOn,string Conversion,string CustomHome,string terrId){
            territoryModel=territory;
            townId = tId;
            subTownId =subTId;
            PostalCode=PCode;
            OrderType=OType;
            if(!String.isBlank(Otype)){ 
                List<String> Types = Otype.split(';');
                for(string Type : Types){
                 if(Type =='New'){
                     if(NewName!=null && NewName!=''){
                         NewOrderType =NewName.substring(0,NewName.lastIndexof(';'));
                     }
                 }
                 if(Type =='Resale'){
                     if(Resale!=null && Resale!=''){
                         ResaleOrderType=Resale.substring(0,Resale.lastIndexof(';'));
                     }
                 }
                 if(Type =='Relocation'){
                     if(Relocation!=null && Relocation!=''){
                         RelocationOrderType=Relocation.substring(0,Relocation.lastIndexof(';'));
                     }
                 }
                 if(Type =='Add-On'){
                     if(AddOn!=null && AddOn!=''){
                         AddOnOrderType=AddOn.substring(0,AddOn.lastIndexof(';'));
                     }
                 }
                 if(Type =='Conversion'){
                     if(Conversion !=null && Conversion!=''){
                         ConversionOrderType=Conversion.substring(0,Conversion.lastIndexof(';'));
                     }
                 }
                 if(Type=='Custom Home'){
                     if(CustomHome!=null && CustomHome!=''){
                        CustomHomeOrderType=CustomHome.substring(0,CustomHome.lastIndexof(';'));
                     }
                 }
                }
            }
            TerritoryID = terrId;
        }
    
    }  
    
}