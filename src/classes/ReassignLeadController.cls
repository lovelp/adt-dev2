public with sharing class ReassignLeadController
{

    //All available sales reps list
    private Map < Id, User > AllAvailableSalesRepsForManager = new Map < Id, User > ();

    public Lead lead {get; set;}

    public boolean isAppt {get; set;}

    public boolean isError {get; set;}

    public string ErrorMessage {get; set;}

    public string Message {get; set;}
    
    //selected salesrep for assignment
    private string selectedSalesRep = null;

    
    //getter for salesrep selected
    public String getSelectedSalesRep()
    {
        return selectedSalesRep;
    }
    
    //setter for salesrep selected
    public void setSelectedSalesRep(String ssr)
    {
        this.selectedSalesRep = ssr;
    }

    public ReassignLeadController()
    {
        isAppt = false;
        if(ApexPages.currentPage().getParameters().get('Id') != null)
        	lead = [Select Name, OwnerId, AssignedBy__c, DateAssigned__c From Lead WHERE Id = : ApexPages.currentPage().getParameters().get('Id')];
        Set<String> rectypes = new Set<String>{RecordTypeDevName.COMPANY_GENERATED_APPOINTMENT, RecordTypeDevName.SELF_GENERATED_APPOINTMENT};
        
        buildAllSalesrepsForAManager();
        if (lead != null)
        	selectedSalesRep = lead.OwnerId;
    }

    public List < SelectOption > getallSalesreps()
    {
        List < SelectOption > SalesReps = new List < SelectOption > ();
        SelectOption anOption;
        User u;
        String Label;
        for (Id uid: AllAvailableSalesRepsForManager.KeySet())
        {
            u = AllAvailableSalesRepsForManager.get(uid);
            if (u.IsActive)
            {
                Label = u.Name;
            }
            anOption = new SelectOption(uid, Label);
            SalesReps.add(anOption);
        }
        return SalesReps;
    }

    //Build all salesreps list and return a string in case of an error, else reutrn Blank
    private String buildAllSalesrepsForAManager()
    {
        //get the user id and the related towns that the user/manager is assigned to
        Id currentUserId = UserInfo.getUserId();
        Id currentUserRoleId = UserInfo.getUserRoleId();
        List < Id > matrixedRoles = new List < Id > ();
        matrixedRoles = Utilities.getMatrixedRolesForManager(currentUserRoleId);
        List < Id > allRoles = new List < Id > ();
        allRoles.add(currentUserRoleId);
        if (matrixedRoles != null)
        {
            allRoles.addAll(matrixedRoles);
        }
        //get all subordinate roles for the user
        List < UserRole > subRoles = [select id, name from UserRole where parentRoleId = : allRoles];
        List < id > allSubordinateRoleIds = new List < Id > ();
        for (UserRole ur: subRoles)
        {
            allSubordinateRoleIds.add(ur.id);
        }

        //get all salesreps for the manager
        List < User > allSubordinateUsers = [select id, name, IsActive from User where UserRoleId in : allSubordinateRoleIds AND IsActive = true];
        for (User u: allSubordinateUsers)
        {
            AllAvailableSalesRepsForManager.put(u.id, u);
        }
        return '';
    }

    public pageReference Save()
    {
        isError = false;
        try
        {
            lead.OwnerId = selectedSalesRep;
            lead.AssignedBy__c=UserInfo.getUserId();
            lead.DateAssigned__c = DateTime.now();
            update lead;
            EmailMessageUtilities.SendEmailNotification(Id.valueOf(selectedSalesRep), 'A lead has been reassigned to you.', 'A lead, ' + lead.Name + ', has been reassigned to you.', URL.getSalesforceBaseUrl().toExternalForm() + '/' + lead.Id, false);
        }
        catch (Exception ex)
        {
            isError = true;
            Message = 'An error occured when changing the Owner.';
        }
        return new pageReference('/' + lead.Id);
    }

    public pageReference Cancel()
    {
        return new pageReference('/' + lead.Id);
    }
}