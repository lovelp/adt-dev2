/************************************* MODIFICATION LOG ********************************************************************************************
* AccountAssignmentBatchProcessor
*
* DESCRIPTION : Reassigns accounts to previous owner if certain conditions are met
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  3/12/2012               - Original Version
* Leonard Alarcon				6/05/2013				- Sprint#11 Change SOQL use OtherDispositionDate__c instead of DispositionDate__c
*														  OtherDispositionDate__c it's now being used to track Owner reassignment
*                                                   
*/

global class AccountAssignmentBatchProcessor implements Database.Batchable<sObject>, Database.Stateful {


    public String query =
            'select Id, OwnerId, PreviousOwner__c, OtherDispositionDate__c, DateAssigned__c, RecordType.DeveloperName, ProcessingType__c, ' +
            'Type, SalesAppointmentDate__c, ScheduledInstallDate__c' + 
            ' from Account where AccountReassigned__c=\'Yes\'';
            //Old SOQL Query
            /*PreviousOwner__c <> null and (DateAssigned__c < LAST_N_DAYS:' + String.escapeSingleQuotes(ResaleGlobalVariables__c.getinstance('DaysBeforeReassignment').value__c) + ' OR DateAssigned__c = null )' +
            ' and (DispositionDate__c < LAST_N_DAYS:' + String.escapeSingleQuotes(ResaleGlobalVariables__c.getinstance('DaysBeforeReassignment').value__c) + ' OR DispositionDate__c = null )' +
            ' and ProcessingType__c <> \'' + IntegrationConstants.PROCESSING_TYPE_USER + '\'' +
            '';*/

    global Database.QueryLocator start( Database.Batchablecontext bc ) {
        system.debug('query = ' + query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        List<Account> accts = (List<Account>)scope;
        List<Account> acctsToUpdate = new List<Account>();
        
        // derive a reference date that determines what constitutes a recent appointment
        Datetime referenceDate = Datetime.now().addDays(-1 * Integer.valueOf(ResaleGlobalVariables__c.getinstance('DaysBeforeReassignment').value__c));
        // later date comparisons will be inequalities so decrement the reference date by one additional day
        referenceDate.addDays(-1);
        
        for (Account a : accts) {
            
            // All Resale and Standard accounts get reset to previous owner but RIF accounts only get reset 
            // if there is no recent or future sales appointment and no recent or future install appointment
            if (a.PreviousOwner__c!=null && (a.Type != IntegrationConstants.TYPE_RIF ||
                (a.Type == IntegrationConstants.TYPE_RIF 
                  && !isRecentScheduledEvent(a.SalesAppointmentDate__c, a.ScheduledInstallDate__c, referenceDate) ) ))
            { 
                // restore ownership
                a.OwnerId = a.PreviousOwner__c;
                //Scenario 8 -Previous Owner set to null in Apex class AccountAssignmentBatchProcessor
                // and clear previous owner to prevent any subsequent expiration later
                a.PreviousOwner__c = null;
                //End Scenario 8
                acctsToUpdate.add(a);
            }   
        }
        update acctsToUpdate;
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
    
    global Boolean isRecentScheduledEvent(Datetime apptDate, Datetime installDate, Datetime referenceDate) {
        
        Boolean returnVal = false;
        if (apptDate != null && Utilities.checkDateOrder(referenceDate, apptDate, false)) {
            returnVal = true;
        }
        if (installDate != null && Utilities.checkDateOrder(referenceDate, installDate, false)) {
            returnVal = true;
        }
        return returnVal;
    }
}