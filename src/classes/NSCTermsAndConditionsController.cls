public class NSCTermsAndConditionsController {
    public String TermsAndConditions_PhoneSales     {get;set;}
    public String TermsAndConditions_FieldSales     {get;set;}
    public Boolean isNSCUser    {get;set;}
    public LoanApplication__c loanApp{get;set;}
    
    public NSCTermsAndConditionsController(ApexPages.StandardController sc) {
        StaticResource fieldSalesTerms;
        //StaticResource phoneSalesTerms;
        this.loanApp = (LoanApplication__c)sc.getRecord();
        system.debug(loanApp.id);
        loanApp = [select CFGEmail__c from LoanApplication__c where id =:loanApp.id];
        User loggedInUser = [Select Id, Profile.Name From User Where Id =: UserInfo.getUserId()];
        isNSCUser = loggedInUser.Profile.Name.contains('NSC') ? true : false;
            
        try {
            fieldSalesTerms = [Select Body From StaticResource Where Name = 'TermsAndConditions_FieldSales'];
            //phoneSalesTerms = [Select Body From StaticResource Where Name = 'TermsAndConditions_PhoneSales'];
        }catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'static resources for Terms & Conditions not found.'));
        }
        
        //Blob fileContent = phoneSalesTerms.Body;
        //TermsAndConditions_PhoneSales = EncodingUtil.base64Encode(fileContent);
        
        //fileContent = EncodingUtil.base64Decode(TermsAndConditions_PhoneSales);
        //TermsAndConditions_PhoneSales = fileContent.toString();
        
        Blob fileContent = fieldSalesTerms.Body;
        // TermsAndConditions_FieldSales = EncodingUtil.base64Encode(fileContent);
        
        fileContent = fieldSalesTerms.Body;
        TermsAndConditions_FieldSales = EncodingUtil.base64Encode(fileContent);
        fileContent = EncodingUtil.base64Decode(TermsAndConditions_FieldSales);
        TermsAndConditions_FieldSales = fileContent.toString();
        TermsAndConditions_FieldSales = TermsAndConditions_FieldSales.replace('xxxxxxxxxx@xxxxx.xxx', loanApp.CFGEmail__c);
    }
}