public class CampaignMemberTriggerHelper{
    
    public static void processCampaignMembersAfterInsert(List<CampaignMember> CampaignMemListAfterInsert){
    
        //updateAccounts(CampaignMemListAfterInsert);
    }
    
   
    
    
    public static void processCampaignMembersAfterDelete(List<CampaignMember> CampaignMemListAfterDelete){
    
        updateAccounts(CampaignMemListAfterDelete);
    }
    
    
    public static void updateAccounts(List<CampaignMember> campMemList){
        Set<id> accountIds=new set<id>();
        //Grab all the associated accounts
        for(CampaignMember cm:campMemList){
            accountIds.add(cm.account__c);
        }
        //query the account and also campaign members in subquery
        List<Account> accList=[select id,name,(select id,expiration_date__c,Campaign.parentId from Campaign_Members__r order by expiration_date__c asc) from Account where id IN:accountIds];
        if(accList.size()>0){
            //loop all the accounts and its campaignmembers to calculate the max expiration date of campaign members
            for(Account acc:accList){
                if(acc.campaign_members__r.size()>0){
                    
                    List<date> dateList=new List<date>();
                    List<CampaignDate> cdList=new List<CampaignDate>();
                    for(CampaignMember cm:acc.Campaign_members__r){
                        if(cm.expiration_date__c!=null){
                            CampaignDate cd=new CampaignDate(cm.Campaign.parentId,cm.expiration_date__c);
                            cdList.add(cd);
                        }
                    }
                    if(cdList.size()>0){
                        //sorting based on expiration date
                        cdList.sort();
                        
                        acc.last_campaign_date_time__c=cdList[cdList.size()-1].expirationDate;
                        system.debug('Expiration Date is '+cdList[cdList.size()-1].expirationDate+' for Account '+acc.name);
                        acc.Last_master_campaign__c=cdList[cdList.size()-1].campaignMasterId;
                    }else{
                        acc.last_campaign_date_time__c=null;
                        acc.Last_master_campaign__c=null;
                    }
                    /*
                    for(CampaignMember cm:acc.Campaign_members__r){
                        system.debug('Expiration Date is '+cm.expiration_date__c+' for Account '+acc.name);
                    }
                    system.debug('Expiration Date is '+acc.Campaign_members__r[0].expiration_date__c+' for Account '+acc.name);
                    acc.last_campaign_date_time__c=acc.Campaign_members__r[0].expiration_date__c;
                    acc.Last_master_campaign__c=acc.Campaign_members__r[0].Campaign.parentId;
                    */
                }else{
                    //assign null if no campaign members
                    acc.last_campaign_date_time__c=null;
                    acc.Last_master_campaign__c=null;
                }
            
            }
        
            //update the acccounts
            try{
                update accList;
            }catch(exception ex){}
        }
    
    
    }
    
    
    
    //comparable to compare dates and return the associated Campaign master id
    public class CampaignDate implements Comparable{
        Id campaignMasterId;
        Date expirationDate;
        
        public campaignDate(Id cpMasterId, Date expDate){
            campaignMasterId=cpMasterId;
            expirationDate=expDate;
        }
        
        public Integer compareTo(Object compareTo) {
            CampaignDate compareToObj = (CampaignDate)compareTo;
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if(compareToObj.expirationDate < expirationDate){
                returnValue = 1;
            }
            else if(compareToObj.expirationDate > expirationDate){
                returnValue = -1;
            }
            return returnValue;
        }
        
        
    }
    
    
    
}