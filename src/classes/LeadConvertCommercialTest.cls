@isTest 
public class LeadConvertCommercialTest {
    static testMethod void testLeadConversion()
    {
        User current = [Select Id from User where Id=:UserInfo.getUserId()];
        Lead l;
        User u;
        Address__c addr = new Address__c();
        addr.Latitude__c = 29.91760000000000;
        addr.Longitude__c = -90.17870000000000;
        addr.Street__c = '56528 E Crystal Ct';
        addr.City__c = 'Westwego';
        addr.State__c = 'LA';
        addr.PostalCode__c = '70094';
        addr.OriginalAddress__c = '56528 E CRYSTAL CT, WESTWEGO, LA, 70094';
        
        ResaleGlobalVariables__c ResaleGlobalVariables = new ResaleGlobalVariables__c(Name = 'DataRecastDisableAccountTrigger', Value__c = 'FALSE');
        ResaleGlobalVariables__c ResaleGlobalVariables1 = new ResaleGlobalVariables__c(Name = 'DataConversion', Value__c = 'FALSE');
        ResaleGlobalVariables__c ResaleGlobalVariables2 = new ResaleGlobalVariables__c(Name = 'CreateContactOnLeadConversion', Value__c = 'true');
        ResaleGlobalVariables__c ResaleGlobalVariables3 = new ResaleGlobalVariables__c(Name = 'GlobalResaleAdmin', Value__c = UserInfo.getName());
        
        insert new List<ResaleGlobalVariables__c>{ResaleGlobalVariables, ResaleGlobalVariables1, ResaleGlobalVariables2, ResaleGlobalVariables3};
        
        test.startTest();
        
        insert addr;
        String dispcomments = 'Test copy to account';
        system.runas(current) {
            Account acct = new Account();
            acct.AddressID__c = addr.Id;
            acct.Name = 'Test Acc';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            //acct.PostalCodeID__c = inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
            acct.ShippingPostalCode = '22102';
            acct.Data_Source__c = 'TEST';
            insert acct;
            
            l = TestHelperClass.createLead(current, false, '');
            l.DispositionComments__c = dispcomments;
            l.PostalCode = '22102';
            l.Business_Id__c = '1100 - Residential';
            l.AddressID__c = addr.id;
            l.SiteStreet__c = '65761';
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.BusinessID__c = l.Business_Id__c.Contains('1100') ? '1100' : '1200';
            pc.Name = l.PostalCode;
            insert pc;
            
            l.PostalCodeID__c = pc.Id;
            insert l;
            
            Task t = new Task(WhoId = l.Id, ActivityDate = date.today());
            insert t;
            
            disposition__c d = new disposition__c();
            d.LeadID__c = l.Id;
            d.DispositionType__c = 'Called';
            insert d;
          
            //LeadConversion lc = new LeadConversion(l.Id);
            //Boolean success = lc.convertLead();
            try{ 
                LeadConvertCommercial.converLead(l.id);
                //LeadConvertCommercial.continueToAccount(acct.Id, l.Id, UserInfo.getUserId());
                LeadConvertCommercial.getrelatedOptyCont(acct.Id);
                //LeadConvertCommercial.assignToManager(l.Id);
                LeadConvertCommercial.runMMB(acct.Id);
                //LeadConvertCommercial.goToMMB(acct.Id);
                //LeadConvertCommercial.saveOptyContact(new List<Opportunity>{new Opportunity(Name = 'Test Oppty', StageName = 'Qualifying', CloseDate = Date.Today())}, new List<Contact>{new Contact(LastName = 'Test Contact', AccountId = acct.Id)}, acct.Id);
                //LeadConvertCommercial.saveOptyContact(new List<Opportunity>(), new List<Contact>(), acct.Id);
                LeadConvertCommercial.returnSortedAccounts(l.Id);
            }catch(exception e){}
            //LeadConvertCommercial.continueToAccount(acct.id,l.id,current.id);
            //LeadConvertCommercial.assignToManager(l.id);
        }
        
        test.stopTest();
        
    }
    
    static testMethod void testLeadConversion1()
    {
        User current = [Select Id from User where Id=:UserInfo.getUserId()];
        Lead l;
        User u;
        Address__c addr = new Address__c();
        addr.Latitude__c = 29.91760000000000;
        addr.Longitude__c = -90.17870000000000;
        addr.Street__c = '56528 E Crystal Ct';
        addr.City__c = 'Westwego';
        addr.State__c = 'LA';
        addr.PostalCode__c = '70094';
        addr.OriginalAddress__c = '56528 E CRYSTAL CT, WESTWEGO, LA, 70094';
        
        ResaleGlobalVariables__c ResaleGlobalVariables = new ResaleGlobalVariables__c(Name = 'DataRecastDisableAccountTrigger', Value__c = 'FALSE');
        ResaleGlobalVariables__c ResaleGlobalVariables1 = new ResaleGlobalVariables__c(Name = 'DataConversion', Value__c = 'FALSE');
        ResaleGlobalVariables__c ResaleGlobalVariables2 = new ResaleGlobalVariables__c(Name = 'CreateContactOnLeadConversion', Value__c = 'true');
        
        insert new List<ResaleGlobalVariables__c>{ResaleGlobalVariables, ResaleGlobalVariables1, ResaleGlobalVariables2};
        
        test.startTest();
        
        insert addr;
        String dispcomments = 'Test copy to account';
        system.runas(current) {
            Account acct = new Account();
            acct.AddressID__c = addr.Id;
            acct.Name = 'Test Acc';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            //acct.PostalCodeID__c = inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
            acct.ShippingPostalCode = '22102';
            acct.Data_Source__c = 'TEST';
            insert acct;
            
            l = TestHelperClass.createLead(current, false, '');
            l.DispositionComments__c = dispcomments;
            l.PostalCode = '22102';
            l.Business_Id__c = '1100 - Residential';
            l.AddressID__c = addr.id;
            l.SiteStreet__c = '65761';
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.BusinessID__c = l.Business_Id__c.Contains('1100') ? '1100' : '1200';
            pc.Name = l.PostalCode;
            insert pc;
            
            l.PostalCodeID__c = pc.Id;
            insert l;
            
            Task t = new Task(WhoId = l.Id, ActivityDate = date.today());
            insert t;
            
            disposition__c d = new disposition__c();
            d.LeadID__c = l.Id;
            d.DispositionType__c = 'Called';
            insert d;
            
            Opportunity oppty = new Opportunity();
            oppty.Name = 'Test Opportunity';
            oppty.StageName = 'Qualified';
            oppty.CloseDate = date.today();
            insert oppty;
            Contact con = new Contact();
            con.lastName = 'TestLastName';
            insert con;
            list<Opportunity> optylist = new list<Opportunity>();
            optylist.add(oppty);
            list<Contact> contlist = new list<Contact>();
            contlist.add(con);
            try{
                //LeadConvertCommercial.saveOptyContact(optylist,contlist,String.valueOf(acct.id));
                LeadConvertCommercial.assignToManager(l.Id);
                LeadConvertCommercial.assignToManager(acct.Id);
                //LeadConvertCommercial.goToMMB(acct.Id);
                
            }catch(exception e){}
        }
        
        test.stopTest();
        
    }
     static testMethod void testLeadConversion2()
    {
        User current = [Select Id from User where Id=:UserInfo.getUserId()];
        Lead l;
        User u;
        Address__c addr = new Address__c();
        addr.Latitude__c = 29.91760000000000;
        addr.Longitude__c = -90.17870000000000;
        addr.Street__c = '56528 E Crystal Ct';
        addr.City__c = 'Westwego';
        addr.State__c = 'LA';
        addr.PostalCode__c = '70094';
        addr.OriginalAddress__c = '56528 E CRYSTAL CT, WESTWEGO, LA, 70094';
        
        ResaleGlobalVariables__c ResaleGlobalVariables = new ResaleGlobalVariables__c(Name = 'DataRecastDisableAccountTrigger', Value__c = 'FALSE');
        ResaleGlobalVariables__c ResaleGlobalVariables1 = new ResaleGlobalVariables__c(Name = 'DataConversion', Value__c = 'FALSE');
        ResaleGlobalVariables__c ResaleGlobalVariables2 = new ResaleGlobalVariables__c(Name = 'CreateContactOnLeadConversion', Value__c = 'true');
        ResaleGlobalVariables__c ResaleGlobalVariables3 = new ResaleGlobalVariables__c(Name = 'GlobalResaleAdmin', Value__c = UserInfo.getName());
        
        insert new List<ResaleGlobalVariables__c>{ResaleGlobalVariables, ResaleGlobalVariables1, ResaleGlobalVariables2, ResaleGlobalVariables3};
        
        test.startTest();
        
        insert addr;
        String dispcomments = 'Test copy to account';
        system.runas(current) {
            Account acct = new Account();
            acct.AddressID__c = addr.Id;
            acct.Name = 'Test Acc';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            //acct.PostalCodeID__c = inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
            acct.ShippingPostalCode = '22102';
            acct.Data_Source__c = 'TEST';
            insert acct;
            
            l = TestHelperClass.createLead(current, false, '');
            l.DispositionComments__c = dispcomments;
            l.PostalCode = '22102';
            l.Business_Id__c = '1100 - Residential';
            l.AddressID__c = addr.id;
            l.SiteStreet__c = '65761';
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.BusinessID__c = l.Business_Id__c.Contains('1100') ? '1100' : '1200';
            pc.Name = l.PostalCode;
            insert pc;
            
            l.PostalCodeID__c = pc.Id;
            insert l;
            
            Task t = new Task(WhoId = l.Id, ActivityDate = date.today());
            insert t;
            
            disposition__c d = new disposition__c();
            d.LeadID__c = l.Id;
            d.DispositionType__c = 'Called';
            insert d;
          
            
            try{ 
                LeadConvertCommercial.converLead(l.id);
               // LeadConvertCommercial.returnSortedAccounts(l.Id);
            }catch(exception e){}
              System.debug('error occured during conversion');
        }
        
        test.stopTest();
        
    }
}