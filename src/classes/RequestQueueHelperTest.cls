@isTest
private class RequestQueueHelperTest {

    static testMethod void testDeleteExpiredItemsMatchCriteria() {
        
        RequestQueue__c rq = new RequestQueue__c();
        rq.ServiceMessage__c = 'TestMessage';
        rq.ServiceTransactionType__c = 'TestServiceTransactionType';
        rq.RequestStatus__c = 'Ready';
        
        Datetime rightNow = Datetime.now();
        rq.RequestDate__c = rightNow.addDays(-120);
         
        insert rq;
        
        TestHelperClass.createReferenceDataForTestClasses();
        
        Integer rqsizebefore = Database.countQuery('Select count() from RequestQueue__c where ServiceTransactionType__c = \'TestServiceTransactionType\'');
        
        test.startTest();
        
        RequestQueueHelper.deleteExpiredItems();
            
        test.stopTest();
        
        Integer rqsizeafter = Database.countQuery('Select count() from RequestQueue__c where ServiceTransactionType__c = \'TestServiceTransactionType\'');
        System.assertEquals(rqsizebefore-1,rqsizeafter, 'Expect one Request Queue item to be deleted');
    }
    
    static testMethod void testDeleteExpiredItemsNoMatch() {
        
        RequestQueue__c rq = new RequestQueue__c();
        rq.ServiceMessage__c = 'TestMessage';
        rq.ServiceTransactionType__c = 'TestServiceTransactionType';
        rq.RequestStatus__c = 'Ready';
        
        Datetime rightNow = Datetime.now();
        // REQUEST QUEUE RETENTION LIMIT IN CUSTOM SETTING ORG DEFAULT IS 8
        rq.RequestDate__c = rightNow.addDays(-7);
         
        insert rq;
        
        TestHelperClass.createReferenceDataForTestClasses();
        
        Integer rqsizebefore = Database.countQuery('Select count() from RequestQueue__c where ServiceTransactionType__c = \'TestServiceTransactionType\'');
        
        test.startTest();
        
        RequestQueueHelper.deleteExpiredItems();
            
        test.stopTest();
        
        Integer rqsizeafter = Database.countQuery('Select count() from RequestQueue__c where ServiceTransactionType__c = \'TestServiceTransactionType\'');
        System.assertEquals(rqsizebefore, rqsizeafter, 'Expect no Request Queue items to be deleted');
    }
    
    
}