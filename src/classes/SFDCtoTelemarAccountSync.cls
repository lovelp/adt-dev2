global class SFDCtoTelemarAccountSync implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    
     
    public Date createdDate=Date.today()-1;
    public String query = 'select Id,name, RequestStatus__c, ServiceTransactionType__c, ServiceMessage__c, AccountID__c, CreatedDate, EventID__c, ResponseScheduleID__c, ResponseTaskCode__c, ResponseTelemarNumber__c, SentDateTime__c, ResponseDateTime__c FROM RequestQueue__c WHERE ServiceTransactionType__c =\'setAppointmentNew-NSC\' and RequestStatus__c =\'Async-Error\' and ResponseTelemarNumber__c=null and createdDate>=:CreatedDate';
    public Integer totalAccountsSuccessful=0;
    public Integer totalRequestQueueItemsReprocessed=0;
    public integer totalreqQueueItems=0;
    /*
    * Uses a SOQL query defined as a public String member variable of the class in order to increase testability
    */
    global Database.QueryLocator start( Database.Batchablecontext bc )
    {
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        List<String> respTelemarNumbers=new List<String>();
        totalreqQueueItems=totalreqQueueItems+scope.size();
        for(RequestQueue__c rq:(List<RequestQueue__c>)scope){
            
                Datetime startTimestamp;
                Datetime endTimestamp;
                system.debug('RQ name:'+rq.name);
                OutgoingAppointmentMessage om = new OutgoingAppointmentMessage();
                om.ServiceMessage = rq.ServiceMessage__c;
                om.EventID = rq.EventID__c;
                try{ 
                    startTimestamp = Datetime.now();
                    om.process();
                    rq.SentDateTime__c = startTimestamp;
                    rq.ResponseDateTime__c = Datetime.now();
                    if (om.Status != null && om.Status.equalsIgnoreCase('error'))
                    {
                        // request was sent and the response received but with some kind of error so mark as such
                        rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCERROR;
                    }
                    else
                    {
                        rq.ResponseScheduleID__c = om.TelemarScheduleID;
                        rq.ResponseTelemarNumber__c = om.TelemarAccountNumber;
                        rq.ResponseTaskCode__c = om.TaskCode;
                        
                        // request was sent and the response received so mark as success
                        rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCSUCCESS;
                        if( !String.isBlank(rq.ResponseTelemarNumber__c) ){
                            respTelemarNumbers.add(rq.ResponseTelemarNumber__c);
                            totalAccountsSuccessful++;
                        }
                    }
                } catch (CalloutException ce) {
                    endTimestamp = Datetime.now();
                    Long durationMs = endTimestamp.getTime() - startTimestamp.getTime();
                    rq.ErrorDetail__c = ce.getMessage() + '  ** DURATION (in ms): ' + durationMs;
                    rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCERROR;
                } catch (IntegrationException ie) {
                    rq.ErrorDetail__c =  ie.getMessage();
                    rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCERROR;
                }
        
        }
        
        system.debug(scope.size());
        update scope;
        
        if( respTelemarNumbers!= null && !respTelemarNumbers.isEmpty() ){
            Map<Id, account> accsMap = new Map<Id, Account>([select id from Account where TelemarAccountNumber__c IN :respTelemarNumbers]);
            if( accsMap != null && !accsMap.isEmpty() ){
                List<RequestQueue__c> rqList = new List<RequestQueue__c> ();
                for(RequestQueue__c rq: [SELECT Id, RequestStatus__c from RequestQueue__c WHERE AccountID__c IN :accsMap.keySet() and RequestStatus__c='Error' order by createddate asc]){
                   rq.RequestStatus__c='Ready';
                   rqList.add(rq);
                }
                update rqList;
                totalRequestQueueItemsReprocessed=totalRequestQueueItemsReprocessed+rqList.size();
            }
        }
        
    }
    
    global void finish(Database.BatchableContext bc)
    {

        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        //can be configured in custom settings
        message.toAddresses = ResaleGlobalVariables__c.getValues('RQProcessingEmailRecepients').value__c.split(';');
        
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id = :bc.getJobId()];  
        String plaintextbody='Total Number of Accounts processed: '+totalreqQueueItems+'\n';
        plaintextbody=plaintextbody+'Number of Accounts successful: '+totalAccountsSuccessful+'\n';
        plaintextbody=plaintextbody+'Number of Accounts errored: '+(totalreqQueueItems-totalAccountsSuccessful)+'\n';
        plaintextbody=plaintextbody+'Number of request queue items linked to successfully processed accounts: '+totalRequestQueueItemsReprocessed+'\n';
        message.plainTextBody = plaintextbody;
        message.subject= 'SFDC to Telemar Failed Accounts';
        Messaging.SingleEmailMessage[] messages = 
            new List<Messaging.SingleEmailMessage> {message};
                 Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: '
                  + results[0].errors[0].message);
        }
        
        ResendFailedRequestsToTelemar rfrt=new ResendFailedRequestsToTelemar();
        DateTime timenow = DateTime.now().addMinutes(5);
        String seconds = '0';
        String minutes = String.valueOf(timenow.minute());
        String hours = String.valueOf(timenow.hour());
        String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
        String jobName = 'Reprocess Errored Dispositions ' + hours + ':' + minutes;
        system.schedule(jobName, sch, rfrt);
        
        

    }
}