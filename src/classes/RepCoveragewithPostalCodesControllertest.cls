/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class RepCoveragewithPostalCodesControllertest {
   
   static testMethod void ShowallTerritorieswithrepCoverage() {        
        integer i=12312334;
        profile p =[select id from profile where Name='ADT NA Sales Representative'];
        
        User u =new user(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='Testrep', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingrepcoverage'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        insert u;
        List<Postal_Code_Aggregate__c> listPca =new List<Postal_Code_Aggregate__c>();
        
        Postal_Code_Aggregate__c Pca = new Postal_Code_Aggregate__c(BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C='111');
        listPca.add(Pca);
 		Postal_Code_Aggregate__c Pca1 = new Postal_Code_Aggregate__c(BUSINESSID__C='1200',TMSUBTOWNID__C='14',TMTOWNID__C='112');        
        listPca.add(Pca1);
        insert listPca;
        Postal_Codes__c pc = new Postal_Codes__c(Name='06010',Area__c='NH/ME/CT/Albany',AreaID__c='100',BusinessID__c='1100',District__c='Central New England',District_Id__c='106',MMB_TownID__c='39',Region__c='NorthEast',Region_Id__c='090',TMSubTownID__c='TER8',TMTownID__c='111');
        insert pc;
        SchedUserTerr__c schusr = new SchedUserTerr__c(Name='Resi Town:111 Sub:TER8',FRIDAY_AVAILABLE_END__C=0,FRIDAY_AVAILABLE_START__C=0,MONDAY_AVAILABLE_END__C=1800,
        MONDAY_AVAILABLE_START__C=1000,ORDER_TYPES__C='New;Relocation;Resale;Custom Home',SATURDAY_AVAILABLE_END__C=0,SATURDAY_AVAILABLE_START__C=0,SUNDAY_AVAILABLE_END__C=0,
        SUNDAY_AVAILABLE_START__C=0,TERRITORY_ASSOCIATION_ID__C='0MI130000008QCRGA2',THURSDAY_AVAILABLE_END__C=0,THURSDAY_AVAILABLE_START__C=0,
        TUESDAY_AVAILABLE_END__C=0,TUESDAY_AVAILABLE_START__C=0,USER__C=u.Id,WEDNESDAY_AVAILABLE_END__C=2100,WEDNESDAY_AVAILABLE_START__C=900,
        BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C ='111');
        insert schusr;
        
        PageReference pageRef = Page.RepCoveragewithPostalcodes;
        Test.setCurrentPage(pageRef);
        RepCoveragewithPostalCodesController repCoverage = new RepCoveragewithPostalCodesController();
        test.startTest();
        repCoverage.getLineofBusiness();
        repCoverage.showRepCoverageforAllTerritories();
        test.stopTest();
    }
    static testMethod void ShowallTerritorieswithNorepCoverage() {
        integer i=12312335;
        profile p =[select id from profile where Name='ADT NA Sales Representative'];
        
        User u =new user(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='Testrep', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingrepcoverage'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        insert u;
        List<Postal_Code_Aggregate__c> listPca =new List<Postal_Code_Aggregate__c>();
        
        Postal_Code_Aggregate__c Pca = new Postal_Code_Aggregate__c(BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C='111');
        listPca.add(Pca);
 		Postal_Code_Aggregate__c Pca1 = new Postal_Code_Aggregate__c(BUSINESSID__C='1200',TMSUBTOWNID__C='14',TMTOWNID__C='112');        
        listPca.add(Pca1);
        insert listPca;
        Postal_Codes__c pc = new Postal_Codes__c(Name='06010',Area__c='NH/ME/CT/Albany',AreaID__c='100',BusinessID__c='1100',District__c='Central New England',District_Id__c='106',MMB_TownID__c='39',Region__c='NorthEast',Region_Id__c='090',TMSubTownID__c='TER8',TMTownID__c='111');
        insert pc;
        SchedUserTerr__c schusr = new SchedUserTerr__c(Name='Resi Town:111 Sub:TER8',FRIDAY_AVAILABLE_END__C=0,FRIDAY_AVAILABLE_START__C=0,MONDAY_AVAILABLE_END__C=1800,
        MONDAY_AVAILABLE_START__C=1000,ORDER_TYPES__C='New;Relocation;Resale;Custom Home',SATURDAY_AVAILABLE_END__C=0,SATURDAY_AVAILABLE_START__C=0,SUNDAY_AVAILABLE_END__C=0,
        SUNDAY_AVAILABLE_START__C=0,TERRITORY_ASSOCIATION_ID__C='0MI130000008QCRGA2',THURSDAY_AVAILABLE_END__C=0,THURSDAY_AVAILABLE_START__C=0,
        TUESDAY_AVAILABLE_END__C=0,TUESDAY_AVAILABLE_START__C=0,USER__C=u.Id,WEDNESDAY_AVAILABLE_END__C=2100,WEDNESDAY_AVAILABLE_START__C=900,
        BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C ='111');
        insert schusr;
        
        PageReference pageRef = Page.RepCoveragewithPostalcodes;
        Test.setCurrentPage(pageRef);
        RepCoveragewithPostalCodesController repCoverage = new RepCoveragewithPostalCodesController();
        
        test.startTest();
        
        repCoverage.getLineofBusiness();
        repCoverage.showAllTerritorieswithNoRepCoverage();
        
        test.stopTest();
    }
    static testMethod void showRepCoverageByTerritory() {
        integer i=12312336;
        profile p =[select id from profile where Name='ADT NA Sales Representative'];
        
        User u =new user(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='Testrep', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingrepcoverage'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        insert u;
        List<Postal_Code_Aggregate__c> listPca =new List<Postal_Code_Aggregate__c>();
        
        Postal_Code_Aggregate__c Pca = new Postal_Code_Aggregate__c(BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C='111');
        listPca.add(Pca);
 		Postal_Code_Aggregate__c Pca1 = new Postal_Code_Aggregate__c(BUSINESSID__C='1200',TMSUBTOWNID__C='14',TMTOWNID__C='112');        
        listPca.add(Pca1);
        insert listPca;
        Postal_Codes__c pc = new Postal_Codes__c(Name='06010',Area__c='NH/ME/CT/Albany',AreaID__c='100',BusinessID__c='1100',District__c='Central New England',District_Id__c='106',MMB_TownID__c='39',Region__c='NorthEast',Region_Id__c='090',TMSubTownID__c='TER8',TMTownID__c='111');
        insert pc;
        SchedUserTerr__c schusr = new SchedUserTerr__c(Name='Resi Town:111 Sub:TER8',FRIDAY_AVAILABLE_END__C=0,FRIDAY_AVAILABLE_START__C=0,MONDAY_AVAILABLE_END__C=1800,
        MONDAY_AVAILABLE_START__C=1000,ORDER_TYPES__C='New;Relocation;Resale;Custom Home',SATURDAY_AVAILABLE_END__C=0,SATURDAY_AVAILABLE_START__C=0,SUNDAY_AVAILABLE_END__C=0,
        SUNDAY_AVAILABLE_START__C=0,TERRITORY_ASSOCIATION_ID__C='0MI130000008QCRGA2',THURSDAY_AVAILABLE_END__C=0,THURSDAY_AVAILABLE_START__C=0,
        TUESDAY_AVAILABLE_END__C=0,TUESDAY_AVAILABLE_START__C=0,USER__C=u.Id,WEDNESDAY_AVAILABLE_END__C=2100,WEDNESDAY_AVAILABLE_START__C=900,
        BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C ='111');
        insert schusr;
        
        PageReference pageRef = Page.RepCoveragewithPostalcodes;
        
        Test.setCurrentPage(pageRef);
        Test.startTest();
        RepCoveragewithPostalCodesController repCoverage = new RepCoveragewithPostalCodesController();
        repCoverage.getLineofBusiness();
        repCoverage.searchPostalCode='';
        repCoverage.SelectedBid='RESI';       
        repCoverage.searchTown ='111';        
        repCoverage.selectedTown =repCoverage.searchTown;
        RepCoveragewithPostalCodesController.findTownID('111','RESI');
        repCoverage.showRepCoverageByTerritory(); 
        
        repCoverage.SelectedBid='RESI'; 
        repCoverage.searchsubTown ='TER8';
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        RepCoveragewithPostalCodesController.findsubtownID('TER8','RESI');
        repCoverage.showRepCoverageByTerritory();
        
        repCoverage.SelectedBid='RESI'; 
        repCoverage.searchTown ='111';
        repCoverage.searchsubTown ='TER8';
        repCoverage.selectedTown =repCoverage.searchTown;
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        RepCoveragewithPostalCodesController.findTownID('111','RESI');
        RepCoveragewithPostalCodesController.findsubtownID('TER8','RESI');
        repCoverage.showRepCoverageByTerritory();
        
        test.stopTest(); 
    }    
    
    
    static testMethod void showRepCoverageByTerritory2() {
        integer i=12312337;
        
        List<Postal_Code_Aggregate__c> listPca =new List<Postal_Code_Aggregate__c>();
        
        profile p =[select id from profile where Name='ADT NA Sales Representative'];
        
        User u =new user(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='Testrep', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingrepcoverage'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        insert u;
        
        Postal_Code_Aggregate__c Pca = new Postal_Code_Aggregate__c(BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C='111');
        listPca.add(Pca);
 		Postal_Code_Aggregate__c Pca1 = new Postal_Code_Aggregate__c(BUSINESSID__C='1200',TMSUBTOWNID__C='14',TMTOWNID__C='112');        
        listPca.add(Pca1);
        insert listPca;
        Postal_Codes__c pc = new Postal_Codes__c(Name='06010',Area__c='NH/ME/CT/Albany',AreaID__c='100',BusinessID__c='1100',District__c='Central New England',District_Id__c='106',MMB_TownID__c='39',Region__c='NorthEast',Region_Id__c='090',TMSubTownID__c='TER8',TMTownID__c='111');
        insert pc;
        SchedUserTerr__c schusr = new SchedUserTerr__c(Name='Resi Town:111 Sub:TER8',FRIDAY_AVAILABLE_END__C=0,FRIDAY_AVAILABLE_START__C=0,MONDAY_AVAILABLE_END__C=1800,
        MONDAY_AVAILABLE_START__C=1000,ORDER_TYPES__C='New;Relocation;Resale;Custom Home',SATURDAY_AVAILABLE_END__C=0,SATURDAY_AVAILABLE_START__C=0,SUNDAY_AVAILABLE_END__C=0,
        SUNDAY_AVAILABLE_START__C=0,TERRITORY_ASSOCIATION_ID__C='0MI130000008QCRGA2',THURSDAY_AVAILABLE_END__C=0,THURSDAY_AVAILABLE_START__C=0,
        TUESDAY_AVAILABLE_END__C=0,TUESDAY_AVAILABLE_START__C=0,USER__C=u.Id,WEDNESDAY_AVAILABLE_END__C=2100,WEDNESDAY_AVAILABLE_START__C=900,
        BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C ='111');
        insert schusr;
        
        PageReference pageRef = Page.RepCoveragewithPostalcodes;
        
        Test.setCurrentPage(pageRef);
        Test.startTest();
        RepCoveragewithPostalCodesController repCoverage = new RepCoveragewithPostalCodesController();
        repCoverage.getLineofBusiness();
        repCoverage.searchPostalCode='';
        repCoverage.SelectedBid='All';       
        repCoverage.searchTown ='111';        
        repCoverage.selectedTown =repCoverage.searchTown;
        RepCoveragewithPostalCodesController.findTownID('111','All');
        repCoverage.showRepCoverageByTerritory(); 
        
        repCoverage.SelectedBid='All'; 
        repCoverage.searchsubTown ='TER8';
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        RepCoveragewithPostalCodesController.findsubtownID('TER8','All');
        repCoverage.showRepCoverageByTerritory();
        
        repCoverage.SelectedBid='All'; 
        repCoverage.searchTown ='111';
        repCoverage.searchsubTown ='TER8';
        repCoverage.selectedTown =repCoverage.searchTown;
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        RepCoveragewithPostalCodesController.findTownID('111','All');
        RepCoveragewithPostalCodesController.findsubtownID('TER8','All');
        repCoverage.showRepCoverageByTerritory();
        
        test.stopTest(); 
    }
    
    static testMethod void showRepCoverageByTerritory1() {
        integer i=12312338;
        profile p =[select id from profile where Name='ADT NA Sales Representative'];
        
        User u =new user(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='Testrep', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingrepcoverage'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        insert u;
        List<Postal_Code_Aggregate__c> listPca =new List<Postal_Code_Aggregate__c>();
        
        Postal_Code_Aggregate__c Pca = new Postal_Code_Aggregate__c(BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C='111');
        listPca.add(Pca);
 		Postal_Code_Aggregate__c Pca1 = new Postal_Code_Aggregate__c(BUSINESSID__C='1200',TMSUBTOWNID__C='14',TMTOWNID__C='112');        
        listPca.add(Pca1);
        insert listPca;
        Postal_Codes__c pc = new Postal_Codes__c(Name='06010',Area__c='NH/ME/CT/Albany',AreaID__c='100',BusinessID__c='1100',District__c='Central New England',District_Id__c='106',MMB_TownID__c='39',Region__c='NorthEast',Region_Id__c='090',TMSubTownID__c='TER8',TMTownID__c='111');
        insert pc;
        SchedUserTerr__c schusr = new SchedUserTerr__c(Name='Resi Town:111 Sub:TER8',FRIDAY_AVAILABLE_END__C=0,FRIDAY_AVAILABLE_START__C=0,MONDAY_AVAILABLE_END__C=1800,
        MONDAY_AVAILABLE_START__C=1000,ORDER_TYPES__C='New;Relocation;Resale;Custom Home',SATURDAY_AVAILABLE_END__C=0,SATURDAY_AVAILABLE_START__C=0,SUNDAY_AVAILABLE_END__C=0,
        SUNDAY_AVAILABLE_START__C=0,TERRITORY_ASSOCIATION_ID__C='0MI130000008QCRGA2',THURSDAY_AVAILABLE_END__C=0,THURSDAY_AVAILABLE_START__C=0,
        TUESDAY_AVAILABLE_END__C=0,TUESDAY_AVAILABLE_START__C=0,USER__C=u.Id,WEDNESDAY_AVAILABLE_END__C=2100,WEDNESDAY_AVAILABLE_START__C=900,
        BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C ='111');
        insert schusr;
        
        PageReference pageRef = Page.RepCoveragewithPostalcodes;
        
        Test.setCurrentPage(pageRef);
        Test.startTest();
        RepCoveragewithPostalCodesController repCoverage = new RepCoveragewithPostalCodesController();
        repCoverage.getLineofBusiness();
        repCoverage.searchPostalCode='';
        repCoverage.SelectedBid='SMB';       
        repCoverage.searchTown ='112';        
        repCoverage.selectedTown =repCoverage.searchTown;
        RepCoveragewithPostalCodesController.findTownID('112','SMB');
        repCoverage.showRepCoverageByTerritory(); 
        
        repCoverage.SelectedBid='SMB'; 
        repCoverage.searchsubTown ='14';
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        RepCoveragewithPostalCodesController.findsubtownID(repCoverage.searchsubTown,'SMB');
        repCoverage.showRepCoverageByTerritory();
        
        repCoverage.SelectedBid='SMB'; 
        repCoverage.searchTown ='112';
        repCoverage.searchsubTown ='14';
        repCoverage.selectedTown =repCoverage.searchTown;
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        RepCoveragewithPostalCodesController.findTownID(repCoverage.searchTown,'SMB');
        RepCoveragewithPostalCodesController.findsubtownID(repCoverage.searchsubTown,'SMB');
        repCoverage.showRepCoverageByTerritory();
        
        test.stopTest(); 
    }
    
    static testMethod void showRepCoverageByTerritory3() {
        integer i=12312339;
        profile p =[select id from profile where Name='ADT NA Sales Representative'];
        
        User u =new user(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='Testrep', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingrepcoverage'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        insert u;
        
        List<Postal_Code_Aggregate__c> listPca =new List<Postal_Code_Aggregate__c>();
        
        Postal_Code_Aggregate__c Pca = new Postal_Code_Aggregate__c(BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C='111');
        listPca.add(Pca);
 		Postal_Code_Aggregate__c Pca1 = new Postal_Code_Aggregate__c(BUSINESSID__C='1200',TMSUBTOWNID__C='14',TMTOWNID__C='112');        
        listPca.add(Pca1);
        insert listPca;
        Postal_Codes__c pc = new Postal_Codes__c(Name='06010',Area__c='NH/ME/CT/Albany',AreaID__c='100',BusinessID__c='1100',District__c='Central New England',District_Id__c='106',MMB_TownID__c='39',Region__c='NorthEast',Region_Id__c='090',TMSubTownID__c='TER8',TMTownID__c='111');
        insert pc;
        SchedUserTerr__c schusr = new SchedUserTerr__c(Name='Resi Town:111 Sub:TER8',FRIDAY_AVAILABLE_END__C=0,FRIDAY_AVAILABLE_START__C=0,MONDAY_AVAILABLE_END__C=1800,
        MONDAY_AVAILABLE_START__C=1000,ORDER_TYPES__C='New;Relocation;Resale;Custom Home',SATURDAY_AVAILABLE_END__C=0,SATURDAY_AVAILABLE_START__C=0,SUNDAY_AVAILABLE_END__C=0,
        SUNDAY_AVAILABLE_START__C=0,TERRITORY_ASSOCIATION_ID__C='0MI130000008QCRGA2',THURSDAY_AVAILABLE_END__C=0,THURSDAY_AVAILABLE_START__C=0,
        TUESDAY_AVAILABLE_END__C=0,TUESDAY_AVAILABLE_START__C=0,USER__C=u.Id,WEDNESDAY_AVAILABLE_END__C=2100,WEDNESDAY_AVAILABLE_START__C=900,
        BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C ='111');
        insert schusr;
        
        PageReference pageRef = Page.RepCoveragewithPostalcodes;
        
        Test.setCurrentPage(pageRef);
        Test.startTest();
        RepCoveragewithPostalCodesController repCoverage = new RepCoveragewithPostalCodesController();
        repCoverage.getLineofBusiness();
        repCoverage.searchPostalCode='';
        repCoverage.SelectedBid='All';       
        repCoverage.selectedTown =repCoverage.searchTown;
        RepCoveragewithPostalCodesController.findTownID(repCoverage.searchTown,'RESI');
        repCoverage.showRepCoverageByTerritory(); 
        
        repCoverage.SelectedBid='SMB'; 
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        RepCoveragewithPostalCodesController.findsubtownID(repCoverage.searchsubTown,'RESI');
        repCoverage.showRepCoverageByTerritory();
        
        repCoverage.SelectedBid='RESI'; 
        repCoverage.selectedTown =repCoverage.searchTown;
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        RepCoveragewithPostalCodesController.findTownID(repCoverage.searchTown,'RESI');
        RepCoveragewithPostalCodesController.findsubtownID(repCoverage.searchsubTown,'RESI');
        repCoverage.showRepCoverageByTerritory();
        
        test.stopTest(); 
    }
    
    static testMethod void showRepCoverageByTerritory_Postalcode() {
        integer i=12312340;
        profile p =[select id from profile where Name='ADT NA Sales Representative'];
        
        User u =new user(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='Testrep', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingrepcoverage'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
            
         insert u;
            
         List<Postal_Code_Aggregate__c> listPca =new List<Postal_Code_Aggregate__c>();
        
        Postal_Code_Aggregate__c Pca = new Postal_Code_Aggregate__c(BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C='111');
        listPca.add(Pca);
 		Postal_Code_Aggregate__c Pca1 = new Postal_Code_Aggregate__c(BUSINESSID__C='1200',TMSUBTOWNID__C='14',TMTOWNID__C='112');        
        listPca.add(Pca1);
        insert listPca;
        Postal_Codes__c pc = new Postal_Codes__c(Name='06010',Area__c='NH/ME/CT/Albany',AreaID__c='100',BusinessID__c='1100',District__c='Central New England',District_Id__c='106',MMB_TownID__c='39',Region__c='NorthEast',Region_Id__c='090',TMSubTownID__c='TER8',TMTownID__c='111');
        insert pc;
        SchedUserTerr__c schusr = new SchedUserTerr__c(Name='Resi Town:111 Sub:TER8',FRIDAY_AVAILABLE_END__C=0,FRIDAY_AVAILABLE_START__C=0,MONDAY_AVAILABLE_END__C=1800,
        MONDAY_AVAILABLE_START__C=1000,ORDER_TYPES__C='New;Relocation;Resale;Custom Home',SATURDAY_AVAILABLE_END__C=0,SATURDAY_AVAILABLE_START__C=0,SUNDAY_AVAILABLE_END__C=0,
        SUNDAY_AVAILABLE_START__C=0,TERRITORY_ASSOCIATION_ID__C='0MI130000008QCRGA2',THURSDAY_AVAILABLE_END__C=0,THURSDAY_AVAILABLE_START__C=0,
        TUESDAY_AVAILABLE_END__C=0,TUESDAY_AVAILABLE_START__C=0,USER__C=u.Id,WEDNESDAY_AVAILABLE_END__C=2100,WEDNESDAY_AVAILABLE_START__C=900,
        BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C ='111');
        insert schusr;
        
        PageReference pageRef = Page.RepCoverage;
        
        Test.setCurrentPage(pageRef);
        
        RepCoveragewithPostalCodesController repCoverage = new RepCoveragewithPostalCodesController();
        test.startTest();
        repCoverage.getLineofBusiness();
        repCoverage.searchPostalCode=pc.Name;
        repCoverage.SelectedBid='Resi';       
        repCoverage.searchTown ='';      
        repCoverage.searchsubTown ='';
        repCoverage.selectedTown =repCoverage.searchTown;
        repCoverage.selectedSubTown =repCoverage.searchsubTown;
        repCoverage.selectedPostalCode =repCoverage.searchPostalCode;
        RepCoveragewithPostalCodesController.findPostalcode(repCoverage.selectedPostalCode,'Resi');
        repCoverage.showRepCoverageByTerritory();
        repCoverage.getsearchTerritoryUsers();
        test.stopTest();
       
    }
    
    static testMethod void repsSchedule_test(){
        integer i=1233341;
        profile p =[select id from profile where Name='ADT NA Sales Representative'];
        
        User u =new user(alias = 'stand', email='standarduser@testorg.com',isactive=true,
            emailencodingkey='UTF-8', lastname='User',firstname='Testrep', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingrepcoverage'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        insert u;
        List<Postal_Code_Aggregate__c> listPca =new List<Postal_Code_Aggregate__c>();
        
        Postal_Code_Aggregate__c Pca = new Postal_Code_Aggregate__c(BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C='111');
        listPca.add(Pca);
 		Postal_Code_Aggregate__c Pca1 = new Postal_Code_Aggregate__c(BUSINESSID__C='1200',TMSUBTOWNID__C='14',TMTOWNID__C='112');        
        listPca.add(Pca1);
        insert listPca;
        Postal_Codes__c pc = new Postal_Codes__c(Name='06010',Area__c='NH/ME/CT/Albany',AreaID__c='100',BusinessID__c='1100',District__c='Central New England',District_Id__c='106',MMB_TownID__c='39',Region__c='NorthEast',Region_Id__c='090',TMSubTownID__c='TER8',TMTownID__c='111');
        insert pc;
        SchedUserTerr__c schusr = new SchedUserTerr__c(Name='Resi Town:111 Sub:TER8',FRIDAY_AVAILABLE_END__C=0,FRIDAY_AVAILABLE_START__C=0,MONDAY_AVAILABLE_END__C=1800,
        MONDAY_AVAILABLE_START__C=1000,ORDER_TYPES__C='New;Relocation;Resale;Custom Home',SATURDAY_AVAILABLE_END__C=0,SATURDAY_AVAILABLE_START__C=0,SUNDAY_AVAILABLE_END__C=0,
        SUNDAY_AVAILABLE_START__C=0,TERRITORY_ASSOCIATION_ID__C='0MI130000008QCRGA2',THURSDAY_AVAILABLE_END__C=0,THURSDAY_AVAILABLE_START__C=0,
        TUESDAY_AVAILABLE_END__C=0,TUESDAY_AVAILABLE_START__C=0,USER__C=u.Id,WEDNESDAY_AVAILABLE_END__C=2100,WEDNESDAY_AVAILABLE_START__C=900,
        BUSINESSID__C='1100',TMSUBTOWNID__C='TER8',TMTOWNID__C ='111');
        
        insert schusr;
        PageReference pageRef = Page.RepCoverage;
        
        Test.setCurrentPage(pageRef);
        RepCoveragewithPostalCodesController repCoverage = new RepCoveragewithPostalCodesController();
        ApexPages.currentPage().getParameters().put('bid', '1100');
        ApexPages.currentPage().getParameters().put('tid', '111');
        ApexPages.currentPage().getParameters().put('stid', 'TER8');
        repCoverage.gettownSubtownpostalcodes();
        repcoverage.getsearchTerritoryUsers();
        
    }
}