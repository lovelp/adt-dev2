/************************************* MODIFICATION LOG ********************************************************************************************
* FinancialHierarchyProximitySearcher
*
* DESCRIPTION : Extends ProximitySearcher and implements search algorithms to find nearby leads and accounts according to the 
*               user's position in the ADT financial hierarchy.  These algorithms apply to Sales Reps and Sales Managers.  Since Sales 
*               Reps are permitted to see accounts and leads owned by other users under certain conditions, the without 
*               sharing keyword and postal code assignments contained the Manager Town object help control data visibility.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani			3/23/2012			- Original Version
*
* Derek Benner                  10/24/2012			- Enhanced to include searches for RIF accounts										
*/

public  without sharing class FinancialHierarchyProximitySearcher extends ProximitySearcher
{
    private enum SObjectType {ACCOUNT, LEAD}
    
    private static final String CLASS_NAME = 'FinancialHierarchyProximitySearcher';
	
	public override String getName() {
		return CLASS_NAME;
	} 
        
    public override List<Lead> findNearbyLeads(String boundingCondition, String endLimit, User currUser, Boolean isSalesRep, Boolean isManager)
    {
        list<Lead> leadList = new list<Lead>();
        
        try
        {
            whereString = getWhereString(currUser, isSalesRep, isManager, SObjectType.LEAD);    
            System.debug('findNearbyLeads.....QUERY...: '+ NEARBY_LEAD_BASE_QUERY + whereString + boundingCondition + ' LIMIT ' + endLimit);                
            leadList =  database.Query(NEARBY_LEAD_BASE_QUERY + whereString + boundingCondition + ' LIMIT ' + endLimit);            
        }
        catch(Exception e)
        {
            System.debug('findNearbyLeads----------Exception occurred: ' + e.getMessage() + ' ' + e.getStackTraceString());         
        }
        return leadList;
    }
    
    public override List<Account> findNearbyAccounts(String boundingCondition, String endLimit, User currUser, Boolean isSalesRep, Boolean isManager)
    {   
        list<Account> accountList = new list<Account>();       
        
        try
        {
            whereString = getWhereString(currUser, isSalesRep, isManager, SObjectType.ACCOUNT);         
            System.debug('findNearbyAccounts.....QUERY...: '+ NEARBY_ACCOUNT_BASE_QUERY + whereString + boundingCondition + ' LIMIT ' + endLimit);      
            accountList =  database.Query(NEARBY_ACCOUNT_BASE_QUERY + whereString + boundingCondition + ' LIMIT ' + endLimit);          
        }
        catch(Exception e)
        {
            System.debug('findNearbyAccounts----------Exception occurred: ' + e.getMessage() + ' ' + e.getStackTraceString());          
        }
        return accountList; 
    }
    
    private String getWhereString(User currUser, Boolean isSalesRep, Boolean isManager, SObjectType sob)
    {
        String wherestr = ' WHERE ';
        
        // CONDITION 1: Owned by the current user
        wherestr += '( (OwnerId = \'' + currUser.Id + '\' )';
        
        // Derive some data needed for other conditions
        List<String> MgrId = new List<String>();
        
        // for managers, already know the user id for the manager
        if(isManager)
        {
            MgrId.add(currUser.Id);
        }
        else if(isSalesRep && currUser.UserRoleId != null)
        {
            // but for sales reps, need to determine the user id for the manager
            // note it is possible for a sales rep to have multiple managers
            for(User usr : Utilities.getManagersForRole(currUser.UserRoleId))
            {
                MgrId.add(usr.Id);  
            }           
        }
        
        // Use the list of manager id(s) to derive towns owned by those manager(s)
        // and the channels for those manager(s)
        List<String> TownIds = new List<String>();
        Set<String> MgrChannelsSet = new Set<String>(); 
        List<String> MgrChannelsList = new List<String>(); 
        String MgrChannelsString = '';
        
        if(MgrId != null && MgrId.size() > 0)
        {
            for(ManagerTown__c MGT : [SELECT TownID__c, ManagerID__c, Type__c
                                          From ManagerTown__c 
                                              WHERE ManagerID__c IN :MgrId])
            {
                if(MGT.TownID__c != null && MGT.TownID__c != '')
                {
                    TownIds.add(MGT.TownID__c);
                }    
                if(isManager && MGT.Type__c != null && MGT.Type__c != '')
                {
                	for(String str : MGT.Type__c.split(';',0))
                	{
                		if(str != '')
                		{
                			MgrChannelsSet.add(str);
                		}
                	}
                }           
            }
        } 
        
        if(!MgrChannelsSet.isEmpty() && isManager)
        {
        	MgrChannelsList.addAll(MgrChannelsSet);        	
        	
        	for(String s : MgrChannelsList)
        	{        		  
	            MgrChannelsString += '\'' + s + '\',';	            
	        }
	        
	        if( MgrChannelsString != '')
	        {
	        	MgrChannelsString =  '(' + MgrChannelsString.substring(0,MgrChannelsString.lastIndexOf(',')) + ')';	
	        }        	        
        }
        
        // several conditions in the where clause apply in different contexts
        // construct those conditions once here then use as applicable later
        String whereAffiliations = getAffiliationsWhereString(currUser, isSalesRep, isManager, sob);
        String whereChannels = getChannelsWhereString(currUser, isSalesRep, isManager, sob, MgrChannelsString);
        String whereNotUserEntered = getUserEnteredRecordWhereString(sob);
        String whereTown = getTownWhereString(TownIds, sob);  
        
        
        // CONDITION 2: Unassigned which equates to being owned by the current user's manager(s)
        // results are also dependent on the current user's credentials matching the account/lead's affiliation
        // and the current user's business unit matching the account/lead's channel 
        if(isSalesRep && MgrId != null && MgrId.size() > 0)
        {               
        	Integer countOfCriteria=0;
           	String MgrsString = '(';
          	for (String s: MgrId)
          	{
          		if (countOfCriteria!=0) 
               	{
               		MgrsString += ', ';
              	}   
                MgrsString += '\''+s+'\'';
              	countOfCriteria++;
         	}
            MgrsString += ')';
            
          	wherestr += ' OR ( OwnerId IN ' + MgrsString + ' ';
          	wherestr += whereAffiliations;
        	wherestr += whereChannels;
        	wherestr += whereNotUserEntered;
        	wherestr += whereTown; 
        	wherestr += ' )';                         
      	}
            
        // CONDITION 3: RIF accounts
      	// results are also dependent on the current user's credentials matching the account's affiliation
        // and the current user's business unit matching the account's channel
        // always applied for SB users and based on configuration for Resi users 
        if (sob == SObjectType.ACCOUNT && shouldSearchRif(currUser)) {
        	wherestr += ' OR (';
        	wherestr += ' OwnerId = \'' + Utilities.getRIFAccountOwner() + '\'';
        	wherestr += whereAffiliations;  
            if(isManager){            	
            	wherestr += getChannelsWhereString(currUser, isSalesRep, isManager, sob, MgrChannelsString, true);            	
            }
            else{
            	wherestr += whereChannels;
            }        	
        	wherestr += ' ) '; 
        }
                              
        // CONDITION 4: Accounts/Leads from peers without recent activity meaning no recent disposition action, 
        // no recent assignment to its current owner, and no recent sales appointment, and not a RIF account (identified
        // as an account owned by the generic ADT user) 
        // results are also dependent on the current user's credentials matching the account's affiliation
        // and the current user's business unit matching the account's channel 
        // and the account/lead being not originating as a SFDC entered lead
        // and the account/lead being associated with a town owned by the current user's manager
        if(isSalesRep || isManager)
        {
            ProximitySearchSettings__c pss = ProximitySearchSettings__c.getInstance(currUser.ProfileId);
            String days = String.escapeSingleQuotes(String.valueOf(Integer.valueof(pss.DaysBeforeAvailibility__c)));
                                  
            // Disposition Date filter      
            if (sob == SObjectType.LEAD || sob == SObjectType.ACCOUNT)
            {       
                wherestr += ' OR ( ';     
                wherestr += '( DispositionDate__c < LAST_N_DAYS:' + days + ' OR DispositionDate__c = null)';
            }
            
            if(sob == SObjectType.LEAD)
            {
                wherestr += ' AND ( DateAssigned__c = null OR DateAssigned__c < LAST_N_DAYS:' + days + ' )';                 
            }
            
            if (sob == SObjectType.ACCOUNT)
            {       
                wherestr += ' AND (DateAssigned__c = null OR  DateAssigned__c < LAST_N_DAYS:' + days + ' )';
                wherestr += ' AND ( SalesAppointmentDate__c = null OR SalesAppointmentDate__c < LAST_N_DAYS:' + days + ')';
                wherestr += ' AND OwnerId != \'' + Utilities.getRIFAccountOwner() + '\'';                                                  
            }       
               
            wherestr += whereAffiliations;
	        wherestr += whereChannels;
	     	wherestr += whereNotUserEntered;
	        wherestr += whereTown;
	        wherestr += ') ';
	              
        }           
     
     	wherestr += ' ) ';
     
     	if(sob == SObjectType.ACCOUNT)
        {
        	String ClosedValues = '';
            
            List<String> invalidValues = new list<String>();
            invalidValues.addAll(IntegrationConstants.STATUS_CLOSED_VALUES);
            
            if(!invalidValues.isEmpty())
            {
            	ClosedValues = '(';
	            for(String s : invalidValues)
	            {
	            	ClosedValues += '\'' + s + '\',';	            		
	            }
	            ClosedValues = ClosedValues.substring(0, ClosedValues.lastIndexOf(',')) + ')';
            }
            
        	if(ClosedValues != '')
            {
            	wherestr += ' AND ( AccountStatus__c NOT IN '+ ClosedValues +')' ;
            } 
        }       
                
        // Accounts must be Active
        if (sob == SObjectType.ACCOUNT)
        {                       
            wherestr += ' AND LeadStatus__c = \'Active\'';
        }
        // Leads should not have been converted to Accounts
        else if (sob == SObjectType.LEAD)
        {
            wherestr += ' AND IsConverted = false';
        }
                        
        wherestr += ' AND ';        
        return wherestr;
    }
  
    private String getAffiliationsWhereString(User currUser, Boolean isSalesRep, Boolean isManager, SObjectType sob) {
    	
    	String wherestr = '';
    	if(isSalesRep && (currUser.Qualification__c == null || (currUser.Qualification__c != null && !currUser.Qualification__c.contains(IntegrationConstants.AFFILIATION_USAA))))
	    {
	    	wherestr += ' AND ( Affiliation__c != \'' + IntegrationConstants.AFFILIATION_USAA + '\' )';           
	    }
	    
	    return wherestr;
    	
    } 
    
    private String getChannelsWhereString(User currUser, Boolean isSalesRep, Boolean isManager, SObjectType sob, String MgrChannelsString) {
    	return getChannelsWhereString(currUser, isSalesRep, isManager, sob, MgrChannelsString, false);
    }
    
    private String getChannelsWhereString(User currUser, Boolean isSalesRep, Boolean isManager, SObjectType sob, String MgrChannelsString, Boolean isSearchRIF) {
    	
    	String wherestr = '';
    	if( isSalesRep && (sob == SObjectType.LEAD || sob == SObjectType.ACCOUNT) && currUser.Business_Unit__c != null)
	    {
	    	wherestr += ' AND ';
	            
	      	if(ResaleGlobalVariables__c.getinstance('AllowResiResaleAndResiDirect').value__c == 'true' && currUser.Business_Unit__c == Channels.TYPE_RESIDIRECT )
	       	{
	        	wherestr += '( Channel__c = \'' + Channels.TYPE_RESIDIRECT + '\' OR Channel__c = \'' + Channels.TYPE_RESIRESALE + '\' )';
	        } 
	        else if(ResaleGlobalVariables__c.getinstance('AllowResiResaleAndResiDirect').value__c == 'true' && currUser.Business_Unit__c == Channels.BUSINESS_UNIT_RESIEXISTINGBUSINESS )
	       	{
	        	wherestr += '( Channel__c = \'' + Channels.TYPE_RESIDIRECT + '\' OR Channel__c = \'' + Channels.TYPE_RESIRESALE + '\' )';
	        }          
	      	else if(currUser.Business_Unit__c == Channels.TYPE_RESIDIRECT)
	        {
	         	wherestr += ' Channel__c = \'' + Channels.TYPE_RESIDIRECT + '\'';
	        }
	        else if(currUser.Business_Unit__c == Channels.BUSINESS_UNIT_RESIEXISTINGBUSINESS)
	        {
	         	wherestr += ' Channel__c = \'' + Channels.TYPE_RESIDIRECT + '\'';
	        }             
	       	else if(currUser.Business_Unit__c == Channels.TYPE_RESIRESALE)
	       	{
	         	wherestr += ' Channel__c = \'' + Channels.TYPE_RESIRESALE + '\'';
	       	} 
	      	else if(currUser.Business_Unit__c == Channels.TYPE_SBRESALE || currUser.Business_Unit__c == Channels.TYPE_SBDIRECT)
	      	{
	         	wherestr += '( Channel__c = \'' + Channels.TYPE_SBRESALE + '\' OR Channel__c = \'' + Channels.TYPE_SBDIRECT + '\' )';
	      	} 
	      	else if(currUser.Business_Unit__c == Channels.TYPE_CUSTOMHOMESALES)
	       	{
	         	wherestr += ' Channel__c = \'' + Channels.TYPE_CUSTOMHOMESALES + '\'';
	      	}          
	      	else if(currUser.Business_Unit__c == Channels.TYPE_HOMEHEALTH)
	      	{
	          	wherestr += ' Channel__c = \'' + Channels.TYPE_HOMEHEALTH + '\'';
	        }                       
	 	} 
	  	else if( isManager && (sob == SObjectType.LEAD || sob == SObjectType.ACCOUNT) && MgrChannelsString != '')
	  	{	    	
	    	if(isSearchRIF){
	    		if(MgrChannelsString.indexOfIgnoreCase(Channels.TYPE_RESIDIRECT)==-1){
	    			MgrChannelsString = MgrChannelsString.replace(')',',') + '\''+ Channels.TYPE_RESIDIRECT +'\')';  
	    		}
	    	}
	    	wherestr += ' AND Channel__c IN ' + MgrChannelsString;
	  	} 
    	
    	return wherestr;
    }


    private String getUserEnteredRecordWhereString(SObjectType sob) {
    	
    	String wherestr = '';
    	if(sob == SObjectType.LEAD)
        {
	    	wherestr += ' AND ( LeadSource <> \'' + 'User Entered' +  '\' )'; 
        }
	   	if (sob == SObjectType.ACCOUNT)
       	{
       		wherestr += ' AND ( ProcessingType__c <> \'' + IntegrationConstants.PROCESSING_TYPE_USER +  '\' )'; 
      	}
        return wherestr;
    	
    }
    
    private String getTownWhereString(List<String> TownIds, SObjectType sob) {
    	
    	String wherestr = '';
    	 
        if(TownIds != null && TownIds.size() > 0)
        {   
        	if (sob == SObjectType.ACCOUNT)
            {               
                wherestr += ' AND ResaleTownNumber__c IN (' ;
            }
            else if(sob == SObjectType.LEAD)
            {
                wherestr += ' AND TownNumber__c IN (' ;
            }
            
            Integer countOfCriteria=0;
            
            for (String s: TownIds)
            {
                if (countOfCriteria!=0) 
                {
                    wherestr += ', ';
                }   
                wherestr += '\''+s+'\'';
                countOfCriteria++;
            }
            wherestr += ')';                                       
        }
        return wherestr;
    	
    }
    
    private Boolean shouldSearchRIF(User u) {
    	
    	Boolean returnValue = true;
    	if (u.Business_Unit__c != null) {
    		if (u.Business_Unit__c == Channels.TYPE_RESIDIRECT || u.Business_Unit__c == Channels.TYPE_RESIRESALE) {
    			if (ResaleGlobalVariables__c.getInstance('AllowResiRIFProximitySearch').Value__c == 'false') {
    				returnValue = false;
    			}
    		}		
    	} else {
    		returnValue = false;
    	}
    	return returnValue;
    }  
}