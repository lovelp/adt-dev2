/************************************* MODIFICATION LOG ********************************************************************************************
* ProfileHelper
*
* DESCRIPTION : Defines utility methods related to Profiles
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover				 1/19/2012			- Original Version
*
*													
*/

public class ProfileHelper {

	public static final Set<String> MANAGER_PROFILE_NAMES = new Set<String>{
		'ADT NA Sales Manager',
		'ADT Business Sales Manager',
		'ADT NSC DLL Sales Manager',
		'ADT Commercial Sales Manager'
	};
	
	public static final Set<String> SALESREP_PROFILE_NAMES = new Set<String>{
		'ADT NA Sales Representative',
		'ADT Commercial Representative'
	};
	
	public static final Set<String> SALESEXEC_PROFILE_NAMES = new Set<String>{
		'ADT NA Sales Executive',
		'ADT NA Sales Exec'
	};

	public static final Set<String> SYSADMIN_PROFILE_NAMES = new Set<String>{
		'System Administrator'
	};
	
	public static final Set<String> INTEGRATION_PROFILE_NAMES = new Set<String>{
		'ADT Integration User'
	};
	
	public static final Set<String> LIMITED_ACCESS_PROFILE_NAMES = new Set<String>{
		'Limited Access'
	};
	
	/** CHECK FOR MANAGER **/
	
	public static Boolean isManager()
	{
		try
		{
			Profile p = [Select Id, Name from Profile where Id = :UserInfo.getProfileId()];
			return isManager(p.Name);
		}
		catch (Exception ex)
		{
			return false;
		}
	}

	public static Boolean isManager(String profilename)
	{
		return Utilities.contains( MANAGER_PROFILE_NAMES, profilename );
	}
	
	public static Boolean isIntegrationUser()
	{
		try
		{
			Profile p = [Select Id, Name from Profile where Id = :UserInfo.getProfileId()];
			return isIntegrationUser(p.Name);
		}
		catch (Exception ex)
		{
			return false;
		}
	}
	
	public static Boolean isIntegrationUser(String profilename)
	{
		return Utilities.contains( INTEGRATION_PROFILE_NAMES, profilename );
	}
	
	/** CHECK FOR SALES REP **/
	
	public static Boolean isSalesRep()
	{
		try
		{
			Profile p = [Select Id, Name from Profile where Id = :UserInfo.getProfileId()];
			return isSalesRep(p.Name);
		}
		catch (Exception ex)
		{
			return false;
		}
	}
	
	public static Boolean isSalesRep(String profilename)
	{
		return Utilities.contains( SALESREP_PROFILE_NAMES, profilename );
	}
	
	
	/** CHECK FOR SALES EXEC **/
	
	public static Boolean isSalesExec()
	{
		try
		{
			Profile p = [Select Id, Name from Profile where Id = :UserInfo.getProfileId()];
			return isSalesExec(p.Name);
		}
		catch (Exception ex)
		{
			return false;
		}
	}
	
	public static Boolean isSalesExec(String profilename)
	{
		return Utilities.contains( SALESEXEC_PROFILE_NAMES, profilename );
	}
	
	
	/** CHECK FOR SYS ADMIN **/
	
	public static Boolean isSysAdmin()
	{
		try
		{
			Profile p = [Select Id, Name from Profile where Id = :UserInfo.getProfileId()];
			return isSysAdmin(p.Name);
		}
		catch (Exception ex)
		{
			return false;
		}
	}
	
	public static Boolean isSysAdmin(String profilename)
	{
		return Utilities.contains( SYSADMIN_PROFILE_NAMES, profilename );
	}
	
	/** CHECK FOR LIMITED ACCESS USER **/
	
	public static Boolean isLimitedAccessUser() {
		try
		{
			Profile p = [Select Id, Name from Profile where Id = :UserInfo.getProfileId()];
			return isLimitedAccessUser(p.Name);
		}
		catch (Exception ex)
		{
			return false;
		}
	}
	
	public static Boolean isLimitedAccessUser(String profilename) {
		return Utilities.contains( LIMITED_ACCESS_PROFILE_NAMES, profilename);
	}
	
	
	/** GET ALL METHODS **/
	
	public static List<User> getAllManagers()
	{
		return new List<User>([
			select Id, Name, isActive from User where Profile.Name IN :ProfileHelper.MANAGER_PROFILE_NAMES order by Name
		]);
	}
	
	public static List<User> getAllActiveManagers()
	{
		return new List<User>([
			select Id, Name from User where Profile.Name IN :ProfileHelper.MANAGER_PROFILE_NAMES and isActive = true order by Name
		]);
	}	
	
	public static List<User> getAllManagers(List<Id> uids)
	{
		return new List<User>([
			select Id, Name from User where Profile.Name IN :ProfileHelper.MANAGER_PROFILE_NAMES and Id IN :uids order by Name
		]);
	}
	
	public static List<User> getAllManagers(List<User> users)
	{
		return new List<User>([
			select Id, Name from User where Profile.Name IN :ProfileHelper.MANAGER_PROFILE_NAMES and Id IN :users order by Name
		]);
	}
}