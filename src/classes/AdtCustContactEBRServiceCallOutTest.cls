@isTest class AdtCustContactEBRServiceCallOutTest {
	static final Integer MAX_EWC_LIMIT = 5;
    static final String SERVICE_TRANSACTION_TYPE = 'addEBRInquiry';
    
    @testSetup static void setup() {
    	RequestQueue__c request = new RequestQueue__c(
            RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY,  
            Counter__c = MAX_EWC_LIMIT,
            ServiceTransactionType__c = SERVICE_TRANSACTION_TYPE
		);   
        
        insert request;
    }
    
    @isTest static void testDoPossibleNowGryphonCallout() {
        RequestQueue__c request = getRequest();
        
        AdtCustContactEBRServiceCallOut.doPossibleNowGryphonCallout(request);
    }
    
    static RequestQueue__c getRequest() {
        return [
            SELECT Id, RequestStatus__c, ErrorDetail__c, Counter__c
            FROM RequestQueue__c
        ][0];
    }
}