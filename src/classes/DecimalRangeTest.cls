/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : DecimalRangeTest is a test class for DecimalRange.apxc
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Jason Pinlac      07/12/2016      - Origininal Version
*
*                           
*/
@isTest
private class DecimalRangeTest {
    
    // Generate Data
    public String errorMessageTest = 'illegal argument: null number';
    
    // Test Methods
    static testMethod void oneDecimalNumberNullTest() {
        try {
             DecimalRange dr = new DecimalRange(NULL); 
        } catch (IllegalArgumentException e) {}   
    }
    
    static testMethod void oneDecimalNumberTest() {
        DecimalRange dr = new DecimalRange(100.0);  
    }
    
    static testMethod void twoDecimalNumberNullTest() {
        try {
             DecimalRange dr = new DecimalRange(NULL, 100.0); 
        } catch (IllegalArgumentException e) {}     
    }
    
     static testMethod void twoDecimalNumberTestCase1() {
             DecimalRange dr = new DecimalRange(100.0, 200.0);     
    }
    
       static testMethod void twoDecimalNumberTestCase2() {
             DecimalRange dr = new DecimalRange(200.0, 100.0);     
    }
    
    static testMethod void otherFunctionalityTest() {
        DecimalRange dr = new DecimalRange(100.0, 200.0);
        Decimal min = dr.min();
        Decimal max = dr.max();
        String toString = dr.toAString();
        Boolean containsBool = dr.contains(50.0);
        Boolean containsBool2 = dr.contains(new DecimalRange(125.0,150.0));
        Boolean overlapsBool = dr.overlaps(new DecimalRange(125.0,150.0));
    }
}