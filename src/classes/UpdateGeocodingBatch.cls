/************************************* MODIFICATION LOG ********************************************************************************
 *
 * DESCRIPTION : UpdateGeocodingBatch is a batch class to update Geocodes for Addresses that are in the Geocoding Queue, then delete them.
 *
 *---------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER            DATE          REASON
 *---------------------------------------------------------------------------------------------------------------------------------------
 * Erin McGee      07/08/2013    - Original Version
 *
 *
 */
global class UpdateGeocodingBatch implements Database.Batchable < sObject > , Database.AllowsCallouts, Schedulable
{

    private final List < Geocoding__c > geocodeDelete;
    public boolean testFlag;
    public String query = 'Select Id, AddressToBeGeocoded__c From Geocoding__c WHERE IsBeingProcessed__c=false limit 3000';
    public String queryTest = 'Select Id, AddressToBeGeocoded__c From Geocoding__c WHERE IsBeingProcessed__c=false limit 1';

    global UpdateGeocodingBatch()
    {
        if (BatchJobHelper.canThisBatchRun(BatchJobHelper.UpdateGeocodingBatch))
        {
            geocodeDelete = new List < Geocoding__c > ();
            for (Geocoding__c g: Database.query(query))
            {
                geocodeDelete.add(g);
            }
        }

    }
    global void execute(SchedulableContext SC)
    {
        UpdateGeocodingBatch z = new UpdateGeocodingBatch();
        z.testflag=false;
        if (BatchJobHelper.canThisBatchRun(BatchJobHelper.UpdateGeocodingBatch))
        {
            Database.executeBatch(z, 1);
        }

        DateTime timenow = system.now().addMinutes(5);
        String seconds = String.valueOf(timenow.second()); //Execute at Zero Seconds
        String minutes = String.valueOf(timenow.minute()); //Execute at every 10th minute of hour
        String hours = String.valueOf(timenow.hour()); // Execute Every Hour
        String sch = seconds + ' ' + minutes + ' * * * ?';
        String jobName = 'Geocode New Addresses' + hours + ':' + minutes;
        system.schedule(jobName, sch, z);
        system.abortJob(sc.getTriggerId());
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        if (!testFlag)
        {
            return Database.getQueryLocator(query);
        }
        else
        {
            return Database.getQueryLocator(queryTest);
        }
    }

    global void execute(Database.BatchableContext BC, List < sObject > scope)
    {
        Set < Id > addressIds = new Set < Id > ();
        List < Geocoding__c > Geocodes = new List < Geocoding__c > ();
        for (Geocoding__c g: (List < Geocoding__c > )(scope))
        {
            addressIds.add(g.AddressToBeGeocoded__c);
            g.IsBeingProcessed__c = true;
            Geocodes.add(g);
        }


        system.debug(LoggingLevel.info, addressIds);
        List < Address__c > addressestoBeGeocoded = [Select Id, Latitude__c, Longitude__c, StandardizedAddress__c, ReProcessGeoCode__c From Address__c Where ID IN: addressIds];

        GoogleGeoCodeResults result;
        List < Address__c > addressesToUpdate = new List < Address__c > ();
        system.debug(LoggingLevel.info, addressestoBeGeocoded);
        for (Address__c address: addressestoBeGeocoded)
        {
            try
            {
                result = new GoogleGeoCodeResults();
                result = GoogleGeoCode.GeoCodeAddress(address.StandardizedAddress__c);
                system.debug(LoggingLevel.info, result);
                address.Latitude__c = decimal.valueOf(result.AddressLat);
                address.Longitude__c = decimal.valueOf(result.AddressLon);
                address.StandardizedAddress__c = result.FormattedAddress;
                address.ReProcessGeoCode__c = false;
                addressesToUpdate.add(address);
            }
            catch (Exception ex)
            {
                system.debug(LoggingLevel.Info, ex.getMessage());
            }
        }
        if (!addressesToUpdate.isEmpty())
        {
            update addressesToUpdate;
        }
        update Geocodes;
    }

    global void finish(Database.BatchableContext BC)
    {
        if (!geocodeDelete.isEmpty())
        {
            delete geocodeDelete;
        }
    }

}