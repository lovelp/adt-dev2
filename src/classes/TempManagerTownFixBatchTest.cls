/**
 Description- This test class used for TempManagerTownFixBatch.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class TempManagerTownFixBatchTest {
    
     static testMethod void testBatch() {
     
         UserRole rl = [select id from UserRole where name = 'South Central Resi SM - D320Mgr' limit 1];
         Profile pl = [select id from Profile where name = 'ADT NA Sales Manager'];
         
         User u = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pl.Id, userroleid= rl.id,
            timezonesidkey='America/Los_Angeles', username='testing_tempmgr'+'@adt.com', EmployeeNumber__c='T1' ,
            StartDateInCurrentJob__c = Date.today());
        
         insert u;
         
         system.runAs(u){
         
         test.startTest();
         List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
         rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
         rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));

         insert rgvList;
         
         Account a2 = TestHelperClass.createAccountData(Date.today(), '1200 - Small Business');
         TempManagerTownFixBatch  b= new TempManagerTownFixBatch();
         Database.executeBatch(b);
         
         test.stopTest();
         }
     }
}