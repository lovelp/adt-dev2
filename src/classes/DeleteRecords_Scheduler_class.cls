global class DeleteRecords_Scheduler_class implements Schedulable{

    public static String sched = '0 0 0 * * ?';  //Every Day at Midnight 

    /*global static String scheduleMe() {
        DeleteRecords_Scheduler_class del = new DeleteRecords_Scheduler_class(); 
        return System.schedule('Cleanup', sched, del);
    }*/

    global void execute(SchedulableContext sc) {
        DeleteRecordsBatchClass cleanup = new DeleteRecordsBatchClass();
        ID batchprocessid = Database.executeBatch(cleanup,2000);           
    }
}