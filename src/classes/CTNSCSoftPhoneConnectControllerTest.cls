/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CTNSCSoftPhoneConnectControllerTest {

    static testMethod void myUnitTest() {
       	CTNSCSoftPhoneConnectController csft = new CTNSCSoftPhoneConnectController();
        csft.DNIS = '1234567';
        csft.ANI = '123456';
        csft.selectedChannel = 'resi direct sales';
        String recordtypeid = csft.LeadRecordTypeId;
    	String CTIJsonStr = '{"ucid": "00001093721447347519","apn": "8888777" , "vdn": "9785625423",  "ani": "9403215632",  "status": "FOUND",  "sqlerror": "0",  "isamerror": "0",  "unixtime": "1438349923",  "unit": "3453",  "line": "061",  "calldate": "07312015",  "calltime": "093843",  "duration": "48",  "apn": "8112912516",  "dialednumber": "2912516",  "appclient": "adt",  "appname": "custcare",  "adtaccnt": "62433456",  "sitetype": "R",  "payamount": "0.00",  "transferterm": "4560125295",  "picstatus": "NA",  "identifiedby": "ANI",  "callerintent": "business",  "transfertype": "business",  "transferpoint": "64",  "lastprompt": "prompt_NLCollection",  "utterance": "technical support",  "emergencycontact": "No",  "pendingpayment": "39.51",  "pastdueamount": "0.00",  "totalamountdue": "39.51",  "nltag": "TECH_SUPPORT" }';
        String startCallResultMessage = CTNSCSoftPhoneConnectController.startCall('8112912516', '9403215632', 'Resi Direct sales', CTIJsonStr);
    }
    
     static testMethod void myUnitTest1() {
       	CTNSCSoftPhoneConnectController csft = new CTNSCSoftPhoneConnectController();
        csft.DNIS = '1234567';
        csft.ANI = '123456';
        csft.selectedChannel = 'resi direct sales';
    	String CTIJsonStr = '{"ucid": "00001093721447347519","apn": "8888777",  "ani": "9403215632",  "status": "FOUND",  "sqlerror": "0",  "isamerror": "0",  "unixtime": "1438349923",  "unit": "3453",  "line": "061",  "calldate": "07312015",  "calltime": "093843",  "duration": "48",  "apn": "8112912516",  "dialednumber": "2912516",  "appclient": "adt",  "appname": "custcare",  "adtaccnt": "62433456",  "sitetype": "R",  "payamount": "0.00",  "transferterm": "4560125295",  "picstatus": "NA",  "identifiedby": "ANI",  "callerintent": "business",  "transfertype": "business",  "transferpoint": "64",  "lastprompt": "prompt_NLCollection",  "utterance": "technical support",  "emergencycontact": "No",  "pendingpayment": "39.51",  "pastdueamount": "0.00",  "totalamountdue": "39.51",  "nltag": "TECH_SUPPORT" }';
        String startCallResultMessage = CTNSCSoftPhoneConnectController.startCall('8112912516', '9403215632', 'Resi Direct sales', CTIJsonStr);
    }
}