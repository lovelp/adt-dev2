/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : TermsAndConditionsResetTest is a test class for TermsAndConditionsReset.cls
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Erin McGee      07/08/2013      - Original Version
*
*                           
*/
@isTest
private class TermsAndConditionsResetTest 
{
	
	static testMethod void TermsAndConditionsResetTest() 
	{
		User sru=TestHelperClass.createSalesRepUser();
		sru.Terms_and_Conditions_Accepted__c=Date.today();
		update sru;

		TermsAndConditionsReset tcr= new TermsAndConditionsReset();
		tcr.query='Select Id, Terms_and_Conditions_Accepted__c From User Where Terms_and_Conditions_Accepted__c!=null limit 1';
		test.startTest();
			Database.executeBatch(tcr);
		test.stopTest();

	}
	
}