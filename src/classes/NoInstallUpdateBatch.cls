/************************************* MODIFICATION LOG ********************************************************************************************
* NoInstallUpdate
*
* DESCRIPTION : This batch changes the status of an Account to NIO if it is in SOLD status, it is more than x days from the Sale Date (TransitionDate3)
*				and if there is no future dated install appointment.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Michael Tuckman          		2/1/2013			- Original Version
*
*													
*/
global class NoInstallUpdateBatch implements Database.Batchable<sObject>, Database.Stateful {
	
	private String niuDays = String.escapeSingleQuotes(ResaleGlobalVariables__c.getinstance('NoInstallUpdateDays').value__c);
	
	public String query = 'Select Id, AccountStatus__c from Account where AccountStatus__c IN ' + this.getSoldValues() + 
			' AND (SoldDate__c != null AND SoldDate__c < LAST_N_DAYS:' + niuDays +') ' +
			'AND (ScheduledInstallDate__c = NULL or ScheduledInstallDate__c < TODAY) ';

	global Database.QueryLocator start( Database.Batchablecontext bc )
	{
		return Database.getQueryLocator(this.query);
	}
	
	global void execute(Database.BatchableContext bc, List<sObject> scope)
	{
		for (Account a : (List<Account>)scope)
		{ 
			a.AccountStatus__c = 'NIO';

		}
		update scope;
	}
	
	global void finish(Database.BatchableContext bc)
	{
		
	}
	
	private String getSoldValues(){
		String SoldValues = '';
		List<String> soldList = new List<String>();
		soldList.addAll(IntegrationConstants.STATUS_SOLD_VALUES);
		if(!soldList.isempty())
		{
			SoldValues = '(';
			for(String s : soldList)
			{
				SoldValues += '\'' + s + '\',';
			}
			SoldValues = SoldValues.substring(0,SoldValues.lastIndexOf(',')) + ')';
		}
		return SoldValues;		
	}
	
}