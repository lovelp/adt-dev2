/***************************** MODIFICATION LOG *************************************************************
 *
 * DESCRIPTION : DOARequestUtils.cls has methods that handle processing of DOA Requests
 *
 *-----------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                   TICKET              REASON
 *-----------------------------------------------------------------------------------------------------------
 * Mike Tuckman                  08/10/2015                                 - Original Version
 * Siddarth Asokan               05/09/2016             HRM 2610            - Deposit Waiver Lock
 * Srinivas Yarramsetti          08/09/2018             HRM 5707            - DOA Request for No Email
 * Giribabu Geda                 09/25/2018             HRM 8086            - DOA Depositwavier not being populated with correct salesrep and approver
 * Siddarth Asokan               10/10/2018             HRM 8015            - Created default approvers for phone sales department
 * Giribabu Geda                 10/28/2018             HRM 8351            - DOA failure
 * Abhinav Pandey                10/28/1028             HRM 8009            - Credit ByPass Process
 * Srinivas Yarramsetti          08/20/2019             HRM 10101           - (TDECOM) Ability for NSC to Set Variable ANSC Increase Limits by Department
 * Siddarth Asokan               09/16/2019             HRM 9983            - Refactoring DOA
 * Siddarth Asokan               10/10/2019             HRM 11480           - DOA Notification misrouted
 */
 
public without sharing class DOARequestUtils {
    // Creates a request queue from the quote trigger
    public static void createRequestQueueForDOA(list<scpq__SciQuote__c> doaQuotes, Boolean isCreditBypass){
        list<RequestQueue__c> doaReqQueueToInsert = new list<RequestQueue__c>();
        for(scpq__SciQuote__c quote: doaQuotes){
            RequestQueue__c rq = new RequestQueue__c();
            rq.Quote__c = quote.Id;
            rq.AccountID__c = quote.Account__c;
            rq.RequestStatus__c = 'Ready';
            rq.ServiceTransactionType__c = isCreditBypass? 'createDOAForCreditBypass' : 'createDOA';
            doaReqQueueToInsert.add(rq);
        }
        if(doaReqQueueToInsert.size() > 0){
            database.insert(doaReqQueueToInsert,false);
        }
    }

    // Called from the node red script
    public static void processRequestQueueForDOA(){
        // Get DOA limit from custom setting
        String doaReqQueueLimitStr = BatchGlobalVariables__c.getinstance('DOA Limit') != null? BatchGlobalVariables__c.getinstance('DOA Limit').value__c: '10';
        Integer doaReqQueueLimit = Integer.ValueOf(doaReqQueueLimitStr);
        // Query the Request Queue records for DOA and process it
        processRequestQueueForDOA([Select Id, ServiceTransactionType__c, RequestStatus__c, Quote__c from RequestQueue__c where RequestStatus__c = 'Ready' AND Quote__c != null AND (ServiceTransactionType__c = 'createDOAForCreditBypass' OR ServiceTransactionType__c = 'createDOA') Order By CreatedDate Asc limit :doaReqQueueLimit]);
    }

    public static void processRequestQueueForDOA(list<RequestQueue__c> doaReqQueueToProcess){
        list<Id> doaQuoteIDs = new list<Id>();
        set<Id> creditBypassDOAQuotesSet = new set<Id>();
        set<Id> DOAQuotesSet = new set<Id>();
        list<Id> accIDs = new list<Id>();
        list<Id> usrIDs = new list<Id>();
        list<DOA_Request__c> doaReqAdds = new list<DOA_Request__c>();
        map<String,RoleHierarchy__mdt> roleMDTMap= new map <String,RoleHierarchy__mdt>();
        map<Id,list<Quote_Job__c>> doaQuoteIdJobsMap = new map<Id,list<Quote_Job__c>>();
        map<String,User> listOfApprovers;
        list<Messaging.SingleEmailMessage> doaNotificationEmails = new List<Messaging.SingleEmailMessage>();
        user appUser = new User();

        for(RequestQueue__c rq: doaReqQueueToProcess){
            rq.RequestStatus__c = 'In Progress';
            doaQuoteIDs.add(rq.Quote__c);
            if(rq.ServiceTransactionType__c == 'createDOAForCreditBypass'){
                creditBypassDOAQuotesSet.add(rq.Quote__c);
            }else if(rq.ServiceTransactionType__c == 'createDOA'){
                DOAQuotesSet.add(rq.Quote__c);
            }
        }
        //Put all the request Queue to 'In Progess' so that it will not be picked up in the next run
        update doaReqQueueToProcess;

        // Cancel pending DOA Requests
        list<DOA_Request__c> doaReqUpds = new list<DOA_Request__c>();
        for (DOA_Request__c r : [Select ID, Quote__c, Quote_ID__c, Approval_Status__c From DOA_Request__c Where Quote__c in :doaQuoteIDs And Approval_Status__c = 'Pending' And DOA_Type_of_Lock__c != 'No Lock']) {
            r.Approval_Status__c = 'Cancelled';
            doaReqUpds.add(r);
        }
        if (!doaReqUpds.isEmpty()){
            update doaReqUpds;
        }

        // Quotes, Accounts, Jobs, Joblines, DOA Requests 
        List <scpq__SciQuote__c> doaQuotes = [select Account__c, scpq__QuoteId__c, DepositWaived__c, DepositWaiverLock__c, DOAApprovalLevel__c, DOALock__c, DOALockID__c, EasyPay__c, Rep_User__c,LastConfiguredby__c,ANSCNegotiationLock__c from 
        scpq__SciQuote__c where ID in :doaQuoteIDs];
        
        List <Quote_Job__c> doaJobs = [select ADSCAddOn__c, ADSCBase__c, ADSCCorpDiscount__c, ADSCInstallPrice__c, ADSCOtherAddOn__c, ADSCOtherCorpDiscount__c, ADSC_Other_Corp_Discount_Percentage__c, 
            ANSCAdditionalMonitoring__c, ANSCAdditionalQSP__c, ANSCBaseMonitoring__c, ANSCBaseQSP__c, ANSCCorpDiscount__c, ANSCFinalRMR__c, ANSCOtherCorpDiscount__c, CreatedById, CreatedDate, DefaultContract__c, 
            DOAApprovalComment__c, DOAApproverLevel__c, DOALock__c, Id, IsDeleted, JobNo__c, JobType__c, LastModifiedById, LastModifiedDate, Name, PackageID__c, ParentQuote__c, 
            ProductFamily__c, PromotionID__c, RequireElectrician__c, RequireLocksmith__c, Sale_Type__c, SystemModstamp, TaskCode__c, TechnologyId__c, TotalADSC__c,DOAModifiedBy__c from Quote_Job__c 
            where ParentQuote__c in :doaQuoteIDs];
        
        for(Quote_Job__c qJob : doaJobs){
            if(doaQuoteIdJobsMap.containsKey(qJob.ParentQuote__c)) { 
                doaQuoteIdJobsMap.get(qJob.ParentQuote__c).add(qJob); 
            } else { 
                doaQuoteIdJobsMap.put(qJob.ParentQuote__c, new list<Quote_Job__c> { qJob }); 
            }
        }

        for (scpq__SciQuote__c q : doaQuotes) {
            accIDs.add(q.Account__c);
            if (q.Rep_User__c != null) usrIDs.add(q.Rep_User__c);
            //HRM 8351 added the lastconfigured by to the list of userids to get the UserName
            if (q.LastConfiguredby__c != null) usrIDs.add(q.LastConfiguredby__c);
        }
        
        map<ID, Account> doaAccts = new map<ID, Account>([select Name, OwnerId, TelemarAccountNumber__c, TransitionDate3__c, Sold_By_System__c, AccountStatus__c, MMBCustomerNumber__c, MMBSiteNumber__c, 
        MMB_Multisite__c, TelemarOrderType__c, Business_Id__c from Account where Id = :accIDs]);
        
        for (Account a : doaAccts.values()){
            usrIDs.add(a.OwnerId);
        }

        map<ID, User> doaUsers = new map<ID, User>([select EmployeeNumber__c, Name, ManagerID, Manager.DelegatedEmail__c, Business_Unit__c, Profile.Name, Profile_Type__c,Rep_Team__c, DelegatedEmail__c from User where ID = :usrIDs]);

        // Role Hierarchy Meta Data Map
        for(RoleHierarchy__mdt roleMDT : [SELECT MasterLabel,DeveloperName,Level1__c,Level2__c,Level3__c,Level4__c,DefaultApprover__c FROM RoleHierarchy__mdt Where Active__c = True]){
            roleMDTMap.put(roleMDT.DeveloperName,roleMDT);
        }
        //HRM 11480 Email Template for DOA
        String DOAEmailTemplateId = '';
        for(EmailTemplate emailTemplate : [SELECT Id FROM EmailTemplate where name = 'DOA Notification' limit 1]){
            DOAEmailTemplateId = emailTemplate.Id;
        }
        
        for (scpq__SciQuote__c q : doaQuotes) {
            Account a = doaAccts.get(q.Account__c);
            String salesRepID;
            // HRM-8086 Start
            if (q.LastConfiguredby__c != null) {
                salesRepID = q.LastConfiguredby__c;
            }else if (q.Rep_User__c != null){
                salesRepID = q.Rep_User__c;
            }else{
                salesRepID = a.OwnerId;
            }// HRM-8086 End
            
            if (DOAQuotesSet.size() > 0 && DOAQuotesSet.contains(q.Id) && !q.DOALock__c && !q.DepositWaiverLock__c && !q.ANSCNegotiationLock__c) continue;  // If not locked - don't create a new one...
            
            DOA_Request__c doaR = new DOA_Request__c();
            doaR.Name = a.Name + ' : ' + q.scpq__QuoteId__c;
            doaR.Account__c = q.Account__c;
            doaR.Sales_Rep__c = salesRepID;
            doaR.Quote__c = q.ID;
            doaR.Quote_ID__c = ' ' + q.scpq__QuoteId__c;
            doaR.Job_Info__c = '';
            doaR.ADSCBase__c = 0;
            doaR.ADSCAddOn__c = 0;
            doaR.ADSCOtherAddOn__c = 0;
            doaR.ADSCCorpDiscount__c = 0;
            doaR.ADSCOtherCorpDiscount__c = 0;
            doaR.ADSCInstallPrice__c = 0;
            doaR.ANSCBaseMonitoring__c = 0;
            doaR.ANSCBaseQSP__c = 0;
            doaR.ANSCAdditionalMonitoring__c = 0;
            doaR.ANSCCorpDiscount__c = 0;
            doaR.ANSCOtherCorpDiscount__c = 0;
            doaR.ANSCFinalRMR__c = 0;

            if(DOAQuotesSet.size() > 0 && DOAQuotesSet.contains(q.Id)){
                // Specific for DOA Locks
                if (q.DOALock__c) doaR.Name += ' : DOA';
                if (q.DepositWaiverLock__c) doaR.Name += ' : DEPWAV';
                if (q.ANSCNegotiationLock__c) doaR.Name += ' : ANSC';
                doaR.DOA_Lock__c = q.DOALock__c;
                doaR.DOALockID__c = q.DOALockID__c;
                doaR.Deposit_Waiver_Lock__c = q.DepositWaiverLock__c;
                doaR.DOAApproverLevel__c = q.DOAApprovalLevel__c;
                doaR.ANSCNegotiationLock__c = q.ANSCNegotiationLock__c;//HRM-10101
            }else if(creditBypassDOAQuotesSet.size() > 0 && creditBypassDOAQuotesSet.contains(q.Id)){
                // Specific for Credit Bypass DOA Locks
                doaR.CreditByPassWaiverRequested__c = true;
            }

            if(doaQuoteIdJobsMap.containsKey(q.Id)){
                for (Quote_Job__c j : doaQuoteIdJobsMap.get(q.Id)){
                    system.debug('** QUOTE JOB: '+j);
                    doaR.Job_Info__c += 'Product: <b>' + j.ProductFamily__c + ((j.DOALock__c)? '  ** DOA APPROVAL REQUIRED **':'') + '</b>';
                    if (DOAQuotesSet.size() > 0 && DOAQuotesSet.contains(q.Id) && j.DOALock__c){
                        doaR.Job_Info__c += '<br>DOA Comment: <b>' + ((Utilities.isEmptyOrNull(j.DOAApprovalComment__c)) ? '':j.DOAApprovalComment__c) + '</b>';
                    }
                    doaR.Job_Info__c += '<br><br>ADSC:';
                    doaR.Job_Info__c += '<br>Base: <b>' + Utilities.currencyDollar(j.ADSCBase__c) + '</b>';
                    doaR.Job_Info__c += '&nbsp;&nbsp;&nbsp;&nbsp;Add-On: <b>' + Utilities.currencyDollar(j.ADSCAddOn__c) + '</b>';
                    doaR.Job_Info__c += '&nbsp;&nbsp;&nbsp;&nbsp;Other: <b>' + Utilities.currencyDollar(j.ADSCOtherAddOn__c) + '</b>';
                    doaR.Job_Info__c += '&nbsp;&nbsp;&nbsp;&nbsp;Total: <b>' + Utilities.currencyDollar(j.ADSCBase__c+j.ADSCAddOn__c+j.ADSCOtherAddOn__c) + '</b>';
                    doaR.Job_Info__c += '<br>Promo Discount: <b>' + Utilities.currencyDollar(j.ADSCCorpDiscount__c) + '</b>';
                    doaR.Job_Info__c += '&nbsp;&nbsp;&nbsp;&nbsp;DOA Discount: <b>' + Utilities.currencyDollar(j.ADSCOtherCorpDiscount__c) + '  (' + j.ADSC_Other_Corp_Discount_Percentage__c.format() + '%)</b>';
                    doaR.Job_Info__c += '<br>Total Install: <b>' + Utilities.currencyDollar(j.ADSCInstallPrice__c) + '</b>';

                    doaR.Job_Info__c += '<br><br>ANSC:';
                    doaR.Job_Info__c += '<br>Base: <b>' + Utilities.currencyDollar(j.ANSCBaseMonitoring__c) + '</b>';
                    doaR.Job_Info__c += '&nbsp;&nbsp;&nbsp;&nbsp;Add-On: <b>' + Utilities.currencyDollar(j.ANSCAdditionalMonitoring__c) + '</b>';
                    doaR.Job_Info__c += '&nbsp;&nbsp;&nbsp;&nbsp;QSP: <b>' + Utilities.currencyDollar(j.ANSCBaseQSP__c+j.ANSCAdditionalQSP__c) + '</b>';
                    doaR.Job_Info__c += '&nbsp;&nbsp;&nbsp;&nbsp;Total: <b>' + Utilities.currencyDollar(j.ANSCBaseMonitoring__c+j.ANSCAdditionalMonitoring__c+j.ANSCBaseQSP__c+j.ANSCAdditionalQSP__c) + '</b>';
                    doaR.Job_Info__c += '<br>Promo Discount: <b>' + Utilities.currencyDollar(j.ANSCCorpDiscount__c) + '</b>';
                    doaR.Job_Info__c += '&nbsp;&nbsp;&nbsp;&nbsp;DOA Discount: <b>' + Utilities.currencyDollar(j.ANSCOtherCorpDiscount__c) + '</b>';
                    doaR.Job_Info__c += '<br>Total Monthly: <b>' + Utilities.currencyDollar(j.ANSCFinalRMR__c) + '</b>';
                    doaR.Job_Info__c += '<hr>';

                    doaR.ADSCBase__c += j.ADSCBase__c;
                    doaR.ADSCAddOn__c += j.ADSCAddOn__c;
                    doaR.ADSCOtherAddOn__c += j.ADSCOtherAddOn__c;
                    doaR.ADSCCorpDiscount__c += j.ADSCCorpDiscount__c;
                    doaR.ADSCOtherCorpDiscount__c += j.ADSCOtherCorpDiscount__c;
                    doaR.ADSCInstallPrice__c += j.ADSCInstallPrice__c;
                    doaR.ANSCBaseMonitoring__c += j.ANSCBaseMonitoring__c;
                    doaR.ANSCBaseQSP__c += j.ANSCBaseQSP__c+j.ANSCAdditionalQSP__c;
                    doaR.ANSCAdditionalMonitoring__c += j.ANSCAdditionalMonitoring__c;
                    doaR.ANSCCorpDiscount__c += j.ANSCCorpDiscount__c;
                    doaR.ANSCOtherCorpDiscount__c += j.ANSCOtherCorpDiscount__c;
                    doaR.ANSCFinalRMR__c += j.ANSCFinalRMR__c;
                }
            }

            // The VP Assignments are false untill exception or hierarchy level is VP
            Boolean runVPAssignments = false;

            User u = doaUsers.get(salesRepID);
            // Get the meta data type custom settings
            RoleHierarchy__mdt roleMetaData = roleMDTMap.get(u.Rep_Team__c);

            try {
                RoleUtils.HierarchyLevel curHierarchyLevel = RoleUtils.getUserAsigneeHierarchy(u);
                map<String, User> approvers = RoleUtils.getRepApproverUserByLevel(u.ID);
                system.debug('** Hierarchy Level: '+curHierarchyLevel);
                system.debug('** Approvers: '+approvers);
                if(DOAQuotesSet.size() > 0 && DOAQuotesSet.contains(q.Id)){
                    // Specific for DOA Locks
                    try{
                        listOfApprovers = RoleUtils.getRepApproverUserByLevel(u.ID);
                        if(String.isNotBlank(DOAEmailTemplateId)){
                            //HRM-11480 DOA email notification creation
                            if(String.isNotBlank(doaR.Sales_Rep__c)){
                                doaR.DOAEmailNotificationUsers__c = String.ValueOf(doaR.Sales_Rep__c).subString(0,15);
                            }
                            if(String.isNotBlank(doaR.DOAApproverLevel__c) && listOfApprovers != null && listOfApprovers.size() > 0){
                                // Send DOA notifications to the lower hierarchy for Field Sales
                                String allLowerApprover = '';
                                if(doaR.DOAApproverLevel__c.equalsIgnoreCase(RoleUtils.REGIONAL_VICE_PRESIDENT)){
                                    allLowerApprover = RoleUtils.AREA_GENERAL_MANAGER+';'+RoleUtils.MANAGER;
                                }else if(doaR.DOAApproverLevel__c.equalsIgnoreCase(RoleUtils.AREA_GENERAL_MANAGER)){
                                    allLowerApprover = RoleUtils.MANAGER;
                                }
                                if(String.isNotBlank(allLowerApprover)){
                                    for(String approvalLevel : allLowerApprover.split(';')){
                                        if(listOfApprovers.get(approvalLevel) != null && listOfApprovers.get(approvalLevel).isActive == true){
                                            // Populate the Approvers in the DOA Email Notification
                                            doaR.DOAEmailNotificationUsers__c += ','+String.ValueOf(listOfApprovers.get(approvalLevel).Id).subString(0,15);
                                        }
                                    }
                                }
                            }
                        }
                    }catch(Exception ex){
                        system.debug('[DOARequestUtils]::[processRequestQueueForDOA - CreateDOAEmailNotificationUsers] --- '+ex.getMessage()+'\n'+ex.getStackTraceString());
                        ADTApplicationMonitor.log(ex, 'DOARequestUtils', 'processRequestQueueForDOA - CreateDOAEmailNotificationUsers', ADTApplicationMonitor.CUSTOM_APP.HERMES);
                    }
                }else if(creditBypassDOAQuotesSet.size() > 0 && creditBypassDOAQuotesSet.contains(q.Id)){
                    // Specific for Credit Bypass DOA Locks
                    doaR.DOAApproverLevel__c = roleMetaData != null && String.isNotBlank(roleMetaData.Level2__c)? roleMetaData.Level2__c : 'Team Manager';
                }
                if(String.isNotBlank(doaR.DOAApproverLevel__c)){
                    appUser = approvers.get(doaR.DOAApproverLevel__c);

                    // Our starting role will be the current for this user and keep reaching out to consecutive higher role
                    String higherApprLevel = doaR.DOAApproverLevel__c;
                    String topApprovalLevel = roleMetaData != null? roleMetaData.Level4__c : RoleUtils.REGIONAL_VICE_PRESIDENT;
                    
                    // Find consecutive higher role and see if we have a user assigned to it on this branch
                    if(appUser == null){
                        // Keep iterating this user's branch upper levels until a user is found
                        do {
                            higherApprLevel = curHierarchyLevel.getNextUpperApprover(higherApprLevel);
                            appUser = approvers.get(higherApprLevel);
                        }while (appUser == null && higherApprLevel != topApprovalLevel);
                    }
                }
                
                if (appUser != null){
                    doaR.Approver__c = appUser.id;
                }else{
                    runVPAssignments = true;
                }
            }catch (Exception e){
                system.debug('[DOARequestUtils]::[processRequestQueueForDOA] --- '+e.getMessage()+'\n'+e.getStackTraceString());
                ADTApplicationMonitor.log(e, 'DOARequestUtils', 'processRequestQueueForDOA', ADTApplicationMonitor.CUSTOM_APP.HERMES);
                runVPAssignments = true;                
            }

            if (runVPAssignments){
                String DOA_VP_Default;

                if(String.isNotBlank(ResaleGlobalVariables__c.getinstance('DOA_VP_Default').value__c)){
                    DOA_VP_Default = ResaleGlobalVariables__c.getinstance('DOA_VP_Default').value__c.substring(0,15);
                }
                
                list<String> VPList = new list<String>{DOA_VP_Default};
                if(roleMetaData != null){
                    VPList.add(roleMetaData.DefaultApprover__c.substring(0,15));
                }
                
                set<String> activeDefaultApprovers = new set<String>();
                for(User defApprover: [Select Id from user where Id IN:VPList and isActive = true]){
                    String approverId = defApprover.Id;
                    activeDefaultApprovers.add(approverId.substring(0,15));
                }
                
                if(roleMetaData != null && activeDefaultApprovers.size() > 0 && activeDefaultApprovers.contains(roleMetaData.DefaultApprover__c)){
                    // For all meta data approver
                    doaR.Approver__c = roleMetaData.DefaultApprover__c; 
                }else if(activeDefaultApprovers.size() > 0 && activeDefaultApprovers.contains(DOA_VP_Default)){
                    // For all field & if meta data approver is inactive
                    doaR.Approver__c = DOA_VP_Default;
                }else{
                    doaR.Approver__c = BatchGlobalVariables__c.getinstance('GlobalAdminId').value__c;
                }
            }
            doaR.OwnerID = salesRepID;
            doar.CreatedById = salesRepID;
            doaReqAdds.add(doaR);
        }
        
        if (!doaReqAdds.isEmpty()){
            insert doaReqAdds;
            createApprovalProcess(doaReqAdds);
        }

        if(String.isNotBlank(DOAEmailTemplateId)){
            try{
                for(DOA_Request__c newDoa : doaReqAdds){
                    if(String.isNotBlank(newDoa.DOAEmailNotificationUsers__c)){
                        for(String notificationUser: newDoa.DOAEmailNotificationUsers__c.split(',')){
                            Messaging.SingleEmailMessage userDOANotificationEmail = new Messaging.SingleEmailMessage();
                            system.debug('Rep: DOAEmailTemplateId: '+DOAEmailTemplateId +' userId: '+ notificationUser + ' DOA Req Id: '+ newDoa.Id);
                            userDOANotificationEmail = createEmailNotificationForDOA(DOAEmailTemplateId,notificationUser,newDoa.Id);
                            if(userDOANotificationEmail != null){
                                doaNotificationEmails.add(userDOANotificationEmail);
                            }
                        }
                    }
                }
            }catch(Exception ex){
                system.debug('[DOARequestUtils]::[processRequestQueueForDOA - CreateDOAEmail] --- '+ex.getMessage()+'\n'+ex.getStackTraceString());
                ADTApplicationMonitor.log(ex, 'DOARequestUtils', 'processRequestQueueForDOA - CreateDOAEmail', ADTApplicationMonitor.CUSTOM_APP.HERMES);
            }
        }
        if(doaNotificationEmails.size() > 0){
            // Send Email Notification for DOA HRM-11480
            try{
                Messaging.sendEmail(doaNotificationEmails);
            }catch(exception e){
                system.debug('[DOARequestUtils]::[processRequestQueueForDOA - sendEmail] --- '+e.getMessage()+'\n'+e.getStackTraceString());
                ADTApplicationMonitor.log(e, 'DOARequestUtils', 'processRequestQueueForDOA - sendEmail', ADTApplicationMonitor.CUSTOM_APP.HERMES);
            }
        }

        // Delete the req queues once the DOA request has been created
        database.delete(doaReqQueueToProcess,false);
    }

    public static Messaging.SingleEmailMessage createEmailNotificationForDOA(Id templateId,Id targetObjectId,Id whatId){
        //HRM-11480 Create Single Email Messages for DOA Notification
        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(templateId,targetObjectId,whatId); // renderStoredEmailTemplate counts as a Query in salesforce
        String emailSubject = email.getSubject();
        String emailTextBody = email.getPlainTextBody();
        email.setSaveAsActivity(false);
        email.setTargetObjectId(targetObjectId);
        email.setSubject(emailSubject);
        email.setPlainTextBody(emailTextBody);
        email.setSenderDisplayName('DOA Notification');
        return email;
    }
    
    public static void createApprovalProcess(list<DOA_Request__c> doaReqAdds){
        list<Approval.ProcessSubmitRequest> appReqs = new list<Approval.ProcessSubmitRequest>();
        for(DOA_Request__c dr : doaReqAdds){
            if (dr.Approval_Status__c == null){
                Approval.ProcessSubmitRequest appReq = new Approval.ProcessSubmitRequest();
                appReq.setComments('DOA Request for Approval.');
                appReq.setObjectId(dr.id);
                appReq.setProcessDefinitionNameOrId('DOA_Approval');
                appReq.setSkipEntryCriteria(true);
                appReqs.Add(appReq);
            }
        }
        
        if (appReqs.size() > 0){
            Approval.ProcessResult[] processResults = null;
            try{
                processResults = Approval.process(appReqs, true);
                system.debug('* Process Results: ' + processResults);
            }catch( Exception e) {
                system.debug('Exception when creating approval process: ' + e);
                ADTApplicationMonitor.log(e, 'DOARequestUtils', 'createApprovalProcess', ADTApplicationMonitor.CUSTOM_APP.HERMES);
            }
        }
    }
    

    // HRM-5707 Create DOA Request for No Email
    public static boolean createNoEmailDOAReq(Account objAccount, User u) {
        Id doaRecordTypeId = Schema.SObjectType.DOA_Request__c.getRecordTypeInfosByName().get('DOA Request for Account').getRecordTypeId();
        Account newobjAccount = [SELECT id, No_Email_Available__c, Email__c, Approval_Status__c,OwnerId, Name FROM Account WHERE id=:objAccount.Id];
        if(objAccount.Email__c == null && newobjAccount.Approval_Status__c != 'Approved'){
            Id currentApprover = findApprover(u);
            DOA_Request__c doaReq = new DOA_Request__c();
            doaReq.Name = objAccount.name;
            doaReq.Account__c = objAccount.Id;
            doaReq.Approval_for_No_Email_in_Account__c = true;
            doaReq.RecordTypeId = doaRecordTypeId;
            doaReq.Sales_Rep__c = u.Id;
            doaReq.Approver__c = currentApprover;
            doaReq.DOA_Lock__c = true;
            try{
                insert doaReq;
                Approval.ProcessSubmitRequest approvalReq = new Approval.ProcessSubmitRequest();
                approvalReq.setObjectId(doaReq.id);
                Approval.ProcessResult result = Approval.process(approvalReq);
                return true;
            }catch(Exception ex){
                system.debug('===>'+ex.getLineNumber()+'==>'+ex.getMessage());
                ADTApplicationMonitor.log(ex, 'DOARequestUtils', 'createNoEmailDOAReq', ADTApplicationMonitor.CUSTOM_APP.HERMES);
                return false;
            }            
        }else{
            return false;
        }
    }
    
    // HRM-5707 Find the approver based on manager or role
    public Static Id findApprover(User u){
        if(u.Managerid != null && u.Manager.isActive){
            //HRM-11480 Use manager only if the user is active since approval process requires an active user
            return u.Managerid;
        }else{
            try{
                Map<String, User> appMap = RoleUtils.getRepApproverUserByLevel(u.Id);
                return appMap.values()[0].Id;
            }catch(Exception ae){
                system.debug('Exception in roleutils: '+ae);
            } 
        }
        //Send to default vp if approverId is empty  
        return ResaleGlobalVariables__c.getInstance('DOA_VP_Default').value__c;
    }
}