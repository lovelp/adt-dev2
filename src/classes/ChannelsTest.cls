/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : ChannelsTest is a test class for Channels.cls
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            03/02/2012          - Origininal Version
*
*                                                    
*/
@isTest
private class ChannelsTest 
{
    static testMethod void testsetChannel()
    {
        Test.startTest();
        
        Lead L1 = new Lead();
        
        L1.NewMoverType__c = 'RL';                      
        Channels.setChannel(L1);
        System.assert(L1.Channel__c == Channels.TYPE_RESIDIRECT, 'The Expected Value was - Resi Direct Sales');
        
        L1.NewMoverType__c = 'RC';                  
        Channels.setChannel(L1);
        System.assert(L1.Channel__c == Channels.TYPE_CUSTOMHOMESALES,'The Expected Value was - Custom Home Sales');
        
        L1.NewMoverType__c = 'SN';                  
        Channels.setChannel(L1);
        System.assert(L1.Channel__c == Channels.TYPE_SBDIRECT, 'The Expected Value was - SB Direct Sales');
        Test.stopTest();
        
        L1.NewMoverType__c = 'AP';                      
        Channels.setChannel(L1);
        System.assert(L1.Channel__c == Channels.TYPE_RESIDIRECT, 'The Expected Value was - Resi Direct Sales');
        
        L1.NewMoverType__c = 'CI';
        Channels.setChannel(L1);
        System.assert(L1.Channel__c == Channels.TYPE_CUSTOMHOMESALES, 'The Expected Value was - Custom Home Sales');
        
        L1.NewMoverType__c = 'CA';                      
        Channels.setChannel(L1);
        System.assert(L1.Channel__c == Channels.TYPE_CUSTOMHOMESALES, 'The Expected Value was - Custom Home Sales');
        
    }
    
    static testMethod void testsetBusinessId()
    {
        Test.startTest();
        
        Lead L1 = new Lead();
        
        L1.Channel__c = Channels.TYPE_SBDIRECT;
        L1.Business_Id__c = null;               
        Channels.setBusinessId(L1);
        System.assert(L1.Business_Id__c == '1200 - Small Business', 'The Expected Value was - 1200 - Small Business');
        
        L1.Channel__c = Channels.TYPE_RESIDIRECT;
        L1.Business_Id__c = null;               
        Channels.setBusinessId(L1);
        System.assert(L1.Business_Id__c == '1100 - Residential', 'The Expected Value was - 1100 - Residential');        
        
        Test.stopTest();
    }
    
    static testMethod void testsetHOADefaults(){
        Test.startTest();
        Lead l = new Lead();
        l.MMBMasterCustomerNumber__c = '1234'; 
        l.HOA_Type__c = 'Homeowner/Site';
        l.MMBCustomerNumber__c = '1234';
        l.Channel__c = Channels.TYPE_HOADIRECTSALES;
        Channels.setHOADefaults(l);
        
        Account a = new Account();
        a.MMBMasterCustomerNumber__c = '1234'; 
        a.HOA_Type__c = 'Homeowner/Site';
        a.MMBCustomerNumber__c = '1234';
        a.Channel__c = Channels.TYPE_HOADIRECTSALES;
        Channels.setHOADefaults(a);
        
        System.assert(l.HOA_Type__c == 'Homeowner/Site', 'The Expected Value was - Homeowner/Site');
        System.assert(a.HOA_Type__c == 'Homeowner/Site', 'The Expected Value was - Homeowner/Site');    
        Test.stopTest();
    }
    
    static testMethod void testgetMappedChannel()
    {
        
        user u = TestHelperClass.createUser(0);
        
        test.startTest();
        
        List<Postal_Codes__c> pcs = new List<Postal_Codes__c>();
        Postal_Codes__c p = new Postal_Codes__c (name='221o2', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100');
        pcs.add(p);
        Postal_Codes__c p1 = new Postal_Codes__c (name='221o3', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100');
        pcs.add(p1);
        insert pcs;
        
        List<PostalCodeReassignmentQueue__c> lstPCQ = new List<PostalCodeReassignmentQueue__c>();
        PostalCodeReassignmentQueue__c PCQ = new PostalCodeReassignmentQueue__c();
        PCQ.PostalCodeId__c =p.id;
        PCQ.PostalCodeNewOnwer__c=u.Id;
        PCQ.TerritoryType__c =Channels.TYPE_RESIDIRECTPHONESALES;        
        lstPCQ.add(PCQ);
        
        PostalCodeReassignmentQueue__c PCQ1 = new PostalCodeReassignmentQueue__c();
        PCQ1.PostalCodeId__c =p.id;
        PCQ1.PostalCodeNewOnwer__c=u.Id;
        PCQ1.TerritoryType__c =Channels.TYPE_SBDIRECTPHONESALES;        
        lstPCQ.add(PCQ1);
        
        PostalCodeReassignmentQueue__c PCQ2 = new PostalCodeReassignmentQueue__c();
        PCQ2.PostalCodeId__c =p.id;
        PCQ2.PostalCodeNewOnwer__c=u.Id;
        PCQ2.TerritoryType__c =Channels.TERRITORYTYPE_BUILDER;        
        lstPCQ.add(PCQ2);
        
        PostalCodeReassignmentQueue__c PCQ3 = new PostalCodeReassignmentQueue__c();
        PCQ3.PostalCodeId__c =p.id;
        PCQ3.PostalCodeNewOnwer__c=u.Id;
        PCQ3.TerritoryType__c =Channels.TYPE_RESIRIF;        
        lstPCQ.add(PCQ3);
        
        PostalCodeReassignmentQueue__c PCQ4 = new PostalCodeReassignmentQueue__c();
        PCQ4.PostalCodeId__c =p.id;
        PCQ4.PostalCodeNewOnwer__c=u.Id;
        PCQ4.TerritoryType__c =Channels.TYPE_SBRIF;        
        lstPCQ.add(PCQ4);
        
        PostalCodeReassignmentQueue__c PCQ5 = new PostalCodeReassignmentQueue__c();
        PCQ5.PostalCodeId__c =p.id;
        PCQ5.PostalCodeNewOnwer__c=u.Id;
        PCQ5.TerritoryType__c =Channels.TYPE_RESIRESALEPHONESALES;        
        lstPCQ.add(PCQ5);
        
        PostalCodeReassignmentQueue__c PCQ6 = new PostalCodeReassignmentQueue__c();
        PCQ6.PostalCodeId__c =p.id;
        PCQ6.PostalCodeNewOnwer__c=u.Id;
        PCQ6.TerritoryType__c =Channels.TYPE_SBRESALEPHONESALES;        
        lstPCQ.add(PCQ6);
        insert lstPCQ;
        
        for(PostalCodeReassignmentQueue__c PQ : lstPCQ){
            String str = Channels.getMappedChannel(PQ.TerritoryType__c,true);
            
        }
        
        test.stopTest();
        //String TerritoryChannel, Boolean mapResale
        
    }
    
    static testMethod void testgetPhoneSaleChannel()
    {
        test.startTest();
        list<Account> lstAcc = new list<Account>();
        
        //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
        insert addr;
        
        for(integer i=0;i<7;i++){
            Account a =new Account();
            a.AddressID__c = addr.Id;
            a.Name = 'test acc';
            a.FirstName__c = 'test';
            a.LastName__c = 'acc';
            a.Business_Id__c = '1100 - Residential';
            a.Data_Source__c = 'TEST';
            a.MMBMasterCustomerNumber__c = '1234'; 
            a.HOA_Type__c = 'Homeowner/Site';
            a.MMBCustomerNumber__c = '1234';
            if(i==0)
                a.Channel__c = Channels.TYPE_RESIDIRECT;
            if(i==1)
                a.Channel__c = Channels.TYPE_RESIRESALE;
            if(i==2)
                a.Channel__c = Channels.TYPE_SBDIRECT;
            if(i==3)
                a.Channel__c = Channels.TYPE_SBRESALE;
            if(i==4)
                a.Channel__c = Channels.TYPE_BUSINESSDIRECTSALES;
            
            lstAcc.add(a);  
        }
        
        insert lstAcc;
        for(Account acc : lstAcc) {
            Channels.getPhoneSaleChannel(acc.Channel__c, true);
        }
        Channels.getBusinessId(Channels.TYPE_RESIDIRECT);
        Channels.getBusinessId(Channels.TYPE_BUSINESSDIRECTSALES);
        Channels.getDefaultReSaleChannel('Small Business');
        Channels.getDefaultChannel('Small Business');
        Channels.getDefaultChannel('Commercial');
        Channels.getFormatedBusinessId('1200-Residential',Channels.BIZID_OUTPUT.FULL);
        Channels.getFormatedBusinessId('1200-Residential',Channels.BIZID_OUTPUT.LONGSTR);
        Channels.getFormatedBusinessId('1200-Residential',Channels.BIZID_OUTPUT.SHORTSTR);
        Channels.getFormatedBusinessId('1200-Residential',Channels.BIZID_OUTPUT.NUM);
        Channels.getFormatedBusinessId('1200-Residential',Channels.BIZID_OUTPUT.DEFAULT_CHANNEL);
        Channels.getFormatedBusinessId('1300-Commercial',Channels.BIZID_OUTPUT.FULL);
        Channels.getFormatedBusinessId('1300-Commercial',Channels.BIZID_OUTPUT.LONGSTR);
        Channels.getFormatedBusinessId('1300-Commercial',Channels.BIZID_OUTPUT.SHORTSTR);
        Channels.getFormatedBusinessId('1300-Commercial',Channels.BIZID_OUTPUT.NUM);
        Channels.getFormatedBusinessId('1100-SB',Channels.BIZID_OUTPUT.DEFAULT_CHANNEL);
        Channels.getFormatedBusinessId('1100-SB',Channels.BIZID_OUTPUT.FULL);
        Channels.getFormatedBusinessId('1100-SB',Channels.BIZID_OUTPUT.LONGSTR);
        Channels.getFormatedBusinessId('1100-SB',Channels.BIZID_OUTPUT.SHORTSTR);
        Channels.getFormatedBusinessId('1100-SB',Channels.BIZID_OUTPUT.NUM);
        Channels.getFormatedBusinessId('1100-SB',Channels.BIZID_OUTPUT.DEFAULT_CHANNEL);
        test.stopTest();
    }  
    
    
}