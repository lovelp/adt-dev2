/************************************* MODIFICATION LOG ********************************************************************************************
* LocationDataProvider
*
* DESCRIPTION : An abstract class defining the interface for all location data providers.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					10/14/2011				- Original Version
*
*													
*/
public abstract class LocationDataProvider {
	
	public static final String CURRENT_LOCATION_MAP_TYPE = '1';
	public static final String DAILY_LOCATION_MAP_TYPE = '2';
	public static final String DISPOSITION_LOCATION_MAP_TYPE = '3';
	public static final String START_GPS = 'startGPS'; 
	public static final String END_GPS = 'endGPS'; 
	
	public abstract List<LocationItem> getLocationItems(LocationDataMessageParameters parameters); 	
	
	public abstract map<String,List<LocationItem>> getStopLocations(Datetime startDT, Datetime endDT, String DevicePhoneNumber); 	

}