/************************************* MODIFICATION LOG ********************************************************************************************
* RecordTypeDevName
*
* DESCRIPTION : Defines string literals corresponding to Record Type Dev Names.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover				 1/19/2012			- Original Version
*
*													
*/

public with sharing class RecordTypeDevName {
	public static final String ADMIN_TIME = 'AdminTime';
	public static final String COMPANY_GENERATED_APPOINTMENT = 'CompanyGeneratedAppointment';
	public static final String INSTALL_APPOINTMENT = 'InstallAppointment';
	public static final String SELF_GENERATED_APPOINTMENT = 'SelfGeneratedAppointment';
	public static final String SALESFORCE_ONLY_EVENT = 'SalesforceOnlyEvent';
	public static final String ADT_NA_RESALE = 'ADTNAResale';
	public static final String STANDARD_ACCOUNT = 'Standard';
	public static final String RIF_ACCOUNT = 'RIF';
	public static final String SYSTEM_GENERATED_LEAD = 'SystemGeneratedLead';
	public static final String USER_ENTERED_LEAD = 'UserEnteredLead';
	
}