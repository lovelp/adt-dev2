public without sharing class ReassignAccountController
{

    //All available sales reps list
    private Map < Id, User > AllAvailableSalesRepsForManager = new Map < Id, User > ();

    public Account acct {get; set;}
    public User usr {get; set;}

    public boolean isAppt {get; set;}

    public boolean isError {get; set;}

    public string ErrorMessage {get; set;}

    public string Message {get; set;}
    
    public Event e;
    
    public List<Event> events;
    
    //getter for salesrep selected
    public String getSelectedSalesRep()
    {
        return selectedSalesRep;
    }
    //setter for salesrep selected
    public void setSelectedSalesRep(String ssr)
    {
        this.selectedSalesRep = ssr;
    }

    //selected salesrep for assignment  
    private String selectedSalesRep = null;

    public ReassignAccountController()
    {
        isAppt = false;
        acct = [Select ID, Name, OwnerId, Owner.Name,Rep_User__c,Rep_User__r.Profile.Name, AssignedBy__c, DateAssigned__c, TelemarAccountNumber__c, SalesAppointmentDate__c, Data_Source__c, ReferredBy__c,
                Type, AccountStatus__c, OutboundTelemarAccountNumber__c 
                From Account WHERE Id = : ApexPages.currentPage().getParameters().get('Id')];

        Set<String> rectypes = new Set<String>{RecordTypeDevName.COMPANY_GENERATED_APPOINTMENT, RecordTypeDevName.SELF_GENERATED_APPOINTMENT};
        
        /*List<Event> events = new List<Event>([
            Select Id from Event
            where WhatId =:acct.Id
                and (StartDateTime > :DateTime.now() or EndDateTime > :DateTime.now())
                and Status__c <> :EventManager.STATUS_CANCELED
                and RecordType.DeveloperName IN :rectypes
            limit 1000
        ]);*/

        events = new List<Event>([
            Select WhoId, WhatId, Description,
                        TaskCode__c, Subject, Status__c, StartDateTime, 
                        ShowAs, ScheduleID__c, ReminderDateTime, RecordTypeId, 
                        OwnerId, IsReminderSet, InstallTechnician__c, EndDateTime,
                        PostalCode__c from Event
            where WhatId =:acct.Id
                and (StartDateTime > :DateTime.now() or EndDateTime > :DateTime.now())
                and Status__c <> :EventManager.STATUS_CANCELED
                and RecordType.DeveloperName IN :rectypes
        ]);
        
        //if(events.size() > 0) e = events[0];
        
        /*if (events.isEmpty())
        {
            buildAllSalesrepsForAManager();
            system.debug(acct.Owner.Name);
            selectedSalesRep = acct.OwnerId;
        }
        else
        {
            isAppt = true;
            ErrorMessage = 'This account has an upcoming appointment.  You cannot change ownership of this account. Please click <a style="margin: 0; font-size: 100%; font-family: Arial,Helvetica,sans-serif; color: #222;" href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + acct.Id + '">here</a> to return to the Account record.';
        }*/
        buildAllSalesrepsForAManager();
        system.debug(acct.Owner.Name);
        selectedSalesRep = acct.OwnerId;
        system.debug(e);
        if (events.size() > 0) {
            //e = events[0];
            isAppt = true;
            ErrorMessage = 'This account has an upcoming appointment. If you reassign this account, please have the new sales rep schedule a new appointment.';
        }
    }

    public List < SelectOption > getallSalesreps()
    {
        List < SelectOption > SalesReps = new List < SelectOption > ();
        SelectOption anOption;
        User u;
        String Label;
        for (Id uid: AllAvailableSalesRepsForManager.KeySet())
        {
            u = AllAvailableSalesRepsForManager.get(uid);
            if (u.IsActive)
            {
                Label = u.Name;
            }
            anOption = new SelectOption(uid, Label);
            SalesReps.add(anOption);
        }
        return SalesReps;
    }

    //Build all salesreps list and return a string in case of an error, else reutrn Blank
    private String buildAllSalesrepsForAManager()
    {
        //get the user id and the related towns that the user/manager is assigned to
        Id currentUserId = UserInfo.getUserId();
        Id currentUserRoleId = UserInfo.getUserRoleId();
        List < Id > matrixedRoles = new List < Id > ();
        matrixedRoles = Utilities.getMatrixedRolesForManager(currentUserRoleId);
        List < Id > allRoles = new List < Id > ();
        allRoles.add(currentUserRoleId);
        if (matrixedRoles != null)
            allRoles.addAll(matrixedRoles);

        //get all subordinate roles for the user
        List < UserRole > subRoles = [select id, name from UserRole where parentRoleId = : allRoles];
        List < id > allSubordinateRoleIds = new List < Id > ();
        for (UserRole ur: subRoles)
        {
            allSubordinateRoleIds.add(ur.id);
        }

        //get all salesreps for the manager
        List < User > allSubordinateUsers = [select id, name, IsActive from User where UserRoleId in : allSubordinateRoleIds AND IsActive = true];
        for (User u: allSubordinateUsers)
        {
            AllAvailableSalesRepsForManager.put(u.id, u);
        }
        return '';
    }

    public pageReference Save()
    {
        isError = false;
        try
        {
            system.debug(e);
            Boolean success;
            if(events.size() > 0) {
                for (Event e : events) {
                    if (e != null) {
                        try {
                            TelemarGateway.cancelAppointment(acct, e);
                        }
                        catch (Exception exc) {}
                    }
                }
                for (Event e : events) {
                    if (e != null) {
                        EventManager.setEventToCanceled(e);
                        EventManager.updateEvent(e);
                    }
                }
            }
            acct.OwnerId = selectedSalesRep;
            acct.Rep_User__c = selectedSalesRep;
            acct.AssignedBy__c=UserInfo.getUserId();
            acct.DateAssigned__c = DateTime.now();
            update acct;
            String message = 'An account, ' + acct.Name + ', has been reassigned to you.';
            if (events.size() > 0)
                message += '<br/><br/>A future dated appointment was cancelled and needs to be rescheduled';
            EmailMessageUtilities.SendEmailNotification(Id.valueOf(selectedSalesRep), 'An account has been reassigned to you.', message , URL.getSalesforceBaseUrl().toExternalForm() + '/' + acct.Id, false);
            disposition__c d = new disposition__c();
            d.DispositionType__c = 'Manager Reassigned Account';
            if (events.size() > 0)
                d.DispositionType__c += ' - Cancelled Appointment';
            d.Comments__c = d.DispositionType__c;
            d.DispositionDate__c =  Datetime.now();
            d.DispositionEmployeeName__c = UserInfo.getName();
            d.TelemarAccountNumber__c = acct.TelemarAccountNumber__c;
            d.AccountID__c = acct.Id;
            insert d;
            
        }
        catch (Exception ex)
        {
            isError = true;
            Message = 'An error occured when changing the Owner.';
            return null;
        }
        return new pageReference('/' + acct.Id);
    }

    public pageReference Cancel()
    {
        return new pageReference('/' + acct.Id);
    }
}