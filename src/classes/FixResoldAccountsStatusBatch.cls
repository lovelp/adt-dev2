/************************************* MODIFICATION LOG ********************************************************************************************
* FixResoldAccountsStatusBatch
*
* DESCRIPTION : Batch class used to correct a Phase 1 issue.
*               Retained for potential future reuse but not planned for deployment with Phase 2. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli          	  10/29/2011			- Original Version
*
*													
*/
global class FixResoldAccountsStatusBatch implements Database.batchable<sObject>, Database.AllowsCallouts{

	//incoming query
	global final String query;
	
	//Constructor - set the local variable with the query string
	global FixResoldAccountsStatusBatch() {
		this.query = 'select id, LeadStatus__c from account where dispositioncode__c in (\'RS - Re-Sold\', \'RD - Resold by Dealer\') and LeadStatus__c = \'Active\'';
	}
	
	//get Querylocator with the specitied query
	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext bc, List<sObject> accounts) {
		List<Account> updatedAccounts = new List<Account>();
		for(sObject s : accounts)
		{
			Account a = (Account)s;
			a.LeadStatus__c = 'Archived';
			updatedAccounts.add(a);
		}
		//update updatedAccounts;
		Database.Saveresult[] SRs = database.update(updatedAccounts, false);
		List<Account> reUpdates = new List<Account>();
		Integer counter = 0;
		for(Database.SaveResult sr:SRs)
		{
			if(!sr.isSuccess())
			{
				reUpdates.add(updatedAccounts[counter]); 
			}
			counter++;
		}		
		if(reUpdates.size() > 0)
		{
			Database.Saveresult[] nSRs = database.update(reUpdates, false);
			for(Database.SaveResult nsr:nSRs)
			{
				if(!nsr.isSuccess())
				{
					//we can do something here if necessary
				}
			}				
		}
	}
	
	global void finish(Database.BatchableContext bc) {

	}

}