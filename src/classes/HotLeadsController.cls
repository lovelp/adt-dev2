public class HotLeadsController{
    
    private final String BUSINESSID_RESIDENTIAL   = '1100';
    private final String BUSINESSID_SMALLBUSINESS = '1200';
    public List<SchedUserTerr__c> schUserList   {get;set;}    
    public String finalUserDetailStringList     {get;set;}
    public Account acc                          {get;set;}
    public Boolean isOwnershipChanged           {get;set;}
    public Boolean isHotLeadAccount                 {get;set;}
    public String hotLeadAccOwner               {get;set;}
    public String hotLeadDateTime               {get;set;}
    public String accType = '';
    private static Map<String, String> ordertypeMap = new Map<String, String>{'A1' => 'Addon', 'N4'=> 'Conversion',  'R3' => 'Relocation', 'N2' => 'New Relocation',  'R4' => 'Resale Multi Site', 'R1' => 'Resale', 'N3' => 'New Multi Site', 'N1' => 'New',  'R2' => 'Reinstatement','Custom Home'=>'Custom Home'};

    public class UserDetailsObject {
        public String userName;
        public String userEmail;
        public String phNumber;
        public String townId;
        public String subTownId;
        public String usId;
        
        UserDetailsObject(){
            userName = '';
            userEmail = '';
            phNumber = '';
            usId = '';
        }
    }

    public HotLeadsController (Apexpages.StandardController sc){
        System.debug('###'+Apexpages.currentPage().getParameters()+'account is'+(Account)sc.getRecord());       
        this.schUserList =new List<SchedUserTerr__c>();
        AccountInfo.accountId = sc.getRecord().Id;
        this.acc = AccountInfo.getAccount();
        if(this.acc != null && String.isNotBlank(this.acc.MMBOrderType__c)){
            if(this.acc.Channel__c != null && this.acc.Channel__c == Channels.TYPE_CUSTOMHOMESALES)
                accType = 'Custom Home';
            else
                accType = this.acc.MMBOrderType__c;
        }
        this.isOwnershipChanged = false;
        this.isHotLeadAccount = (this.acc.HotLeadAssignedDateTime__c != null && String.isNotBlank(this.acc.HotLeadAssignedDateTime__c.format()))? true : false;
        this.hotLeadDateTime = (this.acc.HotLeadAssignedDateTime__c != null && String.isNotBlank(this.acc.HotLeadAssignedDateTime__c.format()))? this.acc.HotLeadAssignedDateTime__c.format() : '';
        this.hotLeadAccOwner = String.isNotBlank(this.acc.owner.name) ? this.acc.owner.name : '';
        System.debug('Id for account is'+AccountInfo.accountId);
        getUserDetails();
    }
    
    public void getUserDetails(){
        this.schUserList.clear();       
        try{
            //leadSharing 
            List<Id> PostalCodeIds = new List<Id>();
            List<UserDetailsObject> userDetailsList = new List<UserDetailsObject>();
            List<Postal_Codes__c> allPostalCodes = new List<Postal_Codes__c>();
            Map<String, String> PCOwners = new Map<String, String>();
            Set<String> allTowns = new Set<String>();
            List<TerritoryAssignment__c> allTAs = new List<TerritoryAssignment__c>();
            Map<Id, String> PCTownsMap = new Map<Id, String>();
            Map<String, String> MTOwners = new Map<String, String>();
            List<Id> ownerIdList = new List<Id> ();
            String BussId = this.acc.Business_Id__c.contains('1200')? '1200' : '1100';
            if(this.acc.postalcodeId__c != null){
                PostalCodeIds.add(this.acc.postalcodeId__c);
            }
            
            //1. get all towns related to the postal code
            allPostalCodes = [Select Id, TownId__c, BusinessId__c, TownUniqueId__c from Postal_Codes__c where Id in : PostalCodeIds];
            for(Postal_Codes__c pcs : allPostalCodes){
                allTowns.add(pcs.TownId__c);
                PCTownsMap.put(pcs.id, pcs.TownId__c);
            }
            
            //2. territory assignment
            allTAs = [Select Id, PostalCodeId__c, OwnerId, TerritoryType__c from TerritoryAssignment__c where PostalCodeId__c in : PostalCodeIds];
            for(TerritoryAssignment__c TA : allTAs){
                PCOwners.put(TA.PostalCodeId__c + ';' + TA.TerritoryType__c, TA.OwnerId);
                ownerIdList.add(TA.OwnerId);
            }
            
            //3. Manager towns
            List<ManagerTown__c> MTs = [Select id, ManagerId__c, TownId__c, BusinessId__c, Type__c from ManagerTown__c where TownId__c in : allTowns];
            for(ManagerTown__c MT : MTs){
                for(String mtts : MT.Type__c.split(';')){
                    MTOwners.put(MT.TownId__c + ';' + MT.BusinessId__c + ';' + mtts, MT.ManagerId__c);
                }
            }
            System.debug('### pc owners are'+PCOwners);
            System.debug('### first map condition is'+this.acc.PostalCodeId__c + ';' + this.acc.Channel__c);
            //Getting the user Details
            if(PCOwners.get(this.acc.PostalCodeId__c + ';' + this.acc.Channel__c) != null) {
                    String userId = PCOwners.get(this.acc.PostalCodeId__c + ';' + this.acc.Channel__c);
                    System.debug('user id is'+userId);
                    if(userId != null){
                    User us = [SELECT Id,Name,Email,MobilePhone FROM User WHERE Id =: userId];    
                    UserDetailsObject usDetailsObject = new userDetailsObject();
                    System.debug('### user is'+us);
                    usDetailsObject.userName = String.isNotBlank(us.Name)?us.Name : '';
                    usDetailsObject.userEmail = String.isNotBlank(us.Email)?us.Email :'';
                    usDetailsObject.phNumber = String.isNotBlank(us.MobilePhone)?us.MobilePhone :'';
                    usDetailsObject.usId = us.Id;
                    userDetailsList.add(usDetailsObject);
                    }
                    //usDetailsObject.userName = (String)schU.getSobject('User__r').get('Name');
                    //usDetailsObject.userEmail = (String)schU.getSobject('User__r').get('Email');
                    //usDetailsObject.phNumber = (String)schU.getSobject('User__r').get('MobilePhone');
                    //usDetailsObject.phNumber = String.isNotBlank(usDetailsObject.phNumber)? usDetailsObject.phNumber.trim() : '';            
                    //usDetailsObject.usId = schU.User__c;
                    //userDetailsList.add(usDetailsObject);
                    
             }
             else if (MTOwners.get(PCTownsMap.get(this.acc.PostalCodeId__c) + ';' + BussId + ';' + this.acc.Channel__c) != null){
                    String userId = PCOwners.get(this.acc.PostalCodeId__c + ';' + this.acc.Channel__c);
                    System.debug('user id manager is'+userId);
                    if(userId != null){
                    User us = [SELECT Id,Name,Email,MobilePhone FROM User WHERE Id =: userId];    
                    UserDetailsObject ussDetailsObject = new userDetailsObject();
                    System.debug('### Manager is'+us);
                    User uss = [SELECT Id,Name,Email,MobilePhone FROM User WHERE Id =: userId];    
                    UserDetailsObject usDetailsObject = new userDetailsObject();
                    usDetailsObject.userName = String.isNotBlank(uss.Name)?uss.Name : '';
                    usDetailsObject.userEmail = String.isNotBlank(uss.Email)?uss.Email :'';
                    usDetailsObject.phNumber = String.isNotBlank(uss.MobilePhone)?uss.MobilePhone :'';
                    usDetailsObject.usId = uss.Id;
                    userDetailsList.add(ussDetailsObject);
                    }
                    //usDetailsObject.userName = (String)schU.getSobject('User__r').get('Name');
                    //usDetailsObject.userEmail = (String)schU.getSobject('User__r').get('Email');
                    //usDetailsObject.phNumber = (String)schU.getSobject('User__r').get('MobilePhone');
                    //usDetailsObject.phNumber = String.isNotBlank(usDetailsObject.phNumber)? usDetailsObject.phNumber.trim() : '';            
                    //usDetailsObject.usId = schU.User__c;
                    //userDetailsList.add(usDetailsObject);
             }
             this.finalUserDetailStringList = JSON.serialize(userDetailsList);
             System.debug('### String for User Details'+this.finalUserDetailStringList);
            
        }
        catch(exception e){
             System.debug('somethign went wrong in ownership change ###'+e);
        }
        
    }
    
    public void changeOwnership() { 
        String usId = ApexPages.CurrentPage().getParameters().get('usId');
        String userEmail = ApexPages.CurrentPage().getParameters().get('userEmail');
        String comments = String.isNotBlank(ApexPages.CurrentPage().getParameters().get('comments'))? ApexPages.CurrentPage().getParameters().get('comments') : '';
        String ph = ApexPages.CurrentPage().getParameters().get('ph');
        String accStatus;
        String orderType;
        try{       
            System.debug('### account is'+this.acc);
            if(this.acc != null && String.isNotBlank(usId) && String.isNotBlank(userEmail)){
                this.acc.OwnerId = usId;
                this.acc.Rep_User__c = usId;
                this.acc.Lead_Origin__c  = 'Hot Leads';//update lead origin
                this.acc.HotLeadAssignedDateTime__c = System.now();          
                update this.acc;
                
                // Disposition record for hot leads
                disposition__c newDispo = new disposition__c();
                newDispo.DispositionType__c = 'System Generated';
                newDispo.DispositionDetail__c = 'Hot Leads';
                newDispo.Comments__c = String.isNotBlank(comments)? comments : '';
                newDispo.DispositionDate__c = DateTime.now();
                newDispo.AccountID__c = acc.Id;
                // Dispositioned by Global Admin
                newDispo.DispositionedBy__c = '00530000005dBvw';
                insert newDispo;
                
                this.isOwnershipChanged = true;
                if(this.isOwnershipChanged){
                    String contact = '';
                    String fName = String.isNotBlank(this.acc.FirstName__c)? this.acc.FirstName__c : '';
                    String lName = String.isNotBlank(this.acc.LastName__c) ? this.acc.LastName__c : '';
                    contact = fName+' '+lName;
                    String dtEmail = System.now().format();
                    accStatus = String.isNotBlank(this.acc.AccountStatus__c) ? this.acc.AccountStatus__c : '';
                    orderType = String.isNotBlank(this.acc.MMBOrderType__c) ? this.acc.MMBOrderType__c : '';
                    String addressAcc = this.acc.SiteStreet__c+', '+this.acc.SiteCity__c+', '+this.acc.SiteState__c+', '+this.acc.SitePostalCode__c;
                    System.debug('### sending the emails'+this.acc.Id+usId+userEmail+this.acc.TelemarAccountNumber__c+this.acc.Name+this.acc.Channel__c+addressAcc+this.acc.phone);
                    if(String.isNotBlank(userEmail)){
                        sendEmailForOwnerShipchanged(this.acc.Id,usId,userEmail,this.acc.TelemarAccountNumber__c,this.acc.Name,this.acc.Channel__c,addressAcc,this.acc.phone,accStatus,NSCLeadCaptureController.convertChanneltoLOB( this.acc.Channel__c ),orderType,comments,contact,dtEmail);
                    }
                    if(String.isNotBlank(ph)){
                        sendSMSForOwnerShipChanged(this.acc.Id,usId,userEmail,this.acc.TelemarAccountNumber__c,this.acc.Name,this.acc.Channel__c,addressAcc,this.acc.phone,accStatus,NSCLeadCaptureController.convertChanneltoLOB( this.acc.Channel__c ),orderType,comments,ph,contact,dtEmail);
                    }
                }
            }
        }
        catch(Exception e){
                System.debug('Error in changing the ownership'+e);
        }   
    }
    
    public void sendEmailForOwnerShipchanged(String accId,String usId,String userEmail,String telAccountNumber,String accountName,String ch,String addrAcc,String phAcc,String accStatus,String LOB,String orderType,String comments,String contact,String dtEmail){
        try{           
            System.debug('### email sending start');
            String baseUrlCampaign = URL.getSalesforceBaseUrl().toExternalForm();
            System.debug('Account to access is'+accId+' and base url is'+baseUrlCampaign);
            Messaging.singleEmailmessage email = new Messaging.singleEmailmessage();
            email.setsubject('New '+ch+' Hot Lead Assigned: '+accountName);
            email.setPlainTextbody('A new '+ch+' Hot Lead has been assigned to you. Please contact the customer. '+'\n Account Telemar Number: '+telAccountNumber+'\n Account Name: '+accountName+'\n Address: '+addrAcc+'\n Phone: '+phAcc+'\n Date/Time: '+dtEmail+'\n Account Status: '+accStatus+'\n LOB: '+LOB+'\n Order Type: '+orderType+'\n Contact: '+contact+'\n Hot Leads Notes: '+comments+'\n link to Account: '+baseUrlCampaign+'/'+accId);
            email.setToAddresses(new String[] { usId });
            email.saveAsActivity = false;
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
            System.debug('###email sent');
        }
        catch(exception e){
            System.debug('cannot send email'+e);
        }
    }

    public void sendSMSForOwnerShipChanged(String accId,String usId,String userEmail,String telAccountNumber,String accountName,String ch,String addrAcc,String phAcc,String accStatus,String LOB,String orderType,String comments,String repPhone,String contact,String dtEmail){
         try{
             List<Wireless_Carrier__c> WirelessCarrierList = Wireless_Carrier__c.getAll().values();
             if( WirelessCarrierList != null && !WirelessCarrierList.isEmpty() ){
                        String regexNumeric = '[^a-zA-Z0-9]';
                        String mobileNumber = repPhone.replaceAll(regexNumeric, '').trim();
                        if(mobileNumber.length() == 11){
                            mobileNumber = mobileNumber.subString(1);
                        }
                        System.debug('### mobileNumber is'+mobileNumber);
                        List<String> SendTo = new List<String>();
                         // Iterate every carrier and use those enabled with an email2sms email address
                        for( Wireless_Carrier__c wCarrier: WirelessCarrierList ){
                            if( wCarrier.SMS_Enabled__c && !String.isBlank(wCarrier.SMS_Email_Service__c)  ){
                                SendTo.add( wCarrier.SMS_Email_Service__c.replaceFirst('number', mobileNumber) );
                            }
                        }
                  String subject ='New '+ch+' Hot Lead Assigned: '+accountName;
                  String msgBody = 'A new '+ch+' Hot Lead has been assigned to you. Please contact the customer. '+' Account Telemar Number: '+telAccountNumber+' Account Name: '+accountName+' Address: '+addrAcc+' Phone: '+phAcc+' Date/Time: '+dtEmail+' Account Status: '+accStatus+' LOB: '+LOB+' Order Type: '+orderType+'Contact: '+contact+' Hot Leads Notes: '+comments;   
                  EmailMessageUtilities.SendEmailNotification(sendTo,subject,msgBody); 
                  System.debug('sms send');
               }
         }
         catch(exception e){
             System.debug('cannot send sms'+e);
         }  
    }
 }