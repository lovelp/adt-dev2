@isTest
global class ADTWebLeadsControllerTest {

    @testsetup static void createCustomSetting(){
        Postal_Codes__c postalCodeObj=new Postal_Codes__c();
        postalCodeObj.name='abc';
        postalCodeObj.BusinessID__c='1100';
        insert postalCodeObj;

        ResaleGlobalVariables__c resaleGlobalVar=new ResaleGlobalVariables__c();
        resaleGlobalVar.name='EmailValidationRegex';
        resaleGlobalVar.value__c='^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        insert resaleGlobalVar;
        
        ResaleGlobalVariables__c resaleGlobalVar1=new ResaleGlobalVariables__c();
        resaleGlobalVar1.name='MMBNameLength';
        resaleGlobalVar1.value__c='0';
        insert resaleGlobalVar1;
        
        ResaleGlobalVariables__c resaleGlobalVar2=new ResaleGlobalVariables__c();
        resaleGlobalVar2.name='MMBAccountNameLength';
        resaleGlobalVar2.value__c='0';
        insert resaleGlobalVar2;
        
        
        
    }
    
    @isTest
    static void testADTWebLeadsAPISuccessScenario(){
        createCustomSetting();
        ADTWebLeadsAPI.DataPowerRequestInfo dataPowerReqInfoWrapInstance= new ADTWebLeadsAPI.DataPowerRequestInfo();
        dataPowerReqInfoWrapInstance.data=new ADTWebLeadsAPI.DataInfo();
        dataPowerReqInfoWrapInstance.data.formName= 'abc';
        dataPowerReqInfoWrapInstance.data.promotionCode= 'abc';
        dataPowerReqInfoWrapInstance.data.messageDivision= 'resi';
        dataPowerReqInfoWrapInstance.data.messageType= 'C';
        dataPowerReqInfoWrapInstance.data.emailSubject= 'abc';
        dataPowerReqInfoWrapInstance.data.businessName= 'abc';
        dataPowerReqInfoWrapInstance.data.streetAddress= 'abc';
        dataPowerReqInfoWrapInstance.data.city= 'abc';
        dataPowerReqInfoWrapInstance.data.stateCode= 'abc';
        dataPowerReqInfoWrapInstance.data.countryCode= 'abc';
        dataPowerReqInfoWrapInstance.data.zipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.primaryPhoneNumber= '1234567890';
        dataPowerReqInfoWrapInstance.data.secondaryPhoneNumber= '1234567890';
        dataPowerReqInfoWrapInstance.data.emailAddress= 'abc@tcs.com';
        dataPowerReqInfoWrapInstance.data.ownershipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.sourceIP= 'abc';
        dataPowerReqInfoWrapInstance.data.createDateTime= 'abc';
        dataPowerReqInfoWrapInstance.data.udf = new List<ADTWebLeadsAPI.UDFParamsInfo>();
        dataPowerReqInfoWrapInstance.data.udf.add(new ADTWebLeadsAPI.UDFParamsInfo());

        String myJSON = JSON.serialize(dataPowerReqInfoWrapInstance);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/api/createWebLead';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(myJSON);

        RestContext.request = req;
        RestContext.response= res;
        ADTWebLeadsAPI.dopostMethod(); 
    }
    
    @isTest
    static void testADTWebLeadsAPIFailureScenario1(){
        createCustomSetting();
        ADTWebLeadsAPI.DataPowerRequestInfo dataPowerReqInfoWrapInstance= new ADTWebLeadsAPI.DataPowerRequestInfo();
        dataPowerReqInfoWrapInstance.data=new ADTWebLeadsAPI.DataInfo();
        dataPowerReqInfoWrapInstance.data.formName= 'abc';
        dataPowerReqInfoWrapInstance.data.promotionCode= 'abc';
        dataPowerReqInfoWrapInstance.data.messageDivision= 'resi';
        dataPowerReqInfoWrapInstance.data.messageType= 'C';
        dataPowerReqInfoWrapInstance.data.emailSubject= 'abc';
        dataPowerReqInfoWrapInstance.data.businessName= 'abc';
        dataPowerReqInfoWrapInstance.data.streetAddress= 'abc';
        dataPowerReqInfoWrapInstance.data.city= 'abc';
        dataPowerReqInfoWrapInstance.data.stateCode= 'abc';
        dataPowerReqInfoWrapInstance.data.countryCode= 'abc';
        dataPowerReqInfoWrapInstance.data.zipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.primaryPhoneNumber= '0123456789';
        dataPowerReqInfoWrapInstance.data.secondaryPhoneNumber= '1234567890';
        dataPowerReqInfoWrapInstance.data.emailAddress= 'abc@tcs.com';
        dataPowerReqInfoWrapInstance.data.ownershipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.sourceIP= 'abc';
        dataPowerReqInfoWrapInstance.data.createDateTime= 'abc';
        dataPowerReqInfoWrapInstance.data.udf = new List<ADTWebLeadsAPI.UDFParamsInfo>();
        dataPowerReqInfoWrapInstance.data.udf.add(new ADTWebLeadsAPI.UDFParamsInfo());

        String myJSON = JSON.serialize(dataPowerReqInfoWrapInstance);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/api/createWebLead';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(myJSON);

        RestContext.request = req;
        RestContext.response= res;
        ADTWebLeadsAPI.dopostMethod(); 
    }

    @isTest
    static void testADTWebLeadsAPIFailureScenario2(){
        createCustomSetting();
        ADTWebLeadsAPI.DataPowerRequestInfo dataPowerReqInfoWrapInstance= new ADTWebLeadsAPI.DataPowerRequestInfo();
        dataPowerReqInfoWrapInstance.data=new ADTWebLeadsAPI.DataInfo();
        dataPowerReqInfoWrapInstance.data.formName= 'abc';
        dataPowerReqInfoWrapInstance.data.promotionCode= 'abc';
        dataPowerReqInfoWrapInstance.data.messageDivision= 'resi';
        dataPowerReqInfoWrapInstance.data.messageType= 'C';
        dataPowerReqInfoWrapInstance.data.emailSubject= 'abc';
        dataPowerReqInfoWrapInstance.data.businessName= 'abc';
        dataPowerReqInfoWrapInstance.data.streetAddress= 'abc';
        dataPowerReqInfoWrapInstance.data.city= 'abc';
        dataPowerReqInfoWrapInstance.data.stateCode= 'abc';
        dataPowerReqInfoWrapInstance.data.countryCode= 'abc';
        dataPowerReqInfoWrapInstance.data.zipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.primaryPhoneNumber= 'a123456712';
        dataPowerReqInfoWrapInstance.data.secondaryPhoneNumber= '1234567123';
        dataPowerReqInfoWrapInstance.data.emailAddress= 'abc@tcs.com';
        dataPowerReqInfoWrapInstance.data.ownershipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.sourceIP= 'abc';
        dataPowerReqInfoWrapInstance.data.createDateTime= 'abc';
        dataPowerReqInfoWrapInstance.data.udf = new List<ADTWebLeadsAPI.UDFParamsInfo>();
        ADTWebLeadsAPI.UDFParamsInfo udfParamsInfoInstance=new ADTWebLeadsAPI.UDFParamsInfo();
        udfParamsInfoInstance.tag='abc';
        udfParamsInfoInstance.value='def';
        dataPowerReqInfoWrapInstance.data.udf.add(udfParamsInfoInstance);

        String myJSON = JSON.serialize(dataPowerReqInfoWrapInstance);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/api/createWebLead';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(myJSON);

        RestContext.request = req;
        RestContext.response= res;
        ADTWebLeadsAPI.dopostMethod();
    }
    
    @isTest
    static void testADTWebLeadsAPIFailureScenario3(){
        createCustomSetting();
        ADTWebLeadsAPI.DataPowerRequestInfo dataPowerReqInfoWrapInstance= new ADTWebLeadsAPI.DataPowerRequestInfo();
        dataPowerReqInfoWrapInstance.data=new ADTWebLeadsAPI.DataInfo();
        dataPowerReqInfoWrapInstance.data.formName= 'abc';
        dataPowerReqInfoWrapInstance.data.promotionCode= 'abc';
        dataPowerReqInfoWrapInstance.data.messageDivision= 'resi';
        dataPowerReqInfoWrapInstance.data.messageType= 'C';
        dataPowerReqInfoWrapInstance.data.emailSubject= 'abc';
        dataPowerReqInfoWrapInstance.data.businessName= 'abc';
        dataPowerReqInfoWrapInstance.data.streetAddress= 'abc';
        dataPowerReqInfoWrapInstance.data.city= 'abc';
        dataPowerReqInfoWrapInstance.data.stateCode= 'abc';
        dataPowerReqInfoWrapInstance.data.countryCode= 'abc';
        dataPowerReqInfoWrapInstance.data.zipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.primaryPhoneNumber= '3123456712';
        dataPowerReqInfoWrapInstance.data.secondaryPhoneNumber= '1234567123';
        dataPowerReqInfoWrapInstance.data.emailAddress= 'abc@tcscom';
        dataPowerReqInfoWrapInstance.data.ownershipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.sourceIP= 'abc';
        dataPowerReqInfoWrapInstance.data.createDateTime= 'abc';
        dataPowerReqInfoWrapInstance.data.udf = new List<ADTWebLeadsAPI.UDFParamsInfo>();
        ADTWebLeadsAPI.UDFParamsInfo udfParamsInfoInstance=new ADTWebLeadsAPI.UDFParamsInfo();
        udfParamsInfoInstance.tag='abc';
        udfParamsInfoInstance.value='def';
        dataPowerReqInfoWrapInstance.data.udf.add(udfParamsInfoInstance);

        String myJSON = JSON.serialize(dataPowerReqInfoWrapInstance);


        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/api/createWebLead';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(myJSON);

        RestContext.request = req;
        RestContext.response= res;
        ADTWebLeadsAPI.dopostMethod(); 
    }
    
    // Reprocess failed records
    @isTest 
    public static void testProcessFailedWebLeadsController(){
         AuditLog__c adtlog=new AuditLog__c(email__c='abc@tcs.com',status__c='Email Failed',type__c='Contact');
        insert adtlog;
        User u=[Select id from User where id=:Userinfo.getUserId()];
        System.runAs(u){
        EmailTemplate emTemp=new EmailTemplate(developerName='Webleads_contact_notification',Name = 'Webleads_contact_notification',TemplateType = 'text',FolderId = UserInfo.getUserId());
        insert  emTemp;
        }

        ADTWebLeadsController.reprocessRecords();
      
    }
    
     @isTest 
    public static void testADTWebLeadsController2(){
        
        ResaleGlobalVariables__c res=new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger',value__C='true');
        insert res;
        Equifax__c eqFax1=new Equifax__c(name='Default Condition Code',value__C='true');
        insert eqFax1;
        Equifax__c eqFax2=new Equifax__c(name='Default Risk Grade',value__C='1');
        insert eqFax2;
        
        Account acc;
        List<Postal_Codes__c> pList=TestHelperClass.createPostalCodes();
        pList.get(0).Name='abc';
        pList.get(0).BusinessID__c='abc';
        update pList.get(0);
        User u=[Select id from User where id=:Userinfo.getUserId()];
        System.runAs(u){
            EmailTemplate emTemp=new EmailTemplate(developerName='Webleads_contact_notification',Name = 'Webleads_contact_notification',TemplateType = 'text',FolderId = UserInfo.getUserId());
            insert  emTemp;
            acc=TestHelperClass.createAccountData();
            acc.PostalCodeId__c=pList.get(0).Id;
            update acc;
        }
        AuditLog__c adtlog=new AuditLog__c(FirstName__c='test',LastName__c='test',BusinessName__c='test',Channel__c = 'SB Direct Sales',LineOfBusiness__c='1200 - Small Business',SecondaryPhone__c='1234567890',AddressID__c=acc.AddressID__c,email__c='abc@tcs.com',status__c='Email Failed',type__c='Lead',PrimaryPhone__c='1234567123',PostalCodeId__c=pList.get(0).Id,PromotionCode__c='abc',Account__c=acc.id);
        insert adtlog;
        ADTWebLeadsController adtWebLeadInstance=new ADTWebLeadsController();
        
        adtWebLeadInstance.populatePostalCodeLookup('abc','abc');
        adtWebLeadInstance.findMatchingAccount(adtlog);
        adtWebLeadInstance.doContactProcessing(adtlog.Id);
        adtWebLeadInstance.doLeadProcessing(adtlog);
        adtWebLeadInstance.doLeadProcessing(new Set<Id>{adtlog.Id});
    }
    
    @isTest
    static void testADTWebLeadsAPISuccessScenarioForLead(){
        createCustomSetting();
        TelemarDecommission__c t=new TelemarDecommission__c(name='WebLeads',value__c='FALSE');
        insert t;
        List<Postal_Codes__c> pList=TestHelperClass.createPostalCodes();
         pList.get(0).Name='12345';
         pList.get(0).BusinessID__c='1100';
         update pList.get(0);
         list<Equifax__c> eqlist = new list<Equifax__c>();
         eqlist.add(new Equifax__c(name = 'Default Condition Code', value__c='CAE1'));
         eqlist.add(new Equifax__c(name = ' Default Risk Grade', value__c='W'));
         insert eqlist;
         Lead_Convert__c lc = new Lead_Convert__c();
         lc.Name = 'SGL';
         lc.Description__c = 'Self Generated Lead';
         lc.SourceNo__c = '022';
         insert lc;
         ResaleGlobalVariables__c rg = new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger',value__c='TRUE');
         insert rg;
        
        ADTWebLeadsAPI.DataPowerRequestInfo dataPowerReqInfoWrapInstance= new ADTWebLeadsAPI.DataPowerRequestInfo();
        dataPowerReqInfoWrapInstance.data=new ADTWebLeadsAPI.DataInfo();
        dataPowerReqInfoWrapInstance.data.formName= 'abc';
        dataPowerReqInfoWrapInstance.data.firstName = 'Iam';
        dataPowerReqInfoWrapInstance.data.firstName = 'TheBoss';
        dataPowerReqInfoWrapInstance.data.promotionCode= 'abc';
        dataPowerReqInfoWrapInstance.data.messageDivision= 'resi';
        dataPowerReqInfoWrapInstance.data.messageType= 'L';
        dataPowerReqInfoWrapInstance.data.emailSubject= 'abc';
        dataPowerReqInfoWrapInstance.data.businessName= 'abc';
        //dataPowerReqInfoWrapInstance.data.streetAddress= 'abc';
        //dataPowerReqInfoWrapInstance.data.city= 'abc';
        dataPowerReqInfoWrapInstance.data.stateCode= 'CO';
        dataPowerReqInfoWrapInstance.data.countryCode= 'US';
        dataPowerReqInfoWrapInstance.data.zipCode= '12345';
        dataPowerReqInfoWrapInstance.data.primaryPhoneNumber= '5615620780';
        //dataPowerReqInfoWrapInstance.data.secondaryPhoneNumber= '1234567890';
        dataPowerReqInfoWrapInstance.data.emailAddress= 'abc@tcs.com';
        //dataPowerReqInfoWrapInstance.data.ownershipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.sourceIP= '10.43.32.18';
        dataPowerReqInfoWrapInstance.data.createDateTime= '11223';
        dataPowerReqInfoWrapInstance.data.udf = new List<ADTWebLeadsAPI.UDFParamsInfo>();
        ADTWebLeadsAPI.UDFParamsInfo udfParamsInfoInstance=new ADTWebLeadsAPI.UDFParamsInfo();
        udfParamsInfoInstance.tag='Request_Message_Format';
        udfParamsInfoInstance.value='RQ20051015';
        dataPowerReqInfoWrapInstance.data.udf.add(udfParamsInfoInstance);

        String myJSON = JSON.serialize(dataPowerReqInfoWrapInstance);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/api/createWebLead';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(myJSON); 

        RestContext.request = req;
        RestContext.response= res;
        ADTWebLeadsAPI.dopostMethod(); 
    }
    
      @isTest
    static void testADTWebLeadsAPISuccessScenarioForLead1(){
        createCustomSetting();
        TelemarDecommission__c t=new TelemarDecommission__c(name='WebLeads',value__c='TRUE');
        insert t;
        List<Postal_Codes__c> pList=TestHelperClass.createPostalCodes();
         pList.get(0).Name='12345';
         pList.get(0).BusinessID__c='1100';
         update pList.get(0);
         list<Equifax__c> eqlist = new list<Equifax__c>();
         eqlist.add(new Equifax__c(name = 'Default Condition Code', value__c='CAE1'));
         eqlist.add(new Equifax__c(name = ' Default Risk Grade', value__c='W'));
         insert eqlist;
         Lead_Convert__c lc = new Lead_Convert__c();
         lc.Name = 'SGL';
         lc.Description__c = 'Self Generated Lead';
         lc.SourceNo__c = '022';
         insert lc;
         ResaleGlobalVariables__c rg = new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger',value__c='TRUE');
         insert rg;
        
        ADTWebLeadsAPI.DataPowerRequestInfo dataPowerReqInfoWrapInstance= new ADTWebLeadsAPI.DataPowerRequestInfo();
        dataPowerReqInfoWrapInstance.data=new ADTWebLeadsAPI.DataInfo();
        dataPowerReqInfoWrapInstance.data.formName= 'abc';
        dataPowerReqInfoWrapInstance.data.firstName = 'Iam';
        dataPowerReqInfoWrapInstance.data.firstName = 'TheBoss';
        dataPowerReqInfoWrapInstance.data.promotionCode= 'abc';
        dataPowerReqInfoWrapInstance.data.messageDivision= 'resi';
        dataPowerReqInfoWrapInstance.data.messageType= 'L';
        dataPowerReqInfoWrapInstance.data.emailSubject= 'abc';
        dataPowerReqInfoWrapInstance.data.businessName= 'abc';
        //dataPowerReqInfoWrapInstance.data.streetAddress= 'abc';
        //dataPowerReqInfoWrapInstance.data.city= 'abc';
        dataPowerReqInfoWrapInstance.data.stateCode= 'CO';
        dataPowerReqInfoWrapInstance.data.countryCode= 'US';
        dataPowerReqInfoWrapInstance.data.zipCode= '12345';
        dataPowerReqInfoWrapInstance.data.primaryPhoneNumber= '5615620780';
        //dataPowerReqInfoWrapInstance.data.secondaryPhoneNumber= '1234567890';
        dataPowerReqInfoWrapInstance.data.emailAddress= 'abc@tcs.com';
        //dataPowerReqInfoWrapInstance.data.ownershipCode= 'abc';
        dataPowerReqInfoWrapInstance.data.sourceIP= '10.43.32.18';
        dataPowerReqInfoWrapInstance.data.createDateTime= '11223';
        dataPowerReqInfoWrapInstance.data.udf = new List<ADTWebLeadsAPI.UDFParamsInfo>();
        ADTWebLeadsAPI.UDFParamsInfo udfParamsInfoInstance=new ADTWebLeadsAPI.UDFParamsInfo();
        udfParamsInfoInstance.tag='Request_Message_Format';
        udfParamsInfoInstance.value='RQ20051015';
        dataPowerReqInfoWrapInstance.data.udf.add(udfParamsInfoInstance);

        String myJSON = JSON.serialize(dataPowerReqInfoWrapInstance);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/api/createWebLead';
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(myJSON); 

        RestContext.request = req;
        RestContext.response= res;
        ADTWebLeadsAPI.dopostMethod();
        list<id> audilid = new list<id>();
        for(Auditlog__c ad:[SELECT id from AuditLog__c]){
            audilid.add(ad.id);
        }
        Messaging.SendEmailResult[] sendem = ADTWebLeadsController.sendMassEmail(audilid);
    }
    
    @isTest
    static void testADTEBRAPI(){
        
        ResaleGlobalVariables__c rgvVar=new ResaleGlobalVariables__c();
        rgvVar.name='DataRecastDisableAccountTrigger';
        rgvVar.value__c='true';
        insert rgvVar;
        
        RequestQueue__c insertReqQue = new RequestQueue__c(ServiceMessage__c = 'test', IsPrimary__c = true);
        insert insertReqQue;
        
        Account acc=TestHelperClass.createAccountData();
        acc.TelemarAccountNumber__c='1234';
        acc.Phone='1234567890';
        acc.PhoneNumber2__c='12345678901';
        acc.PhoneNumber3__c='1234567890';
        acc.PhoneNumber4__c='1234567890';
        update acc;
        AuditLog__c aLog = new AuditLog__c();
        aLog.name = 'Test Audit Log';
        aLog.Account__c = acc.id;
        aLog.FormName__c = 'Test Form Name';
        aLog.Email__c = 'test@tcs.com';
        alog.PrimaryPhone__c = '1478523698';
        aLog.Type__c = 'Contact';
        aLog.LineOfBusiness__c = '1100 - Residential';
        insert aLog;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ADTWEBLEADSMockTest());  
        ADTEBRAPI.doEBRCallOut(insertReqQue);
        ADTEBRAPI adtEbrAPIinstance = new ADTEBRAPI();
        ADTEBRAPI.sendToEBR(acc.Id, aLog.Id);
        ADTEBRAPI.sendToEBR(new list<AuditLog__c>{aLog}, new list<Account>{acc});
        //ADTEBRAPI.doEBRCallOut('jsonString', insertReqQue.id, true);
        
        ADTEBRAPI.EBRRequestWrapper e = new ADTEBRAPI.EBRRequestWrapper('a','a','a','a','a');
        Test.stopTest();
    }
    static testMethod void  sendtooneclickTest(){
        ResaleGlobalVariables__c rgvVar=new ResaleGlobalVariables__c();
        rgvVar.name='DataRecastDisableAccountTrigger';
        rgvVar.value__c='true';
        insert rgvVar;
        Test.setMock(HttpCalloutMock.class, new ADTWEBLEADSMockTest());  
         
        Account acc=TestHelperClass.createAccountData();
        acc.TelemarAccountNumber__c='1234';
        acc.Phone='1234567890';
        acc.PhoneNumber2__c='12345678901';
        acc.PhoneNumber3__c='1234567890';
        acc.PhoneNumber4__c='1234567890';
        update acc;
        
        Promotion__c promo = new Promotion__c();
        promo.Name='$200 off on ADT pulse';
        promo.Description__c='test Desc';
        promo.PromotionCode__c='1234';
        promo.lineofBusiness__c='Residential';
        promo.ordertype__c='R1';
        insert promo;
        
        DNIS__c dd = new DNIS__c();
        dd.Name = '1113335555';
        dd.Business_segment__c='US: Residential';
        dd.Marketing_Generic__c='DM';
        dd.Promotions__c = promo.Id;
        insert dd;
        
        // CallData For Valid Account
        Call_Data__c cd = new Call_Data__c();
        cd.Name = 'Test Call Data';
        cd.Account__c = acc.Id;
        cd.Partner_Id__c = 'ECOM';
        cd.DNIS__c = dd.Id;
        //cd.Call_Disposition__c = callDispo.Id;
        insert cd;
        
        AuditLog__c aLog = new AuditLog__c();
        aLog.name = 'Test Audit Log';
        aLog.Account__c = acc.id;
        aLog.FormName__c = 'Test Form Name';
        aLog.Email__c = 'test@tcs.com';
        alog.PrimaryPhone__c = '1478523698';
        aLog.Type__c = 'Contact';
        aLog.LineOfBusiness__c = '1100 - Residential';
        aLog.CallData__c = cd.Id;
        insert aLog;
        
        
        ADTOneClickAPI adtoneclick = new ADTOneClickAPI();
        ADTOneClickAPI.createOneClickReqQueue(acc.Id, null);
        ADTOneClickAPI.createOneClickReqQueue(null, aLog.Id);
        Test.starttest();
         ADTOneClickAPI.doOneClickCallOut([select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c from RequestQueue__c limit 1]);
        Test.stoptest();
   }
   
   static testmethod void ADTCheetahMailAPITest(){
        ResaleGlobalVariables__c rgvVar=new ResaleGlobalVariables__c();
        rgvVar.name='DataRecastDisableAccountTrigger';
        rgvVar.value__c='true';
        insert rgvVar;
        Test.setMock(HttpCalloutMock.class, new ADTWEBLEADSMockTest());  
        //RequestQueue__c insertReqQue = new RequestQueue__c();
        //insert insertReqQue;
        
        Account acc=TestHelperClass.createAccountData();
        acc.TelemarAccountNumber__c='1234';
        acc.Phone='1234567890';
        acc.PhoneNumber2__c='12345678901';
        acc.PhoneNumber3__c='1234567890';
        acc.PhoneNumber4__c='1234567890';
        update acc;
        AuditLog__c aLog = new AuditLog__c();
        aLog.name = 'Test Audit Log';
        aLog.Account__c = acc.id;
        aLog.FormName__c = 'Test Form Name';
        aLog.Email__c = 'test@tcs.com';
        alog.PrimaryPhone__c = '1478523698';
        aLog.Type__c = 'Contact';
        aLog.LineOfBusiness__c = '1100 - Residential';
        insert aLog;
        ADTCheetahMailAPI cheetah = new ADTCheetahMailAPI();
        cheetah.sendToACKEmail(aLog);
        list<AuditLog__c> aloglist = new list<AuditLog__c>();
        aloglist.add(aLog);
        ADTCheetahMailAPI.sendToACKEmail(aloglist);
        Test.starttest();
        ADTCheetahMailAPI.doACKEmailCallOut([select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate, Counter__c, EventID__c,IsPrimary__c,AuditLog__c from RequestQueue__c limit 1]);
        Test.stoptest();
    }
    
}