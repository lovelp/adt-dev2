@isTest
private class SalesAppointmentEmailControllerTest {
    
    public static Event e;

     
    static void createTestData(){
        TestHelperClass.createReferenceDataForTestClasses();
        /*
        integer i= 3242344;
        Profile p = [select id from profile where name='System Administrator'];
        User integrationUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='TestIntegration', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        insert integrationUser;
        */
        List<user> users=new List<user>();
        
        Profile p = [select id from profile where name='System Administrator'];
        integer i= 23547876;
        User integrationUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='TestIntegration', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(integrationUser);
        
        i= 7987345;
        User NewRehashUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='NewRehash', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(NewRehashUser);
        i= 2343578;
        User ResaleAdmin=new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='ADMIN',firstname='TestGlobal', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        
        users.add(ResaleAdmin);
        
        INSERT users;
        
        Account a=testHelperClass.createAccountData();
        
        
        e=testHelperClass.createEvent(a,testHelperClass.createUser(32423422));
        e.TimeZoneId__c ='America/Los_Angeles';
        contact c=new contact();
        c.firstname='test';
        c.lastname='test';
        c.accountid=a.id;
        insert c;
        e.whoid=c.id;
        update e;
        
        
    
    
    }
    
    static testMethod void TestSalesAppointmentEmailController() {
        createTestData();
        SalesAppointmentEmailController saec=new SalesAppointmentEmailController();
        saec.eId=e.id; 
        saec.isReminder =true;
        saec.getComponentData();
        saec.isReminder =false;
        saec.getComponentData();
        
    }
     static testMethod void TestSalesAppointmentEmailController1() {
        createTestData();
        SalesAppointmentEmailController saec=new SalesAppointmentEmailController();
        
        saec.eId=e.id; 
        saec.isReminder =null;
        saec.getComponentData();
        saec.isReminder =false;
        e.Status__c ='Canceled';
        e.Reschedule_ToID__c=e.id;
        e.OwnerWhenCanceled__c='test1';
        update e;
        saec.getComponentData();
        
    }
}