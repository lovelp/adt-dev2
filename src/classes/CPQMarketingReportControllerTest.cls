/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CPQMarketingReportControllerTest {

    static testMethod void CPQReportRenderTest() {

        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        	TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        Account a;

        System.runAs(current) { 
            
	        //Create Addresses first
	        Address__c addr = new Address__c();
	        addr.Latitude__c = 38.94686000000000;
	        addr.Longitude__c = -77.25470100000000;
	        addr.Street__c = '8952 Brook Rd';
	        addr.City__c = 'McLean';
	        addr.State__c = 'VA';
	        addr.PostalCode__c = '221o2';
	        addr.County__c = 'County123';
	        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
	        
	        insert addr;
            
            a = TestHelperClass.createAccountData(addr, true);
            a.FirstName__c = 'AccountName';
            a.LastName__c = 'MReportTest';
            a.Name = 'AccountNameMReportTest';
            a.Channel__c = 'SB Direct Sales';
            update a;
			
			// new opportunity
			Opportunity opp = New Opportunity();
			opp.AccountId = a.Id;
			opp.Name = 'TestOpp1';
			opp.StageName = 'Prospecting';
			opp.CloseDate = Date.today();
			insert opp;

			// CPQ's XMLs
			String ActivityXML = '<?xml version="1.0" encoding="UTF-8"?><Activity><DOAApproval><Job ApprovalComment="this is a test" ApproverLevel="Sales Manager" Family="Video" InstallPercent="5" MonitorPercent="5" QSPPercent="5"><Package Name="[A-ADT16H-2TB] - 16 Channel DVR Package 2TB"/><Package Name="[A-ADT16H-2TB] - 4 Channel DVR package"/></Job><Job ApprovalComment="This is a test" ApproverLevel="Area Sales Manager" Family="Pulse" InstallPercent="10.0" MonitorPercent="5.0" QSPPercent="3.0"><Package Name="[8101 BUSI] - Enterprise View: Wireless"/></Job></DOAApproval><JobStatuses><ADTJob JobNo="85221507" Status="Signed"/><ADTJob JobNo="85221508" Status="Signed"/></JobStatuses></Activity>';
			String JobXML = '<?xml version="1.0" encoding="UTF-8"?><Jobs><Job ADSCAddOn="1,225.00" ADSCBase="699.00" ADSCCorpDiscount="-300.00" ADSCInstallPrice="1,561.60" ADSCOtherAddOn="100.00" ADSCOtherCorpDiscount="-162.40" ANSCAdditionalMonitoring="0.00" ANSCAdditionalQSP="9.00" ANSCBaseMonitoring="50.99" ANSCBaseQSP="7.00" ANSCCorpDiscount="0.00" ANSCFinalRMR="63.96" ANSCOtherCorpDiscount="-3.03" DOAApprovalComment="This is a test" DOAApproverLevel="Area Sales Manager" DefaultContract="SB025" JobNo="85221507" JobType="INSTP" ProductFamily="Pulse" RequireElectrician="N" RequireLocksmith="N" TaskCode="SPI" TechnologyId="1025"><Packages><Package ID="8101 BUSI" Name="Enterprise View: Wireless"/></Packages><Promotions><Promotion ID="Promo 27" Name="$300 off Pulse Enterprise"/></Promotions><JobLines><JobLine Item="SF3000" Name="SafeWatch 3000 Control Panel" Package="8101 BUSI" PackageQty="1.00"/><JobLine AddOnQty="4.00" Item="WLS912L-433" Name="PremisePro Wireless Glass Break Detector"/></JobLines></Job><Job ADSCAddOn="150.00" ADSCBase="1,070.00" ADSCCorpDiscount="0.00" ADSCInstallPrice="1,285.00" ADSCOtherAddOn="65.00" ADSCOtherCorpDiscount="0.00" ANSCAdditionalMonitoring="0.00" ANSCAdditionalQSP="6.58" ANSCBaseMonitoring="29.67" ANSCBaseQSP="19.83" ANSCCorpDiscount="0.00" ANSCFinalRMR="56.08" ANSCOtherCorpDiscount="0.00" DOAApprovalComment="" DOAApproverLevel="Sales Manager" DefaultContract="SB117" JobNo="85221508" JobType="INST" ProductFamily="Video" RequireElectrician="N" RequireLocksmith="N" TaskCode="SCC" TechnologyId="CCTV"><Packages><Package ID="A-ADT16H-2TB" Name="16 Channel DVR Package 2TB"/></Packages><Promotions/><JobLines><JobLine AddOnQty="1.00" Item="XDLBW2" Name="DVR Lockbox: 8ch and 16ch"/></JobLines></Job></Jobs>';
			
			scpq__SciQuote__c sq = New scpq__SciQuote__c();
			sq.Name = 'TestQuote1';
			sq.scpq__OpportunityId__c = opp.Id;
			sq.scpq__OrderHeaderKey__c = 'whoknows';
			sq.scpq__SciLastModifiedDate__c = Date.today();
			sq.scpq__Status__c = 'Created';
			sq.Activity_Data__c = ActivityXML;
			sq.Job_Details__c = JobXML;
			sq.Account__c = a.Id;			
			insert sq;
			sq.scpq__Status__c = 'Paid';
			update sq;
			sq.scpq__Status__c = 'Signed';
			update sq;
            
            Quote_Activity__c qa = new Quote_Activity__c ();
            qa.name = 'test Quote Activity';
            qa.Account__c = a.id;
            qa.ADSC_Discount__c	 = 10;
            qa.ANSC_Discount__c = 10;
            qa.Comments__c = 'Test Comment';
            qa.PackageID__c = '1233455';
            qa.QSP_Discount__c = 10;
            qa.Quote__c = sq.id;
            insert qa;
			
            
            CPQMarketingReportController cpqRpt = new CPQMarketingReportController();
            cpqRpt.startDate = datetime.now().addDays(-1).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\''); 
            cpqRpt.endDate = datetime.now().addDays(1).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
            cpqRpt.filterBy = 'CreatedDate';
            cpqRpt.LoadQuoteReportData();
            List<CPQMarketingReportController.QuoteInfoRow> res = cpqRpt.getquoteList();
            if(!res.isEmpty()){
            	CPQMarketingReportController.QuoteInfoRow rowProp = res[0];
            	String str1 = rowProp.getCreatedDateStr();
            	String str2 = rowProp.getSubmittedDateStr();            	
            }
        }
        
        // create quote and related data
        // run report
    }
}