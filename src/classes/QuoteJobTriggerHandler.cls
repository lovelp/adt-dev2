/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : Trigger Handler for QuoteJobTrigger
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                                       TICKET     REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari             03/05/2019          - Origininal Version    HRM-9453 - eContract Signed notification to Submitted by Rep and his Manager
*                                                
*/
public class QuoteJobTriggerHandler {

    public static void ProcessQuoteJobAfterInsert(list<Quote_Job__c> jobList) {
        
        ResaleGlobalVariables__c setting = ResaleGlobalVariables__c.getinstance('ContractSignedNotificationPackages');
        String packageIdStr = setting != null ? setting.Value__c : '';
        set<String> packageIds = new set<String>();
        if(String.isNotBlank(packageIdStr)){
        	packageIds.addAll(packageIdStr.trim().split(','));
            map<Id, scpq__SciQuote__c> quotes = new map<Id, scpq__SciQuote__c>();
            for(Quote_Job__c job : jobList){
                for(String pkgId: packageIds){
                	if(String.isNotBlank(job.PackageID__c) && job.PackageID__c.contains(pkgId.trim())){
                		quotes.put(job.ParentQuote__c, new scpq__SciQuote__c(Id = job.ParentQuote__c, Contains_Cyber_Orders__c = true));
                	}
                }
            }
            if(!quotes.isEmpty()){
            	update quotes.values();
            }
        }
    }
}