/**
* @File Name          : AdtCustContactEBRService.cls
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE             TICKET            REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Viraj Shah                    09/18/2019       HRM-10437         - Initial Version
*
**/
public with sharing class AdtCustContactEBRService { 
    /**
    * This method creates the EBR  request queues for Gryphon and Possiblenow callouts
    * @Param Account
    * @ReturnType list of RequestQueue__c
    **/
    public static void createRequestQueues(List<Account> lstacc,List<Lead> lstLead){
        try{
            Set<String> accountIdSet = new Set<String>();
            Set<String> leadIdSet = new Set<String>();
            Set<String> uniquePhoneString = new Set<String>();
            if(lstacc != null && lstacc.size() > 0){
                for(Account acc : lstacc){
                    accountIdSet.add(acc.id);
                }        
            } 
            if(lstLead != null && lstLead.size() > 0){
                for(Lead ld : lstLead){
                    leadIdSet.add(ld.id);
                }        
            }
            List<RequestQueue__c> reqQueueExisting = new List<RequestQueue__c>();
            Integer maxCount = BatchGlobalVariables__c.getinstance('EWC Limit')!= null && String.isNotBlank(BatchGlobalVariables__c.getinstance('requestQueueRetryCounter').value__c)? Integer.valueOf(BatchGlobalVariables__c.getinstance('requestQueueRetryCounter').value__c): Integer.valueOf('5');
            reqQueueExisting  = [select Id,MessageID__c  from RequestQueue__c  where (RequestStatus__c =: IntegrationConstants.REQUEST_STATUS_READY  OR RequestStatus__c =:IntegrationConstants.REQUEST_STATUS_ERROR OR RequestStatus__c =:IntegrationConstants.REQUEST_STATUS_SUCCESS )And (Counter__c = null OR (Counter__c != null AND Counter__c <=:maxCount)) AND (ServiceTransactionType__c= 'addEBRInquiry' OR ServiceTransactionType__c= 'sendEBRRequest') AND (AccountID__c IN: accountIdSet OR Lead_ID__c IN: leadIdSet) AND CreatedDate = Today limit 100];
            if(reqQueueExisting.size() > 0){
                for(RequestQueue__c   rq : reqQueueExisting){
                    if(String.isNotBlank(rq.MessageID__c)){
                        uniquePhoneString.add(rq.MessageID__c.replaceAll('\\D', ''));
                    }
                }
            }
            String Sender =  'SFDC'; //Sender identifier for the request (SFDC/Telemar) 
            String currentDate =  Datetime.now().format('MMddyyyy');  //Date value Format (PossibleNow/Gryphon)
            String referenceData = ' ';
            String telemarId = '';
            String trimmedTelemarId = '';
            Map<String,String> telemarIdMap = new Map<String,String>();
            List<RequestQueue__c> reqQueuesList = new List<RequestQueue__c>();
            // Added condition on 19/09/2019 By Viraj Shah
            Map<Id, Set<String>> mapAccPhone = new Map<Id, Set<String>>();
            if(lstacc != null && lstacc.size() > 0){
                for(Account acc : lstacc){
                    Set<String> formatedPhoneNumbers = new Set<String>();
                    if(String.isNotBlank(acc.PhoneNumber2__c)){
                        formatedPhoneNumbers.add(acc.PhoneNumber2__c);
                    }
                    if(String.isNotBlank(acc.PhoneNumber3__c)){
                        formatedPhoneNumbers.add(acc.PhoneNumber3__c);
                    }
                    if(String.isNotBlank(acc.PhoneNumber4__c)){
                        formatedPhoneNumbers.add(acc.PhoneNumber4__c);
                    }
                    if(String.isNotBlank(acc.Phone)){
                        formatedPhoneNumbers.add(acc.Phone);
                    }  
                    telemarIdMap.put(acc.id,acc.telemarAccountNumber__c);                 
                    mapAccPhone.put(acc.id,formatedPhoneNumbers);
                }
            }
            if(lstLead != null && lstLead.size() > 0){
                for(Lead l: lstLead){
                    Set<String> formatedPhoneNumbers = new Set<String>();
                    if(String.isNotBlank(l.PhoneNumber2__c)){
                        formatedPhoneNumbers.add(l.PhoneNumber2__c);
                    }    
                    if(String.isNotBlank(l.PhoneNumber3__c)){
                        formatedPhoneNumbers.add(l.PhoneNumber3__c);
                    } 
                    if(String.isNotBlank(l.PhoneNumber4__c)){
                        formatedPhoneNumbers.add(l.PhoneNumber4__c);
                    }
                    if(String.isNotBlank(l.Phone)){
                        formatedPhoneNumbers.add(l.Phone);
                    }    
                    telemarIdMap.put(l.id,l.LeadManagementId__c);  
                    mapAccPhone.put(l.id,formatedPhoneNumbers);
                }
            }
            for(String key: mapAccPhone.keySet()){
                telemarId = telemarIdMap.containsKey(key)? telemarIdMap.get(key) : '';
                trimmedTelemarId = '';
                if(String.isNotBlank(telemarId)){
                    if(telemarId.length() >= 8){
                        trimmedTelemarId = telemarId.right(8);
                    }else if(telemarId.length() < 8){
                        trimmedTelemarId = telemarId.leftPad(8-telemarId.length(), '0');
                    }
                }
                for(String dphone : mapAccPhone.get(key)){
                    if(uniquePhoneString.contains(dphone.replaceAll('\\D', ''))){
                        continue;
                    }
                    // Create JSON to store in service message
                    String jsonString = '';
                    referenceData = '';
                    // Create Request queue for each phone number
                    RequestQueue__c rq = new RequestQueue__c(); 
                    referenceData= String.isNotBlank(trimmedTelemarId)?trimmedTelemarId:telemarId;
                    referenceData+= 'H'+ dphone.replaceAll('\\D', '') +String.valueOf(System.now()).replace(' ','-').replace(':','.')+'.000000';    
                    jsonString =  JSON.serialize(new ADTDNCWrapper (Sender,dphone.replaceAll('\\D', ''),currentDate,referenceData) );
                    if(String.isNotBlank(String.valueOf(key)) && String.valueOf(key).substring(0,3) == '001'){
                        rq.AccountID__c = key;
                    }else if(String.isNotBlank(String.valueOf(key)) && String.valueOf(key).substring(0,3) == '00Q'){
                        rq.Lead_ID__c = key;
                    }
                    rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
                    rq.ServiceTransactionType__c = 'addEBRInquiry';
                    rq.MessageID__c = dphone.replaceAll('\\D', '');
                    rq.ServiceMessage__c = jsonString;
                    rq.RequestDate__c = system.now();
                    rq.Counter__c = 0;
                    reqQueuesList.add(rq);
                }
            }
            if(reqQueuesList.size()>0){
                insert reqQueuesList;
            }
        }catch(exception ex){
            ADTApplicationMonitor.log ('EBR Enquiry', ex.getMessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);
        }
    }    
   
    public with sharing class ADTDNCWrapper {
        public String sender; //Sender identifier for the request (SFDC/Telemar) 
        public String phone; //Phone Number value Format: 9999999999 (PossibleNow/Gryphon)
        public String currentDate;  //Date value Format: mmddyyyy (PossibleNow/Gryphon)
        public String referenceData; //Reference Data, Date Format: xxxxxxxxxxxxxxxxxxxyyyy-mm-dd-hh.mm.ss.xxxxxx (PossibleNow/Gryphon)
        public ADTDNCWrapper (String sender,String phone,String currentDate, String referenceData){
            this.phone=phone;
            this.sender=sender;   
            this.currentDate = currentDate;
            this.referenceData = referenceData;            
        }
    }
}