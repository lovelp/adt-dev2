@isTest
private class  PromoterHelperTest {
     
    static testMethod void testInactivatePromoters(){
        Promoter__c p = new Promoter__c();
        p.PromoterCode__c = 'TestPromoter123'; 
        insert p;
        
        Integer rqsizebefore = Database.countQuery('Select count() from Promoter__c limit 10000');
        
        test.startTest();
        
        PromoterHelper.inactivatePromoters();
            
        test.stopTest();
        
        Integer rqsizeafter = Database.countQuery('Select count() from Promoter__c limit 10000');
        System.assertEquals(rqsizebefore,rqsizeafter, 'Expect no Promoter records to be deleted');
    }
    
    static testMethod void testPromoterDailyLoads(){
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        User u, u2;
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            u.EmployeeNumber__c = 'T_12_Unq';
            update u;
            u2 = TestHelperClass.createSalesRepUserWithManager();
            u2.EmployeeNumber__c = 'T_13_Unq';
            update u2;
        }
        
        //Case 1: promoter inserted and assigned to a user
        Promoter__c p1 = new Promoter__c();
        p1.PromoterCode__c = 'p123';
        p1.PromoterName__c = 'Test123';
        p1.EmployeeNumber__c = 'T_12_Unq'; 
        insert p1;
        p1 = [select id, ownerId from Promoter__c where id = : p1.id];
        system.assertEquals(p1.OwnerId, u.id);
        
        //Case 2: promoter inserted and assigned to globaladmin
        Promoter__c p2 = new Promoter__c();
        p2.PromoterCode__c = 'p124';
        p2.PromoterName__c = 'Test123';
        insert p2;
        p2 = [select id, ownerId from Promoter__c where id = : p2.id];
        system.assertEquals(p2.OwnerId, Utilities.getGlobalUnassignedUser());
        
        //Case 3: promoter inserted and assigned to globaladmin
        p1.EmployeeNumber__c = 'T_13_Unq';
        update p1;
        p1 = [select id, ownerId from Promoter__c where id = : p1.id];
        system.assertEquals(p1.OwnerId, u2.id);
        
        //Case 2: promoter inserted and assigned to globaladmin
        p2.EmployeeNumber__c = null;
        update p2;
        p2 = [select id, ownerId from Promoter__c where id = : p2.id];
        system.assertEquals(p2.OwnerId, Utilities.getGlobalUnassignedUser());               
    }

    static testmethod void testPromoterStatusProcessor() {
        // CRON expression: midnight on March 15.
        // Because this is a test, job executes
        // immediately after Test.stopTest().
        Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        // Schedule the test job
        String jobId = System.schedule('PromoterStatusProcessor',CRON_EXP, new PromoterStatusProcessor());
        Test.stopTest();
    }
}