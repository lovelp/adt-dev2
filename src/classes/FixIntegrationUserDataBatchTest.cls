/*
 * Description: This is a test class for the following classes
 * 1.FixIntegrationUserDataBatch
 * Created By: Ravi Pochamalla
 * Updated BY: Parameswaran Iyer
 *
 */

@isTest
private class FixIntegrationUserDataBatchTest{
    private static void createTestData(){
        Postal_Codes__c pc=new Postal_Codes__c (name='12346', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100');
        insert pc; 
        TestHelperClass.createReferenceDataForTestClasses();
        
        Profile p = [select id from profile where name='System Administrator'];
        integer i= 47598347;
        User integrationUser = new User(alias = 'stand', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='User',firstname='TestIntegration', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
        StartDateInCurrentJob__c = Date.today());
        insert integrationUser;

        i= 3456;
        User integrationUserforAcc = new User(alias = 'stand', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='User',firstname='Integration', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
        StartDateInCurrentJob__c = Date.today());
        insert integrationUserforAcc; 
        system.debug(integrationUserforAcc.name);

        i= 3457;
        User integrationUserforResaleAdm = new User(alias = 'stand', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Admin',firstname='TestGlobal', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
        StartDateInCurrentJob__c = Date.today());
        insert integrationUserforResaleAdm;

        ResaleGlobalVariables__c rgv=new ResaleGlobalVariables__c();
        rgv.Name='DataRecastDisableAccountTrigger';
        rgv.value__c='FALSE';
        insert rgv;
   
        //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;

        Account acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'Unit Test Acct';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        TestHelperClass.inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
        //acct.PostalCodeID__c = inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
        acct.DispositionCode__c = 'X-Other';
        acct.ShippingPostalCode = '221o2';
        acct.Data_Source__c = 'TEST';
        insert acct;        
       
        acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'Unit Test Acct';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.PostalCodeID__c = pc.id;
        acct.DispositionCode__c = 'X-Other';
        acct.ShippingPostalCode = '221o2';
        acct.Data_Source__c = 'TEST';
        acct.ownerid=integrationUserforAcc.id;
        insert acct;       
        Account q=[select id,owner.name from account where id=:acct.id];
        list<Account> acclist=[select Id, Name from Account where owner.name = 'Integration User'];
        system.debug('account inserted'+q.owner.name);
        
        list<Account> chngowner=new list<Account>();
        chngowner.add(acct);
        
        FixIntegrationUserDataBatch db=new FixIntegrationUserDataBatch();
        db.changeOwnership(chngowner);  
    }

    static testMethod void testFixIntegrationUserDataBatch(){
        createTestData();
        Test.startTest();
        //List<selectOption> l = FixIntegrationUserDataBatch.getchangeOwnership();            
        Id batchId=Database.executeBatch(new FixIntegrationUserDataBatch());
        Test.stopTest();
    }
}