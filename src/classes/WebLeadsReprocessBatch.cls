global class WebLeadsReprocessBatch implements Database.Batchable<SObject>, Database.stateful{

    @TestVisible private String[] selectedGroupIds;
    @TestVisible private Integer totalRecsproc = 0;
    @TestVisible private Integer totalFailedRecs = 0;
    @TestVisible private String selErrType;
    @TestVisible private String processName;
    public WebLeadsReprocessBatch(String selGroups, String errType){
        selErrType = errType;
        //selectedGroupIds = selGroups.split(',');
        selectedGroupIds = new list<string>();
        for(String str:selGroups.split(',')){
            selectedGroupIds.add(str.trim());
        }
    } 
    
    private list<DataLoadMapping__mdt> returnDataMapFields(){
        return null;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String queryString = 'SELECT id, Error__c, ZipCode__c, EmailSubject__c, PostalCodeId__c, PromotionCode__c, Status__c, CallData__c, LineOfBusiness__c,Account__c, email__c, FirstName__c, LastName__c, PrimaryPhone__c,FormName__c,PostalcodeFromAccount__c FROM AuditLog__c WHERE Id IN:selectedGroupIds LIMIT 1000';
        return Database.getQueryLocator(queryString);
    }
    
    global void execute(Database.BatchableContext bc, List<AuditLog__c> records){
        try{
        list<Call_Data__c> callDataList = new list<Call_Data__c>();
        list<Account> accnlist = new list<Account>();
        
        if(selErrType.contains('DNIS')){
            String callData = records[0].PromotionCode__c;
            list<DNIS__c> dnis = [Select Id, Name from DNIS__c where Name =:callData Limit 1];
            if(dnis.size() >0 && dnis[0].Id != null){
                for(AuditLog__c aul:records){
                    Call_Data__c cd = new Call_Data__c();
                    cd.Id = aul.CallData__c;
                    cd.DNIS__c = dnis[0].id;
                    callDataList.add(cd);//list for update
                    //Account acc = new Account(id=aul.Account__c, DNIS__c = dnis[0].Name, DNIS_Modified_Date__c = system.today(), DNIS_Modifier_Name__c = Userinfo.getUserId());
                    //accnlist.add(acc);
                    aul.ReprocessError__c = 'Reprocessing is successful.';
                      //Check if there are multiple errors on same record - check for postal code error on dnis error recs
                    if(aul.Error__c.contains('POSTALCODE_ERROR')){
                        aul.Error__c = aul.Error__c.replace('DNIS_ERROR','').replace(';','');
                    }
                    else{
                        aul.Status__c = 'Fixed';
                        aul.Error__c = '';
                    }
                }
            }
            else{
                for(AuditLog__c aul:records){
                    aul.ReprocessError__c = 'DNIS not found.';
                }
            }
            if(callDataList.size()>0){
                Database.update(callDataList, false);
                processName = 'DNIS_ERROR: '+dnis[0].Name;
            }
        }
        if(selErrType.contains('POSTALCODE')){
            String zipCode = records[0].ZipCode__c;
            Map<String, Id> postbusMap = new Map<String, Id>();
            for(Postal_Codes__c ps: [select id, BusinessID__c from Postal_Codes__c where Name=:zipCode limit 10]){
                postbusMap.put(ps.BusinessID__c, ps.Id);
            }
            for(AuditLog__c aul:records){
                String audlogLOB = aul.LineOfBusiness__c.substring(0,4);
                if(postbusMap.containskey(audlogLOB)){
                    aul.PostalCodeId__c = postbusMap.get(audlogLOB);
                    if(String.isBlank(aul.PostalcodeFromAccount__c)){
                        Account accn = new Account();
                        accn.Id = aul.Account__c;
                        accn.PostalCodeID__c = postbusMap.get(audlogLOB);
                        accnlist.add(accn);
                    }
                    aul.ReprocessError__c = 'Reprocessing is successful.';
                    //Check if there are multiple errors - Dnis error along with postal code error
                    if(aul.Error__c.contains('DNIS_ERROR')){
                        aul.Error__c = aul.Error__c.replace('POSTALCODE_ERROR','').replace(';','');
                    }
                    else{
                        aul.Status__c = 'Fixed';
                        aul.Error__c = '';
                    }
                        
                }
                 else{
                     aul.ReprocessError__c = 'Postal code not found';
                 }
                    processName = 'POSTALCODE_ERROR: '+zipCode;
            }            

        }
        if(accnlist.size()>0) 
        	Database.update(accnlist, false);
        Database.update(records, false);
        /*list<AuditLog__c> fixedRecords = new list<AuditLog__c>();
        for(AuditLog__c al:records){
            if(al.Status__c == 'Fixed'){
                fixedRecords.add(al);
            }
        }
        if(fixedRecords.size()>0){
            ADTEBRAPI.sendToEBR(records, accnlist);
            ADTCheetahMailAPI.sendToACKEmail(records);
            }*/
        }
        catch(Exception ex){
            ADTApplicationMonitor.log(ex,'WebLeadsReprocessBatch',ex.getmessage(),ADTApplicationMonitor.CUSTOM_APP.SERVICEPOWER);
        }
    }
    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        //Send an email to the Manager if it cannot be assigned to Sales Rep
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //mail.toAddresses =  new String[]{userinfo.getUserEmail()};
        mail.setTargetObjectId(userinfo.getUserId());
        mail.setSubject(processName+' - Web leads reprocessing complete.');
        mail.setPlainTextBody('Web leads reprocessing has been completed for the group - '+processName);
        mail.setSaveAsActivity(false);
        Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }  

}