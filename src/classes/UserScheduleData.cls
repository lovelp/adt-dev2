/************************************* MODIFICATION LOG ********************************************************************************************
* UserScheduleData
*
* DESCRIPTION : Apex class used to communicate between the admin module and the user schedule module              
*
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera               9/28/2016               - Original Version
*
*                                                   
*/

public class UserScheduleData {
    
    public enum SetupAction {VIEW, NEW_SCHEDULE}

    public SetupAction initAction {get; private set;}
    
    public String userID {get;set;}
    public String BusinessID {get;set;}
    public String Town {get;set;}
    public String Subtown {get;set;}
    
    public UserScheduleData(String uID, String bID, String t, String st){
        this.userID = uID;
        this.BusinessID = bID;
        this.Town = t;
        this.Subtown = st;
        initAction = SetupAction.VIEW;
    }
    
    public UserScheduleData(String uID){
        this.userID = uID;
    }
        
    public void setSetupAction( SetupAction sa){
        initAction = sa;
    }
    
}