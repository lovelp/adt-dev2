public class SendAccountToTelemarQueue implements Queueable, Database.AllowsCallouts{
    
    public Account acc;
    public RequestQueue__c rq;
    public Event ev;
    public SendAccountToTelemarQueue(Account acnt, RequestQueue__c reqq, Event eve){
        acc = acnt;
        rq = reqq;
        ev = eve;
    }

    public void execute(QueueableContext context){
        User u = [SELECT Business_Unit__c, EmployeeNumber__c FROM User WHERE Id =:userinfo.getUserid()];
        IntegrationSettings__c settings = IntegrationSettings__c.getInstance(UserInfo.getUserId());
        Boolean TelemarIDAutoGenerate =  IntegrationSettings__c.getInstance().Telemar_Matrix_Callout_Generate__c;
        if(settings.TelemarCalloutsEnabled__c && settings.TelemarCalloutsAsync__c){
            Datetime startTimestamp = system.now();
            Datetime endTimestamp;
            OutgoingAppointmentMessage om = new OutgoingAppointmentMessage();
            om.ServiceMessage = rq.ServiceMessage__c;
            om.EventID = rq.EventID__c;
            try {
                startTimestamp = Datetime.now();
                om.process();
                system.debug('##Telemar Response'+om);
                if (om.Status != null && om.Status.equalsIgnoreCase('error')){
                    // request was sent and the response received but with some kind of error so mark as such
                    rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCERROR;
                    ev.ScheduleID__c = String.valueof(system.now().getTime()).subString(0,10);
                    update ev;
                }
                else{
                    // request was sent and the response received so mark as success
                    rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCSUCCESS;
                    rq.ResponseScheduleID__c = om.TelemarScheduleID;
                    rq.ResponseTelemarNumber__c = om.TelemarAccountNumber;
                    rq.ResponseTaskCode__c = om.TaskCode;
                    //rq.AccountId__c = a.id;
                    //success
                    ev.ScheduleID__c = om.TelemarScheduleID;
                    ev.TaskCode__c = om.TaskCode;
                    update ev;
                }
            } catch (CalloutException ce) {
                system.debug('##Exception1'+ce);
                endTimestamp = Datetime.now();
                Long durationMs = endTimestamp.getTime() - startTimestamp.getTime();
                rq.ErrorDetail__c = ce.getMessage() + '  ** DURATION (in ms): ' + durationMs;
                rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCERROR;
                ev.ScheduleID__c = String.valueof(system.now().getTime()).subString(0,10);
                update ev;
            } catch (IntegrationException ie) {
                system.debug('##Exception2'+ie);
                rq.ErrorDetail__c =  ie.getMessage();
                rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ASYNCERROR;
                ev.ScheduleID__c = String.valueof(system.now().getTime()).subString(0,10);
                update ev;
            }
            update rq;  
        }
    
    }
}