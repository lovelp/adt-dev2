@isTest
private class PostRefreshSandboxTest {
  	@isTest
    static void testMySandboxPrep() {
        // Insert logic here to create records of the objects that the class you’re testing
        // manipulates.

        Test.startTest();

        Test.testSandboxPostCopyScript(
            new PostRefreshSandbox(), UserInfo.getOrganizationId(),
                UserInfo.getOrganizationId(), UserInfo.getOrganizationName());

        Test.stopTest();
        
        /*for(CommercialMessages__c comm : [Select Name, Message__c from CommercialMessages__c]){
        	system.debug('$$$' + comm);
        }
        
        for(ResaleGlobalVariables__c var : [Select Name, Description__c, Value__c from ResaleGlobalVariables__c]){
        	system.debug('$$$' + var);
        }
        
        for(BatchGlobalVariables__c batch : [Select Name, Description__c, Value__c from BatchGlobalVariables__c]){
        	system.debug('$$$' + batch);
        }
        
        for(PartnerConfiguration__c batch : [Select Name, AccountLookUp__c, AccountUpdateAPI__c, SiteCustomerLookupAPI__c, AllowedLOB__c from PartnerConfiguration__c]){
        	system.debug('$$$' + batch);
        }*/
        
        
        // Insert assert statements here to check that the records you created above have
        // the values you expect.
    }
}