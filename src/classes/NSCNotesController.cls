public without sharing class NSCNotesController {
    
    public String noteText {get;set;}
    public String callTime {get;set;}
    public String tabObjectId {get;set;}
    public String leadAccountId {get;set;}
    public Boolean isWrapUp {get;set;}
    
    public List<Call_Data__c> callDataList;
    public Call_Data__c cd {get;set;}
    public String mydate {get;set;}
    
    public List<CallDataWrapper> noteList {get;set;}
    public static String CallDataKeyPrefix {get;set;}
    
    /**
     *  @Constructor
     */
    public NSCNotesController (){
        CallDataKeyPrefix = Call_Data__c.sobjecttype.getDescribe().getKeyPrefix();
        callDataList = new List<Call_data__c>();
        noteList = new List<CallDataWrapper>();
        this.callTime = '';
        this.isWrapUp = false;
    }
    
    public pagereference populateCurrentNote(){
        if(tabObjectId!=null && tabObjectId!='null'){
            cd = [SELECT id, notes__c,Call_Time__c,Received_By__c,Received_By__r.name,LastModifiedDate FROM Call_Data__c WHERE id=:tabObjectId];
            noteText=cd.notes__c;
            mydate=cd.LastModifiedDate.format();
        }
        return null;
    }
    
    public pagereference getRecordNotesList(){
        if(!String.isBlank(leadAccountid)) {
            if(leadAccountId.substring(0,3)=='00Q'){
                cd.lead__c = leadAccountId;
                cd.account__c = null;
                update cd;
                callDataList = [SELECT id,notes__c,Call_Time__c,Received_By__c,Received_By__r.name,LastModifiedDate FROM Call_Data__c WHERE lead__c=:leadAccountId AND id!=:tabObjectId order by lastmodifiedDate desc];
            }else if(leadAccountId.substring(0,3)=='001'){
                cd.account__c = leadAccountId;
                cd.lead__c = null;
                cd.AccountLastActivityDate__c = [select id,LastActivityDate__c from Account where id=:leadAccountId].LastActivityDate__c;
                update cd;
                callDataList = [SELECT id,notes__c,Call_Time__c,Received_By__c,Received_By__r.name,LastModifiedDate FROM Call_Data__c WHERE account__c=:leadAccountId AND id!=:tabObjectId order by lastmodifiedDate desc];
            }
        }
        if(callDataList.size()>0){
            noteList.clear();
            for(Call_Data__c cd:callDataList){
                if(!string.isBlank(cd.notes__c)){
                    CallDataWrapper cdw=new CallDataWrapper(cd.notes__c,cd.lastModifiedDate.format(),cd.Received_By__r.name);
                    noteList.add(cdw);
                }
            }
        }
        return null;
    }
    
    public PageReference save() {
        system.debug('Note Text is '+noteText);
        // Call time
        if( !String.isBlank(callTime) ){
            try{
                cd.Call_Time__c = Decimal.valueOf( callTime );
                isWrapUp = true;
            }
            catch(Exception err){ } 
        }
        if(tabObjectId=='null' || string.isBlank(tabObjectId)){
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter this on a valid record'); 
             ApexPages.addMessage(myMsg);                
             return null;
        }
        cd.notes__c = noteText;
        cd.Received_By__c = UserInfo.getUserId();
        Update cd;
        mydate=datetime.now().format();
        return null;
    }
    
    public Boolean getPreviousNotes(){
        if(noteList.size()>0){
            return true;
        }    
        return false;
    }
    
    public class CallDataWrapper{
        public String note{get;set;}
        public String lastModifiedDate{get;set;}
        public String receivedBy{get;set;}
        public CallDataWrapper(String noteVal,String modifieddate,String receivedbyname){
            note=noteVal;
            lastModifiedDate=modifieddate;
            receivedBy=receivedbyname;
        }
    
    }
    
    
}