/************************************* MODIFICATION LOG ********************************************************************************************
* AssignmentMonitor
*
* DESCRIPTION : Schedulable class to execute lead and account reassignment batches
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					3/12/2012				- Original Version
*
*													
*/

global class AssignmentMonitor implements Schedulable {
	global void execute(SchedulableContext sc) {
		Database.executeBatch( new AccountAssignmentBatchProcessor() );
		Database.executeBatch( new LeadAssignmentBatchProcessor() );
	}
}