@isTest
public class RecordSearchControllerTest {
    
    static testMethod void testRecordSearchController()
    {
        User current = [Select Id from User where Id=:UserInfo.getUserId()];
        Lead l;
        User u;
        
        ResaleGlobalVariables__c ResaleGlobalVariables = new ResaleGlobalVariables__c(Name = 'DataRecastDisableAccountTrigger', Value__c = 'FALSE');
        ResaleGlobalVariables__c ResaleGlobalVariables1 = new ResaleGlobalVariables__c(Name = 'DataConversion', Value__c = 'FALSE');
        
        insert new List<ResaleGlobalVariables__c>{ResaleGlobalVariables, ResaleGlobalVariables1};
        
        test.startTest();
        Address__c addr = new Address__c();
        addr.Latitude__c = 29.91760000000000;
        addr.Longitude__c = -90.17870000000000;
        addr.Street__c = '65761 E Crystal Ct';
        addr.City__c = 'Westwego';
        addr.State__c = 'LA';
        addr.PostalCode__c = '22102';
        addr.OriginalAddress__c = '56528 E CRYSTAL CT, WESTWEGO, LA, 70094';
        
        insert addr;
        String dispcomments = 'Test copy to account';
        system.runas(current) {
            Account acct = new Account();
            acct.AddressID__c = addr.Id;
            acct.Name = 'Test Acc';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            //acct.PostalCodeID__c = inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
            acct.ShippingPostalCode = '22102';
            acct.Data_Source__c = 'TEST';
            insert acct;
            
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.BusinessID__c = '1100';
            pc.Name = '22102';
            insert pc;
            
            l = TestHelperClass.createLead(current, false, '');
            l.DispositionComments__c = dispcomments;
            l.PostalCode = '22102';
            l.Business_Id__c = '1100 - Residential';
            l.AddressID__c = addr.id;
            l.SiteStreet__c = '65761';
            l.PostalCodeID__c = pc.Id;
            insert l;
            
            try{ 
                //RecordSearchController.redirectPage(l.Id);
                //RecordSearchController.redirectPage(acct.Id);
                RecordSearchController.searchRecords('test',l.id);
                //RecordSearchController.changeOwnerShipLC(acct.Id, l.Id, [Select Id From User Where Id !=: UserInfo.getUserId() limit 1].Id);
                //RecordSearchController.changeOwnerShipLC(acct.Id, null, [Select Id From User Where Id !=: UserInfo.getUserId() limit 1].Id);
                
                LeadSearchController.searchLeadWrapper wrapper = new LeadSearchController.searchLeadWrapper();
                wrapper.leadSearch = l;
                RecordSearchController.orderRecords(wrapper);
                
                RecordSearchController.recordSorting sortingWrapper = new RecordSearchController.recordSorting(acct.Id, 1);
                RecordSearchController.recordSorting sortingWrapper1 = new RecordSearchController.recordSorting(acct.Id, 2);
                //System.assert(sortingWrapper.compareTo(sortingWrapper1) > 0);
            }catch(exception e){}
            test.stopTest();
        }
    }
    
    static testMethod void testRecordSearchController1()
    {
        User current = [Select Id from User where Id=:UserInfo.getUserId()];
        Lead l;
        User u;
        
        ResaleGlobalVariables__c ResaleGlobalVariables = new ResaleGlobalVariables__c(Name = 'DataRecastDisableAccountTrigger', Value__c = 'FALSE');
        ResaleGlobalVariables__c ResaleGlobalVariables1 = new ResaleGlobalVariables__c(Name = 'DataConversion', Value__c = 'FALSE');
        
        insert new List<ResaleGlobalVariables__c>{ResaleGlobalVariables, ResaleGlobalVariables1};
        
        test.startTest();
        Address__c addr = new Address__c();
        addr.Latitude__c = 29.91760000000000;
        addr.Longitude__c = -90.17870000000000;
        addr.Street__c = 'a5761 E Crystal Ct';
        addr.City__c = 'Westwego';
        addr.State__c = 'LA';
        addr.PostalCode__c = '22102';
        addr.OriginalAddress__c = '56528 E CRYSTAL CT, WESTWEGO, LA, 70094';
        
        insert addr;
        String dispcomments = 'Test copy to account';
        system.runas(current) {
            Account acct = new Account();
            acct.AddressID__c = addr.Id;
            acct.Name = 'Test Acc';
            acct.LeadStatus__c = 'Active';
            acct.UnassignedLead__c = false;
            acct.InService__c = true;
            acct.Business_Id__c = '1100 - Residential';
            //acct.PostalCodeID__c = inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
            acct.ShippingPostalCode = '22102';
            acct.Data_Source__c = 'TEST';
            insert acct;
            
            l = TestHelperClass.createLead(current, false, '');
            l.DispositionComments__c = dispcomments;
            l.PostalCode = '22102';
            l.Business_Id__c = '1100 - Residential';
            l.AddressID__c = addr.id;
            l.SiteStreet__c = 'a5761';
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.BusinessID__c = l.Business_Id__c.Contains('1100') ? '1100' : '1200';
            pc.Name = l.PostalCode;
            insert pc;
            
            l.PostalCodeID__c = pc.Id;
            l.OwnerId = UserInfo.getUserId();
            insert l;
            
            try{ 
                //RecordSearchController.changeOwnerShip(l.Id, [Select Id From User Where Id !=: UserInfo.getUserId() limit 1].Id);
                //RecordSearchController.redirectPage(l.Id);
                RecordSearchController.sortedMap(new Map<Id, Integer>{l.Id => 1});
                RecordSearchController.sendnotification(l.Id, Userinfo.getUserId());
                LeadSearchController.searchLeadWrapper wrapper = new LeadSearchController.searchLeadWrapper();
                wrapper.leadSearch = l;
                RecordSearchController.orderRecords(wrapper);
            }catch(exception e){}
            test.stopTest();
        }
    }
}