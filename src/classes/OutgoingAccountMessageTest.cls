/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OutgoingAccountMessageTest {

    static testMethod void testCreateRequest() {
        
        Account a;
        Disposition__c d;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();

            Address__c addr = buildAddr();
            insert addr;
            a.AddressID__c = addr.Id;
            a.TelemarAccountNumber__c = 'Q1112345';
            update a;
            
            a = [
                Select TelemarAccountNumber__c, FirstName__c, LastName__c, Name, Phone, Email__c, Business_Id__c,
                    DispositionComments__c, QueriedSource__c, ReferredBy__c, SiteCity__c, SiteStreet__c,
                    SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, OutboundTelemarAccountNumber__c
                from Account
                where Id = :a.Id
            ];  
            
            
            a.FirstName__c = 'FirstName';
            a.LastName__c = 'LastName';
            a.Name = 'Name';
            a.Phone = '1234567890';
            a.Email__c = 'email@email.com';
            a.DispositionComments__c = 'comments';
            a.QueriedSource__c = 'queriedSource';
            a.ReferredBy__c = 'referredBy';
            a.Business_Id__c = '1200';
            a.MMBBillingSystem__c = 'Informix';
            a.MMBOrderType__c = 'R1';
            a.Channel__c = Channels.TYPE_CUSTOMHOMESALES;
            a.MMBDisconnectDate__c = system.today();
            a.chargeBack__c = 'Y';
            u.EmployeeNumber__c = 'emp123';
            
            update u;
            
            d = new Disposition__c();
            d.AccountID__c = a.Id;
            d.DispositionType__c = '2X - Two-time Non-Pay';
            d.DispositionDate__c = system.now();
            d.Comments__c = 'Disposition Comments';
            insert d;
        }
        
        String serialRequest;
        OutgoingAccountMessage acm;
        test.startTest();
        Test.setMock(WebServiceMock.class, new ADTSalesMobilityFacadeMockImpl());
            system.runas(u) {
                acm = new OutgoingAccountMessage();
                acm.a = a;
                acm.d = d;
                acm.TransactionType = IntegrationConstants.TRANSACTION_DISPOSITION_ONLY;
                serialRequest = acm.createRequest();
                
                acm.a = a;
                acm.d = d;
                acm.TransactionType = IntegrationConstants.TRANSACTION_ACCOUNT + '-DSC';
                serialRequest = acm.createRequest();
                
                
                try{
                	acm.processResponse();
                }catch(Exception ex){}
            }
        test.stopTest();
        
        system.debug('serialRequest = ' + serialRequest);
        
        ADTSalesMobilityFacade.setAccountRequest_element req = (ADTSalesMobilityFacade.setAccountRequest_element) system.Json.deserialize(serialRequest, system.type.forName('ADTSalesMobilityFacade.setAccountRequest_element'));
        
        system.assertEquals(acm.TransactionType, req.TransactionType);
        system.assertEquals( a.OutboundTelemarAccountNumber__c, req.TelemarAccountNumber);
        system.assertEquals( u.EmployeeNumber__c, req.HREmployeeID);
        ADTSalesMobilityFacade.Account_element presale = req.Account;
        system.assertEquals( a.FirstName__c, presale.FirstName);
        system.assertEquals( a.LastName__c, presale.LastName);
        //TODO: revisit based on whether BusinessName will be provided only if account's business id is 1200
        //system.assertEquals( a.Name, presale.BusinessName);
        system.assertEquals( a.Phone, presale.PhoneNumber1);
        system.assertEquals( a.Email__c, presale.Email);
        system.assertEquals( a.ReferredBy__c, presale.ReferredBy);
        
    }
    
    private static Address__c buildAddr() {

        Address__c addr = new Address__c();
        addr.Street__c = 'Street';
        addr.City__c = 'City';
        addr.State__c = 'State';
        addr.PostalCode__c = '12345';
        addr.PostalCodeAddOn__c = '6789';
        addr.CountryCode__c = 'US';
        addr.OriginalAddress__c = '123 address';
        return addr;
    }
}