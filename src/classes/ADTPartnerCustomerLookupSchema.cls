/************************************* MODIFICATION LOG ************************************************************
* ADTPartnerCustomerLookupSchema
*
* DESCRIPTION : Defines the schema of the ADTPartnerCustomerLookupAPI web service
* 
*-------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*-------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan &    09/01/2017    - Original Version
* Shiva Pochamalla
* Siddarth Asokan      09/20/2017    - API split into Account Lookup & Customer Lookup API
*/

global class ADTPartnerCustomerLookupSchema {
    // Request Start
    public class CustomerLookupRequest{
        public String partnerId;
        public String callId;
        public String opportunityId;        
        public String partnerRepName;
    }
    // Request End
    
    // Response Start
    public class CustomerLookupResponse{
        public String message;
        public String opportunityId;
        public String leadManagementId;
        public CreditRating creditRating;
        public list<CustomerSites> sites;
        public list<Serviceability> orderServiceability;
        public ADTPartnerLoanAppAPISchema.LoanApplication loanApplication;
    }
    
    public class CreditRating{
        public String dateRetrieved;
        public String riskGrade;
        public String approvalType;
        public PAYMENT_VALUE paymentFrequency;
        public Integer depositPercent;
        public Boolean multiPayAllowed;
        //public MULTIPAYTERM_VALUE multiPayTerms;
        public List<String> multiPayTerms;
        public Boolean easyPayRequired;
        public String message;
    }
    
    public enum PAYMENT_VALUE {Annual, Quarterly, Monthly}
    public enum MULTIPAYTERM_VALUE {Three}
    public enum STATUS_VALUE {InService, OutOfService}
    public enum CUSTOMERSTATUS_VALUE {Active, InActive}
    public enum SITEFLAG_VALUE {HOA, BHT, CHS}
    public enum LIFESAFTEYFLAG_VALUE {Smoke, CODetection, CellBackup}
    public enum RULETYPE_VALUE {SiteSelection, CustomerSiteSelection, CustomerSelection, PhoneActivation}
    public enum BILLINGSYSTEM_VALUE {MMB, MMBP1, Informix}
    public enum CONTRACTSTATUS_VALUE {Active, Canceled, PendingCancel}
    
    public class CustomerSites{
        public String id;
        public STATUS_VALUE status;
        public String name;
        public String addrLine1;
        public String addrLine2;
        public String city;
        public String state;
        public String postalCode;
        public String postalCodeAddOn;
        public String siteType;
        //public SITEFLAG_VALUE siteFlags;
        public list<String> siteFlags;
        public String systemNumber;
        public String csNumber;
        public String pulseService;
        public String systemType;
        public String panelType;
        //public LIFESAFTEYFLAG_VALUE lifeSafetyFlags;
        public list<String> lifeSafetyFlags;
        public String servicePlan;
        public Customer customer;
        public list<SelectionRule> selectionRules;
        public list<Zones> zones;
        public String radio;
    }
    
    public class Zones implements comparable{
        public String id;
        public String description;
        public String comment;
        public Integer compareTo(Object compareTo){
            Integer returnValue = 0;
            Zones zoneObj = (Zones)compareTo;
            if(Integer.valueOf(zoneObj.id) > Integer.valueOf(id))
                returnValue = -1;
            else if(Integer.valueOf(zoneObj.id) < Integer.valueOf(id))
                returnValue = 1;
            return returnValue;
        }
    }
    
    public class Name{
        public String first;
        public String last;
        public String company;
    }
    
    public class Customer{
        public String id;
        public CUSTOMERSTATUS_VALUE status;
        public Name name;
        public String addrLine1;
        public String addrLine2;
        public String city;
        public String state;
        public String postalCode;
        public String postalCodeAddOn;
        public BILLINGSYSTEM_VALUE billingSystem;
        public String customerType;
        public Integer pastDueDays;
        public Contract contract;
    }
    
    public class Contract{
        public String contractNumber;
        public CONTRACTSTATUS_VALUE status;
        public String startDate;
        public String expirationDate;
        public Disco disco;
        public PastDue pastDue;
        public Decimal currentBalance;
    }
    
    public class Disco{
        public String discoDate;
        public String reason;
        public Integer nonPay;
    }

    public class PastDue{
        public String status;
        public Decimal balance;
    }
    
    public class SelectionRule{
        public RULETYPE_VALUE ruleType;
        public Boolean allowed;
        public String message;
        public String orderType;
    }
    
    public class Serviceability{
        public String name;
        public String orderType;
        public String description;
        public Boolean orderAllowed;
        public Boolean appointmentAllowed;
        public String orderMessage;
        public String appointmentMessage;
    }
    // Response End
}