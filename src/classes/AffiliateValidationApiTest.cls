/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AffiliateValidationApiTest {

    static testMethod void UsaaTest() {
		User u = TestHelperClass.createUser(1000);
		IntegrationSettings__c iSettings = new IntegrationSettings__c();
		iSettings.USAA_Endpoint__c = 'testEndpoint';
		iSettings.USAA_Username__c = 'testUsername';
		iSettings.USAA_Password__c = 'testPass';
		iSettings.USAA_Timeout__c = 60000;		
		insert iSettings;

		System.runAs(u) {
			 // Setup test data
			String MemNumber = '042751881';
			String LastName = 'NOCHE';
			String FirstName = 'ESTA';
			String birthdate = '10/10/1975';
			String city = 'HOUSTON'; 
			String state = 'TX'; 
			String zipcode = '77017'; 
			String email = 'test@test.com';
            String phone = '5619887534';
			
			Test.startTest();
				StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
				mock.setStaticResource('USAAResponseCalloutMock');
				mock.setStatusCode(200);
				mock.setHeader('Content-Type', 'application/json');
				Test.setMock(HttpCalloutMock.class, mock);
				
				AffiliateValidationApi api = new AffiliateValidationApi();
				String sRetValueWithMemNo = api.ValidateUSAAMembership(MemNumber, LastName);
				String sRetValueWithDOB = api.ValidateUSAAMembershipAdvance(FirstName, LastName, birthdate, phone, city, state, zipcode, email);		  		
			Test.stopTest();
		}
    }
}