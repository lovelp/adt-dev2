public with sharing class LoanApplicationPaymentController {

    public String urlChase{get;set;}
    public String transactionDetails{get;set;}
    public String loanId {get;set;}
    public LoanApplicationPaymentController(){
        LoanApplicationDataAccess dataObject = new LoanApplicationDataAccess();
        this.loanId = ApexPages.currentPage().getParameters().get('loanId');
        transactionDetails = '';
        urlChase = dataObject.getPaymentURL(loanId);
        urlChase = urlChase.replace('AND','&');
        //urlChase = 'https://www.chasepaymentechhostedpay.com/hpf/1_1?hostedSecureID=cpt401429197611&action=buildForm&sessionId=Loan-0022&hosted_tokenize=store_only&payment_type=Credit_Card&formType=0&amp;allowed_types=Discover|MasterCard|Visa&amount=null&required=minimum&collectAddress=3&name=Test%20Credit&cardIndicators=Y&zip=32223&callback_url=https://adt--adtdev2--c.cs14.visual.force.com/resource/1414279602000/Paymentech_Callback&css_url=https://adt--adtdev2--c.cs14.visual.force.com/resource/1415367869000/Paymentech_Css';
        System.debug('### chase Url is'+urlChase);
    }
    
     public PageReference postPayment(){
        LoanApplicationDataAccess dataObject = new LoanApplicationDataAccess();
        System.debug('### loan id is'+this.loanId);
        String transactionSuccess  = dataObject.saveProfileId(transactionDetails,this.loanId);
        if(transactionSuccess.equalsIgnoreCase('false')){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Cannot Complete Payment'));
        }       
        return null;
    }
}