public class ADTPartnerSetDLLAppointmentController 
{
    public enum EmailNotificationType {e_NEW, e_CANCEL,e_RESCHEDULE, e_REMINDER}
    
    public class invalidDataException extends Exception {}
    //public Event upcomingCustomerAppointment;
    public String SelectedAgentID;
    public integer apptDuration;
    public Date apptStartDate;
    public Time sameDayStartTime;
    public Map<Id, User> mapTerritoryUser;
    public Map<Id, List<SchedUserTerr__c>> mapSalesRepSchedule;
    public boolean isSameDayAppointment;
    Map<String, Map<Id, FieldSalesAppointmentController.AgentSchedule>> SalesRepAvailableTimeMap;
    ADTPartnerGetDLLAppointmentController getDLLAppointmentController;
    Map<Id, FieldSalesAppointmentController.AgentSchedule> mapSalesRepAvailableTime;
    setDLLAppointmentResponse dllAppointmentResponse;
    public integer statusCode;
    public TimeZone customerTimeZone;
    public ADTPartnerAppointmentHelper apptHelperController;
    public string strJobNumber ;
    
    /*public class setDLLAppointmentRequest
    {
        public setInstallAppointmentRequest setInstallAppointmentRequest;
    }*/
    
    public class setDLLAppointmentRequest
    {
        public string partnerId;
        public string callId;
        public string opportunityId;
        public string partnerRepName;
        public string orderNumber;
        public List<appointmentRequest> appointmentRequest; 
    }
    
    public class appointmentRequest
    {
        public string jobNumber;
        public string installType;
        public string offerId;
        public string appointmentDate;
        public string appointmentTime;
    }
    
    /*public class setDLLAppointmentResponse
    {
        public setInstallAppointmentResponse setInstallAppointmentResponse;
    }*/
    
    public class setDLLAppointmentResponse
    {
        public string opportunityId;
        public string orderNumber;
        public List<appointmentResponse> appointmentRequest;
    }
    
    public class appointmentResponse
    {
        public string jobNumber;
        public string installType;
        public boolean success;
        public string message;
        public string offerId;
        public string appointmentDate;
        public string appointmentTime;
    }
    
    public ADTPartnerSetDLLAppointmentController ()
    {
        dllAppointmentResponse = new setDLLAppointmentResponse ();  
        statusCode = 200;
        customerTimeZone = UserInfo.getTimeZone();
        mapTerritoryUser = new Map<Id, User> ();
        mapSalesRepSchedule = new Map<Id, List<SchedUserTerr__c>> ();
        apptDuration = 0;
        // default the req start date to next date, in case we get blank start date from Partner
        apptStartDate = date.today().addDays(1);
        isSameDayAppointment = false;
        apptHelperController = new ADTPartnerAppointmentHelper ();
        SalesRepAvailableTimeMap = new Map<String, Map<Id, FieldSalesAppointmentController.AgentSchedule>> ();
        strJobNumber = '';
    }
    
    public setDLLAppointmentResponse parseJSONRequest (string jsonRequest)
    {
        setDLLAppointmentRequest appointmenRequestWrapper = new setDLLAppointmentRequest ();
        
        try
        {
            // if blank json request
            if (string.isBlank(jsonRequest))
            {
                createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('400').Error_Message__c);                                         
                statusCode = 400;
            }
            else
            {
                appointmenRequestWrapper = (setDLLAppointmentRequest) system.JSON.deserialize(jsonRequest, setDLLAppointmentRequest.class);
                
                if (string.isNotBlank(appointmenRequestWrapper.partnerId) && string.isNotBlank(appointmenRequestWrapper.callId) && string.isNotBlank(appointmenRequestWrapper.partnerRepName))
                {
                    //system.debug('@@appointmenRequestWrapper.appointmentRequest : '+appointmenRequestWrapper.appointmentRequest.size());
                    if (appointmenRequestWrapper.appointmentRequest != null && appointmenRequestWrapper.appointmentRequest.size() > 0 && string.isNotBlank(appointmenRequestWrapper.opportunityId) && string.isNotBlank(appointmenRequestWrapper.orderNumber))
                    {
                        if (string.isNotBlank(appointmenRequestWrapper.appointmentRequest[0].installType) && appointmenRequestWrapper.appointmentRequest[0].installType.equalsIgnoreCase('phoneActivation'))
                        {
                            string accountId = apptHelperController.validateCallAndOpportunity (appointmenRequestWrapper.opportunityId, appointmenRequestWrapper.callId);
                            if (string.isNotBlank(accountId))
                            {
                                system.debug('get account');
                                
                                List<account> lstAccount = apptHelperController.getAccount (accountId);                    
                                if (lstAccount != null)
                                {
                                    List<scpq__SciQuote__c> lstQuote = apptHelperController.getQuote (appointmenRequestWrapper.orderNumber , lstAccount[0].id);
                                    if (lstQuote != null)
                                    {   
                                        if (string.valueOf(lstQuote[0].scpq__Status__c).equalsIgnoreCase('Submitted') || string.valueOf(lstQuote[0].scpq__Status__c).equalsIgnoreCase('Signed'))
                                        {
                                            strJobNumber = lstQuote[0].MMBJobNumber__c;
                                            system.debug('@@strJobNumber: '+strJobNumber);
                                            setDLLAppointmentSlot (appointmenRequestWrapper, lstAccount[0], lstQuote[0]);
                                        }
                                        else
                                        {
                                            // Order Number not in Signed or Submitted status
                                            createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('OrderStatusNotValid').Error_Message__c.replace('requestID','Order Number'));                                         
                                            statusCode = 200;
                                        }
                                    }
                                    else
                                    {
                                        //Order number not found
                                        createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('404').Error_Message__c.replace('requestID','Order Number'));                            
                                        statusCode = 404; 
                                    }                        
                                }
                                else 
                                { 
                                    // Opportunity ID not found
                                    createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('404').Error_Message__c.replace('requestID','Opportunity ID'));                                       
                                    statusCode = 404; 
                                }  
                            }
                            else
                            {
                                // Opportunity ID and Call Id does not return a common account
                                createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('appointmentCallOppIDError').Error_Message__c);                                       
                                statusCode = 400;
                            }
                        }
                        
                        else
                        {
                            createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('NotPhoneActivation').Error_Message__c);
                            statusCode = 400;
                            //string errMessage = 'Installation Type is not Phone Activation';
                            //ADTApplicationMonitor.log ('ADTPartner Invalid Data Error', errMessage, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                        }
                    } 
                    else
                    {
                        // opportunity id or order number missing
                        createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('400').Error_Message__c);                              
                    	statusCode = 400;
                    	//throw new invalidDataException('opportunity id or order number missing');
                    }
                }
                else
                {
                    //invalid input, missing required elements
                    createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('400').Error_Message__c);                              
                    statusCode = 400;
                    //throw new invalidDataException('invalid input, missing required elements');
                }
            }
            
        }
        catch (Exception ex)
        {
            // something went wrong
            ADTApplicationMonitor.log(ex, 'ADTPartnerSetDLLAppointmentController', 'parseJSONRequest', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('500').Error_Message__c);                              
            statusCode = 500;
        }
        
        return dllAppointmentResponse;
    }
    
    public void setDLLAppointmentSlot (setDLLAppointmentRequest appointmenRequestWrapper, Account acc, scpq__SciQuote__c quote)
    {
        try
        {  
            //if (string.isNotBlank(appointmenRequestWrapper.appointmentRequest[0].appointmentDate) && string.isNotBlank(appointmenRequestWrapper.appointmentRequest[0].appointmentTime))
            //{
                string errMessage = apptHelperController.validateDateTimeFormat (appointmenRequestWrapper.appointmentRequest[0].appointmentDate, appointmenRequestWrapper.appointmentRequest[0].appointmentTime);
                if (string.isBlank(errMessage))
                {
                    if (string.isNotBlank(appointmenRequestWrapper.appointmentRequest[0].appointmentDate))
                    {
                        apptStartDate = date.valueOf(appointmenRequestWrapper.appointmentRequest[0].appointmentDate);
                    }
                    //apptStartDate = date.valueOf(appointmenRequestWrapper.appointmentRequest[0].appointmentDate);
                    
                    // check if the requested appt date is not a past date
                    if (!apptHelperController.pastDateApptStartDate(apptStartDate))
                    {
                        apptDuration = SchedDefaults__c.getInstance('In House Activation').Duration__c.intValue();  
                        // check for slot round time
                        integer intRequestTime = 0;
                        string errMsg = '';
                        integer roundTimeDivisible = Integer.valueOf(PartnerSettings__c.getorgDefaults().DLLRoundTimeDivisible__c);
                        if (roundTimeDivisible == 0 || roundTimeDivisible == null)
                        {
                            roundTimeDivisible = apptDuration;
                        }
                        if (string.isNotBlank(appointmenRequestWrapper.appointmentRequest[0].appointmentTime))
                        {
                            intRequestTime = integer.valueOf(appointmenRequestWrapper.appointmentRequest[0].appointmentTime.substringAfterLast(':'));
                        }
                        if (intRequestTime != 0)
                        {
                            errMsg = apptHelperController.checkForSlotRoundTime(intRequestTime, roundTimeDivisible);
                        }
                        if (string.isBlank(errMsg))
                        {                            
                            // check for valid Job Number
                            if ((appointmenRequestWrapper.appointmentRequest[0].jobNumber == '' || appointmenRequestWrapper.appointmentRequest[0].jobNumber == null) || (appointmenRequestWrapper.appointmentRequest[0].jobNumber != '' && appointmenRequestWrapper.appointmentRequest[0].jobNumber != null && appointmenRequestWrapper.appointmentRequest[0].jobNumber ==  quote.MMBJobNumber__c ))
                            {
                                // validate if the DLL order, proceed only if true
                                if (apptHelperController.validateInHouseActivationOrder (acc.id, appointmenRequestWrapper.orderNumber))
                                {                     
                                    isSameDayAppointment = apptHelperController.sameDayAppointment (apptStartDate);
                                    customerTimeZone = apptHelperController.getcustomerTimeZone (acc);   
                                    
                                    // search for the DLL rep and create an event                     
                                    scheduleAgentAppointment (appointmenRequestWrapper, acc, quote);
                                    
                                }
                                else
                                {
                                    // Not a DLL order
                                    createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('Not Activation Order').Error_Message__c);                             
                                    statusCode = 200; 
                                }
                            }
                            else
                            {
                                // invalid Job Number
                                createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('InvalidJobNumber').Error_Message__c);                             
                                statusCode = 400; 
                            }
                        }
                        else
                        {
                            // invalid appt time
                            createErrorResponse (appointmenRequestWrapper, errMsg);                             
                            statusCode = 400; 
                        }                                                
                    }
                    else
                    {
                        // invalid start date
                        createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('pastStartDate').Error_Message__c);                             
                        statusCode = 400; 
                        //throw new invalidDataException('Invalid Start date');
                    } 
                }
                else
                {
                    // invalid date or time format
                    createErrorResponse (appointmenRequestWrapper, errMessage);                             
                    statusCode = 400; 
                    //throw new invalidDataException('invalid time');
                }
            /*}
            else
            {
                // appointment start date or time missing
                createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('400').Error_Message__c);                             
                statusCode = 400;
                //throw new invalidDataException('start date time missing');
            }*/
            
        }
        catch (Exception ex)
        {
            // something went wrong
            ADTApplicationMonitor.log(ex, 'ADTPartnerSetDLLAppointmentController', 'setDLLAppointmentSlot', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('500').Error_Message__c);                              
            statusCode = 500;
        }
    }
    
    private boolean scheduleAgentAppointment (setDLLAppointmentRequest appointmenRequestWrapper, Account acc, scpq__SciQuote__c quote)
    {
        try
        {
            if( isSameDayAppointment )
            {
                sameDayStartTime = apptHelperController.getSameDayStartTime (apptStartDate, customerTimeZone);                
            }
            
            // there is no other appointments scheduled yet
            string errString = apptHelperController.verifyUpcomingAppointments(acc, customerTimeZone);
            if( string.isNotBlank(errString) )
            {
                system.debug('Cannot save this appointment.' + errString);
                createErrorResponse (appointmenRequestWrapper, errString);
                statusCode = 200;                
                return false;
            }
            
            // Find first available technician for in-house activation                       
            mapSalesRepAvailableTime = new Map<Id, FieldSalesAppointmentController.AgentSchedule> ();
            
            apptHelperController.getUserScheduleSetting (apptStartDate);
            mapTerritoryUser = apptHelperController.mapTerritoryUser;
            mapSalesRepSchedule = apptHelperController.mapSalesRepSchedule;
            
            mapSalesRepAvailableTime = apptHelperController.getUserScheduleByEvent( mapTerritoryUser, apptDuration, apptStartDate, customerTimeZone, sameDayStartTime );
            system.debug('@@getUserScheduleByEvent.mapSalesRepAvailableTime: '+mapSalesRepAvailableTime);
            
            mapSalesRepAvailableTime = apptHelperController.getUserBySchedule( mapSalesRepAvailableTime, mapSalesRepSchedule, apptDuration, apptStartDate, mapTerritoryUser,customerTimeZone  );
            system.debug('@@getUserBySchedule.mapSalesRepAvailableTime: '+mapSalesRepAvailableTime);
            
            SalesRepAvailableTimeMap = new Map<String, Map<Id, FieldSalesAppointmentController.AgentSchedule>>();
            SalesRepAvailableTimeMap.put( apptStartDate.format(), mapSalesRepAvailableTime.clone());
            
            List<string> SelectedAvailableTimeSlot = new List<string> ();
            if (string.isNotBlank(appointmenRequestWrapper.appointmentRequest[0].appointmentTime))
            {
                for (appointmentRequest objApptRequest : appointmenRequestWrapper.appointmentRequest)
                {
                    SelectedAvailableTimeSlot.add(objApptRequest.appointmentTime);
                }
            }
            else
            {
                string apptTime = getFirstSlotOfNextDay(apptStartDate.format());
                if (string.isNotBlank(apptTime))
                {
                    SelectedAvailableTimeSlot.add(apptTime);
                }                
            }
            system.debug('@@SelectedAvailableTimeSlot: '+SelectedAvailableTimeSlot.size());
            if (SelectedAvailableTimeSlot == null || SelectedAvailableTimeSlot.isEmpty() || SelectedAvailableTimeSlot.size() == 0)
            {
                system.debug('No time slot available for appointment tomorrow');
                createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('NoTimeSlotAvailable').Error_Message__c);
                statusCode = 200;                
                return false;
            }
            
            Time SelectedAvailableTime = apptHelperController.getTimeFromString( SelectedAvailableTimeSlot );
            System.debug('$$$$SelectedAvailableTime'+SelectedAvailableTime);
            if(String.isBlank(SelectedAgentID) )
            {
                SelectedAgentID = getSelectedAgentIDFromSchedule( SelectedAvailableTime, apptStartDate.format() );
            }
            system.debug('@@SelectedAgentID: '+SelectedAgentID);
            
            // Read and validate the fields sales agent selected
            if( Utilities.isEmptyOrNull(SelectedAgentID)  )
            {
                system.debug('Error while saving appointment. No agent received.');
                createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('No Agent Available').Error_Message__c);
                statusCode = 200;                
                return false;
            }
            
            Event e = CreateAppointment(SelectedAgentID, acc, apptStartDate, appointmenRequestWrapper.orderNumber, SelectedAvailableTime );
            
            String appRes;           
            
            try {
                // need to update the account before creating an event on this user
                update acc;
                // attempt to create appointment
                EventManager.createEvent(e);
            }
            catch(Exception eErr) {
                appRes = eErr.getMessage();
                ADTApplicationMonitor.log(eErr, 'ADTPartnerSetDLLAppointmentController', 'scheduleAgentAppointment', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            }
            
            // If the event was created successfully send an email notification to this rep
            if( !String.isBlank(e.Id))
            {
                // Fix for HRM - 3915 (Sales Appointment Date not populating when the appointment is created in Matrix)
                acc.SalesAppointmentDate__c = e.StartDateTime;
                if (EventUtilities.shouldSetTaskCodeForAccount(e.TaskCode__c, acc.Type)) {  
                    acc.TaskCode__c = e.TaskCode__c;
                }
                update acc;
                // In case of same day appointments notify by SMS if enabled for this user
                if( isSameDayAppointment )
                {
                    EmailMessageUtilities.NotifyUserBySMS( e.Id, false );                    
                    SendAppointmentNotificationToManager( SelectedAgentID, e.Id, EmailNotificationType.e_NEW );                    
                }
                EmailMessageUtilities.SendAppointmentNotification(e.Id, SelectedAgentID);
            }
            
            // A non-empty value received here means an error occurred
            if( !String.isBlank(appRes) )
            {
                if(appRes.contains('DUPLICATE_VALUE'))
                {
                    system.debug('The DLL tech attempting to be scheduled is no longer available.  Please perform the search again to see if that time slot is still available with another DLL Tech or book another time slot.');
                    createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('RepNoLongerAvailable').Error_Message__c);
                	statusCode = 200;                    
                }
                else
                {
                    system.debug('Error while creating appointment. '+ appRes);                    
                    createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('500').Error_Message__c);                              
                    statusCode = 500;                                    
                }
                return false;
            }
            
            system.debug('appointmenRequestWrapper.appointmentRequest[0].jobNumber: '+appointmenRequestWrapper.appointmentRequest[0].jobNumber);
            system.debug('quote.MMBJobNumber__c: '+quote.MMBJobNumber__c);
            //setInstallAppointmentResponse objResponse = new setInstallAppointmentResponse ();
            dllAppointmentResponse.opportunityId = appointmenRequestWrapper.opportunityId;
            dllAppointmentResponse.orderNumber = appointmenRequestWrapper.orderNumber;
            appointmentResponse objApptResponse = new appointmentResponse ();
            objApptResponse.jobNumber = (appointmenRequestWrapper.appointmentRequest[0].jobNumber != null && appointmenRequestWrapper.appointmentRequest[0].jobNumber != '') ? appointmenRequestWrapper.appointmentRequest[0].jobNumber : quote.MMBJobNumber__c;
            objApptResponse.installType = appointmenRequestWrapper.appointmentRequest[0].installType;
            objApptResponse.success = true;
            objApptResponse.message = PartnerAPIMessaging__c.getinstance('Set DLL Success').Error_Message__c;            
            objApptResponse.appointmentDate = appointmenRequestWrapper.appointmentRequest[0].appointmentDate != null ? appointmenRequestWrapper.appointmentRequest[0].appointmentDate : string.valueOf(apptStartDate);
            objApptResponse.appointmentTime = appointmenRequestWrapper.appointmentRequest[0].appointmentTime != null ? appointmenRequestWrapper.appointmentRequest[0].appointmentTime : SelectedAvailableTimeSlot[0].substringBeforeLast(':') ;
            string offerId = objApptResponse.appointmentDate.replace('-', '') + objApptResponse.appointmentTime.replace(':', '') + string.valueOf(SelectedAvailableTime.addMinutes(apptDuration)).substringBeforeLast(':').replace(':', '');
            objApptResponse.offerId = (appointmenRequestWrapper.appointmentRequest[0].offerId != null && appointmenRequestWrapper.appointmentRequest[0].offerId != '') ? appointmenRequestWrapper.appointmentRequest[0].offerId : offerId;
            List<appointmentResponse> lstApptResponse = new List<appointmentResponse> ();
            lstApptResponse.add(objApptResponse);
            dllAppointmentResponse.appointmentRequest = lstApptResponse;  
            //dllAppointmentResponse.setInstallAppointmentResponse = objResponse;
            statusCode = 200;
            
            return true;
        }
        catch (Exception ex)
        {
            system.debug('@@Exception Occurred: ' + ex.getLineNumber() + ex.getMessage() + ex.getCause() );
            ADTApplicationMonitor.log(ex, 'ADTPartnerSetDLLAppointmentController', 'scheduleAgentAppointment', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            createErrorResponse (appointmenRequestWrapper, PartnerAPIMessaging__c.getinstance('500').Error_Message__c);
            statusCode = 500;
            return false;
        }
    }
    
    private void createErrorResponse (setDLLAppointmentRequest appointmenRequestWrapper, string errMessage)
    {
        //setInstallAppointmentResponse objResponse = new setInstallAppointmentResponse ();
        dllAppointmentResponse.opportunityId = (appointmenRequestWrapper.opportunityId != null) ? appointmenRequestWrapper.opportunityId : '';
        dllAppointmentResponse.orderNumber = (appointmenRequestWrapper.orderNumber != null) ? appointmenRequestWrapper.orderNumber : '';
        appointmentResponse objApptResponse = new appointmentResponse ();
        objApptResponse.jobNumber = (appointmenRequestWrapper.appointmentRequest!= null && appointmenRequestWrapper.appointmentRequest[0].jobNumber != null  && appointmenRequestWrapper.appointmentRequest[0].jobNumber != '') ? appointmenRequestWrapper.appointmentRequest[0].jobNumber : strJobNumber;
        objApptResponse.installType = (appointmenRequestWrapper.appointmentRequest != null && appointmenRequestWrapper.appointmentRequest[0].installType != null) ? appointmenRequestWrapper.appointmentRequest[0].installType : '';
        objApptResponse.success = false;
        objApptResponse.message = errMessage;        
        objApptResponse.appointmentDate = (appointmenRequestWrapper.appointmentRequest != null && appointmenRequestWrapper.appointmentRequest[0].appointmentDate != null) ? appointmenRequestWrapper.appointmentRequest[0].appointmentDate : string.valueOf(apptStartDate);
        objApptResponse.appointmentTime = (appointmenRequestWrapper.appointmentRequest != null && appointmenRequestWrapper.appointmentRequest[0].appointmentTime != null) ?appointmenRequestWrapper.appointmentRequest[0].appointmentTime : '';
        objApptResponse.offerId = (appointmenRequestWrapper.appointmentRequest != null && appointmenRequestWrapper.appointmentRequest[0].offerId != null && appointmenRequestWrapper.appointmentRequest[0].offerId != '') ? appointmenRequestWrapper.appointmentRequest[0].offerId : '';        
        List<appointmentResponse> lstApptResponse = new List<appointmentResponse> ();
        lstApptResponse.add(objApptResponse);
        dllAppointmentResponse.appointmentRequest = lstApptResponse;
        //dllAppointmentResponse.setInstallAppointmentResponse = objResponse;
    }
    
    
    public Event CreateAppointment(Id OwnerID, account a , date SchDate, string ActivationQuoteId, Time SelectedAvailableTime)
    {
        Event e;
        // The account owner should be changed to the sales rep when the appt is being set
        a.OwnerID = SelectedAgentID;
        a.Rep_User__c = SelectedAgentID;
        // Create event record or use an existing appointment record for the reschedule
        EventDefaults__c ed = EventDefaults__c.getInstance();
        System.debug('$$$$ed'+ed);
        e = new Event();
        //save the timezoneid
        e.TimeZoneId__c = customerTimeZone.getId();
        e.WhatId = a.Id;
        // Set appointment record type
        RecordType rt = [select id, name, sobjecttype from RecordType where sobjectType = 'Event' AND Name = :RecordTypeName.COMPANY_GENERATED_APPOINTMENT];
        e.RecordTypeId = rt.Id;
        
        String sTempSubjectCityState = EventManager.getAcctCityState(e.WhatId);
        // TO-DO: Task code on reschedule
        
        e.TaskCode__c = 'DLL';
        e.Subject = EventManager.SUBJECT_PREFIX + e.TaskCode__c + sTempSubjectCityState; 
        
        Integer offsetHourValue = ((customerTimeZone.getOffset(SchDate))/1000/60/60) * (-1); 
        e.StartDateTime = DateTime.newInstanceGmt(SchDate,  SelectedAvailableTime).addHours(offsetHourValue);
        
        // DLL Appointment duration is setup in minutes
        e.EndDateTime = e.StartDateTime.addMinutes(apptDuration);
        
        // Additional appointment information
        //e.Description = AppointmentComments;
        e.Status__c = EventManager.STATUS_SCHEDULED;
        e.IsReminderSet = ed.IsReminderSet__c;
        
        e.ReminderDateTime = e.StartDateTime.addMinutes( -1 * Integer.valueOf( ed.ReminderMinutesBeforeEvent__c ) );
        e.IncomingType__c = EventManager.INCOMING_TYPE_SG;
        e.OwnerId = SelectedAgentID;
        e.Appointment_Type__c = 'In House Activation';        
        a.Data_Source__c = '';
        e.Quote_Id__c = ActivationQuoteId;
           
        return e;
    }
    
    
    public string getFirstSlotOfNextDay (String SelectedDate)
    {
        string apptTime = '';
        mapSalesRepAvailableTime = SalesRepAvailableTimeMap.get(SelectedDate);
        system.debug('@@mapSalesRepAvailableTime: '+mapSalesRepAvailableTime);
        if( mapSalesRepAvailableTime != null && !mapSalesRepAvailableTime.isEmpty() )
        {
            List<FieldSalesAppointmentController.AgentSchedule> aL = new List<FieldSalesAppointmentController.AgentSchedule>();
            aL.addAll(mapSalesRepAvailableTime.values());
            aL.sort();
            system.debug('@@aL: '+aL);
            apptTime = string.valueOf(aL[0].startTime) ;
        }
        system.debug('@@apptTime: '+apptTime);
        return apptTime;
    }
    
    public String getSelectedAgentIDFromSchedule(Time selTimeEl, String SelectedDate)
    {       
        mapSalesRepAvailableTime = SalesRepAvailableTimeMap.get(SelectedDate);
        
        if( mapSalesRepAvailableTime != null && !mapSalesRepAvailableTime.isEmpty() )
        {
            List<FieldSalesAppointmentController.AgentSchedule> aL = new List<FieldSalesAppointmentController.AgentSchedule>();
            aL.addAll(mapSalesRepAvailableTime.values());
            aL.sort();
            System.debug('$$$$mapSalesRepAvailableTime'+mapSalesRepAvailableTime);
           
            for( FieldSalesAppointmentController.AgentSchedule aSch : aL )
            {
                for( FieldSalesAppointmentController.AgentSlot asEl: aSch.AgentAvailableSlotList)
                {
                    System.debug('$$$$AgentAvailableSlotList'+ aSch.AgentAvailableSlotList);
                    if( selTimeEl >= asEl.startTime  && selTimeEl <= asEl.endTime )
                    {
                        system.debug('@@@ agent found = '+aSch);
                        return aSch.userId;
                    }
                }
            }
        }
        return '';
    }
    
    public void SendAppointmentNotificationToManager(String agentId, String eId, EmailNotificationType eType)
    {
        system.debug('@@ sending out notification to manager...');
        try
        {
            // read manager for this user
            User activationAgent = [SELECT Name, Id, ManagerId, UserRole.ParentRoleId, UserRole.Name, UserRoleId FROM User WHERE Id = :agentId];
            Id activationAgentManagerId = null;
            if( !String.isBlank(activationAgent.ManagerId) )
            {
                activationAgentManagerId = activationAgent.ManagerId;
            }
            // This user does not have a manager setup, try to read one from parent role
            else 
            {
                List<User> activationAgentManagerList = [SELECT Id, UserRole.Name, UserRoleId FROM User WHERE UserRoleId = :activationAgent.UserRole.ParentRoleId AND IsActive = true];
                if( activationAgentManagerList != null && !activationAgentManagerList.isEmpty() )
                {
                    activationAgentManagerId = activationAgentManagerList[0].Id;
                }
            }
            // if we could locate a manager for this user
            if( !String.isBlank(activationAgentManagerId) )
            {
                String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
                String alertContent = '';
                String alertSubject = 'Express Activation Appointment - ';
                
                // Change content and subject of the email alert depending on the type of action taken
                // New
                if( eType == EmailNotificationType.e_NEW )
                { 
                    alertSubject += 'NEW';
                    alertContent = 'New InHouse express activation appointment scheduled for ';
                }
                // Cancellation
                if( eType == EmailNotificationType.e_CANCEL )
                { 
                    alertSubject += 'CANCELLED';
                    alertContent = 'Cancelled InHouse express activation appointment for ';
                }
                // Reminder
                else if( eType == EmailNotificationType.e_REMINDER )
                { 
                    alertSubject += 'REMINDER';
                    alertContent = 'This is a friendly reminder of InHouse express activation appointment for ';
                }
                // Reschedule
                else if( eType == EmailNotificationType.e_RESCHEDULE )
                { 
                    alertSubject += 'RESCHEDULE';
                    alertContent = 'Reschedule InHouse express activation appointment for ';
                }
                
                alertSubject += ' - '+ activationAgent.Name;
                alertContent += activationAgent.Name;
                
                EmailMessageUtilities.SendEmailNotification(activationAgentManagerId, alertSubject, alertContent, sfdcBaseURL+'/'+eId, true);
            }
        }
        catch(Exception err)
        {
            // error while sending email notification to manager
            ADTApplicationMonitor.log(err, 'ADTPartnerSetDLLAppointmentController', 'SendAppointmentNotificationToManager', ADTApplicationMonitor.CUSTOM_APP.PARTNER);            
            system.debug('[ADTPartnerSetDLLAppointmentController]::[SendAppointmentNotificationToManager] ERROR: '+err.getMessage()+' \n TRACE: '+err.getStackTraceString());
        }
    }
    
}