/************************************* MODIFICATION LOG ********************************************************************************************
 * ADTEBRAPI
 *
 * DESCRIPTION : Serves as a class to be used for invocation of AdtCustContactEBRService and handling the API response
 *
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE              TICKET               REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Parameswaran Iyer             08/22/2018        HRM - 4050           - Original Version
 * Kalyani Pinisetti             02/25/2019        HRM - 8518           - Added Additional fields in One Click Feed
 * Siddarth Asokan               03/29/2019        ECOM - 967           - Modified the call to One click 
 */

public class ADTEBRAPI{
   /**
    * Wrapper class to store request of EBR
    **/
    public class EBRRequestWrapper{

        public String sender{get;set;}
        public String phone{get;set;}
        public String currentDate{get;set;}
        public String referenceInfo{get;set;}
        public String referenceData{get;set;}   

        public EBRRequestWrapper(String sender,String phone,String currentDate,String referenceInfo,String referenceData){
            this.sender=sender;
            this.phone=phone;
            this.currentDate=currentDate;
            this.referenceInfo=referenceInfo;
            this.referenceData=referenceData;
        }
    }
    
   /**
    * This method constructs the JSON & performs callout
    * Called from SciQuoteTriggerHelper & ADTWebLeadsController
    * @Param Id Account Id & Audit Log Id
    * @ReturnType void
    **/
    public static void sendToEBR(Id accountId, Id aLogId){
        List<RequestQueue__c> reqQueuesToInsert = new List<RequestQueue__c>();
        for(account acct : [Select Id,phone,PhoneNumber2__c,PhoneNumber3__c,PhoneNumber4__c,TelemarAccountNumber__c from Account Where Id =: accountId AND TelemarAccountNumber__c != null]){
            // For the ECOM flow, the audit log Id will be null
            reqQueuesToInsert = createEBRRequestQueues(acct,aLogId);
        }
        // Insert Req Queue List
        if(reqQueuesToInsert.size()>0){
            insert reqQueuesToInsert;
        }
    }
    //For bulk processing
    public static void sendToEBR(list<AuditLog__c> alogs, list<Account> accnlist){
        list<RequestQueue__c> finalReqlist = new list<RequestQueue__c>();
        set<Id> accntlist = new set<Id>();
        Map<Id, Id> audaccntMap = new Map<Id, Id>();
        for(AuditLog__c al: alogs){
            audaccntMap.put(al.Account__c, al.Id);
        }
        for(Account a: accnlist){
            accntlist.add(a.Id);
        }
        for(Account acc: [Select Id, phone,PhoneNumber2__c,PhoneNumber3__c,PhoneNumber4__c,TelemarAccountNumber__c 
                              FROM Account Where Id IN :accntlist AND TelemarAccountNumber__c != null]){
            List<RequestQueue__c> reqQueuesToInsert = new List<RequestQueue__c>();
            reqQueuesToInsert = createEBRRequestQueues(acc,audaccntMap.get(acc.Id));
            finalReqlist.addAll(reqQueuesToInsert);
        }
        // Insert Req Queue List
        if(finalReqlist.size()>0){
            insert finalReqlist;
        }
    }
    
    /**
     * This method creates the EBR request queues
     * @Param Account
     * @ReturnType list of RequestQueue__c
     **/
     public static list<RequestQueue__c> createEBRRequestQueues(Account acc,Id aLogId){
        String dateStr = system.now().format('MMddyyyy');
        String telemarId = String.valueOf(acc.TelemarAccountNumber__c);
        String trimmedTelemarId = '';
        if(String.isNotBlank(telemarId)){
            if(telemarId.length() >= 8){
                trimmedTelemarId = telemarId.right(8);
            }else if(telemarId.length() < 8){
                trimmedTelemarId = telemarId.leftPad(8-telemarId.length(), '0');
            }
        }
        
        Set<String> formatedPhoneNumbers = new Set<String>();
        String primaryPhone;
        //Account phone
        if(acc.phone!=null){
            String phNo = Utilities.getFormattedPhoneForWebleads(String.valueOf(acc.phone));
            if(String.isNotBlank(phNo)){
                formatedPhoneNumbers.add(phNo);
                primaryPhone = phNo;
            }
        }
        
        // Cell Phone
        if(acc.PhoneNumber2__c!=null){
            String cellphNo = Utilities.getFormattedPhoneForWebleads(String.valueOf(acc.PhoneNumber2__c));
            if(String.isNotBlank(cellphNo)){
                formatedPhoneNumbers.add(cellphNo);
            }
        }
        
        // Alternate Phone
        if(acc.PhoneNumber3__c!=null){
            String altphNo = Utilities.getFormattedPhoneForWebleads(String.valueOf(acc.PhoneNumber3__c));
            if(String.isNotBlank(altphNo)){
                formatedPhoneNumbers.add(altphNo);
            }
        }
        
        // Spouse Phone
        if(acc.PhoneNumber4__c!=null){
            String spousephNo = Utilities.getFormattedPhoneForWebleads(String.valueOf(acc.PhoneNumber4__c));
            if(String.isNotBlank(spousephNo)){
                formatedPhoneNumbers.add(spousephNo);
            }
        }
        
        List<RequestQueue__c> reqQueuesList = new List<RequestQueue__c>();
        // Send request for each phone number
        for(String phone: formatedPhoneNumbers){
            // Create JSON to store in service message
            String refData = String.isNotBlank(trimmedTelemarId)?trimmedTelemarId:telemarId;
            refData += 'H'+ phone +String.valueOf(System.now()).replace(' ','-').replace(':','.')+'.000000';
            String jsonString = JSON.serialize(new EBRRequestWrapper('SFDC',phone,dateStr,telemarId,refData));
            System.debug('EBR JSON: '+jsonString);
            
            // Create Request queue for each phone number
            RequestQueue__c rq = new RequestQueue__c();
            rq.AccountID__c = acc.Id;
            if(aLogId != null){
                rq.AuditLog__c = aLogId;
            }
            rq.RequestStatus__c = 'Ready';
            rq.ServiceTransactionType__c = 'sendEBRRequest';
            rq.MessageID__c = phone;
            rq.ServiceMessage__c = jsonString;
            rq.RequestDate__c = system.now();
            rq.Counter__c = 0;
            if(phone == primaryPhone){
                rq.IsPrimary__c = true;
            }
            reqQueuesList.add(rq);
        }
        return (reqQueuesList.size()>0)?reqQueuesList:null;
     }
     
    /**
     * This method is used to make the callout to AdtCustContactEBRService
     * @Param jsonString
     * @ReturnType Void
     **/
    public static RequestQueue__c doEBRCallOut(RequestQueue__c requeue){
        String userName = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPUsername__c:'abc';
        String password = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPPassword__c:'def';
        String endPointURL = (!Test.isRunningTest())? IntegrationSettings__c.getInstance().DPEBREndpoint__c:'http:abc.com';
        Decimal timeout = (!Test.isRunningTest())? IntegrationSettings__c.getInstance().DPEBRTimeout__c:6000;
        Blob headerValue = Blob.valueOf(userName + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        Httprequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type', 'application/json');
        request.setEndpoint(endPointURL);
        request.setTimeout(Integer.ValueOf(timeout));
        request.setMethod('POST');
        request.setBody(requeue.ServiceMessage__c);
        system.debug('EBR Request: '+request);
        try{
            // Send the request
            response = http.send(request);
        }catch(exception e){
            requeue.ErrorDetail__c = 'Exception: '+ e.getMessage();
        }
        system.debug('EBR Response: '+response);
        if(response != null && (response.getStatusCode() == 204 || response.getStatusCode() == 200)){
            // success response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_SUCCESS;
            requeue.ErrorDetail__c = null;
            /*Initialize onclick
            if(requeue.IsPrimary__c){
                // For the ECOM flow, the audit log Id will be null
                ADTOneClickAPI.createOneClickReqQueue(requeue.AccountID__c,requeue.AuditLog__c);
            }*/
        }
        else{
            // error response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            requeue.ErrorDetail__c = String.isNotBlank(requeue.ErrorDetail__c)? requeue.ErrorDetail__c + '\n EBR Response: ' +response.getbody() : response.getbody();
        }
        return requeue;
    }
}