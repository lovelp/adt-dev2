/************************************* MODIFICATION LOG ********************************************************************************************
* RequestQueueRetentionMonitor
*
* DESCRIPTION : Implements Schedulable and is responsible for initiating the process of removing unneeded Request Queue records
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner                  3/5/2012                - Original Version
*
*                                                   
*/

global class RequestQueueRetentionMonitor implements Schedulable {

    global void execute(SchedulableContext SC) {
        
        RequestQueueHelper.deleteExpiredItems();
         
   }

}