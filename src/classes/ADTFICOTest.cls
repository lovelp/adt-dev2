@isTest
private class ADTFICOTest {
	private static Account createTestData(Boolean isMissingData){
		//
        // Create test data
        //
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
            TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        
        Account a;
		User current = [Select Id from User where Id = :UserInfo.getUserId()];
		
        System.runAs(current) { 
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
            insert addr;
            
            Postal_Codes__c pc = new Postal_Codes__c();
            pc.Name = '221o2';
            pc.APM_PostalCode__c = true;
            insert pc;
            
            a = TestHelperClass.createAccountData(addr, true);
            a.FirstName__c = isMissingData?null:'TestFName';
            a.LastName__c = 'TestLName';
            a.Name = 'TestFName TestLName';
            a.Channel__c = Channels.TYPE_RESIDIRECT;
            a.TelemarAccountNumber__c = 'AA00112279';
            a.DOB_encrypted__c = '01/01/1990';
            a.PostalCodeID__c = pc.Id;
            a.PreviousStreet__c =  'Test';
            if(isMissingData) a.AddressID__c = null;
            update a;
            
            // settings
            Map<String, String> EquifaxSettings = new Map<String, String>
                        { 'Password' => ''
                        , 'CreditCheckAttempts'=>'3'
                        , 'Order Types For Credit Check'=>'N1;R1'
                        , 'Failure Risk Grade'=>'X'
                        , 'Failure Risk Grade Display Value'=>'APPROV'
                        , 'Failure Condition Code'=>'APPROV'
                        , 'Days to allow additional check'=>'90'
                        , 'Default Risk Grade'=>'W'
                        , 'Default Risk Grade Display Value'=>'(Credit not run) Approved: Annual, Easypay, Full Deposit'
                        , 'Default Condition Code'=>'CAE1'
                        , 'URL'=>''
                        , 'MessageId'=>'1014'
                        , 'Login ID'=>''};
            List<Equifax__c> EquifaxSettingList = new List<Equifax__c>(); 
            for( String sN: EquifaxSettings.keySet()){
                Equifax__c eS = new Equifax__c( name = sN );
                eS.value__c = EquifaxSettings.get(sN);
                EquifaxSettingList.add( eS );
            }
            insert EquifaxSettingList;
            Equifax_Conditions__c EquifaxConditionSetting1 = new Equifax_Conditions__c( Name='CAE1');
            EquifaxConditionSetting1.Agent_Message__c = 'No Payment Restrictions';
            EquifaxConditionSetting1.Agent_Quoting_Message__c = 'No Payment Restrictions';
            EquifaxConditionSetting1.Customer_Message__c = 'We were able to pull your credit and we are able offer you our best pricing';
            EquifaxConditionSetting1.Deposit_Required_Percentage__c = 0.0;
            EquifaxConditionSetting1.EasyPay_Required__c = true;
            EquifaxConditionSetting1.Payment_Frequency__c = 'A';
            EquifaxConditionSetting1.Three_Pay_Allowed__c = true;
            insert EquifaxConditionSetting1;
            
            Equifax_Conditions__c EquifaxConditionSetting2 = new Equifax_Conditions__c( Name='APPROV');
            EquifaxConditionSetting2.Agent_Message__c = 'No Payment Restrictions';
            EquifaxConditionSetting2.Agent_Quoting_Message__c = 'No Payment Restrictions';
            EquifaxConditionSetting2.Customer_Message__c = 'We were able to pull your credit and we are able offer you our best pricing';
            EquifaxConditionSetting2.Deposit_Required_Percentage__c = 0.0;
            EquifaxConditionSetting2.EasyPay_Required__c = false;
            EquifaxConditionSetting2.Payment_Frequency__c = 'A';
            EquifaxConditionSetting2.Three_Pay_Allowed__c = true;
            insert EquifaxConditionSetting2;
            
            Equifax_Mapping__c eMap = new Equifax_Mapping__c();
            eMap.CreditScoreStartValue__c = '';
            eMap.CreditScoreEndValue__c = '';
            eMap.ApprovalType__c = 'CAE1';
            eMap.RiskGrade__c = 'Y';
            eMap.RiskGradeDisplayValue__c = '(NO Hit) Approved: Annual, Easypay, Full Deposit';
            eMap.NoHit__c = true;
            eMap.HitCode__c = '2';
            eMap.HitCodeResponse__c = '';
            insert eMap;
            
            Equifax_Mapping__c eMap2 = new Equifax_Mapping__c();
            eMap2.CreditScoreStartValue__c = '800';
            eMap2.CreditScoreEndValue__c = '900';
            eMap2.ApprovalType__c = 'APPROV';
            eMap2.RiskGrade__c = 'A';
            eMap2.RiskGradeDisplayValue__c = 'Standard Pricing Market: Approved, No Payment Requirements';
            eMap2.NoHit__c = false;
            eMap2.HitCode__c = '1';
            eMap2.HitCodeResponse__c = '';
            insert eMap2;
            
            Lead_Convert__c lConv = new Lead_Convert__c( name = '***************' );
            lConv.SourceNo__c = '020'; 
            lConv.Description__c = 'Default';
            insert lConv;
        }
        Return a;
	}
	
    static testMethod void EquifaxCreditCheckTestPhone() {
    	Account acc = createTestData(false);
        User u = TestHelperClass.createSalesRepUser();
        u.Phone = '305231123';
        update u;
        
        // Page load
        NSCCreditScoreCheckController creditController;
        System.runAs(u) {
            Test.setCurrentPage(Page.NSCCreditScoreCheck);
            ApexPages.currentPage().getParameters().put('aId', acc.Id);
            creditController = new NSCCreditScoreCheckController();
            creditController.agentType = 'PHONE';
            Boolean isPhone = creditController.renderPhoneView;
        }
        
        // Credit request test
        System.runAs(u) {
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('EquifaxConsumerResponseCalloutMock');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/xml');
            Test.setMock(HttpCalloutMock.class, mock);
            system.debug('mock: '+mock);
            PageReference pRefConsumer = creditController.GetConsumerScoreInformation();
            Boolean renderMainFrm = creditController.renderMainFrm;
            String EquifaxCondition = creditController.EquifaxConditionJSON;
        }  
    }
    
    static testMethod void EquifaxCreditCheckTestFieldWithMissingData() {
    	Account acc = createTestData(true);
    	
        User u = TestHelperClass.createSalesRepUser();
        u.Phone = '305231123';
        update u;
        
        // Page load
        NSCCreditScoreCheckController creditController;
        System.runAs(u) {
            Test.setCurrentPage(Page.NSCCreditScoreCheck);
            ApexPages.currentPage().getParameters().put('aId', acc.Id);
            creditController = new NSCCreditScoreCheckController();
            creditController.agentType = 'FIELD';
        }
        
        // Credit request test
        System.runAs(u) {
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('EquifaxConsumerResponseCalloutMock');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/xml');
            Test.setMock(HttpCalloutMock.class, mock);
            PageReference pRefConsumer = creditController.GetConsumerScoreInformation();
            Boolean renderMainFrm = creditController.renderMainFrm;
            String EquifaxCondition = creditController.EquifaxConditionJSON;
        }  
    }
    
    static testMethod void EquifaxCreditCheckTestTrigger() {
    	Account acc = createTestData(false);
        User u = TestHelperClass.createSalesRepUser();
        u.Phone = '305231123';
        update u;
        
        // Page load
        NSCCreditScoreCheckController creditController;
        System.runAs(u) {
            creditController = new NSCCreditScoreCheckController(acc.Id);
        }
        
        // Credit request test
        System.runAs(u) {
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('EquifaxConsumerResponseCalloutMock');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/xml');
            Test.setMock(HttpCalloutMock.class, mock);
            PageReference pRefConsumer = creditController.GetConsumerScoreInformation();
            Boolean renderMainFrm = creditController.renderMainFrm;
            String EquifaxCondition = creditController.EquifaxConditionJSON;
        }  
    }
}