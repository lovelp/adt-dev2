/************************************* MODIFICATION LOG ********************************************************************************************
* GoogleGeoCodeSchedulable
* 
* DESCRIPTION : A schedulable class that initiates the proces by which non-geocoded addresses are updated
*               with latitude and longitude geocode data to support mapping functions.  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*               				10/14/2011			- Original Version
*
*													  
*													
*/

global class GoogleGeoCodeSchedulable  implements Schedulable {
	global void execute(SchedulableContext SC) {
		
		// initiate a batch to geocode addresses
		// scope parameter is 1 in order to ensure that the batch attempts only 1 callout
        if(BatchJobHelper.canThisBatchRun(BatchJobHelper.GoogleGeoCodeBatch))
        {		
			Id batchProcessId = Database.executebatch(new GoogleGeoCodeBatch(), 1);
        }
		
		GoogleGeoCodeSchedulable m = new GoogleGeoCodeSchedulable();
		DateTime timenow = DateTime.now().addHours(Integer.valueOf(ResaleGlobalVariables__c.getinstance('GoogleGeocodeSchedulerInterval').value__c));
		String seconds = String.valueOf('0'); 
		String minutes = String.valueOf('0'); 
		String hours = String.valueOf(timenow.hour()); 
		
		String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
		String jobName = 'Google Geocode - ' + hours + ':' + minutes;
		system.schedule(jobName, sch, m);		
		system.abortJob(sc.getTriggerId());			
	}
}