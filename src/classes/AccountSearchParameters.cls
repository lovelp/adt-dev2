/************************************* MODIFICATION LOG ********************************************************************************************
* AccountSearchParameters
*
* DESCRIPTION : Serves a data structure for user specified search criteria for creating Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2011			- Original Version
*
*													
*/

public class AccountSearchParameters {
	
	// Values of user entered search criteria
	public String[] dataSource {get; set;} 
    public String[] leadType {get; set;} 
    public Boolean newMoverIndicator {get; set;} 
    public Boolean nonWorkedLeadIndicator {get;set;}        
    public String[] discoReason {get;set;} 
    public Datetime dateAssignedBefore {get;set;}
    public Datetime dateAssignedAfter {get;set;}
    public Boolean phoneAvailableIndicator {get;set;}
    public String siteStreet {get;set;}
    public String siteCity {get;set;}
    public String[] siteState {get;set;}
    public String sitePostalCode {get;set;}
    public Date discoDateBefore {get;set;}
    public Date discoDateAfter {get;set;}
    public Boolean activeSiteIndicator {get;set;}
    public Integer queryRowLimit {get;set;}
    public Set<String> accountIdSet {get;set;}
	
	//new phase 2 fields
	public Date newMoverDateBefore {get;set;}				//Account.NewMoverDate__c and Lead.NewMoverDate__c
	public Date newMoverDateAfter {get;set;}				//Account.NewMoverDate__c and Lead.NewMoverDate__c
	public DateTime salesAppointmentDateBefore {get;set;}	//Account.SalesAppointmentDate__c
	public DateTime salesAppointmentDateAfter {get;set;}	//Account.SalesAppointmentDate__c
	public Boolean phoneSale {get;set;}						//Account.ProcessingType__c == "NSC"
	public String[] dispositionCode {get;set;}				//Account.DispositionCode__c or Lead.DispositionCode__c
	public Boolean manuallyAssigned {get;set;}				//Account.ManuallyAssigned__c
	public String sitePostalCodeAddOn {get;set;}
	
	public Boolean hasAccountSpecificCriteria() {
		return (
			this.phoneSale || 
			this.salesAppointmentDateAfter != null || 
			this.salesAppointmentDateBefore != null || 
			(this.discoReason != null && discoReason.size() > 0) || 
			this.discoDateAfter != null || 
			this.discoDateBefore != null || 
			this.activeSiteIndicator || 
			this.manuallyAssigned
		);
	}
}