/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class AdtCustContactEBRServiceTest {
    static testMethod void test1() {
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));

        insert rgvList;
        List<Account> listacc = new List<Account>();
        Address__c addr = new Address__c();
        addr.Latitude__c = 29.91760000000000;
        addr.Longitude__c = -90.17870000000000;
        addr.Street__c = '56528 E Crystal Ct';
        addr.City__c = 'Westwego';
        addr.State__c = 'LA';
        addr.PostalCode__c = '70094';
        addr.OriginalAddress__c = '56528 E CRYSTAL CT, WESTWEGO, LA, 70094';
        insert addr;
        
        Account acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'abc';
        acct.FirstName__c ='test' ;
        acct.LastName__c ='testabc' ;
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.PostalCodeID__c = TestHelperClass.inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
        acct.DispositionCode__c = 'Disqualified';
        acct.ShippingPostalCode = '22102';
        acct.Data_Source__c = 'TEST';
        acct.PhoneNumber2__c = '4567788900';
        acct.PhoneNumber3__c = '345566767788';
        acct.PhoneNumber4__c = '1112223333';
        acct.Phone = '(111) 111-1111';
        listacc.add(acct);
        insert listacc;
        
        String Source='User Entered';
        List<Lead> lead = new List<Lead>();
        String postalId = TestHelperClass.inferPostalCodeID('22102', '1100');
        Lead l = new Lead();
        l.LastName = 'TestLastName';
        l.Company = 'TestCompany';
        l.LeadSource = Source != '' ? Source : null;
        l.SGCOM3__SG_infoGroupId__c = l.LeadSource == 'Salesgenie.com' ? '12345' : null;
        l.DispositionCode__c = 'Phone - No Answer';
        l.Channel__c = 'Resi Direct Sales';
        l.SiteStreet__c = '8952 Brook Rd';
        l.SiteCity__c = 'McLean';       
        l.SiteStateProvince__c = 'VA';
        l.SiteCountryCode__c = 'US';
        l.SitePostalCode__c = '22102';
        l.PostalCodeID__c = postalId;
        l.AddressID__c = null;
        l.Business_Id__c = null;
        l.NewMoverDate__c = Date.today().addDays(-30);
        l.PhoneNumber2__c = '4567788900';
        l.PhoneNumber3__c = '345566767788';
        l.PhoneNumber4__c = '1112223333';
        l.Phone = '(111) 111-1111';
        l.ExternalID__c ='test123';
        lead.add(l);
        insert lead;
        Test.startTest();
        AdtCustContactEBRService.createRequestQueues(listacc, lead);
        Test.stopTest();
    }
}