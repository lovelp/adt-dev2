/************************************* MODIFICATION LOG ************************************************************
* ADTPartnerLoanAppAPIHelper
*
* DESCRIPTION : Defines logic to handle the method calls from RV Loan Application API. 
*-------------------------------------------------------------------------------------------------------------------
* DEVELOPER             DATE          Ticket         REASON
*-------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari      05/09/2019    HRM-9547      Original Version
* Abhinav Pandey        03/11/2019             HRM-10881         - Flex Fi Changes
*/
public class ADTPartnerLoanAppAPIHelper {
    
    //validate input request parameters
    public static String responseMessage;
    public static String validateRequest(ADTPartnerLoanAppAPISchema.LoanApplicationRequestMessage req){
        try{
        list<String> errorMessage = new list<String>();
        ADTPartnerLoanAppAPISchema.Customer cust = req.loanApplicationRequest.customer;
        ADTPartnerLoanAppAPISchema.PaymentInfo payInfo = cust.paymentInfo;
        ADTPartnerLoanAppAPISchema.BillingAddress prevAddr = cust.previousAddress;
        ADTPartnerLoanAppAPISchema.BillingAddress billAddr = cust.billingAddress;
        if(String.isBlank(cust.taxIdNumber)){
            errorMessage.add('SSN');
        }
        if(String.isBlank(cust.dateOfBirth)){
            errorMessage.add('Data of birth');
        }
        if(cust.annualIncome == null){
            errorMessage.add('Annual Income');
        }
        if(String.isBlank(cust.creditCardCVV)){
            errorMessage.add('Card CVV');
        }
        
        if(payInfo != null){
            if(String.isBlank(payInfo.refNumber)){
                errorMessage.add('Payment Profile Id');
            }
            if(String.isBlank(payInfo.maskedCardNumber)){
                errorMessage.add('Credit Card Number');
            }
            if(String.isBlank(payInfo.brand)){
                errorMessage.add('Credit Card Type');
            }
            if(String.isBlank(payInfo.cardExpiry)){
                errorMessage.add('Card expiry');
            }
            if(String.isBlank(payInfo.transactionNumber)){
                errorMessage.add('Transaction Number');
            }
        }
        
        if (billAddr !=null){
            if(String.isBlank(billAddr.addrLine1)){
                errorMessage.add('Billing Address Address Line 1');
            }
            if(String.isBlank(billAddr.city)){
                errorMessage.add('Billing Address City');
            }
            if(String.isBlank(billAddr.state)){
                errorMessage.add('Billing Address State');
            }else if(billAddr.state.length() != 2){
                errorMessage.add('Billing Address State Code should be of length 2');
            }
            if(String.isBlank(billAddr.postalCode)){
                errorMessage.add('Billing Address Postal Code');
            }               
        }
        if (prevAddr !=null){
            if(String.isBlank(prevAddr.addrLine1)){
                errorMessage.add('Previous Address Address Line 1');
            }
            if(String.isBlank(prevAddr.city)){
                errorMessage.add('Previous Address City');
            }
            if(String.isBlank(prevAddr.state)){
                errorMessage.add('Previous Address State');
            }else if(prevAddr.state.length() != 2){
                errorMessage.add('Previous Address State Code should be of length 2');
            }
            if(String.isBlank(prevAddr.postalCode)){
                errorMessage.add('Previous Address Postal Code');
            }               
        }
        
        String message = '';
        if(!errorMessage.isEmpty()){
            message = 'Missing values in request: ' + String.join(errorMessage, ', ');
        }
        return message;
        }catch(Exception ae){
            return ae.getmessage();
        }
    }
    public final static String DATE_FORMAT = 'yyyy-MM-dd HH:mm:ss';
    
    /**************************************************************************************************************************************************
     * 
     * 
     * ************************************************************************************************************************************************/
    public static ADTPartnerLoanAppAPISchema.LoanApplication getLoanApplication(ADTPartnerLoanAppAPISchema.LoanApplicationRequestMessage req, String accId){
        //RV Loan API
        if(req != null){
            //Validate input request parameters
            String validationMessage = validateRequest(req);
            if(String.isNotBlank(validationMessage)){
                throw new IntegrationException(validationMessage);
            }
        }
        Account acc = Utilities.queryAccount(accId);
        LoanApplicationDataAccess dataLayerObject = new LoanApplicationDataAccess();
        list<LoanApplication__c> loanApplicationList = getLoanApplication(acc.Id);
        LoanApplication__c loanApp;
        //Create a new loan application if no active one is found.
        if(loanApplicationList.isEmpty() && req != null){
            String message = dataLayerObject.getLoanApplicationAllowedMessage(acc,true,'').stripHtmlTags().replaceAll('\n', '');//HRM-10881 Added true boolean
            System.debug('Message is'+message);
            if(message == 'false' || String.isBlank(message) || message == 'null' ){
                loanApp = createLoanApplication(req, acc);
                System.debug('Inside loan app'+loanApp);
                if(loanApp == null || loanApp.Id == null){
                    throw new IntegrationException('Uknown Error');            
                }
                if(loanApp.ApplicationStatus__c.contains('Failed')){
                   String deactivate = dataLayerObject.deactivateApplication(loanApp.Id);
                   throw new IntegrationException('Callout to Zoot failed:'+loanApp.LoanDecisionResponse__c);
                }
            } else{
                throw new IntegrationException('Data Error: '+message);
            }
        //Site Look API
        }else if(loanApplicationList.isEmpty() && req == null){
            return null;
        }else{
            loanApp = loanApplicationList.get(0);
            //Create a new application if it is not submitted. OR loan applicaiton is submitted but declined and retry allowed true create a newapp.
            if((loanApp.SubmittedDateTime__c == null) || (loanApp.DecisionStatus__c == 'DECLINED' && loanApp.RetryAllowed__c)){
                //For site lookup API we don't give back if loan app is not submitted.
                LoanApplication__c loanAppNew = new LoanApplication__c();
                if(req != null){
                    loanAppNew = createLoanApplication(req, acc);
                }
                else{
                    return null;
                }
                system.debug('Inside loan app'+loanAppNew);
                if(loanAppNew == null || loanAppNew.Id == null){
                    throw new IntegrationException('Uknown Error');        
                }
                if(loanApp.ZootRequestStatus__c.contains('Failed')){
                    throw new IntegrationException('Callout to Zoot failed:'+loanApp.LoanDecisionResponse__c);
                }
                String deactivate = dataLayerObject.deactivateApplication(loanApp.Id);
                loanApp = loanAppNew;
            }
        }
        return getLoanApplicationForResponse(loanApp);

    }
    
    public static ADTPartnerLoanAppAPISchema.LoanApplication getLoanApplicationForResponse(LoanApplication__c loanApp){
        ADTPartnerLoanAppAPISchema.LoanApplication loanApplication = new ADTPartnerLoanAppAPISchema.LoanApplication();
        loanApplication.id = loanApp.LoanApplicationId__c;
        loanApplication.decisionStatus = loanApp.DecisionStatus__c;
        loanApplication.loanStatus = loanApp.DecisionStatus__c;
        loanApplication.decisionReasonCd = loanApp.DecisionReasonCd__c;
        loanApplication.decisionReasonDesc = loanApp.DecisionReasonDesc__c;
        loanApplication.retryAllowed = loanApp.RetryAllowed__c;
        loanApplication.ofac = loanApp.OFAC__c;
        responseMessage = loanApp.CustomerLoanStatus__c;

        if(loanApp.SubmittedDateTime__c != null){
            loanApplication.submittedDate = loanApp.SubmittedDateTime__c.formatGmt('yyyy-MM-dd');
            Integer expirationDays = Integer.valueOf(FlexFiConfig__c.getinstance('loanApplicationExpirationDays').Value__c);
            loanApplication.expiryDate = loanApp.SubmittedDateTime__c.addDays(expirationDays).formatGmt('yyyy-MM-dd');
        }
        return loanApplication;
    }
    
    public static list<LoanApplication__c> getLoanApplication(Id accId){
        list<LoanApplication__c> loanApplicationList = [SELECT Id, ssn__c,DecisionStatus__c, LoanApplicationId__c,SubmitLoanTerms_ConditionAcceptedDate__c,  AnnualIncome__c, PaymentechProfileId__c, Account__r.Phone,Account__r.EquifaxRiskGrade__c, ApplicationStatus__c,
                                                        DOB__c,FirstName__c,LastName__c ,TilaEmail__c,CFGEmail__c,Account__r.Email__c,BillingAddress__c,ShippingAddress__c,BillingAddress__r.Street__c, BillingAddress__r.Street2__c, BillingAddress__r.StreetNumber__c,
                                                        BillingAddress__r.StreetName__c, BillingAddress__r.CountryCode__c,BillingAddress__r.City__c, BillingAddress__r.State__c, BillingAddress__r.PostalCode__c,OFAC__c,ZootRequestStatus__c,LoanDecisionResponse__c, CustomerLoanStatus__c,
                                                        ShippingAddress__r.Street__c, ShippingAddress__r.Street2__c, ShippingAddress__r.StreetNumber__c,ShippingAddress__r.StreetName__c, ShippingAddress__r.CountryCode__c,ShippingAddress__r.City__c, ShippingAddress__r.State__c, ShippingAddress__r.PostalCode__c,
                                                        RequestRetryCounter__c,DecisionReasonDesc__c,RetryAllowed__c,SubmittedDateTime__c,LoanStatus__c,DecisionReasonCd__c,SourceChannel__c,PreviousAddressLine1__c,previousAddressLine2__c,PreviousAddressPostalCd__c,PreviousAddressStateCd__c,PreviousAddressCity__c,PreviousAddressCountryCd__c,eConsentAcceptedDateTime__c,Account__r.EquifaxApprovalType__c,Account__r.PhoneNumber2__c
                                                        FROM LoanApplication__c 
                                                        WHERE Account__c = :accId AND ActiveLoanApplication__c = true
                                                        order by LastModifiedDate desc nulls last
                                                        limit 1];
        return loanApplicationList;
    }
    
    public static LoanApplication__c createLoanApplication(ADTPartnerLoanAppAPISchema.LoanApplicationRequestMessage req, Account acc){
        try{
        System.debug('Inside create loan application');
        LoanApplication__c loanAppObj = new LoanApplication__c();
        loanAppObj.Account__c = acc.Id;
        loanAppObj.SSN__c =  Utilities.decryptDataForDP(req.loanApplicationRequest.customer.taxIdNumber);
        loanAppObj.DOB__c =  Utilities.decryptDataForDP(req.loanApplicationRequest.customer.dateOfBirth);
        loanAppObj.AnnualIncome__c = req.loanApplicationRequest.customer.annualIncome;
        loanAppObj.CardType__c = req.loanApplicationRequest.customer.paymentInfo.brand;
        loanAppObj.CreditCardName__c = req.loanApplicationRequest.customer.paymentInfo.cardholdersName;
        loanAppObj.CreditCardNumber__c = req.loanApplicationRequest.customer.paymentInfo.maskedCardNumber;
        loanAppObj.CreditCardExpirationDate__c = req.loanApplicationRequest.customer.paymentInfo.cardExpiry;
        loanAppObj.PaymentechProfileId__c = req.loanApplicationRequest.customer.paymentInfo.refNumber;
        loanAppObj.FirstName__c = acc.FirstName__c; 
        loanAppObj.LastName__c = acc.LastName__c;
        loanAppObj.TilaEmail__c = acc.Email__c;
        loanAppObj.CFGEmail__c = acc.Email__c;
        loanAppObj.Account__r = acc;
        Address__c prevAdd = new Address__c();
        Address__c billingAddr = new Address__c();
        LoanApplicationDataAccess.ShippingAddress billingAddressObject;
        //LoanApplicationDataAccess.ShippingAddress previousAddressObject;
        ADTPartnerLoanAppAPISchema.BillingAddress previousAddressObject = req.loanApplicationRequest.customer.previousAddress;
        if(previousAddressObject != null){
            prevAdd = checkForMelissaData(req.loanApplicationRequest.customer.previousAddress);
            if(prevAdd != null){
                loanAppObj.PreviousAddressCity__c = String.isNotBlank(prevAdd.City__c) ? prevAdd.City__c : '';
                loanAppObj.PreviousAddressCountryCd__c = String.isNotBlank(prevAdd.CountryCode__c) ? prevAdd.CountryCode__c : '';
                loanAppObj.PreviousAddressLine1__c = String.isNotBlank(prevAdd.Street__c) ? prevAdd.Street__c : '';
                loanAppObj.PreviousAddressLine2__c = String.isNotBlank(prevAdd.Street2__c) ? prevAdd.Street2__c : '';
                loanAppObj.PreviousAddressStateCd__c = String.isNotBlank(prevAdd.state__c) ? prevAdd.state__c : '';
                loanAppObj.PreviousAddressPostalCd__c = String.isNotBlank(prevAdd.postalCode__c) ? prevAdd.postalCode__c : '';
            }
        }
        List<Address__c> addrResponseList = [SELECT Id, Street__c, Street2__c, City__c, State__c, PostalCode__c, CountryCode__c FROM Address__c WHERE Id =: acc.AddressId__c];
        
        if(req.loanApplicationRequest.customer.billingAddress != null){
            //String billingAddress = convertAddressFormatForLoan(req.loanApplicationRequest.customer.billingAddress);
            billingAddr = checkForMelissaData(req.loanApplicationRequest.customer.billingAddress);
            loanAppObj.BillingAddress__r = billingAddr;
            if(billingAddr!= null && billingAddr.Id != null){
                loanAppObj.BillingAddress__c = billingAddr.Id;
            }
        }else{
            loanAppObj.BillingAddress__r = addrResponseList.get(0);
            loanAppObj.BillingAddress__c = acc.AddressId__c;
        }
        loanAppObj.ShippingAddress__r = addrResponseList.get(0);
        loanAppObj.ShippingAddress__c = acc.AddressId__c;
        system.debug('$$$' + billingAddr);
        
        loanAppObj.ApplicationStatus__c = 'Application Initiated';
        loanAppObj.DeactivateApplication__c = false;
        system.debug('$$$' + loanAppObj);
        CallLoanAPI.skipRetry = true;
        String zootResponse = CallLoanAPI.getLoanDecisionRequest(null, Utilities.decryptDataForDP(req.loanApplicationRequest.customer.creditCardCVV), 
                                                                 Utilities.decryptDataForDP(req.loanApplicationRequest.customer.taxIdNumber), new list<LoanApplication__c>{loanAppObj}, 
                                                                 billingAddr);
        System.debug('### zoot response is' + zootResponse + ' loan id is ' + loanAppObj.Id);
        try{
            if(prevAdd != null && prevAdd.Id == null && String.isNotBlank(prevAdd.Street__c)){
                system.debug('$##'+prevAdd);
                insert prevAdd;
            }
        }catch(Exception e){
            System.debug('cannot save previous address' + e);
        }
        list<LoanApplication__c> loanApplicationList = getLoanApplication(acc.Id);
        if(!loanApplicationList.isEmpty()){
            loanAppObj = loanApplicationList.get(0);
            }
            return loanAppObj;
        }
        catch(exception e){
             system.debug('Exception '+e);
             ADTApplicationMonitor.log(e, 'ADTPartnerLoanAppAPIHelper',req.loanApplicationRequest.opportunityId, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
             return null; 
        }
        
    }
    // melissa data check
    public static Address__c checkForMelissaData(ADTPartnerLoanAppAPISchema.BillingAddress billingAddressObject){
        try{
            //System.debug('###'+billingAddress);
            MelissaDataAPI.MelissaDataResults melissaResponse;
            Address__c address = new Address__c();
            //check if address has lat, lon, postal code.
            String uniqueOriginalAddress = billingAddressObject.addrLine1+'+';
            uniqueOriginalAddress += String.isnotBlank(billingAddressObject.addrLine2) ? billingAddressObject.addrLine2+'+'+billingAddressObject.city+'+'+billingAddressObject.state+'+'+billingAddressObject.postalCode :billingAddressObject.city+'+'+billingAddressObject.state+'+'+billingAddressObject.postalCode;
            List<Address__c> addrResponseList = [SELECT Id, Street__c, Street2__c, City__c, State__c, PostalCode__c, CountryCode__c FROM Address__c WHERE OriginalAddress__c =: uniqueOriginalAddress];
            // melissa check to normalize if address does not have lat, lon, postal code
            if(addrResponseList.size() == 0){
                //check if reqAddr is null
                melissaResponse = MelissaDataAPI.validateAddr(billingAddressObject.addrLine1, billingAddressObject.addrLine2 !=null ? billingAddressObject.addrLine2: '', billingAddressObject.city, billingAddressObject.state,billingAddressObject.postalCode);
                if(melissaResponse != null){
                    System.debug('### mdr is'+melissaResponse);
                    uniqueOriginalAddress ='';
                    uniqueOriginalAddress = melissaResponse.Addr1+'+';
                    String address2 = melissaResponse.Addr2;
                    if (String.isNotBlank(melissaResponse.Suite)){
                        address2 = melissaResponse.Suite;
                    }
                    uniqueOriginalAddress += String.isNotBlank(address2) ? address2 + '+' +melissaResponse.City+ '+' +melissaResponse.State+'+'+melissaResponse.Zip : melissaResponse.City+ '+'+ melissaResponse.State+ '+'+ melissaResponse.Zip;
                    //Query the address
                    addrResponseList = [SELECT Id, Street__c, Street2__c, City__c, State__c, PostalCode__c, CountryCode__c FROM Address__c WHERE OriginalAddress__c =: uniqueOriginalAddress];
                    if(addrResponseList.size() == 0){
                        //create Billing Address
                        address = createAddress(melissaResponse, billingAddressObject);
                        System.debug('address created' + address);
                    } else{
                        address =  addrResponseList[0];
                        System.debug('address found ' + address);
                    }
                } else{
                    //return 'Error: Cannot do Melissa Data Call';
                    return null;
                }
            } else {
                address =  addrResponseList[0];
                System.debug('address found without melissa' + address);
            }
            return address;
        }catch(exception e){
            System.debug('### melissa error'+e);
            return null;
        }
    }
    
    public static Address__c createAddress(MelissaDataAPI.MelissaDataResults mdr,ADTPartnerLoanAppAPISchema.BillingAddress billingAddressObject){
        try{
            system.debug('Create Address'+billingAddressObject);
            Boolean error = false;
            //Boolean didOverride = overRideAdd == 'true'?true:false;
            Address__c addr = new Address__c();
            // Set Addr
            addr.Street__c = mdr != null ? mdr.Addr1 : billingAddressObject.addrLine1;
            String address2 = mdr!=null? mdr.Addr2:billingAddressObject.addrLine2 ;
            if (mdr!=null && String.isNotBlank(mdr.Suite)){
                address2 = mdr.Suite;
            }
            addr.Street2__c = String.isNotBlank(address2) ? address2 : String.isNotBlank(billingAddressObject.addrLine2)? billingAddressObject.addrLine2 :'';
            addr.StreetNumber__c = mdr != null ? mdr.StreetNumber: '';
            addr.StreetName__c = mdr != null ? mdr.StreetName : '';
            addr.City__c = mdr != null ? mdr.City :String.isNotBlank(billingAddressObject.city) ? billingAddressObject.city : '';
            addr.County__c = mdr != null ? mdr.County :'';
            addr.State__c = mdr != null ?  mdr.State :String.isNotBlank(billingAddressObject.state) ? billingAddressObject.state : '';
            addr.PostalCode__c = (mdr != null && String.isNotBlank(mdr.Zip))? mdr.Zip : String.isNotBlank(billingAddressObject.postalCode) ? billingAddressObject.postalCode : '';
            addr.PostalCodeAddOn__c = mdr != null ? mdr.Plus4 :'';
            addr.CountryCode__c = (mdr != null && String.isNotBlank(mdr.CountryCode)) ? mdr.CountryCode : '';
            addr.Latitude__c = mdr != null ? decimal.valueOf(mdr.Lat) : 0.00;
            addr.Longitude__c = mdr != null ? decimal.valueOf(mdr.Lon) : 0.00;
            addr.OriginalAddress__c = mdr != null ? mdr.Addr1+'+':billingAddressObject.addrLine1+'+';
            if(mdr != null){
                addr.Validated__c = true;
                String errorFlag = '';
                String melissaAddress2 = mdr.Addr2;
                if (String.isNotBlank(mdr.Suite)){
                    melissaAddress2 = mdr.Suite;
                }
                addr.OriginalAddress__c += String.isnotBlank(melissaAddress2) ? melissaAddress2 + '+' +mdr.City+ '+' +mdr.State+'+'+mdr.Zip : mdr.City+ '+'+ mdr.State+ '+'+ mdr.Zip;
                /*for (String code : mdr.ResultCodes){
                    if (code.containsIgnoreCase('AE')){
                        errorFlag = 'Error:'+code;
                        break;
                    }
                    else if (code.startsWith('AC')){
                        //do nothing
                    }
                    else if (code.startsWith('AS')) {
                        //do nothing
                    }
                }
                if(String.isNotBlank(errorFlag)){
                    //return errorFlag;
                    return null;
                }*/
                
            }
            else {
                addr.OriginalAddress__c += String.isnotBlank(billingAddressObject.addrLine2) ? billingAddressObject.addrLine2 + '+' +billingAddressObject.city+ '+' +billingAddressObject.state+'+'+billingAddressObject.postalCode : billingAddressObject.city+ '+'+ billingAddressObject.state+ '+'+ billingAddressObject.postalCode;
            }
            addr.StandardizedAddress__c = mdr != null ? mdr.Addr1+',' :billingAddressObject.addrLine1 + ',';
            if(mdr != null){
                String meliAddress2 = mdr.Addr2;
                if (String.isNotBlank(mdr.Suite)){
                    meliAddress2 = mdr.Suite;
                }
                addr.StandardizedAddress__c += String.isnotBlank(meliAddress2) ?meliAddress2 + ',' +mdr.City+ ',' +mdr.State+','+mdr.Zip : mdr.City+ ','+ mdr.State+ ','+ mdr.Zip;
            }
            else {
                addr.StandardizedAddress__c += String.isnotBlank(billingAddressObject.addrLine2) ? billingAddressObject.addrLine2 + ',' +billingAddressObject.city+ ',' +billingAddressObject.state+','+billingAddressObject.postalCode: billingAddressObject.city+ ','+ billingAddressObject.state+ ','+ billingAddressObject.postalCode;
            }
            addr.StandardizedAddress__c = addr.StandardizedAddress__c.toUpperCase();
            addr.OriginalAddress__c = addr.OriginalAddress__c.toUpperCase();
            //insert addr;
            system.debug('Address Id' + addr);
            return addr;
        }
        catch(Exception ae){
            system.debug('Exception '+ae);
            return null;
        }
    }
    

}