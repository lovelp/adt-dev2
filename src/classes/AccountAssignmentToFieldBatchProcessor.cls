/************************************* MODIFICATION LOG ********************************************************************************************
* AccountAssignmentBatchProcessorNew
*
* DESCRIPTION : Reassigns accounts to previous owner if certain conditions are met
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    TICKET                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan               03/12/2012                                      - Original Version
* Siddarth Asokan               05/15/2018              HRM-4831                - Moved Affiliates and Call Disposition to custom object
*/

global class AccountAssignmentToFieldBatchProcessor implements Database.Batchable<sObject>, Database.Stateful {
    public String strquery;
    Set<Id> affiliatesExcluded = new Set<Id>();
    Set<Id> callDisposExcluded = new Set<Id>();
    Set<String> dataSourcesToExclude = new Set<String>();
    
    public AccountAssignmentToFieldBatchProcessor(){        
        strquery ='Select Id, Name, PostalcodeId__c, SitePostalCode__c, Owner.Profile.Name, ADTEmployee__c, Contact_Owner__c, Business_Id__c, Channel__c, ProcessingType__c, UnassignedLead__c, DateAssigned__c, NewLead__c, Affiliation__c, OwnerId, Owner.Name, QueriedSource__c, TelemarLeadSource__c, GenericMedia__c, RecordTypeId, RecordType.Name, Ownership_Transfer_Details__c, Lead_Origin__c';
        
        for(LeadSharingCriteria__c LSC :[Select Affiliate__c, CallDisposition__c from LeadSharingCriteria__c where Active__c = true]){
            if(LSC.Affiliate__c != null)
                affiliatesExcluded.add(LSC.Affiliate__c);
            if(LSC.CallDisposition__c != null)
                callDisposExcluded.add(LSC.CallDisposition__c);
        }
        if(affiliatesExcluded.size() > 0){
            strquery = strquery +', ( Select Id, Active__c, Affiliate__c, Affiliate__r.Name from Account_Affiliations__r Where Affiliate__c != null )';
        }
        if(callDisposExcluded.size() > 0){
            strquery = strquery +', ( Select Id, Account__c, Call_Disposition__r.Name, Call_Disposition__c, Call_Disposition__r.Action__c, Call_Disposition__r.Tier_One__c, Call_Disposition__r.Tier_One__r.Name From CallData__r where Call_Disposition__c != null AND CreatedDate = LAST_N_MONTHS:' + String.escapeSingleQuotes(BatchGlobalVariables__c.getinstance('LS_NoOfMonthsOfRecordCreation').value__c)+')';
        }    
        strquery = strquery + ' From Account Where SalesAppointmentDate__c = Null AND ExcludeLeadSharing__c = false AND CreatedDate = LAST_N_MONTHS:' + String.escapeSingleQuotes(BatchGlobalVariables__c.getinstance('LS_NoOfMonthsOfRecordCreation').value__c); 
         
        if(BatchGlobalVariables__c.getinstance('LS_ADTEmployee') != Null && (BatchGlobalVariables__c.getinstance('LS_ADTEmployee').value__c == '1')){
           strquery = strquery + ' AND ADTEmployee__c != true '; 
        }
        
        if(BatchGlobalVariables__c.getinstance('LS_DataSourceExclusion') != Null && BatchGlobalVariables__c.getinstance('LS_DataSourceExclusion').value__c != Null){
           for(String dataSource: BatchGlobalVariables__c.getinstance('LS_DataSourceExclusion').value__c.split(',')){
               dataSourcesToExclude.add(dataSource.trim());
           }
           if(dataSourcesToExclude.size() > 0 )
                strquery = strquery + ' AND Data_Source__c NOT IN :dataSourcesToExclude';        
        }
        
        strquery = strquery + ' AND (NOT Business_Id__c LIKE \'%1300%\') AND AccountStatus__c != \'' + IntegrationConstants.STATUS_SLD + '\' AND AccountStatus__c != \'' + IntegrationConstants.STATUS_SLS +'\' AND AccountStatus__c != \'' + IntegrationConstants.STATUS_CLS +'\''+
                              ' AND LastActivityDate__c < LAST_N_DAYS:' + String.escapeSingleQuotes(BatchGlobalVariables__c.getinstance('LS_DaysBeforeReassignmentToField').value__c) +
                              ' AND (Owner.Profile_Type__c = \'NSC Agent\' OR Owner.Profile_Type__c = \'NSC Manager\')';   
    }
    
    global Database.QueryLocator start( Database.Batchablecontext bc ) {
        system.debug('Query = ' + strquery);
        return Database.getQueryLocator(strquery);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {        
        List<Account> acctBatchLst = (List<Account>)scope;
        List<Account> acctsToUpdateLst = new List<Account>();
        System.debug('Account Batch size is: ' +acctBatchLst.size());
        if (acctBatchLst.size() > 0){
            for (Account a:acctBatchLst){  
                Boolean skipAccount = false;
                
                // check this account does not have a disposition
                if(a.CallData__r.size() > 0){
                    for(Call_Data__c aCd:a.CallData__r){
                        if(callDisposExcluded.size() > 0 && ((String.isNotBlank(aCd.Call_Disposition__c) && callDisposExcluded.contains(aCd.Call_Disposition__c)) || (String.isNotBlank(aCd.Call_Disposition__r.Tier_One__c) && callDisposExcluded.contains(aCd.Call_Disposition__r.Tier_One__c)))){
                            skipAccount = true;
                            break;
                        }
                    }
                }
                
                // check this account does not have a membership to excluded affiliates
                if(!skipAccount && a.Account_Affiliations__r.size() > 0){
                    for(Account_Affiliation__c aAff:a.Account_Affiliations__r){
                        if(affiliatesExcluded.size() > 0 && affiliatesExcluded.contains(aAff.Affiliate__c)){
                            skipAccount = true;
                            break;
                        }
                    }
                }
                
                System.debug('Account Id: '+ a.Id + ' Owner before assignment: '+ a.OwnerId+' Skip Account: '+skipAccount);
                
                if(!skipAccount){
                    // Processing Type set to NSC to reuse existing functionality of ChangeAccountOwnerController.changeOwnershipWithListOfAccounts
                    a.ProcessingType__c = IntegrationConstants.PROCESSING_TYPE_NSCLeadSharing;
                    a.Lead_Origin__c = 'Matrix Rehash';
                    a.Ownership_Transfer_Details__c = a.Owner.Name;
                    acctsToUpdateLst.add(a);
                }
            }
        }
        
        if (acctsToUpdateLst.size() > 0){
            ChangeAccountOwnerController.changeOwnershipWithListOfAccounts(acctsToUpdateLst);
            
            for (Account acc : acctsToUpdateLst){
                System.debug('Account Id: '+ acc.Id + ' Owner after assignment: '+ acc.OwnerId);
                if(acc.Ownership_Transfer_Details__c != acc.Owner.Name){
                    acc.Ownership_Transfer_Details__c = 'Account transferred from '+ acc.Ownership_Transfer_Details__c + ' to '+ acc.Owner.Name + ' on ' + system.now();
                }
                acc.DateAssigned__c = acc.LastModifiedDate;
            }
            
            // Using Database.SaveResult to not blow up the entire batch if a record fails in an update operation
            Database.SaveResult[] srList = Database.update(acctsToUpdateLst, false);
        
            Map<Id,Messaging.SingleEmailMessage> emailsToSendToManager = new Map<Id,Messaging.SingleEmailMessage>();
            
            for (Account a : acctsToUpdateLst){
                if (a.Contact_Owner__c){
                    System.debug('Account Id: '+ a.Id + ' Manager after assignment: '+ a.OwnerId);

                    //Send an email to the Manager if it cannot be assigned to Sales Rep
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(a.OwnerId); 
                    System.debug('Acc name'+a.Name);
                    mail.setSubject('Account '+ a.Name + ' cannot be assigned to Sales Rep');
                    mail.setPlainTextBody('Account '+ a.Name + ' under the postal code ' + a.SitePostalCode__c + ' cannot be assigned to Sales Rep. Please reassign the account as needed.');
                    mail.setSaveAsActivity(false);
                    emailsToSendToManager.put(a.Id, mail);
                    System.debug('Email manager'+emailsToSendToManager);
                    // Using the contact owner field as a reference only, it is set & cleared before update
                    a.Contact_Owner__c = false;
                }
            }
            
            // Create dispositions for lead sharing
            List<Disposition__c> disposToInsert = new List<Disposition__c>();
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Disposition record for hot leads
                    disposition__c newDispo = new disposition__c();
                    newDispo.DispositionType__c = 'System Generated';
                    newDispo.DispositionDetail__c = 'NSC Lead Shared';
                    newDispo.DispositionDate__c = DateTime.now();
                    newDispo.AccountID__c = sr.getId();
                    // Dispositioned by NSC Lead Sharing Admin
                    if(BatchGlobalVariables__c.getinstance('LS_DispositionOwner') != Null && BatchGlobalVariables__c.getinstance('LS_DispositionOwner').value__c != Null)
                        newDispo.DispositionedBy__c = BatchGlobalVariables__c.getinstance('LS_DispositionOwner').value__c;
                    disposToInsert.add(newDispo);
                }else{
                    // If update was not successful, remove the email from the map by passing the account Id
                    emailsToSendToManager.remove(sr.getId());
                }
            }
            if(disposToInsert.size() > 0)
                insert disposToInsert;
            
            try {
                // Placed in a try/catch block to avoid issues
                Messaging.sendEmail(emailsToSendToManager.values());
            }
            catch (Exception  e){
                System.debug('Error thrown while sending email to the Manager in AccountAssignmentToFieldBatchProcessor.cls: '+e);
            }
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}