@isTest
public class ADTPartnerCreditCheckAPITest{
    private static Account accnt;
    private static Opportunity opp;
    private static void createTestData(){
        //Do not run trigger while creating test data
        ResaleGlobalVariables__c rgv=new ResaleGlobalVariables__c();
        rgv.name='DataRecastDisableAccountTrigger';
        rgv.value__c='TRUE';
        insert rgv;
        //Custom setting record for error message.
        list<ErrorMessages__c> emlist = new list<ErrorMessages__c>();
        emlist.add(new ErrorMessages__c(name='EXCEPTION', Message__c='An exception occurred. Please contact ADT support Team'));
        emlist.add(new ErrorMessages__c(name='OPPORTUNITY_NOT_FOUND', Message__c='Opportunity not found'));
        insert emlist;
       
        list<PartnerAPIMessaging__c> partapilist = new list<PartnerAPIMessaging__c>();
        partapilist.add(new PartnerAPIMessaging__c(name='400',API_Name__c = 'invalid request'));
        partapilist.add(new PartnerAPIMessaging__c(name='404',API_Name__c= 'Opty not found', Error_Message__c = 'requestID'));
        partapilist.add(new PartnerAPIMessaging__c(name='500',API_Name__c= 'Something went wrong while processing the request. Please try again later.'));
        partapilist.add(new PartnerAPIMessaging__c(name='OptyLeadConvert',API_Name__c= 'Opportunity Id not found. Lead associated to call ID is converted to Account ID'));
        insert partapilist;
        
        list<PartnerSettings__c> partset = new list<PartnerSettings__c>();
        partset.add(new PartnerSettings__c(ValidUSStateCodes__c='AL;AK;AZ;AR;CA;CO;CT;DE;DC;FL;GA;HI;ID;IL;IN;IA;KS;KY;LA;ME;MD;MA;MI;MN;MS;MO;MT;NE;NV;NH;NJ;NM;NY;NC;ND;OH;OK;OR;PA;PR;RI;SC;SD;TN;TX;UT;VT;VA;VI;WA;WV;WI;WY'));
        insert partset;
        //partapilist.add(new PartnerAPIMessaging__c(name='500',API_Name__c= 'Something went wrong while processing the request. Please try again later.'));
        List<Postal_Codes__c> pcs = new List<Postal_Codes__c>();
        Postal_Codes__c pc = new Postal_Codes__c (name='221o2', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100', APM_PostalCode__c = true);
        pcs.add(pc);
        insert pcs;
        //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        accnt = TestHelperClass.createAccountData(addr, true);
        accnt.FirstName__c = 'TestFName';
        accnt.LastName__c = 'TestLName';
        accnt.Name = 'TestFName TestLName';
        accnt.OwnerId = current.Id;
        accnt.Channel__c = 'Custom Home Sales';
        accnt.TelemarAccountNumber__c = 'AA00112279';
        //accnt.Equifax_Last_Check_DateTime__c = system.now().addDays(-5);
        accnt.EquifaxRiskGrade__c = 'A';
        accnt.MMBOrderType__c = 'R1';
        accnt.PostalCodeId__c = pc.Id;
        update accnt;

        opp = New Opportunity();
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        opp.AccountId = accnt.Id;
        insert opp;

        IntegrationSettings__c is=new IntegrationSettings__c();
        is.Equifax_EndPoint__c = 'https://dev2.api.adt.com/adtfico';
        is.Equifax_Username__c = 'ibmcpqoms';
        is.Equifax_Password__c = 'abcd1234';
        insert is;
   
        ResaleGlobalVariables__c rgv1=new ResaleGlobalVariables__c();
        rgv1.name='IntegrationUser';
        rgv1.value__c='Integration User';
        insert rgv1;

        ResaleGlobalVariables__c rgv2=new ResaleGlobalVariables__c();
        rgv2.name='RIFAccountOwnerAlias';
        rgv2.value__c='ADT RIF';
        insert rgv2;

        ResaleGlobalVariables__c rgv3=new ResaleGlobalVariables__c();
        rgv3.name='DataConversion';
        rgv3.value__c='FALSE';
        insert rgv3;
        
        
        ResaleGlobalVariables__c rgv4 = new ResaleGlobalVariables__c();
        rgv4.name = 'DataRecastDisableAccountTrigger';
        rgv4.value__c = 'FALSE';
        insert rgv4;
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '4';
        insert resale;
        
        ResaleGlobalVariables__c rgv6 = new ResaleGlobalVariables__c();
        rgv6.name = 'Alternate Pricing Model Order Types';
        rgv6.value__c = 'N1;N2;N3;N4;R1;R2;R3;R4;A1;A2;A3;A4';
        insert rgv6;
        
        Equifax__c eq=new Equifax__c();
        eq.name='T2_messageId';
        eq.value__c='1014';
        insert eq;
        
        list<Equifax__c> equilist = new list<Equifax__c>();
        equilist.add(new Equifax__c(name='Failure Condition Code',value__c='APPROV'));
        equilist.add(new Equifax__c(name='Failure Risk Grade',value__c='X'));
        equilist.add(new Equifax__c(name='Failure Risk Grade Display Value',value__c='APPROV'));
        insert equilist;
        
        // settings
        Map<String, String> EquifaxSettings = new Map<String, String>
                    { 'Password' => ''
                    , 'CreditCheckAttempts'=>'3'
                    , 'Order Types For Credit Check'=>'N1;R1'
                    , 'Failure Risk Grade'=>'X'
                    , 'Failure Risk Grade Display Value'=>'APPROV'
                    , 'Failure Condition Code'=>'APPROV'
                    , 'Days to allow additional check'=>'90'
                    , 'Default Risk Grade'=>'W'
                    , 'Default Approved Risk Grade'=>'A'
                    , 'Default Risk Grade Display Value'=>'(Credit not run) Approved: Annual, Easypay, Full Deposit'
                    , 'Default Condition Code'=>'CAE1'
                    , 'URL'=>''
                    , 'MessageId'=>'1014'
                    , 'Login ID'=>''};
                    
        List<Equifax__c> EquifaxSettingList = new List<Equifax__c>(); 
        for( String sN: EquifaxSettings.keySet()){
            Equifax__c eS = new Equifax__c( name = sN );
            eS.value__c = EquifaxSettings.get(sN);
            EquifaxSettingList.add( eS );
        }
        insert EquifaxSettingList;
        Equifax_Conditions__c EquifaxConditionSetting1 = new Equifax_Conditions__c( Name='CAE1');
        EquifaxConditionSetting1.Agent_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting1.Agent_Quoting_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting1.Customer_Message__c = 'We were able to pull your credit and we are able offer you our best pricing';
        EquifaxConditionSetting1.Deposit_Required_Percentage__c = 0.0;
        EquifaxConditionSetting1.EasyPay_Required__c = true;
        EquifaxConditionSetting1.Payment_Frequency__c = 'A';
        EquifaxConditionSetting1.Three_Pay_Allowed__c = true;
        insert EquifaxConditionSetting1;
        
        Equifax_Conditions__c EquifaxConditionSetting2 = new Equifax_Conditions__c( Name='APPROV');
        EquifaxConditionSetting2.Agent_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting2.Agent_Quoting_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting2.Customer_Message__c = 'We were able to pull your credit and we are able offer you our best pricing';
        EquifaxConditionSetting2.Deposit_Required_Percentage__c = 0.0;
        EquifaxConditionSetting2.EasyPay_Required__c = false;
        EquifaxConditionSetting2.Payment_Frequency__c = 'A';
        EquifaxConditionSetting2.Three_Pay_Allowed__c = true;
        insert EquifaxConditionSetting2;
        
        Equifax_Mapping__c eMap = new Equifax_Mapping__c();
        eMap.CreditScoreStartValue__c = '';
        eMap.CreditScoreEndValue__c = '';
        eMap.ApprovalType__c = 'CAE1';
        eMap.RiskGrade__c = 'Y';
        eMap.RiskGradeDisplayValue__c = '(NO Hit) Approved: Annual, Easypay, Full Deposit';
        eMap.NoHit__c = true;
        eMap.HitCode__c = '2';
        eMap.HitCodeResponse__c = '';
        insert eMap;
        
        list<FlexFiMessaging__c> flexMsgs = new list<FlexFiMessaging__c>();
        FlexFiMessaging__c msg1 = new FlexFiMessaging__c(Name = 'FlexFiRiskGradeNotValid');
        msg1.ErrorMessage__c = 'Credit is Run but Risk Grade is Not Approved for Loan Application. Please Proceed to ADT Financing.';
        flexMsgs.add(msg1);
        
        FlexFiMessaging__c msg2 = new FlexFiMessaging__c(Name = 'DOBError');
        msg2.ErrorMessage__c = 'Account Does Not Have a Date Of Birth. Provide Date Of Birth Or Proceed to ADT Financing.';
        flexMsgs.add(msg2);
        
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'NoEmailError');
        msg3.ErrorMessage__c = 'Account Does Not Have an Email. Provide an Email Or Proceed to ADT Financing.';
        flexMsgs.add(msg3);
        
        FlexFiMessaging__c msg4 = new FlexFiMessaging__c(Name = 'TnCAcceptedByMessage');
        msg4.ErrorMessage__c = 'Terms & Condition Accepted by';
        flexMsgs.add(msg4);
        
        insert flexMsgs;
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('EquifaxConsumerResponseCalloutMock');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
        ProposalOnlyStates__c Pros = new ProposalOnlyStates__c(Name='States', States__c='IL,LA,MD,MA,MI,MS,SC,TN,TX,UT,VA,NV,FL');
        insert Pros;
    }
    
   /************************************************************************************************************
    * Description: Test Scenario with last check date is older than 90 days or null
    * **********************************************************************************************************/
    
   static testMethod void checkEquifaxScenario() {
       createTestData();
       User u =  [Select Id from User where Id = :UserInfo.getUserId()];
       PartnerConfiguration__c partnerConfig = new PartnerConfiguration__c();
        partnerConfig.PartnerMessage__c = 'Transfer to ADT';
        partnerConfig.CreditCheckApi__c = 'Load Application';
        partnerConfig.PartnerID__c = 'Partner001';
        insert partnerConfig; 
       System.runAs(u) {
           String sBody = '{ "partnerID": "Partner001","partnerRepName": "Test User","opportunityID": "'+opp.Id+'",'+
                           '"callID":"aIG000000288fjsjw","customer": {"taxIDNumber": "1111","dateOfBirth": "1990-01-01", "previousAddress": {'+
                           '"addrLine1": "101 Main Street","addrLine2": "","city": "New York","state": "NY",  "postalCode": "10010"}}}';
           RestRequest req = new RestRequest();
           RestResponse res = new RestResponse();
           req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/checkCredit';
           req.httpMethod = 'POST';//HTTP Request Type
           req.requestBody = Blob.valueof(sBody);
           
           RestContext.request = req;
           RestContext.response= res;
           ADTPartnerCreditCheckAPI.dopostmethod();
           }
   }
   
    /************************************************************************************************************
    * Description: Test Scenario with last check date is older than 90 days or null
    * **********************************************************************************************************/
      static testMethod void nochecktoEquifaxScenario() {
       createTestData();
       accnt.Equifax_Last_Check_DateTime__c = Datetime.now();
       accnt.Equifax_Credit_Score__c = '560';
       accnt.EquifaxApprovalType__c = 'CAE1';
       accnt.EquifaxRiskGrade__c = 'F';
       accnt.Equifax_Last_Four__c = '1111';
       update accnt;
          
       PartnerConfiguration__c partnerConfig = new PartnerConfiguration__c();
        partnerConfig.PartnerMessage__c = 'Transfer to ADT';
        partnerConfig.CreditCheckApi__c = 'Loan Application';
        partnerConfig.PartnerID__c = 'Partner001';
        insert partnerConfig;   
        
       User u =  [Select Id from User where Id = :UserInfo.getUserId()];
       System.runAs(u) {
           String sBody = '{ "partnerID": "Partner001","partnerRepName": "Test User","opportunityID": "'+opp.Id+'",'+
                           '"callID":"aIG000000288fjsjw","customer": {"taxIDNumber": "1111","dateOfBirth": "1990-01-01", "previousAddress": {'+
                           '"addrLine1": "101 Main Street","addrLine2": "","city": "New York","state": "NY",  "postalCode": "10010"}}}';
       
           
           RestRequest req = new RestRequest();
           RestResponse res = new RestResponse();
           req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/checkCredit';
           req.httpMethod = 'POST';//HTTP Request Type
           req.requestBody = Blob.valueof(sBody) ;
       
           RestContext.request = req;
           RestContext.response= res;
           ADTPartnerCreditCheckAPI.dopostmethod();
          

       }
   }

   
     /************************************************************************************************************
    * Description: Opportunity not found
    * **********************************************************************************************************/
      static testMethod void Optynotfound() {
       createTestData();
       accnt.Equifax_Last_Check_DateTime__c = Datetime.now();
       accnt.Equifax_Credit_Score__c = '560';
       accnt.EquifaxApprovalType__c = 'CAE1';
       accnt.EquifaxRiskGrade__c = 'F';
       accnt.Equifax_Last_Four__c = '1111';
       update accnt;
       User u =  [Select Id from User where Id = :UserInfo.getUserId()];
       System.runAs(u) {
           String sBody = '{ "partnerID": "","partnerRepName": "Test User","opportunityID": "",'+
                           '"callID":"aIG000000288fjsjw","customer": {"taxIDNumber": "1111","dateOfBirth": "1990-01-01", "previousAddress": {'+
                           '"addrLine1": "101 Main Street","addrLine2": "","city": "New York","state": "NY",  "postalCode": "10010"}}}';
           
           RestRequest req = new RestRequest();
           RestResponse res = new RestResponse();
           req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/checkCredit';
           req.httpMethod = 'POST';//HTTP Request Type
           req.requestBody = Blob.valueof(sBody) ;
           system.debug('###'+req);    
           RestContext.request = req;
           RestContext.response= res;
           ADTPartnerCreditCheckAPI.dopostmethod();
          

       }
   }
   
    /************************************************************************************************************
    * Description: Opportunity not found
    * **********************************************************************************************************/
      static testMethod void emptyJson() {
       createTestData();
       accnt.Equifax_Last_Check_DateTime__c = Datetime.now();
       accnt.Equifax_Credit_Score__c = '560';
       accnt.EquifaxApprovalType__c = 'CAE1';
       accnt.EquifaxRiskGrade__c = 'F';
       accnt.Equifax_Last_Four__c = '1111';
       
       update accnt;
       User u =  [Select Id from User where Id = :UserInfo.getUserId()];
       System.runAs(u) {
        
           String sBody = '';
           RestRequest req = new RestRequest();
           RestResponse res = new RestResponse();
           req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() +  '/services/apexrest/checkCredit';
           req.httpMethod = 'POST';//HTTP Request Type
           req.requestBody = Blob.valueof(sBody) ;
           system.debug('###'+req);    
           RestContext.request = req;
           RestContext.response= res;
           ADTPartnerCreditCheckAPI.dopostmethod();
          

       }
   }
}