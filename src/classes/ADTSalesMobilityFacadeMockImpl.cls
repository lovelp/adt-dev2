@isTest
global class ADTSalesMobilityFacadeMockImpl implements WebServiceMock {
	
   public static Boolean switchValues = false;
   public static Boolean exceptionFlag=false;
    
   global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) 
   {
        Boolean responseAvailable = false;
        if(exceptionFlag)
       	throw new IntegrationException('This is an integration exception');  
       
        if( soapAction == 'http://www.adt.com/SalesMobility/setAppointment' )
        {
        	ADTSalesMobilityFacade.setAppointmentResponse_element response_x
        		= new ADTSalesMobilityFacade.setAppointmentResponse_element();
        	response_x.MessageID = 'testID';
        	response_x.MessageStatus = IntegrationConstants.MESSAGE_STATUS_OK;
        	response_x.Error = '';
        	response_x.TelemarAccountNumber = '';
        	response_x.TelemarScheduleID = (switchValues == true)?'a321':'a123';
        	response_x.TaskCode = '';
        	response.put('response_x', response_x);
        	responseAvailable = true;
        }
        else if( soapAction == 'http://www.adt.com/SalesMobility/setUnavailableTime' ){
        	ADTSalesMobilityFacade.setUnavailableTimeResponse_element response_x
        		= new ADTSalesMobilityFacade.setUnavailableTimeResponse_element();
	        response_x.MessageID = 'testID';
	        response_x.MessageStatus = IntegrationConstants.MESSAGE_STATUS_OK;
	        response_x.Error = '';
	        response_x.TelemarScheduleID = (switchValues == true)?'sg321':'sg123';
        	response.put('response_x', response_x);
	        responseAvailable = true;
        }
        else if( responseAvailable == false ) response.put('response_x', null); 
       	
   }
   
}