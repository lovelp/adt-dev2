public class AccountSearchController {
    @AuraEnabled
    public static List<AccountWrapper> searchAccount(string jobNumber,string custNumber,string siteNumber){
        System.debug('The jobNumber is'+jobNumber+' '+custNumber+' '+siteNumber);
        List<Quote_Job__c > quoteJobList = new List<Quote_Job__c >();
        Id accId = null;
        list<Account> accList = new list<Account>();
        list<AccountWrapper> accWrapList = new list<AccountWrapper>();
        if(string.isNotBlank(jobNumber)){
            //Query to find account from quote based on jobNumber
            quoteJobList = [SELECT id,JobNo__c,ParentQuote__c,ParentQuote__r.Account__c,ParentQuote__r.scpq__QuoteId__c,ParentQuote__r.Job_Details__c,ParentQuote__r.scpq__Status__c,
                            ParentQuote__r.MMBJobNumber__c,ParentQuote__r.MMBOrderType__c,ParentQuote__r.QuotePaidOn__c,ParentQuote__r.Account__r.EquifaxApprovalType__c,
                            ParentQuote__r.Account__r.EquifaxRiskGrade__c,ParentQuote__r.Account__r.TelemarAccountNumber__c,ParentQuote__r.Account__r.Equifax_Last_Check_DateTime__c 
                            FROM Quote_Job__c WHERE JobNo__c != null AND JobNo__c =: jobNumber LIMIT 1];
            if(quoteJobList.size() > 0 && quoteJobList != null){
                if(quoteJobList[0].ParentQuote__r.Account__c != null){
                    accId = quoteJobList[0].ParentQuote__r.Account__c;
                }
            }
            if(accId != null){
                accWrapList = getAccountList(null,null,accId);
            }
        }
        else if(string.isNotBlank(siteNumber) && string.isNotBlank(custNumber)){
            accWrapList = getAccountList(custNumber,siteNumber,'');
        } 
        System.debug('The acclist is'+accWrapList);
        return accWrapList;
    }
    public static List<AccountWrapper> getAccountList(string custNumber,string siteNumber,string aId){
        list<Account> accList = new list<Account>();
        list<AccountWrapper> accWrapList = new list<AccountWrapper>();
        string query = '';
        boolean continueFlag = false;
        String lastActivity = '';
        query = 'SELECT Id,Name,EquifaxRiskGrade__c,DispositionCode__c,EquifaxApprovalType__c,Equifax_Last_Check_DateTime__c,TelemarAccountNumber__c,MMBSiteNumber__c,MMBCustomerNumber__c,BillingAddress,Email__c,';
        query = query +'LastActivityDate__c,SalesAppointmentDate__c,ScheduledInstallDate__c,TransitionDate1__c,TransitionDate2__c,TransitionDate3__c,TransitionDate4__c,';
        query = query+'AccountFullStatus__c,SinceLastDisposition__c,ResaleTownNumber__c,Lead_Origin__c,SiteCity__c,SiteState__c,SiteStreetName__c,SitePostalCode__c,SiteStreet__c,SiteStreet2__c';
        query+=' FROM  account WHERE ';
        if(string.isNotBlank(custNumber) && string.isNotBlank(siteNumber) && string.isNotBlank(aId)){
            System.debug('Inside all');
            query = query + 'MMBSiteNumber__c=:siteNumber and MMBCustomerNumber__c =:custNumber and Id =:aId';
            continueFlag = true;
        }else if(continueFlag == false && string.isNotBlank(custNumber) && string.isNotBlank(siteNumber)){
            System.debug('Inside second');
            query = query + 'MMBSiteNumber__c=:siteNumber and MMBCustomerNumber__c =:custNumber';
            continueFlag = true;
        }else if (continueFlag == false && string.isNotBlank(aId)){
            System.debug('Inside third');
            query= query + 'Id =:aId';
        }
        accList = Database.query(query);
        if(accList.size() > 0 && accList != null){
            for(Account acc:accList){
                AccountWrapper accWrap = new AccountWrapper();
                accWrap.accId = acc.id;
                accWrap.accName = acc.Name;
                accWrap.address = acc.SiteStreet__c+' '+acc.SiteStreetName__c+' '+acc.SiteCity__c+' '+acc.SiteState__c+' '+acc.SitePostalCode__c;
                accWrap.email = acc.email__c;
                accWrap.telemarNumber =  acc.TelemarAccountNumber__c;
                accWrap.leadOrigin = acc.Lead_Origin__c;
                accWrap.customerNumber = acc.MMBCustomerNumber__c;
                accWrap.siteNumber = acc.MMBSiteNumber__c;
                accWrap.resaleTownNumber = acc.ResaleTownNumber__c;
                accWrap.lastDispoCode = acc.DispositionCode__c;
                accWrap.accFullStatus = acc.AccountFullStatus__c;
                accWrap.lastActivityDate = acc.LastActivityDate__c;
                lastActivity = NSCLeadCaptureController.lastActivityDateValue(acc.SalesAppointmentDate__c, acc.ScheduledInstallDate__c, acc.TransitionDate1__c,acc.TransitionDate2__c, acc.TransitionDate3__c, acc.TransitionDate4__c);
                if(String.isNotBlank(lastActivity)){
                    List<String> lastActivityList=lastActivity.split(',');
                    accWrap.lastActivityType = lastActivityList[1];
                }
                accWrapList.add(accWrap);
            }
        }
        return accWrapList;
    }
    @AuraEnabled
    public static String getUIThemeDescription() {
        String theme = UserInfo.getUiThemeDisplayed();
        return theme;
    }
    public class AccountWrapper{
        @AuraEnabled public String accId;
        @AuraEnabled public String accName;
        @AuraEnabled public String address;
        @AuraEnabled public String email;
        @AuraEnabled public String telemarNumber;
        @AuraEnabled public String leadOrigin;
        @AuraEnabled public String customerNumber;
        @AuraEnabled public String siteNumber;
        @AuraEnabled public String resaleTownNumber;
        @AuraEnabled public string lastDispoCode;
        @AuraEnabled public String accFullStatus;
        @AuraEnabled public Date lastActivityDate;
        @AuraEnabled public String lastActivityType;
    }
    //https://adt--adtdev2.lightning.force.com/lightning/cmp/c__AccountDisplay?c__jobNumber=1234&c__customerNumber=1234&c__siteNumber=1234
}