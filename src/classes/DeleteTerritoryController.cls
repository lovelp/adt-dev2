/************************************* MODIFICATION LOG ********************************************************************************************
* DeleteTerritoryController
*
* DESCRIPTION : Supports the delete function for Territories.
*               It is a Controller Extension in the SFDC MVC architecture.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2011			- Original Version
*
*													
*/
public class DeleteTerritoryController {

	public Territory__c terr;
	public DeleteTerritoryController(ApexPages.StandardController controller)
	{
		terr = (Territory__c)controller.getRecord();
	}

	public pageReference deleteAndRedirect()
	{
		delete terr;
		String prefix = Schema.Sobjecttype.Territory__c.getKeyPrefix();
		return new pageReference('/' + prefix + '/o');
	}
}