@isTest
global class RequestQueueWebServiceMockImpl implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       ADTSalesMobilityFacade.setAppointmentResponse_element respElement = new ADTSalesMobilityFacade.setAppointmentResponse_element();
       respElement.messageStatus='OK';
       respElement.error='';
       respElement.TelemarAccountNumber='70000007';
       
       response.put('response_x', respElement);
   }
}