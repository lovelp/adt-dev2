/************************************* MODIFICATION LOG ********************************************************************************************
* ShowLeadOwner
*
* DESCRIPTION : Supports the display of the owner of a lead.
*               It is a Controller Extension in the SFDC MVC architecture.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover          	   3/1/2012			- Original Version
*
*													
*/
public with sharing class ShowLeadOwner {

	public string OwnerName {get; set;}

	public ShowLeadOwner(ApexPages.StandardController controller)
	{
		Id leadid = controller.getId();
		Lead leadInfo = [select ownerid from Lead where id = : leadid limit 1];
		
		User owner = [select name from User where id = : leadInfo.OwnerId Limit 1];
		OwnerName = owner.Name;
	}	
	
}