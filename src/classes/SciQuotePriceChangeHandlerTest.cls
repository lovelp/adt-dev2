@isTest
public class SciQuotePriceChangeHandlerTest {
    @isTest public static void ProcessSciQuoteAfterUpdate(){
        User salesRep = TestHelperClass.createSalesRepUser();
        Account a1;
        Account a2;
        Opportunity opp;
        scpq__SciQuote__c sq;
        System.runAs(salesRep) {
            list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
            rgvs.add(new ResaleGlobalVariables__c(Name='DataRecastDisableAccountTrigger', value__c='FALSE'));
            rgvs.add(new ResaleGlobalVariables__c(Name='IntegrationUser', value__c='Integration User'));
            rgvs.add(new ResaleGlobalVariables__c(Name='RIFAccountOwnerAlias', value__c='ADT RIF'));
            rgvs.add(new ResaleGlobalVariables__c(Name='DataConversion', value__c='FALSE'));
            insert rgvs;
            // create accounts
            a1 = TestHelperClass.createAccountData();
            opp = New Opportunity();
            opp.AccountId = a1.Id;
            opp.Name = 'TestOpp1';
            opp.StageName = 'Prospecting';
            opp.CloseDate = Date.today();
            insert opp;
        }
        Test.startTest();
        System.runAs(salesRep) {
            sq = New scpq__SciQuote__c();
            sq.Name = 'TestQuote1';
            sq.Job_Details__c = '<?xml version="1.0" encoding="UTF-8"?><Jobs AltPricing="Y" DOAApprovalLockLevel= "Director" DOAApprovalLockID="Y" CLVScore = "4" ChurnRisk="High" CommRepHRID="Z123467Z" CommRepName="Testing" CommRepPhone="(408) 507-3112" CommRepType="FIELD"  ContractGeneratedBy="unitTestSalesRep1@testorg.com"  POEStatus= "Error"  POEErrorCode = "Error in submission"  POEErrorDescription = "Test error message" CustomerEMailID= "test2@adt.com" EmailChangeUserID = "test1@email.com" DOALock="N" DepositWaived="N" DepositWaiverLock="N" EasyPay="Y" EquifaxApprovalType="CAE1" EquifaxRiskGrade="W" PaymentTakenBy="unitTestSalesRep1@testorg.com" SubmittedBy ="unitTestSalesRep1@testorg.com">    <Job ADSCAddOn="0.00" ADSCBase="379.00" ADSCCorpDiscount="0.00" ADSCInstallPrice="508.00" ADSCInstallerCorpDiscount="0.00"  ADSCInstallerLineDiscount="0.00" ADSCOtherAddOn="129.00"  ADSCOtherCorpDiscount="0.00" ANSCAdditionalMonitoring="0.00" ANSCAdditionalQSP="0.00" ANSCBaseMonitoring="32.99" ANSCBaseQSP="5.00" ANSCCorpDiscount="0.00" ANSCFinalRMR="37.99"  ANSCInstallerCorpDiscount="0.00"  ANSCInstallerLineDiscount="0.00"  FirstAvailableInstallDate = "10/20/2019"  InstallDate = "03/29/2019" ANSCOtherCorpDiscount="0.00"  DLL="N" DOALock="N"   DOAApprovalComment = "test" DOAApproverLevel= "Director"  DOAModifiedBy = "test@test.com" DefaultContract="SP006" JobNo="3456" JobType="INST" ProductFamily="Home Security" RequireElectrician="N" RequireLocksmith="N" SaleType="NewSale"  TaskCode="RIF" TechnologyId="1025">  <Packages> <Package ID="L1 004" Name="Traditional + Life Safety + Cellguard"/>  </Packages>  <Promotions Name="test"/> <JobLines>            <JobLine Item="L1 V128 HW"  Name="(HW) Vista128 Control + Std. KP" Package="L1 004" PackageQty="1.00"/>   </JobLines>    </Job></Jobs>';
            sq.scpq__OpportunityId__c = opp.Id;
            sq.scpq__OrderHeaderKey__c = 'whoknows';
            sq.scpq__SciLastModifiedDate__c = Date.today();
            sq.scpq__Status__c = 'Created';
            sq.Activity_Data__c = '<?xml version="1.0" encoding="UTF-8"?><Activity>    <DOAApproval>        <Job ApprovalComment="Test DOA TEST" ApproverLevel="Director"            Family="Home Security" HoldRequired="true"            InstallPercent="45.00" MonitorPercent="0.00" QSPPercent="0.00">            <Package Name="[L1 001] - Traditional"/>        </Job>    </DOAApproval>   <DepositWaived>        <Job ApprovalComment="Test DOA TEST" ApproverLevel="Director"            Family="Home Security" HoldRequired="true"            InstallPercent="45.00" MonitorPercent="0.00" QSPPercent="0.00">            <Package Name="[L1 001] - Traditional"/>        </Job>    </DepositWaived>   <EmailActivity>        <Job ApprovalComment="Test DOA TEST" ApproverLevel="Director"            Family="Home Security" HoldRequired="true"            InstallPercent="45.00" MonitorPercent="0.00" QSPPercent="0.00">            <Package Name="[L1 001] - Traditional"/>        </Job>    </EmailActivity></Activity>';
            insert sq;
            sq.scpq__Status__c = 'Paid';
            update sq;
        }
        scpq__SciQuote__c sq2 = [select Account__c, Name from scpq__SciQuote__c where id = :sq.id];
        System.assert(sq2.Account__c != null, 'Account ID should not be null');
        System.assertEquals(a1.id, sq2.Account__c, 'Account ID does not match');
        Test.stopTest();
    }
    
    @isTest static void testCreateSciQuote1() {
        
        //list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
        //rgvs.add(new ResaleGlobalVariables__c(Name='DataRecastDisableAccountTrigger', value__c='TRUE'));
        //insert rgvs;
        
        User salesRep = TestHelperClass.createSalesRepUser();
        //TestHelperClass.createReferenceDataForTestClasses();
        system.debug('==> '+salesRep);
        Account a1;
        Account a2;
        Opportunity opp;
        scpq__SciQuote__c sq;
        Quote_Job__c qj;
        System.runAs(salesRep) {
            list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
            rgvs.add(new ResaleGlobalVariables__c(Name='DataRecastDisableAccountTrigger', value__c='FALSE'));
            rgvs.add(new ResaleGlobalVariables__c(Name='IntegrationUser', value__c='Integration User'));
            rgvs.add(new ResaleGlobalVariables__c(Name='RIFAccountOwnerAlias', value__c='ADT RIF'));
            rgvs.add(new ResaleGlobalVariables__c(Name='DataConversion', value__c='FALSE'));
            rgvs.add(new ResaleGlobalVariables__c(Name='SendAppointmentEmails', value__c='true'));
            insert rgvs;
            
            // create accounts
            a1 = TestHelperClass.createAccountData();
            a1.Rep_User__c = salesRep.id;
            a1.Email__c = 'test@adt.com';
            update a1;
            opp = New Opportunity();
            opp.AccountId = a1.Id;
            opp.Name = 'TestOpp1';
            opp.StageName = 'Prospecting';
            opp.CloseDate = Date.today();
            insert opp;
        }
        
        Test.startTest();
        
        System.runAs(salesRep) {
            SciQuoteTriggerUtilities squ = new SciQuoteTriggerUtilities();
            Quote_Job_Line__c qjl = new Quote_Job_Line__c();
            sq = New scpq__SciQuote__c();
            sq.Name = 'TestQuote1';
            sq.scpq__OpportunityId__c = opp.Id;
            sq.scpq__OrderHeaderKey__c = 'whoknows';
            sq.scpq__SciLastModifiedDate__c = Date.today();
            sq.scpq__Status__c = 'Created';
            sq.Activity_Data__c = '<?xml version="1.0" encoding="UTF-8"?>'
                +'<Activity>'
                +'<DOAApproval>'
                +'<Job ApprovalComment="Test DOA TEST" ApproverLevel="Director" Family="Home Security" HoldRequired="true" InstallPercent="45.00" MonitorPercent="0.00" QSPPercent="0.00">'
                +'<Package Name="[L1 001] - Traditional"/>'
                +'</Job>'
                +'</DOAApproval>'
                +'<DepositWaived>'
                +'<Job ApprovalComment="Test DOA TEST" ApproverLevel="Director" Family="Home Security" HoldRequired="true" InstallPercent="45.00" MonitorPercent="0.00" QSPPercent="0.00">'
                +'<Package Name="[L1 001] - Traditional"/>'
                +'</Job>'
                +'</DepositWaived>'
                +'<EmailActivity>'
                +'<Job ApprovalComment="Test DOA TEST" ApproverLevel="Director" Family="Home Security" HoldRequired="true" InstallPercent="45.00" MonitorPercent="0.00" QSPPercent="0.00">'
                +'<Package Name="[L1 001] - Traditional"/>'
                +'</Job>'
                +'</EmailActivity>'
                +'</Activity>';
            sq.Job_Details__c = '<?xml version="1.0" encoding="UTF-8"?>'
                +'<Jobs AltPricing="Y" DOAApprovalLockLevel= "Director" DOAApprovalLockID="Y" CLVScore = "4" ChurnRisk="High" CommRepHRID="Z123467Z" CommRepName="Testing" CommRepPhone="(408) 507-3112" CommRepType="FIELD"  ContractGeneratedBy="unitTestSalesRep1@testorg.com"  POEStatus= "Error"  POEErrorCode = "Error in submission"  POEErrorDescription = "Test error message" CustomerEMailID= "test2@adt.com" EmailChangeUserID = "test1@email.com" DOALock="N" DepositWaived="N" DepositWaiverLock="N" EasyPay="Y" EquifaxApprovalType="CAE1" EquifaxRiskGrade="W" PaymentTakenBy="unitTestSalesRep1@testorg.com" SubmittedBy ="unitTestSalesRep1@testorg.com">'
                    +'<Job ADSCAddOn="0.00" ADSCBase="379.00" ADSCCorpDiscount="0.00" ADSCInstallPrice="508.00" ADSCInstallerCorpDiscount="0.00"  ADSCInstallerLineDiscount="0.00" ADSCOtherAddOn="129.00"  ADSCOtherCorpDiscount="0.00" ANSCAdditionalMonitoring="0.00" ANSCAdditionalQSP="0.00" ANSCBaseMonitoring="32.99" ANSCBaseQSP="5.00" ANSCCorpDiscount="0.00" ANSCFinalRMR="37.99"  ANSCInstallerCorpDiscount="0.00"  ANSCInstallerLineDiscount="0.00"  FirstAvailableInstallDate = "10/20/2019"  InstallDate = "03/29/2019" ANSCOtherCorpDiscount="0.00"  DLL="N" DOALock="N"   DOAApprovalComment = "test" DOAApproverLevel= "Director"  DOAModifiedBy = "test@test.com" DefaultContract="SP006" JobNo="3456" JobType="INST" ProductFamily="Home Security" RequireElectrician="N" RequireLocksmith="N" SaleType="NewSale"  TaskCode="RIF" TechnologyId="1025"> '
                        +'<PriceChanges Name="Testing">'
                        	+'<PriceChange ItemID="1234" LoginId="'+Userinfo.getUserName()+'" Original="1234" Current="5678"></PriceChange>'
                        +'</PriceChanges>'
                		+'<Packages>'
                            +'<Package ID="L1 004" Name="Traditional + Life Safety + Cellguard"/>'
                		+'</Packages>'
                		+'<Promotions Name="test"/>'
                		+'<JobLines>'
                			+'<JobLine Item="L1 V128 HW"  Name="(HW) Vista128 Control + Std. KP" Package="L1 004" PackageQty="1.00"/>'
                		+'</JobLines>'
                	+'</Job>'
                +'</Jobs>';
            insert sq;
            sq.scpq__Status__c = 'Paid';
            sq.MMBJobNumber__c = '234567';
            update sq;
            sq.Job_Details__c = '<?xml version="1.0" encoding="UTF-8"?>'
                +'<Jobs AltPricing="Y" DOAApprovalLockLevel= "Director" DOAApprovalLockID="Y" CLVScore = "4" ChurnRisk="High" CommRepHRID="Z123467Z" CommRepName="Testing" CommRepPhone="(408) 507-3112" CommRepType="FIELD"  ContractGeneratedBy="unitTestSalesRep1@testorg.com"  POEStatus= "Error"  POEErrorCode = "Error in submission"  POEErrorDescription = "Test error message" CustomerEMailID= "test2@adt.com" EmailChangeUserID = "test1@email.com" DOALock="N" DepositWaived="N" DepositWaiverLock="N" EasyPay="Y" EquifaxApprovalType="CAE1" EquifaxRiskGrade="W" PaymentTakenBy="unitTestSalesRep1@testorg.com" SubmittedBy ="unitTestSalesRep1@testorg.com">'
                    +'<Job ADSCAddOn="0.00" ADSCBase="379.00" ADSCCorpDiscount="0.00" ADSCInstallPrice="508.00" ADSCInstallerCorpDiscount="0.00"  ADSCInstallerLineDiscount="0.00" ADSCOtherAddOn="129.00"  ADSCOtherCorpDiscount="0.00" ANSCAdditionalMonitoring="0.00" ANSCAdditionalQSP="0.00" ANSCBaseMonitoring="32.99" ANSCBaseQSP="5.00" ANSCCorpDiscount="0.00" ANSCFinalRMR="37.99"  ANSCInstallerCorpDiscount="0.00"  ANSCInstallerLineDiscount="0.00"  FirstAvailableInstallDate = "10/20/2019"  InstallDate = "03/29/2019" ANSCOtherCorpDiscount="0.00"  DLL="N" DOALock="N"   DOAApprovalComment = "test" DOAApproverLevel= "Director"  DOAModifiedBy = "test@test.com" DefaultContract="SP006" JobNo="3456" JobType="INST" ProductFamily="Home Security" RequireElectrician="N" RequireLocksmith="N" SaleType="NewSale"  TaskCode="RIF" TechnologyId="1025"> '
                        +'<PriceChanges Name="Testing">'
                        	+'<PriceChange ItemID="2345" LoginId="'+Userinfo.getUserName()+'" Original="2345" Current="9865"></PriceChange>'
                        +'</PriceChanges>'
                		+'<Packages>'
                            +'<Package ID="L1 004" Name="Traditional + Life Safety + Cellguard"/>'
                		+'</Packages>'
                		+'<Promotions Name="test"/>'
                		+'<JobLines>'
                			+'<JobLine Item="L1 V128 HW"  Name="(HW) Vista128 Control + Std. KP" Package="L1 004" PackageQty="1.00"/>'
                		+'</JobLines>'
                	+'</Job>'
                +'</Jobs>';
            sq.scpq__Status__c = 'Signed';   
            update sq;
            qj = new Quote_Job__c();
            qj.Job_Status__c = 'submitted';
            qj.ParentQuote__c = sq.id;
            insert qj; 
            qjl.Name = '1234556';
            qjl.Quote_Job__c = qj.id;
            insert qjl;
            list<Quote_Job_Line__c> ls = new list<Quote_Job_Line__c>();
            ls.add(qjl);  
            sq.MMBCustomerNumber__c = '11122233';
            sq.scpq__Status__c = 'Submitted';
            update sq;
            sq.Activity_Type__c = 'ReworkQuote';
            sq.scpq__Status__c = 'Created';
            update sq;
            
            Quote_Activity__c qa = new Quote_Activity__c ();
            qa.name = 'test Quote Activity';
            qa.Account__c = a1.id;
            qa.ADSC_Discount__c	 = 10;
            qa.ANSC_Discount__c = 10;
            qa.Comments__c = 'Test Comment';
            qa.PackageID__c = '1233455';
            qa.QSP_Discount__c = 10;
            qa.Quote__c = sq.id;
            insert qa;
            //RoleUtils.HierarchyLevel curHierarchyLevel = RoleUtils.getUserAsigneeHierarchy(salesRep);
            SciQuoteTriggerUtilities.emailHierarchy(qa, sq, salesRep);
        }
        scpq__SciQuote__c sq2 = [select Account__c, Name from scpq__SciQuote__c where id = :sq.id];
        Test.stopTest();
    }
}