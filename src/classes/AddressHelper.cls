/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : AddressHelper.cls has methods that perform common, often re-used functions for Address
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            02/23/2012          - Origininal Version
* Mike Tuckman                  03/22/2014          - (Hermes) Support Street 2
* Nikita Jain                   08/09/2017          - HRM 7777 Lead creation Error  
* Srinivas Yarramsetti          09/27/2018          HRM - 7486      - Remove extra spaces from address fields
*/
public with sharing class AddressHelper{
    
    public static Map<String, Address__c> upsertAddresses(Map<String, Address__c> newAddresses){
        Map<String, Address__c> returnAddresses = new Map<String, Address__c>();
        List<Database.upsertResult> upsertResult = Database.upsert(newAddresses.values(), Address__c.OriginalAddress__c , false);
        List<Id> AddressIds = new List<Id>();
        for(Database.upsertResult result : upsertResult){
            if (result.isSuccess()) 
                AddressIds.add(result.getId());
        }
        List<Address__c> Addresses = [SELECT Id, CountryCode__c, County__c, OriginalAddress__c, Street__c, City__c, State__c, PostalCode__c, PostalCodeAddOn__c FROM Address__c WHERE id in : AddressIds];
        List<Address__c> AddressesCopy=new List<Address__c>();
        for(Address__c addr : Addresses){
            String orignAdd = addr.OriginalAddress__c;
            String upperorignAdd = addr.OriginalAddress__c.toUpperCase();
            if (!orignAdd.equals(upperorignAdd)){       
                addr.OriginalAddress__c=addr.OriginalAddress__c.toUpperCase();
                //addr.StandardizedAddress__c=addr.StandardizedAddress__c.toUpperCase();  
                AddressesCopy.add(addr);
            }                                                                       //HRM 7777 Lead creation Error  
            returnAddresses.put(addr.OriginalAddress__c.toUpperCase(), addr);       //HRM 7777 Lead creation Error      
        }
        if (!AddressesCopy.isEmpty()){
             update AddressesCopy;      
        }//HRM 7777 Lead creation Error 
        return returnAddresses;      
    }
    
    public static String getConcatenatedAddress(String AddressStreet, String City, String State, String PostalCode){
        return getConcatenatedAddress(AddressStreet, '', City, State, PostalCode);
    }

    public static String getConcatenatedAddress(String AddressStreet, String AddressStreet2, String City, String State, String PostalCode)
    {
        String returnAddress = '';
        returnAddress += AddressStreet.toUpperCase()+'+';
        if (AddressStreet2 != null && AddressStreet2.length() > 0)
            returnAddress += AddressStreet2.toUpperCase()+'+';
        returnAddress += City.toUpperCase() +'+'+ State.toUpperCase() +'+'+ PostalCode;
        return Utilities.removeExtraSpaces(returnAddress);//HRM-7486;
    }
    
    public static String getStandardizedAddress(String AddressStreet, String City, String State, String PostalCode){
        return getStandardizedAddress(AddressStreet, '',  City,  State,  PostalCode);
    }
    public static String getStandardizedAddress(String AddressStreet, String AddressStreet2, String City, String State, String PostalCode)
    {
        String returnAddress = '';
        returnAddress += AddressStreet.toUpperCase() + ', ';
        if (AddressStreet2 != null && AddressStreet2.length() >0)
            returnAddress += AddressStreet2.toUpperCase() + ', ';
        returnAddress += City.toUpperCase() + ', ' + State.toUpperCase() + ' ' + PostalCode;
        return Utilities.removeExtraSpaces(returnAddress);//HRM-7486;
    }

}