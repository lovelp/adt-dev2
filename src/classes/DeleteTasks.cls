/************************************* MODIFICATION LOG ********************************************************************************************
* DeleteTasks
*
* DESCRIPTION : Deletes the tasks 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    TICKET					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Giribabu Geda               	05/08/2018               						- Original Version
*/
global class DeleteTasks implements Database.batchable<SObject> , Database.stateful{

    public Integer totalSuccessRecords = 0; 
    public Integer totalFailureRecords = 0;
    public Integer totalNoOfRecords = 0;

    global Database.QueryLocator start(Database.BatchableContext BC){
        String query='' ;
        //set<String> statusSet = new set<String>();
        String Status ='';
        Integer NoOfDays = 0;
        if(BatchGlobalVariables__c.getinstance('DaysOverDuefortaskdeletion') != null && BatchGlobalVariables__c.getinstance('Statusesfortaskdeletion') != null){
            Status = BatchGlobalVariables__c.getinstance('Statusesfortaskdeletion').value__c;
            NoofDays = integer.valueof(BatchGlobalVariables__c.getinstance('DaysOverDuefortaskdeletion').value__c);
        }
        if(String.isNotBlank(Status) && NoOfDays > 0){
        	query = 'Select Id  FROM Task';
        	
        	query = query +' where Status IN ('+ Status +') AND ActivityDate < LAST_N_DAYS:'+ NoofDays;
        	//query = query +' where Status IN :statusSet AND ActivityDate < LAST_N_DAYS:'+ NoOfDays;
             
        }else {
        	query = 'Select Id  FROM Task limit 0';
        }
        System.Debug('query====='+query);
        return Database.getQueryLocator(query); 
    }
    
    global void execute(Database.BatchableContext BC, List<task> scope){
        if(scope.size() > 0){
        	totalNoOfRecords +=	scope.size();

			// Delete the existing Task records
        	Database.DeleteResult[] drList = database.delete(scope,false);
        	for(Database.DeleteResult dr : drList){
        		if (dr.isSuccess())
        			totalSuccessRecords++;
        		else
        			totalFailureRecords++;
        	}
        }
    }
    
    // The finish method is called at the end after the deletion process is completed.
    global void finish(Database.BatchableContext BC){
        String targetobjId = BatchGlobalVariables__c.getinstance('GlobalAdminId').value__c;
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		
		String subject = 'Deleted tasks overdue for more than '+BatchGlobalVariables__c.getinstance('DaysOverDuefortaskdeletion').value__c +'days';
		// Send email to Global Admin
		email.setTargetObjectId(targetobjId);
		email.setSubject(subject);
		email.setPlainTextBody('Deleted tasks over due by '+BatchGlobalVariables__c.getinstance('DaysOverDuefortaskdeletion').value__c +' days. Total number of overdue tasks :'+totalNoOfRecords +'. Records deleted sucessfully : '+totalSuccessRecords  +'. Records failed:'+totalFailureRecords);
		email.setSaveAsActivity(false);
		Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
    }
}