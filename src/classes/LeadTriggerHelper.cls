/************************************* MODIFICATION LOG ********************************************************************************
 *
 * DESCRIPTION : LeadTriggerHelper.cls has methods that govern behaviour of inserted and updated Lead records
 *
 *---------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                  REASON
 *---------------------------------------------------------------------------------------------------------------------------------------
 * Shivkant Vadlamani            02/23/2012          - Original Version
 * Erin McGee                    02/21/2013          - Update for Touched only being changed by Lead Owner
 * Erin McGee                    03/20/2013          - Added change to not update Disposition Date when Disposition "Mailed"
 * Jaydip Bhattacharya           06/19/2013          - Modified for Sprint#11, to populate other disposition date field.
 * Erin McGee                    06/20/2013          - Added Repeat Disposition Functionality
 * Erin McGee                    06/21/2013          - Added Daily Threshold Count      
 * Jaydip Bhattacharya           06/24/2013          - Modified for Sprint#12, to populate Lead Type as Pre Mover for BUDCO leads
 * Erin McGee                    07/10/2013          - Moved Daily Threshold Updates to perform after insert and update
 * Jaydip Bhattacharya           08/01/2013          - To Assign Type Pre Mover, changed values PM to PN and PR to PA
 * Jaydip Bhattacharya           08/07/2013          - Remove parts to eneter data to daily threshold. This is replaced with new code in batch job.
 * Mike Tuckman (001)            11/01/2013          - Support for Devcon
 * Leonard Alarcon               01/09/2014          - Added in ProcessLeadsBeforeInsert(...) for 'BUDCO' feeds, a check for NewMoverType = "AP"
 * Mike Tuckman (002)            03/22/2014          - (Hermes) Added edit of address fields - do not allow if user is using new address valdiation
 * Leonard Alarcon               11/11/2014          - Added in ProcessLeadsBeforeInsert(...) for 'BUDCO' feeds, Story#686 a check for NewMoverType = "CI" and "CA"
 * Leonard Alarcon               01/22/2015          - Added in ProcessLeadsBeforeInsert(...) addition for Amplifinity integration Type__c = "AMP" 
 * Siddarth Asokan               08/09/2016          - Added in ProcessLeadsBeforeInsert(...) to throw an error on the BUDCO lead if NewMoverType__c is missing/ not in our valid list
 * Abhinav Pandey                09/19/2018          - HRM 7996 - Added gryphon check for disposition creation
 * Siddarth Asokan               05/20/2019          - HRM 9708 - Run auto owner assignment
 * Nitin Gottiparthi             07/18/2019          HRM-10370     - Modified ProcessLeadsAfterUpdate to Send email notification when lead assigned to comm rep
 * Siju Varghese                 08/26/2019          - Added flag to avoid duplicate email notification\
 *Abhinav Pandey                 09/19/2019          HRM - 10437   - Possible Now Gryphon changes
 */
public with sharing class LeadTriggerHelper 
{
    public static Boolean emailNotificationFlag = false; // Siju Added 
    public static Boolean isPossibleNowRun = false;
    private static RecordType systemGenLeadRecType = Utilities.getRecordType(RecordTypeName.SYSTEM_GENERATED_LEAD, 'Lead'); 
    private static RecordType UserEnteredLeadRecType = Utilities.getRecordType(RecordTypeName.USER_ENTERED_LEAD, 'Lead');
    private static RecordType commercialRecordType = Utilities.getRecordType(RecordTypeName.COMMERCIAL_LEAD, 'Lead');
    private static Id dataConversionUser = Utilities.getDataConversionUser();
    private static Set<String> LeadDispositionContext; 

    static {
        LeadDispositionContext = new Set<String>();
    }
    
    public static void ProcessLeadsBeforeInsert(List<Lead> NewLeads){       
        System.Debug('NewLeads====='+NewLeads);
        map<String, Address__c> validNewAddresses = new map<String, Address__c>();
        list<Lead> BUDCO_Leads = new list<Lead>(); 
        list<Lead> Manual_Leads = new list<Lead>(); 
        list<Lead> TR_Leads = new list<Lead>(); 
        map<String, Lead> Salesgenie_LeadsMap = new map<String, Lead>(); 
        list<Lead> HOA_Leads = new list<Lead>(); 
        
        // Retrieve userID in order to verify if user is home Health
        String s_UserId = UserInfo.getUserId();
        User UserObj = [select id, Business_Unit__c from User where id = : s_UserId Limit 1];
        
        // Store ID of HOA Rehash Owner
        ID hoaOwnerID = Utilities.getHOARehashOwner();      
        
        //Added by TCS for HRM 475
        Map<String, String> alphaconver = new Map<String, String>();
        for(AlphaConversion__c ac: [SELECT id, Name, AlphaCharValue__c FROM AlphaConversion__c]){
            alphaconver.put(ac.name, ac.AlphaCharValue__c);
        }
            
        for(Lead ld : NewLeads){         
            // Added by TCS for 475
            if(System.Label.SwitchForTranslation == 'True'){
                if (String.isNotBlank(ld.FirstName))
                    ld.FirstName = Utilities.NormalizeString(ld.FirstName, alphaconver);
                if (String.isNotBlank(ld.LastName))
                    ld.LastName = Utilities.NormalizeString(ld.LastName, alphaconver );
                if (String.isNotBlank(ld.SiteCity__c))
                    ld.SiteCity__c = Utilities.NormalizeString(ld.SiteCity__c, alphaconver);
                if (String.isNotBlank(ld.SiteStreet__c))
                    ld.SiteStreet__c = Utilities.NormalizeString(ld.SiteStreet__c, alphaconver );
                if(String.isNotBlank(ld.SiteStreet2__c))
                    ld.SiteStreet2__c =  Utilities.NormalizeString(ld.SiteStreet2__c, alphaconver );
            }
            //001
            
            if(UserObj.Business_Unit__c == 'Devcon'){
                ld.SG_Channel__c = UserObj.Business_Unit__c; 
            }
            if (ld.LeadSource == 'Manual Import' || ld.AssignOwner__c){
                ld.RecordTypeId = String.isNotBlank(ld.Business_Id__c) && ld.Business_Id__c.contains('1300')? commercialRecordType.Id: systemGenLeadRecType.Id;
                ld.DateAssigned__c = System.now();
                ld.NewLead__c = true;
                
                if(ld.SiteCity__c != null && ld.SiteCity__c.length() > 24){
                    ld.SiteCity__c = ld.SiteCity__c.substring(0,24);
                }   
                // HRM-10369 National & Commercial lead owner assignment
                Manual_Leads.add(ld);
            }else if(ld.LeadSource  == 'BUDCO'){
                // Added by Sid to throw an error on the BUDCO lead if NewMoverType__c is missing/ not in our valid list
                if(String.isBlank(ld.NewMoverType__c)){
                    ld.AddError('New Mover Type for record with ExternalId: '+ld.ExternalID__c+ ' is missing');
                    continue;
                }else if(ResaleGlobalVariables__c.getInstance('ValidNewMoverType') != null && !ResaleGlobalVariables__c.getInstance('ValidNewMoverType').value__c.contains(ld.NewMoverType__c)){
                    ld.AddError('Unsupported New Mover Type for record with ExternalId: '+ld.ExternalID__c);
                    continue;
                }
                
                ld.RecordTypeId = String.isNotBlank(ld.Business_Id__c) && ld.Business_Id__c.contains('1300')? commercialRecordType.Id: systemGenLeadRecType.Id;
                
                //Jaydip B added below if part as part of Sprint#12
                //Leo story#686. added 'CI' and 'CA'
                if (ld.NewMoverType__c=='PN'||ld.NewMoverType__c=='PI'||ld.NewMoverType__c=='PA' || ld.NewMoverType__c=='CI' ||ld.NewMoverType__c=='CA') {
                    ld.Type__c='Pre Mover';
                }else if (ld.NewMoverType__c == 'AP'){ //LA Jan-2014 Release
                    ld.Type__c = 'ANI';
                }else{
                    ld.Type__c = 'New Mover';
                }
                
                ld.NewLead__c = true;
                system.debug('Lead is: '+ld);
                BUDCO_Leads.add(ld);
            }else if(ld.LeadSource  == 'Salesgenie.com'){
                System.debug('$$$ SalesGenie.com');
                ld.RecordTypeId = String.isNotBlank(ld.Business_Id__c) && ld.Business_Id__c.contains('1300')? commercialRecordType.Id: systemGenLeadRecType.Id;
                ld.DateAssigned__c = System.now();
                ld.NewLead__c = true;
                
                if(ld.SiteCity__c != null && ld.SiteCity__c.length() > 24){
                    ld.SiteCity__c = ld.SiteCity__c.substring(0,24);
                }
                
                if (UserObj.Business_Unit__c == Channels.TYPE_HOMEHEALTH){
                    ld.Channel__c = Channels.TYPE_HOMEHEALTH;
                    ld.Type__c = 'Other';
                }else if(ld.SGCOM3__SG_InfoGroupDatabase__c == 'Business' || ld.SGCOM3__SG_InfoGroupDatabase__c == 'NewBusiness' || ld.SGCOM3__SG_InfoGroupDatabase__c == 'CanadaBusiness' || ld.SGCOM3__SG_InfoGroupDatabase__c == 'CanadaNewBusine'){
                    ld.Channel__c = Channels.TYPE_SBDIRECT;
                    ld.Type__c = 'Other';
                }else if(ld.SGCOM3__SG_InfoGroupDatabase__c == 'Consumer'){
                    ld.Channel__c = Channels.TYPE_RESIDIRECT;
                    ld.Type__c = 'Other';
                }else if(ld.SGCOM3__SG_InfoGroupDatabase__c == 'NewMover'){
                    ld.Channel__c = Channels.TYPE_RESIDIRECT;
                    ld.Type__c = 'New Mover';
                }               
                if(ld.SGCOM3__SG_infoGroupId__c != null){
                    Salesgenie_LeadsMap.put(ld.SGCOM3__SG_infoGroupId__c,ld);   
                }               
            }else if (ld.LeadSource == 'TelemarRehash' || ld.LeadSource == 'TelemarRELO'){
                if (ld.LeadSource == 'TelemarRehash'){
                    ld.LeadSource = IntegrationConstants.DATA_SOURCE_TELEMAR_REHASH;
                }
                if (ld.LeadSource == 'TelemarRELO'){
                    ld.LeadSource = IntegrationConstants.DATA_SOURCE_TELEMAR_RELO;
                }
                ld.RecordTypeId = String.isNotBlank(ld.Business_Id__c) && ld.Business_Id__c.contains('1300')? commercialRecordType.Id: systemGenLeadRecType.Id;
                ld.DateAssigned__c = System.now();
                ld.NewLead__c = true;
                
                if(ld.SiteCity__c != null && ld.SiteCity__c.length() > 24){
                    ld.SiteCity__c = ld.SiteCity__c.substring(0,24);
                }
                
                // derive disposition values from supplied CI Notes
                if (ld.IncomingCINotes__c != null && ld.IncomingCINotes__c.length() > 0){
                    disposition__c d = LeadTriggerUtilities.getMostRecentDisposition(ld.Id, ld.IncomingCINotes__c);
                    
                    ld.DispositionCode__c = d.DispositionType__c;
                    ld.DispositionDetail__c = d.DispositionDetail__c;
                    ld.DispositionComments__c = d.Comments__c;
                    if(ld.DispositionCode__c != 'Mailed (Resi)' && ld.DispositionCode__c !='Mailed Letter (SB)'){
                        ld.DispositionDate__c = d.DispositionDate__c;
                       //Jaydip B added below statement for sprint#11
                        ld.Other_Disposition_Date__c=d.DispositionDate__c;
                    }else {
                        //Jaydip B added else condition for sprint#11
                        ld.DispositionDate__c = d.DispositionDate__c;
                    }
                }
                
                // If SourceCriteriaDescription__c contains 'DKL' then this is a Do Not Call
                if (ld.SourceCriteriaDescription__c != null && ld.SourceCriteriaDescription__c.contains('DKL')){
                    ld.DoNotCall = true;
                    ld.Phone = IntegrationConstants.DNC;
                    ld.PhoneNumber2__c = IntegrationConstants.DNC;
                    ld.PhoneNumber3__c = IntegrationConstants.DNC;
                    ld.PhoneNumber4__c = IntegrationConstants.DNC;
                }
                    
                TR_Leads.add(ld);
            }else if (ld.LeadSource == 'Amplifinity'){
                // LA - Integration support for AMP
                ld.Type__c = 'AMP';
            }else if (ld.LeadSource == 'Relo Certification'){
                // 6682 changes
            }else{
                ld.LeadSource  = 'User Entered';
            }
            
            if(ld.SitePostalCode__c != null  && ld.SitePostalCode__c != ''){
                LeadTriggerUtilities.processPostalCode(ld);
            }           
            
            if(ld.AddressID__c == NULL && (ld.SiteStreet__c != null || ld.SiteCity__c != null || ld.SiteStateProvince__c != null || ld.SitePostalCode__c != null || ld.SiteCountryCode__c != null)){   
                Address__c newAddress = LeadTriggerUtilities.setSiteAddress(ld);
                if(newAddress.Street__c != 'FAILED'){
                    validNewAddresses.put(newAddress.OriginalAddress__c, newAddress);                       
                }                   
            }           
            if(ld.Company == '[not provided]'){
                ld.Company = 'N/A';
            }
            
            if(String.isBlank(ld.Business_Id__c) || (String.isNotBlank(ld.Business_Id__c) && !ld.Business_Id__c.contains('1300'))){
                // Commercial accounts will have the biz Id & channel during the import
                Channels.setChannel(ld);
                Channels.setBusinessId(ld);
            }
            
            LeadTriggerUtilities.setCompanyAndLastName(ld);

            // Added for HOA             
            if(HOAHelper.isHOARecord(ld) && UserInfo.getUserId() == dataConversionUser){
                channels.setHOADefaults(ld);
                ld.OwnerId = hoaOwnerID;
                //HOAHelper.setHOAOwnership(ld);
            }
        }
        
        if(!validNewAddresses.isEmpty()){
            validNewAddresses = AddressHelper.upsertAddresses(validNewAddresses);       
        }       
        if(NewLeads.size() > 0){
            LeadTriggerUtilities.setPostalCodeLookup(NewLeads);     
        }       
        if(NewLeads.size() > 0 && !validNewAddresses.isEmpty()){
            LeadTriggerUtilities.setSiteAddressIds(validNewAddresses, NewLeads);        
        }       
        if(BUDCO_Leads.size() > 0){
            ChangeLeadOwnerController.ChangeOwnershipWithListOfLeads(BUDCO_Leads,false);          
        }
        system.debug('Manual_Leads.size: '+ Manual_Leads.size()); 
        if(Manual_Leads.size() > 0){
            ChangeLeadOwnerController.ChangeOwnershipWithListOfLeads(Manual_Leads,false);
        } 
        if(TR_Leads.size() > 0){
            ChangeLeadOwnerController.ChangeOwnershipWithListOfLeads(TR_Leads,false);
        }
        if(Salesgenie_LeadsMap.size() > 0){
            LeadTriggerUtilities.SalesgenieLeadValidation(Salesgenie_LeadsMap);         
        }             
    }
    
    public static void ProcessLeadsAfterInsert(List < Lead > NewLeads){
        list<Lead> HOA_Leads = new list<Lead>();
        list<Lead> possibleNowGryphonLeadsList = new list<Lead>();//HRM - 10437 
        list<Lead> possibleNowGryphonLeadListForStatus = new list<Lead>();//HRM - 10437
        List < Geocoding__c > newGeocodes = new List < Geocoding__c > ();
        List < disposition__c > dispositions = new List < disposition__c > ();
        for (Lead ld: NewLeads){
            if (ld.LeadSource == IntegrationConstants.DATA_SOURCE_TELEMAR_REHASH){
                dispositions.addAll(LeadTriggerUtilities.createDispositions(ld.Id, ld.IncomingCINotes__c));
            }
            if (ld.Latitude__c == 0 && ld.Longitude__c == 0 && ld.AddressID__c != null){
                Geocoding__c geocode = new Geocoding__c();
                geocode.AddressToBeGeocoded__c = ld.AddressID__c;
                newGeocodes.add(geocode);
            }
            
            // Added for HOA                     
            if( HOAHelper.isHOARecord(ld) && ( HOAHelper.isHOAMaster(ld) ||  HOAHelper.isHOACustomerSite(ld) ) ){
                HOA_Leads.add(ld);
            }
             //HRM - 10437 
            if(!isPossibleNowRun  && (String.isNotBlank(ld.AccountStatus__c) && ResaleGlobalVariables__c.getinstance('PossibleNowGryphonAccountStatus') != null && String.isNotBlank(ResaleGlobalVariables__c.getinstance('PossibleNowGryphonAccountStatus').value__c) && ResaleGlobalVariables__c.getinstance('PossibleNowGryphonAccountStatus').value__c.containsIgnoreCase(ld.AccountStatus__c))){
               possibleNowGryphonLeadListForStatus.add(ld);
            }
        }
        try{
            if(!isPossibleNowRun && possibleNowGryphonLeadListForStatus.size() > 0){
                AdtCustContactEBRService.createRequestQueues(null,possibleNowGryphonLeadListForStatus);
                isPossibleNowRun = true;
            }
            if(!isPossibleNowRun){
                //Get all values for matching dispositions on custom meta data.
                possibleNowGryphonLeadsList = LeadTriggerUtilities.getAllPossibleNowLeads(NewLeads);//HRM - 10437 
                if(possibleNowGryphonLeadsList.size() > 0){
                    // Create Request Queue based on matching values.
                    AdtCustContactEBRService.createRequestQueues(null,possibleNowGryphonLeadsList);
                    isPossibleNowRun = true;
                }
            }
        }catch(exception ex){}
        if (!newGeocodes.isEmpty()){
            insert newGeocodes;
        }

        insert dispositions;

        if(HOA_Leads.size()>0){
            HOAHelper.ProcessHOALeadConversion(HOA_Leads);
        }
    }
    
    public static void ProcessLeadsBeforeUpdate(Map<Id, Lead> NewLeadsMap, Map<Id, Lead> OldLeadsMap){    
        Map<String, Address__c> validNewAddresses = new Map<String, Address__c>();
        Map<String, Lead> allNeedsAssignmentLeads = new map<String, Lead>();
        list<Lead> AddressModifiedLeads = new list<Lead>();
        set<String> OwnershipChangedLeadIds = new set<String>();
        
        User current = [select Id, Address_Validation__c from User where Id=:UserInfo.getUserId()];
        
        //Added by TCS for HRM 475
        Map<String, String> alphaconver = new Map<String, String>();
        for(AlphaConversion__c ac: [SELECT id, Name, AlphaCharValue__c FROM AlphaConversion__c]){
            alphaconver.put(ac.name, ac.AlphaCharValue__c);
        }
        
        for(Lead ld : NewLeadsMap.values()){
            // Added by TCS for 475
            if(System.Label.SwitchForTranslation == 'True'){
                if (String.isNotBlank(ld.FirstName))
                    ld.FirstName = Utilities.NormalizeString(ld.FirstName, alphaconver );
                if (String.isNotBlank(ld.LastName))
                    ld.LastName = Utilities.NormalizeString(ld.LastName, alphaconver );
                if (String.isNotBlank(ld.SiteCity__c))
                    ld.SiteCity__c= Utilities.NormalizeString(ld.SiteCity__c, alphaconver);
                if (String.isNotBlank(ld.SiteStreet__c))
                    ld.SiteStreet__c = Utilities.NormalizeString(ld.SiteStreet__c, alphaconver );
            }
        
            if(ld.LeadSource  == 'BUDCO' && ld.Type__c != 'New Mover' && ld.Type__c !='Pre Mover' && ld.NewMoverType__c != 'AP'){
                ld.Type__c = 'New Mover';
            }
            
            if(ld.SitePostalCode__c != null  && ld.SitePostalCode__c != '' && !ld.SitePostalCode__c.equals(OldLeadsMap.get(ld.Id).SitePostalCode__c)){
                LeadTriggerUtilities.processPostalCode(ld);
            }
            if ((ld.DispositionCode__c != OldLeadsMap.get(ld.Id).DispositionCode__c || ld.DispositionDetail__c != OldLeadsMap.get(ld.Id).DispositionDetail__c)){
                if (ld.DispositionCode__c != 'Mailed (Resi)' && ld.DispositionCode__c != 'Mailed Letter (SB)'){
                    //Jaydip B added below if condition
                    ld.Other_Disposition_Date__c=DateTime.now();
                }
                ld.DispositionDate__c = DateTime.now();
            } 
                      
            if(ld.SiteStreet__c != OldLeadsMap.get(ld.Id).SiteStreet__c ||
            ld.SiteCity__c != OldLeadsMap.get(ld.Id).SiteCity__c ||
            ld.SiteStateProvince__c != OldLeadsMap.get(ld.Id).SiteStateProvince__c ||
            ld.SitePostalCode__c != OldLeadsMap.get(ld.Id).SitePostalCode__c ||
            ld.SiteCounty__c != OldLeadsMap.get(ld.Id).SiteCounty__c ||
            ld.SiteCountryCode__c != OldLeadsMap.get(ld.Id).SiteCountryCode__c ||
            ld.SitePostalCodeAddOn__c != OldLeadsMap.get(ld.Id).SitePostalCodeAddOn__c || 
            ld.Business_Id__c != OldLeadsMap.get(ld.Id).Business_Id__c
            && (ld.SiteStreet__c != null || ld.SiteCity__c != null || ld.SiteStateProvince__c != null || ld.SitePostalCode__c != null || ld.SiteCountryCode__c != null))
            {   
                Address__c newAddress = LeadTriggerUtilities.setSiteAddress(ld);
                if(newAddress.Street__c != 'FAILED'){                   
                    validNewAddresses.put(newAddress.OriginalAddress__c, newAddress);        
                    AddressModifiedLeads.add(ld);               
                } 
            }    
            
            if(ld.NewMoverType__c!= OldLeadsMap.get(ld.Id).NewMoverType__c){
                Channels.setChannel(ld);
            }
            
            if(ld.Channel__c != OldLeadsMap.get(ld.Id).Channel__c){               
                Channels.setBusinessId(ld);             
            }
                        
            if(ld.LastName != OldLeadsMap.get(ld.Id).LastName || ld.Company != OldLeadsMap.get(ld.Id).Company){
                LeadTriggerUtilities.setCompanyAndLastName(ld); 
            }                  
            
            if(ld.OwnerId != OldLeadsMap.get(ld.Id).OwnerId){
                OwnershipChangedLeadIds.add(ld.Id);
            }

            // HRM-10846 Assign Owner functionality
            if(ld.AssignOwner__c && ld.AssignOwner__c != OldLeadsMap.get(ld.Id).AssignOwner__c){
                allNeedsAssignmentLeads.put(ld.Id, ld);
            }

            if(ld.DispositionCode__c != OldLeadsMap.get(ld.Id).DispositionCode__c || ld.DispositionDetail__c != OldLeadsMap.get(ld.Id).DispositionDetail__c){
                ld.NewLead__c = false;
            }  
            
            // if DNC lead and any phone number field has been changed, then set off DNC value
            if(ld.DoNotCall){
                //ld.DoNotCall = false;
                String DNC = IntegrationConstants.DNC;
                if((ld.Phone != OldLeadsMap.get(ld.Id).Phone && OldLeadsMap.get(ld.Id).Phone == DNC) ||
                (ld.PhoneNumber2__c != OldLeadsMap.get(ld.Id).PhoneNumber2__c && OldLeadsMap.get(ld.Id).PhoneNumber2__c == DNC) ||
                (ld.PhoneNumber3__c != OldLeadsMap.get(ld.Id).PhoneNumber3__c && OldLeadsMap.get(ld.Id).PhoneNumber3__c == DNC) ||
                (ld.PhoneNumber4__c != OldLeadsMap.get(ld.Id).PhoneNumber4__c && OldLeadsMap.get(ld.Id).PhoneNumber4__c == DNC)){
                    ld.DoNotCall = false;
                    if(ld.Phone == DNC){ ld.Phone = null; }
                    if(ld.PhoneNumber2__c.contains(DNC)){ ld.PhoneNumber2__c = null; }
                    if(ld.PhoneNumber3__c == DNC){ ld.PhoneNumber3__c = null; }
                    if(ld.PhoneNumber4__c == DNC){ ld.PhoneNumber4__c = null; }
                }
                
            }
            //Change lead record type if business id is 1300
            ld.RecordTypeId = String.isNotBlank(ld.Business_Id__c) && ld.Business_Id__c.contains('1300')? commercialRecordType.Id : UserEnteredLeadRecType.Id;
        }       
        
        if(!validNewAddresses.isEmpty()){
            validNewAddresses = AddressHelper.upsertAddresses(validNewAddresses);       
        }   
        if(!AddressModifiedLeads.isEmpty()){
            LeadTriggerUtilities.setPostalCodeLookup(AddressModifiedLeads);                 
        }
        if(!AddressModifiedLeads.isEmpty() && !validNewAddresses.isEmpty()){
            LeadTriggerUtilities.setSiteAddressIds(validNewAddresses, AddressModifiedLeads);                
        }   
        if(!OwnershipChangedLeadIds.isEmpty()){
            StreetSheetManager.deleteItems(OwnershipChangedLeadIds);
        }
        if(!allNeedsAssignmentLeads.isEmpty()){
            ChangeLeadOwnerController.ChangeOwnershipWithListOfLeads(allNeedsAssignmentLeads.values(), false);
        }
    } 
    
       
    public static void ProcessLeadsAfterUpdate(Map < Id, Lead > NewLeadsMap, Map < Id, Lead > OldLeadsMap){
        list < Lead > LeadsWithDispositionModified = new list < Lead > ();
        list < Lead > LeadsForOwnerChangeNotification = new list < Lead > ();
        list<Lead> possibleNowGryphonLeadsList = new list<Lead>();//HRM - 10437 
        list<Lead> possibleNowGryphonLeadListForStatus = new list<Lead>();//HRM - 10437 

        set < Id > ownedLeads = new set < Id > ();
        map<String,GryphonDispositon__mdt> gryphonMap = new map<String,GryphonDispositon__mdt>();    
        for(GryphonDispositon__mdt grDispostion : [SELECT MasterLabel,AccountDispositionSB__c,AccountDispositionRESI__c FROM GryphonDispositon__mdt WHERE ActiveType__c= true]){         
            gryphonMap.put(grDispostion.MasterLabel,grDispostion);
        }
        for (Lead ld: NewLeadsMap.values()){
            if (ld.DispositionCode__c != OldLeadsMap.get(ld.Id).DispositionCode__c || ld.DispositionDetail__c != OldLeadsMap.get(ld.Id).DispositionDetail__c || ld.Repeat_Disposition__c){
                String dspKey = ld.DispositionCode__c +'#'+ ld.DispositionDetail__c + '#' + ld.Id;
                if( ld.Repeat_Disposition__c != null && (ld.Repeat_Disposition__c || !LeadDispositionContext.contains(dspKey)) && !gryphonMap.containsKey(ld.DispositionCode__c)){
                    LeadsWithDispositionModified.add(ld);
                }               
                LeadDispositionContext.add( dspKey );
            }
            if (UserInfo.getUserId() == ld.OwnerId){
                ownedLeads.add(ld.ID);
            }
            //HRM - 10437 
            if(!isPossibleNowRun  && (ld.DispositionCode__c != OldLeadsMap.get(ld.Id).DispositionCode__c || ld.DispositionDetail__c != OldLeadsMap.get(ld.Id).DispositionDetail__c)){                
                possibleNowGryphonLeadsList.add(ld);
            }
            //HRM - 10437 
            if(!isPossibleNowRun  && (ld.AccountStatus__c != oldLeadsMap.get(ld.id).AccountStatus__c && String.isNotBlank(ld.AccountStatus__c) && ResaleGlobalVariables__c.getinstance('PossibleNowGryphonAccountStatus') != null && String.isNotBlank(ResaleGlobalVariables__c.getinstance('PossibleNowGryphonAccountStatus').value__c) && ResaleGlobalVariables__c.getinstance('PossibleNowGryphonAccountStatus').value__c.containsIgnoreCase(ld.AccountStatus__c))){
               possibleNowGryphonLeadListForStatus.add(ld);
            }
            //Nitin added HRM-10370
            // Check and send email notification commented out since we need to handle queues
            String ownerId = ld.OwnerId;
            if(ld.OwnerId != OldLeadsMap.get(ld.Id).OwnerId){
                LeadsForOwnerChangeNotification.add(ld);
            }
        }

        if (!LeadsWithDispositionModified.isEmpty()){
            system.debug(' ** TRIGGER::AFTER_UPDATE ... running' );
            LeadTriggerUtilities.createDispositionOnLead(LeadsWithDispositionModified);
        }
        
        try{
            //HRM - 10437 
            if(!isPossibleNowRun && possibleNowGryphonLeadsList.size() > 0){
                List<Lead> leadListToProcessForRequestQueue = new List<Lead>();//HRM - 10437 
                leadListToProcessForRequestQueue = LeadTriggerUtilities.getAllPossibleNowLeads(possibleNowGryphonLeadsList);
                if(leadListToProcessForRequestQueue.size() > 0){
                     //create request Queue for ebr preocessing.
                     AdtCustContactEBRService.createRequestQueues(null,leadListToProcessForRequestQueue);
                     isPossibleNowRun = true; // variable set to true to prevent re run of the logic.
                }               
            }
            if(!isPossibleNowRun && possibleNowGryphonLeadListForStatus.size() > 0){
                AdtCustContactEBRService.createRequestQueues(null,possibleNowGryphonLeadListForStatus);
                isPossibleNowRun = true; // variable set to true to prevent re run of the logic.
            }
        }catch(exception ex){
            System.debug('Unable to create request queue for possible now gryphon.'+ex);
        }
        //Nitin added HRM-10370
        if(!emailNotificationFlag){  // Siju Added the flag emailNotificationFlag to avoid sending duplicate owner assignment emails
            emailNotificationFlag = true;
            if(LeadsForOwnerChangeNotification.size() > 0){
                // Send Email Notification for Lead Assignment
                LeadTriggerUtilities.sendLeadAssignmentEmail(LeadsForOwnerChangeNotification);
            }
        }
        LeadTriggerUtilities.updateAllProspectListTouched(ownedLeads);
    }
}