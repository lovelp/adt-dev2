@isTest
private class ManagerTownTest {	
	
	static testMethod void testStep1() {
		Test.startTest();
		User salesRep = TestHelperClass.createSalesRepUserWithManager();
		UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
		User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId Limit 1];
		User adminUser = TestHelperClass.createAdminUser();
		
		ApexPages.PageReference ref = new PageReference('/apex/ManagerTown');
	    Test.setCurrentPageReference(ref);
	    
	    System.runAs(adminUser) { 
			Postal_Codes__c newPC1 = new Postal_Codes__c ( name = '04547', businessId__c = '1100', TownId__c = '12345', Town__c = 'Unit Test Town 1', AreaId__c = '123', Area__c = 'Unit Test Area');
			insert newPC1;	
			Postal_Codes__c newPC2 = new Postal_Codes__c ( name = '04572', businessId__c = '1200', TownId__c = '54321', Town__c = 'Unit Test Town 2', AreaId__c = '123', Area__c = 'Unit Test Area');
			insert newPC2;
			List<Postal_Codes__c> pcs = TestHelperClass.createPostalCodes();
			ManagerTown__c existingMT = TestHelperClass.createManagerTown(ManagerUser, newPC1, Channels.TYPE_RESIDIRECT + ';' + Channels.TYPE_RESIRESALE);
			
			ManagerTown mt = new ManagerTown();
			
			//Step 1 is to select a manager. Click Next without selecting a manager and make sure an error message is thrown
			mt.Next();
			system.assertEquals(ApexPages.getMessages()[0].getSummary(), ErrorMessages__c.getInstance('MT_SM').Message__C );
			
			//Now select a manager and try clicking the next button again. In this case, isStep2 will be set to true
			mt.selectedManagerId = ManagerUser.id;
			mt.Next();
			system.assertEquals(mt.isStep2, true);
			
			//step 1 is successful.
			//In step 2, a new manager town can be added, existing manager town can be deleted or edited.
			
			//Add a new manager town
			//First step for adding new MT is to select an area and ckick find towns button. 
			
			//if no town is selected, an error is thowrn - testing this scenario first
			List<SelectOption> allareas = mt.getAreaItems();
			system.debug(allareas);
			mt.retrieveTowns();
			system.assert(mt.availableTowns==null);
			system.assert(mt.refreshedForAddingTowns==null);
			//system.assertEquals(ApexPages.getMessages()[1].getSummary(), ErrorMessages__c.getInstance('MT_SA').Message__C );
			// now select a town and click get towns
			mt.selectedArea = allareas[1].getValue(); //index 0 is always blank so select 1
			mt.retrieveTowns();
			//system.assert(mt.availableTowns.size()>0);
			//system.assertEquals(mt.refreshedForAddingTowns, true);
			
			//now that the area is selected and towns are returned
			//1 - click addSelectedTowns without selecting a town
			mt.addSelectedTowns(); //throws a message and does nothing
			
			//2 - click addSelectedTowns by selecting an exiting town
			mt.setselectedTownsToAddToManager(new String[]{newPC1.TownId__c+'-'+newPC1.BusinessId__c});
			mt.addSelectedTowns(); //throws an error and does nothing
			
			//3 - click addSelectedTowns by selecting a town that doesnt belong to manager
			mt.setselectedTownsToAddToManager(new String[]{newPC2.TownId__c+'-'+newPC2.BusinessId__c});
			mt.addSelectedTowns(); 
			mt.getselectedTownsToAddToManager();
			//system.assert(mt.mts.size() == 2);
						
			//now delete the existing town and add the same town back
			mt.removedMT = newPC1.TownId__c+';'+newPC1.BusinessId__c;
			mt.deleteTown();
			//system.assert(mt.mts.size() == 1);
			mt.setselectedTownsToAddToManager(new String[]{newPC1.TownId__c+'-'+newPC1.BusinessId__c});
			mt.addSelectedTowns();
			//system.assert(mt.mts.size() == 2);
			
			//now make a few changes to the type selections and save the results
			mt.mts[1].isResiDirect = false;
			mt.mts[0].isSBResale = false;
			mt.Save();
			
			List<ManagerTownRealignmentQueue__c> mtqs = [select id from ManagerTownRealignmentQueue__c where NewOwnerId__c =: ManagerUser.id];
			//initially manager had 12345 with resi direct and resi resale.
			//after the above process, manager should have all resi types but resi direct for 12345. total = 1 (because resi resale already existed)
			//manager should also have all resi types for 54321 but resi resale. total = 1
			//grand total = 2 records in ManagerTownRealignmentQueue__c
			system.debug('*****************************');
			system.debug(mtqs.size());
			//system.assert(mtqs.size() == 2);
			
			//click Start over just to cover it
			mt.StartOver();
	    }
	    Test.stopTest();
	}
	 
	 //Inactve Manager test
	
		static testMethod void testStep2() {
		Test.startTest();
		User salesRep = TestHelperClass.createSalesRepUserWithManager();
		UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
		User ManagerUser = [Select Id, Name,isactive from User where UserRoleId = : salesRepRole.ParentRoleId Limit 1];
		ManagerUser.isactive =false;
		update ManagerUser;
		
		User adminUser = TestHelperClass.createAdminUser();
		
		ApexPages.PageReference ref = new PageReference('/apex/ManagerTown');
	    Test.setCurrentPageReference(ref);
	    
	    System.runAs(adminUser) { 
			Postal_Codes__c newPC1 = new Postal_Codes__c ( name = '04547', businessId__c = '1100', TownId__c = '12345', Town__c = 'Unit Test Town 1', AreaId__c = '123', Area__c = 'Unit Test Area');
			insert newPC1;	
			Postal_Codes__c newPC2 = new Postal_Codes__c ( name = '04572', businessId__c = '1200', TownId__c = '54321', Town__c = 'Unit Test Town 2', AreaId__c = '123', Area__c = 'Unit Test Area');
			insert newPC2;
			List<Postal_Codes__c> pcs = TestHelperClass.createPostalCodes();
			ManagerTown__c existingMT = TestHelperClass.createManagerTown(ManagerUser, newPC1, Channels.TYPE_RESIDIRECT + ';' + Channels.TYPE_RESIRESALE);
			
			ManagerTown mt = new ManagerTown();
			
			//Step 1 is to select a manager. Click Next without selecting a manager and make sure an error message is thrown
			mt.Next();
			system.assertEquals(ApexPages.getMessages()[0].getSummary(), ErrorMessages__c.getInstance('MT_SM').Message__C );
			
			//Now select a manager and try clicking the next button again. In this case, isStep2 will be set to true
			mt.selectedManagerId = ManagerUser.id;
			mt.Next();
			system.assertEquals(mt.isStep2, true);
			
			//step 1 is successful.
			//In step 2, a new manager town can be added, existing manager town can be deleted or edited.
			
			//Add a new manager town
			//First step for adding new MT is to select an area and ckick find towns button. 
			
			//if no town is selected, an error is thowrn - testing this scenario first
			List<SelectOption> allareas = mt.getAreaItems();
			system.debug(allareas);
			mt.retrieveTowns();
			system.assert(mt.availableTowns==null);
			system.assert(mt.refreshedForAddingTowns==null);
			//system.assertEquals(ApexPages.getMessages()[1].getSummary(), ErrorMessages__c.getInstance('MT_SA').Message__C );
			// now select a town and click get towns
			mt.selectedArea = allareas[1].getValue(); //index 0 is always blank so select 1
			mt.retrieveTowns();
			//system.assert(mt.availableTowns.size()>0);
			//system.assertEquals(mt.refreshedForAddingTowns, true);
			
			//now that the area is selected and towns are returned
			//1 - click addSelectedTowns without selecting a town
			mt.addSelectedTowns(); //throws a message and does nothing
			
			//2 - click addSelectedTowns by selecting an exiting town
			mt.setselectedTownsToAddToManager(new String[]{newPC1.TownId__c+'-'+newPC1.BusinessId__c});
			mt.addSelectedTowns(); //throws an error and does nothing
			
			//3 - click addSelectedTowns by selecting a town that doesnt belong to manager
			mt.setselectedTownsToAddToManager(new String[]{newPC2.TownId__c+'-'+newPC2.BusinessId__c});
			mt.addSelectedTowns(); 
			mt.getselectedTownsToAddToManager();
			//system.assert(mt.mts.size() == 2);
						
			//now delete the existing town and add the same town back
			mt.removedMT = newPC1.TownId__c+';'+newPC1.BusinessId__c;
			mt.deleteTown();
			//system.assert(mt.mts.size() == 1);
			mt.setselectedTownsToAddToManager(new String[]{newPC1.TownId__c+'-'+newPC1.BusinessId__c});
			mt.addSelectedTowns();
			//system.assert(mt.mts.size() == 2);
			
			//now make a few changes to the type selections and save the results
			mt.mts[1].isResiDirect = false;
			mt.mts[0].isSBResale = false;
			mt.Save();
			
			List<ManagerTownRealignmentQueue__c> mtqs = [select id from ManagerTownRealignmentQueue__c where NewOwnerId__c =: ManagerUser.id];
			//initially manager had 12345 with resi direct and resi resale.
			//after the above process, manager should have all resi types but resi direct for 12345. total = 1 (because resi resale already existed)
			//manager should also have all resi types for 54321 but resi resale. total = 1
			//grand total = 2 records in ManagerTownRealignmentQueue__c
			system.debug('*****************************');
			system.debug(mtqs.size());
			//system.assert(mtqs.size() == 2);
			
			//click Start over just to cover it
			mt.StartOver();
	    }
	    Test.stopTest();
	}
}