/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class StreetSheetSelectorTest {

    static testMethod void testAddAccounts() {
        list<Account> accounts;
        Account a1, a2;
        Address__c addr;
        StreetSheet__c s;
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
        	a1 = TestHelperClass.createAccountData();
        	addr = [Select Id, PostalCode__c from Address__c where Id = :a1.AddressID__c];
        	a2 = TestHelperClass.createAccountData(addr, a1.PostalCodeID__c, true);
        	accounts = new list<Account>{a1,a2};
        	u = TestHelperClass.createSalesRepUser();
        	a1.OwnerId = u.Id;
        	a2.OwnerId = u.Id;
        	update accounts;
        	s = new StreetSheet__c();
        	s.OwnerId = u.Id;
        	insert s;
        }
        
        String acctprefix = Schema.Sobjecttype.Account.getKeyPrefix();
        
        system.runas(u) {
	        ApexPages.Standardsetcontroller scset = new ApexPages.Standardsetcontroller(accounts);
	        scset.setSelected(accounts);
	        test.startTest();
	        	StreetSheetSelectorExtension sse = new StreetSheetSelectorExtension(scset);
	        	PageReference validateref = sse.validate();
	        	system.assertEquals(null, validateref, 'StreetSheetSelectorExtension.validate failed.');
	        	
	        	StreetSheetSelectorController ssc = new StreetSheetSelectorController();
	        	ssc.itemIds = new list<String>{a1.Id,a2.Id};
	        	PageReference cancelref = ssc.cancel();
	        	system.assertEquals('/' + acctprefix, cancelref.getURL());
				ssc.ssid = s.Id;
				PageReference addref = ssc.addToStreetSheet();
				system.assertEquals('/' + s.Id, addref.getURL());
	        test.stopTest();
        }
        
        StreetSheet__c ss = [Select Id, (Select Id from Street_Sheet_Items__r) from StreetSheet__c where Id = :s.Id];
        system.assertEquals(accounts.size(), ss.Street_Sheet_Items__r.size());
    }
    
    static testMethod void testAddAccountsNoSelection() {
       
        List<Account> accounts = new List<Account>();
        
		ApexPages.Standardsetcontroller scset = new ApexPages.Standardsetcontroller(accounts);
	    Test.startTest();
	   	StreetSheetSelectorExtension sse = new StreetSheetSelectorExtension(scset);
	  	PageReference validateref = sse.validate();
	    System.assert(validateref.getURL().contains('/apex/ShowError?Error=error.streetsheet.noitemsselected'), 'Expect a validation error');
	       	
	  	Test.stopTest();
        
    }
    
    static testMethod void testAddAccountsNoProspectList() {
       
        List<Account> accounts = new List<Account>();
      
        Account a1, a2;
        Address__c addr;
        StreetSheet__c s;
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        System.runas(current) {
        	a1 = TestHelperClass.createAccountData();
        	addr = [Select Id, PostalCode__c from Address__c where Id = :a1.AddressID__c];
        	a2 = TestHelperClass.createAccountData(addr, a1.PostalCodeID__c, true);
        	accounts = new list<Account>{a1,a2};
        	u = TestHelperClass.createSalesRepUser();
        	a1.OwnerId = u.Id;
        	a2.OwnerId = u.Id;
        	update accounts;
        	
        }
        
       System.runas(u) {
			ApexPages.Standardsetcontroller scset = new ApexPages.Standardsetcontroller(accounts);
	        scset.setSelected(accounts);
	    	Test.startTest();
	   		StreetSheetSelectorExtension sse = new StreetSheetSelectorExtension(scset);
	  		PageReference validateref = sse.validate();
	    	System.assert(validateref.getURL().contains('/apex/ShowError?Error=error.streetsheet.noneowned'), 'Expect a validation error');
	       
	  		Test.stopTest();
       } 
    }
    
    static testMethod void testAddAccount() {

        Account a1;
        Address__c addr;
        StreetSheet__c s;
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
        	a1 = TestHelperClass.createAccountData();
        	addr = [Select Id, PostalCode__c from Address__c where Id = :a1.AddressID__c];    	
        	u = TestHelperClass.createSalesRepUser();
        	a1.OwnerId = u.Id;
        	update a1;
        	s = new StreetSheet__c();
        	s.OwnerId = u.Id;
        	insert s;
        }
        
        ApexPages.PageReference ref = new PageReference('/apex/StreetSheetAddAccount?iid=' + a1.Id);
	    Test.setCurrentPageReference(ref);
	    
		ApexPages.StandardController sc = new ApexPages.StandardController(new Account());
        system.runas(u) {
	        
	        test.startTest();
	        	StreetSheetSelectorExtension sse = new StreetSheetSelectorExtension(sc);
	        	PageReference validateref = sse.validate();
	        	system.assertEquals(null, validateref, 'StreetSheetSelectorExtension.validate failed.');
	        	
	        	StreetSheetSelectorController ssc = new StreetSheetSelectorController();
	        	ssc.itemIds = new list<String>{a1.Id};
	        	PageReference cancelref = ssc.cancel();
	        	system.assertEquals('/' + a1.Id, cancelref.getURL());
				ssc.ssid = s.Id;
				PageReference addref = ssc.addToStreetSheet();
				system.assertEquals('/' + s.Id, addref.getURL());
	        test.stopTest();
        }
        
        StreetSheet__c ss = [Select Id, (Select Id from Street_Sheet_Items__r) from StreetSheet__c where Id = :s.Id];
        system.assertEquals(1, ss.Street_Sheet_Items__r.size());
    }
    
    static testMethod void testAddLeads() {
    	
        list<Lead> leads;
        Lead l1, l2;
        StreetSheet__c s;
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
        	u = TestHelperClass.createSalesRepUser();
        	l1 = TestHelperClass.createLead(u);
        	l2 = TestHelperClass.createLead(u);
        	leads = new list<Lead>{l1,l2};
        	s = new StreetSheet__c();
        	s.OwnerId = u.Id;
        	insert s;
        }
        
        String leadprefix = Schema.Sobjecttype.Lead.getKeyPrefix();
        
        system.runas(u) {
	        ApexPages.Standardsetcontroller scset = new ApexPages.Standardsetcontroller(leads);
	        scset.setSelected(leads);
	        test.startTest();
	        	StreetSheetSelectorExtension sse = new StreetSheetSelectorExtension(scset);
	        	PageReference validateref = sse.validate();
	        	system.assertEquals(null, validateref, 'StreetSheetSelectorExtension.validate failed.');
	        	StreetSheetSelectorController ssc = new StreetSheetSelectorController();
	        	ssc.itemIds = new list<String>{l1.Id,l2.Id};
	        	PageReference cancelref = ssc.cancel();
	        	system.assertEquals('/' + leadprefix, cancelref.getURL());
				ssc.ssid = s.Id;
				PageReference addref = ssc.addToStreetSheet();
				system.assertEquals('/' + s.Id, addref.getURL());
	        test.stopTest();
        }
        
        StreetSheet__c ss = [Select Id, (Select Id from Street_Sheet_Items__r) from StreetSheet__c where Id = :s.Id];
        system.assertEquals(leads.size(), ss.Street_Sheet_Items__r.size());
    }
    
    static testMethod void testAddLead() {
    	
        Lead l1;
        StreetSheet__c s;
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {
        	u = TestHelperClass.createSalesRepUser();
        	l1 = TestHelperClass.createLead(u);

        	s = new StreetSheet__c();
        	s.OwnerId = u.Id;
        	insert s;
        }
        
		ApexPages.PageReference ref = new PageReference('/apex/StreetSheetAddLead?iid=' + l1.Id);
	    Test.setCurrentPageReference(ref);
	    
		ApexPages.StandardController sc = new ApexPages.StandardController(new Lead());
        
        system.runas(u) {
	        test.startTest();
	        	StreetSheetSelectorExtension sse = new StreetSheetSelectorExtension(sc);
	        	PageReference validateref = sse.validate();
	        	system.assertEquals(null, validateref, 'StreetSheetSelectorExtension.validate failed.');
	        	StreetSheetSelectorController ssc = new StreetSheetSelectorController();
	        	ssc.itemIds = new list<String>{l1.Id};
	        	PageReference cancelref = ssc.cancel();
	        	system.assertEquals('/' + l1.Id, cancelref.getURL());
				ssc.ssid = s.Id;
				PageReference addref = ssc.addToStreetSheet();
				system.assertEquals('/' + s.Id, addref.getURL());
	        test.stopTest();
        }
        
        StreetSheet__c ss = [Select Id, (Select Id from Street_Sheet_Items__r) from StreetSheet__c where Id = :s.Id];
        system.assertEquals(1, ss.Street_Sheet_Items__r.size());
    }
}