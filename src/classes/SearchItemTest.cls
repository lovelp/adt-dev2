@isTest
private class  SearchItemTest {
	
	private static void createRefData()
	{
		System.runAs(TestHelperClass.createAdminUser())
		{
			TestHelperClass.createReferenceDataForTestClasses();
		}
		TestHelperClass.createReferenceUserDataForTestClasses();
	}
	
	static testMethod void testConstructFrom1100Account() {
		
		createRefData();
		System.runAs(TestHelperClass.createUser(100))
		{
			Date aDate = Date.today();
			Account a1100 = TestHelperClass.createAccountData();
			a1100.DisconnectDate__c = aDate;
			a1100.Business_Id__c = '1100 - Residential';
			
			Test.startTest();
			
			SearchItem si = new SearchItem(a1100, true);
			// formatted name should match the result of formatting the input
			System.assertEquals(si.getformattedDiscoDate(), aDate.format(), 'Formatted name should match the result of formatting the input');
			// for business ID 1100, business name should be blank
			System.assertEquals(si.getBusinessName(), '', 'Business Name should be blank for accounts with business ID 1100');
			// ...customer name should be the account name
			System.assertEquals(si.getCustomerName(), TestHelperClass.ACCOUNT_NAME, 'Customer Name should be the account name');
			
			Test.stopTest();
		}
	}
	
	static testMethod void testConstructFrom1200Account() {
		createRefData();
		System.runAs(TestHelperClass.createUser(100))
		{		
			Date aDate = Date.today();
		
			Account a1200 = TestHelperClass.createAccountData();
			a1200.DisconnectDate__c = aDate;
			a1200.Business_Id__c = '1200 - Small Business';
			
			Test.startTest();
			
			SearchItem si = new SearchItem(a1200, true);
			// formatted name should match the result of formatted the input
			System.assertEquals(si.getformattedDiscoDate(), aDate.format(), 'Formatted name should match the result of formatting the input');
			// for business ID 1200, customer name should be blank
			System.assertEquals(si.getCustomerName(), '', 'Customer Name should be blank for accounts with business ID 1200');
			// and business name should be the account name
			System.assertEquals(si.getBusinessName(), TestHelperClass.ACCOUNT_NAME, 'Business Name should be the account name');
				
			
			Test.stopTest();
		}	
	}
	
	static testMethod void testConstructFrom1100StreetSheetItem() {
		createRefData();
		System.runAs(TestHelperClass.createUser(100))
		{	
			Date aDate = Date.today();
			String businessID = '1100 - Residential';
			
			Account a1100a = TestHelperClass.createAccountData(aDate, businessID);
			Account a1100b = TestHelperClass.createAccountData();
			
			StreetSheet__c ss = new StreetSheet__c();
			ss.Name = 'Unit Test Street Sheet';
			
			insert ss;
			
			StreetSheetItem__c ssi1100a = new StreetSheetItem__c();
			ssi1100a.AccountID__c = a1100a.Id;
			ssi1100a.StreetSheet__c = ss.Id;
			
			insert ssi1100a;
			
			StreetSheetItem__c ssi1100b = new StreetSheetItem__c();
			ssi1100b.AccountID__c = a1100b.Id;
			ssi1100b.StreetSheet__c = ss.Id;
			
			insert ssi1100b; 
		
			StreetSheetItem__c fullItemA = [select Name, Latitude__c, Longitude__c, Phone__c, Type__c, Data_Source__c, DisconnectReason__c, DisconnectDate__c, InService__c, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, Business_ID__c, AccountName__c from StreetSheetItem__c where Id = :ssi1100a.Id];
			StreetSheetItem__c fullItemB = [select Name, Latitude__c, Longitude__c, Phone__c, Type__c, Data_Source__c, DisconnectReason__c, DisconnectDate__c, InService__c, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, Business_ID__c, AccountName__c from StreetSheetItem__c where Id = :ssi1100b.Id];
			
			
			Test.startTest();
			
			SearchItem siA = new SearchItem(fullItemA);
	
			// formatted name should match the result of formatted the input
			System.assertEquals(aDate.format(), siA.getformattedDiscoDate(), 'Formatted disco date should match the formatted input' );
			// for business ID 1100, business name should be blank
			System.assertEquals('', siA.getBusinessName(), 'Business name should');	
			// ... customer name should be the account name
			System.assertEquals(TestHelperClass.ACCOUNT_NAME, siA.getCustomerName(), 'Customer name should match the account name');
			// ... and formatted Active Site should match the input
			System.assertEquals(fullItemA.InService__c, siA.getFormattedActiveSite(), 'Formatted Active Site should match the input: Yes');
			
			SearchItem siB = new SearchItem(fullItemB);
			System.assertEquals(fullItemA.InService__c, siA.getFormattedActiveSite(), 'Formatted Active Site should match the input: No');
			
			Test.stopTest();
		}
	}
	
	static testmethod void testLeadSearchItem()
	{
		createRefData();
		System.runAs(TestHelperClass.createUser(100))
		{		
			User current = [select Id from User where Id=:UserInfo.getUserId()];
			Lead l;
			system.runas(current) {
				User u = TestHelperClass.createSalesRepUser();
				l = TestHelperClass.createLead(u);
			}
			l.Business_Id__c = '1100 - Residential';
			
			
			test.startTest();
				SearchItem si = new SearchItem(l, false);
			test.stoptest();
			
			System.assertEquals(l.id, si.id); 
	    	System.assertEquals(l.SiteStreet__c, si.siteStreet); 
	    	System.assertEquals(l.SiteCity__c, si.siteCity); 
	    	System.assertEquals(l.SiteStateProvince__c, si.siteState); 
	    	System.assertEquals(l.SitePostalCode__c, si.sitePostalCode); 
	    	
	    	// for business ID 1100, business name should be blank
			System.assertEquals('', si.getBusinessName(), 'Business Name should be blank for leads with business ID 1100');
			// ...customer name should be the account name
			System.assert(si.getCustomerName() != null, 'Customer Name should not be null');
		}
		
	}

}