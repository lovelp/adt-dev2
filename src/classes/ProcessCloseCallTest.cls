//Test Class for ProcessCloseCall
@isTest
public with sharing class ProcessCloseCallTest {


  @isTest
    public static void testHotLead(){
        
        user u = new User();
        u.id = UserInfo.getUserId();
        u.MobilePhone = '95959595951';
        update u;
        Test.startTest();
        system.runAs(u){
            ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
            res.name = 'DataRecastDisableAccountTrigger';
            res.value__c = 'TRUE';
            insert res;
                
            ResaleGlobalVariables__c res1 = new ResaleGlobalVariables__c();
            res1.name = 'allowedOrderTypesHotLeads';
            res1.value__c = 'N1;R1;R2;N2';
            insert res1;
                
            Wireless_Carrier__c wirelesObj = new Wireless_Carrier__c();
            wirelesObj.Name = 'Verizon';
            wirelesObj.SMS_Email_Service__c = 'number@vtext.com';
            wirelesObj.SMS_Enabled__c = TRUE;
            insert wirelesObj;
        }
        
                
        Address__c addObj = ADTPartnerCustomerLookupTest.createAddr();
        Account acc = ADTPartnerCustomerLookupTest.CreateAccount(addObj);
        acc.Email__c = 'a@tcs.com';
        acc.OwnerId = u.id;
        acc.Channel__c = 'Home Health';
        acc.MMBOrderType__c = 'N1';
        update acc;
        
        TerritoryAssignment__c terriassgObj = new TerritoryAssignment__c();
        terriassgObj.PostalCodeID__c = acc.PostalCodeID__c;
        terriassgObj.TerritoryType__c ='Home Health';
        insert terriassgObj;
        
        ManagerTown__c managrTwnObj = new ManagerTown__c();
        managrTwnObj.ManagerID__c= u.Id;
        managrTwnObj.TownID__c = 'TestTNID';
        managrTwnObj.Type__c = '1;2;3';
        insert managrTwnObj ;
         //callDispDetail = [ SELECT ActionItems__c FROM Call_Disposition__c WHERE Name =:cDisp.DispositionDetail__c LIMIT 1]; 
        
        disposition__c dispo= new disposition__c(DispositionType__c='Mailed', DispositionDetail__c='PP - One-Legger - Follow-up Appt Scheduled', DispositionDate__c=DateTime.now(), Comments__c ='test comment');
        insert dispo;
        Call_Disposition__c callDispo= new Call_Disposition__c(Name='PP - One-Legger - Follow-up Appt Scheduled', Telemar_Dispo_Code__c='1LA',ActionItems__c='Hot Leads', Comments_Required__c=true, Active__c=true,Tier_One__c= dispo.name);
        insert callDispo;
        Opportunity opp = New Opportunity();
          opp.AccountId = acc.Id;
          opp.Name = 'TestOpp1';
          opp.StageName = 'Prospecting';
          opp.CloseDate = Date.today();
          insert opp; 
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Account__c = acc.Id;
        q.Name = 'Test Quote';
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'TestStatus';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        ApexPages.currentPage().getParameters().put('usId',u.id);
        ApexPages.currentPage().getParameters().put('userEmail','a@tcs.com');
        ApexPages.currentPage().getParameters().put('ph','1111111111');
        ApexPages.StandardController sc = new Apexpages.StandardController(acc);
        HotLeadsController hotLead = new HotLeadsController(sc);
        hotLead.changeOwnership();
        
        Id userId = userInfo.getUserId();
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        ProcessCloseCall.processDispositionActions(current.id,acc,dispo);
        Test.stopTest();
    }
     @isTest
    public static void testHotLead2(){
        
        user u = new User();
        u.id = UserInfo.getUserId();
        u.MobilePhone = '95959595951';
        update u;
        Test.startTest();
        system.runAs(u){
            ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
            res.name = 'DataRecastDisableAccountTrigger';
            res.value__c = 'TRUE';
            insert res;
                
            ResaleGlobalVariables__c res1 = new ResaleGlobalVariables__c();
            res1.name = 'allowedOrderTypesHotLeads';
            res1.value__c = 'N1;R1;R2;N2';
            insert res1;
                
            Wireless_Carrier__c wirelesObj = new Wireless_Carrier__c();
            wirelesObj.Name = 'Verizon';
            wirelesObj.SMS_Email_Service__c = 'number@vtext.com';
            wirelesObj.SMS_Enabled__c = TRUE;
            insert wirelesObj;
        }
        
                
        Address__c addObj = ADTPartnerCustomerLookupTest.createAddr();
        Account acc = ADTPartnerCustomerLookupTest.CreateAccount(addObj);
        acc.Email__c = 'a@tcs.com';
        acc.OwnerId = u.id;
        acc.Channel__c = 'Home Health';
        acc.MMBOrderType__c = 'N1';
        update acc;
        
        TerritoryAssignment__c terriassgObj = new TerritoryAssignment__c();
        terriassgObj.PostalCodeID__c = acc.PostalCodeID__c;
        terriassgObj.TerritoryType__c ='Home Health';
        insert terriassgObj;
        
        ManagerTown__c managrTwnObj = new ManagerTown__c();
        managrTwnObj.ManagerID__c= u.Id;
        managrTwnObj.TownID__c = 'TestTNID';
        managrTwnObj.Type__c = '1;2;3';
        insert managrTwnObj ;
         //callDispDetail = [ SELECT ActionItems__c FROM Call_Disposition__c WHERE Name =:cDisp.DispositionDetail__c LIMIT 1]; 
        
        disposition__c dispo= new disposition__c(DispositionType__c='Mailed', DispositionDetail__c='PP - One-Legger - Follow-up Appt Scheduled', DispositionDate__c=DateTime.now(), Comments__c ='test comment');
        insert dispo;
        Call_Disposition__c callDispo= new Call_Disposition__c();
        insert callDispo;
        Opportunity opp = New Opportunity();
          opp.AccountId = acc.Id;
          opp.Name = 'TestOpp1';
          opp.StageName = 'Prospecting';
          opp.CloseDate = Date.today();
          insert opp; 
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Account__c = acc.Id;
        q.Name = 'Test Quote';
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'TestStatus';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        ApexPages.currentPage().getParameters().put('usId',u.id);
        ApexPages.currentPage().getParameters().put('userEmail','a@tcs.com');
        ApexPages.currentPage().getParameters().put('ph','1111111111');
        ApexPages.StandardController sc = new Apexpages.StandardController(acc);
        HotLeadsController hotLead = new HotLeadsController(sc);
        hotLead.changeOwnership();
        
        Id userId = userInfo.getUserId();
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        ProcessCloseCall.processDispositionActions(current.id,acc,dispo);
        Test.stopTest();
    }
      @isTest
    public static void testHotLead1(){
        
        user u = new User();
        u.id = UserInfo.getUserId();
        u.MobilePhone = '95959595951';
        update u;
        Test.startTest();
        system.runAs(u){
            ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
            res.name = 'DataRecastDisableAccountTrigger';
            res.value__c = 'TRUE';
            insert res;
                
            ResaleGlobalVariables__c res1 = new ResaleGlobalVariables__c();
            res1.name = 'allowedOrderTypesHotLeads';
            res1.value__c = 'N1;R1;R2;N2';
            insert res1;
                
            Wireless_Carrier__c wirelesObj = new Wireless_Carrier__c();
            wirelesObj.Name = 'Verizon';
            wirelesObj.SMS_Email_Service__c = 'number@vtext.com';
            wirelesObj.SMS_Enabled__c = TRUE;
            insert wirelesObj;
        }
        
                
        Address__c addObj = ADTPartnerCustomerLookupTest.createAddr();
        Account acc = ADTPartnerCustomerLookupTest.CreateAccount(addObj);
        acc.Email__c = 'a@tcs.com';
        acc.OwnerId = u.id;
        acc.Channel__c = 'Home Health';
        acc.MMBOrderType__c = 'N1';
        update acc;
        
        TerritoryAssignment__c terriassgObj = new TerritoryAssignment__c();
        terriassgObj.PostalCodeID__c = acc.PostalCodeID__c;
        terriassgObj.TerritoryType__c ='Home Health';
        insert terriassgObj;
        
        ManagerTown__c managrTwnObj = new ManagerTown__c();
        managrTwnObj.ManagerID__c= u.Id;
        managrTwnObj.TownID__c = 'TestTNID';
        managrTwnObj.Type__c = '1;2;3';
        insert managrTwnObj ;
         //callDispDetail = [ SELECT ActionItems__c FROM Call_Disposition__c WHERE Name =:cDisp.DispositionDetail__c LIMIT 1]; 
        
        disposition__c dispo= new disposition__c(DispositionType__c='Mailed', DispositionDetail__c='PP - One-Legger - Follow-up Appt Scheduled', DispositionDate__c=DateTime.now(), Comments__c ='test comment');
        insert dispo;
        Call_Disposition__c callDispo= new Call_Disposition__c(Name='PP - One-Legger - Follow-up Appt Scheduled', Telemar_Dispo_Code__c='1LA',ActionItems__c='Reassignment', Comments_Required__c=true, Active__c=true,Tier_One__c= dispo.name);
        insert callDispo;
        Opportunity opp = New Opportunity();
          opp.AccountId = acc.Id;
          opp.Name = 'TestOpp1';
          opp.StageName = 'Prospecting';
          opp.CloseDate = Date.today();
          insert opp; 
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Account__c = acc.Id;
        q.Name = 'Test Quote';
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'TestStatus';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        ApexPages.currentPage().getParameters().put('usId',u.id);
        ApexPages.currentPage().getParameters().put('userEmail','a@tcs.com');
        ApexPages.currentPage().getParameters().put('ph','1111111111');
        ApexPages.StandardController sc = new Apexpages.StandardController(acc);
        HotLeadsController hotLead = new HotLeadsController(sc);
        hotLead.changeOwnership();
        
        Id userId = userInfo.getUserId();
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        ProcessCloseCall.processDispositionActions(current.id,acc,dispo);
        Test.stopTest();
    }
    
      @isTest
    public static void testHotLead3(){
        
        user u = new User();
        u.id = UserInfo.getUserId();
        u.MobilePhone = '95959595951';
        update u;
        Test.startTest();
        system.runAs(u){
            ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
            res.name = 'DataRecastDisableAccountTrigger';
            res.value__c = 'TRUE';
            insert res;
                
            ResaleGlobalVariables__c res1 = new ResaleGlobalVariables__c();
            res1.name = 'allowedOrderTypesHotLeads';
            res1.value__c = 'N1;R1;R2;N2';
            insert res1;
                
            Wireless_Carrier__c wirelesObj = new Wireless_Carrier__c();
            wirelesObj.Name = 'Verizon';
            wirelesObj.SMS_Email_Service__c = 'number@vtext.com';
            wirelesObj.SMS_Enabled__c = TRUE;
            insert wirelesObj;
        }
        
                
        Address__c addObj = ADTPartnerCustomerLookupTest.createAddr();
        Account acc = ADTPartnerCustomerLookupTest.CreateAccount(addObj);
        acc.Email__c = 'a@tcs.com';
        acc.OwnerId = u.id;
        acc.Channel__c = 'Home Health';
        acc.MMBOrderType__c = 'N1';
        update acc;
        
        TerritoryAssignment__c terriassgObj = new TerritoryAssignment__c();
        terriassgObj.PostalCodeID__c = acc.PostalCodeID__c;
        terriassgObj.TerritoryType__c ='Home Health';
        insert terriassgObj;
        
        ManagerTown__c managrTwnObj = new ManagerTown__c();
        managrTwnObj.ManagerID__c= u.Id;
        managrTwnObj.TownID__c = 'TestTNID';
        managrTwnObj.Type__c = '1;2;3';
        insert managrTwnObj ;
         //callDispDetail = [ SELECT ActionItems__c FROM Call_Disposition__c WHERE Name =:cDisp.DispositionDetail__c LIMIT 1]; 
        
        disposition__c dispo= new disposition__c(DispositionType__c='Mailed', DispositionDetail__c='', DispositionDate__c=DateTime.now(), Comments__c ='test comment');
        insert dispo;
        Call_Disposition__c callDispo= new Call_Disposition__c(Name='PP - One-Legger - Follow-up Appt Scheduled', Telemar_Dispo_Code__c='1LA',ActionItems__c='Reassignment', Comments_Required__c=true, Active__c=true,Tier_One__c= dispo.name);
        insert callDispo;
        Opportunity opp = New Opportunity();
          opp.AccountId = acc.Id;
          opp.Name = 'TestOpp1';
          opp.StageName = 'Prospecting';
          opp.CloseDate = Date.today();
          insert opp; 
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Account__c = acc.Id;
        q.Name = 'Test Quote';
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'TestStatus';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        ApexPages.currentPage().getParameters().put('usId',u.id);
        ApexPages.currentPage().getParameters().put('userEmail','a@tcs.com');
        ApexPages.currentPage().getParameters().put('ph','1111111111');
        ApexPages.StandardController sc = new Apexpages.StandardController(acc);
        HotLeadsController hotLead = new HotLeadsController(sc);
        hotLead.changeOwnership();
        
        Id userId = userInfo.getUserId();
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        ProcessCloseCall.processDispositionActions(current.id,acc,dispo);
        Test.stopTest();
    }
    
    
     @isTest
    public static void testHotLead4(){
        
        user u = new User();
       // u.username= 'nishanthmandala@adt.com.adttest3';
        u.id = UserInfo.getUserId();
     //   u.username = 'SF Test';
        u.MobilePhone = '95959345951';
        update u;
        Test.startTest();
        system.runAs(u){
            ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
            res.name = 'DefaultNationalOwnerUsername';
            res.value__c = u.username; 
            insert res;
                
            ResaleGlobalVariables__c res1 = new ResaleGlobalVariables__c();
            res1.name = 'allowedOrderTypesHotLeads';
            res1.value__c = 'N1;R1;R2;N2';
            insert res1;
                
            Wireless_Carrier__c wirelesObj = new Wireless_Carrier__c();
            wirelesObj.Name = 'Verizon';
            wirelesObj.SMS_Email_Service__c = 'number@vtext.com';
            wirelesObj.SMS_Enabled__c = TRUE;
            insert wirelesObj;
        }
        ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
            res.name = 'DataRecastDisableAccountTrigger';
            res.value__c = 'TRUE';
            insert res;
                
        Address__c addObj = ADTPartnerCustomerLookupTest.createAddr();
        Account acc = ADTPartnerCustomerLookupTest.CreateAccount(addObj);
        acc.Email__c = 'a@tcs.com';
        acc.OwnerId = u.id;
        acc.Channel__c = 'National Account Sales';
        acc.MMBOrderType__c = 'N1';
        update acc;
        
        TerritoryAssignment__c terriassgObj = new TerritoryAssignment__c();
        terriassgObj.PostalCodeID__c = acc.PostalCodeID__c;
        terriassgObj.TerritoryType__c ='Home Health';
        insert terriassgObj;
        
        ManagerTown__c managrTwnObj = new ManagerTown__c();
        managrTwnObj.ManagerID__c= u.Id;
        managrTwnObj.TownID__c = 'TestTNID';
        managrTwnObj.Type__c = '1;2;3';
        insert managrTwnObj ;
         //callDispDetail = [ SELECT ActionItems__c FROM Call_Disposition__c WHERE Name =:cDisp.DispositionDetail__c LIMIT 1]; 
        
        disposition__c dispo= new disposition__c(DispositionType__c='Mailed', DispositionDetail__c='PP - One-Legger - Follow-up Appt Scheduled', DispositionDate__c=DateTime.now(), Comments__c ='test comment');
        insert dispo;
        Call_Disposition__c callDispo= new Call_Disposition__c(Name='PP - One-Legger - Follow-up Appt Scheduled', Telemar_Dispo_Code__c='1LA',ActionItems__c='Hot Leads', Comments_Required__c=true, Active__c=true,Tier_One__c= dispo.name);
        insert callDispo;
        Opportunity opp = New Opportunity();
          opp.AccountId = acc.Id;
          opp.Name = 'TestOpp1';
          opp.StageName = 'Prospecting';
          opp.CloseDate = Date.today();
          insert opp; 
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Account__c = acc.Id;
        q.Name = 'Test Quote';
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'TestStatus';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        ApexPages.currentPage().getParameters().put('usId',u.id);
        ApexPages.currentPage().getParameters().put('userEmail','a@tcs.com');
        ApexPages.currentPage().getParameters().put('ph','1111111111');
        ApexPages.StandardController sc = new Apexpages.StandardController(acc);
        HotLeadsController hotLead = new HotLeadsController(sc);
        hotLead.changeOwnership();
        
        Id userId = userInfo.getUserId();
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        ProcessCloseCall.processDispositionActions(current.id,acc,dispo);
        Test.stopTest();
    }
     @isTest
    public static void test5(){
        
        user u = new User();
       // u.username= 'nishanthmandala@adt.com.adttest3';
        u.id = UserInfo.getUserId();
     //   u.username = 'SF Test';
        u.MobilePhone = '95959345951';
        update u;
        Test.startTest();
        system.runAs(u){
            ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
            res.name = 'DefaultNationalOwnerUsername';
            res.value__c = u.username; 
            insert res;
                
            ResaleGlobalVariables__c res1 = new ResaleGlobalVariables__c();
            res1.name = 'allowedOrderTypesHotLeads';
            res1.value__c = 'N1;R1;R2;N2';
            insert res1;
                
            Wireless_Carrier__c wirelesObj = new Wireless_Carrier__c();
            wirelesObj.Name = 'Verizon';
            wirelesObj.SMS_Email_Service__c = 'number@vtext.com';
            wirelesObj.SMS_Enabled__c = TRUE;
            insert wirelesObj;
        }
        
        ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
        res.name = 'DataRecastDisableAccountTrigger';
        res.value__c = 'TRUE';
        insert res;    
        Address__c addObj = ADTPartnerCustomerLookupTest.createAddr();
        Account acc = ADTPartnerCustomerLookupTest.CreateAccount(addObj);
        acc.Email__c = 'a@tcs.com';
        acc.OwnerId = u.id;
        acc.Channel__c = 'National Account Sales';
        acc.MMBOrderType__c = 'N1';
        update acc;
        
        TerritoryAssignment__c terriassgObj = new TerritoryAssignment__c();
        terriassgObj.PostalCodeID__c = acc.PostalCodeID__c;
        terriassgObj.TerritoryType__c ='Home Health';
        insert terriassgObj;
        
        ManagerTown__c managrTwnObj = new ManagerTown__c();
        managrTwnObj.ManagerID__c= u.Id;
        managrTwnObj.TownID__c = 'TestTNID';
        managrTwnObj.Type__c = '1;2;3';
        insert managrTwnObj ;
         //callDispDetail = [ SELECT ActionItems__c FROM Call_Disposition__c WHERE Name =:cDisp.DispositionDetail__c LIMIT 1]; 
        disposition__c dispo= new disposition__c(DispositionType__c='Mailed', DispositionDetail__c='PP - One-Legger - Follow-up Appt Scheduled', DispositionDate__c=DateTime.now(), Comments__c ='test comment');
        insert dispo;
        Call_Disposition__c callDispo= new Call_Disposition__c(Name='PP - One-Legger - Follow-up Appt Scheduled', Telemar_Dispo_Code__c='1LA',ActionItems__c='addEbrInquiry', Comments_Required__c=true, Active__c=true,Tier_One__c= dispo.name);
        insert callDispo;
        Opportunity opp = New Opportunity();
          opp.AccountId = acc.Id;
          opp.Name = 'TestOpp1';
          opp.StageName = 'Prospecting';
          opp.CloseDate = Date.today();
          insert opp; 
        scpq__SciQuote__c q = new scpq__SciQuote__c();
        q.Account__c = acc.Id;
        q.Name = 'Test Quote';
        q.scpq__OpportunityId__c = opp.id;
        q.scpq__OrderHeaderKey__c = 'abcxyz';
        q.scpq__SciLastModifiedDate__c = Date.today();
        q.scpq__QuoteId__c = '783188231';
        q.MMBJobNumber__c = '213561';
        q.scpq__Status__c = 'TestStatus';
        q.createdDate = Date.today()-2;
        q.MMBOrderType__c = 'test';
        q.MMBOutOrderType__c = 'test out';
        q.Order_Source__c = 'FIELD';
        q.scpq__TotalDiscountPercent__c = 16.00;
        insert q;
        
        ApexPages.currentPage().getParameters().put('usId',u.id);
        ApexPages.currentPage().getParameters().put('userEmail','a@tcs.com');
        ApexPages.currentPage().getParameters().put('ph','1111111111');
        ApexPages.StandardController sc = new Apexpages.StandardController(acc);
        HotLeadsController hotLead = new HotLeadsController(sc);
        hotLead.changeOwnership();
        
        Id userId = userInfo.getUserId();
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        ProcessCloseCall.processDispositionActions(current.id,acc,dispo);
        Test.stopTest();
    }
    
}