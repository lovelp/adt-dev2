/************************************* MODIFICATION LOG ********************************************************************************************
* RecordTypeName
*
* DESCRIPTION : Defines string literals corresponding to Record Type names.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover				 1/19/2012			- Original Version
*
*													
*/

public with sharing class RecordTypeName {
	public static final String ADMIN_TIME = 'Admin Time';
	public static final String COMPANY_GENERATED_APPOINTMENT = 'Company Generated Appointment';
	public static final String INSTALL_APPOINTMENT = 'Install Appointment';
	public static final String SELF_GENERATED_APPOINTMENT = 'Field Sales Appointment';
	public static final String SALESFORCE_ONLY_EVENT = 'Salesforce-Only Event';
	public static final String ADT_NA_RESALE = 'ADT NA Resale';
	public static final String STANDARD_ACCOUNT = 'Standard';
	public static final String RIF_ACCOUNT = 'RIF';
	public static final String SYSTEM_GENERATED_LEAD = 'System Generated Lead';
	public static final String USER_ENTERED_LEAD = 'User Entered Lead';
	public static final String COMMERCIAL_LEAD = 'Commercial';
}