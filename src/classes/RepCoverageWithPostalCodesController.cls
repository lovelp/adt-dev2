/************************************* MODIFICATION LOG ********************************************************************************************
* RepCoverageController
*
* DESCRIPTION : Controller to display the Repcoverage for terrioties by Order types
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* GiriBabu Geda                 09/12/2016            - Original Version
* Siddarth Asokan               07/31/2019            - Biz Id changes to support Commercial                           
*/
public class RepCoverageWithPostalCodesController {
    public string SelectedBid {get;set;}
    public String searchTown {get; set;}
    public String selectedTown {get; set;}
    public String searchsubTown {get;set;}
    public String searchPostalCode {get;set;}
    public String selectedSubTown {get;set;}
    public String selectedPostalCode {get;set;}
    public transient list<allTerritoryUsers> allterrList {get;set;}
    public Map<Id,String> monUsrMap {get;set;}
    public Map<Id,String> tueUsrMap {get;set;}
    public Map<Id,String> wedUsrMap {get;set;}
    public Map<Id,String> thuUsrMap {get;set;}
    public Map<Id,String> friUsrMap {get;set;}
    public Map<Id,String> satUsrMap {get;set;}
    public Map<Id,String> sunUsrMap {get;set;}
    
    public RepCoverageWithPostalCodesController(){
            
        SelectedBid ='';
        selectedTown ='';
        selectedPostalCode ='';
        selectedSubTown= ''; 
        allterrList = new list<allTerritoryUsers>();
        monUsrMap = new Map<Id,String>();
        tueUsrMap = new Map<Id,String>();
        wedUsrMap = new Map<Id,String>();
        thuUsrMap = new Map<Id,String>();
        friUsrMap = new Map<Id,String>();
        satUsrMap = new Map<Id,String>();
        sunUsrMap = new Map<Id,String>();
    }
    
    Public list<SelectOption>  getLineofBusiness(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All','All'));
        options.add(new SelectOption('RESI','RESI'));
        options.add(new SelectOption('SMB','SMB'));
        //options.add(new SelectOption('COM','COM'));
        return options;
    }
    //Method to autofill the town search field
    @remoteAction
    public static list<String> findTownID(string searchTown,string busId){
        system.Debug('searchTown=='+searchTown);
        List<String> StrTowns = new List<String>();
        Set<String> setTowns = new Set<String>();
        String strSearch = '%'+SearchTown+'%';
        String BID = '';
        List<String> lstBID = new List<String>{'1100','1200'};         
        if(busId == 'All'){
            for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessID__c IN:lstBID and TMTownID__c like :strSearch]){
              setTowns.add(PCA.TMTownID__c);
            }
        }else{
            // HRM-10450 Biz Id Change
            BID = Channels.getFormatedBusinessId(busId, Channels.BIZID_OUTPUT.NUM);
            for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessID__c =:BID and TMTownID__c like :strSearch]){
                  setTowns.add(PCA.TMTownID__c);
            }
        }
        StrTowns.addAll(setTowns); 
        return StrTowns;
    }
    //Method to autofill the sub-town search field
    @remoteAction
    public static list<String> findsubtownID(string searchSubTown,string busId){
        List<String> StrSubTowns = new List<String>();
        set<String> setSubTowns = new Set<String>();
        List<String> lstBID = new List<String>{'1100','1200'}; 
        String strSearch = '%'+searchsubTown+'%';
        String BID ='';
        if(busId == 'All'){
             for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessID__c IN :lstBID and TMSubTownID__c like :strSearch]){
              setSubTowns.add(PCA.TMSubTownID__c);
            }
        }else{
            // HRM-10450 Biz Id Change
            BID = Channels.getFormatedBusinessId(busId, Channels.BIZID_OUTPUT.NUM);
            for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessID__c = :BID and TMSubTownID__c like :strSearch]){
                  setSubTowns.add(PCA.TMSubTownID__c);
            }
        }
        StrSubTowns.addAll(setSubTowns);
        return StrSubTowns;
    }

    //Method to autofill the Postalcodesearch field
    @remoteAction
    public static list<String> findPostalcode(string searchPostalCode,string busId){
        List<String> strPostalCodes = new List<String>();
        String strSearch = searchPostalCode+'%';
        String strQuery ='Select id,Name from Postal_Codes__c where Name like \''+strSearch+'\'';
        if(busId=='RESI'){
           strQuery =strQuery+ ' and BusinessID__c=\'1100\' order by Name Asc';
        }else if(busId=='SMB'){
            strQuery =strQuery+ ' and BusinessID__c=\'1200\' order by Name Asc';
        }else{
            strQuery =strQuery+ ' and BusinessID__c IN (\'1200\',\'1100\') order by Name Asc';
        }
        for(Postal_Codes__c pcId :database.Query(strQuery)){
            strPostalCodes.add(pcId.Name);
        }
        return strPostalCodes;
    }
    
    public pagereference showRepCoverageByTerritory(){
        allterrList = new list<allTerritoryUsers>();
        Map<String,List<SchedUserTerr__c>> mapSchusrTerr =New Map<String,List<SchedUserTerr__c>>();
        Map<String,List<String>> mapPostalcodes = new Map<String,List<String>>();
        List<SchedUserTerr__c> SchedUserTerr =new List<SchedUserTerr__c>();
        Map<String,Postal_Code_Aggregate__c> mapPCA = new Map<String,Postal_Code_Aggregate__c>();
        Set<String> businessId = new set<String>();
        Set<String> tmTownId = new set<String>();
        Set<String> tmSubtownId = new set<String>();
        list<String> lstBids = new List<String>();
        String bid ='';
        if(SelectedBid == 'All'){
            lstBids.add('1100');
            lstBids.add('1200');
        }else {
            // HRM-10450 Biz Id Change
            bid = Channels.getFormatedBusinessId(SelectedBid, Channels.BIZID_OUTPUT.NUM);
        }
        if(String.isBlank(selectedTown) && !String.isBlank(searchTown)){
            selectedTown =searchTown;
        }
        if(String.isBlank(selectedSubTown) && !String.isBlank(searchSubTown)){
            selectedSubTown =searchSubTown;
        }
        if(String.isBlank(selectedPostalCode) &&  !String.isBlank(searchPostalCode)){
            selectedPostalCode=searchPostalCode;
        }
        if(String.isNotBlank(selectedTown) && String.isBlank(selectedSubTown)){           
            if(lstBids!=null && lstBids.size()>0){
                for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessID__c IN : lstBids and TMTownID__c=:selectedTown]){
                    mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);                
                }
                SchedUserTerr = [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,TMTownID__c,TMSubTownID__c,BusinessId__c,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c IN:lstBids and TMTownID__c=:selectedTown and user__r.isActive=true limit 10000];
            }else{
                for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessID__c= : bid and TMTownID__c=:selectedTown]){
                    mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);               
                }
                SchedUserTerr = [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,TMTownID__c,TMSubTownID__c,BusinessId__c,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c=:bid and TMTownID__c=:selectedTown and user__r.isActive=true limit 10000];
            }                      
        }else if(String.isNotBlank(selectedSubTown) && String.isBlank(selectedTown)) { 
            if(lstBids!=null && lstBids.size()>0){
                for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessID__c IN : lstBids and TMSubTownID__c=: selectedSubTown]){
                    mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);
                }            
                SchedUserTerr = [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,TMTownID__c,TMSubTownID__c,BusinessId__c,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c IN:lstBids and TMSubTownID__c=:selectedSubTown and user__r.isActive=true limit 10000 ];
            }else{
                for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessID__c= : bid and TMSubTownID__c=: selectedSubTown]){
                    mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);
                }            
                SchedUserTerr = [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,TMTownID__c,TMSubTownID__c,BusinessId__c,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c=:bid and TMSubTownID__c=:selectedSubTown and user__r.isActive=true  limit 10000];
            }
        }else if(String.isNotBlank(selectedTown) && String.isNotBlank(selectedSubTown)){
            if(lstBids!=null && lstBids.size()>0){
                for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessID__c IN : lstBids and TMTownID__c=:selectedTown and TMSubTownID__c=: selectedSubTown]){
                    mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);
                }            
                SchedUserTerr = [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,TMTownID__c,TMSubTownID__c,BusinessId__c,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c=:bid and TMTownID__c=:selectedTown and TMSubTownID__c=:selectedSubTown and user__r.isActive=true limit 10000];
            }else{
                for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessID__c= : bid and TMTownID__c=:selectedTown and TMSubTownID__c=: selectedSubTown]){
                    mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);
                }            
                SchedUserTerr = [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,TMTownID__c,TMSubTownID__c,BusinessId__c,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c=:bid and TMTownID__c=:selectedTown and TMSubTownID__c=:selectedSubTown and user__r.isActive=true limit 10000];
            }
        }else if(String.isNotBlank(selectedPostalCode)){            
            String strQuery ='Select id,Name,TMSubTownID__c,TMTownID__c from Postal_Codes__c where Name = \''+selectedPostalCode+'\'';
            list<String> tmTownids = new List<String>();
            list<String> tmSubTownids = new List<String>();
            
            if(SelectedBid=='RESI'){
               strQuery =strQuery+ ' and BusinessID__c=\'1100\' ';
            }else if(SelectedBid=='SMB'){
                strQuery =strQuery+ ' and BusinessID__c=\'1200\' ';
            }else{
                strQuery =strQuery+ ' and BusinessID__c IN (\'1100\',\'1200\') ';
            }
            List<Postal_Codes__c> postcodes = Database.query(strQuery);
            for(Postal_Codes__c pcs : postcodes){
                if(pcs.TMTownID__c!=null){
                    tmTownids.add(pcs.TMTownID__c);
                }
                if(pcs.TMSubTownID__c!=null){
                    tmSubTownids.add(pcs.TMSubTownID__c);
                }
            }
            System.Debug('postcodes==='+postcodes);
            
            if(lstBids !=null && lstBids.size()>0){
                for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessId__c IN :lstBids and TMTownID__c IN :tmTownids and (TMSubTownID__c IN :tmSubTownids or TMSubTownID__c=:null)]){
                    mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);
                } 
                SchedUserTerr = [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,TMTownID__c,TMSubTownID__c,BusinessId__c,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c IN :lstBids and TMTownID__c IN :tmTownids and (TMSubTownID__c IN :tmSubTownids or TMSubTownID__c=:null) and user__r.isActive=true limit 10000];
            }else{
                for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessId__c=:bid and TMTownID__c IN :tmTownids and (TMSubTownID__c IN :tmSubTownids or TMSubTownID__c=:null)]){
                    mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);
                }
                SchedUserTerr = [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,TMTownID__c,TMSubTownID__c,BusinessId__c,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c=:bid and TMTownID__c IN :tmTownids and (TMSubTownID__c IN :tmSubTownids or TMSubTownID__c=:null) and user__r.isActive=true limit 10000];
            }
        }else{            
            if(lstBids !=null && lstBids.size()>0){
                for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessId__c IN :lstBids]){
                    mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);
                } 
                SchedUserTerr = [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,TMTownID__c,TMSubTownID__c,BusinessId__c,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c=:lstBids and user__r.isActive=true limit 10000];
            }else{
                for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c where BusinessId__c=:bid]){
                    mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);
                }
                SchedUserTerr = [Select id,Name,Order_Types__c,User__c, User__r.LastName, User__r.FirstName,TMTownID__c,TMSubTownID__c,BusinessId__c,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c=:bid and user__r.isActive=true limit 10000];
            }
        }
        
        for(SchedUserTerr__c schusr : SchedUserTerr){
            system.Debug('schusr==='+schusr);
            List<SchedUserTerr__c> lstterrusers = mapSchusrTerr.get(schusr.BusinessId__c+':'+schusr.TMTownID__c+':'+schusr.TMSubTownID__c);
            if (lstterrusers == null) {
                lstterrusers = new List<SchedUserTerr__c>();
                mapSchusrTerr.put(schusr.BusinessId__c+':'+schusr.TMTownID__c+':'+schusr.TMSubTownID__c, lstterrusers);
            }
            lstterrusers.add(schusr);
        } 
        system.Debug(mapPCA.keySet());
        String territoryModel;String PostalCode;string townId;string subTownId;string TMBusinessId;
        for(String str : mapPCA.keySet()){
            if(mapSchusrTerr.get(str)!=null && mapSchusrTerr.get(str).Size()>0){
                String NewRepName='';
                String ResaleRepName='';
                String RelocationRepName='';
                String AddOnRepName='';
                String ConversionRepName='';
                String CustomHomeRepName=''; 
                String ordType ='New;Resale;Relocation;Add-On;Conversion;Custom Home';
                for(SchedUserTerr__c schterr : mapSchusrTerr.get(str)){
                    townId =schterr.TMTownID__c;
                    subTownId =schterr.TMSubTownID__c;
                    TMBusinessId=schterr.BusinessId__c;                    
                    if(schterr.Order_Types__c !=null){                       
                        String usrName = schterr.User__r.FirstName+' '+ schterr.User__r.LastName;                       
                        if(schterr.Order_Types__c.Contains('New'))
                            NewRepName +=usrName+';';
                        if(schterr.Order_Types__c.Contains('Resale'))
                            ResaleRepName +=usrName+';';
                        if(schterr.Order_Types__c.Contains('Relocation'))
                            RelocationRepName +=usrName+';';
                        if(schterr.Order_Types__c.Contains('Add-On'))
                            AddOnRepName +=usrName+';';
                        if(schterr.Order_Types__c.Contains('Conversion'))
                            ConversionRepName +=usrName+';';
                        if(schterr.Order_Types__c.Contains('Custom Home'))
                            CustomHomeRepName +=usrName+';';                        
                    }
                }
                allTerritoryUsers terr = new allTerritoryUsers('UserSchedules',townId,subTownId,null,
                                                    ordType,NewRepName,ResaleRepName,RelocationRepName,AddOnRepName,ConversionRepName,CustomHomeRepName,str,TMBusinessId);
                allterrList.add(terr);
            }else{                             
                allTerritoryUsers terr = new allTerritoryUsers('UserSchedules',mapPCA.get(str).TMTownID__c,mapPCA.get(str).TMSubTownID__c,null,null,
                                                                null,null,null,null,null,null,null,mapPCA.get(str).BusinessID__c);
                allterrList.add(terr);
            }            
        }
        selectedTown = '';
        selectedPostalCode = '';
        selectedSubTown= '';        
        return null;
   }
   
   public pagereference showRepCoverageforAllTerritories(){
        allterrList = new list<allTerritoryUsers>();
        Map<String,List<SchedUserTerr__c>> mapSchusrTerr =New Map<String,List<SchedUserTerr__c>>();
        Map<String,List<String>> mapPostalcodes = new Map<String,List<String>>();
        Map<String,Postal_Code_Aggregate__c> mapPCA = new Map<String,Postal_Code_Aggregate__c>();
        Set<String> businessId = new set<String>();
        Set<String> tmTownId = new set<String>();
        Set<String> tmSubtownId = new set<String>();
        
        for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c]){
            mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);
            businessId.add(PCA.BusinessID__c);
            tmTownId.add(PCA.TMTownID__c);
            tmSubtownId.add(PCA.TMSubTownID__c);    
        }
        
        for(Postal_Codes__c postalcode : [Select id,Name,TMSubTownID__c,TMTownID__c,BusinessID__c from Postal_Codes__c  where BusinessID__c IN : businessId and TMTownID__c IN : tmTownId and TMSubTownID__c IN : tmSubtownId] ){
                
            List<String> lstPCs = mapPostalcodes.get(postalcode.BusinessID__c+':'+postalcode.TMTownID__c+':'+postalcode.TMSubTownID__c);
            if (lstPCs == null) {
                lstPCs = new List<String>();
                mapPostalcodes.put(postalcode.BusinessID__c+':'+postalcode.TMTownID__c+':'+postalcode.TMSubTownID__c, lstPCs);
            }
            lstPCs.add(postalcode.Name);
        }
                
        String strName = SelectedBid+'%';
               
        for(SchedUserTerr__c schusr : [Select id,Name,Order_Types__c,BusinessId__c,TMSubTownID__c,TMTownID__c,User__c, User__r.LastName, User__r.FirstName,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c IN : businessId and TMTownID__c IN : tmTownId and TMSubTownID__c IN : tmSubtownId  and user__r.isActive=true]){
            List<SchedUserTerr__c> lstterrusers = mapSchusrTerr.get(schusr.BusinessId__c+':'+schusr.TMTownID__c+':'+schusr.TMSubTownID__c);
            if (lstterrusers == null) {
                lstterrusers = new List<SchedUserTerr__c>();
                mapSchusrTerr.put(schusr.BusinessId__c+':'+schusr.TMTownID__c+':'+schusr.TMSubTownID__c, lstterrusers);
            }
            lstterrusers.add(schusr);
        }
        String territoryModel;String PostalCode;string townId;string subTownId;string TMbusinessId;
        for(String str : mapPCA.keySet()){
            if(mapSchusrTerr.get(str)!=null && mapSchusrTerr.get(str).Size()>0){
                String NewRepName='';
                String ResaleRepName='';
                String RelocationRepName='';
                String AddOnRepName='';
                String ConversionRepName='';
                String CustomHomeRepName='';  
                String ordType ='New;Resale;Relocation;Add-On;Conversion;Custom Home';
                for(SchedUserTerr__c schterr : mapSchusrTerr.get(str)){
                        townId =schterr.TMTownID__c;
                    subTownId =schterr.TMSubTownID__c;
                    TMbusinessId =schterr.BusinessId__c;                    
                    if(schterr.Order_Types__c !=null){
                        String usrName = schterr.User__r.FirstName+' '+ schterr.User__r.LastName;
                        if(schterr.Order_Types__c.Contains('New'))
                            NewRepName +=usrName+';';
                        if(schterr.Order_Types__c.Contains('Resale'))
                            ResaleRepName +=usrName+';';
                        if(schterr.Order_Types__c.Contains('Relocation'))
                            RelocationRepName +=usrName+';';
                        if(schterr.Order_Types__c.Contains('Add-On'))
                            AddOnRepName +=usrName+';';
                        if(schterr.Order_Types__c.Contains('Conversion'))
                            ConversionRepName +=usrName+';';
                        if(schterr.Order_Types__c.Contains('Custom Home'))
                            CustomHomeRepName +=usrName+';';                       
                    }
                }
                allTerritoryUsers terr = new allTerritoryUsers('UserSchedules',townId,subTownId,null,
                                                    ordType,NewRepName,ResaleRepName,RelocationRepName,AddOnRepName,ConversionRepName,CustomHomeRepName,str,TMbusinessId);
                allterrList.add(terr);
            }else {
                allTerritoryUsers terr = new allTerritoryUsers('UserSchedules',mapPCA.get(str).TMTownID__c,mapPCA.get(str).TMSubTownID__c ,null,null,
                                                                null,null,null,null,null,null,null,mapPCA.get(str).BusinessId__c);
                allterrList.add(terr);
            }
        }
        return null;        
   }
   
   public pagereference showAllTerritorieswithNoRepCoverage(){        
        allterrList = new list<allTerritoryUsers>();
        Map<String,List<SchedUserTerr__c>> mapSchusrTerr =New Map<String,List<SchedUserTerr__c>>();
        Map<String,String> MapAccandterrids = new Map<String,String>();
        Map<String,List<String>> mapPostalcodes = new Map<String,List<String>>();
        Map<String,Postal_Code_Aggregate__c> mapPCA = new Map<String,Postal_Code_Aggregate__c>();
        Set<String> businessId = new set<String>();
        Set<String> tmTownId = new set<String>();
        Set<String> tmSubtownId = new set<String>();
        
        for(Postal_Code_Aggregate__c PCA : [select id,Name,BusinessID__c,TMSubTownID__c,TMTownID__c from Postal_Code_Aggregate__c]){
            mapPCA.Put(PCA.BusinessID__c+':'+PCA.TMTownID__c+':'+PCA.TMSubTownID__c,PCA);
            businessId.add(PCA.BusinessID__c);
            tmTownId.add(PCA.TMTownID__c);
            tmSubtownId.add(PCA.TMSubTownID__c);    
        }
                
        String strName = SelectedBid+'%';
               
        for(SchedUserTerr__c schusr : [Select id,Name,Order_Types__c,BusinessId__c,TMSubTownID__c,TMTownID__c,User__c, User__r.LastName, User__r.FirstName,Territory_Association_ID__c from SchedUserTerr__c where BusinessId__c IN : businessId and TMTownID__c IN : tmTownId and TMSubTownID__c IN : tmSubtownId  and user__r.isActive=true]){
            List<SchedUserTerr__c> lstterrusers = mapSchusrTerr.get(schusr.BusinessId__c+':'+schusr.TMTownID__c+':'+schusr.TMSubTownID__c);
            if (lstterrusers == null) {
                lstterrusers = new List<SchedUserTerr__c>();
                mapSchusrTerr.put(schusr.BusinessId__c+':'+schusr.TMTownID__c+':'+schusr.TMSubTownID__c, lstterrusers);
            }
            lstterrusers.add(schusr);
        }
        
        for(String str : mapPCA.keySet()){
            if(mapSchusrTerr.get(str)==null){
                allTerritoryUsers terr = new allTerritoryUsers(mapPCA.get(str).Name,mapPCA.get(str).TMTownID__c,mapPCA.get(str).TMSubTownID__c ,null,null,
                                                                null,null,null,null,null,null,null,mapPCA.get(str).BusinessId__c);
                allterrList.add(terr);
            }            
        }
        
        return null;
        
   }
   public List<SchedUserTerr__c> getsearchTerritoryUsers(){
        List<SchedUserTerr__c> terrUserSchedules = new List<SchedUserTerr__c>();        
        String selectedTerrId =ApexPages.currentPage().getParameters().get('terrid');
        String bId=ApexPages.currentPage().getParameters().get('bId');
        String tId=ApexPages.currentPage().getParameters().get('tId');
        String stId=ApexPages.currentPage().getParameters().get('stid');
        
        terrUserSchedules=[select id,user__c,user__r.Name,Order_Types__c,TMTownID__c,TMSubTownID__c,BusinessId__c,Monday_Available_Start__c,Monday_Available_End__c,
                                                   Tuesday_Available_Start__c, Tuesday_Available_End__c,
                                                   Wednesday_Available_Start__c,Wednesday_Available_End__c,
                                                   Thursday_Available_Start__c,Thursday_Available_End__c,
                                                   Friday_Available_Start__c,Friday_Available_End__c,
                                                   Saturday_Available_Start__c,Saturday_Available_End__c,
                                                   Sunday_Available_Start__c,Sunday_Available_End__c
                           From SchedUserTerr__c Where BusinessId__c=:bId and TMTownID__c=:tId and TMSubTownID__c=:stId and user__r.isactive=true and Order_Types__c!=null limit 10000];
        for (SchedUserTerr__c ut:terrUserSchedules){
            String repNotAvailable = 'Not Available';
            if (ut.Monday_Available_Start__c > 0 && ut.Monday_Available_End__c > 0)               
                monUsrMap.put(ut.user__c,GetStartAndEndtimes(ut.Monday_Available_Start__c,ut.Monday_Available_End__c));
            else
                monUsrMap.put(ut.user__c, repNotAvailable);
                
            if (ut.Tuesday_Available_Start__c > 0  && ut.Tuesday_Available_End__c > 0)                
                tueUsrMap.put(ut.user__c,GetStartAndEndtimes(ut.Tuesday_Available_Start__c,ut.Tuesday_Available_End__c));
            else
                tueUsrMap.put(ut.user__c, repNotAvailable);
                
            if (ut.Wednesday_Available_Start__c > 0 && ut.Wednesday_Available_End__c > 0)                
                wedUsrMap.put(ut.user__c,GetStartAndEndtimes(ut.Wednesday_Available_Start__c,ut.Wednesday_Available_End__c));
            else
                wedUsrMap.put(ut.user__c, repNotAvailable);
                
            if (ut.Thursday_Available_Start__c > 0 && ut.Thursday_Available_End__c > 0)               
                thuUsrMap.put(ut.user__c,GetStartAndEndtimes(ut.Thursday_Available_Start__c,ut.Thursday_Available_End__c));
            else
                thuUsrMap.put(ut.user__c, repNotAvailable);
                
            if (ut.Friday_Available_Start__c > 0 && ut.Friday_Available_End__c > 0)               
                friUsrMap.put(ut.user__c,GetStartAndEndtimes(ut.Friday_Available_Start__c,ut.Friday_Available_End__c));
            else
                friUsrMap.put(ut.user__c, repNotAvailable);
                
            if (ut.Saturday_Available_Start__c > 0 && ut.Saturday_Available_End__c > 0)               
                satUsrMap.put(ut.user__c,GetStartAndEndtimes(ut.Saturday_Available_Start__c,ut.Saturday_Available_End__c));
            else
                satUsrMap.put(ut.user__c, repNotAvailable);
                
            if (ut.Sunday_Available_Start__c > 0 && ut.Sunday_Available_End__c > 0)               
                sunUsrMap.put(ut.user__c,GetStartAndEndtimes(ut.Sunday_Available_Start__c,ut.Sunday_Available_End__c));
            else
                sunUsrMap.put(ut.user__c, repNotAvailable);
        }
        return terrUserSchedules;
    }
    
    public string GetStartAndEndtimes(Decimal starttime,Decimal endtime){
        Integer AvailableStartTimeHr = ((decimal)(starttime)/100).intValue();
        Integer AvailableStartTimeMin = (((decimal)(starttime)/100 - AvailableStartTimeHr) * 100).intValue();
        Integer AvailableEndTimeHr = ((decimal)(endtime)/100).intValue();
        Integer AvailableEndTimeMin = (((decimal)(endtime)/100 - AvailableEndTimeHr) * 100).intValue();
        Time resstartVal = Time.newInstance(AvailableStartTimeHr, AvailableStartTimeMin, 0, 0);                
        Time resEndVal = Time.newInstance(AvailableEndTimeHr, AvailableEndTimeMin, 0, 0);
        DateTime startDt = DateTime.newInstance(date.today(),resstartVal);
        DateTime endDt = DateTime.newInstance(date.today(),resEndVal);
        String timeformat =startDt.format('hh:mm a')+' to '+endDt.format('hh:mm a');
        return timeformat;
    }
    
    
   public list<Postal_Codes__c> gettownSubtownpostalcodes(){
       Map<String,List<Postal_Codes__c>> mapPostalcodes = new Map<String,List<Postal_Codes__c>>();
       String bId=ApexPages.currentPage().getParameters().get('bId');
       String tId=ApexPages.currentPage().getParameters().get('tId');
       String stId=ApexPages.currentPage().getParameters().get('stid');
       
       List<Postal_Codes__c> lstPostcodes = new List<Postal_Codes__c>();
       String strSOQL ='Select id,Name,TMSubTownID__c,TMTownID__c,BusinessID__c from Postal_Codes__c where BusinessID__c=\''+ bId +'\'';
       
       if(String.isNotBlank(tId)){
           strSOQL +=' and TMTownID__c =\''+tId +'\'';
       }
       if(String.isNotBlank(stId)){
           strSOQL +=' and TMSubTownID__c  =\''+stId +'\'';
       }
       System.Debug(strSOQL);
       lstPostcodes =Database.Query(strSOQL);
       for(Postal_Codes__c postalcode : lstPostcodes ){
            List<Postal_Codes__c> lstPCs = mapPostalcodes.get(postalcode.BusinessID__c+':'+postalcode.TMTownID__c+':'+postalcode.TMSubTownID__c);
            if (lstPCs == null) {
                lstPCs = new List<Postal_Codes__c>();
                mapPostalcodes.put(postalcode.BusinessID__c+':'+postalcode.TMTownID__c+':'+postalcode.TMSubTownID__c, lstPCs);
            }
            lstPCs.add(postalcode);
        }
        system.Debug(mapPostalcodes.get(bId+':'+tId+':null'));
        if(String.isBlank(stId)){
            return mapPostalcodes.get(bId+':'+tId+':null');
        }else{
            return mapPostalcodes.get(bId+':'+tId+':'+stId);
        }
   }
   
   public class allTerritoryUsers{
        public String territoryModel{get; set;}
        public String PostalCode{get; set;}
        public String OrderType{get; set;}
        public string townId{get; set;}
        public string subTownId{get; set;}
        public String TerritoryAssignedRep{get; set;}
        public String NewOrderType{get; set;}
        public String ResaleOrderType{get; set;}
        public String RelocationOrderType{get; set;}
        public String AddOnOrderType{get; set;}
        public String ConversionOrderType{get; set;}
        public String CustomHomeOrderType{get; set;}
        public String TerritoryID{get;set;}
        public String BusinessId{get;set;}
        
        public allTerritoryUsers(String territory,string tId,string subTId,String PCode,String OType,
                                    string NewName,string Resale,string Relocation,
                                    string AddOn,string Conversion,string CustomHome,string terrId,string busId){
            territoryModel=territory;
            townId = tId;
            subTownId =subTId;
            PostalCode=PCode;
            OrderType=OType;
            if(String.isNotBlank(Otype)){
                for(string Type : Otype.split(';')){
                    if(Type =='New' && String.isNotBlank(NewName)){
                         NewOrderType = NewName.substring(0,NewName.lastIndexof(';'));
                    }
                    if(Type =='Resale' && String.isNotBlank(Resale)){
                         ResaleOrderType = Resale.substring(0,Resale.lastIndexof(';'));
                    }
                    if(Type =='Relocation' && String.isNotBlank(Relocation)){
                         RelocationOrderType = Relocation.substring(0,Relocation.lastIndexof(';'));
                    }
                    if(Type =='Add-On' && String.isNotBlank(AddOn)){
                         AddOnOrderType=AddOn.substring(0,AddOn.lastIndexof(';'));
                    }
                    if(Type =='Conversion' && String.isNotBlank(Conversion)){
                         ConversionOrderType=Conversion.substring(0,Conversion.lastIndexof(';'));
                    }
                    if(Type == 'Custom Home' && String.isNotBlank(CustomHome)){
                        CustomHomeOrderType=CustomHome.substring(0,CustomHome.lastIndexof(';'));
                    }
                }
            }
            TerritoryID = terrId;
            BusinessId = busId;
        }    
    }     
}