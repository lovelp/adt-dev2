/************************************* MODIFICATION LOG ********************************************************************************************
* StreetSheetManager
*
* DESCRIPTION : Responsible for persisting and deleting Street Sheet data.
*
*               Note: Street Sheets are now known as Prospect Lists
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*             				 10/14/2011			- Original Version
* Magdiel Herrera			 8/6/2014			- Added scoring support on prospects
*													
*/
public with sharing class StreetSheetManager {
	
	// Street Sheets are retained for a maximum of 30 days after their last update
	private static final Integer DAYS_TO_RETAIN_SHEET = 30;
	
	// Creates a Street Sheet with the supplied name and the specified items
	public static String create (String streetSheetName, Set<String> itemIdSet, Map<String, StreetSheetItem__c> scoredItemObjMap) {
		// create Street Sheet record
		StreetSheet__c newStreetSheet = new StreetSheet__c(); 
	    newStreetSheet.Name = streetSheetName;
	        				
	    insert(newStreetSheet);
	    System.debug('newStreetSheet...:' +newStreetSheet);
	    
		String streetSheetID = newStreetSheet.Id;
		// create the Street Sheet Item record(s)
		addItems(streetSheetID, itemIdSet, scoredItemObjMap);
		return streetSheetID;
		
	}
	
	public static String create (String streetSheetName, Set<String> itemIdSet) {
		// populate score for these prospects
		Map<String, StreetSheetItem__c> scoredItemObjMap = getStreetSheetItemScore(itemIdSet);
		return create(streetSheetName, itemIdSet, scoredItemObjMap);
	}
	
	// Creates a Street Sheet with a default name and the specified items
	public static String create (Set<String> itemIdSet) {
		return create(deriveDefaultName(), itemIdSet);
	}
	
	public static void addItems(String streetSheetID, Set<String> itemIdSet) {
		// populate score for these prospects
		Map<String, StreetSheetItem__c> scoredItemObjMap = getStreetSheetItemScore(itemIdSet);
		addItems(streetSheetID, itemIdSet, scoredItemObjMap);
	}
	
	// Adds the specified items to an existing Street Sheet
	public static void addItems(String streetSheetID, Set<String> itemIdSet, Map<String, StreetSheetItem__c> scoredItemObjMap) {
		List<StreetSheetItem__c> streetSheetItemsList = new List <StreetSheetItem__c>();
		String acctprefix = Schema.Sobjecttype.Account.getKeyPrefix();
		String leadprefix = Schema.Sobjecttype.Lead.getKeyPrefix();
		
		//Check to make sure that the lead/account is not already in the streetsheet
		List<StreetSheetItem__c> existingSSI = new List<StreetSheetItem__c>();
		Set<Id> existingSSIID = new Set<Id>();
		existingSSI = [Select Id, AccountId__c, LeadId__c from StreetSheetItem__c where StreetSheet__c = : streetSheetId];
		for(StreetSheetItem__c ssitem : existingSSI)
		{
			if(ssitem.AccountId__c != null)
				existingSSIID.add(ssitem.AccountId__c);
			else if (ssitem.LeadId__c != null)
				existingSSIID.add(ssitem.LeadId__c);
		}
		
		for(String itemId : itemIdSet){	
			Boolean addToList = false;
			StreetSheetItem__c ssi = new StreetSheetItem__c();
			ssi.StreetSheet__c = streetSheetID;
			if (itemId.toLowerCase().startsWith(acctprefix.toLowerCase()))
			{
				if(!existingSSIID.contains(itemId))
				{
					ssi.AccountID__c = itemID;
					if(scoredItemObjMap != null && !scoredItemObjMap.isEmpty() && scoredItemObjMap.containsKey(itemID)){
						StreetSheetItem__c scoreObjVal = scoredItemObjMap.get(itemID);
						ssi.Score__c = scoreObjVal.Score__c;
					}
					addToList = true;
				}
			}
			else
			{
				if(!existingSSIID.contains(itemId))
				{
					ssi.LeadID__c = itemID;
					if(scoredItemObjMap != null && !scoredItemObjMap.isEmpty() && scoredItemObjMap.containsKey(itemID)){
						StreetSheetItem__c scoreObjVal = scoredItemObjMap.get(itemID);
						ssi.Score__c = scoreObjVal.Score__c;
					}
					addToList = true;
				}
			}
			if(addToList)
				streetSheetItemsList.add(ssi);
	    	//streetSheetItemsList.add(new StreetSheetItem__c (AccountID__c=itemID, StreetSheet__c=streetSheetID)); 		
	    }
	    
	    insert(streetSheetItemsList);
	    System.debug('streetSheetItemsList...:' +streetSheetItemsList);
	}
	
	// Deletes Street Sheet Items associated with the supplied account ID on Street Sheets
	// created by the supplied user ID
	public static void deleteStreetSheetItems(Map<String, Set<String>> ownerToAccountsMap) 
	{		
		if (ownerToAccountsMap != null && ownerToAccountsMap.size() > 0) 
		{	
			String selectString = 'Select Name, CreatedById, AccountID__c from StreetSheetItem__c';
			String whereString = ' WHERE AccountID__c IN (';	
			Map<String,String> AccountAndOwnerIdMap = new Map<String,String>();
			 
			Integer countOfOwners = 0;
			for (String owner : ownerToAccountsMap.keySet()) 
			{				
				if (countOfOwners != 0) 
				{
					whereString += ', ';
				}		
				
				Integer countOfCriteria=0;
				for (String s: ownerToAccountsMap.get(owner))
				{
					if (countOfCriteria != 0) 
					{
						whereString += ', ';
					}	
					whereString += '\''+s+'\'';
					countOfCriteria++;
					AccountAndOwnerIdMap.put(s,owner);
				}	
				countOfOwners++;
			}
			whereString += ') ';
			
			System.debug('SOQL = ' + selectString + whereString);
			
			List<StreetSheetItem__c> streetSheetItemsList = database.query(selectString + whereString);
			
			List<StreetSheetItem__c> streetSheetItemsDeleteList = new List<StreetSheetItem__c>();		
			
			for(StreetSheetItem__c ssi : streetSheetItemsList)
			{
				if(AccountAndOwnerIdMap.get(ssi.AccountID__c) == ssi.CreatedById)
				{
					streetSheetItemsDeleteList.add(ssi);
				}				
			}
			
			if(streetSheetItemsDeleteList != null && streetSheetItemsDeleteList.size() > 0)
			{
				try 
				{
					delete streetSheetItemsDeleteList;
				} 
				catch (Exception e) 
				{
					System.debug('Unable to delete Street Sheet Items: ' + e.getMessage());
				}	
			}			
		}		
	}	
	
	// Finds and deletes Street Sheets that have had 30 days of inactivity
	public static void deleteExpiredSheets() {
		
		Map<String, StreetSheetRetentionLimit__c> retentionLimitsMap = StreetSheetRetentionLimit__c.getAll();
    
        // Should be one object in the map so get its value
        List<StreetSheetRetentionLimit__c>  retentionLimit = retentionLimitsMap.values(); 
        
        // Assume the default value defined here
        Integer daysToRetain = DAYS_TO_RETAIN_SHEET;
        // But use the value from the custom setting as an override
        if (retentionLimit.size() > 0) {
        	daysToRetain = Integer.valueOf(retentionLimit[0].Name);
        }
        
		Integer daysToDecrement = -1 * daysToRetain; // daysToRetain - daysToRetain * 2;
		Datetime rightNow = Datetime.now();
		Datetime deleteBeforeDatetime = rightNow.addDays(daysToDecrement);
	
		
		// This query will be subject to the limit of 50,000 records returned from a SOQL statement.
		// Based on the size of the user base and the use case for Street Streets (reps create one per day), do not expect to 
		// approach this limit.  If over time, the limit is reached, this method will need to refactored to process batches of records.
		StreetSheet__c[] streetSheetArray = [select ID from StreetSheet__c where LastModifiedDate <= :deleteBeforeDatetime];
		
		try {
			delete streetSheetArray;
		} catch (Exception e) {
			System.debug('Unable to delete expired Street Sheets: ' + e.getMessage());
		}
		
	}
    
    public static void deleteItems(set<String> OwnershipChangedLeadIds)
    {    	
    	list<StreetSheetItem__c> ProspectsListToDelete;    	
    	
    	if(!OwnershipChangedLeadIds.isEmpty())
    	{
    		ProspectsListToDelete = [SELECT Id, Name 
    										FROM StreetSheetItem__c 
    										WHERE LeadID__c IN :OwnershipChangedLeadIds];	
    		
    		if(ProspectsListToDelete != null && ProspectsListToDelete.size() > 0)
    		{
    			delete ProspectsListToDelete;
    		}
    	}    	
    } 
    
    public static void deleteItems(map<String, Account> allNeedsAssignmentAccounts, List<Account> allPreassignedAccounts)
    {
    	set<String> OwnershipChangedAccountIds = new set<String>();
    	list<StreetSheetItem__c> ProspectsListToDelete;
    	
    	if(!allNeedsAssignmentAccounts.isEmpty())
    	{
    		OwnershipChangedAccountIds.addAll(allNeedsAssignmentAccounts.keySet());
    	}
    	if(allPreassignedAccounts != null && allPreassignedAccounts.size() > 0)
    	{
    		for(Account a : allPreassignedAccounts)
    		{
    			OwnershipChangedAccountIds.add(a.Id);
    		}
    	}    	
    	if(!OwnershipChangedAccountIds.isEmpty())
    	{
    		ProspectsListToDelete = [SELECT Id, Name 
    										FROM StreetSheetItem__c 
    										WHERE AccountID__c IN :OwnershipChangedAccountIds];	
    		
    		if(ProspectsListToDelete != null && ProspectsListToDelete.size() > 0)
    		{
    			delete ProspectsListToDelete;
    		}
    	}    	
    } 
	
	/**
	 *	Given a list of prospects Accounts/Leads set a score based on predefined business rules
	 *	@method getStreetSheetItemScore
	 *	@param 	List<SObject>					List of prospects to score
	 *	@return Map<String, StreetSheetItem__c>	Associative array containing the score results for the prospects received 
	 */
	public static Map<String, StreetSheetItem__c> getStreetSheetItemScore(List<SObject> pList){
		Map<String, StreetSheetItem__c> resultMap = new Map<String, StreetSheetItem__c>();

		for(SObject prospectObj: pList){
			Integer ItemScore = 99;// default
			if(prospectObj instanceOf Account){
				ItemScore = getAccountProspectScore((Account)prospectObj);
			}
			else if (prospectObj instanceOf Lead){
				ItemScore = getLeadProspectScore((Lead)prospectObj);
			}
			else{
				// SObject type not supported for prospect lists
			}
			resultMap.put(prospectObj.Id, new StreetSheetItem__c( Score__c = ItemScore ));
		}
		
		return resultMap;
	}
	
	/**
	 *	Given a set of SObject Ids set a prospect score on each
	 *	@method	getStreetSheetItemScore
	 *	@param	Set<String> Ids to score
	 *	@return Map<String, StreetSheetItem__c>	Associative array containing the score results for the prospects received 
	 */
	public static Map<String, StreetSheetItem__c> getStreetSheetItemScore(Set<String> pIds){
		Map<String, StreetSheetItem__c> resultMap = new Map<String, StreetSheetItem__c>();
		String acctprefix = Schema.Sobjecttype.Account.getKeyPrefix();
		String leadprefix = Schema.Sobjecttype.Lead.getKeyPrefix();
		
		Set<Id> accIds = new Set<Id>();
		Set<Id> leadIds = new Set<Id>();
		
		for(String itemId: pIds){
			if (itemId.toLowerCase().startsWith(acctprefix.toLowerCase())){
				accIds.add(itemId);
			}
			else if (itemId.toLowerCase().startsWith(leadprefix.toLowerCase())){
				leadIds.add(itemId);
			}
			else{
				// not supported
			}
		}
		
		for(Account a: [SELECT Name, Type, LeadStatus__c, DisconnectDate__c, DispositionCode__c, Phone, NewMoverDate__c FROM Account WHERE Id IN :accIds]){
			resultMap.put(a.Id, new StreetSheetItem__c( Score__c = getAccountProspectScore(a) ));
		}
		
		for(Lead l: [SELECT isConverted, Type__c, DispositionDetail__c, Phone, DispositionCode__c, NewMoverDate__c FROM Lead WHERE Id IN :leadIds]){
			resultMap.put(l.Id, new StreetSheetItem__c( Score__c = getLeadProspectScore(l) ));
		}
		
		return resultMap;
	}
	
	public static Integer getAccountProspectScore(Account a){
		
		Date n90 = Date.today().addDays(-90);
		
		// Score 1
		if( a.Type == 'Discontinuance' && a.LeadStatus__c == 'Active' && a.DisconnectDate__c >= n90 && Utilities.isEmptyOrNull(a.DispositionCode__c) && a.Phone <> 'DO NOT CALL'){
			return 1;
		}
		
		// Score 2
		if( a.LeadStatus__c == 'Active' && a.NewMoverDate__c >= n90 && Utilities.isEmptyOrNull(a.DispositionCode__c) && a.Type == 'New Mover' && a.Phone <> 'DO NOT CALL' ){
			return 2;
		}
		
		// Score 3
		if ( a.LeadStatus__c == 'Active' && a.NewMoverDate__c < n90 && Utilities.isEmptyOrNull(a.DispositionCode__c) && a.Type == 'New Mover' && a.Phone <> 'DO NOT CALL'){
			return 3;
		}
		
		// Score 4
		if ( a.Type == 'Discontinuance' && a.LeadStatus__c == 'Active' && a.Phone <> 'DO NOT CALL' && Utilities.isEmptyOrNull(a.DispositionCode__c) ) {
			return 4;
		}
		
		return 99; //default
	}
	
	public static Integer getLeadProspectScore(Lead l){
		
		Date n90 = Date.today().addDays(-90);
		
		// Score 1
		if( l.isConverted == FALSE && l.Type__c == 'Pre Mover' && Utilities.isEmptyOrNull(l.DispositionDetail__c) && l.Phone <> 'DO NOT CALL' ){
			return 1;
		}
		
		// Score 2
		if( l.isConverted == FALSE && l.Type__c == 'New Mover' && l.NewMoverDate__c >= n90 && Utilities.isEmptyOrNull(l.DispositionDetail__c) && l.Phone <> 'DO NOT CALL' ){
			return 2;
		}
		
		// Score 3
		if( l.isConverted == FALSE && l.NewMoverDate__c < n90 && Utilities.isEmptyOrNull(l.DispositionDetail__c) && l.Phone <> 'DO NOT CALL' ){
			return 3;
		}
		
		// Score 4		
		if( l.isConverted == FALSE && l.Phone <> 'DO NOT CALL' && Utilities.isEmptyOrNull(l.DispositionCode__c) ){
			return 4;
		}
		
		return 99;
	}
	
	private static String deriveDefaultName() {
		DateTime currentTime = DateTime.now();
        return UserInfo.getName() + '-' + currentTime.format('MM/dd/yyyy HH:mm:ss');	
	}
	 

}