global class ProspectListsCtrl {
  
  global ProspectListsCtrl(){}
  
  @RemoteAction
  global static Boolean EmbedCustomBtn(){
	  String bUnitStr = ProspectListConfig__c.getInstance().Business_Unit__c;
      User uObj = [SELECT Id, Business_Unit__c FROM User WHERE Id = :UserInfo.getUserId()];//
	  if(!Utilities.isEmptyOrNull(bUnitStr) && !Utilities.isEmptyOrNull(uObj.Business_Unit__c) ) {
	    Set<String> sbu = new Set<String>();
	    sbu.addAll(bUnitStr.split(','));
	    if(sbu.contains(uObj.Business_Unit__c)){
	      return true;
	    } 
	  }    
      return false;
  }
  
  
}