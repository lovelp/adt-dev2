@isTest
private class StreetSheetManagerTest {
    
    private static String VALID_ID = '001T000000qShRf';
    private static String INVALID_ID = 'Invalid';
    private static String STREET_SHEET_Name = 'Unit Test Street Sheet';
    
    static testMethod void testCreateWithDefaultName() {
        
        User admin1 = TestHelperClass.createUser(100);
        User admin2 = TestHelperClass.createUser(200);
        System.runAs(admin1) {        
        	TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        
        Account a;
        System.runAs(admin2) {
        	a = TestHelperClass.createAccountData();
        }	
        
        Set<String> itemSet0 = new Set<String>();
        
        Set<String> itemSet1 = new Set<String>();
        itemSet1.add(a.Id);
        
        Set<String> itemSetInvalid = new Set<String>();
        itemSetInvalid.add(INVALID_ID);
        
        Test.startTest();
        System.runAs(admin2) {
        	try {
            	String ssId = StreetSheetManager.create(itemSet0);
        	} catch (DmlException dml) {
            	System.assert(false, 'Do not expect a DmlException (empty set): ' + dml.getMessage() + ' ' + dml.getStackTraceString());
        	} catch (Exception e) {
            	System.assert(false, 'Do not expect an Exception (empty set): ' + e.getMessage() + ' ' + e.getStackTraceString());
        	}
        
        	try {
            	String ssId = StreetSheetManager.create(itemSet1);
        	} catch (DmlException dml) {
            	System.assert(false, 'Do not expect a DmlException (set of 1 Id): ' + dml.getMessage() + ' ' + dml.getStackTraceString());
        	} catch (Exception e) {
            	System.assert(false, 'Do not expect an Exception (set of 1 Id): ' + e.getMessage() + ' ' + e.getStackTraceString());
        	} 
        
        	try {
            	String ssId = StreetSheetManager.create(itemSetInvalid);
            	System.assert(false, 'Expect an Exception (invalid Id)  but none occurred');
        	} catch (Exception e) {
            	// expect this
        	}
        }	  
        
        Test.stopTest();
        
    }
    
    static testMethod void testStreetSheetExpirationProcessor() {
    	
    	User admin1 = TestHelperClass.createUser(100);
        User admin2 = TestHelperClass.createUser(200);
        System.runAs(admin1) {        
        	TestHelperClass.createReferenceDataForTestClasses();
        	TestHelperClass.createReferenceUserDataForTestClasses();
        }
        
    	StreetSheet__c s1;
    	StreetSheet__c s2;
    	List<StreetSheet__c> slist;
    	System.runAs(admin2) {
        	s1 = new StreetSheet__c();
        	Integer daysToRetain = 30;
        	s1.LastModifiedDate = DateTime.now().addDays(-1 * daysToRetain - 1);
        	s1.CreatedDate = s1.LastModifiedDate.addDays(-1);
        	s2 = new StreetSheet__c();
        	s2.LastModifiedDate = DateTime.now().addDays(-1 * daysToRetain + 1);
        	s2.CreatedDate = s2.LastModifiedDate.addDays(-1);
        	slist = new List<StreetSheet__c>{s1, s2};
        	insert slist;
    	}	
        
        test.startTest();
            	StreetSheetExpirationBatchProcessor sep = new StreetSheetExpirationBatchProcessor();
            	sep.query += ' and ID IN (\'' + s1.Id + '\',\'' + s2.Id + '\') limit 200';
            	Database.executeBatch(sep);
        test.stopTest();
        
        List<StreetSheet__c> sconfirm = new List<StreetSheet__c>([select Id from StreetSheet__c where Id IN :slist]);
        system.assertEquals(1, sconfirm.size());
        system.assertEquals(s2.Id, sconfirm[0].Id);
    }
    
    static testMethod void testDeleteStreetSheetItems() {
        
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        	TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        Account a;

        System.runAs(current) {
        
            u = TestHelperClass.createSalesRepUser();
            a = TestHelperClass.createAccountData();
            a.OwnerId = u.Id;
            update a;
        
            StreetSheet__c ss = new StreetSheet__c();
            ss.CreatedById = u.Id;
            ss.Name = 'Unit Test Street Sheet';
        
            insert ss;
        
            StreetSheetItem__c ssi1 = new StreetSheetItem__c();
            ssi1.AccountID__c = a.Id;
            ssi1.CreatedById = u.Id;
            ssi1.StreetSheet__c = ss.Id;
        
            insert ssi1;
        
            StreetSheetItem__c ssi2 = new StreetSheetItem__c();
            ssi2.AccountID__c = a.Id;
            ssi2.CreatedById = u.Id;
            ssi2.StreetSheet__c = ss.Id;
        
            insert ssi2; 
            
        }
        
        Map<String, Set<String>> ownerToAccountsMap = new Map <String, Set<String>>();
        Set<String> accountSet = new Set<String>();
        accountSet.add(a.Id);
        ownerToAccountsMap.put(u.id, accountSet);
        Set<String> accountSet2 = new Set<String>();
        accountSet2.addAll(accountSet);
        accountSet2.add(VALID_ID);
        ownerToAccountsMap.put(VALID_ID, accountSet2);
        
        Test.startTest();
        
        StreetSheetManager.deleteStreetSheetItems(null);
        
        StreetSheetManager.deleteStreetSheetItems(new Map <String, Set<String>>());
        
        StreetSheetManager.deleteStreetSheetItems(ownerToAccountsMap);
        
        
        Test.stopTest();
        
        List<StreetSheet__c> listSheets = [select Id from StreetSheet__c where CreatedById = : u.Id];
        System.assertEquals(1, listSheets.size(), 'Should 1 Street Sheet record');
        
        List<StreetSheetItem__c> listSheetItems = [select Id from StreetSheetItem__c where AccountID__c = : a.Id];
        
        System.assertEquals(2, listSheetItems.size(), 'Should be 2 Street Sheet Item records');
    }
    
    static testMethod void testdeleteAllProspectListItems()
    {
        set<String> OwnershipChangedLeadIds = new set<String>();

		// MT - 2013/7/26 Added to allow LeadTriggerHelper to function
        User admin = TestHelperClass.createAdminUser();
        TestHelperClass.createSalesRepUser(); 
        System.runAs(admin) {        
        	TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
        
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        System.runAs(current) {
	
	        //Test.startTest();
	        
	        Postal_Codes__c p = new  Postal_Codes__c();
	        p.Name = '22102';
	        p.BusinessID__c = '1100';
	        insert p;
	        
	        Lead L1 = new Lead();                       
	        L1.SiteStreet__c = '2251 Pimmit Drive';
	        L1.SiteCity__c = 'Falls Church';        
	        L1.SiteStateProvince__c = 'VA';
	        L1.SitePostalCode__c = '22102';
	        L1.SiteCountryCode__c = 'US';
	        L1.DispositionCode__c = 'Phone - Left Msg';
	        L1.LastName = 'Bruce Wayne';
	        L1.Channel__c = 'Custom Home Sales';
	        L1.Business_Id__c = '1100 - Residential';
	        L1.Company = 'Test Company';
	        insert L1;
	        
	        StreetSheet__c ss = new StreetSheet__c();
	        ss.Name = 'Unit Test Street Sheet';     
	        insert ss;
	    
	        StreetSheetItem__c ssi1 = new StreetSheetItem__c();
	        ssi1.LeadID__c = L1.Id;
	        ssi1.StreetSheet__c = ss.Id;        
	        insert ssi1;
	        List<Lead> lst = new List<Lead>();
	        lst.add(L1);
	        List<SObject> objList = lst;
	        
	        OwnershipChangedLeadIds.add(L1.Id);
	        StreetSheetManager.getStreetSheetItemScore(objList);
	        StreetSheetManager.deleteExpiredSheets();
	        StreetSheetManager.deleteItems(OwnershipChangedLeadIds);
	        //Test.stopTest();
	        
	        List<StreetSheetItem__c> listSheetItems = [select Id from StreetSheetItem__c where AccountID__c = : L1.Id];
	        
	        System.assertEquals(0, listSheetItems.size(), 'Should be no Street Sheet Item records');
	
        }
    }
}