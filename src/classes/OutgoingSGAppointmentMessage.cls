/************************************* MODIFICATION LOG ********************************************************************************************
* OutgoingSGAppointmentMessage
*
* DESCRIPTION : Creates and processes setSGAppointment messages for Telemar Interface
*				Extends OutgoingMessage with message specific logic
*               Delegates to ADTSalesMobilityFacade (wsdl2apex code)
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					1/19/2012				- Original Version
*
*													
*/

public with sharing class OutgoingSGAppointmentMessage extends OutgoingMessage {
	
	public Lead l {get;set;}
	public Account a {get;set;}
	public Event e {get;set;}
	
	private ADTSalesMobilityFacade.setSGAppointmentRequest_element request;
	private ADTSalesMobilityFacade.setSGAppointmentResponse_element response;
	private ADTSalesMobilityFacade.PreSale_element presale;
	private ADTSalesMobilityFacade.Appointment_element appt;
	
	public String TelemarAccountNumber {get {return tan;}}
	
	private String tan;
	
	public OutgoingSGAppointmentMessage()
	{
		this.ServiceName = 'setSGAppointment';
	}
	
	public override String createRequest()
	{
		
		request = new ADTSalesMobilityFacade.setSGAppointmentRequest_element();
		
		User userPerformingAction = [Select Id, EmployeeNumber__c, TimeZoneSidKey from User where ID = :UserInfo.getUserId()];
		
		User userBeingScheduled;
		String timeZoneOffset = '';
		if (userPerformingAction != null && userPerformingAction.Id == e.OwnerId) {
			userBeingScheduled = userPerformingAction;
			// get the current user's offset as of the time of the event
			// this handles time changes between daylight savings and standard time
			timeZoneOffset = e.StartDateTime.format('Z');
		} else {
			userBeingScheduled = [Select Id, EmployeeNumber__c, TimeZoneSidKey from User where ID = :e.OwnerId];

			if (userBeingScheduled.TimeZoneSidKey == userPerformingAction.TimeZoneSidKey) {
				// no difference in time zone so get the current user's offset as of the time of the event
				// this handles time changes between daylight savings and standard time
				timeZoneOffset = e.StartDateTime.format('Z');
			} else {
				// otherwise, use the user's current time zone identifier and the scheduled date to derive the offset
				timeZoneOffset = EventUtilities.deriveUserTimeZoneOffsetForDate(e.StartDateTime, userBeingScheduled.TimeZoneSidKey);
			}
			
		}
		
		
		request.MessageID = getMessageId(userPerformingAction.Id);
		request.TransactionType = this.TransactionType;
		// intentionally using the formula field OutboundTelemarAccountNumber__c rather than the TelemarAccountNumber__c
		// since the schedule action may be against a former RIF account that has a RifTelemarAccountNumber__c populated
		if (a != null && a.OutboundTelemarAccountNumber__c != null) {
			request.TelemarAccountNumber = a.OutboundTelemarAccountNumber__c;
		}
		else if (l != null && l.TelemarAccountNumber__c != null) {
			request.TelemarAccountNumber = l.TelemarAccountNumber__c;
		}
		request.HREmployeeID = (userPerformingAction.EmployeeNumber__c == null ? '' : userPerformingAction.EmployeeNumber__c);
		
		if (TransactionType == IntegrationConstants.TRANSACTION_NEW) 
		{
			if (a != null) {
				buildPresaleFromAccount(a);
			} else if (l != null) {
				buildPresaleFromLead(l);
			}
		}
		else
		{
			presale = null;
		}
		
		request.PreSale = presale;
		
		appt = new ADTSalesMobilityFacade.Appointment_element();
		
		appt.OriginalTelemarScheduleID = (e.ScheduleID__c == null ? '' : e.ScheduleID__c);
		
		if (TransactionType != IntegrationConstants.TRANSACTION_CANCEL)
		{
			appt.AppointmentStartDateTime = e.StartDateTime;
			appt.AppointmentOffsetToGMT = timeZoneOffset;
			Double minutes = (e.EndDateTime.getTime() - e.StartDateTime.getTime()) / 1000 / 60;
			appt.Duration = String.valueof((Integer)minutes);
			appt.TaskCode = (e.TaskCode__c == null ? '' : e.TaskCode__c);
			appt.Notes = truncateTextArea(e.Description, 300);
			appt.HREmployeeID = (userBeingScheduled.EmployeeNumber__c == null ? '' : userBeingScheduled.EmployeeNumber__c);
		}
		request.Appointment = appt;
		
		return system.Json.serialize(request);
	}
	
	public override void processResponse()
	{
		if (request == null && this.ServiceMessage != null) {
			request = (ADTSalesMobilityFacade.setSGAppointmentRequest_element)system.Json.deserialize(this.ServiceMessage, system.type.forName('ADTSalesMobilityFacade.setSGAppointmentRequest_element'));
		}
		else if (request == null) {
			createRequest();
		}
			
		ADTSalesMobilityFacade.SalesMobilitySOAP soap = new ADTSalesMobilityFacade.SalesMobilitySOAP();
		response = soap.setSGAppointment(
			request.MessageID,  
			request.TransactionType, 
			request.TelemarAccountNumber, 
			request.HREmployeeID, 
			request.PreSale, 
			request.Appointment
		);
		
		mid = response.MessageID;
		mstatus = response.MessageStatus;
		err = response.Error;
		sid = response.TelemarScheduleID;
		tan = response.TelemarAccountNumber;
		
		System.debug('$$$ mid: ' + mid);
		System.debug('$$$ mstatus: ' + mstatus);
		System.debug('$$$ err: ' + err);
		System.debug('$$$ sid: ' + sid);
		System.debug('$$$ tan: ' + tan);
		
		
		if (mstatus != IntegrationConstants.MESSAGE_STATUS_OK && mstatus != IntegrationConstants.MESSAGE_STATUS_FAIL) {
			throw new IntegrationException('Invalid response from Telemar: MessageStatus of ' + mstatus + ' is invalid');
		}
		
		if (mstatus == IntegrationConstants.MESSAGE_STATUS_FAIL) {
			throw new IntegrationException (err);
		}
		
	}
	
	private void buildPresaleFromLead(Lead l) {
		presale = new ADTSalesMobilityFacade.PreSale_element();
		presale.FirstName = truncateFirstName(l.FirstName);
		presale.LastName = truncateLastName(l.LastName);
		if (l.Business_Id__c != null && l.Business_Id__c.contains('1200')) {
			presale.BusinessName = truncateBusinessName(l.Company);
		}
		
		//each address line is 30 chars in Telemar
		//divide first 120 chars of Site street into 4 address lines
		String[] alines = getAddressLines(l.SiteStreet__c);
		
		presale.AddressLine1 = alines[0];
		presale.AddressLine2 = alines[1];
		presale.AddressLine3 = alines[2];
		presale.AddressLine4 = alines[3];
		
		presale.City = truncateSiteCity(l.SiteCity__c);
		presale.State = l.SiteStateProvince__c;
		presale.PostalCode = truncatePostalCode(l.SitePostalCode__c, l.SitePostalCodeAddOn__c);
		presale.CountryCode = l.SiteCountryCode__c;
		presale.PhoneNumber1 = truncatePhone(l.Phone);
		presale.Email = l.Email;
		presale.Comments = null;
		presale.QueriedSource = null;
		presale.ReferredBy = TextUtilities.removeDiacriticalMarks(l.ReferredBy__c);
		presale.Channel = l.Channel__c;
		
	}
	
	private void buildPresaleFromAccount(Account a) {
		presale = new ADTSalesMobilityFacade.PreSale_element();
		presale.FirstName = truncateFirstName(a.FirstName__c);
		presale.LastName = truncateLastName(a.LastName__c);
		if (a.Business_Id__c != null && a.Business_Id__c.contains('1200')) {
			presale.BusinessName = truncateBusinessName(a.Name);
		}
		
		//each address line is 30 chars in Telemar
		//divide first 120 chars of Site street into 4 address lines
		String[] alines = getAddressLines(a.SiteStreet__c);
		
		presale.AddressLine1 = alines[0];
		presale.AddressLine2 = alines[1];
		presale.AddressLine3 = alines[2];
		presale.AddressLine4 = alines[3];
		
		presale.City = truncateSiteCity(a.SiteCity__c);
		presale.State = a.SiteState__c;
		presale.PostalCode = truncatePostalCode(a.SitePostalCode__c, a.SitePostalCodeAddOn__c);
		presale.CountryCode = a.SiteCountryCode__c;
		presale.PhoneNumber1 = truncatePhone(a.Phone);
		presale.Email = a.Email__c;
		presale.Comments = null;
		presale.QueriedSource = null;
		presale.ReferredBy = TextUtilities.removeDiacriticalMarks(a.ReferredBy__c);
		presale.Channel = a.Channel__c;
		
	}
}