/**
    * This class contains unit tests for validating the behavior of Apex classes
    * and triggers.
    *
    * Unit tests are class methods that verify whether a particular piece
    * of code is working properly. Unit test methods take no arguments,
    * commit no data to the database, and are flagged with the testMethod
    * keyword in the method definition.
    *
    * All test methods in an organization are executed whenever Apex code is deployed
    * to a production organization to confirm correctness, ensure code
    * coverage, and prevent regressions. All Apex classes are
    * required to have at least 75% code coverage in order to be deployed
    * to a production organization. In addition, all triggers must have some code coverage.
    * 
    * The @isTest class annotation indicates this class only contains test
    * methods. Classes defined with the @isTest annotation do not count against
    * the organization size limit for all Apex scripts.
    *
    * See the Apex Language Reference for more information about Testing and Code Coverage.
    */
    @isTest
    private class AccountAffiliationControllerTest  {

    public static Account acc;

    public static testMethod void myUnitTest() {
        // TO DO: implement unit test
    
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;
    
        System.runAs(current) { 
        u = TestHelperClass.createSalesRepUser();
        u.Phone = '305231123';
        update u;
    
        //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
    
        insert addr;
    
        acc = TestHelperClass.createAccountData(addr, true);
        acc.FirstName__c = 'TestFName';
        acc.LastName__c = 'TestLName';
        acc.Name = 'TestFName TestLName';
        acc.OwnerId = u.Id;
        update acc;
    
        aff = new Affiliate__c();
        aff.Affiliate_Description__c = 'USAA Test';
        aff.Relevance__c = 1;
        aff.Name = 'USAA';
        aff.Member_Required__c = true;
        aff.Member_validated_Field_Sales__c = true;
        insert aff; 
     
        
        Account_Affiliation__c accaff = new Account_Affiliation__c();
        accaff.Affiliate__c = aff.id;
        accaff.Active__c = true;
        accaff.Member__c = acc.id;
        accaff.MemberNumber__c = '';
        accaff.Military_Rank__c = 'CAPTAIN';
        insert accaff;
    
        // Create Test
    
        Test.setCurrentPage(Page.AccountAffiliation);
        ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(acc);
    
        // Undefined view
        AccountAffiliationController  accAffCtrl = new AccountAffiliationController (sc1);
    
        ApexPages.currentPage().getParameters().put('CF00N18000000OdEH', 'TestFName TestLName');
        ApexPages.currentPage().getParameters().put('CF00N18000000OdEH_lkid', acc.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/'+acc.Id);
    
        // Create view
        accAffCtrl = new AccountAffiliationController (sc1);
        List<SelectOption> sO1 = accAffCtrl.AffiliateList;
        Boolean b1 = accAffCtrl.renderSaveNew;
        accAffCtrl.AccountAffiliation = aff.Id;
        accAffCtrl.accAff.MemberNumber__c = '1234122';
        PageReference pref1 = accAffCtrl.saveNew();
    
    
        // Update Test
    
        Test.setCurrentPage(Page.AccountAffiliation);
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(accaff);
    
        ApexPages.currentPage().getParameters().put('Id', accaff.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/'+acc.Id);
    
    
        // Edit view
        AccountAffiliationController  accAffCtrl1 = new AccountAffiliationController (sc2);
        List<SelectOption> sO2 = accAffCtrl1.AffiliateList;
        accAffCtrl1.accAff.MemberNumber__c = '';
        accAffCtrl1.accAff.Member__c = acc.id;
        accAffCtrl1.updateRecord();
        accAffCtrl1.checkifUSAA();
        // accAffCtrl1.validateMemberID();
        // accAffCtrl.createAffValRec('Hello');
        PageReference pref2 = accAffCtrl1.save();
        PageReference pref3 = accAffCtrl1.cancel();
        
        }
    }
    

    public static testMethod void myUnitTest_TCS() {
        // TO DO: implement unit test
    
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        TestHelperClass.createReferenceDataForTestClasses();
        }
    
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;
    
        System.runAs(current) { 
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            update u;
        
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
            insert addr;
        
            acc = TestHelperClass.createAccountData(addr, true);
            acc.FirstName__c = 'TestFName';
            acc.LastName__c = 'TestLName';
            acc.Name = 'TestFName TestLName';
            acc.OwnerId = u.Id;
            update acc;
        
            aff = new Affiliate__c();
            aff.Affiliate_Description__c = 'USAA Test';
            aff.Relevance__c = 1;
            aff.Member_Required__c = false;
            aff.Member_validated_Field_Sales__c = true;
            aff.Name = 'USAA';
            insert aff; 
        
            Account_Affiliation__c accaff = new Account_Affiliation__c();
            accaff.Affiliate__c = aff.id;
            accaff.Active__c = true;
            accaff.Member__c = acc.id;
            accaff.MemberNumber__c = 'test';
            accaff.Military_Rank__c = 'CAPTAIN';
            insert accaff;
            
            // Create Test
        
            Test.setCurrentPage(Page.AccountAffiliation);
            ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(acc);
        
            // Undefined view
            AccountAffiliationController  accAffCtrl = new AccountAffiliationController (sc1);
        
            ApexPages.currentPage().getParameters().put('CF00N18000000OdEH', 'TestFName TestLName');
            ApexPages.currentPage().getParameters().put('CF00N18000000OdEH_lkid', acc.Id);
            ApexPages.currentPage().getParameters().put('retURL', '/'+acc.Id);
        
            // Create view
            accAffCtrl = new AccountAffiliationController (sc1);
            List<SelectOption> sO1 = accAffCtrl.AffiliateList;
            Boolean b1 = accAffCtrl.renderSaveNew;
            accAffCtrl.AccountAffiliation = aff.Id;
            accAffCtrl.accAff.MemberNumber__c = '1234122';
            PageReference pref1 = accAffCtrl.saveNew();
        
        
            // Update Test
        
            Test.setCurrentPage(Page.AccountAffiliation);
            ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(accaff);
        
            ApexPages.currentPage().getParameters().put('Id', accaff.Id);
            ApexPages.currentPage().getParameters().put('retURL', '/'+acc.Id);
        
            List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>();
            AFF_Validation__c affVal = new AFF_Validation__c();
            affVal.AccountID__c = acc.Id;
            affVal.UserID__c = admin.Id;
            affVal.AffiliateID__c = accaff.Id;
            affValLst.add(affVal);
        
        
            ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
            resale.name = 'USAAAttempts';
            resale.value__c = '4';
            insert resale; 
        
            List<ResaleGlobalVariables__c> Globallist = new List<ResaleGlobalVariables__c >();
            ResaleGlobalVariables__c resale1 = new ResaleGlobalVariables__c();
            resale1.name = 'USAATimeout';
            resale1.value__c = '8';
            insert resale1;
        
        
        
            // Edit view
            AccountAffiliationController  accAffCtrl1 = new AccountAffiliationController (sc2);
            List<SelectOption> sO2 = accAffCtrl1.AffiliateList;
            accAffCtrl1.getUSAAAttemptParameters();
            accAffCtrl1.accAff.MemberNumber__c = '@111';
            accAffCtrl.accAff.Member__c = acc.id;
            accAffCtrl.updateRecord();
            PageReference pref2 = accAffCtrl1.save();
            PageReference pref3 = accAffCtrl1.cancel();
    
        }
    }
    
    public static testMethod void myUnitTest_TCS1() {
        // TO DO: implement unit test
    
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        TestHelperClass.createReferenceDataForTestClasses();
        }
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('USAAResponseCalloutMock');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;
    
        System.runAs(current) { 
        u = TestHelperClass.createSalesRepUser();
        u.Phone = '305231123';
        update u;
    
        //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
    
        insert addr;
    
        acc = TestHelperClass.createAccountData(addr, true);
        acc.FirstName__c = 'TestFName';
        acc.LastName__c = 'TestLName';
        acc.Name = 'TestFName TestLName';
        acc.OwnerId = u.Id;
        update acc;
    
        aff = new Affiliate__c();
        aff.Affiliate_Description__c = 'USAA Test';
        aff.Relevance__c = 1;
        aff.Name = 'USAA';
        aff.Member_Required__c = true;
        aff.Member_validated_Field_Sales__c = false;
        insert aff; 
    
        Account_Affiliation__c accaff = new Account_Affiliation__c();
        accaff.Affiliate__c = aff.id;
        accaff.Active__c = true;
        accaff.Member__c = acc.id;
        accaff.MemberNumber__c = 'test';
        accaff.Military_Rank__c = 'CAPTAIN';
        insert accaff;
    
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '0';
        insert resale; 
    
        List<ResaleGlobalVariables__c> Globallist = new List<ResaleGlobalVariables__c >();
        ResaleGlobalVariables__c resale1 = new ResaleGlobalVariables__c();
        resale1.name = 'USAATimeout';
        resale1.value__c = '1';
        insert resale1;
    
        // Create Test    
        Test.setCurrentPage(Page.AccountAffiliation);
        ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(acc);
    
        // Undefined view
        AccountAffiliationController  accAffCtrl = new AccountAffiliationController (sc1);
    
        ApexPages.currentPage().getParameters().put('CF00N18000000OdEH', 'TestFName TestLName');
        ApexPages.currentPage().getParameters().put('CF00N18000000OdEH_lkid', acc.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/'+acc.Id);
    
        // Create view
    
        accAffCtrl = new AccountAffiliationController (sc1);
        List<SelectOption> sO1 = accAffCtrl.AffiliateList;
        Boolean b1 = accAffCtrl.renderSaveNew;
        accAffCtrl.AccountAffiliation = aff.Id;
        accAffCtrl.accAff.MemberNumber__c = '1234122';
        PageReference pref1 = accAffCtrl.saveNew();
    
    
        // Update Test
    
        Test.setCurrentPage(Page.AccountAffiliation);
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(accaff);
    
        ApexPages.currentPage().getParameters().put('Id', accaff.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/'+acc.Id);
    
        List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>();
        AFF_Validation__c affVal = new AFF_Validation__c();
        affVal.AccountID__c = acc.Id;
        affVal.UserID__c = UserInfo.getuserid();
        affVal.AffiliateID__c = accaff.Id;
        affVal.Failed_Attempts__c = 1;
        affVal.Agent_Input__c = 'Attempt 1 with input :'+'Hello';
        affValLst.add(affVal);
        
        CollaborationGroup postGroup = new CollaborationGroup(Name = 'Post', OwnerId = UserInfo.getUserId(), CollaborationType = 'Public');
        insert postGroup;
        
        FeedItem post = new FeedItem();
        post.parentId = postGroup.Id;
        post.body = 'This Works';
        insert post;
        // Edit view
    
        AccountAffiliationController  accAffCtrl1 = new AccountAffiliationController (sc2);
        List<SelectOption> sO2 = accAffCtrl1.AffiliateList;
        accAffCtrl1.getUSAAAttemptParameters();
        accAffCtrl1.accAff.MemberNumber__c = '@111';
        accAffCtrl.accAff.Member__c = acc.id;
        accAffCtrl1.createAffValRec('Hello');
        accAffCtrl1.addAFFValRec('Hello');
        accAffCtrl1.resetAffValRec();
        //accAffCtrl1.getElapsedTimeInMins();
        accAffCtrl1.callUSAA();
        accAffCtrl1.updateRecord();
        
        // accAffCtrl.getLogoURL(post.id);
        PageReference pref2 = accAffCtrl1.save();
        PageReference pref3 = accAffCtrl1.cancel();
    
        }         
    }
    
    public static testMethod void myUnitTest2() {
        // TO DO: implement unit test
    
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
            TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;

        System.runAs(current) { 
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            update u;
        
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
            insert addr;
        
            acc = TestHelperClass.createAccountData(addr, true);
            acc.FirstName__c = 'TestFName';
            acc.LastName__c = 'TestLName';
            acc.Name = 'TestFName TestLName';
            acc.OwnerId = u.Id;
            update acc;
        
            aff = new Affiliate__c();
            aff.Affiliate_Description__c = 'USAA Test';
            aff.Relevance__c = 1;
            aff.Name = 'USAA';
            aff.Member_Required__c = true;
            aff.Member_validated_Field_Sales__c = true;
            insert aff; 
         
            
            Account_Affiliation__c accaff = new Account_Affiliation__c();
            accaff.Affiliate__c = aff.id;
            //accaff.Active__c = true;
            accaff.Member__c = acc.id;
            accaff.MemberNumber__c = '';
            accaff.Military_Rank__c = 'CAPTAIN';
            insert accaff;
            
          
            // Update Test
        
            Test.setCurrentPage(Page.AccountAffiliation);
            ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(accaff);
            
            
            ApexPages.currentPage().getParameters().put('Id', accaff.Id);
            ApexPages.currentPage().getParameters().put('retURL', '/'+acc.Id);
        
        
            // Edit view
            AccountAffiliationController  accAffCtrl1 = new AccountAffiliationController (sc2);
            List<SelectOption> sO2 = accAffCtrl1.AffiliateList;
            accAffCtrl1.accAff.MemberNumber__c = '123456';
            accAffCtrl1.accAff.Member__c = acc.id;
            accAffCtrl1.updateRecord();
            accAffCtrl1.checkifUSAA();
            // accAffCtrl1.getElapsedTimeInMins();
            // accAffCtrl1.validateMemberID();
            // accAffCtrl.createAffValRec('Hello');
            PageReference pref2 = accAffCtrl1.save();
            PageReference pref3 = accAffCtrl1.cancel();
    
        }
    }
}