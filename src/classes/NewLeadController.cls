/************************************* MODIFICATION LOG ********************************************************************************************
*
* DESCRIPTION : NewLeadController.cls is controller for NewLead.page. This contoller has init method that sets default values when User creates new Leads from Salesforce.com UI 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                TICKET              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            04/01/2011                              - Origininal Version
* Mike Tuckman                  03/22/2014                              - (Hermes) Added extensive logis to enter and valdiate address fields   
* Magdiel Herrera               9/15/2015                               - Added support for NSC phone sales reps                            
* Magdiel Herrera               11/16/2015                              - Added support for BestBuy sales reps
* Prashant                      03/20/2018          HRM-475
* Prashant                      04/20/2018          HRM-6915
* Viraj                         09/09/2018          HRM-10096           - Added check for special characters
*/

public with sharing class NewLeadController 
{
    public String recordType {get;set;}
    public Lead ld {get;set;}
    public String allowOverride {get;set;}
    public String title {get;set;}
    public String saveButtonTxt {get;set;}
    public Boolean isAppointmentRequired {get;set;}
    public Boolean runConversion {get;set;}
    private Boolean confirm = false;
    private User current;
    private PageReference createLeadPage;
    private Boolean didOverride;
    private ID leadID;
    private Map<String, String> NSCLeadRedirectMap;

    public NewLeadController(ApexPages.StandardController stdController)
    {
        recordType = ApexPages.currentPage().getParameters().get('RecordType');
    }
    
    public PageReference init()
    {
        
        NSCLeadRedirectMap = new Map<String, String>();
        NSCLeadRedirectMap.putAll(ApexPages.currentPage().getParameters());
        isAppointmentRequired = true;
        runConversion = false;
        
        current = [select Id, Address_Validation__c,Profile_Type__c, Profile.Name from User where Id=:UserInfo.getUserId()];
        System.debug('Address Validation:' + current.Address_Validation__c);
        
        // Profile dependent logic
        if( current.Profile_Type__c == 'NSC Agent' ){
            return NSCServiceLayout();
        }
        else if ( current.Profile.Name == 'Best Buy Sales Representative' ){
            isAppointmentRequired = false;
        }
        
        if (!current.Address_Validation__c)
            return createLead();
            
        leadID = ApexPages.currentPage().getParameters().get('id');
        
        if (leadID != null){
            //HRM-6915 - Added site street3 in query.
            ld = [select id, FirstName, TelemarLeadSource__c, LastName, Company, Channel__c, SiteStreet__c, SiteStreet2__c, SiteStreet3__c, SiteCity__c, SiteStateProvince__c, SitePostalCode__c, SitePostalCodeAddOn__c, LeadManagementId__c,
                LeadSource, QueriedSource__c, GenericMedia__c, SiteStreetName__c, SiteStreetNumber__c, SiteValidationMsg__c, SiteValidated__c, SiteEdited__c, SiteCounty__c, IncomingLatitude__c, IncomingLongitude__c from Lead where id = :leadId];
                title = 'Lead Address Edit and Validation';
        }
        else {
            ld = New Lead();
            ld.Company = 'N/A';
            if (recordType != null)
                ld.RecordTypeId = recordType;
            title = 'New Lead Address Entry and Validation';
        }
        
        allowOverride = 'false';
        didOverride = false;
        saveButtonTxt = 'Check Address';
        
        return null;
    
    }
    
    public PageReference doOverride(){
        didOverride = true;
        return doSave();
    }
        
    
    public PageReference doSave(){
        //Added by TCS for HRM 475
        Map<String, String> alphaconver = new Map<String, String>();
        for(AlphaConversion__c ac: [SELECT id, Name, AlphaCharValue__c FROM AlphaConversion__c]){
            alphaconver.put(ac.name, ac.AlphaCharValue__c);
        }

        // Added by TCS for 475
        if(System.Label.SwitchForTranslation == 'True'){
            if ( !Utilities.isEmptyOrNull(ld.FirstName) )
               ld.FirstName = Utilities.NormalizeString( ld.FirstName, alphaconver );
            if ( !Utilities.isEmptyOrNull(ld.LastName) )
               ld.LastName = Utilities.NormalizeString( ld.LastName, alphaconver );
            if ( !Utilities.isEmptyOrNull(ld.Company) && ld.Company != 'N/A')
               ld.Company = Utilities.NormalizeString( ld.Company, alphaconver );   
            if ( !Utilities.isEmptyOrNull(ld.SiteCity__c) )
               ld.SiteCity__c= Utilities.NormalizeString( ld.SiteCity__c, alphaconver);
            if ( !Utilities.isEmptyOrNull(ld.SiteStreet__c) )
               ld.SiteStreet__c = Utilities.NormalizeString( ld.SiteStreet__c, alphaconver );
            if ( !Utilities.isEmptyOrNull(ld.SiteStreet2__c) )
               ld.SiteStreet2__c = Utilities.NormalizeString( ld.SiteStreet2__c, alphaconver );
        }
        
        //HRM-10096 -  Check for specialcharacter on Firstname and Lastname
        
        if(Label.SpecialCharactersChannels !='stop' && Label.SpecialCharactersChannels.split(';').contains(ld.Channel__c) && 
           (Utilities.containsSpecialChar(ld.FirstName) || Utilities.containsSpecialChar(ld.LastName))){
           ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMessages__c.getInstance('AccountNameSpecialChar').Message__c));
           return null;
        }
        
        MelissaDataAPI.MelissaDataResults mdr = MelissaDataAPI.validateAddr(ld.SiteStreet__c, ld.SiteStreet2__c, ld.SiteCity__c, ld.SiteStateProvince__c, ld.SitePostalCode__c);
        
        Boolean error = false;
        runConversion = false;
        allowOverride = 'false';
        
        //Added by TCS for HRM 6311
        setCHSOnChannel();
        Channels.setBusinessId(ld);
        //END of 6311
        
        if (mdr == null) return null;
        
        for (String code : mdr.ResultCodes){
            if (code.startsWith('AE')){
                error = true;
                confirm = false;
                allowOverride = 'true';
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, code));
            }
            else if (code.startsWith('AC'))
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, code));
            else if (code.startsWith('AS')) {
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, code));
            }
        }
         //Modified by TCS HRM 4984 -- Increased Length to 50 from 30
        if ((ld.SiteStreet__c != null && ld.SiteStreet__c.length() > Integer.valueOf(ResaleGlobalVariables__c.getinstance('MMBAddressLength').value__c)) || (ld.SiteStreet2__c != null && ld.SiteStreet2__c.length() > Integer.valueOf(ResaleGlobalVariables__c.getinstance('MMBAddressLength').value__c))) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Address Line 1 or 2 is greater than '+Integer.valueOf(ResaleGlobalVariables__c.getinstance('MMBAddressLength').value__c)+' characters. Please shorten and correct'));
            error = true;
        }


        if (!error)
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Hit Save again to confirm.'));
 
        
        ld.SiteStreetName__c = mdr.StreetName;
        ld.SiteStreetNumber__c = mdr.StreetNumber;
        ld.SiteEdited__c = true;

        if (((!error && confirm) || (error && didOverride)) && ld.SiteStreet__c == mdr.Addr1 && ld.SiteCity__c == mdr.City && ld.SiteStateProvince__c == mdr.State && ld.SitePostalCode__c == mdr.Zip){
            ld.SiteValidated__c = true;
            if (error && didOverride) {
                ld.SiteValidationMsg__c = 'User Override';
                for (String code: mdr.ResultCodes) {
                    ld.SiteValidationMsg__c += ':' + code.substring(0,4);
                }
            }
            if (leadID != null)
                update ld;
            else
                insert ld;
            return createLead();
        }
        
        if (!error) {
            confirm = true;
            saveButtonTxt = 'Save';
        }
        else saveButtonTxt = 'Check Address';

        ld.SiteStreet__c = mdr.Addr1;
        ld.SiteStreet2__c = mdr.Addr2;
        
        if (mdr.Suite != '')
            ld.SiteStreet2__c += ' ' + mdr.Suite;

        ld.SiteCity__c = mdr.City;
        ld.SiteStateProvince__c = mdr.State;
        ld.SitePostalCode__c = mdr.Zip;
        ld.SitePostalCodeAddOn__c = mdr.Plus4;
        ld.SiteCounty__c = mdr.County;
        ld.IncomingLatitude__c = decimal.valueOf(mdr.Lat);
        ld.IncomingLongitude__c = decimal.valueOf(mdr.Lon);
        
        return null;
    
    } 
    
    public PageReference NSCServiceLayout(){
        if( NSCLeadRedirectMap.containsKey('dnis') ){
            createLeadPage = new PageReference('/apex/NSCLeadCapture');
        }
        else{
            createLeadPage = new PageReference('/apex/NSCEntry');
        }        
        createLeadPage.getParameters().putAll(NSCLeadRedirectMap);        
        //createLeadPage.getParameters().put('nooverride', '1');  
        //createLeadPage.getParameters().put('retURL', '/00Q/o');
        //createLeadPage.getParameters().put('RecordType', recordType);    
        //createLeadPage.getParameters().put('ent', 'Lead');  
        //createLeadPage.getParameters().put('cdId','a11e0000001q7wD');    
        return createLeadPage;      
    } 
    
    public PageReference createLead()
    {
        // auto-convert users
        if( !isAppointmentRequired ){
            runConversion = true;
            //return convertValidatedLead( ld );
        }
        else{
            if (leadID != null){
                createLeadPage = new PageReference('/' + ld.ID);
                return createLeadPage; 
            }
            if (current.Address_Validation__c) {
                createLeadPage = new PageReference('/' + ld.ID + '/e');
            } 
            else {
                createLeadPage = new PageReference('/00Q/e'); 
                createLeadPage.getParameters().put('lea3','N/A');
            }
            createLeadPage.getParameters().put('nooverride', '1');  
            createLeadPage.getParameters().put('retURL', '/00Q/o');
            createLeadPage.getParameters().put('RecordType', recordType);    
            createLeadPage.getParameters().put('ent', 'Lead');      
        }
        return createLeadPage;      
    } 
    
    public PageReference ConvertLead(){
        return convertValidatedLead(ld);
    }
    
    /**
     *  Lead conversion with callout to Telemar for a Telemar Account Number
     *  @param Lead
     *  @return PageReference     
     */
    private PageReference convertValidatedLead(Lead l){
        Map<String, ErrorMessages__c> errormap = ErrorMessages__c.getAll();
        try {
            TelemarGateway.AccountRequest ar = new TelemarGateway.AccountRequest();
            String regexNumeric = '[^a-zA-Z0-9]';
            String dnis = ResaleGlobalVariables__c.getinstance('BestBuyDNIS').value__c;//'800BESTADT';
            ar.Dnis = dnis;
            ar.Promotion = '';
            ar.Submitter = UserInfo.getUserId();
            l.LeadSource = 'INSTORE';
            l.TelemarLeadSource__c = 'INSTORE';
            l.QueriedSource__c = 'BEST BUY';
            l.GenericMedia__c = 'BBY';
            TelemarGateway.ScheduleResult res = TelemarGateway.createAccount(l, ar);    
            if (res == null) {
                //get error map
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, errormap.get('EM_Telemar').Message__c));
                return null;
            }
            if (res.AccountNumber != null) {
                l.TelemarAccountNumber__c = (res.AccountNumber.length() > 10 ? res.AccountNumber.substring(0,10) : res.AccountNumber);
                upsert l;
            }
            
            //convert lead
            LeadConversion lc;
            if (res.AccountNumber != null){ //received telemar account number
                lc = new LeadConversion(l.Id, l.TelemarAccountNumber__c);
            }
            else{
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Telemar account number was not received.'));
                return null;
            }
            Boolean success = lc.convertLead();
            if (success) {
                // Default account values
                Account a = [SELECT TelemarLeadSource__c, QueriedSource__c, GenericMedia__c, MMBLookup__c, MMBOrderType__c FROM Account WHERE Id = :lc.AccountId ]; 
                if( Utilities.isEmptyOrNull(a.TelemarLeadSource__c) ){
                    a.TelemarLeadSource__c = 'INSTORE';
                }
                a.QueriedSource__c = 'BEST BUY';
                a.GenericMedia__c = 'BBY';
                a.MMBLookup__c = true;
                a.MMBOrderType__c = 'N1';
                update a;
                return new PageReference('/' + a.Id + '/e');
            }
            else {
                for (Database.Error error : lc.errors) {
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, error.getMessage()));
                }                               
                return null;
            }
        }
        catch (CalloutException ex) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'ERR: '+errormap.get('EM_CommErrorTelemarAvailability').Message__c));
            return null;
        } 
        catch (IntegrationException ie) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, ie.getMessage()));                        
            return null;
        } 
        catch(Exception err) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, err.getMessage()));
            return null;
        }
        return null;
    }

    //Added by TCS for HRM 6311
    private void setCHSOnChannel(){
        // Check for CHS for only resi channel
        if(ld.channel__c == Channels.TYPE_RESIDIRECT || ld.channel__c == Channels.TYPE_RESIRESALE || ld.channel__c == Channels.TYPE_CUSTOMHOMESALES){
            // If this lead has an address then look into this record to find if it is a CHS address
            if(ld.AddressId__c != null && [SELECT CHS__c from Address__c WHERE id = :ld.AddressId__c].CHS__c == true){
                ld.channel__c = Channels.TYPE_CUSTOMHOMESALES;
            }
            else {
                // Look into the Address table to find the CHS address else default to resi direct
                List<Address__c> addrCHS = [SELECT Name from Address__c WHERE CHS__c = true AND Street__c = :ld.SiteStreet__c AND State__c = :ld.SiteStateProvince__c AND PostalCode__c = :ld.SitePostalCode__c limit 1];
                if (addrCHS != null && !addrCHS.isEmpty())
                    ld.channel__c = Channels.TYPE_CUSTOMHOMESALES;
                else
                    ld.channel__c = Channels.TYPE_RESIDIRECT;
            }
        }
    }
}