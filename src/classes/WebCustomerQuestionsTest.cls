@isTest
private class WebCustomerQuestionsTest
{
    public static final String ACCOUNT_FIRSTNAME = 'UnitTestFirst';
    public static final String ACCOUNT_LASTNAME = 'UnitTestLast';
    public static final String ACCOUNT_NAME = 'Unit Test Acct';
    public static final String OTHER_DISPOSITION = 'X - Other';
    
    @testSetup
    static void testData()
    {
               
        TestHelperClass.createReferenceDataForTestClasses();      
        //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        Account acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'test1 testing';
        acct.FirstName__c = 'test1';
        acct.LastName__c = 'testing';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1200 - SmallBusiness';
        acct.PostalCodeID__c = TestHelperClass.inferPostalCodeID(addr.PostalCode__c, acct.Business_Id__c.Contains('1100') ? '1100' : '1200');
        acct.DispositionCode__c = 'X - Other';
        acct.ShippingPostalCode = '221o2';
        acct.Data_Source__c = 'User Entered';
        acct.MMBOrderType__c ='R2';
        acct.MMB_Disco_Site__c = true;
        acct.MMB_Disco_Count__c =1;
        acct.NonPayDiscoReasonCode__c ='Customer Cancel - Non Payment';  
        ResaleGlobalVariables__c.getinstance('DataRecastDisableAccountTrigger').value__c = 'TRUE';
        //acct.DNIS__c =dnis.Id;      
        insert acct;
        
        // Call Disposition
            Call_Disposition__c callDispo = new Call_Disposition__c();
            callDispo.Name = 'EC Transfer to Loyalty';
            callDispo.Action__c = 'Transferred';
            callDispo.Active__c = true;
            insert callDispo;
        // CallData For Valid Account
            Call_Data__c cd = new Call_Data__c();
            cd.Name = 'Test Call Data';
            cd.Account__c = acct.Id;
            cd.Call_Disposition__c = callDispo.Id;
            cd.UniqueVisitorId__c = 'test1';
            insert cd;
        List<CustomerInterestConfig__c> lstCustomerInterestConfig =  new List<CustomerInterestConfig__c>();
        CustomerInterestConfig__c CI = new CustomerInterestConfig__c();
        CI.ConfiguratorVersion__c = '';
        CI.CallData__c =  null;
        CI.Type__c = 'Question';
        CI.uniqueVisitorNumber__c = '1234';
        CI.SequenceNumber__c = '1';
        CI.Response__c= '';
        CI.FriendlyId__c = 'test';
        lstCustomerInterestConfig.add(CI);
        
        CustomerInterestConfig__c CI1 = new CustomerInterestConfig__c();
        CI1.ConfiguratorVersion__c = '';
        CI1.CallData__c =  cd.id;
        CI1.Type__c = 'Question';
        CI1.uniqueVisitorNumber__c = '1234';
        CI1.SequenceNumber__c = '1';
        CI1.Response__c= '';
        CI1.FriendlyId__c = 'test';
        lstCustomerInterestConfig.add(CI1);
        
        CustomerInterestConfig__c CI2 = new CustomerInterestConfig__c();
        CI2.ConfiguratorVersion__c = '';
        CI2.CallData__c =  null;
        CI2.Type__c = 'Product';
        CI2.uniqueVisitorNumber__c = '1234';
        CI2.SequenceNumber__c = '2';
        CI2.Response__c= '';
        CI2.FriendlyId__c = 'test';
        lstCustomerInterestConfig.add(CI2);
        
        CustomerInterestConfig__c CI3 = new CustomerInterestConfig__c();
        CI3.ConfiguratorVersion__c = '';
        CI3.CallData__c =  cd.id;
        CI3.Type__c = 'Product';
        CI3.uniqueVisitorNumber__c = '1234';
        CI3.SequenceNumber__c = '2';
        CI3.Response__c= '';
        CI3.FriendlyId__c = 'test';
        lstCustomerInterestConfig.add(CI3);
        insert lstCustomerInterestConfig;
    }
    
    /*public static Id inferPostalCodeID(String postalCode, String businessID) {
        
        Id returnValue = null;
        try {
            Postal_Codes__c pc = [select id, name, businessId__c from Postal_Codes__c where name = :postalCode and businessId__c = :businessID];
        
            if (pc != null) {
                returnValue = pc.Id;
            }
        }
        catch (Exception e) {
            Postal_Codes__c newPC = new Postal_Codes__c ( name = postalCode, businessId__c = businessID, TownId__c = 'TestTNID');
            
            insert newPC;
            
            returnValue = newPC.Id;
                
        }
        
        return returnValue;
    }*/
   
    static testMethod void unitTest1()
    {
        Account acc = [Select Id from Account limit 1];
        WebCustomerQuestions obj = new WebCustomerQuestions(); 
        obj.accountComponent = acc.id;
        WebCustomerQuestions wcq = new WebCustomerQuestions();
        WebCustomerQuestions.CustomerConfigWrapper wrap = new WebCustomerQuestions.CustomerConfigWrapper();
        wrap.sequenceNumber = '10';
        wrap.recordId = acc.Id;
        wrap.uniqueVisitorNumber = '123465';
        wrap.friendlyId  = '76876999';
        wrap.entity = '887687';
        wrap.callData = '87987879';
        wrap.entityType = '9879879879';
        wrap.response = 'true';
        wrap.compareTo(wrap);
        Map<String, Object> mapObj = new Map<String, Object>();
        mapObj.put('10',wrap);
        wrap.sequenceNumber = '11';
        mapObj.put('11',wrap);
        WebCustomerQuestions.filterResultsOfSearch('test', acc.Id);        
        Test.setCurrentPageReference(new PageReference('Page.WebCustomerQuestions')); 
        System.currentPageReference().getParameters().put('questionsMap', JSON.serialize(mapObj));
        System.currentPageReference().getParameters().put('accountCompId', acc.id);
        obj.saveAttachQuestions();
        obj.initializeThatDependsOnReceivedString();
    }
    
}