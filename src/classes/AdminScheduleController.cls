public class AdminScheduleController {

    public String selectedRadio{get;set;}
    public transient String JSONUserlist{get;set;}
    public String selectedId{get;set;}
    public Map<id,userrole> RolesMap;
    //public Map<id,User> usersMap;
    public transient String JSONterritoryList{get;set;}
    public List<user> terrUserList{get;set;}
    public String selectedTerrId{get;set;}
    public String selectedTerrUserId{get;set;}
    public String selectedTerriUserId{get;set;}
    public Boolean showTerritoryUsers{get;set;}
    public Map<id,Territory2> territoriesMap;
    public transient String JSONTerrUserList{get;set;}
    public adminScheduleController(){
        
        //find users
        /*
        List<id> FieldRoleids;
        List<userrole> userRoles=[select id from userrole where name='NA Resi Program Dir/Sales Ops' or name='NA SB Program Dir/Sales Ops'];
        if(userRoles.size()>0){
            FieldRoleids=new List<id>();
            for(userrole ur:userRoles){
                FieldRoleids.add(ur.id);
            }
        }
        Set<id> roles1=subordinateRolesClass.getSubordinateRoles(FieldRoleids[0]);
        Set<id> roles2=subordinateRolesClass.getSubordinateRoles(FieldRoleids[1]);
        
        roles1.addAll(roles2);*/
        List<String> supportedProfiles=new List<String>{'ADT NA Sales Manager','ADT NA Sales Representative'};
        
        //selectedRadio='User';
        RolesMap=new Map<id,userrole>([select id,name from userrole]);
        Map<id,User> usersMap=new Map<id,user>([select id,name,UserRoleId from user where isActive=true and profile.name IN:supportedProfiles ]);
        List<user> users=usersMap.values();
        terrUserList=new List<user>();
        List<allUsers> allUserList=new List<allUsers>();
        String roleName='No role';
        
        for(User u:users){
            if(!utilities.isEmptyorNull(u.UserRoleId)){
                roleName=rolesMap.get(u.UserRoleId).name;
               
            }
            allUserList.add(new allUsers(u.id,u.name,roleName));

        }
        JSONUserlist=JSON.serialize(allUserList);
        JSONUserlist=String.escapeSingleQuotes(JSONUserList);
        
        List<territory2> territories=[select id,name from Territory2];
        List<allTerritories> allTerrList=new List<allTerritories>();
        for(territory2 terr:territories){
            allTerrList.add(new allTerritories(terr.id,terr.name));
        }
        
        JSONTerritoryList=JSON.serialize(allTerrList);
        JSONTerrUserList=JSONUserList;
        system.debug('Serialized Territory List is '+JSONTerritoryList);
        system.debug('Serialized list is '+JSONUserList);
        territoriesMap=new Map<id,Territory2>([select id,name from Territory2]);
    }
    

    
    public class allUsers{
        public String userName;
        public id userId;
        public String Role;
        
        
        public allUsers(Id usrid,String usrname,String rlename){
            userId=usrid;
            userName=usrName;
            Role=rlename;
        }
    
    }
    
    public class allterritories{
        public String territoryName;
        public Id terrId;
        
        public allTerritories(Id terrtId,String terrName){
            territoryName=terrName;
            terrId=terrtid;
        }
    }
    
    
    public pageReference searchUser(){
        User u=[select id,userRoleId,profile.name from user where id=:selectedId];
        //system.debug(rolesMap.get(usersMap.get(selectedId).userRoleId).name);
        if(!utilities.isEmptyOrNull(u.userRoleId)){
            /*
            if(rolesMap.get(u.userRoleId).name.contains('Mgr')){
                PageReference pageRef = new PageReference('/apex/managerSchedule?managerId='+selectedId);
                return pageRef;
            }else if(rolesMap.get(u.userRoleId).name.contains('Rep')){
                PageReference pageRef = new PageReference('/apex/UserSchedule?UserId='+selectedId);
                return pageRef;
            }*/
            
                    
            if(u.profile.name=='ADT NA Sales Manager'){
                PageReference pageRef = new PageReference('/apex/managerSchedule?managerId='+selectedId);
                pageRef.setRedirect(true);
                return pageRef;
            }else if(u.profile.name=='ADT NA Sales Representative'){
                PageReference pageRef = new PageReference('/apex/UserSchedule?UserId='+selectedId);
                pageRef.setRedirect(true);
                return pageRef;
            }
            
            
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Cannot schedule for the selected user'));
        return null;
        
    }
    
    public pagereference searchTerritoryUsers(){
        showTerritoryUsers=true;
        //List<UserTerritory2Association> territoryAssociations=[select id,userId from UserTerritory2Association where isactive=true and territory2id=:selectedTerrId];
        List<SchedUserTerr__c> terrUserSchedules=[select id,user__c,Territory_Association_ID__c from SchedUserTerr__c where Territory_Association_ID__c=:selectedTerrId];
        set<id> userIdSet=new set<id>();
        if(terrUserSchedules.size()>0){
            for(SchedUserTerr__c uta:terrUserSchedules){
                userIdSet.add(uta.user__c);      
            }
            
            terrUserList=[select id,name from user where id IN:userIdset and isactive=true];
            
            
        }

        
        //to refresh user list
        List<String> supportedProfiles=new List<String>{'ADT NA Sales Manager','ADT NA Sales Representative'};
        
        //selectedRadio='User';
        //RolesMap=new Map<id,userrole>([select id,name from userrole]);
        Map<id,User> usersMap=new Map<id,user>([select id,name,UserRoleId from user where isActive=true and profile.name IN:supportedProfiles and id NOT IN:userIdset ]);
        List<user> users=usersMap.values();
        List<allUsers> allUserList=new List<allUsers>();
        String roleName='No role';
        
        for(User u:users){
            if(!utilities.isEmptyorNull(u.UserRoleId)){
                roleName=rolesMap.get(u.UserRoleId).name;
               
            }
            allUserList.add(new allUsers(u.id,u.name,roleName));

        }
        JSONTerrUserList=JSON.serialize(allUserList);
        JSONTerrUserList=String.escapeSingleQuotes(JSONTerrUserList);
        
        
        return null;
    }
    
    public pageReference selectTerrUser(){
        PageReference pageRef = new PageReference('/apex/UserSchedule?UserId='+selectedTerrUserId);
        pageRef.setRedirect(true);
        return pageRef;
    
    }
    
    public pagereference addUsertoTerritory(){
        system.debug('selected user is ------------------------------'+selectedTerriUserId);
        SchedUserTerr__c newSchedule=new SchedUserTerr__c(Territory_Association_ID__c=selectedTerrId, User__c=selectedTerriUserId,Name=territoriesMap.get(selectedTerrId).name);
        Insert newSchedule;
        searchTerritoryUsers();
        return null;
    }
    
    public pageReference deleteTerrUser(){
    
        SchedUserTerr__c tobeDeleted=[select id from SchedUserTerr__c where user__c=:selectedTerrUserId and Territory_Association_ID__c =:selectedTerrId];
        delete tobeDeleted;
        searchTerritoryUsers();
        return null;
    
    }

}