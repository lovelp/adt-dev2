/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CaseTriggerHelperTest {

    static testMethod void testCommissionErrorCaseSubmission() {
        
        User SalesRep;
        User admUser;
        Account a1;
        RecordType CommissionErrorRT = [SELECT Id, Name FROM RecordType WHERE Name = 'Callidus'];
        Case CommissionCase;
        
        admUser = TestHelperClass.createAdminUser(); 
        test.startTest();
        System.runAs(admUser) {
            TestHelperClass.createReferenceDataForTestClasses();
            TestHelperClass.createReferenceUserDataForTestClasses();
        }

        // Init management hierarchy
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {     
            SalesRep = TestHelperClass.createExecWithTeam();
        }
        
        SalesRep.Business_Unit__c = 'SB Direct Sales';
        update SalesRep; 
        Profile managerProfile = [select id from profile where name='ADT NA Sales Manager' limit 1];
        User approver = [Select Id from user where profileId =: managerProfile.id and isactive = true limit 1];
        System.runAs(SalesRep) {

            a1 = TestHelperClass.createAccountData();
            
            CommissionCase = new Case(  Subject                     = 'Commission Error', 
                                        Description                 = 'Testing Commission Error', 
                                        Status                      = 'New', 
                                        Origin                      = 'Sales Rep', 
                                        Reason                      = 'New problem', 
                                        Compensation_Plan__c        = 'CL9', 
                                        Commission_Reason_Code__c   = '4B',
                                        RecordTypeId                = CommissionErrorRT.Id,
                                        IsEscalated                 = false,
                                        Commission_User__c          = UserInfo.getUserId(), // Same as controller logic
                                        Account                     = a1 );
            CommissionCase.Assignee__c = approver.Id;                            
            insert CommissionCase;
        }
        
        // Read sales rep's manager
        Map<String, User> hierarchy = RoleUtils.getRepApproverUserByLevel(SalesRep.Id);
        
        // This rep's manager
        Id SalesRepManagerId = RoleUtils.getNextAvailableAsignee(SalesRep.Id);
        User SalesRepManager = hierarchy.get(RoleUtils.Manager);
        if(SalesRepManager == null){
            SalesRepManager = approver;
            SalesRepManagerId = approver.Id;
        }
        //system.assertEquals( SalesRepManager.Id, SalesRepManagerId);
        
        // MCA Needed
        system.debug('@@@== >'+CommissionCase);
        System.runAs(admUser) { 
            CommissionCase.Status = 'MCA Needed';
            update CommissionCase;
        }
        
        // On MCA Needed the rep needs to enter an amount for expected commission
        System.runAs(SalesRep) {
            CommissionCase.Expected_Commission_Amount__c = 40.0;
            update CommissionCase; 
        }
         
        
        // Approve commission by Manager
        System.runAs(SalesRepManager) {
            CommissionCase.Actual_Adjustment_Amount__c = CommissionCase.Expected_Commission_Amount__c;
            update CommissionCase;
            
            //CommissionCase.IsEscalated = true;
            //CommissionCase.Approval_Levels_Reached__c = SalesRepManager.Name;         
            //update CommissionCase;
            
            CommissionCase.Commission_Error_Final_Disposition__c = 'Approved';
            update CommissionCase;
            
            CommissionCase.Status = 'Closed';
            update CommissionCase;
        }       
        test.stopTest();        
    }
}