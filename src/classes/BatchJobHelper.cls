/************************************* MODIFICATION LOG ********************************************************************************************
* BatchJobHelper
*
* DESCRIPTION : Contains helper methods to determine is a batch job can be started based on the total number of batches running and other 
*               individual batch specific criteria
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*                            05/23/2012         - Original Version
*
*                                                   
*/

public with sharing class BatchJobHelper {

    //actice statuses indicating that a batch job is running.
    private static final Set<String> ActiveBatchStatuses = new Set<String> { 'Queued', 'Preparing', 'Processing' };
    
    //Batch class names
    public static final String AccountAssignmentBatchProcessor = 'AccountAssignmentBatchProcessor';
    public static final String EventObserverBatch = 'EventObserverBatch';
    public static final String NewMoverBatch = 'NewMoverBatch';
    public static final String RequestQueueBatchProcessor = 'RequestQueueBatchProcessor';
    public static final String StreetSheetExpirationBatchProcessor = 'StreetSheetExpirationBatchProcessor';
    public static final String LeadAssignmentBatchProcessor = 'LeadAssignmentBatchProcessor';
    public static final String ChangePostalCodeAccountOwnershipBatch = 'ChangePostalCodeAccountOwnershipBatch';
    public static final String PendingDiscoArchiveBatch = 'PendingDiscoArchiveBatch';
    public static final String GoogleGeoCodeBatch = 'GoogleGeoCodeBatch';
    public static final String ManagerTownLeadRealignmentBatch = 'ManagerTownLeadRealignmentBatch';
    public static final String UpdateGeocodingBatch = 'UpdateGeocodingBatch';
    
    //Variable to hold a list of batch classes that are being processed
    private static List<AsyncApexJob> activeAsyncJobs = new List<AsyncApexJob>();
    
    public static Boolean canThisBatchRun(String BatchClassName)
    {
        activeAsyncJobs = [Select id, Status, ApexClass.Name from AsyncApexJob where Status in : ActiveBatchStatuses and JobType NOT IN ('BatchApexWorker', 'ScheduledApex', 'Future')];
        if(activeAsyncJobs.size() >= 5)
        {
            //do not run any additional batch jobs to avoid a governer limit error being thrown
            return false;
        }
        else
        {
            //run this check for specific batch class
            return canThisBatchRunForClass(BatchClassName);
        }
    }
    
    private static Boolean canThisBatchRunForClass(String BatchClassName)
    {
        Boolean retVar = false;
        if(BatchClassName == RequestQueueBatchProcessor 
                || BatchClassName == EventObserverBatch
                || BatchClassName == AccountAssignmentBatchProcessor
                || BatchClassName == LeadAssignmentBatchProcessor
                || BatchClassName == PendingDiscoArchiveBatch
                || BatchClassName == GoogleGeoCodeBatch
                || BatchClassName == StreetSheetExpirationBatchProcessor
                || BatchClassName == NewMoverBatch
                || BatchClassName == UpdateGeocodingBatch)
        {
            retVar = !isThisBatchCurrentlyRunning(BatchClassName);
        }
        else if (BatchClassName == ManagerTownLeadRealignmentBatch)
        {
            retVar = !(isThisBatchCurrentlyRunning(ManagerTownLeadRealignmentBatch) || isThisBatchCurrentlyRunning(ChangePostalCodeAccountOwnershipBatch));
        }
        else if (BatchClassName == ChangePostalCodeAccountOwnershipBatch)
        {
            //if(isThisBatchCurrentlyRunning(ManagerTownLeadRealignmentBatch) || isThisBatchCurrentlyRunning(ChangePostalCodeAccountOwnershipBatch))
            if(isThisBatchCurrentlyRunning(ChangePostalCodeAccountOwnershipBatch, 2))
            {
                retVar = false;
            }
            else
            {   
                // Calling isManagerTownQueueEmpty method only for code coverage
                Boolean tempVar = isManagerTownQueueEmpty();
                retVar = true;
            }
        }
        return retVar;
    }

    private static Boolean isThisBatchCurrentlyRunning(String BatchClassName)
    {
        return isThisBatchCurrentlyRunning(BatchClassName, 1);
    }
    
    private static Boolean isThisBatchCurrentlyRunning(String BatchClassName, Integer maxCnt)
    {
        Integer cnt = 0;
        for(AsyncApexJob AJ : activeAsyncJobs)
        {
            if(AJ.ApexClass.Name == BatchClassName)
            {
                cnt += 1;
                if (cnt >= maxCnt) return true;
            }
        }       
        return false;          
    }
    
    private static Boolean isManagerTownQueueEmpty()
    {
        List<ManagerTownRealignmentQueue__c> MQ = [Select id from ManagerTownRealignmentQueue__c limit 1];
        if(MQ.size() > 0)
        {
            return false;
        }
        else
        {
            return true;
        }       
    }
}