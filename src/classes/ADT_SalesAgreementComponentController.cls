/************************************* MODIFICATION LOG ********************************************************************************************
* ADT_SalesAgreementComponentController
*
* DESCRIPTION : Controller for Sales Agreement lightning component
*  1. Create
*  2. Update
*  3. Lookup
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                        DATE              TICKET            REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Harsha                        28/Jun/2019        HRM-9710         Sales Agreement Lookup/Create/Update
* Jitendra Kothari              07/15/2019         HRM-10409        Enhance Existing Sales Agreement Lookup to Add Fields
* Jitendra Kothari              08/22/2019         HRM-10982        SFDC & Sales Pilot Integration - Additional Fields
* Jitendra Kothari              09/06/2019         HRM-11324        Ability to capture existing equipment that customer can bring to the new site 
*/

public without sharing class ADT_SalesAgreementComponentController {
    public static final String CREATE_TYPE = 'Create';
    public static final String LOOKUP_TYPE = 'Lookup';
    
    public class DataWrapper {
        @AuraEnabled public Opportunity opp;
        @AuraEnabled public String requestType;
        @AuraEnabled public SalesAgreementSchema.SalesAgreementCreateRequest sacReq;
        @AuraEnabled public SalesAgreementSchema.SalesAgreementLookupRequest salReq;
        @AuraEnabled public SalesAgreementSchema.SalesAgreementCreateResponse sacRes;
        @AuraEnabled public SalesAgreementSchema.SalesAgreementLookupResponse salRes;
        @AuraEnabled public Boolean isSelected;
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public Boolean showButton;
        @AuraEnabled public String pageMessage;
        @AuraEnabled public String SalesAgreementId;
        @AuraEnabled public String returnURL;
        @AuraEnabled public String MMBBillingSystems;
        @AuraEnabled public String installCharge;
        @AuraEnabled public String sellingTotalRMR;
        @AuraEnabled public String saleStatus;
        @AuraEnabled public String AgreementPhase;
        @AuraEnabled public String SalesAgreement_LastModified;
        @AuraEnabled public String Address;
    }
    
    @AuraEnabled
    public static DataWrapper initActions(String oppId, String reqType) {
        DataWrapper wrapper = new DataWrapper();
        wrapper.pageMessage = '';
        wrapper.showButton = true;
        wrapper.requestType = reqtype;
        wrapper.isSelected = false;
        wrapper.isSuccess = false;
        try{
            wrapper.opp = SalesAgreementAPI.getOpportunity(oppId);
          
            if(wrapper.opp == null || String.isBlank(wrapper.opp.Id)){
                //wrapper.pageMessage = CommercialMessages__c.getValues('OppIdRequired').Message__c;
                wrapper.pageMessage = CommercialMessages__c.getValues('OppRequired').Message__c;
                return wrapper;
            }
            
            if(String.isBlank(wrapper.requestType)){
                wrapper.pageMessage = CommercialMessages__c.getValues('requestTypeRequired').Message__c;
                return wrapper;
            }
            wrapper.isSuccess = false;
            if(wrapper.requestType == CREATE_TYPE){
                //return if opportunity already have Sales Agreement
                if(wrapper.opp.SalesAgreementId__c != null){
                    wrapper.pageMessage = CommercialMessages__c.getValues('SalesAgreementPresent').Message__c;
                    wrapper.showButton = false;
                    wrapper.isSuccess = true;
                    wrapper.sacRes = new SalesAgreementSchema.SalesAgreementCreateResponse();
                    wrapper.SalesAgreementId = wrapper.opp.SalesAgreementId__c;
                    return wrapper;
                } 
                wrapper.sacReq = SalesAgreementAPI.getCreateRequest(wrapper.opp); 
                system.debug(wrapper.sacReq);
                if(String.isBlank(wrapper.sacReq.PrimarySalesRep)){
                    wrapper.pageMessage = CommercialMessages__c.getValues('SalesRepRequired').Message__c;
                    wrapper.showButton = false;
                    return wrapper;
                }
                wrapper.MMBBillingSystems = getInstance(wrapper.sacReq.mastermindInstanceID);
                wrapper.Address = SalesAgreementAPI.getAddressWithComma(wrapper.sacReq.CustomerAddress1, wrapper.sacReq.CustomerAddress2, 
                                                                        wrapper.sacReq.CustomerCity, wrapper.sacReq.CustomerState, 
                                                                        wrapper.sacReq.CustomerCounty, wrapper.sacReq.CustomerZip);
            } else if(wrapper.requestType == LOOKUP_TYPE){
                wrapper.salReq = SalesAgreementAPI.getLookupRequest(wrapper.opp);
                if(wrapper.salReq.SalesAgreementID != null && !Test.isRunningTest()){
                    wrapper = submit(JSON.serialize(wrapper));
                    wrapper = selectSalesAgreement(JSON.serialize(wrapper), wrapper.opp.Id, wrapper.salReq.SalesAgreementID);
                }
            }
            
        }catch(Exception e){
            system.debug('Exception----'+e.getMessage()+'---LNO---'+e.getLineNumber());
            ADTApplicationMonitor.log ('Sales Agreement Construction Exception', e.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
            wrapper.pageMessage = e.getMessage();
        }
        return wrapper;
    }
    @AuraEnabled
    public static DataWrapper submit(String wrapperData){
        DataWrapper wrapper = (DataWrapper)JSON.deserialize(wrapperData, DataWrapper.class);
        try{
            wrapper.isSuccess = false;
            wrapper.pageMessage = '';
            String message;
            if(wrapper.requestType == CREATE_TYPE){
                List<Opportunity> oppList = new List<Opportunity>();
                if(wrapper.opp.Id!=null){
                     oppList = [select id,SalesAgreementId__c from opportunity where id=:wrapper.opp.Id and SalesAgreementId__c!=null];
                }
                if(oppList.size()>0){
                    if(oppList[0].SalesAgreementId__c !=null){
                        wrapper.pageMessage = CommercialMessages__c.getValues('SalesAgreementPresent').Message__c;
                        wrapper.showButton = false;
                        wrapper.isSuccess = true;
                        wrapper.sacRes = new SalesAgreementSchema.SalesAgreementCreateResponse();
                        wrapper.SalesAgreementId = oppList[0].SalesAgreementId__c;
                        return wrapper;
                    }
                }
                wrapper.sacRes = SalesAgreementAPI.createSalesAgreement(null, wrapper.sacReq);
                if(wrapper.sacRes.status == true){
                    wrapper.returnURL = goToSalesPilot(wrapper.sacRes.result, wrapper.opp.Id);
                    system.debug(wrapper.returnURL);
                    return wrapper;
                }
                wrapper.isSuccess = wrapper.sacRes.status;
                message = wrapper.sacRes.message;
            }else if(wrapper.requestType == LOOKUP_TYPE){
                if(String.isBlank(wrapper.salReq.SalesAgreementID)){
                    wrapper.pageMessage = CommercialMessages__c.getValues('SalesAgreementRequired').Message__c;
                    return wrapper;
                }
                wrapper.opp.LookupSalesAgreementRequestJSON__c = JSON.serialize(wrapper.salReq, true);
                SalesAgreementSchema.SalesAgreementLookupResp resp = SalesAgreementAPI.lookupSalesAgreement(null, wrapper.salReq);
                wrapper.isSuccess = resp.status;
                message = resp.message;
                wrapper.salRes = resp.result;
                
                if(wrapper.isSuccess == true){
                	wrapper.MMBBillingSystems = getInstance(wrapper.salRes.mastermindInstanceID + '');
                	wrapper.Address = SalesAgreementAPI.getAddressWithComma(wrapper.salRes.CustomerAddress1, wrapper.salRes.CustomerAddress2, 
                                    	wrapper.salRes.CustomerCity, wrapper.salRes.CustomerState, 
                                    	wrapper.salRes.CustomerCounty, wrapper.salRes.CustomerZip);
                    wrapper.opp.LookupSalesAgreementResponseJSON__c = JSON.serialize(resp, true);                	
					populateFieldsForUI(wrapper);               	
                }
                if(!Test.isRunningTest() && String.isBlank(wrapper.opp.SalesAgreementId__c)){
                    update wrapper.opp;
                }
                if(wrapper.isSuccess == true){
                	if(wrapper.salRes.lastModified != null){
                    	wrapper.opp.SalesAgreement_LastModified__c = Datetime.valueOfGmt(wrapper.salRes.lastModified.replace('T', ' '));
                	}
                	wrapper.opp.SalesAgreement_LastLookupRun__c = system.now();
                }
            }
            //Incase of failure
            if(wrapper.isSuccess == false){
                wrapper.pageMessage = message;
            }/*else{
            	if(wrapper.salRes.lastModified != null){
                    wrapper.opp.SalesAgreement_LastModified__c = Datetime.valueOfGmt(wrapper.salRes.lastModified.replace('T', ' '));
                }
                
            }*/
        }catch(Exception e){
            system.debug('Error--->'+e.getMessage() +'--LNO--'+ e.getLineNumber());
            ADTApplicationMonitor.log ('Sales Agreement submit method Exception', e.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
            wrapper.pageMessage = e.getMessage();
            //if(Test.isRunningTest()){
            	database.update(wrapper.opp, false);
            //}
        }
        return wrapper;
    }
    public static void populateFieldsForUI(DataWrapper wrapper){
    	if(wrapper.salRes.sites != null && !wrapper.salRes.sites.isEmpty()){
    		wrapper.salRes.numberOfSites = wrapper.salRes.sites.size();
	    	for(SalesAgreementSchema.Sites site : wrapper.salRes.sites){
	    		site.siteAddress = SalesAgreementAPI.getAddressWithComma(site.siteAddress1, site.siteAddress2, 
	                                    	site.siteCity, site.siteState, 
	                                    	site.siteCounty, site.siteZip);
	            if(site.systemDesigns != null && !site.systemDesigns.isEmpty()){
	            	for(SalesAgreementSchema.SystemDesigns sysDes : site.systemDesigns){
	            		sysDes.siteID = site.siteID;
	            	}
	            }
	    	}
    	}
    }
    public static String checkOppForSAId(String saId){
    	list<Opportunity> oppList = [SELECT Id, SalesAgreementId__c FROM Opportunity WHERE SalesAgreementId__c = :saId];
    	if(!oppList.isEmpty()){
    		list<String> oppIds = new list<String>();
    		for(Opportunity opp : oppList){
    			oppIds.add(opp.Id);
    		}
    		return String.join(oppIds, ', ');
    	}
    	return '';
    }
    @AuraEnabled
    //public static DataWrapper selectSalesAgreement(String oppId, String saId){
    public static DataWrapper selectSalesAgreement(String wrapperData, String oppId, String saId){
        DataWrapper wrapper = (DataWrapper)JSON.deserialize(wrapperData, DataWrapper.class);
        try{
            //wrapper.opp = SalesAgreementAPI.getOpportunity(oppId);
            system.debug('wrapper.opp----'+wrapper.opp);
            wrapper.returnURL = null;
            if(saId != wrapper.opp.SalesAgreementId__c){
            	String StrOppIds = checkOppForSAId(saId);
            	if(String.isNotBlank(StrOppIds)){
	        		wrapper.pageMessage = CommercialMessages__c.getValues('SalesAgreementPresentForOtherOpp').Message__c.replace('[SalesAgreementId]', saId).replace('[Opportunity]', StrOppIds);
	                return wrapper;
            	}
                SalesAgreementSchema.SalesAgreementUpdateRequest sauReq = SalesAgreementAPI.getUpdateRequest(wrapper.opp); 
                sauReq.SalesAgreementID = saId;
                wrapper.opp.UpdateSalesAgreementRequestJSON__c = JSON.serialize(sauReq, true);
                SalesAgreementSchema.SalesAgreementUpdateResponse sauRes = SalesAgreementAPI.updateSalesAgreement(oppId, sauReq);
                
                if(Test.isRunningTest()) sauRes.status = true;
                if(sauRes.status == true){
                    wrapper.opp.UpdateSalesAgreementResponseJSON__c = JSON.serialize(sauRes, true);
                    wrapper.opp.SalesAgreementId__c = saId;
                }else{
                	//wrapper.isSuccess = sauRes.status;
                    wrapper.pageMessage = sauRes.message;
                    return wrapper;
                }
            }
            SalesAgreementAPI.populateOppFromResponse(wrapper.salRes, wrapper.opp, 'UI');
            system.debug('wrapper.opp--'+wrapper.opp);
            
            wrapper.returnURL = '/'+oppId;
            if(Test.isRunningTest()){
                Integer divideByZeroException = 10/0;
            }
            
        }catch(Exception ex){
            system.debug('ex error--'+ex.getMessage() +'--LNO--'+ex.getLineNumber());
            ADTApplicationMonitor.log ('Sales Agreement selectSalesAgreement method Exception', ex.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
            wrapper.pageMessage = ex.getMessage();
            database.update(wrapper.opp, false);
        }
        system.debug('wrapper---'+wrapper);
        system.debug('wrapper.returnURL---'+wrapper.returnURL);
         System.debug('Exit selectSalesAgreement');
        return wrapper;
    }
    @Auraenabled
    public static String goToSalesPilot(String salesAgreementId, String oppId){
        String url = IntegrationSettings__c.getinstance().SalesPilotURL__c;
        if(url != null){
            url = url.replace('[SalesAgreementId]', salesAgreementId);
            return url;
        }
        return '/' + oppId;
    }
    public static String getInstance(String instanceId){
        if(instanceId == '2'){
            return '2 - MMB';
        }else if(instanceId == '1'){
            return '1 - P1MMB';
        }else{
        	return '';
        }
    }
}