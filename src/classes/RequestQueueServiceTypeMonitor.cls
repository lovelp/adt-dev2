/************************************* MODIFICATION LOG ********************************************************************************************
* RequestQueueServiceTypeMonitor
*
* DESCRIPTION : Implements Schedulable and is responsible for retrieving records from Request Queue with the Request Status = Ready.  
*               Records are retrieved ordered by Request Date to ensure delivery of messages in the appropriate order.  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Viraj Shah                    20/09/2019              - Original Version
*
*                                                   
*/

global class RequestQueueServiceTypeMonitor implements Schedulable {
    
    /*
    * Uses scope parameter of 1 since the batch class being constructed will make a callout and SFDC limits allow 
    * a batch class to make only one callout.
    * 
    * Includes logic to schedule its next execution based upon the custom setting RequestQueueServiceTypeMonitor.
    */
    global void execute(SchedulableContext sc){
     /*  //Database.executeBatch(new AdtCustContactEBRServiceBatch()); 
        //String requestMonitorRunning = BatchState__c.getInstance('IsRequestQueueMonitorRunning').value__c;
        if(BatchJobHelper.canThisBatchRun(BatchJobHelper.AdtCustContactEBRServiceBatch)){
          //Database.executeBatch(new AdtCustContactEBRServiceBatch(), 1);
        }
         
        //reschedule the same class again
        RequestQueueServiceTypeMonitor rqm = new RequestQueueServiceTypeMonitor();
        DateTime timenow = DateTime.now().addMinutes(Integer.valueOf('3'));
        String seconds = '0';
        String minutes = String.valueOf(timenow.minute());
        String hours = String.valueOf(timenow.hour());
        
        //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
        String jobName = 'RequestQueueServiceTypeMonitor-'+minutes+'-'+hours;
        if( Test.isRunningTest() ){
          jobName = 'RequestQueueServiceTypeMonitor - '+jobName;
        }
        system.schedule(jobName, sch, rqm);
        if (sc != null)    
          system.abortJob(sc.getTriggerId());  
     */
      } 
}