/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : AddressHelperTest is a test class for AddressHelper.cls
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani			03/01/2012			- Origininal Version
*
*													 
*/
@isTest
private class AddressHelperTest 
{
	static testMethod void testsetSiteAddress()
	{
		map<String, Address__c> newAddresses = new map<String, Address__c>();
	
		Test.startTest();		
		
		Address__c addr = new Address__c();
		addr.Latitude__c = 38.94686000000000;
		addr.Longitude__c = -77.25470100000000;
		addr.Street__c = '8952 Brook Rd';
		addr.City__c = 'McLean';
		addr.State__c = 'VA';
		addr.PostalCode__c = '221o2';
		addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
		
		newAddresses.put(addr.OriginalAddress__c, addr);
				
		AddressHelper.upsertAddresses(newAddresses);
		
		System.assert(addr.Id != null,'This is a valid address record');
		
		Test.stopTest();
	}	
	
}