public class RealTimePaymentErrorResponseJ2A {
    public class Errors {
        public String status;
        public String errorCode;
        public String errorMessage;
    }
    public List<Errors> errors;
    public static RealTimePaymentErrorResponseJ2A parse(String json) {
        return (RealTimePaymentErrorResponseJ2A) System.JSON.deserialize(json, RealTimePaymentErrorResponseJ2A.class);    
    }
}