/*
* ADTNotereceivableAPI
*
* DESCRIPTION : This class Contains is a  GET Request and gets a response for Notes received. Gets the Json notes response and stores it in the customer Loan object
*  1. getNotes

*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Nitin Gottiparthi              6/24/2019              - Original Version                                                  
*/
public with sharing class ADTNotereceivableAPI {
    
    // This method is used for retreiving the notes for a particualar customer based on the customer number
    public static list<CustomerLoan__c> getNotes(string custNumber) {
        if(String.isBlank(custNumber)){
            return null;
        }
        String userName = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPUsername__c:'abc';
        String password = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPPassword__c:'def';
        String url = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().NotesEndPoint__c : 'xyz';
        url += '/notes?';
        url += 'customer=' + custNumber;
        system.debug(url);
        HttpRequest req = new HttpRequest();
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setEndpoint(url);
        req.setTimeout(Integer.ValueOf(120000));
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        Httpresponse res;
        res = new Http().send(req);
        system.debug(res);
        if(res != null && res.getStatusCode() == 200) {
            Map<String, Object> respJSON = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            system.debug('respJson'+respJSON);
            List<Object> notes;
            if(respJSON.get('notes') != null){
                notes = (List<Object>)respJSON.get('notes');
            }else{
                return null;
            }               
            map<String, CustomerLoan__c> newLoansMap = new map<String, CustomerLoan__c>();
            set<String> custNos = new set<String>();
            set<String> quoteJobSet = new Set<String>();
            if(notes != null){
                for(Object obj: notes) {
                    Map<String, Object> note = (Map<String, Object>) obj;
                    String customerNumber = note.get('customer') != null? String.valueOf(note.get('customer')) : null;
                    if(String.isNotBlank(customerNumber)){
                        custNos.add(customerNumber);
                        CustomerLoan__c newLoan = new CustomerLoan__c();
                        newLoan.CustomerNumber__c = customerNumber;
                        newLoan.InstallmentsBilled__c = note.get('lastInstallBillNo') != null? (Decimal)note.get('lastInstallBillNo') : 0;
                        newLoan.AmountBilled__c = note.get('totalBilledAmount') != null? (Decimal)note.get('totalBilledAmount') : 0;
                        newLoan.AmountFinanced__c = note.get('initialAmount') != null? (Decimal)note.get('initialAmount') : 0;
                        newLoan.EachInstallmentAmount__c = note.get('installmentAmount') != null? (Decimal)note.get('installmentAmount') : 0;
                        newLoan.QuoteJobNumber__c = note.get('job') != null? String.valueOf(note.get('job')) : null;
                        quoteJobSet.add(newLoan.QuoteJobNumber__c);
                        newLoan.Last_Refreshed_on__c = system.now();
                        newLoan.NoteNumber__c = note.get('note') != null? String.valueOf(note.get('note')) : null;
                        newLoan.SiteNumber__c = note.get('site') != null? String.valueOf(note.get('site')) : null;
                        newLoan.TotalInstallments__c = note.get('noInstallBills') != null? (Decimal)note.get('noInstallBills') : 0;
                        newLoan.Active__c = true;
                        newLoan.Loan_Type__c = 'ADT RIC';
                        if(String.isNotBlank(newLoan.QuoteJobNumber__c)){
                            newLoansMap.put(newLoan.QuoteJobNumber__c, newLoan);
                        }
                    }
                }
                //Get the Quote jon parent (Quote no)
                for(Quote_Job__c aj: [SELECT JobNo__c, ParentQuote__c,ParentQuote__r.scpq__QuoteId__c FROM Quote_Job__c WHERE JobNo__c IN: quoteJobSet]){
                    newLoansMap.get(aj.JobNo__c).QuoteOrderNumber__c = aj.ParentQuote__r.scpq__QuoteId__c;
                }  
            }
            return newLoansMap.values();
        }else if(res != null) {
            errorContent errWrap = new errorContent();
            errWrap = (errorContent)JSON.deserialize(res.getBody(),errorContent.class);
            if(errWrap.errors != null) {
                system.debug('errWrap'+errWrap.errors[0].errorMessage);
                system.debug('errWrap'+errWrap.errors[0]);   
            }
        }
        return null;
    }
    public class errorContent{
        public errorDetails[] errors;
    }
    public class errorDetails{
        public string status;
        public string errorCode;
        public string errorMessage;
    }
    public errorContent errWrap; 
}