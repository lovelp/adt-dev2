public with sharing class ADTPartnerAccountLookupSchema {
    
    public ADTPartnerAccountLookupSchema(){
        //Do Nothing
    }
    public static String SORT_BY = '';
    public static String L_DISTANCE ='lDistance';
    public static String L_ACTIVITYDATE ='lActivityDate';
    //Request for Account LookupApi
    public class AccountLookUpRequest {
        public String partnerId;
        public String callId;
        public String opportunityId;
        public String partnerRepName;
        public Name name;
        public String busLine;
        public String source;
        public Address address;
        public Phones phones;
        public String email;
    }

    public class Address {
        public String addrLine1;
        public String addrLine2;
        public String city;
        public String county;
        public String state;
        public String postalCode;
        public String postalCodeAddOn;
        public String crossStreet;
        public String subDivision;
        public Double lat;
        public Double lon;
    }

    public class Phones {
        public String phone;
        public String cell;
    }

    public class Name {
        public String first;
        public String last;
        public String company;
    }

   //Response for Account LookupApi
    public class AccountLookUpResponse {
        public String callId;
        public String opportunityId;
        public List<LocationFlags> locationFlags;
        public List<Permits> permits;
        public List<Fees> fees;
        public List<Accounts> accounts;
        public List<String> alerts;
        public String message;
        public Integer errorStatusCode;
    }
    
   public class Activites implements comparable{
        public String activity;
        public String activityDate;
        public String activityTime;
        public SalesRep salesRep;
        public String status;
        
        public Integer compareTo(Object compareTo){
            Integer returnValue = 0;
            Activites accObj = (Activites)compareTo;
            if(activitydate != null){
                if(date.valueOf(accObj.activitydate) > date.valueOf(activitydate)){
                    returnValue = 1;
                }
                else if(date.valueOf(accObj.activitydate) < date.valueOf(activitydate)){
                    returnValue = -1;
                }
            }
            return returnValue;
        }
    
    }

    public class LocationFlags {
        public String flag;
        public String description;
        public String agentMessage;
    }

    public class Fees {
        public String id;
        public String description;
        public Double minAmount;
        public Double maxAmount;
    }

    public class Permits {
        public String id;
        public String description;
        public Double price;
        public Boolean quotable;
    }

    public class Accounts implements comparable{
        public String opportunityId;
        public String acctId;
        public String status;
        public NameResponse name;
        public String addrLine1;
        public String addrLine2;
        public String city;
        public String state;
        public Phones phones;
        public String email;
        public List<String> matchList;
        public String postalCode;
        public String postalCodeAddOn;
        public String businessId;
        public String channel;
        public List<Activites> activities;
        public List<String> alertMessages;
        public Date lastActivityDate;
        public Integer levDistance;
        public Integer levAddrDistance;
        public Integer levEmailDistance;
        public Integer levAccountNameDistance;
        public Integer levPhoneDistance;
        public String leadManagementId;
        
        public Integer compareTo(Object compareTo){
            Accounts accObj = (Accounts)compareTo;
            if (ADTPartnerAccountLookupSchema.SORT_BY == ADTPartnerAccountLookupSchema.L_DISTANCE) {
                return sortByDistance(accObj);
            }

            if (ADTPartnerAccountLookupSchema.SORT_BY == ADTPartnerAccountLookupSchema.L_ACTIVITYDATE) {
                return sortByLastActivity(accObj);
            }
            return 0;
        }
        private Integer sortByLastActivity(Accounts accObj) {
           Integer returnValue = 0;
              if(accObj.lastActivityDate > lastActivityDate){
                returnValue = 1;
            }
            else if(accObj.lastActivityDate < lastActivityDate){
                returnValue = -1;
            }
            return returnValue;
        }


        private Integer sortByDistance(Accounts accObj) {
           Integer returnValue = 0;
              if(accObj.levDistance < levDistance){
                returnValue = 1;
            }
            else if(accObj.levDistance > levDistance){
                returnValue = -1;
            }
            return returnValue;
        }     
        
    }

    public class NameResponse {
        public String first;
        public String last;
        public String accountName;
    }

    public class SalesRep {
        public String name;
        public String email;
        public String phone;
    }
}