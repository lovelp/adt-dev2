@isTest
public class OutboundTestDataUtility{
    
    //Create Dialer_List_Output_Control__c
    public static Dialer_List_Output_Control__c createDLPControl(){
        
        Dialer_List_Output_Control__c dlplcont = new Dialer_List_Output_Control__c();
        //dlplcont.Dialer_List_Fields__c = ''
        dlplcont.Name = 'TestDailerOTPCON';
        dlplcont.File_Output_Name__c = 'TestDailerOTPCON.txt';
        dlplcont.File_Output_Target__c = '/axway/app_data_out/dialer/dev';
        dlplcont.File_Output_Record_Layout__c = 'Test';
        //dlplcont.Type__c = '';
        return dlplcont;
    }
    //Create Promotion__c id
    public static Promotion__c CreatePromotion(){
        Promotion__c promo = new Promotion__c();
        promo.Name='$200 off on ADT pulse';
        promo.Description__c='test Desc';
        promo.PromotionCode__c='1234';
        promo.lineofBusiness__c='Residential';
        promo.ordertype__c='R1';
        
        return promo; 
    }
    //Create DNIS__c record
    public static DNIS__c CreateDnis(Id promoId){        
        DNIS__c dnis= new DNIS__c();
        dnis.name='1113335555';
        dnis.Start_Date__c=system.TODAY();
        dnis.End_Date__c=system.TODAY().addDays(4);
        dnis.Lead_Source__c='Ving Rhames';
        dnis.LineOfBusiness__c='Residential';
        dnis.Promotions__c=promoId;             
        return dnis;
    }
    //Create campaign 
    public static Campaign createCampaign(Id DnisId, Id dlplcontid){
        
        Campaign newCamp = new Campaign();
        newCamp.Name = 'TestCamp';
        newCamp.DNIS__c = dnisId; 
        newCamp.Dialer_List_Output_Control__c = dlplcontid;
        newCamp.Email_Notification_User__c = Userinfo.getUserId();
        newCamp.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('CampaignJob').getRecordTypeId();
        newCamp.Status = 'Planned';
        newCamp.Type = 'Master';
        newCamp.ImmediateRun__c = true;
        return newCamp;
        
    }
    //Create Campaign_Selection_Criteria_item__c
    public static Campaign_Selection_Criteria_item__c cscItem(Id campId){
        
        Campaign_Selection_Criteria_item__c cscitem = new Campaign_Selection_Criteria_item__c();
        cscitem.Campaign__c = campId;
        cscitem.ObjectsList__c = 'Account';
        return cscitem;
    }
    //Create Campaign member
    public static CampaignMember createNewCampMem(Id campId,Id accId, Id conctId){
        CampaignMember campmem = new CampaignMember();
        campmem.CampaignId = campId;
        campmem.Account__c = accId;
        campmem.ContactId = conctId;
        return campmem;
    }
    
    //Crate contact
    public static Contact createContact(Id accId){
        
        Contact con = new Contact(lastname = 'TestLastName', AccountId = accId, Phone='+11234567890');
        return con;
        
    }

}