@RestResource(urlMapping = '/getInstallAppointmentSlots/*')
global class ADTPartnerGetDLLAppointmentAPI {
    @HttpPost
    global static void doPost ()
    {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        
        ADTPartnerGetDLLAppointmentController getDLLAppointmentController = new ADTPartnerGetDLLAppointmentController ();
        //getDLLAppointmentController.parseJSONRequest (request.requestBody.toString());
        
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(getDLLAppointmentController.parseJSONRequest (request.requestBody.toString())));
        
        response.statusCode = getDLLAppointmentController.statusCode;
    }

}