global class DeleteRecordsBatchClass implements Database.batchable<SObject>,Database.stateful{
public List<objWrapper> objWrapList = new List<objWrapper>();
   global Iterable<SObject> start(Database.batchableContext info){ 
      return new CallIterableClass();
   }
   
   global void execute(Database.batchableContext info, List<SObject> sObjs){ 
       objWrapper objWrap;
       Set<string> objName = new set<string>();
       Map<string,set<string>> sDataList = new Map<string,set<string>>();
       for(SObject s:sobjs){
           if(sDataList.containskey(string.valueof(s.getSObjectType()))){
               set<string> sData = sDataList.get(string.valueof(s.getSObjectType()));
               sData.add(s.id);
               sDataList.put(string.valueof(s.getSObjectType()),sData);
           }else{
               sDataList.put(string.valueof(s.getSObjectType()),new set<string>{s.id});
           }
           objName.add(string.valueof(s.getSObjectType()));
       }
       for(string objN:objName){
           objWrap = new objWrapper();
           integer totalRecords=0;
           if(sDataList.containskey(objN)){
               totalRecords = sDataList.get(objN).size();    
           }
           objWrap.objName = objN;
           objWrap.totRec =totalRecords;
           objWrapList.add(objWrap);
       }
       System.debug('The onjWrapList is'+objWrapList);
       delete sObjs;
   }
    public class objWrapper{
        public string objName{get;set;}
        public integer totRec{get;set;}
    }

    global void finish(Database.BatchableContext BC){
        //Send an email to the User after your batch completes
        if(objWrapList.size()>0){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
        User u = [SELECT Name, Email, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        String[] toAddresses = new String[] {u.email};                
        mail.setToAddresses(toAddresses);        
        mail.setSubject('Number of Records Deleted Per Object');        
        mail.setPlainTextBody('Number of Records Deleted Per Object');
        System.debug('The objWrapList is'+objWrapList);
        string htmlBody = '<div> ';
        htmlBody += '<table> <tr>';
        htmlBody += '<th>Object Name</th>';
        htmlBody += '<th>Number of Records Deleted</th>';
        htmlBody += '</tr>';
        for(objWrapper o:objWrapList){
            htmlBody += '<tr>';
            htmlBody += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ' + o.objName + '</td>';
            htmlBody += '<td> &nbsp;&nbsp;&nbsp;' + o.totRec + '</td>';
            htmlBody += '</tr>';
        }
        htmlBody +='</table> </div>';
        mail.setHtmlBody(htmlBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}