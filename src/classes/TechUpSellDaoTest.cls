/************************************* MODIFICATION LOG ********************************************************************************************
* 
* TechUpSellDaoTest
*
* DESCRIPTION :
* Test Coverage for TechUpSellDao, TechUpSellProductController class
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE             Ticket         REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* 
* Jitendra Kothari            11/22/2018      		-    - Test Coverage
*/
@isTest (SeeAllData = false)
private class TechUpSellDaoTest {
	private static Account acc;
	private static void createTestdata(){
		list<ResaleGlobalVariables__c> reslist = new list<ResaleGlobalVariables__c>();
        ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
        res.name = 'DataRecastDisableAccountTrigger';
        res.value__c = 'TRUE';
        reslist.add(res);
        ResaleGlobalVariables__c res1 = new ResaleGlobalVariables__c();
        res1.name = 'GlobalResaleAdmin';
        res1.value__c = 'Global Admin';
        reslist.add(res1);
        ResaleGlobalVariables__c res2 = new ResaleGlobalVariables__c();
        res2.name = 'EventRecTypeNameAllowed';
        res2.value__c = 'Company Generated Appointment,Field Sales Appointment';
        reslist.add(res2);
        ResaleGlobalVariables__c res3 = new ResaleGlobalVariables__c();
        res3.name = 'DisqualifyEventTaskCode';
        res3.value__c = 'TRV';
        reslist.add(res3);
        insert reslist;
		acc = TestHelperClass.createAccountData();
	}
    static testMethod void myUnitTest() {
        createTestdata();
        Test.startTest();
        Test.setCurrentPageReference(Page.TechUpSell);
		ApexPages.Standardcontroller stdcon = new Apexpages.Standardcontroller(acc);
        TechUpSellProductController techup = new TechUpSellProductController(stdcon);
        techup.setTechUpSellDao(new TechUpSellDao());
        TechUpSellProductController.saveCatalog(acc.Id, '[{\"pName\":\"Product A\",\"pQuantity\":\"3\",\"pNotes\":\"Test Notes\"}]');
        list<ProductCatalog__c> prodList = [Select Id From ProductCatalog__c Where ProductAccount__c = :acc.Id];
        system.assertEquals(false, prodList.isEmpty());
        system.assertEquals(1, prodList.size());
        list<Disposition__c> dispList = [Select Id From Disposition__c Where AccountID__c = :acc.Id];
        system.assertEquals(false, dispList.isEmpty());
        system.assertEquals(1, dispList.size());
        techup = new TechUpSellProductController(stdcon);
        Test.stopTest();
    }
}