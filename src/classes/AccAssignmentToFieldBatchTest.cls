//Mounika Anna -- Lead sharing for NSCAccounts -- HRM- 3137
//AccountAssignmentTofieldBatchProcessor test class

@istest
public class AccAssignmentToFieldBatchTest {
 
    static testMethod void testNSCAccountExpirationToField() {        
        List<Account> accts = new List<Account>();
        User admin = TestHelperClass.createAdminUser();        

        system.runAs(admin) {
            TestHelperClass.createReferenceDataForTestClasses();
            List<Equifax__c> elist = new List<Equifax__c>();
            elist.add(new Equifax__c(Name='Employee Condition Code', value__c='CME3'));
            elist.add(new Equifax__c(Name='Employee Risk Grade', value__c='U'));
            elist.add(new Equifax__c(Name='Default Condition Code', value__c='CAE1'));
            elist.add(new Equifax__c(Name='Default Risk Grade', value__c='W'));
            insert elist;       
            
             //Global unassigned user
            Profile pgu = [select id from profile where name='System Administrator'];
            UserRole urExisting = [select id, OpportunityAccessForAccountOwner from UserRole where name like '%Sales Group VP%' limit 1];
            User gbladmin = new User(alias = 'utsr1', email='unitTestSalesAdmin1@testorg.com', 
                emailencodingkey='UTF-8', lastname='TestGlobal ADMIN', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = pgu.Id, userroleid = urExisting.Id, EmployeeNumber__c = TesthelperClass.generateEmployeeNumber(),
                timezonesidkey='America/Los_Angeles', username='testgbladmin@testorg.com',
                StartDateInCurrentJob__c = Date.today());    
            insert gbladmin;
            
            // Data conversion user 
            Profile pr = [select id from profile where name='ADT Integration User']; 
            User intU = new User(alias = 'utsm1', email='unitTestIntegration1@testorg.com', 
            emailencodingkey='UTF-8', firstname='TestIntegration', lastname='User', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = pr.Id, EmployeeNumber__c = 'T987654123',
            timezonesidkey='America/Los_Angeles', username='unitTestIntegration1@testorg.com',
            StartDateInCurrentJob__c = Date.today());
            insert intU; 
            
            // Batch Global Variables
            List<BatchGlobalVariables__c> batchGloballist = new List<BatchGlobalVariables__c>();
            batchGloballist.add(new BatchGlobalVariables__c(Name='LS_DaysBeforeReassignmentToField', value__c='1'));
            batchGloballist.add(new BatchGlobalVariables__c(Name='LS_NoOfMonthsOfRecordCreation', value__c='1'));
            insert batchGloballist;
            
            if(BatchGlobalVariables__c.getinstance('LS_DispositionOwner') != null){
                BatchGlobalVariables__c batchGlobalDispoOwner = new BatchGlobalVariables__c();
                batchGlobalDispoOwner = BatchGlobalVariables__c.getinstance('LS_DispositionOwner');
                batchGlobalDispoOwner.Value__c = intU.id;
                update batchGlobalDispoOwner;
            }
            
            //Affiliate creation
            Affiliate__c aff = new Affiliate__c();
            aff.Name = 'USAA';
            aff.Active__c = true;
            insert aff;
            
            // Call Disposition
            Call_Disposition__c callDispo = new Call_Disposition__c();
            callDispo.Name = 'EC Transfer to Loyalty';
            callDispo.Action__c = 'Transferred';
            callDispo.Active__c = true;
            insert callDispo;
            
            // Lead Sharing Criteria
            List<LeadSharingCriteria__c> criteriaList= new List<LeadSharingCriteria__c>();
            criteriaList.add(new LeadSharingCriteria__c(Active__c = true, Affiliate__c = aff.Id));
            criteriaList.add(new LeadSharingCriteria__c(Active__c = true, CallDisposition__c = callDispo.Id));
            insert criteriaList;
 
            List<Postal_Codes__c> pcs = new List<Postal_Codes__c>();
            Postal_Codes__c p = new Postal_Codes__c (name='221o2', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100');
            pcs.add(p);
            Postal_Codes__c p1 = new Postal_Codes__c (name='221o3', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100');
            pcs.add(p1);
            insert pcs;
            User u = TestHelperClass.createSalesRepUser();
            u.Username = 'testsalesrep@testorg.com';
            update u;
            
            User mu= TestHelperClass.createManagerUser();
            Territory__c t = TestHelperClass.createTerritory();
            TerritoryAssignment__c ta = TestHelperClass.createTerritoryAssignment(t, u, p);
            ManagerTown__c mt = TestHelperClass.createManagerTown(mu,p1,'Resi Direct Sales');
            
            Account expiredAccount = TestHelperClass.createAccountData();
            expiredAccount.DispositionDate__c = system.now().addDays(-2);
            User Nscrep = createNSCRepUser();
            Nscrep.Username = 'testnscrep@testorg.com';
            update Nscrep;
            expiredAccount.Contact_Owner__c = true;
            expiredAccount.OwnerId = Nscrep.Id;
            accts.add(expiredAccount);
            
            Address__c addr = [Select Id, PostalCode__c from Address__c where ID = :expiredAccount.AddressID__c];
            
            Account validAccount = TestHelperClass.createAccountData(addr);
            validAccount.DispositionDate__c = system.now().addDays(-2);
            validAccount.OwnerId = Nscrep.Id;
            validAccount.Contact_Owner__c = true;
            accts.add(validAccount);
            
            // CallData For Valid Account
            Call_Data__c cd = new Call_Data__c();
            cd.Name = 'Test Call Data';
            cd.Account__c = validAccount.Id;
            cd.Call_Disposition__c = callDispo.Id;
            insert cd;
            
            Account noPrevOwner = TestHelperClass.createAccountData(addr);
            noPrevOwner.PostalCodeID__c = p1.id; 
            noPrevOwner.DispositionDate__c = system.now();
            noPrevOwner.OwnerId = mu.Id;
            noPrevOwner.Contact_Owner__c = true;
            accts.add(noPrevOwner);
            
            Account sgAccount = TestHelperClass.createAccountData(addr);
            sgAccount.DispositionDate__c = system.now().addDays(-2);
            sgAccount.OwnerId = Nscrep.Id;
            sgAccount.ADTEmployee__c = true;
            sgAccount.Contact_Owner__c = true;
            accts.add(sgAccount);
            update accts;
             
            Account_Affiliation__c accaff = new Account_Affiliation__c();
            accaff.Active__c = true;
            accaff.Affiliate__c = aff.Id;
            accaff.Member__c = validAccount.id;
            accaff.MemberNumber__c='1234';
            insert accaff;
        }
        
        String aidstr = '';
        for (Account a : accts) {
            aidstr += '\'' + a.Id + '\',';
        }
        aidstr = aidstr.substring(0, aidstr.length()-1);
        
        test.startTest();        
            AccountAssignmentToFieldBatchProcessor aa = new AccountAssignmentToFieldBatchProcessor();
            aa.strquery = 'Select Id, postalcodeId__c, SitePostalCode__c, ADTEmployee__c,Contact_Owner__c, Business_Id__c, Channel__c, ProcessingType__c, UnassignedLead__c, DateAssigned__c, NewLead__c, Affiliation__c, OwnerId, Owner.Name, QueriedSource__c, TelemarLeadSource__c, GenericMedia__c, RecordTypeId, RecordType.Name, Ownership_Transfer_Details__c, (Select Id,Active__c,Affiliate__c from Account_Affiliations__r Where Affiliate__c != null), (select id,Account__c,Call_Disposition__r.Name, Call_Disposition__c, Call_Disposition__r.Action__c, Call_Disposition__r.Tier_One__c,Call_Disposition__r.Tier_One__r.Name from CallData__r) From Account where ID IN (' + aidstr + ') ';
            Database.executeBatch(aa);        
        test.stopTest();
        
        List<Account> aconfirm = [Select Id, OwnerId from Account where ID IN :accts];
    }
    
    //AccountAssignmentTofieldBatchProcessor test class for Scheduler
    static testMethod void testAccAssignmenttoFieldschedule() {
        TestHelperClass.createReferenceDataForTestClasses();
        // Batch Global Variables
        List<BatchGlobalVariables__c> batchGloballist = new List<BatchGlobalVariables__c>();
        batchGloballist.add(new BatchGlobalVariables__c(Name='LS_ADTEmployee', value__c='1'));
        batchGloballist.add(new BatchGlobalVariables__c(Name='LS_DataSourceExclusion', value__c='WEB'));
        batchGloballist.add(new BatchGlobalVariables__c(Name='LS_DaysBeforeReassignmentToField', value__c='1'));
        batchGloballist.add(new BatchGlobalVariables__c(Name='LS_NoOfMonthsOfRecordCreation', value__c='1'));
        insert batchGloballist;
        
        Test.StartTest();
        AccountAssignmentToFieldScheduler aa = new AccountAssignmentToFieldScheduler();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, aa); 
        Test.stopTest(); 
    }      

  
  //------------------------------------------------------------------------------------------------------------------------------
    //createNSCRepUser()
    //------------------------------------------------------------------------------------------------------------------------------
    public static User createNSCRepUser() {
        Profile p1 = [select id from profile where name='ADT NSC Sales Representative'];
        UserRole urExisting = [select id, OpportunityAccessForAccountOwner from UserRole where name like '%Rep' limit 1];
        User NSCRep = new User(alias = 'utsr1', email='unitTestSalesRep1@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p1.Id, userroleid = urExisting.Id, EmployeeNumber__c = TestHelperClass.generateEmployeeNumber(),
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com',
                StartDateInCurrentJob__c = Date.today());
        insert NSCRep;
        return NSCRep;
    }
}