/************************************* MODIFICATION LOG ********************************************************************************************
* ADTGoMapController
* 
DESCRIPTION : Obtains all dynamic data required by the ADTGo Map Display
*             It is a Controller in the SFDC MVC architecture. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Mike Tuckman    				8/15/13				- Original Version
*
*													
*/

public class ADTGoMapController
{ 
    // contains ADTGoMapItems for display on the map 
    public List<ADTGoMapItem> locations {get;set;}
    // a delimited list of accounts for mapping
    public String mapDataPointsStr {get; set;}
    public String mapAddress {get;set;}
    public String mapUpdated {get;set;}
    public String mapUpdAge {get;set;}
    public String mapDoUpdate {get;set;}
    public String mapDoPSAPUpdate {get;set;}
    public Integer mapOffset {get;set;}
    public String mapPSAPData {get;set;}
    public String mapDescription {get;set;}
    public String mapCustTime {get;set;}
    public String mapDispo {get;set;}
    public String mapDoPoller {get;set;}
    public Boolean mapDispMap {get;set;}
    public List<SelectOption> mapDispoOptions {get;set;}
    
    // delimiter that divides accounts to be mapped; also used to delimit disposition updates from the page
    private static final String POINT_DELIMITER = '~~~~';
    // delimiter that divides an account id and a disposition code
    private static final String DATA_DELIMITER = '#';
    
    // Context User's data
    public User currUser {get;set;}
    
    // corresponds to the starting point of the search; can be an account, a user entered address, or the user's current location
    public ADTGoMapItem startingPoint {get;set;}
    
    // holds the account Id supplied as a query string parameter
    public String mid {get;set;}
    // holds the account Id supplied as a query string parameter
    public String mAid {get;set;}

    // holds the latitude and longitude supplied as a query string parameter
    public Decimal mLat {get;set;}
    public Decimal mLon {get;set;}
    
	public ADTGoGeoCode.ADTGoGeoCodeRevResults revResults {get;set;}
	public String mPollerInterval {get;set;}
    public String mMessage {get;set;}
    public String mDebug {get;set;}
    public ADTGoPSAP.PSAPResults mapPSAPResults;
    
    public String debugEmail;
	
	private Decimal demoSeq;
	private Id histID;
	private String priorZip;
	private Boolean firsttime;
    private String userDesc;
    private ADTGoGeoCode.ADTGoTimeZoneResults tz;
    private String doDebug;
    public String restResponse;
    private String displayDemographics;
    private String eventLogging;
    private ID logID;
    private ADTGoEventLog__c eventLog;
    
    private Boolean initRun;
   
    //------------------------------------------------------------------------------------------------------------------------------
    //init()     "hairColor": string,
    //------------------------------------------------------------------------------------------------------------------------------
    public PageReference init()
    {       	
        try {
            // look for query string parameters
            //System.debug(ApexPages.currentPage().getParameters());
            mid = ApexPages.currentPage().getParameters().get('id'); 
            doDebug = ApexPages.currentPage().getParameters().get('debug');
            System.debug('ID...:' + mid); 
            system.debug(doDebug);
            
            if (mid == null){
                mMessage = 'Event not passed';
                return null;
            }
 
            String[] parts = mid.split('-');
            if (mid <> '9999999997' && (parts[0].length() <> 8 || parts[1].length() <> 4 || parts[2].length() <> 4 || parts[3].length() <> 4 || parts[4].length() <> 12)) {
                mMessage = 'Event ID is invalid';
                return null;
            }
            
            /*if (mid == 'e843fc70-6145-4e74-a410-0c522bd97f45') {
                mid = '9999999997';
                mapDescription = 'White Male, 5 feet 7 inches, 220 pounds';
            }*/
            
            mPollerInterval = '5';
            mapDoPoller = 'true';
            mMessage = '';
            initRun = true;
            mapDispMap = true;
    
            // initialize lists that will contain search results
            locations = new List<ADTGoMapItem>();
            
            demoSeq = 0;
            
            mapDispoOptions = getOptions();
            displayDemographics = ADTGoSetting.getSetting('Demographics');
            eventLogging = ADTGoSetting.getSetting('EventLogging');
            
            firsttime = true;
            getLocations();        
            firsttime = false;
    
            prepareMapData();
            
            eventLog = null;
            createUpdateEventLog(null);
            
        }
        catch(Exception e){
            mMessage = 'Init failed: ' + e.getMessage();
        }

        
        return null;
    } 
    
    public Pagereference closeCall(){
        if (String.isBlank(mapDispo)) {
            mMessage = 'Please select Disposition to close call';
            return null;
        }
        
        Map <String, ADTGo_Dispos__c> dispos = ADTGo_Dispos__c.getAll();
        
        String resp = ADTGoRest.setDisposition(mid, mapDispo, dispos.get(mapDispo).DispoDisplay__c);
        
        if ( !String.isBlank(resp) ) {
        	mMessage = 'Disposition Callout Failed - ' + resp;
            return null;
        }

        if( !dispos.get(mapDispo).StopUpdates__c ) {
            mMessage = 'Call disposition has been sent - call is not closed - updates continue';
            createUpdateEventLog(mapDispo, false);
            return null;
        }

        mMessage = 'Call has been dispositioned and closed - Please close window';
        mapDoUpdate = 'false';
        mapDoPSAPUpdate = 'false';
        mapDoPoller = 'false';
        createUpdateEventLog(null, true);
        return null;
    }
    
    public Pagereference updatePage(){
        try{
            locations.clear();
            getLocations();
            prepareMapData();
        }
        catch(Exception e){
            mMessage = 'Page Updated failed: ' + e.getMessage();
        }
    	
    	return null;
    }
    
    public void getLocations(){
    	
        ADTGoRest.UserResults userResults;

		mapDoUpdate = 'false';
		mapDoPSAPUpdate = 'false';
        mPollerInterval = '5';
        revResults = null;
            
        try{
            if (mid == '9999999997'){
                userResults = ADTGoRest.getUser(mid, demoSeq);
                demoSeq = userResults.demoseq;
                mapDescription = 'White Male, 5 feet 7 inches, 220 pounds : ' + demoSeq.toPlainString();
            }
            else {
                userResults = ADTGoRest.getUserCanopy(mid);
                system.debug(ADTGoRest.canopyResponse.locations);
                mapDescription = '';
                if(String.isNotBlank(userResults.firstname)) mapDescription += userResults.firstname + ' ' ;
                if(String.isNotBlank(userResults.lastname)) mapDescription += userResults.lastname + ' ' ;
                if(String.isNotBlank(userResults.phone)) mapDescription += userResults.phone + ' ' ;
                if(displayDemographics != null && (displayDemographics.equalsIgnoreCase('true')  || displayDemographics.equalsIgnoreCase('t') || displayDemographics.equalsIgnoreCase('y') || displayDemographics.equalsIgnoreCase('yes'))) {
                    if(String.isNotBlank(userResults.attr.race)) mapDescription += userResults.attr.race + ' ' ;
                    if(String.isNotBlank(userResults.attr.gender)) mapDescription += userResults.attr.gender + ' ' ;
                    if(String.isNotBlank(userResults.attr.hairColor)) mapDescription += userResults.attr.hairColor + ', ';
                    if(String.isNotBlank(userResults.attr.heightInInches)) mapDescription += userResults.attr.heightInInches + ' inches, ';
                    if(String.isNotBlank(userResults.attr.weightInLbs)) mapDescription += userResults.attr.weightInLbs + ' pounds';
                }
            }
        }
        catch(Exception e){
            mMessage = 'Request to retrieve event location failed: ' + e.getMessage();
            return;
        }
		
        if(String.isNotBlank(userResults.message)) {
            system.debug(userResults.message);
            system.debug(String.isNotBlank(userResults.message));
        	mMessage = userResults.message;
        }
		
		if (initRun){
            Decimal lat;
            Decimal lon;
            for (ADTGoRest.ADTGoLocations l : ADTGoRest.canopyResponse.locations) {
                lat = decimal.valueOf(l.location.split(',')[0]);
                lat.setScale(5);
                lon = decimal.valueOf(l.location.split(',')[1]);
                lon.setScale(5);
                revResults = ADTGoGeoCode.GeoCodeLatLon(lat, lon);
				locations.add(new ADTGoMapItem(lat, lon, revResults.address, l.created));
            }
            initRun = false;
			mLat = userResults.loc.lat;
			mLon = userResults.loc.lon;
			mapDoUpdate = 'true';
            tz = ADTGoGeoCode.TimeZoneLatLon(mLat, mLon);
            mapOffset = tz.offset;
		}
		else if (userResults.loc.lat != mLat || userResults.loc.lon != mLon){
			revResults = ADTGoGeoCode.GeoCodeLatLon(userResults.loc.lat, userResults.loc.lon);
			mLat = userResults.loc.lat;
			mLon = userResults.loc.lon;			
			locations.add(new ADTGoMapItem(userResults.loc.lat, userResults.loc.lon, revResults.address, userResults.updated));
			mapDoUpdate = 'true';
            tz = ADTGoGeoCode.TimeZoneLatLon(mLat, mLon);
            mapOffset = tz.offset;
		}

        if (revResults != null && revResults.zipcode != priorZip){
			priorZip = revResults.zipcode;
            //mapPSAPResults = ADTGoPSAP.getPSAP(revResults);
            mapPSAPResults = ADTGoPSAP.getPSAP(userResults.loc.lat, userResults.loc.lon);
            if (String.isNotBlank(mapPSAPResults.CountyName)) {
                mapPSAPData = '';
                if (String.isNotBlank(mapPSAPResults.CountyName)) mapPSAPData += 'County: ' + mapPSAPResults.CountyName;
                if (String.isNotBlank(mapPSAPResults.Agency)) mapPSAPData += ' | Agency: ' + mapPSAPResults.Agency;
                if (String.isNotBlank(mapPSAPResults.OperatorPhone)) mapPSAPData += ' | Operator: ' + mapPSAPResults.SitePhone;
                if (String.isNotBlank(mapPSAPResults.PolicePhone)) mapPSAPData += ' | Police: ' + mapPSAPResults.PolicePhone;
                if (String.isNotBlank(mapPSAPResults.FirePhone)) mapPSAPData += ' | Fire: ' + mapPSAPResults.FirePhone;
                if (String.isNotBlank(mapPSAPResults.EMSPhone)) mapPSAPData += ' | EMS: ' + mapPSAPResults.EMSPhone;
            }
            else mapPSAPData = '*' + mapPSAPData;
		}
		
		if (revResults != null) mapAddress = revResults.address;
		if (userResults.updated != null) mapUpdated = userResults.updated.format();
		if (userResults.updated != null) mapUpdAge = ADTGoDateDiff.diff(userResults.updated, DateTime.now());
        if (tz != null) mapCustTime = DateTime.now().format('h:mm a',tz.timeZoneId);
                
        if (userResults.loc.lat == null || userResults.loc.lat == 0) mMessage = 'No location returned';

        if (doDebug != null) {
            mDebug = ADTGoRest.debugResponse;
            if (mapDoUpdate == 'true') {
                ADTGoDebugLogs__c dLog = new ADTGoDebugLogs__c();
                dLog.Latitude__c = mLat;
                dLog.Longitude__c = mLon;
                dLog.EventID__c = mid;
                dLog.JSONResponse__c = ADTGoRest.debugResponse;
                insert dLog;
            }
        }
    }
    

    
    //------------------------------------------------------------------------------------------------------------------------------
    // prepareMapData()
    //------------------------------------------------------------------------------------------------------------------------------
    public void prepareMapData() {  
    	mapDataPointsStr = '';
   		for(ADTGoMapItem mi : locations)
        {
       		mapDataPointsStr += mi.asString();
           	mapDataPointsStr += POINT_DELIMITER;       
        }
        
       System.debug('prepareMapData-----------mapDataPointsStr:' + mapDataPointsStr);
    	
    }
    
    private List<SelectOption> getOptions() {
        List<SelectOption> options = new List<SelectOption>();
		Map <String, ADTGo_Dispos__c> dispos = ADTGo_Dispos__c.getAll();
        options.add(new SelectOption('','-- Select Call Disposition --'));
    	for(String code : dispos.keySet()){
            options.add(new SelectOption(code,dispos.get(code).DispoDisplay__c));
		}
        return options;
    }
    
    private void createUpdateEventLog(String dispo) {
        createUpdateEventLog(dispo, null);
    }
    
    private void createUpdateEventLog(String dispo, Boolean callClosed) {
    		system.debug('** eventLogging:' + eventLogging);
    		if(eventLogging != null && (eventLogging.equalsIgnoreCase('true')  || eventLogging.equalsIgnoreCase('t') || eventLogging.equalsIgnoreCase('y') || eventLogging.equalsIgnoreCase('yes'))) {
	    		if (eventLog == null) {
	    			eventLog = new ADTGoEventLog__c();
	    			eventLog.name = mid;
	    			if (dispo != null) eventLog.Disposition__c = dispo;
                    if (callClosed !=null) eventLog.CallClosed__c = callClosed;
	    			eventLog.IPAddress__c = GetUserIPAddress();
	    			insert eventLog;
	    			system.debug('** eventLogging: inserted');
	    		}
	    		else {
	    			eventLog.name = mid;
	    			if (dispo != null) eventLog.Disposition__c = dispo;
                    if (callClosed !=null) eventLog.CallClosed__c = callClosed;
	    			//eventLog.IPAddress__c = GetUserIPAddress();
	    			update eventLog;
	    			system.debug('** eventLogging: updated');
	    		}
	    }
    }

	public static String GetUserIPAddress() {
		
		string ReturnValue = '';
	
		ReturnValue = ApexPages.currentPage().getHeaders().get('True-Client-IP');
				
		if (String.isBlank(ReturnValue)) {
			ReturnValue = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
		} // get IP address when no caching (sandbox, dev, secure urls)
				
		system.debug('USER IP ADDRESS: ' + ReturnValue);
		system.debug('** Headers');
		for ( String key : ApexPages.currentPage().getHeaders().keySet() ){
    			system.debug(key + ':' + ApexPages.currentPage().getHeaders().get(key));
		}
			
		return ReturnValue;
		
	} // GetUserIPAddress


}