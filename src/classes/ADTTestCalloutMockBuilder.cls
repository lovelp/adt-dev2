/*
 * Created by Nitin
 * This class is a generic class which can be used to 
 * create mock response for the API callout in test class
 * 
 */
public class ADTTestCalloutMockBuilder implements HttpCalloutMock {
    
    protected Integer code;
    protected String respBody;
    
    /* code - response code for the test Mock response
     * respBody - response body for the test Mock Reponse
     */
    public ADTTestCalloutMockBuilder(Integer code, String respBody) {
        this.code = code;
        this.respBody = respBody;
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(code);
        resp.setBody(respBody);
        return resp;
    }
}