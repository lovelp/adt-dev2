/************************************* MODIFICATION LOG ********************************************************************************************
* NSCAffiliateCaptureController
*
* DESCRIPTION : Controller for the visualforce page in the console to handle account affiliations
*  NOTE: If last name is same for two accounts eg, John Doe & Jane Doe, USAA will still count agains the failed attempts
*        So restricting the time & number of attempts the call is made to USAA.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan               12/04/2015            -Original Version
* Leonard Alarcon               03/18/2016            - Support for USAA
* TCS                           04/24/2017            - Military Rank display for USSA  *    
* Siju Varghese                 07/22/2019            - HRM-9702 - Add 403 Error code for  - User Locked Messages
* Nishanth Mandala              10/8/2019             - HRM-11053 - Affiliations lookup display based on channel match
*/

global without sharing class NSCAffiliateCaptureController{
    public Map<Id,Account_Affiliation__c> accAffMap { get;set; }
    public Map<String,Affiliate__c> affNameRecordMap{ get;set; }
    public List<String> affiliateNames              { get;set; }
    public String logoURL                           { get;set; }
    public String newMemberNumber                   { get;set; }
    public String newFirstname                      { get;set; }
    public String newLastname                       { get;set; }
    public String newDob                            { get;set; }
    public String newphonenumber                    { get;set; }
    public String newCity                           { get;set; }
    public String newState                          { get;set; }
    public String newZip                            { get;set; }
    public String newEmail                          { get;set; }
    public String updatedMemberNumber               { get;set; }
    public String affiliateDescription              { get;set; }
    public String selectedAffiliateValue            { get;set; }
    public String affiliateListJSON                 { get;set; }
    public string accChannel                        { get;set; }  // HRM 11053 - Affiliations lookup display based on channel match (Home health)
    public String accAffRecId                       { get;set; }
    public Boolean showExistingAffiliations         { get;set; }
    public Boolean isRequired                       { get;set; }
    public Boolean isUSAA                           { get;set; }
    public Boolean isValidated                      { get;set; }
    public Boolean addAffiliation;
    public String aId;
    public String affiliateId;
    public decimal tMins;
    public decimal usaaFailCount;
    public decimal usaaTimeout;    

    
    public Account a;
    
    //Military Rank Added by TCS
    public String sMilRank    { get;set; }

    /**
     *  @ Constructor
     **/
    global NSCAffiliateCaptureController(){
        
        a = new Account();
        aId = '';
        affiliateId = '';
        accAffRecId = '';
        selectedAffiliateValue = '';
        newMemberNumber = '';
        updatedMemberNumber = '';
        logoURL = '';
        addAffiliation = false;
        showExistingAffiliations = false;
        isRequired = false;
        isUSAA = false;
        newFirstname = '';
        newLastname = '';
        newDob = '';
        newphonenumber = '';
        newCity = '';
        newState = '';
        newZip = '';
        newEmail = '';
        tMins = 0;
        isValidated = false;
        usaaFailCount = 0;       
        usaaTimeout = 0;
        affiliateNames = new List<String>();
        affNameRecordMap = new Map<String,Affiliate__c>();
        sMilRank = ''; //Added by TCS
        
        // Get the number of attempts to be made for USAA
        if(Integer.valueof(ResaleGlobalVariables__c.getinstance('USAAAttempts').value__c) > 0){
            usaaFailCount = Integer.valueof(ResaleGlobalVariables__c.getinstance('USAAAttempts').value__c);
        }else{
            usaaFailCount = 4;
        }
         
        if(Integer.valueof(ResaleGlobalVariables__c.getinstance('USAATimeout').value__c) > 0){
            usaaTimeout = Integer.valueof(ResaleGlobalVariables__c.getinstance('USAATimeout').value__c);
        }else{
            usaaTimeout = 10;
        }

        // Getting the default value of the partnership from DNIS and displaying the description
        try{
            affiliateId = ApexPages.currentPage().getParameters().get('affId');
            if(affiliateId != Null && affiliateId != ''){
                selectedAffiliateValue = [Select Name From Affiliate__c where ID =: affiliateId AND Active__c = true ORDER BY name].Name;
            }
        }catch(Exception err){
            system.debug('Dev: Error is ...'+err.getMessage());    
        }
            
        // Getting the account Id from URL and getting account affiliate details
        aId = ApexPages.currentPage().getParameters().get('aId');
        if(aId != Null && aId != ''){
            accAffMap = new Map<Id,Account_Affiliation__c>([Select Id, Name, Active__c, Builder__c, MemberNumber__c, Affiliate__r.Name, Affiliate__r.Affiliate_Description__c, Military_Rank__c From Account_Affiliation__c Where Member__c =: aId]);
            a = [select LastName__c, FirstName__c, Channel__c from Account where id = :aId]; // HRM 11053 - Affiliations lookup display based on channel match (Home health)
            accChannel = a.channel__c; // HRM 11053 - Affiliations lookup display based on channel match (Home health)
        }        
        
        if(accAffMap.size()>0)
            showExistingAffiliations = true;

        for (Affiliate__c aff : [Select Id,Name,Affiliate_Description__c,Channel__c, Member_Required__c, Member_Validated__c From Affiliate__c Where Active__c = true ORDER BY name]){ // HRM 11053 - Affiliations lookup display based on channel match (Home health)
            if(string.isBlank(aff.Channel__c) || ((string.isNotBlank(aff.Channel__c) && aff.channel__c.contains(accChannel)) )) // HRM 11053 - Affiliations lookup display based on channel match (Home health)
            {
            affiliateNames.add(aff.Name);
            affNameRecordMap.put(aff.Name,aff);
            }     
        }
        if(affNameRecordMap.size()==0){ // HRM 11053 - Affiliations lookup display based on channel match (Home health)
            selectedAffiliateValue = '';  //to prevent null pointer exception in the below If block when the map is empty - // HRM 11053 - Affiliations lookup display based on channel match (Home health)
        }
        
        affiliateListJSON = JSON.serialize(affiliateNames);
        if(selectedAffiliateValue != Null && selectedAffiliateValue != ''){
            logoURL = getLogoURL(affNameRecordMap.get(selectedAffiliateValue).Id);
          }
    }

    /**
    *  Adds new Account Affiliate extended
    **/
    public pagereference addAccountAffiliateExt(String sMemberNumber){        
        newMemberNumber = sMemberNumber;
        addAccountAffiliate();
        return null;
    }

    /**
    *  Adds new Account Affiliate
    **/
    public pagereference addAccountAffiliate(){
        system.debug('Dev: Selected Typeahead value is: '+selectedAffiliateValue);
        
            
        if(selectedAffiliateValue != Null && selectedAffiliateValue != ''){       
            affiliateDescription = affNameRecordMap.get(selectedAffiliateValue).Affiliate_Description__c;
            logoURL = getLogoURL(affNameRecordMap.get(selectedAffiliateValue).Id);
            isRequired = affNameRecordMap.get(selectedAffiliateValue).Member_Required__c;
            isValidated = affNameRecordMap.get(selectedAffiliateValue).Member_Validated__c;
            if (selectedAffiliateValue == 'USAA' && isValidated == true)
                isUSAA = true;
            else
                isUSAA = false;
            system.debug('DEV: Add affiliation value? '+addAffiliation);
            if(addAffiliation){
                showExistingAffiliations = true;
                system.debug('DEV: showExistingAffiliations? '+showExistingAffiliations);
                Account_Affiliation__c newAccAff = new Account_Affiliation__c();
                newAccAff.Affiliate__c = affNameRecordMap.get(selectedAffiliateValue).Id;
                newAccAff.Active__c = true;
                if(aId != Null && aId != '')
                    newAccAff.Member__c = aId;
                if(newMemberNumber != Null)
                    newAccAff.MemberNumber__c = newMemberNumber;
                // Military Rank Added by TCS
                if(sMilRank != Null)
                    newAccAff.Military_Rank__c = sMilRank;
                
                insert newAccAff;
                accAffMap = new Map<Id,Account_Affiliation__c>([Select Id, Name, Affiliate__r.Name, Affiliate__r.Affiliate_Description__c, Active__c, Builder__c, MemberNumber__c,Military_Rank__c From Account_Affiliation__c Where Member__c =: aId]);
                
                // Reset condition to false so that the next Affiliate dosent get added automatically
                addAffiliation = false;
            }
            // Reset Member Number to blank for the next Affiliate           
            newMemberNumber = '';           
        }else{
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Select an affiliate from the dropdown');
            ApexPages.addMessage(errorMsg);
        }
        return null;
    }  
    
    /**
    * Called when adding an affiliate
    ***/
    public pagereference setAddAffiliation(){
        String sResponse = '';
        boolean continueFlow = true;
        String sMemNum = '';
        String agentInput = '';        
        
        // Check to see if the Member# Validation flag is set to Validate.
        if (isRequired && String.isBlank(newMemberNumber)){    
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Member Number is Required to add Affiliate');
            ApexPages.addMessage(errorMsg);
        }else{
            if (selectedAffiliateValue != null){
                // Validating if an active Account Affiliate already exists
                for(Account_Affiliation__c existingAccAff : accAffMap.values()){
                    system.debug('DEV: For loop Affiliate name: '+existingAccAff.Affiliate__r.Name+' & Selected Affiliate is: '+selectedAffiliateValue);
                    if(existingAccAff.Affiliate__r.Name == selectedAffiliateValue && existingAccAff.Active__c){
                        system.debug('DEV: Active affiliate already present!');
                        addAffiliation = false;
                        continueFlow = false;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Active membership already exists for the Affiliate'));
                        break;
                    }
                }
                
                if(continueFlow){
                    // Instantianting the affiliate validation class
                    AffiliateValidationApi api = new AffiliateValidationApi();
                    
                    Boolean callUsaaValidation = false;
                    
                    // Query the Affiliate Validation Records
                    List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>();                   
                    affValLst = getAffValRec();                    
                    agentInput = newMemberNumber + ',' + a.LastName__c;
                     
                    
                    if(affValLst.size() > 0 ){          
                        AFF_Validation__c affVal = affValLst[0];
                        if ( affVal.Failed_Attempts__c >= usaaFailCount && tMins < usaaTimeout && tMins != 0 && affVal.AffiliateID__r.Name == 'USAA'){
                        //if ( affVal.Failed_Attempts__c >= usaaFailCount && tMins < usaaTimeout || tMins != 0 ) && (affVal.AffiliateID__r.Name == 'USAA') ){
                            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Maximum number of Validation reached - You must wait for ' + usaaTimeout + ' Minutes before attempting to validate again.');
                            ApexPages.addMessage(errorMsg);                            
                        }               
                    }
    
                    // Validating USAA membership with member number and account last name
                    if (selectedAffiliateValue == 'USAA' && newMemberNumber != null && a != null){
                        if(isValidated)
                            sResponse = api.ValidateUSAAMembership(newMemberNumber, a.LastName__c);
                        else
                            sResponse = 'No Response';                    

                        if (sResponse != null){
                            System.debug('Response from the AffiliateValidationApi: '+sResponse);
                            if (sResponse.contains('Success')){
                                // Reset the fail counter because of folowing good call
                                resetAffValRec();
                                
                                // If response is success we add/update the record
                                //********* Modified by Divya rai - 4/20/2017 as 'Military Rank' added in the response
                                string tempMemNum;
                                tempMemNum = sResponse.substringAfter('Success:');
                                sMemNum = tempMemNum.substringBefore(':MilitaryRank:');
                                sMilRank = tempMemNum.substringAfter('MilitaryRank:');
                                //****************************************
                                addAffiliation = true;
                            }else{// If Membership NOT found mismatch DO NOT ADD/UPDATE record
                                if ( (sResponse.contains('404')) || (sResponse.contains('400')) || (sResponse.contains('412')) || (sResponse.contains('500')) ) { // No Match Found
                                    //Track the number of Failed Attempts
                                    addAFFValRec(agentInput);

                                    // To calculate the time remaining to do the usaa validation
                                    List<AFF_Validation__c> newAffValLst1 = new List<AFF_Validation__c>();
                                    newAffValLst1 = getAffValRec();
                                    Decimal timeRemaining = usaaTimeout;

                                    //Check validation
                                    /* Siju Commented to avoid negative Values
                                    if(newAffValLst1.size() > 0){ 
                                        timeRemaining = usaaTimeout - tMins;
                                    }*/
                                    //Siju Newly Added - July 2019
                                    if(newAffValLst1.size() > 0){ 
                                        if(tMins < usaaTimeout)
                                            timeRemaining = usaaTimeout - tMins;
                                        else
                                           timeRemaining = usaaTimeout; 
                                    }
                                    //ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'No Match Found. After ' + usaaFailCount + ' Failed Attempts, you must wait for '+ timeRemaining +' Minutes Before you Validate Again.');
                                    //Siju Added - HRM-9702
                                     String errMsg = 'No Match Found. After ';
                                     if(sResponse.contains('500')) errMsg = 'We are unable to process your request. After ';
                                     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.WARNING,errMsg + usaaFailCount + ' Failed Attempts, you must wait for '+ timeRemaining +' Minutes Before you Validate Again.');
                                     ApexPages.addMessage(errorMsg);
                                }
                                //Siju Added - HRM-9702    
                                else if (sResponse.contains('403') ){
                                    ApexPages.Message infoMsg = new ApexPages.Message(ApexPages.Severity.INFO,'User Locked.' );
                                    ApexPages.addMessage(infoMsg);
                                }
                                else { 
                                    // Any other error we bypass and add/update record
                                    sMemNum = newMemberNumber;
                                    addAffiliation = true;                            
                                }
                            }
                        }  
                    }
                    else{
                        addAffiliation = true;                      
                    }
                    return addAccountAffiliate();
                }
            }
        }
        return null;
    }/*end setAddAffiliation */


    /**
    *  Updates single record at a time with the correct Member Number
    **/
    public void updateRecord(){
        String currentAffiliate = '';
        if(updatedMemberNumber != '' && accAffRecId != ''){
            currentAffiliate = accAffMap.get(accAffRecId).Affiliate__r.Name;
            System.debug('Current affiliate is: '+currentAffiliate);
            if (currentAffiliate == 'USAA'){
                Boolean isSuccess = validateUsaaMemNo();
                if (isSuccess){
                    Account_Affiliation__c accAffToUpdate = new Account_Affiliation__c();
                    accAffToUpdate.Id = accAffRecId;
                    accAffToUpdate.MemberNumber__c = updatedMemberNumber;
                    update accAffToUpdate;
                }
                else{
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot update membership since it failed validation.');
                    ApexPages.addMessage(errorMsg); 
                }
            }           
        }
    }
    
    /**
     *  Call the AffiliateValidationApi to validate the USAA Membership with member Number
     *  Currently called when updating the record
     **/
   @TestVisible private Boolean validateUsaaMemNo(){
        // Instantianting the affiliate validation class
        AffiliateValidationApi api = new AffiliateValidationApi();
        Boolean sendFlag = true;
        String sAccountID = a.Id;
        String sResponse = '';
        if (updatedMemberNumber != Null){
            if (isValidated == true){
                sResponse = api.ValidateUSAAMembership(updatedMemberNumber, a.LastName__c);
            }else{
                sResponse = 'Success';
            }
        
            if (sResponse != null){
                if (sResponse == 'Success'){
                    sendFlag = true;
                }
                else{
                    // If Membership NOT found/ mismatch DO NOT ADD/UPDATE record else bypass
                    if ( (sResponse.substringBefore(':') == '400') || (sResponse.substringBefore(':') == '412') || (sResponse.substringBefore(':') == '404') )                    
                        sendFlag = false;
                    else
                        sendFlag = true;            
                }
            }
        }       
        return sendFlag;
    }

    /**
     *  Deactivate the Account Affiliate record
     **/
    public void deactivateRecord(){
        if(accAffRecId != ''){
            Account_Affiliation__c accAffToUpdate = new Account_Affiliation__c();
            accAffToUpdate.Id = accAffRecId;
            
            if (accAffMap.get(accAffRecId).Active__c == true){
                String currentAffiliate = '';
                currentAffiliate = accAffMap.get(accAffRecId).Affiliate__r.Name;
                System.debug('*** currentAffiliate' + currentAffiliate);
                for(Account_Affiliation__c existingAccAff : accAffMap.values()){
                    System.debug('*** existingAccAff = '+existingAccAff);
                    if (existingAccAff.Active__c == true && existingAccAff.Affiliate__r.Name == currentAffiliate && existingAccAff.id != accAffRecId){
                        ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot reactivate when there is already an active membership.');
                        ApexPages.addMessage(errorMsg);
                    }
                }
                accAffToUpdate.Active__c = accAffMap.get(accAffRecId).Active__c;
                update accAffToUpdate;
                currentAffiliate = '';
            }else{
                accAffToUpdate.Active__c = accAffMap.get(accAffRecId).Active__c;
                update accAffToUpdate;
            }
        }
    }
    
    /**
     *  Get the Url of the logo image uploaded in the chatter feed
     **/
    private string getLogoURL(String parentID){
        String url;
        List<feedItem> affiFeedItemList=[select ContentFileName,RelatedRecordId,contentType,parentId FROM FeedItem WHERE Type = 'ContentPost' AND ParentId=:parentID];
        if(affiFeedItemList.size()>0){
            for(feedItem fi:affiFeedItemList){
                if(fi.contentType != Null){
                    if(fi.contentType.contains('image')){
                        url = '/sfc/servlet.shepherd/version/download/'+fi.RelatedRecordId;
                        break;
                    }
                }
            }
        }
        return url;
    }

    /**
     *  Call the AffiliateValidationApi to validate the USAA Membership with Date Of Birth
     **/
    public pagereference validateUsaaDOB(){
        String sResponse = '';
        boolean continueFlow = true;
        String sDob = '';
        String sMemNum = '';
        String agentInput = '';

        newFirstname = a.FirstName__c;
        newLastname  = a.LastName__c;

        
        //Format DOB to comform to USAA DOB
        if (String.isNotBlank(newDob)){
            String sDD = newDob.substring(8);
            String sMM = newDob.substring(5,7);
            String sYYYY = newDob.substring(0,4);
            sDob = sMM + '/' + sDD + '/' + sYYYY;
        }


        
        // Check to see if the Member# Validation flag is set to Validate.        
        if (isValidated == false){
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Advance Search can not be used. The validation flag is not set.');
            ApexPages.addMessage(errorMsg);
        }
        else if ( (String.isBlank(newFirstname)) || (String.isBlank(newLastname)) ) { //|| (String.isBlank(newCity)) || (String.isBlank(newState)) || (String.isBlank(newZip)) || (String.isBlank(newEmail)) ){
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: First Name or Last Name are missing from the Account. Please review Account.');
            ApexPages.addMessage(errorMsg);
        }else if (String.isBlank(sDob)){
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Make sure you enter a Valid Birthdate.');
            ApexPages.addMessage(errorMsg);
        }
        else{
            if (selectedAffiliateValue != null){
                // Validating if an active Account Affiliate already exists
                system.debug('DEV: accAffMap: '+accAffMap);
                for(Account_Affiliation__c existingAccAff : accAffMap.values()){
                	system.debug('DEV: existingAccAff: '+existingAccAff);
                    system.debug('DEV: For loop Affiliate name: '+existingAccAff.Affiliate__r.Name+' & Selected Affiliate is: '+selectedAffiliateValue);
                    if(existingAccAff.Affiliate__r.Name == selectedAffiliateValue && existingAccAff.Active__c){
                        addAffiliation = false;
                        continueFlow = false;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Active membership already exists for the Affiliate.'));
                        break;
                    }
                }

                if(continueFlow){
                    // Instantianting the affiliate validation class
                    AffiliateValidationApi api = new AffiliateValidationApi();                   
                    Boolean callUsaaValidation = false;
                    
                    // Query the Affiliate Validation Records
                    List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>();                   
                    affValLst = getAffValRec();                    
                    
                    agentInput = newFirstname + ',' + newLastname + ',' + newEmail + ',' + newPhonenumber + ',' + newCity + ',' + newState+ ',' + newZip; 
                   
                    if(affValLst.size() > 0 ){          
                        AFF_Validation__c affVal = affValLst[0];
                        
                        if (affVal.Failed_Attempts__c >= usaaFailCount && tMins < usaaTimeout && tMins != 0 && affVal.AffiliateID__r.Name == 'USAA'){
                            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Maximum number of Validation reached - You must wait for ' + usaaTimeout + ' Minutes before attempting to validate again.');
                            ApexPages.addMessage(errorMsg);
                        }               
                    }

                    // Validating USAA membership with member number and account last name
                    if (selectedAffiliateValue == 'USAA'){  //&& newMemberNumber != null && a != null){                       
                        
                        
                        sResponse = api.ValidateUSAAMembershipAdvance(newFirstname,newLastname,sDob,newPhonenumber,newCity,newState,newZip,newEmail);

                        if (sResponse != null){
                            System.debug('Response from the AffiliateValidationApi: '+sResponse);
                            if (sResponse.contains('Success')){
                                // Reset the fail counter because of folowing good call
                                resetAffValRec();
                                
                                // If response is success we add/update the record
                                //********* Modified by Divya rai - 4/20/2017 as 'Military Rank' added in the response
                                string tempMemNum;
                                tempMemNum = sResponse.substringAfter('Success:');
                                sMemNum = tempMemNum.substringBefore(':MilitaryRank:');
                                sMilRank = tempMemNum.substringAfter('MilitaryRank:');
                                //****************************************
                                addAffiliation = true;
                                return addAccountAffiliateExt(sMemNum);
                            }else{// If Membership NOT found mismatch DO NOT ADD/UPDATE record
                                if ( (sResponse.contains('404')) || (sResponse.contains('400')) || (sResponse.contains('500'))) {//|| (sResponse.contains('412')) ) { // No Match Found
                                    //Track the number of Failed Attempts
                                    addAFFValRec(agentInput);

                                    // To calculate the time remaining to do the usaa validation
                                    List<AFF_Validation__c> newAffValLst2 = new List<AFF_Validation__c>();
                                    newAffValLst2 = getAffValRec();
                                    Decimal timeRemaining = usaaTimeout;

                                    //Check validation
                                    /* Siju Commented 
                                    if(newAffValLst2.size() > 0){ 
                                        timeRemaining = usaaTimeout - tMins;
                                    }
                                    */
                                    
                                    if(newAffValLst2.size() > 0){ 
                                        if(tMins >0)
                                            timeRemaining = usaaTimeout - tMins;
                                        else
                                           timeRemaining = usaaTimeout; 
                                    }
                                    //ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'No Match Found. After ' + usaaFailCount + ' Failed Attempts, you must wait for '+ timeRemaining +' Minutes Before you Validate Again.');
                                    //Siju Added - HRM-9702
                                     String errMsg = 'No Match Found. After ';
                                     if(sResponse.contains('500')) errMsg = 'We are unable to process your request. After ';
                                     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.WARNING,errMsg + usaaFailCount + ' Failed Attempts, you must wait for '+ timeRemaining +' Minutes Before you Validate Again.');
                                     
                                    ApexPages.addMessage(errorMsg);
                                    
                                }else if (sResponse.contains('412') ){
                                    ApexPages.Message infoMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please, Enter Additional Fields, to Validate.' );
                                    ApexPages.addMessage(infoMsg);
                                //Siju Added - HRM-9702    
                                }else if (sResponse.contains('403') ){
                                    ApexPages.Message infoMsg = new ApexPages.Message(ApexPages.Severity.INFO,'User Locked.' );
                                    ApexPages.addMessage(infoMsg);      
                                     
                                }else {
                                    // Any other error we do not know member number, therefore we display error
                                    ApexPages.Message infoMsg = new ApexPages.Message(ApexPages.Severity.INFO,'USAA is currently unavailable, Please ADD Affiliate using a Member Number or try again later.' );
                                    ApexPages.addMessage(infoMsg);                           
                                    
                                }
                            }
                        } 
                    }
                    else{
                        addAffiliation = true;                      
                    }
                }
            }
        }
        return null;
    }/*end validateUsaaDOB*/

    /*****************************
    **
    ** Add AFF_Validation record
    **
    *****************************/
   @TestVisible private void addAFFValRec( String agentInputStr){               
        // Initailize Affiliate Validation Record
        AFF_Validation__c affVal = new AFF_Validation__c();
        
        List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>();
        affValLst = getAffValRec();
            
        //Check validation
        if(affValLst.size() > 0){           
            affVal = affValLst[0];            
            Datetime startDate = affVal.CreatedDate; 
            Datetime endDate = system.now();
   
            integer intDays =  startDate.Date().daysBetween(endDate.Date());
            datetime sameDayEndDate = startDate.addDays(intDays);
            tMins = ((endDate.getTime())/1000/60) - ((sameDayEndDate.getTime())/1000/60);
        
            if ( (tMins < usaaTimeout) && (affVal.Failed_Attempts__c <= usaaFailCount) ){                
                affVal.Failed_Attempts__c = affVal.Failed_Attempts__c +1;
                affVal.Agent_Input__c = affVal.Agent_Input__c + ' Attempt '+ affVal.Failed_Attempts__c + ' with input :'+agentInputStr;
                update affVal;
            }else if (tMins > usaaTimeout){
                createAffValRec(agentInputStr);
            }
        }
        else{
            createAffValRec(agentInputStr);
        }
    }
    
    private List<AFF_Validation__c> getAffValRec(){    
        List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>([Select Id, Failed_Attempts__c, Agent_Input__c, AffiliateID__c,AffiliateID__r.Name, CreatedDate from AFF_Validation__c where UserID__c =: UserInfo.getUserId() AND AccountID__c =: aId Order By CreatedDate Desc]);  
        return affValLst;
    }

  @TestVisible  private void createAffValRec(String inputStr){
        AFF_Validation__c newAffVal = new AFF_Validation__c();
        newAffVal.UserID__c = UserInfo.getUserId();
        newAffVal.AccountID__c = aId;
        newAffVal.AffiliateID__c = affNameRecordMap.get(selectedAffiliateValue).Id;
        newAffVal.Failed_Attempts__c = 1;
        newAffVal.Agent_Input__c = 'Attempt 1 with input :'+inputStr;
        insert newAffVal;
    }
    
  @TestVisible  private void resetAffValRec(){        
        // Initailize Affiliate Validation Record
        AFF_Validation__c affVal = new AFF_Validation__c();
        
        List<AFF_Validation__c> affValLst = new List<AFF_Validation__c>();
        affValLst = getAffValRec();
            
        //Check validation
        if(affValLst.size() > 0){           
            affVal = affValLst[0];            
            Datetime startDate = affVal.CreatedDate; 
            Datetime endDate = system.now();
   
            integer intDays =  startDate.Date().daysBetween(endDate.Date());
            datetime sameDayEndDate = startDate.addDays(intDays);
            tMins = ((endDate.getTime())/1000/60) - ((sameDayEndDate.getTime())/1000/60);
        
            if ( (tMins < usaaTimeout) && (affVal.Failed_Attempts__c <= usaaFailCount) ){
                // If a successful attempt is made the counter should be reset
                affVal.Failed_Attempts__c = 0;
                affVal.Agent_Input__c = affVal.Agent_Input__c + ' Successful attempt made.';
                update affVal;
            }
        }
    }
}