/************************************* MODIFICATION LOG ********************************************************************************************
* FixCustomerIdBatch
*
* DESCRIPTION : Batch class used to correct a Phase 1 issue.
*               Retained for potential future reuse but not planned for deployment with Phase 2. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli          	  10/9/2011			- Original Version
*
*													
*/

global class FixCustomerIdBatch  implements Database.batchable<sObject>{
	
	global final String query;
	
	global FixCustomerIdBatch() {
		this.query = 'Select id, CustomerId__c, leadExternalId__c from account where CustomerId__c = null';
	}
	
	//get Querylocator with the specitied query
	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext bc, List<sObject> accounts) {
		List<Account> updatedAccounts = new List<Account>();
		for(sObject s : accounts)
		{
			Account a = (Account)s;
			try{
				a.CustomerID__c = a.LeadExternalID__c.split('-')[1];
				updatedAccounts.add(a);
			}
			catch (Exception ex){
				//do nothing
			}
		}
		update updatedAccounts;
	}
	
	global void finish(Database.BatchableContext bc) {

	}
}