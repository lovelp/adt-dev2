public without sharing class NSCHelper {

    public static String accPrefix;
    public static String leadPrefix;
    public enum STATIC_CUSTOMER_INTEREST {RELO, DISCO, MULTI}
    
    static {
        accPrefix = Schema.getGlobalDescribe().get('Account').getDescribe().getKeyPrefix();
        leadPrefix = Schema.getGlobalDescribe().get('Lead').getDescribe().getKeyPrefix();
    }
    
    public static Boolean isLeadID(String lId){
        return !Utilities.isEmptyOrNull(lId) && lId.startsWith(leadPrefix); 
    }
    
    public static Boolean isAccountID(String aId){
        return !Utilities.isEmptyOrNull(aId) && aId.startsWith(accPrefix); 
    }
    
    public static Boolean isDLL(scpq__SciQuote__c quote, Boolean DLLFlag){
       Boolean resVal = false;
       if( quote != null ){
           if( !String.isBlank(quote.scpq__Status__c) && (quote.scpq__Status__c.equalsIgnoreCase('Submitted') || quote.scpq__Status__c.equalsIgnoreCase('Signed')) ){
               resVal = true;
           }
       }
       return resVal && DLLFlag;
    }
    
    public Static Account readAccountInfo( String accId){
        return [SELECT Name,PostalCodeID__r.Town_Lookup__r.name,TMTownID__c,TMSubTownID__c,Business_ID__c,MMBOrderType__c, Profile_RentOwn__c, Profile_BuildingType__c, Profile_YearsInResidence__c, FirstName__c, LastName__c, SiteCity__c, SiteStreet__c, SiteStreet2__c, SiteCounty__c , SiteState__c, SitePostalCodeAddOn__c, SitePostalCode__c, email__c, Phone, Channel__c, ShippingStreet, ShippingStreet2__c, ShippingPostalCode, ShippingCity, IncomingCounty__c, ShippingState, MMBDisconnectService__c, MMBDisconnectDate__c, MMB_Relo_Address__c, MMB_Relo_Site_Number__c FROM Account WHERE Id = :accId];
    }
    
    public static void processProfileQuestionBeforeUpdate(Map<Id, ProfileQuestion__c> prevQMap, Map<Id, ProfileQuestion__c> currQMap){
        Set<Id> questionTypeIds = new Set<Id>(); 
        for(ProfileQuestion__c q: [SELECT Name, (Select Answer__c From Profile_Question_Answers__r limit 1)  FROM ProfileQuestion__c WHERE Id IN :currQMap.keySet()]){
            if( q.Profile_Question_Answers__r != null && !q.Profile_Question_Answers__r.isEmpty()){
                questionTypeIds.add(q.Id);
            }
        }
        for(ProfileQuestion__c q: currQMap.values()){
            ProfileQuestion__c oldQuestion = prevQMap.get(q.Id);
            if( oldQuestion.Type__c != q.Type__c && questionTypeIds.contains(q.Id)){
                oldQuestion.addError('Cannot change type to a question with answers. Please deactive this question and create a new one with the desired type.');
            }
        }
    }
    
    public static String FormatPhone(String Phone) {
        string nondigits = '[^0-9]';
        string PhoneDigits;      
        // remove all non numeric
        PhoneDigits = Phone.replaceAll(nondigits,'');      
        if(phoneDigits.length()<=3)
          return '('+phoneDigits;
        else if(phoneDigits.length()>3 && phoneDigits.length()<=6)
          return '('+phoneDigits.subString(0,3)+') '+
                 phoneDigits.subString(3,phoneDigits.length());
        else if(phoneDigits.length()>6 && phoneDigits.length()<=10)
          return '('+phoneDigits.subString(0,3)+') '+
                 phoneDigits.subString(3,6)+'-'+
                 phoneDigits.subString(6,phoneDigits.length());      
        return( Phone );
    }
    
    public static Lead_Convert__c getLeadConvertInfo( Account a){
        Lead_Convert__c lConv;
        // All three values are empty, use default values
        if( Utilities.isEmptyOrNull(a.TelemarLeadSource__c) && Utilities.isEmptyOrNull(a.QueriedSource__c) && Utilities.isEmptyOrNull(a.GenericMedia__c)){
            if (a.Business_Id__c.startsWith('1100')) {
                a.TelemarLeadSource__c = 'SGL';
                a.QueriedSource__c = 'SGL';
                a.GenericMedia__c = 'SGL';
            }
            if (a.Business_Id__c.startsWith('1200')) {
                a.TelemarLeadSource__c = 'BGL';
                a.QueriedSource__c = 'BGL';
                a.GenericMedia__c = 'SGL';
            }
        }
        if( !Utilities.isEmptyOrNull(a.TelemarLeadSource__c) || !Utilities.isEmptyOrNull(a.QueriedSource__c) || !Utilities.isEmptyOrNull(a.GenericMedia__c)){
            // 1- Check for a matching QueriedSource__c
            if(!Utilities.isEmptyOrNull(a.QueriedSource__c)){
                try{
                    lConv = [SELECT SourceNo__c, PartnerNo__c, Name, Id, Description__c, AffiliateID__c FROM Lead_Convert__c Where Name = :a.QueriedSource__c limit 1];
                }
                catch(Exception err){ }
            }
            // 2- Check for a matching TelemarLeadSource__c
            if( lConv == null && !Utilities.isEmptyOrNull(a.TelemarLeadSource__c)){
                try{
                    lConv = [SELECT SourceNo__c, PartnerNo__c, Name, Id, Description__c, AffiliateID__c FROM Lead_Convert__c Where Name = :a.TelemarLeadSource__c limit 1];
                }
                catch(Exception err){ }
            }
            // 3- Check for a matching GenericMedia__c ONLY if no Queried Source nor Telemar Source found
            if( lConv == null && !Utilities.isEmptyOrNull(a.GenericMedia__c)){
                try{
                    lConv = [SELECT SourceNo__c, PartnerNo__c, Name, Id, Description__c, AffiliateID__c FROM Lead_Convert__c Where Name = :a.GenericMedia__c limit 1];
                }
                catch(Exception err){ }
            }
        }
        // 4- Default - Everything else failed
        if( lConv == null){
            lConv = [Select SourceNo__c, PartnerNo__c, Name, Id, Description__c, AffiliateID__c From Lead_Convert__c Where Description__c = 'Default'];
        }
        return lConv;
    }
    
    @future
    public static void WrapUpCall( String SelectedTier1Disposition, String SelectedTier2Disposition, String DispositionComments, String dspQuery, Id cDataId, Id uId ){
        try{
            // log future request parameters to track any errors in case they arise
            ADTApplicationMonitor.log('Tier One: '+SelectedTier1Disposition);
            ADTApplicationMonitor.log('Tier Two: '+SelectedTier2Disposition);
            ADTApplicationMonitor.log('Comments: '+DispositionComments);
            ADTApplicationMonitor.log('Query: '+dspQuery);
            ADTApplicationMonitor.log('Call Data Id: '+cDataId);
            ADTApplicationMonitor.log('User Id: '+uId);
            
            Call_Disposition__c TierOneDispoObj;
            Call_Disposition__c TierTwoDispoObj;       
            Call_Data__c cData = [SELECT Call_Disposition__c, Account__c FROM Call_Data__c WHERE ID = :cDataId];     
            User u = [SELECT EmployeeNumber__c, Name FROM User WHERE ID = :uId];       
            Disposition__c newDispo = new Disposition__c();
            if( !String.isBlank(SelectedTier2Disposition) ){
                newDispo.DispositionDetail__c = SelectedTier2Disposition;
            } 
            for(sObject sobj: Database.query(dspQuery)){
                Call_Disposition__c tmpObj = ((Call_Disposition__c)sobj);
                if( tmpObj.Tier_One__c != null ){
                    TierTwoDispoObj = tmpObj;
                    newDispo.DispositionType__c = tmpObj.Telemar_Dispo_Code__c;
                }
                else{
                    if( utilities.isEmptyOrNull(newDispo.DispositionType__c) ){
                        newDispo.DispositionType__c = tmpObj.Telemar_Dispo_Code__c;
                    }
                    TierOneDispoObj = tmpObj;
                }
            }
            
            // Disposition settings
            newDispo.DispositionDate__c = DateTime.now();
            newDispo.Comments__c = DispositionComments;
            newDispo.DispositionedBy__c = UserInfo.getUserId();
            newDispo.DispositionEmployeeNumber__c = u.EmployeeNumber__c;
            newDispo.DispositionEmployeeName__c = u.Name;
            
            //refresh account
            Account acc;
            if( !String.isBlank( cData.Account__c )   ){
                acc = [SELECT Name,Rep_User__c,Rep_HRID__c,Rep_Phone__c, TelemarAccountNumber__c, OwnerId, Owner.Name, Profile_RentOwn__c, Profile_BuildingType__c, Profile_YearsInResidence__c, FirstName__c, LastName__c, AddressID__c, SiteCity__c, SiteStreet__c, SiteStreet2__c, SiteCounty__c , SiteState__c, SitePostalCodeAddOn__c, SitePostalCode__c, Latitude__c, Longitude__c, 
                                      email__c, Phone, PhoneNumber3__c, PhoneNumber4__c, Channel__c, ShippingStreet, ShippingStreet2__c, ShippingPostalCode, ShippingCity, IncomingCounty__c, ShippingState, MMB_TownID__c, MMBLookup__c, MMBDisconnectService__c, MMBDisconnectDate__c, MMB_Relo_Address__c, MMB_Relo_Site_Number__c, MMB_Relo_Customer__c, MMBContractNumber__c, MMBCustomerNumber__c, 
                                      MMB_Past_Due_Balance__c, MMB_Disco_Site__c, MMB_Disco_Count__c, TelemarLeadSource__c, LeadSourceNo__c, QueriedSource__c, GenericMedia__c, Business_Id__c, DispositionEmployeeNumber__c, DispositionEmployeeName__c, MMBOrderType__c, MMBSiteNumber__c,Referred_Name__c,Referred_Phone__c, Referred_Address__c,is_Referred__c 
                        FROM Account 
                        WHERE Id = :cData.Account__c];
                newDispo.AccountID__c = acc.Id;
                // Send disposition to telemar
                TelemarGateway.addDisposition( new Map<Account, Disposition__c>{acc=>newDispo} );
            }
            if( TierTwoDispoObj != null ){
                newDispo.DispositionDetail__c = TierTwoDispoObj.Name;
            }

            newDispo.DispositionType__c = TierOneDispoObj.Name;
            insert newDispo;
            if( TierTwoDispoObj != null ){
                cData.Call_Disposition__c = TierTwoDispoObj.Id;             
            }
            else{
                cData.Call_Disposition__c = TierOneDispoObj.Id;
            }
            update cData;
        }
        catch(Exception err){
            ADTApplicationMonitor.log(err, 'NSCHelper', 'WrapUpCall', ADTApplicationMonitor.CUSTOM_APP.MATRIX);
        }       
    }
    
    
}