/************************************* MODIFICATION LOG ********************************************************************************************
* TempManagerTownFixBatch
*
* DESCRIPTION : Batch class used to correct a Phase 1 issue.
*               Retained for potential future reuse but not planned for deployment with Phase 2. 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli             10/9/2011         - Original Version
*
*                                                   
*/

global class TempManagerTownFixBatch  implements Database.batchable<sObject>{
    
    global final String query;
    
    global TempManagerTownFixBatch() {
        this.query = 'select id, PostalCodeId__c, OwnerId, DateAssigned__c, UnassignedLead__c from account where owner.userrole.name like \'%Resi%\' and owner.userrole.Name like \'%Mgr%\' and business_id__c = \'1200 - Small Business\'';
    }
    
    //get Querylocator with the specitied query
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> accounts) {
        List<Account> updatedAccounts = new List<Account>();
        for(sObject s : accounts)
        {
            Account a = (Account)s;
            updatedAccounts.add(a);
        }
        ChangeAccountOwnerController.changeOwnershipWithListOfAccounts(updatedAccounts);
        update updatedAccounts;
    }
    
    global void finish(Database.BatchableContext bc) {

    }
}