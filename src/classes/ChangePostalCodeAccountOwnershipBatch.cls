/************************************************ MODIFICATION LOG *******************************************************************
*
* DESCRIPTION : ChangePostalCodeAccountOwnershipBatch.cls is a batch class that changes account and lead ownership based on territory 
*               assignments.
*
*-------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                 DATE                TICKET        REASON
*-------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli           11/11/2011                        - Origininal Version
* Sunil Addepalli           02/20/2012                        - Modified to process both accounts and leads
*                                                             - Added functionality to prevent multiple batches running simultaneously
* Siddarth Asokan           08/26/2019          HRM-10846     - Commercial Flow account assignment
*/

global class ChangePostalCodeAccountOwnershipBatch implements Database.batchable<sObject>, Database.AllowsCallouts{
    //incoming query
    global final String query;
    global final String NewOwnerId;
    global final Boolean IsAccount;
    global final List<PostalCodeReassignmentQueue__c> deletePCQ;
    global final map<string, string> mgrs;
    global final String Qual;
    global final String globalUser;
    
    //Constructor - set the local variable with the query string
    global ChangePostalCodeAccountOwnershipBatch(String query, String newOwnerId, Boolean isAccount, List<PostalCodeReassignmentQueue__c> deletePCQ, String qualification, map<string, string> managers){
        this.query = query;
        this.NewOwnerId = newOwnerId;
        this.IsAccount = isAccount; 
        this.deletePCQ = deletePCQ;
        this.Qual = qualification;
        this.mgrs = managers;
        this.globalUser = Utilities.getGlobalUnassignedUser();
    }
    
    //get Querylocator with the specitied query 
    global Database.QueryLocator start(Database.BatchableContext bc){
        try{
            delete deletePCQ;
        }catch(exception ex) {}
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> sobjs){
        String businessId = '';
        if(IsAccount){
            Map<Id,Account> updatedAccountsMap = new Map<Id,Account>();
            Map<Id,Id> updatedAccountOwnerMap = new Map<Id,Id>();
            for(sObject sacct : sobjs){
                Account acct = (account)sacct;
                businessId = String.isNotBlank(acct.Business_Id__c)? Channels.getFormatedBusinessId(acct.Business_Id__c, Channels.BIZID_OUTPUT.NUM): '1100';
                if(acct.Affiliation__c != IntegrationConstants.AFFILIATION_USAA || (this.Qual == IntegrationConstants.AFFILIATION_USAA && acct.Affiliation__c == IntegrationConstants.AFFILIATION_USAA)){
                    acct.OwnerId = this.newOwnerId;
                }else if(this.mgrs.get(acct.postalCodeId__c + ';' + businessId + ';' + acct.Channel__c) != null){
                    acct.OwnerId = this.mgrs.get(acct.postalCodeId__c + ';' + businessId + ';' + acct.Channel__c);
                }else{
                    acct.OwnerId = this.globalUser;
                }
                acct.DateAssigned__c = DateTime.now();
                acct.UnassignedLead__c = false;
                acct.ManuallyAssigned__c = false;
                acct.PreviousOwner__c = null;
                acct.AssignedBy__c = null;
                acct.NewLead__c = true;
                updatedAccountsMap.put(acct.Id,acct);
                updatedAccountOwnerMap.put(acct.Id,acct.OwnerId);
            }
            
            Database.Saveresult[] srs = database.update(updatedAccountsMap.values(), false);
            List<Account> reUpdates = new List<Account>();
            List<Id> successAccountId = new List<Id>();
            for(Database.SaveResult sr:srs){
                if(sr.isSuccess()){
                    successAccountId.add(sr.getId());
                }else{
                    reUpdates.add(updatedAccountsMap.get(sr.getId()));
                }
            }
            if(reUpdates.size() > 0){
                Database.Saveresult[] nSRs = database.update(reUpdates, false);
                for(Database.SaveResult sr:nSRs){
                    if(sr.isSuccess()){
                        successAccountId.add(sr.getId());
                    }
                }
            }
            if(successAccountId != null && successAccountId.size() > 0){
                // HRM-10846 - Update child contact records
                list<contact> contactsToUpdate = new list<contact>();
                for(Contact c : [Select Id,OwnerId,AccountId from contact where AccountId IN: successAccountId]){
                    c.OwnerId = updatedAccountOwnerMap.get(c.AccountId);
                    contactsToUpdate.add(c);
                }
                if(contactsToUpdate.size() > 0){
                    database.update(contactsToUpdate,false);
                }
                // HRM-10846 - Update child opportunity records
                list<Opportunity> oppToUpdate = new list<Opportunity>();
                for(Opportunity o : [Select Id,OwnerId,AccountId from Opportunity where AccountId IN: successAccountId]){
                    o.OwnerId = updatedAccountOwnerMap.get(o.AccountId);
                    oppToUpdate.add(o);
                }
                if(oppToUpdate.size() > 0){
                    database.update(oppToUpdate,false);
                }
            }
        }else{
            Map<Id,Lead> updatedLeadsMap = new Map<Id,Lead>();
            for(sObject slead : sobjs){
                Lead led = (Lead)slead;
                businessId = String.isNotBlank(led.Business_Id__c)? Channels.getFormatedBusinessId(led.Business_Id__c, Channels.BIZID_OUTPUT.NUM): '1100';
                if((led.Affiliation__c != 'USAA') || (this.Qual == 'USAA' && led.Affiliation__c == 'USAA')){
                    led.OwnerId = this.newOwnerId;
                }else if(this.mgrs.get(led.TownNumber__c + ';' + businessId + ';' + led.Channel__c) != null){
                    led.OwnerId = this.mgrs.get(led.TownNumber__c + ';' + businessId + ';' + led.Channel__c);
                }else{
                    led.OwnerId = this.globalUser;
                }
                led.DateAssigned__c = DateTime.now();
                led.UnassignedLead__c = false;
                led.PreviousOwner__c  = null;
                led.AssignedBy__c = null;
                led.NewLead__c = true;
                updatedLeadsMap.put(led.Id,led);
            }
            
            Database.Saveresult[] srs = database.update(updatedLeadsMap.values(), false);
            List<Lead> reUpdates = new List<Lead>();
            for(Database.SaveResult sr:srs){
                if(!sr.isSuccess()){
                    reUpdates.add(updatedLeadsMap.get(sr.getId())); 
                }
            }
            if(reUpdates.size() > 0){
                Database.Saveresult[] nSRs = database.update(reUpdates, false);         
            }           
        }
    }
    
    global void finish(Database.BatchableContext bc) {}
}