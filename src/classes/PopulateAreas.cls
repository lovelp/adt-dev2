/************************************* MODIFICATION LOG ********************************************************************************************
* PopulateAreas
*
* DESCRIPTION : Batch class used on an ad-hoc basis by an administrator to re-sync area names in the Postal Codes object.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*           				 10/14/2011			- Original Version
*
*													
*/

global class PopulateAreas implements Database.Batchable<SObject>, Database.Stateful 
{
	// Private state used by internal methods only; not persisted across batch executions
	private transient List<Postal_Codes__c> batch;
    private boolean batchSet = false;
	
	//-----------------------------------------------------------------------------------------------------------------------
	//Constructor
	//-----------------------------------------------------------------------------------------------------------------------
	global PopulateAreas()
	{
	}
	
	//-----------------------------------------------------------------------------------------------------------------------
	//start
	//-----------------------------------------------------------------------------------------------------------------------
	global Database.QueryLocator start(Database.BatchableContext ctx) 
	{
		String soql = 'SELECT Id, Area__c, AreaId__c, All_Areas__c, BusinessId__c FROM Postal_Codes__c';
			
	    return Database.getQueryLocator(soql); 
    }
    
    
	//-----------------------------------------------------------------------------------------------------------------------
	//execute
	//-----------------------------------------------------------------------------------------------------------------------
	global void execute(Database.BatchableContext ctx, List<SObject> scope) 
	{
		batch = (List<Postal_Codes__c>)scope;
		String allAreas;
		
		Postal_Codes__c pc = [select id, All_Areas__c from Postal_Codes__c where Name = '99999'];
		
		allAreas = pc.All_Areas__c;
		
		if(allAreas == null)
			allAreas = '';
	
		for(Postal_Codes__c postalCode : batch)
		{
			String area = postalCode.Area__c;
			String areaId = postalCode.AreaID__c;
			String businessId = postalCode.BusinessId__c;
			 
			if(area == null)
				area = '';
			if(areaId == null)
				areaId = '';
			if(businessId == null)
				businessId = '';
				
			List<String> allAreasM = new List<String>();
			Map<String, String> allAreasS = new Map<String, String>();
			allAreasM = allAreas.split('~');
			for(String aa : allAreasM)
			{
				allAreasS.put(aa, aa);
			}
			
			if( area != '' && areaId != '' && businessId != '')
			{
				if(allAreas == '' || allAreasS.get(areaId + ' - ' + area + ' (' + businessId + ')') == null)
				{
					allAreas += '~' + areaId + ' - ' + area + ' (' + businessId + ')';
					
				}
			}
			
//			allAreas = allAreas.replace('null', '');
//			postalCode.All_Areas__c = allAreas;

			pc.All_Areas__c = allAreas;
		}
		
		update pc;
		
//		update batch;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------
	//finish off the batch
	//-----------------------------------------------------------------------------------------------------------------------
	global void finish(Database.BatchableContext ctx) 
	{
	}		

}