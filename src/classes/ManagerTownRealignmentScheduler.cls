/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : ManagerTownRealignmentScheduler.cls is a schecdulable class that invokes ManagerTownLeadRealignmentBatch 
*               and reschedules itself x minutes after the current run based on a custom setting determining x (interval)
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli               11/15/2011          - Origininal Version
*
* Sunil Addepalli               02/15/2012          - Modified to add ability to process leads
*                                                     Added functionality to skip starting the batch if another batch is running.
*                                                    
*/

global class ManagerTownRealignmentScheduler implements Schedulable{

    global void execute(SchedulableContext SC) {
        //String mgrAcctBatch = BatchState__c.getInstance('IsAcctManagerTownBatchRunning').value__c;
        //String mgrLeadBatch = BatchState__c.getInstance('IsLeadManagerTownBatchRunning').value__c;
        //String pcAcctBatch = BatchState__c.getInstance('IsAcctTerritoryRealignmentBatchRunning').value__c;
        //String pcLeadBatch = BatchState__c.getInstance('IsLeadTerritoryRealignmentBatchRunning').value__c;
        String MTInterval = BatchState__c.getInstance('ManagerTownScheduledBatchInterval').value__c;

        //run this batch only if no other manager town batch or postal code realignment batch is running
        if(BatchJobHelper.canThisBatchRun(BatchJobHelper.ManagerTownLeadRealignmentBatch))
        {
            //select top record from manager town orderd by created date
            List<ManagerTownRealignmentQueue__c> MQr = new List<ManagerTownRealignmentQueue__c>();
            MQr = [select id, BusinessId__c, Channel__c, TownId__c, NewOwnerId__c, OldOwnerId__c from ManagerTownRealignmentQueue__c order by createddate Limit 1];
            //check if there are any records to process
            if(MQr.size() > 0)
            {
                List<ManagerTownRealignmentQueue__c> MQ = [select id, BusinessId__c, Channel__c, TownId__c, NewOwnerId__c, OldOwnerId__c from ManagerTownRealignmentQueue__c where TownId__c =: MQr[0].TownId__c and BusinessId__c =: MQr[0].BusinessId__c and OldOwnerId__c =: MQr[0].OldOwnerId__c order by createddate];
                string chnls = '';
                Integer length = MQ.size();
                Integer counter = 0;
                for(ManagerTownRealignmentQueue__c M : MQ)
                {
                    counter++;
                    if(counter != length){
                        chnls += '\'' + Channels.getMappedChannel(M.Channel__c, true) + '\', ';
                    } else {
                        chnls += '\'' + Channels.getMappedChannel(M.Channel__c, true) + '\'';
                    }
                     
                }               
                //build a query for all accounts to be processed.
                String acctQry = 'Select id, OwnerId, DateAssigned__c, UnassignedLead__c from Account where ResaleTownNumber__c = ';
                acctQry += '\'' + MQ[0].TownId__c + '\' ';
                acctQry += 'and Business_Id__c like \'%' + MQ[0].BusinessId__c + '%\' ';
                acctQry += 'and Channel__c in (' + chnls + ')';
                acctQry += 'and OwnerId = \'' + MQ[0].OldOwnerId__c + '\' ';
                acctQry += 'and (ProcessingType__c = \'NSC\' ';
                acctQry += 'or RecordType.DeveloperName = \'ADTNAResale\')';
                //start a batch with this query and the new owner for each of the records
                id acctBatchId = database.executeBatch(new ManagerTownLeadRealignmentBatch(acctQry, MQ[0].NewOwnerId__c, true, MQ), 1000);
                
                //build a query for all leads to be processed.
                String leadQry = 'Select  id, OwnerId, DateAssigned__c, UnassignedLead__c from Lead where TownNumber__c = ';
                leadQry += '\'' + MQ[0].TownId__c + '\' ';
                leadQry += 'and Business_Id__c like \'%' + MQ[0].BusinessId__c + '%\' ';
                leadQry += 'and Channel__c in (' + chnls + ')';
                leadQry += 'and OwnerId = \'' + MQ[0].OldOwnerId__c + '\' ';
                leadQry += 'and LeadSource in (\'BUDCO\', \'Manual Import\', \'Telemar Rehash\') ';
                //start a batch with this query and the new owner for each of the records
                id leadBatchId = database.executeBatch(new ManagerTownLeadRealignmentBatch(leadQry, MQ[0].NewOwnerId__c, false, MQ), 1000); 
                
                //delete MQ;            
            }
        }
        
        //reschedule every x minutes 
        ManagerTownRealignmentScheduler m = new ManagerTownRealignmentScheduler();
        DateTime timenow = system.now().addMinutes(Integer.valueOf(MTInterval));
        String seconds = String.valueOf(timenow.second()); //Execute at Zero Seconds
        String minutes = String.valueOf(timenow.minute()); //Execute at every 10th minute of hour
        String hours = String.valueOf(timenow.hour()); // Execute Every Hour
        //String dayOfMonth = String.valueOf(timenow.day()); // Execute Every Day of the Month
        
        //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = seconds + ' ' + minutes + ' * * * ?';
        String jobName = 'Manager Town Realignment - ' + timenow;
        system.schedule(jobName, sch, m);       
        system.abortJob(sc.getTriggerId()); 
    }

}