/************************************* MODIFICATION LOG ********************************************************************************************
* ADTSalesMobilityFacade
*
* DESCRIPTION : Defines web services to enable SFDC to Telemar integration
*               Originally generated by wsdl2apex but modified to obtain endpoint and credentials from custom settings
*
*               Search for MANUAL MODIFICATION to locate these changes.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner				 3/23/2012			- Original Version
* 
*													
*/

public class ADTSalesMobilityFacade {
    public class PreSale_element {
        public String FirstName;
        public String LastName;
        public String BusinessName;
        public String AddressLine1;
        public String AddressLine2;
        public String AddressLine3;
        public String AddressLine4;
        public String City;
        public String State;
        public String PostalCode;
        public String CountryCode;
        public String PhoneNumber1;
        public String PhoneNumber2;
        public String PhoneNumber3;
        public String PhoneNumber4;
        public String Email;
        public String Comments;
        public String QueriedSource;
        public String ReferredBy;
        public String Channel;
        private String[] FirstName_type_info = new String[]{'FirstName','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] LastName_type_info = new String[]{'LastName','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] BusinessName_type_info = new String[]{'BusinessName','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AddressLine1_type_info = new String[]{'AddressLine1','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AddressLine2_type_info = new String[]{'AddressLine2','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AddressLine3_type_info = new String[]{'AddressLine3','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] AddressLine4_type_info = new String[]{'AddressLine4','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] City_type_info = new String[]{'City','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] State_type_info = new String[]{'State','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PostalCode_type_info = new String[]{'PostalCode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] CountryCode_type_info = new String[]{'CountryCode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PhoneNumber1_type_info = new String[]{'PhoneNumber1','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PhoneNumber2_type_info = new String[]{'PhoneNumber2','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PhoneNumber3_type_info = new String[]{'PhoneNumber3','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PhoneNumber4_type_info = new String[]{'PhoneNumber4','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Email_type_info = new String[]{'Email','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Comments_type_info = new String[]{'Comments','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] QueriedSource_type_info = new String[]{'QueriedSource','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] ReferredBy_type_info = new String[]{'ReferredBy','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Channel_type_info = new String[]{'Channel','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'FirstName','LastName','BusinessName','AddressLine1','AddressLine2','AddressLine3','AddressLine4','City','State','PostalCode','CountryCode','PhoneNumber1','PhoneNumber2','PhoneNumber3','PhoneNumber4','Email','Comments','QueriedSource','ReferredBy','Channel'};
    }
    public class SalesMobilitySOAP {
        /************
        *  MANUAL MODIFICATION
        *
        *  start: obtain endpoint from custom settings
        *
        *************/
		public String endpoint_x = IntegrationSettings__c.getinstance().TelemarEndpoint__c;
		/************
        *  MANUAL MODIFICATION
        *
        *  end: obtain endpoint from custom settings
        *
        *************/
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        /************
        *  MANUAL MODIFICATION
        *
        *  start: set timeout from custom settings 
        *
        *************/
        public Integer timeout_x = Integer.valueOf(IntegrationSettings__c.getinstance().TelemarCalloutTimeout__c);
        /************
        *  MANUAL MODIFICATION
        *
        *  end: set timeout from custom settings 
        *
        *************/
        
        private String[] ns_map_type_info = new String[]{'http://www.adt.com/SalesMobility/', 'ADTSalesMobilityFacade'};
        public ADTSalesMobilityFacade.setAccountResponse_element setAccount(String MessageID,String TransactionType,String TelemarAccountNumber,String HREmployeeID,ADTSalesMobilityFacade.Account_element Account,ADTSalesMobilityFacade.AccountDSC_element AccountDSC,ADTSalesMobilityFacade.Disposition_element Disposition) {
            ADTSalesMobilityFacade.setAccountRequest_element request_x = new ADTSalesMobilityFacade.setAccountRequest_element();
            ADTSalesMobilityFacade.setAccountResponse_element response_x;
            /************
            *  MANUAL MODIFICATION
            *
            *  start: Provide credentials
            *
            *************/ 
			String authInfo = EncodingUtil.base64Encode(Blob.valueOf(IntegrationSettings__c.getinstance().TelemarUserName__c + ':' + IntegrationSettings__c.getinstance().TelemarPassword__c));
			inputHttpHeaders_x = new Map<String, String>();
			inputHttpHeaders_x.put('Authorization', 'Basic ' + authInfo);
			/************
            *  MANUAL MODIFICATION
            *
            *  end: Provide credentials
            *
            *************/ 
            request_x.MessageID = MessageID;
            request_x.TransactionType = TransactionType;
            request_x.TelemarAccountNumber = TelemarAccountNumber;
            request_x.HREmployeeID = HREmployeeID;
            request_x.Account = Account;
            request_x.AccountDSC = AccountDSC;
            request_x.Disposition = Disposition;
            Map<String, ADTSalesMobilityFacade.setAccountResponse_element> response_map_x = new Map<String, ADTSalesMobilityFacade.setAccountResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.adt.com/SalesMobility/setAccount',
              'http://www.adt.com/SalesMobility/',
              'setAccountRequest',
              'http://www.adt.com/SalesMobility/',
              'setAccountResponse',
              'ADTSalesMobilityFacade.setAccountResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        public ADTSalesMobilityFacade.setUnavailableTimeResponse_element setUnavailableTime(String MessageID,String TransactionType,String TelemarScheduleID,String HREmployeeID,DateTime ScheduleDateTime,String ScheduleOffsetToGMT,String Duration,String PostalCode,String ScheduleNotes,String UserHREmployeeID) {
            ADTSalesMobilityFacade.setUnavailableTimeRequest_element request_x = new ADTSalesMobilityFacade.setUnavailableTimeRequest_element();
            ADTSalesMobilityFacade.setUnavailableTimeResponse_element response_x;
            /************
            *  MANUAL MODIFICATION
            *
            *  start: Provide credentials
            *
            *************/
			String authInfo = EncodingUtil.base64Encode(Blob.valueOf(IntegrationSettings__c.getinstance().TelemarUserName__c + ':' + IntegrationSettings__c.getinstance().TelemarPassword__c));
			inputHttpHeaders_x = new Map<String, String>();
			inputHttpHeaders_x.put('Authorization', 'Basic ' + authInfo);
			/************
            *  MANUAL MODIFICATION
            *
            *  end: Provide credentials
            *
            *************/ 
            request_x.MessageID = MessageID;
            request_x.TransactionType = TransactionType;
            request_x.TelemarScheduleID = TelemarScheduleID;
            request_x.HREmployeeID = HREmployeeID;
            request_x.ScheduleDateTime = ScheduleDateTime;
            request_x.ScheduleOffsetToGMT = ScheduleOffsetToGMT;
            request_x.Duration = Duration;
            request_x.PostalCode = PostalCode;
            request_x.ScheduleNotes = ScheduleNotes;
            request_x.UserHREmployeeID = UserHREmployeeID;
            Map<String, ADTSalesMobilityFacade.setUnavailableTimeResponse_element> response_map_x = new Map<String, ADTSalesMobilityFacade.setUnavailableTimeResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.adt.com/SalesMobility/setUnavailableTime',
              'http://www.adt.com/SalesMobility/',
              'setUnavailableTimeRequest',
              'http://www.adt.com/SalesMobility/',
              'setUnavailableTimeResponse',
              'ADTSalesMobilityFacade.setUnavailableTimeResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        public ADTSalesMobilityFacade.setSGAppointmentResponse_element setSGAppointment(String MessageID,String TransactionType,String TelemarAccountNumber,String HREmployeeID,ADTSalesMobilityFacade.PreSale_element PreSale,ADTSalesMobilityFacade.Appointment_element Appointment) {
            ADTSalesMobilityFacade.setSGAppointmentRequest_element request_x = new ADTSalesMobilityFacade.setSGAppointmentRequest_element();
            ADTSalesMobilityFacade.setSGAppointmentResponse_element response_x;
            /************
            *  MANUAL MODIFICATION
            *
            *  start: Provide credentials
            *
            *************/
			String authInfo = EncodingUtil.base64Encode(Blob.valueOf(IntegrationSettings__c.getinstance().TelemarUserName__c + ':' + IntegrationSettings__c.getinstance().TelemarPassword__c));
			inputHttpHeaders_x = new Map<String, String>();
			inputHttpHeaders_x.put('Authorization', 'Basic ' + authInfo);
			/************
            *  MANUAL MODIFICATION
            *
            *  end: Provide credentials
            *
            *************/ 
            request_x.MessageID = MessageID;
            request_x.TransactionType = TransactionType;
            request_x.TelemarAccountNumber = TelemarAccountNumber;
            request_x.HREmployeeID = HREmployeeID;
            request_x.PreSale = PreSale;
            request_x.Appointment = Appointment;
            Map<String, ADTSalesMobilityFacade.setSGAppointmentResponse_element> response_map_x = new Map<String, ADTSalesMobilityFacade.setSGAppointmentResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.adt.com/SalesMobility/setSGAppointment',
              'http://www.adt.com/SalesMobility/',
              'setSGAppointmentRequest',
              'http://www.adt.com/SalesMobility/',
              'setSGAppointmentResponse',
              'ADTSalesMobilityFacade.setSGAppointmentResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        public ADTSalesMobilityFacade.setAppointmentResponse_element setAppointment(String MessageID,String TransactionType,String TelemarAccountNumber,String HREmployeeID,String LeadSource,ADTSalesMobilityFacade.PreSale_element PreSale,ADTSalesMobilityFacade.Appointment_element Appointment,String Dnis,String Promotion) {
            ADTSalesMobilityFacade.setAppointmentRequest_element request_x = new ADTSalesMobilityFacade.setAppointmentRequest_element();
            ADTSalesMobilityFacade.setAppointmentResponse_element response_x;
            /************
            *  MANUAL MODIFICATION
            *
            *  start: Provide credentials
            *
            *************/
			String authInfo = EncodingUtil.base64Encode(Blob.valueOf(IntegrationSettings__c.getinstance().TelemarUserName__c + ':' + IntegrationSettings__c.getinstance().TelemarPassword__c));
			inputHttpHeaders_x = new Map<String, String>();
			inputHttpHeaders_x.put('Authorization', 'Basic ' + authInfo);
			/************
            *  MANUAL MODIFICATION
            *
            *  end: Provide credentials
            *
            *************/ 
            request_x.MessageID = MessageID;
            request_x.TransactionType = TransactionType;
            request_x.TelemarAccountNumber = TelemarAccountNumber;
            request_x.HREmployeeID = HREmployeeID;
            request_x.LeadSource = LeadSource;
            request_x.PreSale = PreSale;
            request_x.Appointment = Appointment;
            request_x.Dnis = Dnis;
            request_x.Promotion = Promotion;
            request_x.Lsrc  = '';
            /*if( TransactionType != null && TransactionType.endsWith('-NSC')  ){
            	request_x.Lsrc  = LeadSource;
            }
            else{
            	request_x.Lsrc  = 'TEMP'; // standard value for non auto-convert leads
            }*/            
            Map<String, ADTSalesMobilityFacade.setAppointmentResponse_element> response_map_x = new Map<String, ADTSalesMobilityFacade.setAppointmentResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.adt.com/SalesMobility/setAppointment',
              'http://www.adt.com/SalesMobility/',
              'setAppointmentRequest',
              'http://www.adt.com/SalesMobility/',
              'setAppointmentResponse',
              'ADTSalesMobilityFacade.setAppointmentResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
    public class Appointment_element {
        public DateTime AppointmentStartDateTime;
        public String AppointmentOffsetToGMT;
        public String Duration;
        public String TaskCode;
        public String Notes;
        public String OriginalTelemarScheduleID;
        public String HREmployeeID;
        private String[] AppointmentStartDateTime_type_info = new String[]{'AppointmentStartDateTime','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] AppointmentOffsetToGMT_type_info = new String[]{'AppointmentOffsetToGMT','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Duration_type_info = new String[]{'Duration','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TaskCode_type_info = new String[]{'TaskCode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Notes_type_info = new String[]{'Notes','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] OriginalTelemarScheduleID_type_info = new String[]{'OriginalTelemarScheduleID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] HREmployeeID_type_info = new String[]{'HREmployeeID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'AppointmentStartDateTime','AppointmentOffsetToGMT','Duration','TaskCode','Notes','OriginalTelemarScheduleID','HREmployeeID'};
    }
    public class setAccountRequest_element {
        public String MessageID;
        public String TransactionType;
        public String TelemarAccountNumber;
        public String HREmployeeID;
        public ADTSalesMobilityFacade.Account_element Account;
        public ADTSalesMobilityFacade.AccountDSC_element AccountDSC;        
        public ADTSalesMobilityFacade.Disposition_element Disposition;
        private String[] MessageID_type_info = new String[]{'MessageID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TransactionType_type_info = new String[]{'TransactionType','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TelemarAccountNumber_type_info = new String[]{'TelemarAccountNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] HREmployeeID_type_info = new String[]{'HREmployeeID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Account_type_info = new String[]{'Account','http://www.adt.com/SalesMobility/','Account_element','0','1','false'};
        private String[] AccountDSC_type_info = new String[]{'AccountDSC','http://www.adt.com/SalesMobility/','AccountDSC_element','0','1','false'};
        private String[] Disposition_type_info = new String[]{'Disposition','http://www.adt.com/SalesMobility/','Disposition_element','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'MessageID','TransactionType','TelemarAccountNumber','HREmployeeID','Account','AccountDSC','Disposition'};
    }
    public class setAccountResponse_element {
        public String MessageID;
        public String MessageStatus;
        public String Error;
        private String[] MessageID_type_info = new String[]{'MessageID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] MessageStatus_type_info = new String[]{'MessageStatus','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Error_type_info = new String[]{'Error','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'MessageID','MessageStatus','Error'};
    }
    public class setSGAppointmentRequest_element {
        public String MessageID;
        public String TransactionType;
        public String TelemarAccountNumber;
        public String HREmployeeID;
        public ADTSalesMobilityFacade.PreSale_element PreSale;
        public ADTSalesMobilityFacade.Appointment_element Appointment;
        private String[] MessageID_type_info = new String[]{'MessageID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TransactionType_type_info = new String[]{'TransactionType','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TelemarAccountNumber_type_info = new String[]{'TelemarAccountNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] HREmployeeID_type_info = new String[]{'HREmployeeID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PreSale_type_info = new String[]{'PreSale','http://www.adt.com/SalesMobility/','PreSale_element','0','1','false'};
        private String[] Appointment_type_info = new String[]{'Appointment','http://www.adt.com/SalesMobility/','Appointment_element','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'MessageID','TransactionType','TelemarAccountNumber','HREmployeeID','PreSale','Appointment'};
    }
    public class setSGAppointmentResponse_element {
        public String MessageID;
        public String MessageStatus;
        public String Error;
        public String TelemarAccountNumber;
        public String TelemarScheduleID;
        private String[] MessageID_type_info = new String[]{'MessageID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] MessageStatus_type_info = new String[]{'MessageStatus','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Error_type_info = new String[]{'Error','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TelemarAccountNumber_type_info = new String[]{'TelemarAccountNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TelemarScheduleID_type_info = new String[]{'TelemarScheduleID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'MessageID','MessageStatus','Error','TelemarAccountNumber','TelemarScheduleID'};
    }
    public class setAppointmentRequest_element {
        public String MessageID;
        public String TransactionType;
        public String TelemarAccountNumber;
        public String HREmployeeID;
        public String LeadSource;
        public ADTSalesMobilityFacade.PreSale_element PreSale;
        public ADTSalesMobilityFacade.Appointment_element Appointment;
        public String Dnis;
        public String Promotion;
        public String Lsrc;
        private String[] MessageID_type_info = new String[]{'MessageID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TransactionType_type_info = new String[]{'TransactionType','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TelemarAccountNumber_type_info = new String[]{'TelemarAccountNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] HREmployeeID_type_info = new String[]{'HREmployeeID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] LeadSource_type_info = new String[]{'LeadSource','http://www.adt.com/SalesMobility/','LeadSource_element','1','1','false'};
        private String[] PreSale_type_info = new String[]{'PreSale','http://www.adt.com/SalesMobility/','PreSale_element','0','1','false'};
        private String[] Appointment_type_info = new String[]{'Appointment','http://www.adt.com/SalesMobility/','Appointment_element','1','1','false'};
        private String[] Dnis_type_info = new String[]{'Dnis','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Promotion_type_info = new String[]{'Promotion','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Lsrc_type_info = new String[]{'Lsrc','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'MessageID','TransactionType','TelemarAccountNumber','HREmployeeID','LeadSource','PreSale','Appointment','Dnis','Promotion','Lsrc'};
    }
    public class setAppointmentResponse_element {
        public String MessageID;
        public String MessageStatus;
        public String Error;
        public String TelemarAccountNumber;
        public String TelemarScheduleID;
        public String TaskCode;
        private String[] MessageID_type_info = new String[]{'MessageID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] MessageStatus_type_info = new String[]{'MessageStatus','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Error_type_info = new String[]{'Error','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TelemarAccountNumber_type_info = new String[]{'TelemarAccountNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TelemarScheduleID_type_info = new String[]{'TelemarScheduleID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TaskCode_type_info = new String[]{'TaskCode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'MessageID','MessageStatus','Error','TelemarAccountNumber','TelemarScheduleID','TaskCode'};
    }
    public class AccountDSC_element {
    	public String Location;
    	public String SourceSystem;
    	public String CustomerNumber;
    	public String Pic;
    	public String ReasonCode;
    	public String DiscLevel;
    	public String SiteNumber;
    	public String CTC;
    	public String EffectiveDate;
    	public String ExistingDISCO;
    	public String InfoOnly;
        private String[] Location_type_info = new String[]{'Location','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] SourceSystem_type_info = new String[]{'SourceSystem','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] CustomerNumber_type_info = new String[]{'CustomerNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'}; 
        private String[] Pic_type_info = new String[]{'Pic','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] ReasonCode_type_info = new String[]{'ReasonCode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] DiscLevel_type_info = new String[]{'DiscLevel','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] SiteNumber_type_info = new String[]{'SiteNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] CTC_type_info = new String[]{'CTC','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] EffectiveDate_type_info = new String[]{'EffectiveDate','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] ExistingDISCO_type_info = new String[]{'ExistingDISCO','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] InfoOnly_type_info = new String[]{'InfoOnly','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'Location','SourceSystem','CustomerNumber','Pic','ReasonCode','DiscLevel','SiteNumber','CTC','EffectiveDate','ExistingDISCO','InfoOnly'};
    }
    public class Account_element {
        public String FirstName;
        public String LastName;
        public String BusinessName;
        public String PhoneNumber1;
        public String PhoneNumber2;
        public String PhoneNumber3;
        public String PhoneNumber4;
        public String Email;
        public String Comments;
        public String ReferredBy;
        private String[] FirstName_type_info = new String[]{'FirstName','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] LastName_type_info = new String[]{'LastName','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] BusinessName_type_info = new String[]{'BusinessName','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PhoneNumber1_type_info = new String[]{'PhoneNumber1','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PhoneNumber2_type_info = new String[]{'PhoneNumber2','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PhoneNumber3_type_info = new String[]{'PhoneNumber3','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PhoneNumber4_type_info = new String[]{'PhoneNumber4','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Email_type_info = new String[]{'Email','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Comments_type_info = new String[]{'Comments','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] ReferredBy_type_info = new String[]{'ReferredBy','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'FirstName','LastName','BusinessName','PhoneNumber1','PhoneNumber2','PhoneNumber3','PhoneNumber4','Email','Comments','ReferredBy'};
    }
    public class setUnavailableTimeResponse_element {
        public String MessageID;
        public String MessageStatus;
        public String Error;
        public String TelemarScheduleID;
        private String[] MessageID_type_info = new String[]{'MessageID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] MessageStatus_type_info = new String[]{'MessageStatus','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Error_type_info = new String[]{'Error','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TelemarScheduleID_type_info = new String[]{'TelemarScheduleID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'MessageID','MessageStatus','Error','TelemarScheduleID'};
    }
    public class Disposition_element {
        public String DispositionCodeDesc;
        public DateTime DispositionDateTime;
        public String DispositionOffsetToGMT;
        public String DispositionComment;
        private String[] DispositionCodeDesc_type_info = new String[]{'DispositionCodeDesc','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] DispositionDateTime_type_info = new String[]{'DispositionDateTime','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] DispositionOffsetToGMT_type_info = new String[]{'DispositionOffsetToGMT','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] DispositionComment_type_info = new String[]{'DispositionComment','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'DispositionCodeDesc','DispositionDateTime','DispositionOffsetToGMT','DispositionComment'};
    }
    public class setUnavailableTimeRequest_element {
        public String MessageID;
        public String TransactionType;
        public String TelemarScheduleID;
        public String HREmployeeID;
        public DateTime ScheduleDateTime;
        public String ScheduleOffsetToGMT;
        public String Duration;
        public String PostalCode;
        public String ScheduleNotes;
        public String UserHREmployeeID;
        private String[] MessageID_type_info = new String[]{'MessageID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TransactionType_type_info = new String[]{'TransactionType','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TelemarScheduleID_type_info = new String[]{'TelemarScheduleID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] HREmployeeID_type_info = new String[]{'HREmployeeID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] ScheduleDateTime_type_info = new String[]{'ScheduleDateTime','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] ScheduleOffsetToGMT_type_info = new String[]{'ScheduleOffsetToGMT','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Duration_type_info = new String[]{'Duration','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PostalCode_type_info = new String[]{'PostalCode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] ScheduleNotes_type_info = new String[]{'ScheduleNotes','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] UserHREmployeeID_type_info = new String[]{'UserHREmployeeID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.adt.com/SalesMobility/','false','false'};
        private String[] field_order_type_info = new String[]{'MessageID','TransactionType','TelemarScheduleID','HREmployeeID','ScheduleDateTime','ScheduleOffsetToGMT','Duration','PostalCode','ScheduleNotes','UserHREmployeeID'};
    }
}