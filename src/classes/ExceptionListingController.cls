/************************************* MODIFICATION LOG ********************************************************************************************
* ExceptionListingController
*
* DESCRIPTION : Controller for exception listing page -- shows accounts in managers' towns
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  4/4/2012                - Original Version
*
*                                                   
*/

public class ExceptionListingController {

    public List<Town> towns {get;set;}
    public Integer totalAccounts {get;set;}
    public Integer totalSubAccounts {get;set;}
    public Integer totalExAccounts {get;set;}
    
    private List<ManagerTown__c> mgrtowns {get;set;}
    private List<Account> subAccounts {get;set;}                // accounts owned by subordinates
    private List<Account> exAccounts {get;set;}                 // accounts not owned by subordinates
    private boolean isManager;
    
    private class Town {
        public ManagerTown__c mt {get;set;}
        public List<TownAccount> accounts {get;set;}
        public Integer totalSubAccounts 
        {
            get;
            set {
                this.totalSubAccounts = value;
                this.totalAccounts = (this.totalExAccounts == null ? 0: this.totalExAccounts) + this.totalSubAccounts;
            }
        }
        public Integer totalExAccounts 
        {
            get;
            set {
                this.totalExAccounts = value;
                this.totalAccounts = (this.totalSubAccounts == null ? 0 : this.totalSubAccounts) + this.totalExAccounts;
            }
        }
        public Integer totalAccounts {get;set;}
        
        public Town(ManagerTown__c mt, List<TownAccount> accounts) {
            this.mt = mt;
            this.accounts = accounts;
            this.totalSubAccounts = 0;
            this.totalExAccounts = 0;
            this.totalAccounts = 0;
        }
    }
    
    private class TownAccount {
        
        public Account a {get;set;}
        
        public Boolean ownedBySubordinate {get;set;}
        
        public TownAccount(Account account) {
            this.a = account;
        }
    }

    public ExceptionListingController() {
        isManager = ProfileHelper.isManager();
        if (isManager) {
            totalAccounts = 0;
            totalSubAccounts = 0;
            totalExAccounts = 0;
            buildTowns();
        }
    }
    
    public PageReference onPageLoad() {
        // redirect if not manager
        PageReference ref = null;
        if (!isManager) {
            ref = Page.ShowError;
            ref.getParameters().put('Error','EL_notmanager');
        }
        return ref;
    }

    private void buildTowns() {
        mgrtowns = new List<ManagerTown__c>([
            Select Id, Name, TownId__c, ManagerID__c, BusinessID__c, Type__c
            From ManagerTown__c
            where ManagerID__c = :UserInfo.getUserId()
        ]);
        
        Map<String, ManagerTown__c> townmap = new Map<String, ManagerTown__c>();
        Set<String> channels = new Set<String>();
        Set<String> tids = new Set<String>();
        for (ManagerTown__c mt : mgrtowns) {
            tids.add(mt.TownID__c);
            for (String c : mt.Type__c.split(';')) {
                channels.add(c);
            }
            if (!townmap.containsKey(mt.TownID__c)) {
                townmap.put(mt.TownID__c, mt);
            }
        }
        
        List<User> subordinates = Utilities.getManagersAndUserSubordinates(new Set<Id>{UserInfo.getUserId()});
        
        subAccounts = new List<Account>([
            Select Id, Name, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c,
                AccountStatus__c, DispositionCode__c, Channel__c, ResaletownNumber__c, DisconnectReason__c,
                NewMoverType__c, Phone, Data_Source__c, Business_Id__c, TaskCode__c,
                LeadStatus__c, Owner.Name, SalesAppointmentDate__c
            From Account
            Where 
                //ProcessingType__c = :IntegrationConstants.PROCESSING_TYPE_USER    // SG accounts only
                RecordType.DeveloperName = : RecordTypeDevName.STANDARD_ACCOUNT
                and ResaleTownNumber__c IN :tids
                and OwnerId IN :subordinates
                and Channel__c IN :channels
                and SalesAppointmentDate__c >= LAST_N_DAYS:28
                and SalesAppointmentDate__c <= today
            order by SalesAppointmentDate__c asc
            limit 1000
        ]);
        
        /*List<AggregateResult> townSubCounts = new List<AggregateResult>([
            select count(Name) numaccts, PostalCodeID__r.TownID__c tid
            from Account
            Where 
                //ProcessingType__c = :IntegrationConstants.PROCESSING_TYPE_USER    // SG accounts only
                RecordType.DeveloperName = : RecordTypeDevName.STANDARD_ACCOUNT
                and ResaleTownNumber__c IN :tids
                and OwnerId IN :subordinates
                and Channel__c IN :channels
                and SalesAppointmentDate__c >= LAST_N_DAYS:28
            group by PostalCodeID__r.TownId__c
        ]);*/
        
        exAccounts = new List<Account>([
            Select Id, Name, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c,
                AccountStatus__c, DispositionCode__c, Channel__c, ResaletownNumber__c, DisconnectReason__c,
                NewMoverType__c, Phone, Data_Source__c, Business_Id__c, TaskCode__c,
                LeadStatus__c, Owner.Name, SalesAppointmentDate__c
            From Account
            Where 
                //ProcessingType__c = :IntegrationConstants.PROCESSING_TYPE_USER    // SG accounts only
                RecordType.DeveloperName = : RecordTypeDevName.STANDARD_ACCOUNT
                and ResaleTownNumber__c IN :tids
                and OwnerId NOT IN :subordinates
                and Channel__c IN :channels
                and SalesAppointmentDate__c >= LAST_N_DAYS:28
                and SalesAppointmentDate__c <= today
            order by SalesAppointmentDate__c asc
            limit 1000
        ]);
        
        /*List<AggregateResult> townExCounts = new List<AggregateResult>([
            select count(Name) numaccts, PostalCodeID__r.TownID__c tid
            from Account
            Where 
                //ProcessingType__c = :IntegrationConstants.PROCESSING_TYPE_USER    // SG accounts only
                RecordType.DeveloperName = : RecordTypeDevName.STANDARD_ACCOUNT
                and ResaleTownNumber__c IN :tids
                and OwnerId NOT IN :subordinates
                and Channel__c IN :channels
                and SalesAppointmentDate__c >= LAST_N_DAYS:28
            group by PostalCodeID__r.TownId__c
        ]);*/
        
        Map<ManagerTown__c, List<TownAccount>> townaccts = new Map<ManagerTown__c,List<TownAccount>>();
        
        addAccountsToMap(townmap, townaccts, exAccounts, false);
        addAccountsToMap(townmap, townaccts, subAccounts, true);
        
        //Map<String, Integer> townExCountMap = buildTownCountMap(townExCounts);
        //Map<String, Integer> townSubCountMap = buildTownCountMap(townSubCounts);
        
        towns = new List<Town>();
        for (ManagerTown__c mt : townaccts.keySet()) {
            Town t = new Town(mt, townaccts.get(mt));
            /*if (townSubCountMap.containsKey(mt.TownID__c)) {
                t.totalSubAccounts = townSubCountMap.get(mt.TownID__c);
                totalSubAccounts += t.totalSubAccounts;
            }
            if (townExCountMap.containsKey(mt.TownID__c)) {
                t.totalExAccounts = townExCountMap.get(mt.TownID__c);
                totalExAccounts += t.totalExAccounts;
            }
            */
            for (TownAccount ta : t.accounts) {
                if (ta.ownedBySubordinate) {
                    t.totalSubAccounts++;
                } else {
                    t.totalExAccounts++;
                }
            }
            totalSubAccounts += t.totalSubAccounts;
            totalExAccounts += t.totalExAccounts;
            totalAccounts += t.totalAccounts;
            towns.add(t);
        }
    }
    
    private void addAccountsToMap(Map<String, ManagerTown__c> townmap, Map<ManagerTown__c, List<TownAccount>> townaccts, List<Account> accounts, Boolean ownedBySubordinate) {
        
        for (Account a : accounts) {
            ManagerTown__c mt = townmap.get(a.ResaleTownNumber__c);
            Boolean isInChannel = false;
            for (String c : mt.Type__c.Split(';')) {
                if (c == a.Channel__c) {
                    isInChannel = true;
                    break;
                }
            }
            
            if (isInChannel &&
                 (
                    a.Business_Id__c == mt.BusinessID__c || 
                    (a.Business_Id__c != null && mt.BusinessID__c != null && a.Business_Id__c.contains(mt.BusinessId__c))
                )
            ) {
                TownAccount ta = new TownAccount(a);
                ta.ownedBySubordinate = ownedBySubordinate;
                if (townaccts.containsKey(mt)) {
                    townaccts.get(mt).add(ta);
                } else {
                    townaccts.put(mt, new List<TownAccount>{ta});
                }
            }
        }
    }
    
    private Map<String, Integer> buildTownCountMap(List<AggregateResult> townCounts) {
        Map<String, Integer> townCountMap = new Map<String, Integer>();
        for (AggregateResult ar : townCounts) {
            if (ar.get('tid') != null) {
                String tid = String.valueof(ar.get('tid'));
                if (!townCountMap.containsKey(tid)) {
                    townCountMap.put(tid, Integer.valueof(ar.get('numaccts')));
                }
            }
        }
        return townCountMap;
    }
}