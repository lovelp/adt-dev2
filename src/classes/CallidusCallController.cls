/************************************* MODIFICATION LOG ********************************************************************************************
*
* DESCRIPTION : CallidusCallController.cls is controller for CallidusCall.page. This contoller has init method that saves a Logging record indicating user has called Callidus 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Michael Tuckman				06/01/2014			- Origininal Version
*								
*/

public with sharing class CallidusCallController 
{
	
	public CallidusCallController()
	{
	
	}
	
	public PageReference init()
    {
    	Logging__c log = new Logging__c();
    	log.Action_Taken__c = 'Callidus Call';
    	insert log;
    	
    	return null;    	
    } 

}