/************************************* MODIFICATION LOG ********************************************************************************************
* GoogleGeoCodeBatch
* 
* DESCRIPTION : Batch class that identifies non-geocoded addresses from Address object, delegates to GoogleGeoCode class to obtain 
*               geocode data for such addresses then updates records in the Address object with the latitude and longitude.  
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*               				10/14/2011			- Original Version
*
*													  
*													
*/
global class GoogleGeoCodeBatch implements Database.batchable<sObject>, Database.AllowsCallouts {
	
	
	global final String query = 'select id, originalAddress__c, StandardizedAddress__c, latitude__c, Longitude__c from Address__c where (Latitude__c = null and createddate >= ';
	global final Integer DEFAULT_TIME_PERIOD = 36;
	
	global final String TIME_ZONE = '-05:00';
	
	//get Querylocator with the specitied query
	global Database.QueryLocator start(Database.BatchableContext bc) {
		
		
		String configuredTimePeriod = ResaleGlobalVariables__c.getinstance('GeocodeTimePeriod').value__c;
		
        
        // Assume the default value defined here
        Integer timePeriodInHours = DEFAULT_TIME_PERIOD;
        // But use the value from the custom setting as an override
        if (configuredTimePeriod != null && configuredTimePeriod.length() > 0) {
        	timePeriodInHours = Integer.valueOf(configuredTimePeriod);
        }
        
		Integer hoursToDecrement = timePeriodInHours - timePeriodInHours * 2;
		Datetime datetimeValue = Datetime.now().addHours(hoursToDecrement);
		
		// append the derived Datetime to the query by formatting the date as required and including the TIME_ZONE literal
    	// This approach is necessary since the date format mask 'yyyy-MM-dd\'T\'HH:mm:ssZ' returns 
    	// a value 2011-01-01T00:00:00-0500 whereas the required format is 2011-01-01T00:00:00-05:00
		System.debug(query + datetimeValue.format('yyyy-MM-dd\'T\'HH:mm:ss') + TIME_ZONE);
		
		return Database.getQueryLocator(query + datetimeValue.format('yyyy-MM-dd\'T\'HH:mm:ss') + TIME_ZONE + ') or ReProcessGeoCode__c = true Limit 50000');
	}
	
	
	global void execute(Database.BatchableContext bc, List<sObject> addresses) {
		
		GoogleGeoCodeResults result;
		List<Address__c> addressesToUpdate = new List<Address__c>();
		
		for(sObject sobjAddr : addresses){
			try{
				Address__c address = (Address__c)sobjAddr;
				result = new GoogleGeoCodeResults();
				result = GoogleGeoCode.GeoCodeAddress(address.StandardizedAddress__c);
				//if(result.ExactMatch == 'true')
				//{
					address.Latitude__c = decimal.valueOf(result.AddressLat);
					address.Longitude__c = decimal.valueOf(result.AddressLon);
					address.StandardizedAddress__c = result.FormattedAddress;
					address.ReProcessGeoCode__c = false;
					addressesToUpdate.add(address);
				//}
				//else
				//{
				//	address.ReProcessGeoCode__c = false;
				//	addressesToUpdate.add(address);
				//}
			}
			catch(Exception ex)
			{
				//Do Nothing. If there is an exception, the record will be tried again during the next run as long
				//as the next run is within 36 hours.
				//Address__c address = (Address__c)sobjAddr;
				//address.ReProcessGeoCode__c = true;
				//addressesToUpdate.add(address);
			}
		}
		if(addressesToUpdate.size() > 0) {
			update addressesToUpdate;
		}	
		
	}
	
	global void finish(Database.BatchableContext bc) {
		
	}

}