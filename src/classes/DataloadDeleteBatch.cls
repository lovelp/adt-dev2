/************************************* MODIFICATION LOG ********************************************************************************************
* DeleteTasks
*
* DESCRIPTION : Deletes the DataLoadBatchRecords 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    TICKET          REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Mounika Anna                07/09/2018                           - Original Version
*/
global class DataloadDeleteBatch implements Database.batchable<SObject> , Database.stateful{

    global Database.QueryLocator start(Database.BatchableContext BC){
       String query='' ;
        if(label.DataLoadDeleteBatchQuery!=null){
        	query = label.DataLoadDeleteBatchQuery;
       		System.Debug('query====='+query);
        }
        return Database.getQueryLocator(query); 
    }
    
    global void execute(Database.BatchableContext BC, List<DataloadTemp__c > scope){
        if(scope.size() > 0){
      	  // Delete the existing DataLoadBatch records
          Database.DeleteResult[] drList = database.delete(scope,false);
        }
    }
    
    // The finish method is called at the end after the deletion process is completed.
    global void finish(Database.BatchableContext BC){
        
    }
}