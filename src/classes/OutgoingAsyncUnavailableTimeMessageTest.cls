@istest
public class OutgoingAsyncUnavailableTimeMessageTest{
    
    static testMethod void testOutgoingAsyncUnavailableTimeMessage(){
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        insert rgvList;
        

        UserRole ur = new UserRole(Name = 'CEO');
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
        
		User testUserA = new User(
    	Alias = 'standard', Email='standarduser@testorg.com',  
    	EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
    	LocaleSidKey='en_US', ProfileId = p.Id,
    	TimeZoneSidKey='America/Los_Angeles', UserName='testUserA0@testorganise.com');
        insert testUserA;
        
        
        User testUserB = new User(
    	Alias = 'standard', Email='standarduser1@testorg.com',  
    	EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
    	LocaleSidKey='en_US', ProfileId = p.Id,
    	TimeZoneSidKey='America/Los_Angeles', UserName='testUserB1@testorganise.com');
        //testUserB.Id = e.OwnerId;
        testUserB.ManagerID = testUserA.id;
        testUserB.UserRoleId = ur.Id;
        //testUserB.Profile.Name = p.Name;
        insert testUserB;
        
        Event e = new Event();
        e.ScheduleID__c = 'id123';
        e.DurationInMinutes = 20;
        e.OwnerId = testUserA.id;
        e.ActivityDateTime = DateTime.newInstance(2019, 07, 13, 05, 51, 34);
        insert e;
        
        
        IntegrationSettings__c i=new IntegrationSettings__c();
        i.TelemarEndpoint__c='';
        insert i;
        OutgoingAsyncUnavailableTimeMessage oAUTMInstance = new OutgoingAsyncUnavailableTimeMessage();
        oAUTMInstance.eventId = e.Id;
        oAUTMInstance.u=[select id,EmployeeNumber__c from User where id=:Userinfo.getUSerId() limit 1];
        oAUTMInstance.e=new Event(startdatetime=system.today(),enddatetime=system.today(),PostalCode__c='400610',Description='abc',ScheduleID__c='1',ownerid=Userinfo.getUSerId());
        
        ADTSalesMobilityFacadeMockImpl fakeResp=new ADTSalesMobilityFacadeMockImpl();
        Test.setMock(WebServiceMock.class, fakeResp);
        test.starttest(); 
        oAUTMInstance.processResponse();
        test.stoptest();
        
    }

    /*static testmethod void exceptionsTest(){
        OutgoingAsyncUnavailableTimeMessage oAUTMInstance = new OutgoingAsyncUnavailableTimeMessage();
        ADTSalesMobilityFacadeMockImpl fakeResp = new ADTSalesMobilityFacadeMockImpl();
        ADTSalesMobilityFacadeMockImpl.exceptionFlag = true;
        Test.startTest(); 
        Test.setMock(WebServiceMock.class, fakeResp);
        oAUTMInstance.processResponse();
    }*/
    
    public testmethod static void method2(){
        
        IntegrationSettings__c i=new IntegrationSettings__c();
        i.TelemarEndpoint__c='';
        insert i;
        OutgoingAsyncUnavailableTimeMessage oAUTMInstance = new OutgoingAsyncUnavailableTimeMessage();
        oAUTMInstance.u=[select id,EmployeeNumber__c from User where id=:Userinfo.getUSerId() limit 1];
        oAUTMInstance.e=new Event(startdatetime=system.today(),enddatetime=system.today(),PostalCode__c='400610',Description='abc',ScheduleID__c='1',ownerid=Userinfo.getUSerId());
        
        ADTSalesMobilityFacadeMockImpl fakeResp = new ADTSalesMobilityFacadeMockImpl();
        Test.setMock(WebServiceMock.class, fakeResp);
        test.starttest(); 
        oAUTMInstance.processResponse();
        test.stoptest();
        
    }
    
}