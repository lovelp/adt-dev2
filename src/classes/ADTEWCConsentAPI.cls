/************************************* MODIFICATION LOG ********************************************************************************************
* ADTEWCConsentAPI
*
* DESCRIPTION : Serves as a class to be used for invocation of ADT EWC Consent Callout
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE              TICKET               REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* V Shah                    06/21/2019              HRM -            - Original Version
* 
*/

public class ADTEWCConsentAPI{
    /**
* Wrapper class to store request of EWC
**/
    public static String ServiceTransactionType;
    public class addEWCWrapper{
        public Long customer{get;set;}
        public Long phone{get;set;}
        public String consentDateTime{get;set;}
        public String lead{get;set;}
        public String dnis{get;set;}
        public String actor{get; set;}
        public addEWCWrapper(Long phone,Long customer,String consentDateTime,String actor, String lead, String dnis){
            this.phone=phone;
            if(customer != 0){
                this.customer=customer;
            }
            this.actor=actor;
            this.lead=lead;
            this.dnis = dnis; // added by Abhinav
            this.consentDateTime=consentDateTime;   
        }
    }
    public class sendTextWrapper{
        public Long customer{get;set;}
        public Long phone{get;set;}
        public String zip{get;set;}
        public String lead{get;set;}
        public String actor{get; set;}
        public String dnis{get;set;}
        public String state{get; set;}
        public String textProviderTemplate{get;set;}
        public String textProviderCampaign{get; set;}
        public sendTextWrapper(Long phone,Long customer,String actor, String lead, String dnis, String zip, String state, String textProviderTemplate, String textProviderCampaign){
            this.phone=phone;
            if(customer != 0){
                this.customer=customer;
            }
            this.actor=actor;
            this.lead=lead;
            this.dnis=dnis;
            this.zip=zip;
            this.state=state;
            this.lead=lead;
            this.textProviderTemplate=textProviderTemplate;
            this.textProviderCampaign=textProviderCampaign;
        }
    }
    
    public class updateWrapper{
        public Long phone{get;set;}
        public Long customer{get;set;}
        public String consentDateTime{get;set;}
        public String actor{get; set;}
        public String lead{get;set;}
        public Boolean consent{get; set;}
        public Boolean dnt{get; set;}
        public String dnis{get;set;}
        
        public updateWrapper(Long phone,Long customer,String consentDatetime, String actor, String lead, Boolean consent, Boolean dnt, String dnis){
            this.phone=phone;
            if(customer != 0){
                this.customer=customer;
            }
            this.consentDateTime = consentDateTime;
            this.actor=actor;
            this.lead=lead;
            this.consent = consent;
            this.dnt = dnt;
            this.dnis=dnis;
        }
    }
    
    /**
* This method constructs the JSON & performs callout
* Called from SciQuoteTriggerHelper & ADTWebLeadsController
* @Param Id Account Id & Audit Log Id
* @ReturnType void
**/
    public static void sendToEWC(Id accountId, Id aLogId){
        String flagToRunEwc = ResaleGlobalVariables__c.getinstance('runEwcMarketing')!=null && String.isNotBlank(ResaleGlobalVariables__c.getinstance('runEwcMarketing').value__c) ? ResaleGlobalVariables__c.getinstance('runEwcMarketing').value__c : 'true';
        if(flagToRunEwc.equalsIgnoreCase('true')){
            try{           
                List<RequestQueue__c> reqQueuesToInsert = new List<RequestQueue__c>();
                account acct  = [Select Id, MMBCustomerNumber__c ,phone,PhoneNumber2__c,PhoneNumber3__c,PhoneNumber4__c,TelemarAccountNumber__c from Account Where Id =: accountId AND TelemarAccountNumber__c != null Limit 1];
                reqQueuesToInsert = createRequestQueues(acct,aLogId,'Ready');
                // Insert Req Queue List
                if(reqQueuesToInsert.size()>0){
                    insert reqQueuesToInsert;
                }
            }catch(exception e){
                System.debug('Send to EWC failed'+e.getMessage() +'-LNO-' + e.getLineNumber() );
            }
        }
    }
    //For SendText processing
    public static void sendText(Map<Id,Id> mapAccAudit){
        String flagToRunEwc = ResaleGlobalVariables__c.getinstance('runEwcMarketing')!=null && String.isNotBlank(ResaleGlobalVariables__c.getinstance('runEwcMarketing').value__c) ? ResaleGlobalVariables__c.getinstance('runEwcMarketing').value__c : 'true';
        if(flagToRunEwc.equalsIgnoreCase('true')){
            try{
                list<RequestQueue__c> finalReqlist = new list<RequestQueue__c>();
                for(Account acc: [Select Id, MMBCustomerNumber__c ,SitePostalCode__c,SiteState__c,phone, TelemarAccountNumber__c, PhoneNumber2__c, PhoneNumber3__c, PhoneNumber4__c FROM Account Where Id IN :mapAccAudit.keySet()]){
                    List<RequestQueue__c> reqQueuesToInsert = new List<RequestQueue__c>();
                    AuditLog__c aLog = [SELECT id,CallData__r.DNIS__r.ByPassSendtext__c FROM AuditLog__c WHERE Account__c =:acc.Id limit 1];
                    String reqstatus = (aLog.CallData__c != null && aLog.CallData__r.DNIS__c != null && aLog.CallData__r.DNIS__r.ByPassSendtext__c)? 'Bypass':'Ready';
                    reqQueuesToInsert = createRequestQueues(acc,mapAccAudit.get(acc.Id),reqstatus);
                    finalReqlist.addAll(reqQueuesToInsert);
                }
                // Insert Req Queue List
                if(finalReqlist.size()>0){
                    insert finalReqlist;
                }
            }catch(exception e){
                System.debug('Send Text Failed'+e);
            }
        }  
    }
    
    /**
    * This method creates the EWC request queues
    * @Param Account
    * @ReturnType list of RequestQueue__c
    **/
    public static list<RequestQueue__c> createRequestQueues(Account acc,Id aLogId, string reqstatus){
        String nowDateTime = Datetime.now().format('yyyy-MM-dd HH:mm:ssXXX');
        Set<String> formatedPhoneNumbers = new Set<String>();
        String primaryPhone;
        String dnis = '';
        String consentDateTime = nowDateTime;
        String telemarId = String.valueOf(acc.TelemarAccountNumber__c);      
        String customer = String.isNotBlank(acc.MMBCustomerNumber__c) ?acc.MMBCustomerNumber__c : '0';
        String actor = 'SFDC'; 
        String lead = telemarId; 
        List<RequestQueue__c> reqQueuesList = new List<RequestQueue__c>();
        SendTextConfiguration__mdt conf = new SendTextConfiguration__mdt();
        List<AuditLog__c> aLog = [SELECT id,CallData__r.DNIS__r.ByPassSendtext__c,CallData__r.DNIS__r.Name,PromotionCode__c,StateCode__c,ZipCode__c, PrimaryPhone__c,SecondaryPhone__c FROM AuditLog__c WHERE id =:aLogId limit 1];
        dnis=aLog.size() > 0 ? aLog[0].PromotionCode__c : '';
        
        if(aLog.size() > 0 && String.isNotBlank(aLog[0].PrimaryPhone__c)){
            formatedPhoneNumbers.add(aLog[0].PrimaryPhone__c);
        }
        if(aLog.size() > 0 && String.isNotBlank(aLog[0].SecondaryPhone__c)){
            formatedPhoneNumbers.add(aLog[0].SecondaryPhone__c);
        }
        for(String phone: formatedPhoneNumbers){
            // Create JSON to store in service message
            String jsonString = '';
            // Create Request queue for each phone number
            RequestQueue__c rq = new RequestQueue__c();
            if(ServiceTransactionType == 'sendEWCRequest'){
                jsonString =  JSON.serialize(new addEWCWrapper(Long.valueOf(phone.replaceAll('\\D', '')),Long.valueOf(customer.replaceAll('\\D', '')),consentDateTime,actor, lead, dnis),true);
            }
            else if(ServiceTransactionType == 'sendText'){
                String textProviderTemplate = '';
                String textProviderCampaign = '';
                try{
                    conf =[ Select TextProviderCampaign__c,TextProviderTemplate__c from SendTextConfiguration__mdt where MasterLabel='sendText' Limit 1];
                    textProviderTemplate = conf.TextProviderTemplate__c;
                    textProviderCampaign = conf.TextProviderCampaign__c;               
                    }
                catch(exception ex){
                    system.debug('exception'+ex.getMessage() +'--LNO--'+ex.getLineNumber());
                }
                jsonString =  JSON.serialize(new sendTextWrapper(Long.valueOf(phone.replaceAll('\\D', '')),Long.valueOf(customer.replaceAll('\\D', '')),actor, lead, dnis, (alog.size() > 0 && String.isNotBlank(aLog[0].ZipCode__c)) ? aLog[0].ZipCode__c : '' , (aLog.size() > 0 && String.isNotBlank(aLog[0].StateCode__c)) ? aLog[0].StateCode__c : '', textProviderTemplate, textProviderCampaign),true);
            }
            rq.AccountID__c = acc.Id;
            if(aLogId != null){
                rq.AuditLog__c = aLogId;
            }
            rq.RequestStatus__c = reqstatus;
            rq.ServiceTransactionType__c = ServiceTransactionType;
            rq.MessageID__c = phone;
            rq.ServiceMessage__c = jsonString;
            rq.RequestDate__c = system.now();
            rq.Counter__c = 0;
            reqQueuesList.add(rq);
        }
        return (reqQueuesList.size()>0)?reqQueuesList:null;
    }
    
    
    /**
    * This method is used to make the callout to AdtCustContactEWCService
    * @Param jsonString
    * @ReturnType Void
    **/
    public static RequestQueue__c doEWCCallOut(RequestQueue__c requeue,String TransactionType){
        Httprequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        try{
            
            String userName = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPUsername__c:'abc';
            String password = (!Test.isRunningTest())?IntegrationSettings__c.getInstance().DPPassword__c:'def';
            String endPointURL = (!Test.isRunningTest())? IntegrationSettings__c.getInstance().EWCBaseURL__c:'xyz';
            if(TransactionType == 'sendEWCRequest'){
                endPointURL += '/consents';
            }
            if(TransactionType == 'sendText'){
                endPointURL += '/sendText';
            }
            
            Blob headerValue = Blob.valueOf(userName + ':' + password);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            request.setHeader('Authorization', authorizationHeader);
            request.setHeader('Content-Type', 'application/json');
            request.setEndpoint(endPointURL);
            request.setTimeout(Integer.ValueOf(120000));
            request.setMethod('POST');
            request.setbody(requeue.ServiceMessage__c);
            // Send the request
            response = http.send(request);
        }catch(exception e){
            // error response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            requeue.ErrorDetail__c = 'Exception: '+ e.getMessage();
        }
        if(response != null && (response.getStatusCode() == 202 || response.getStatusCode() == 204)){
            // success response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_SUCCESS;
            requeue.ErrorDetail__c = null;
        }
        else{
            // error response
            requeue.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            requeue.ErrorDetail__c = '\n EWC Response: Invalid Response returned';
        }
        return requeue;
    }
    
    public static Map<String, Boolean> updateADTEWCConsentAPI(Map<String, OptOutContactsAppplicationController.OptOutPhonesWrapper> phoneNumbersToOptOut, String EndPointName, Decimal logcount,Account a){
        String customerNumber = String.isNotBlank(a.MMBCustomerNumber__c) ? a.MMBCustomerNumber__c : '0';
        String telemarNumber = String.isNotBlank(a.TelemarAccountNumber__c) ? a.TelemarAccountNumber__c : '';
        List<AuditLog__c> aLog = new List<AuditLog__c>();
        try{
           aLog = [SELECT id,PromotionCode__c FROM AuditLog__c WHERE Account__c =:a.Id limit 1];
        }catch(exception ex){
            System.debug('no audit log');
        }
        String dnis=(aLog.size() > 0 && String.isNotBlank(aLog[0].PromotionCode__c) ) ? aLog[0].PromotionCode__c : String.isNotBlank(a.DNIS__c) ? a.DNIS__c : '';
        List<Disposition__c> dispositionRecordsToInsert = new List<Disposition__c>();
        Map<String, Boolean> failSuccessNumbersMap = new Map<String, Boolean>();
        //  Iterate through phoneNumbersToOptOut map and make a callout for each phone number
        for(String phoneNumber : phoneNumbersToOptOut.keyset()) {
            Disposition__c newDispo = new Disposition__c();
            newDispo.DispositionType__c = 'System Generated';
            newDispo.DispositionDetail__c = 'Texting Opt-out requested for '+phoneNumber;
            newDispo.DispositionDate__c = DateTime.now();
            newDispo.AccountID__c = phoneNumbersToOptOut.get(phoneNumber).accId;
            // Dispositioned by Global Admin
            newDispo.DispositionedBy__c = '00530000005dBvw';
            
            try{
                // API Configuration : Custom metadata where all API related configuration is saved.
                String userName = (!Test.isRunningTest()) ? IntegrationSettings__c.getInstance().DPUsername__c : 'abc';
                String password = (!Test.isRunningTest()) ? IntegrationSettings__c.getInstance().DPPassword__c : 'def';
                String url = (!Test.isRunningTest()) ? IntegrationSettings__c.getInstance().EWCBaseURL__c+'/consents' : 'xyz';
                Decimal timeout =  120000;
                
                // Instantiate a new http object
                Http h = new Http();        
                // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
                HttpRequest req = new HttpRequest();
                req.setEndpoint(url);
                req.setTimeout(Integer.ValueOf(120000));
                Blob headerValue = Blob.valueOf(username + ':' + password);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                req.setHeader('Authorization', authorizationHeader);
                req.setHeader('Content-Type', 'application/json');
                req.setMethod('PUT'); 
                
                String jsonString = JSON.serialize(new updateWrapper(Long.valueOf(phoneNumber.replaceAll('\\D', '')),Long.valueOf(customerNumber),Datetime.now().format('yyyy-MM-dd HH:mm:ssXXX'),'sfdc',telemarNumber,false,true,dnis),true);
                req.setbody(jsonString);
                HttpResponse res;
                // Send the request, and return a response
                res = h.send(req);
                System.debug('response is'+res);
                // If the Response status code of connection lies between 200 and 300 then only we save data as Connection Success
                if(res.getStatusCode() == 202 || res.getStatusCode() == 204) {
                    newDispo.Comments__c = 'Request Successful';
                    failSuccessNumbersMap.put(phoneNumber, true);
                } 
                else {
                    newDispo.Comments__c = 'Request Failed';
                    failSuccessNumbersMap.put(phoneNumber, false);
                }
            }
            catch(Exception e){
                newDispo.Comments__c = 'Request Failed ';
                failSuccessNumbersMap.put(phoneNumber, false);
            }
            
            dispositionRecordsToInsert.add(newDispo);
        }
        
        if(!dispositionRecordsToInsert.isEmpty()){
            insert dispositionRecordsToInsert;
        }
        return failSuccessNumbersMap;
    }
}