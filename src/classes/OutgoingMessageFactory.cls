/************************************* MODIFICATION LOG ********************************************************************************************
* OutgoingMessageFactory
*
* DESCRIPTION : Returns proper subclass of OutgoingMessage
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					1/19/2012				- Original Version
*
*													
*/

public with sharing class OutgoingMessageFactory {
	
	private static final String UNAVAILABLE_TIME_PREFIX = 'setUnavailableTime';
	private static final String SETAPPOINTMENT_PREFIX = 'setAppointmentNew';
	
	public static OutgoingMessage createOutgoingMessage(String serviceTransactionType)
	{
		if (serviceTransactionType != null && serviceTransactionType.contains(UNAVAILABLE_TIME_PREFIX) )
		{
			return new OutgoingAsyncUnavailableTimeMessage();
		}
		else if (serviceTransactionType != null && serviceTransactionType.contains(SETAPPOINTMENT_PREFIX) ){
			return new OutgoingAppointmentMessage();
		}
		
		return new OutgoingAccountMessage();
	}
}