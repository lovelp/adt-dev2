/*******************************************************************************************************
 * Description: Helper class for data load object trigger. 
 * 
 * Modificaiton log
 * 
 * Modified By                   Date           Ticket          Comments
 * ----------------------------------------------------------------------------------------
 * Srinivas Yarramsetti         6/18/2018       HRM-4051        Initial design for ticket 
 *
 */

public class DataLoadTriggerHelper{
    
    public static void initialprocessing(list<DataloadTemp__c> dataloadRecordNew){
    
        Map<String, String> dnisTeleMarSourMap = new Map<String, String>();
        for(DNIS__c dnis: [SELECT id, Tel_Lead_Source__c, Tel_Lead_Source__r.Name, Name FROM DNIS__c LIMIT 50000]){
            if(dnis.Tel_Lead_Source__c != null){
                dnisTeleMarSourMap.put(dnis.Name, dnis.Tel_Lead_Source__r.Name);
            }
        }    
            
        //Setup Group Id
        list<DataloadTemp__c> recentRecCreateByUser = [SELECT id, GroupId__c, SystemModstamp FROM DataloadTemp__c WHERE CreatedById =: Userinfo.getUserId() 
                                                       AND CreatedDate = TODAY ORDER BY SystemModstamp DESC LIMIT 1];
        //User Alias + “-“ + Upload Type + “-“ + Date + “-“ + Counter e.g. mtuck-Account-20180612-01
        User usr = [SELECT id, alias FROM User WHERE id=:Userinfo.getUserId()];
        String groupId = usr.alias.toUpperCase()+'-'+System.now().format('yyyyMMdd');//Date.Today().year()+Date.Today().month()+Date.Today().Day();
        if(recentRecCreateByUser.size()>0 && String.isNotBlank(recentRecCreateByUser.get(0).GroupId__c)){
            if(Decimal.valueOf((system.now().getTime()/(1000*60) - recentRecCreateByUser.get(0).SystemModstamp.getTime()/(1000*60))) > 2){
                String[] stringlist = recentRecCreateByUser.get(0).GroupId__c.split('-');
                groupId = groupId+'-'+String.valueOf((Integer.valueOf(stringlist.get((stringlist.size()-1)))+1));
                system.debug('Group Id##'+groupId);
            }else{
                groupId = String.isNotBlank(recentRecCreateByUser.get(0).GroupId__c)?recentRecCreateByUser.get(0).GroupId__c:groupId;
            }
        }else{
            groupId = groupId+'-'+'1';
        }
    
        for(DataloadTemp__c dlt: dataloadRecordNew){
            system.debug('Data Load Temp Record: '+ dlt);
            dlt.GroupId__c = groupId;
            if(String.isNotBlank(dlt.SiteLastName__c)){
                dlt.UniqueId__c = dlt.SiteLastName__c.trim()+'+';
            }
            if(String.isNotBlank(dlt.AddressLine1__c)){
                dlt.UniqueId__c = dlt.UniqueId__c+dlt.AddressLine1__c.trim()+'+';
            }
            if(String.isNotBlank(dlt.sitePostalCode__c)){
                dlt.UniqueId__c = dlt.UniqueId__c+dlt.sitePostalCode__c.trim();
            }
            if(String.isNotBlank(dlt.SitePostalCode__c) && dlt.SitePostalCode__c.contains('-')){
                dlt.PostalCodeAddOn__c = dlt.SitePostalCode__c.substringAfter('-');
                dlt.SitePostalCode__c = dlt.SitePostalCode__c.substringBefore('-');
            }
            //HRM-10450 Buisiness Id change Start
            dlt.BusinessId__c = Channels.getFormatedBusinessId(dlt.BusinessId__c, Channels.BIZID_OUTPUT.FULL);
            if(String.isBlank(dlt.BusinessId__c)){
                // If business Id is blank, default to Resi
                dlt.BusinessId__c = Channels.TYPE_RESIDENTIAL;
            }
            if(String.isBlank(dlt.Channel__c) && String.isNotBlank(dlt.BusinessId__c)){
                //Populate default Channel if empty
                dlt.Channel__c = Channels.getDefaultChannel(dlt.BusinessId__c);
            }
            //HRM-10450 Buisiness Id change End
            if(String.isBlank(dlt.TelemarLeadSource__c) && dnisTeleMarSourMap.keyset().size()>0 && String.isNotBlank(dnisTeleMarSourMap.get(dlt.DNIS__c))){
                dlt.TelemarLeadSource__c = dnisTeleMarSourMap.get(dlt.DNIS__c);     
            }
            if(String.isBlank(dlt.QueriedSource__c) && dnisTeleMarSourMap.keyset().size()>0 && String.isNotBlank(dnisTeleMarSourMap.get(dlt.DNIS__c))){
                dlt.QueriedSource__c = dnisTeleMarSourMap.get(dlt.DNIS__c);
            }
        }
    }
}