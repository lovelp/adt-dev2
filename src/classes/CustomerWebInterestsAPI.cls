/************************************* MODIFICATION LOG ********************************************************************************************
* CustomerWebInterestsAPI
*
* DESCRIPTION : API Class.
*
* 
* Jitendra Kothari				10/10/2019			HRM-10967			Configurator Cheetahmail Requirement
*/
@RestResource(urlmapping='/processWebInterest/*')
global class CustomerWebInterestsAPI {
    public class WebInterestRequest{
        public String uniqueVisitorId;
        public String configuratorVersion;
        public String friendlyId;
        public String email;
        public String uniqueId;
        public String sequenceId;
        public String question;
        public String answer;
    }
    
    public class SfdcError{
        public list<errors> errors;
    }
    public class errors{
        public string status;
        public string errorCode;
        public string errorMessage;
    }
    
    @HTTPPost
    global static void processRequest(){
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        try{
            if(String.isBlank(request.requestBody.toString())){
                response.statusCode = 400;
                response.responseBody = Blob.valueOf(errorResponse('JSON request is empty.',400));
            }
            else{
                WebInterestRequest webinter = new WebInterestRequest();
                webinter =  (WebInterestRequest)JSON.deserialize(request.requestBody.toString(), WebInterestRequest.class);
                if(String.isBlank(webinter.uniqueVisitorId)){
                    response.statusCode = 400;
                    response.responseBody = Blob.valueOf(errorResponse(PartnerAPIMessaging__c.getinstance('400').Error_Message__c+'. Unique visitor id is empty.',400));
                }
                else{
                    updateWebInterest(webinter);
                    response.statusCode = 200;
                    response.responseBody = Blob.valueOf('{"message":"Web interest saved."}');  
                }
            }
        }catch(Exception ae){
            response.statusCode = 400;
            response.responseBody = Blob.valueOf(errorResponse(PartnerAPIMessaging__c.getinstance('500').Error_Message__c,500));
            ADTApplicationMonitor.log ('Customer web interest API', ae.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);
        }
    }
    
    public static void updateWebInterest(WebInterestRequest webIntObj){
        list<CustomerWebQuestion__c> custwebqueslist = [SELECT id, Question__c, QuestionId__c FROM CustomerWebQuestion__c ORDER BY CreatedDate DESC LIMIT 50000];
        CustomerWebQuestion__c cuswebnew;
        Id indenCuswebid;
        //Check if there are matching questions on question id.
        if(custwebqueslist.size()>0){
            for(CustomerWebQuestion__c cwb:custwebqueslist){
                if(cwb.Question__c.equalsIgnoreCase(webIntObj.question)){
                    indenCuswebid = cwb.Id;
                    break;
                }
            }
        }
        else{
            cuswebnew = new CustomerWebQuestion__c();
            cuswebnew.Question__c = webIntObj.question;
            cuswebnew.QuestionId__c = webIntObj.uniqueId;
        }
        //If the actual question didn't match.
        if(String.isBlank(indenCuswebid)){
            cuswebnew = new CustomerWebQuestion__c();
            cuswebnew.Question__c = webIntObj.question;
            cuswebnew.QuestionId__c = webIntObj.uniqueId;
        }
        //Insert new questions
        if(cuswebnew != null){
            insert cuswebnew;
            indenCuswebid = cuswebnew.id;
        }
        list<CustomerInterestConfig__c> cuslist = [SELECT id, UniqueQuestionId__c FROM CustomerInterestConfig__c 
                                                   WHERE UniqueVisitorNumber__c =:webIntObj.uniqueVisitorId
                                                   AND FriendlyId__c =:webIntObj.friendlyId
                                                   AND QuestionLookup__c =:indenCuswebid];
        if(cuslist.size()>0){
            cuslist[0].Response__c = webIntObj.answer;
            update cuslist;
        }
        else{
            CustomerInterestConfig__c cic = new CustomerInterestConfig__c();
            cic.ConfiguratorVersion__c = webIntObj.configuratorVersion;
            //cic.Entity__c = webIntObj.question;
            cic.FriendlyId__c = webIntObj.friendlyId;
            cic.Response__c = webIntObj.answer;
            cic.SequenceNumber__c = webIntObj.sequenceId;
            //Start HRM-10967
            //cic.Type__c = (webIntObj.question == 'Product Information')?'Product':'Question';
            map<String, String> mappings = getInterestTypes();
            if(mappings.containsKey(webIntObj.question)){
            	cic.Type__c = mappings.get(webIntObj.question);
            } else {
            	cic.Type__c = 'Question';
            }
            //End HRM-10967
            cic.UniqueQuestionId__c = webIntObj.uniqueId;
            cic.UniqueVisitorNumber__c = webIntObj.uniqueVisitorId;
            cic.Email__c = webIntObj.email;
            cic.QuestionLookup__c = indenCuswebid;
            //Start HRM-10967
            /*if(cic.Type__c == 'Email'){
            	CustomerInterestConfig__c val = getExistingRecord(cic.UniqueVisitorNumber__c, cic.FriendlyId__c, 'Email Details');
            	if(val != null){
            		ADTCheetahMailSchema.sendCheetahEmail(val, cic.Email__c);
            	}
            } else*/ 
            if(cic.Type__c == 'Email Details'){
            	//CustomerInterestConfig__c val = getExistingRecord(cic.UniqueVisitorNumber__c, cic.FriendlyId__c, 'Email');
            	//if(val != null){
            	ADTCheetahMailSchema.sendCheetahEmail(cic);
            	//}
            } else{
            	insert cic;
            }
            //End HRM-10967
        }
       
    }
    //Start HRM-10967
    /*public static CustomerInterestConfig__c getExistingRecord(String uuid, String friendlyId, String typeVal){
    	list<CustomerInterestConfig__c> cuslist = [SELECT id, UniqueQuestionId__c, Response__c, Email__c, FriendlyId__c 
    												FROM CustomerInterestConfig__c 
                                                   WHERE UniqueVisitorNumber__c =:uuid
                                                   AND FriendlyId__c =:friendlyId
                                                   AND Type__c =:typeVal];
        if(!cuslist.isEmpty()){
        	return cuslist.get(0);
        }   
        return null;                                        
    }*/
    
    public static map<String, String> getInterestTypes(){
    	map<String, String> mappings = new map<String, String>();
		for (Interest_Type__mdt mapping : [SELECT MasterLabel, Type__c FROM Interest_Type__mdt]) {
			mappings.put(mapping.MasterLabel, mapping.Type__c);
		}
		return mappings;
    }
    //End HRM-10967
    public static String errorResponse(String mesge, Integer errcode){
        SfdcError errmes = new SfdcError();
        errors er = new errors();
        er.errorCode = '-1';
        er.status = String.valueOf(errcode);
        er.errorMessage = mesge;
        list<errors> errlist = new list<errors>();
        errlist.add(er);
        errmes.errors = errlist;
        String errString = JSON.serialize(errmes, true);
        return errString;
    }
}