/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ButtonBarCtrlTest { 
    static void pageComponentsTest() {
        Account a;
        User u;
        User current = [select Id from User where Id = :UserInfo.getUserId()];
        system.runas(current) {        
          u = TestHelperClass.createSalesRepUser();
        }
        
        system.runas(u) {
            a = TestHelperClass.createAccountData();
            PageReference pRef = Page.AccountButtonBar;
            Test.setCurrentPage(pRef);
            
            Apexpages.StandardController con = new Apexpages.StandardController(a); 
            Apexpages.currentPage().getParameters().put('Id', a.Id);
            ButtonBarCtrl ctrlObj = new ButtonBarCtrl(con);
            Boolean b1 = ctrlObj.renderMMBSearchBtn;
            Boolean b2 = ctrlObj.renderNewQuoteBtn;
        }
    }
    
    static testMethod void testButtonBar(){
        // Test Data
        Equifax__c eq = new Equifax__c();
        eq.name = 'Order Types For Credit Check1';
        eq.value__c = 'N1;R1';
        insert eq;
        Equifax__c eq1 = new Equifax__c();
        eq1.name = 'Order Types For Credit Check - Renters';
        eq1.Value__c = 'N1;R1';
        insert eq1;
        Equifax__c eq2 = new Equifax__c();
        eq2.name = 'Days to allow additional check';
        eq2.Value__c = '90';
        insert eq2;
        
        //create postal code
        List<Postal_Codes__c> postalCodes = TestHelperClass.createPostalCodes();
        postalCodes[0].name='22102';
        update postalCodes[0];
        
         //create address for creating accounts
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '22102';
        addr.OriginalAddress__c = '8952 Brook Rd+McLean+VA+22102';
        insert addr;
        
        ResaleGlobalVariables__c globalVar1 = new ResaleGlobalVariables__c();
        globalVar1.Name = 'DataRecastDisableAccountTrigger';
        globalVar1.value__c = 'FALSE';
        insert globalVar1;
        ResaleGlobalVariables__c globalVar2 = new ResaleGlobalVariables__c();
        globalVar2.Name = 'IntegrationUser';
        globalVar2.value__c = 'Integration User';
        insert globalVar2;
        ResaleGlobalVariables__c globalVar3 = new ResaleGlobalVariables__c();
        globalVar3.Name = 'RIFAccountOwnerAlias';
        globalVar3.value__c = 'ADT RIF';
        insert globalVar3;
        ResaleGlobalVariables__c globalVar4 = new ResaleGlobalVariables__c();
        globalVar4.Name = 'DataConversion';
        globalVar4.value__c = 'FALSE';
        insert globalVar4;
        
        RecordType standardAcctRecType = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account');
        Account acc = new Account();
        acc.RecordTypeId = standardAcctRecType.Id;
        acc.Name = 'test';
        acc.Email__c='test1@test.com';
        acc.Business_Id__c = '1100 - Residential';
        acc.Channel__c = 'Resi Direct Sales';
        acc.AddressID__c = addr.Id;
        acc.MMBOrderType__c = 'N1';
        acc.HOA__c = true;
        insert acc;
        
        
        
        Apexpages.StandardController con = new Apexpages.StandardController(acc);
        ButtonBarCtrl ctrlObj = new ButtonBarCtrl(con);
        ctrlObj.callADTGo();
     //   ButtonBarCtrl.callMMBLookup(acc.id);
    }
    static testMethod void testDoCallOut() {
        // Test Data
        Equifax__c eq = new Equifax__c();
        eq.name = 'Order Types For Credit Check2';
        eq.value__c = 'N1;R1';
        insert eq;
        Equifax__c eq1 = new Equifax__c();
        eq1.name = 'Order Types For Credit Check - Renters';
        eq1.Value__c = 'N1;R1';
        insert eq1;
        Equifax__c eq2 = new Equifax__c();
        eq2.name = 'Days to allow additional check';
        eq2.Value__c = '90';
        insert eq2;
        
        //create postal code
        List<Postal_Codes__c> postalCodes = TestHelperClass.createPostalCodes();
        postalCodes[0].name='22102';
        update postalCodes[0];
        
         //create address for creating accounts
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '22102';
        addr.OriginalAddress__c = '8952 Brook Rd+McLean+VA+22102';
        insert addr;
        
        ResaleGlobalVariables__c globalVar1 = new ResaleGlobalVariables__c();
        globalVar1.Name = 'DataRecastDisableAccountTrigger';
        globalVar1.value__c = 'FALSE';
        insert globalVar1;
        ResaleGlobalVariables__c globalVar2 = new ResaleGlobalVariables__c();
        globalVar2.Name = 'IntegrationUser';
        globalVar2.value__c = 'Integration User';
        insert globalVar2;
        ResaleGlobalVariables__c globalVar3 = new ResaleGlobalVariables__c();
        globalVar3.Name = 'RIFAccountOwnerAlias';
        globalVar3.value__c = 'ADT RIF';
        insert globalVar3;
        ResaleGlobalVariables__c globalVar4 = new ResaleGlobalVariables__c();
        globalVar4.Name = 'DataConversion';
        globalVar4.value__c = 'FALSE';
        insert globalVar4;
        
        RecordType standardAcctRecType = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account');
        Account acc = new Account();
        acc.RecordTypeId = standardAcctRecType.Id;
        acc.Name = 'test';
        acc.Email__c='test1@test.com';
        acc.Business_Id__c = '1100 - Residential';
        acc.Channel__c = 'Resi Direct Sales';
        acc.AddressID__c = addr.Id;
        acc.MMBOrderType__c = 'N1';
        acc.HOA__c = true;
        insert acc;
        
        IntegrationSettings__c objIntegrationSettings = new IntegrationSettings__c();
        objIntegrationSettings.SetupOwnerId = UserInfo.getOrganizationId();
        objIntegrationSettings.GoToADT_Endpoint__c = 'test.link.com';
        objIntegrationSettings.GoToADT_Username__c = 'test';
        objIntegrationSettings.GoToADT_Password__c = 'password';
        insert objIntegrationSettings;
        
     //   ButtonBarCtrl.callMMBLookup(acc.id);
        
        GoToADTControllerExtension.PostInviteItemRequestWrapper objWrapper = new GoToADTControllerExtension.PostInviteItemRequestWrapper();
        
        GoToADTControllerExtension.PostInviteItemWrapper objWrapper1 = new GoToADTControllerExtension.PostInviteItemWrapper();
        GoToADTControllerExtension.PostInviteItemWrapper objWrapper2 = new GoToADTControllerExtension.PostInviteItemWrapper('test@test.com', 432237, 'adtgo', 'p', 'YOURAPP');
        
        Test.setMock(HttpCalloutMock.class, new ADTPartnerSetDLLCalloutMock());
        GoToADTControllerExtension objGoToADTControllerExtension = new GoToADTControllerExtension();
        objGoToADTControllerExtension.doCallout('{}', acc.Id);
    }
}