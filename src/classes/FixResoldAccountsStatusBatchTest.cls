/*
 * Description: This is a test class for the following classes
                1. FixResoldAccountsStatusBatch
 * Created By: Shiva Pochamalla
 *
 * ---------------------------------------------------------------------------Changes below--------------------------------------------------------------------------------
 * 
 *
 */


@isTest
private class FixResoldAccountsStatusBatchTest{
    public static PostalCodeReassignmentQueue__c pcrq;
    public static void createTestData(){
        
        TestHelperClass.createReferenceDataForTestClasses();
        
        Profile p = [select id from profile where name='System Administrator'];
        integer i= 47598347;
        User integrationUser = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='User',firstname='TestIntegration', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
            StartDateInCurrentJob__c = Date.today());
        insert integrationUser;
        
        
        
        
        //create postal code
        List<Postal_Codes__c> postalCodes= TestHelperClass.createPostalCodes();
        
        //create address for creating accounts
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
        insert addr;
        
        
        //create accounts
        
        Account acct=TestHelperClass.createAccountData(addr,postalCodes[0].id,false);
        acct.channel__c='Resi Resale';
        acct.dispositioncode__c='RS - Re-Sold';
        acct.LeadStatus__c ='Active';
        insert acct;
    }
    
    
    static testMethod void TestFixResoldAccountsStatusBatch() {        
        createTestData();
        test.startTest();
            FixResoldAccountsStatusBatch frasb=new FixResoldAccountsStatusBatch();
            database.executebatch(frasb);
        test.stopTest();
        
    }
}