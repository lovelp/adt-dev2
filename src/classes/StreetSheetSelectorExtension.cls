/************************************* MODIFICATION LOG ********************************************************************************************
* StreetSheetSelectorExtension
*
* DESCRIPTION : Extension for adding accounts and leads to street sheet pages
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover                  4/25/2012               - Original Version
*
*                                                   
*/

public with sharing class StreetSheetSelectorExtension {

    public List<String> itemIds {get;set;}
    
    public StreetSheetSelectorExtension(ApexPages.Standardsetcontroller stdcon) {
        itemIds = new List<String>();
        List<SObject> items = stdcon.getSelected();
        for (Sobject s : items) {
            itemIds.add(s.Id);
        }
    }
    
    public StreetSheetSelectorExtension(ApexPages.StandardController stdcon) {
        
        String itemID = System.currentPageReference().getParameters().get('iid');
        if (itemID != null && itemID.length() > 0) {
            itemIds = new List<String>();
            itemIds.add(itemID);
        }
        
    }
    
    public PageReference validate() {
        if (itemIds == null || itemIds.size() == 0) {
            return new PageReference('/apex/ShowError?Error=error.streetsheet.noitemsselected');
        }
        list<StreetSheet__c> sslist = new list<StreetSheet__c>([Select Id from StreetSheet__c where OwnerId = :UserInfo.getUserId() limit 1]);
        if (sslist.size() == 0) {
            return new PageReference('/apex/ShowError?Error=error.streetsheet.noneowned');
        }
        return null;
    }
}