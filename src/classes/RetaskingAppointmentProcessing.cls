public class RetaskingAppointmentProcessing{
    
    public String accID;//Set it from another class.
    public Account customerAccount;//Account details 
    public Address__c custAccountAddress;//Address Details
    public GoogleMapsTimeZone.GoogleTZResponse googleTZResp; //Response from google api for timezones
    public TimeZone CustomerTimeZone;//Customer timezone
    public class GenericException extends Exception{}//For creating a generic exception.
    public Date requestSchDate;//Set it from outside
    private final String BUSINESSID_RESIDENTIAL = '1100';
    public Map<Id, String> repAndTimeZone;
    public string retaskDuration;
    public string fieldAppType;
    public Id eventId;
    public class AvailableSalesRep{
        public Id repId;
        public String repName;
        public SchedUserTerr__c scheUserTerrRow;
        public Time[] startEndTimes;//First index will have reps start time and second his end time
        public list<existingEvents> exisitngCalEvents;
        public Date requestAppntDate;
    }
    
    public class ExistingEvents{
        public Time[] existStartEnd;//Existing appointment start and end time 
        public integer durationinMin;
        public integer travelTimeBeforeAfter; // travel time to and from exisiting event to new event in mins
        public id EventId;
    }
    
    public set<Id> repsFromScheTerr;
    
    /*************************************************************************************************
    * Desription: Method to intialize all required details. Account details, address details and 
    *             Customer address timezone details.
    *
    **************************************************************************************************/
    public void initSalesUserAvailability(){
        try{
            // Read additional information on the account
            Map<String, String> TimeZoneSidKeyValue = new Map<String, String>();
            String CustomerTimeZoneLabel;
            customerAccount = new Account();
            //Read account details
            customerAccount = [SELECT Id, Name, AddressID__c, MMBOrderType__c, TelemarAccountNumber__c, SalesAppointmentDate__c, FirstName__c, 
                                LastName__c, SiteStreet__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c, SiteCountryCode__c, 
                                Phone, Email__c, DispositionComments__c, ReferredBy__c, Channel__c, Business_ID__c, ProcessingType__c, 
                                TaskCode__c, TransitionDate2__c, LeadExternalID__c, Data_Source__c, Type, AccountStatus__c, OutboundTelemarAccountNumber__c, 
                                Affiliation__c, TMTownID__c, TMSubTownID__c FROM ACCOUNT WHERE id = :accID];
            // Read this account's address including the geocoding info
            custAccountAddress = [SELECT OriginalAddress__c, Latitude__c, Longitude__c, City__c, County__c, PostalCode__c, PostalCodeAddOn__c, StandardizedAddress__c, 
                                State__c, Street__c, StreetNumber__c, Street2__c, StreetName__c, Validated__c, ValidationMsg__c FROM Address__c WHERE Id = :customerAccount.AddressID__c];
            //If customer Account or Address does not exist throw exception.
            if(customerAccount == null && custAccountAddress == null){
                throw new GenericException('Problem querying Account or Address');
            }
            if(customerAccount.Channel__c == Channels.TYPE_CUSTOMHOMESALES){
                fieldAppType = 'Custom Home';
            }else{
                Map<String, SchedDefaults__c> SchedDefaults = SchedDefaults__c.getAll();
                for( String f : SchedDefaults.keyset()) {
                    SchedDefaults__c defObj = SchedDefaults.get(f);
                    if(defObj != null && String.isNotBlank(defObj.Code__c)){
                        for( String c: defObj.Code__c.split(',')){
                            if(customerAccount.MMBOrderType__c == c){
                                fieldAppType = f;
                                break;
                            }
                        }
                    }
                }
            }
            // init timezone mapping between Sidkey and label used on the user object
            if(test.isRunningTest()){
                GoogleMapsTimeZone.GoogleTZResponse tzResp = new GoogleMapsTimeZone.GoogleTZResponse();
                tzResp.dstOffset = 0;
                tzResp.rawOffset = -18000;
                tzResp.timeZoneId = 'America/New_York';
                tzResp.timeZoneName = 'Eastern Standard Time';
                tzResp.error_message = 'error';
                googleTZResp=tzResp ;
            }else{
                googleTZResp = GoogleMapsTimeZone.getAddressTimeZone(custAccountAddress);
                system.debug('##$$%%'+googleTZResp);
            }
            Schema.DescribeFieldResult fieldResult = User.TimeZoneSidKey.getdescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for(Schema.PicklistEntry p : ple){
                TimeZoneSidKeyValue.put(p.getValue(), p.getlabel());
            }
            CustomerTimeZone = TimeZone.getTimeZone(googleTZResp.timeZoneId);
        }
        catch(Exception err){}
    } 
    
    /**********************************************************************************************************
     *  Description: Core method which is invoked from another controller to give all repdetails
     * 
     * *******************************************************************************************************/
    
    public list<AvailableSalesRep> getAllEligibleReps(){
        initSalesUserAvailability();
        Account acnt = customerAccount;
        String CurrentMonth = requestSchDate.Month()+'/'+requestSchDate.Year();
        // HRM-10450 Biz Id Change
        String bId = Channels.getFormatedBusinessId(acnt.Business_Id__c, Channels.BIZID_OUTPUT.NUM);
        if(String.isBlank(bId)){
            bId = BUSINESSID_RESIDENTIAL;
        }
        //Query schedule user territories
        String schSOQLQuery = 'SELECT Name, User__c, User__r.Id, User__r.Username, User__r.Name, User__r.TimeZoneSidKey, User__r.IsActive, Manager_Notes__c, Default_Starting_Address__c,Territory_Association_ID__c,'+ 
                              '                Default_Starting_Address__r.StandardizedAddress__c, Sunday_Available_Start__c, Sunday_Available_End__c, Monday_Available_Start__c, Monday_Available_End__c, Quota__c,'+
                              '                Tuesday_Available_Start__c, Wednesday_Available_Start__c, Wednesday_Available_End__c, Tuesday_Available_End__c, Thursday_Available_Start__c, Thursday_Available_End__c,'+ 
                              '                Friday_Available_Start__c, Friday_Available_End__c, Saturday_Available_Start__c, Saturday_Available_End__c, Manager_Weighting__c, Order_Types__c, Required_Appts__c,'+
                              '                (SELECT Id, OwnerId, Name, CreatedDate, User__c, Territory__c, Sub_Territory__c, Order_Appointment_Type__c, Period_Month__c, Period_Year__c, Period__c, SchedUserTerr__c,' +
                              '                        Quota_Appointments_Set__c, Quota_Appointments_Cancelled__c, Quota_Appointments__c, Additional_Appointments_Set__c, Additional_Appointments_Cancelled__c,'+
                              '                        Additional_Appointments__c, Last_Appointment_Set_Date_Time__c, Last_Appointment_Date_Time__c'+
                              '                 FROM User_Appointment_Counts__r where Period__c=\''+CurrentMonth+'\' )'+
                              '         FROM SchedUserTerr__c ';
        schSOQLQuery += ' WHERE TMTownID__c = \''+acnt.TMTownID__c+'\' AND TMSubTownID__c = \''+(String.isBlank(acnt.TMSubTownID__c)?'':acnt.TMSubTownID__c)+'\' AND BusinessId__c = \''+bId+'\' AND User__r.IsActive = true AND User__r.Profile_Name__c LIKE \'%NA%\'';        
        system.debug( 'Query ====> ' + schSOQLQuery );
        list<AvailableSalesRep> availSaleReplist = new list<AvailableSalesRep>();
        repsFromScheTerr = new set<Id>();
        repAndTimeZone = new Map<Id, String>();
        //Create a initla wrapper with repid, name, start and end availability for that day
        for(SchedUserTerr__c schObj : (List<SchedUserTerr__c>)Database.Query(schSOQLQuery)){
            if((String.isBlank(fieldAppType) || utilities.IsEmptyOrNull(schObj.Order_Types__c) || ( !utilities.IsEmptyOrNull(schObj.Order_Types__c) && !schObj.Order_Types__c.contains(fieldAppType)))){
                    continue;
            }
            Time[] tempStartEndTimes = indentifyDayReps(schObj);
            if(tempStartEndTimes != null){
                repsFromScheTerr.add(schObj.User__r.Id);
                repAndTimeZone.put(schObj.User__r.Id,schObj.User__r.TimeZoneSidKey);//Needed for setting timezone of rep to customer
                system.debug('TimezoneMap'+repAndTimeZone);
                AvailableSalesRep avaSalesRep = new AvailableSalesRep();
                avaSalesRep.repId = schObj.User__r.Id;
                avaSalesRep.repName = schObj.User__r.Name;
                avaSalesRep.requestAppntDate = requestSchDate;
                avaSalesRep.scheUserTerrRow = schObj;
                avaSalesRep.startEndTimes = convertoCustTimeZone(tempStartEndTimes,schObj.User__r.TimeZoneSidKey);//indentifyDayReps(schObj);
                availSaleReplist.add(avaSalesRep);
            }
        }
        
        Map<Id, list<ExistingEvents>> exisEventsRelatedtoRep = new Map<Id, list<ExistingEvents>>();
        //This below method will return wrapper of salesreps with their exisiting appointements.
        exisEventsRelatedtoRep = currentEventsForRep();
        system.debug('@##'+exisEventsRelatedtoRep);
        list<AvailableSalesRep> availSaleReplistFinal = new list<AvailableSalesRep>();
        for(AvailableSalesRep asr:availSaleReplist){
            asr.exisitngCalEvents = exisEventsRelatedtoRep.get(asr.repId);
            availSaleReplistFinal.add(asr);
        }
        system.debug('@$$##Final All details'+availSaleReplistFinal);
        return availSaleReplistFinal;
    }
    

    public Map<Id, list<ExistingEvents>> currentEventsForRep(){
        list<Event> currentAppForSchDate = new list<Event>();
        list<ExistingEvents> exisEvenlist = new list<ExistingEvents>();
        Map<Id, list<ExistingEvents>> repEventsforSchDt = new Map<Id, list<ExistingEvents>>();
        Map<id, id> dummyUserEvent = new Map<id, id>();
        String startOfTheday = DateTime.newInstanceGMT(requestSchDate.year(),requestSchDate.Month(),requestSchDate.day(),0,0,0).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        String endOfTheDate = DateTime.newInstanceGMT(requestSchDate.year(),requestSchDate.Month(),requestSchDate.day(),23,59,59).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        system.debug('##1'+startOfTheday+endOfTheDate);
        String queryString = 'SELECT id, StartDateTime, EndDateTime, whoId, whatId, OwnerId FROM Event WHERE '+'OwnerId IN:repsFromScheTerr AND StartDateTime >'+startOfTheday+' AND EndDateTime < '+endOfTheDate;
        //Query appointments for that day for selected reps.
        currentAppForSchDate = Database.query(queryString);
        system.debug('##1.2'+currentAppForSchDate);
        //Calculate travel distances
        Map<Id, Integer> eventDistanceMap=calculateDistanceToTravel(currentAppForSchDate);
        ExistingEvents exisEve = new existingEvents();
        for(Event eve:currentAppForSchDate){
            //for current event do not show as busy
            if(eve.Id == eventId){
                continue;
            }
            Time[] startEndDtTime = new List<Time> { null, null };
            startEndDtTime[0] = eve.StartDateTime.Time();
            startEndDtTime[1] = eve.EndDateTime.Time();
            exisEve.existStartEnd = convertoCustTimeZone(startEndDtTime,repAndTimeZone.get(eve.OwnerId));
            system.debug('existing'+repAndTimeZone.get(eve.OwnerId)+eve.OwnerId);
            exisEve.durationinMin = Integer.valueOf((eve.EndDateTime.getTime() - eve.StartDateTime.getTime())/60000);//startEndDtTime[1]-startEndDtTime[0];
            exisEve.travelTimeBeforeAfter = eventDistanceMap !=null && eventDistanceMap.get(eve.Id)!=null? eventDistanceMap.get(eve.Id):30;//Default for now. Change later
            exisEve.EventId = eve.Id;
            if(!repEventsforSchDt.keyset().contains(eve.OwnerId)){
                exisEvenlist.clear();
                exisEvenlist.add(exisEve);
                system.debug('3##'+exisEvenlist);
                repEventsforSchDt.put(eve.OwnerId,exisEvenlist);
            }else{
                exisEvenlist.clear();
                exisEvenlist = repEventsforSchDt.get(eve.OwnerId);
                exisEvenlist.add(exisEve);
                repEventsforSchDt.put(eve.OwnerId,exisEvenlist);
            }
        }
        return repEventsforSchDt;
    }
    
    public Time[] indentifyDayReps(SchedUserTerr__c objRepSch){
        // Find day of the week for this date component
        Date dtWeekStartDay = requestSchDate.toStartOfWeek();
        Integer DayOfWeek = dtWeekStartDay.daysBetween(requestSchDate);
        Time[] resVal = new List<Time> { null, null };
        Integer AvailableStartTime = 0;
        Integer AvailableEndTime = 0;
        if(DayOfWeek == 0 && objRepSch.Sunday_Available_Start__c != null && objRepSch.Sunday_Available_End__c != null){ // Sunday
            AvailableStartTime = objRepSch.Sunday_Available_Start__c.intValue();
            AvailableEndTime = objRepSch.Sunday_Available_End__c.intValue();
        }else if(DayOfWeek == 1  && objRepSch.Monday_Available_Start__c != null && objRepSch.Monday_Available_End__c != null ){ // Monday
            AvailableStartTime = objRepSch.Monday_Available_Start__c.intValue();
            AvailableEndTime = objRepSch.Monday_Available_End__c.intValue();
        }else if(DayOfWeek == 2  && objRepSch.Tuesday_Available_Start__c != null && objRepSch.Tuesday_Available_End__c != null){ // Tuesday
            AvailableStartTime = objRepSch.Tuesday_Available_Start__c.intValue();
            AvailableEndTime = objRepSch.Tuesday_Available_End__c.intValue();
        }else if(DayOfWeek == 3 && objRepSch.Wednesday_Available_Start__c != null && objRepSch.Wednesday_Available_End__c != null){ // Wednesday
            AvailableStartTime = objRepSch.Wednesday_Available_Start__c.intValue();
            AvailableEndTime = objRepSch.Wednesday_Available_End__c.intValue();
        }else if(DayOfWeek == 4  && objRepSch.Thursday_Available_Start__c != null && objRepSch.Tuesday_Available_End__c != null ){ // Thursday
            AvailableStartTime = objRepSch.Thursday_Available_Start__c.intValue();
            AvailableEndTime = objRepSch.Thursday_Available_End__c.intValue();
        }else if(DayOfWeek == 5 && objRepSch.Friday_Available_Start__c != null && objRepSch.Friday_Available_End__c != null ){ // Friday
            AvailableStartTime = objRepSch.Friday_Available_Start__c.intValue();
            AvailableEndTime = objRepSch.Friday_Available_End__c.intValue();
        }else if(DayOfWeek == 6 && objRepSch.Saturday_Available_Start__c != null && objRepSch.Saturday_Available_End__c != null ){ // Saturday
            AvailableStartTime = objRepSch.Saturday_Available_Start__c.intValue();
            AvailableEndTime = objRepSch.Saturday_Available_End__c.intValue();
        }
        if(AvailableStartTime == 0 || AvailableEndTime == 0){
            return null;
        }
        
        Integer AvailableStartTimeHr = ((decimal)(AvailableStartTime)/100).intValue();
        Integer AvailableStartTimeMin = (((decimal)(AvailableStartTime)/100 - AvailableStartTimeHr) * 100).intValue();
        Integer AvailableEndTimeHr = ((decimal)(AvailableEndTime)/100).intValue();
        Integer AvailableEndTimeMin = (((decimal)(AvailableEndTime)/100 - AvailableEndTimeHr) * 100).intValue();
        resVal[0] = Time.newInstance(AvailableStartTimeHr, AvailableStartTimeMin, 0, 0);
        resVal[1] = Time.newInstance(AvailableEndTimeHr, AvailableEndTimeMin, 0, 0);//.addHours(-1*FieldAppTypeDuration); //Remove last 2 hours from end time
        return resVal;
        
    }
    
    public Time[] convertoCustTimeZone(Time[] repsDateTimes, String repsTimeZone){
        system.debug('##Check1-repTZ: '+repsTimeZone+'CustTZ: '+googleTZResp.timeZoneId);
        //integer customeOffsert = googleTZResp.dstOffset + googleTZResp
        if(String.isNotBlank(repsTimeZone) && repsTimeZone != googleTZResp.timeZoneId){
            Timezone repTz = Timezone.getTimeZone(repsTimeZone);
            Timezone custTz = Timezone.getTimeZone(googleTZResp.timeZoneId);
            DateTime currentDateTime = Datetime.newInstance(requestSchDate,Time.newInstance(0,0,0,0));
            integer timeDiff = custTz.getOffset(currentDateTime)-repTz.getOffset(currentDateTime);
            repsDateTimes[0] = repsDateTimes[0].addMinutes(timeDiff/(60*1000));
            repsDateTimes[1] = repsDateTimes[1].addMinutes(timeDiff/(60*1000));
            system.debug('Offset - Rep'+repTz.getOffset(currentDateTime)+'Offset - Cust'+custTz.getOffset(currentDateTime));
            system.debug('##Check2-repTZ: '+repsTimeZone+'CustTZ: '+googleTZResp.timeZoneId+'repTZId: '+repTz+'custTzId: '+custTz+'TimeDiff in mins: '+timeDiff/(60*1000)+'ConvertedTimes: '+repsDateTimes);
        }
        return repsDateTimes;
    }
    
    //Return map of eventId and travel distance from new customer to that customer appointment.
    public Map<Id, Integer> calculateDistanceToTravel(list<Event> existEvents){
        try{
            Map<Id, Id> eveAccntId = new Map<Id,Id>();
            for(Event eve:existEvents){
                eveAccntId.put(eve.Id, eve.whatId);
            }
            Map<Id, Id> accntAddrMap = new Map<Id, Id>();
            for(Account acnt:[SELECT Id, AddressID__c FROM Account WHERE Id IN:eveAccntId.values()]){
                accntAddrMap.put(acnt.Id, acnt.AddressID__c);
            }
            Integer timeToTravel=0;
            list<Address__c> reqAccountAddr = [SELECT id, Longitude__c,StandardizedAddress__c, Latitude__c FROM Address__c WHERE Id IN:accntAddrMap.values()];
            system.debug('##Destin'+reqAccountAddr);
            GoogleDistanceMatrix.setDistanceMatrixDestinations(reqAccountAddr);
            //custAccountAddress = [SELECT id, Longitude__c,StandardizedAddress__c, Latitude__c FROM Address__c limit 1];
            system.debug('##source'+custAccountAddress);
            GoogleDistanceMatrix.setDistanceMatrixOrigins(new list<Address__c>{custAccountAddress});
            // Read travel information for each Origin-Destination places
            Map<Id, integer> eveDistance = new Map<Id, Integer>();
            Map<String, Integer> addressDistanceMap = new Map<String, Integer>();
            if(GoogleDistanceMatrix.isDistanceMatrixAvailable()) {
                System.debug('### google distance matrix is available');
                for(GoogleDistanceMatrix.DistanceMatrixResult dRes: GoogleDistanceMatrix.getDistanceMatrix_retask()){
                    system.debug('Origin'+dRes.origin);
                    system.debug('Destination'+dRes.destination);
                    system.debug('TravelInformation'+dRes.TravelInformation);
                    addressDistanceMap.put(dRes.destination.plUID,Math.round(dRes.TravelInformation.duration.value/60));
                    system.debug('##'+addressDistanceMap);
                }
                if(addressDistanceMap.keyset() != null){
                    for(Event eve:existEvents){
                        if(accntAddrMap.get(eve.WhatId) !=null){
                            timeToTravel=0;
                            timeToTravel = addressDistanceMap.get(accntAddrMap.get(eve.WhatId));
                            timeToTravel = timeToTravel+15-(Math.mod(timeToTravel,15));
                            eveDistance.put(eve.Id, timeToTravel);
                            system.debug(eveDistance);
                        }
                    }
                    return eveDistance;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }catch(Exception ae){
            return null;
        }
    }
}