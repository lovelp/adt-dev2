/************************************* MODIFICATION LOG ********************************************************************************************
* GenProspectListCtrl
*
* DESCRIPTION : Controller for prospect list generation page
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                    REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera               5/6/2014               - Original Version
*
*/

global class GenProspectListCtrl {
	
	public String 	listType 				{ get; set; }
	public Lead 	leadFilter 				{ get; set; } 
	public String 	meridianSchedule 		{ get; set; }
	public String 	maxProspectSizeStr 		{ get; set; }
	public Boolean 	renderTargetDateFilter 	{ get; set; }
	public Boolean 	renderMaxResultSize 	{ get; set; }
	public User		rep						{ get; set; }
	public Boolean  useZipCode				{ get; set; }
	public String   ZipCodeStr				{ get; set; }
	public String 	actionDisabled			{ get; set; }
    public String   mCurrentUserProfile 	{ get; set; }
    public Boolean  includePhoneNo			{ get; set; }
	
    public Boolean isManager {
    	get{
    		return ProfileHelper.isManager(mCurrentUserProfile); 
    	}
    }
	
	/**
	 *	@Constructor
	 */
	public GenProspectListCtrl(){
		DateTime futureScheduleStart 	= DateTime.now().addDays(1);
		leadFilter 						= new lead(EstimatedMailReceiveDate__c = Date.today().addDays(1));
		maxProspectSizeStr 				= '10';
		meridianSchedule 				= 'AM';
		renderTargetDateFilter 			= false;
		renderMaxResultSize 			= false;
		useZipCode						= false;
		ZipCodeStr 						= '';
		actionDisabled 					= 'disabled';
		includePhoneNo					= true;
		rep 		= [SELECT Id, Name, Business_Unit__c, Qualification__c, ManagerId, UserRoleId, TownNumber__c, ProfileId FROM User WHERE Id = :userInfo.getUserId() ];
		Profile prf = [SELECT Id, Name FROM Profile WHERE Id=:rep.ProfileId];
		mCurrentUserProfile = prf.Name; 	    
	}
	
	/**
	 *	Action called from the page to run logic for prospect list
	 *	@return PageReference	Returns a reference to the prospect list management interface in case it was created and null otherwise
	 */	
	public PageReference GenerateProspects(){
		
		String rLat = Apexpages.currentPage().getParameters().get('rLat');
		String rLon = Apexpages.currentPage().getParameters().get('rLon');
		
		try{

			if( !Utilities.isEmptyOrNull(listType) ){
				
				if(listType=='Field'){
					
					Set<String> ProspectIDs = new Set<String>();
					FinancialHierarchyProximitySearcher pSearchObj = (FinancialHierarchyProximitySearcher)ProximitySearcherFactory.getSearcher(true, false);					
					Set<String> pendingProspectIds = nonWorkedProspects();
					
					if(!useZipCode){
	
						Set<String> accsSalesAppt = new Set<String>();  
						Set<String> accsInstallAppt = new Set<String>();						

						if(pendingProspectIds!= null && !pendingProspectIds.isEmpty()){
			            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'We found prospects which are still assigned to a previous list. Please complete any pending dispositions. '));
						}
						
						String dynamicEventSearchStr = ' SELECT Id, What.Id, OwnerId, What.Type, StartDateTime, RecordType.Name '
													  +' FROM Event '
													  +' WHERE OwnerId = \''+UserInfo.getUserId()+'\''
													  +'		 	AND What.Type IN (\'Account\') '
													  +'		 	AND CALENDAR_YEAR(convertTimezone(StartDateTime)) = '+leadFilter.EstimatedMailReceiveDate__c.year() 
													  +'			AND CALENDAR_MONTH(convertTimezone(StartDateTime)) =  '+leadFilter.EstimatedMailReceiveDate__c.month()
													  +'			AND DAY_IN_MONTH(convertTimezone(StartDateTime)) = '+leadFilter.EstimatedMailReceiveDate__c.day()
													  +'			[[MERIDIAN_RESTRICTION]] '
													  +' limit 1';
						// Search for appointments
						if(meridianSchedule == 'AM'){
							dynamicEventSearchStr = dynamicEventSearchStr.replace('[[MERIDIAN_RESTRICTION]]', 'AND HOUR_IN_DAY(convertTimezone(StartDateTime)) > 1 AND HOUR_IN_DAY(convertTimezone(StartDateTime)) <= 12'); 
						}
						else if(meridianSchedule == 'PM'){
							dynamicEventSearchStr = dynamicEventSearchStr.replace('[[MERIDIAN_RESTRICTION]]', 'AND HOUR_IN_DAY(convertTimezone(StartDateTime)) > 12 AND HOUR_IN_DAY(convertTimezone(StartDateTime)) <= 24'); 
						}
						
						for(Event e :Database.query( dynamicEventSearchStr ) ){
							//Install Appointment - Once we get an install apointment we'll be always dealing with an account				
							if(e.RecordType.Name.equalsIgnoreCase(RecordTypeName.INSTALL_APPOINTMENT)){
								accsInstallAppt.add(e.What.Id);
							}
							//Field Sales Appointment
							else if(e.RecordType.Name.equalsIgnoreCase(RecordTypeName.SELF_GENERATED_APPOINTMENT)){
								accsSalesAppt.add(e.What.Id);
							}
						}
		
						
						// find prospects near sales appointment				
						if(accsSalesAppt!=null && !accsSalesAppt.isEmpty()){
							getProspectIdsFromAccount(accsSalesAppt, pSearchObj, ProspectIDs, maxProspectSizeStr, pendingProspectIds);
						}
						
						// find prospects near install appointment - NO PROSPECTS FOUND FOR ANY SALES APPT
						if(accsSalesAppt==null || accsSalesAppt.isEmpty() || ProspectIDs == null || ProspectIDs.isEmpty()){
							
							// Make sure we still have some room to keep adding prospects
							Integer maxProspectSize = Integer.valueOf(maxProspectSizeStr);
							if(ProspectIDs!= null && !ProspectIDs.isEmpty()){
								maxProspectSize -= ProspectIDs.size(); 
							}
							
							// Go and get more prospects if still we have some room from the initial request	
							if(maxProspectSize > 0)
								getProspectIdsFromAccount(accsInstallAppt, pSearchObj, ProspectIDs, String.valueOf(maxProspectSize), pendingProspectIds);					
						}
	
					}
					else{
						if(!Utilities.isEmptyOrNull(rLat) && !Utilities.isEmptyOrNull(rLon)){
							Address__c geCodeAcc = new Address__c(Latitude__c = Decimal.valueOf(rLat) ,Longitude__c = Decimal.valueOf(rLon) );
							getProspectIdsFromAddr(geCodeAcc, pSearchObj, ProspectIDs, maxProspectSizeStr, pendingProspectIds);
						}
						else{
		            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Sorry, we did not received your custom address. Please try submitting again.'));
						}
					}
					
					// generate prospects from any account/leads found near the appointment
					if(ProspectIDs!=null && !ProspectIDs.isEmpty()){
						try{
							processProspectsOwnership(ProspectIDs);							
							String prospectListID = StreetSheetManager.create('FIELD - '+prospectListDefaultName(rep.Name), ProspectIDs);
							return new PageReference('/apex/ManageProspectList?id='+prospectListID+'&sfdc.override=1');
						}
						catch(Exception err){
		            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Unexpected error while creating your list. ' + err.getMessage()+' '+err.getStackTraceString() ));
						}
					}
					// Use zipCode
					else if(!useZipCode){
		            	noProspectsFound();
						useZipCode = true;
						return null;
					}
					else{
						noProspectsFound();
						return null;
					}
				}
				
				// Generating a Phone list
				else if(listType=='Phone'){
					
					Set<String> leadIds 			= new Set<String>();
					Set<String> accIds 				= new Set<String>();
					Set<String> ProspectIDs 		= new Set<String>();
					Set<String> pendingProspectIds 	= nonWorkedProspects(); 
					Integer maxProspectSize 		= Integer.valueOf(maxProspectSizeStr);
					Date newMoverDateCap 			= Date.today().addDays(90);
					 
					Map<String, StreetSheetItem__c> scoredItemObjMap = new Map<String, StreetSheetItem__c>(); 
					
					if(pendingProspectIds!= null && !pendingProspectIds.isEmpty()){
		            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'We found prospects which are still assigned to a previous list. Please complete any pending dispositions. '));
					}
					
					// Resi Resale rep
					if(rep.Business_Unit__c == 'Resi Resale'){
						
						// Condition #1
						Map<String, Account> discoAccProspects = new Map<String, Account>([ SELECT Id, Type FROM Account WHERE OwnerID = :rep.Id AND Type = 'Discontinuance' AND LeadStatus__c = 'Active' AND DisconnectDate__c >= LAST_N_DAYS:90 AND DispositionCode__c = '' AND Phone <> null AND Phone <> 'DO NOT CALL' AND Id NOT IN :pendingProspectIds limit :maxProspectSize]);						
						if(discoAccProspects !=null && !discoAccProspects.isEmpty()){
							maxProspectSize -= discoAccProspects.size();
							for(String s: discoAccProspects.keySet()){
								scoredItemObjMap.put(s, new StreetSheetItem__c( Score__c = 1));
							}
						}
						
						// Condition #2
						if(maxProspectSize>0){
							Map<String, Account> resaleNewMoverAccProspects = new Map<String, Account>([ SELECT Id, Type FROM Account WHERE OwnerID = :rep.Id AND LeadStatus__c = 'Active' AND NewMoverDate__c >= LAST_N_DAYS:90 AND DispositionCode__c = '' AND Type = 'New Mover' AND Id NOT IN :accIds AND Phone <> null AND Phone <> 'DO NOT CALL' AND Id NOT IN :pendingProspectIds limit :maxProspectSize ]);
							if(resaleNewMoverAccProspects!=null && !resaleNewMoverAccProspects.isEmpty()){
								maxProspectSize -= resaleNewMoverAccProspects.size();
								for(String s: resaleNewMoverAccProspects.keySet()){
									scoredItemObjMap.put(s, new StreetSheetItem__c( Score__c = 2));
								}
							}
						}
						
						// Condition #3
						if(maxProspectSize>0){
							Date n90 = Date.today().addDays(-90);
							Map<String, Account> resaleNewMover90AccProspects = new Map<String, Account>([ SELECT Id, Type FROM Account WHERE OwnerID = :rep.Id AND LeadStatus__c = 'Active' AND NewMoverDate__c < :n90 AND DispositionCode__c = '' AND Type = 'New Mover' AND Id NOT IN :accIds AND Phone <> null AND Phone <> 'DO NOT CALL' AND Id NOT IN :pendingProspectIds limit :maxProspectSize ]);
							if(resaleNewMover90AccProspects!=null && !resaleNewMover90AccProspects.isEmpty()){
								maxProspectSize -= resaleNewMover90AccProspects.size();
								for(String s: resaleNewMover90AccProspects.keySet()){
									scoredItemObjMap.put(s, new StreetSheetItem__c( Score__c = 3));
								}
							}
						}
					}
					
					// New Sales rep
					else{
						
						// Condition #1
						Map<String, Lead> preMoverLeadProspects = new Map<String, Lead>([SELECT Id, Type__c FROM Lead WHERE OwnerID = :rep.Id AND isConverted = FALSE AND Type__c = 'Pre Mover' AND DispositionDetail__c = '' AND Phone <> null AND Phone <> 'DO NOT CALL' AND Id NOT IN :pendingProspectIds AND Id NOT IN :leadIds limit :maxProspectSize]);
						if(preMoverLeadProspects !=null && !preMoverLeadProspects.isEmpty()){
							maxProspectSize -= preMoverLeadProspects.size(); 
							for(String s: preMoverLeadProspects.keySet()){
								scoredItemObjMap.put(s, new StreetSheetItem__c( Score__c = 1));
							}
						}
						
						// Condition #2
						if(maxProspectSize>0){
							Map<String, Lead> newMoverLeadProspects = new Map<String, Lead>([SELECT Id, Type__c FROM Lead WHERE OwnerID = :rep.Id AND isConverted = FALSE AND Type__c = 'New Mover' AND NewMoverDate__c >= LAST_N_DAYS:90 AND DispositionDetail__c = '' AND Id NOT IN :preMoverLeadProspects.keySet() AND Phone <> null AND Phone <> 'DO NOT CALL' AND Id NOT IN :pendingProspectIds AND Id NOT IN :leadIds limit :maxProspectSize]);
							if(newMoverLeadProspects!=null && !newMoverLeadProspects.isEmpty()){
								maxProspectSize -= newMoverLeadProspects.size();
								for(String s: newMoverLeadProspects.keySet()){
									scoredItemObjMap.put(s, new StreetSheetItem__c( Score__c = 2));
								}
							}
						}
						
						// Condition #3
						if(maxProspectSize > 0){
							Date n90 = Date.today().addDays(-90);
							Map<String, Lead> discoLeadProspects = new Map<String, Lead>([SELECT Id, Type__c FROM Lead WHERE OwnerID = :rep.Id AND isConverted = FALSE AND NewMoverDate__c < :n90 AND DispositionDetail__c = '' AND Phone <> null AND Phone <> 'DO NOT CALL' AND Id NOT IN :pendingProspectIds AND Id NOT IN :leadIds limit :maxProspectSize]);
							if(discoLeadProspects!=null && !discoLeadProspects.isEmpty()){
								maxProspectSize -= discoLeadProspects.size();
								for(String s: discoLeadProspects.keySet()){
									scoredItemObjMap.put(s, new StreetSheetItem__c( Score__c = 3 ));
								}
							}
						}
					}
					
					// We still have some room to keep adding items as our previous search didn't returned enough results
					if(maxProspectSize>0){
						
						// A resale rep does his search on accounts
						if(rep.Business_Unit__c == 'Resi Resale') {
							Map<String, Account> fillAccProspects = new Map<String, Account>([ SELECT Id, Type FROM Account WHERE OwnerID = :rep.Id AND Type = 'Discontinuance' AND LeadStatus__c = 'Active' AND Phone <> null AND Phone <> 'DO NOT CALL' AND DispositionCode__c = '' AND Id NOT IN :pendingProspectIds limit :maxProspectSize]);
							if(fillAccProspects !=null && !fillAccProspects.isEmpty()){
								maxProspectSize -= fillAccProspects.size();
								for(String s: fillAccProspects.keySet()){
									scoredItemObjMap.put(s, new StreetSheetItem__c( Score__c = 4 ));
								}
							}
						}
						
						// For new sales rep
						else {
							Map<String, Lead> fillerLeadProspects = new Map<String, Lead>([SELECT Id, Type__c FROM Lead WHERE OwnerID = :rep.Id AND isConverted = FALSE AND Phone <> null AND Phone <> 'DO NOT CALL' AND DispositionCode__c = '' AND Id NOT IN :pendingProspectIds AND Id NOT IN :leadIds Order By DispositionCode__c desc limit :maxProspectSize]);
							if(fillerLeadProspects!=null && !fillerLeadProspects.isEmpty()){
								for(String s: fillerLeadProspects.keySet()){
									scoredItemObjMap.put(s, new StreetSheetItem__c( Score__c = 4 ));
								}
							}					
						}
					}
					
					// Generate list					 
					if( scoredItemObjMap!=null && !scoredItemObjMap.isEmpty() ){
						try{
							processProspectsOwnership(scoredItemObjMap.keySet());							
							String prospectListID = StreetSheetManager.create('PHONE - '+prospectListDefaultName(rep.Name), scoredItemObjMap.keySet(), scoredItemObjMap);
							return new PageReference('/apex/ManageProspectList?id='+prospectListID+'&sfdc.override=1');
						}
						catch(Exception err){
		            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Unexpected error while creating your list. ' + err.getMessage()+' '+err.getStackTraceString() ));
						}
					}
					else{
						noProspectsFound();
						return null;
					}
					
				}
				
			}
	
			
		}
		catch(Exception err){
	    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Sorry, we encountered an unexpected error while processing your request. Please try submitting again.' 
	    		+ err.getMessage()+' '+err.getStackTraceString()));
		}
		
						
		return null;
	} 
	
	/**
	 *	
	 */
	private boolean getProspectIdsFromAddr(Address__c zipCodeVal, FinancialHierarchyProximitySearcher pSearchObj, Set<String> ProspectIDs, String MaxAllowed, Set<String> pendingProspects){
		try{
			if(zipCodeVal!=null){
				MapItem startingPoint = new MapItem(zipCodeVal.Latitude__c, zipCodeVal.Longitude__c);
					
				String boundingCondition = MapUtilities.getBoundingCondition(startingPoint, 1);
				
				List<Account> ProspectList = new List<Account>();
				
				// Exclude some records depending on bussiness rules
				Set<String> excludedObjs = new Set<String> (); 
				if( ProspectIDs!=null && !ProspectIDs.isEmpty() )
					excludedObjs.addAll(ProspectIDs);
				if( pendingProspects!=null && !pendingProspects.isEmpty() )
					excludedObjs.addAll(pendingProspects);
				boundingCondition += getAccountsExcludedCondition(excludedObjs) +' AND Phone <> \'DO NOT CALL\' AND Phone <>\'\' ';
				
				for(Account aProspect:  pSearchObj.findNearbyAccounts(boundingCondition, MaxAllowed, rep, true, false)){
					ProspectIDs.add(aProspect.Id);
				}
				for(Lead aProspect:  pSearchObj.findNearbyLeads(boundingCondition, MaxAllowed, rep, true, false)){
					ProspectIDs.add(aProspect.Id);
				}
				return true;
			}
		}
		catch(Exception err){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Unexpected error while querying locator. ' + err.getMessage()+' '+err.getStackTraceString() )); 
		}
		return false;
	}
	
	private boolean getProspectIdsFromAccount(Set<String> scheduleAccIds, FinancialHierarchyProximitySearcher pSearchObj, Set<String> ProspectIDs, String MaxAllowed, Set<String> pendingProspects){
		try{
			for(Account a: [SELECT Id, Name, AddressID__r.Latitude__c, AddressID__r.Longitude__c FROM Account WHERE ID IN :scheduleAccIds]){
				
				// This is our start point as it's an account scheduled for a morning/afternoon appointment
				// create a MapItem from the lat and long of this pivot account
				MapItem startingPoint = new MapItem(a.AddressID__r.Latitude__c, a.AddressID__r.Longitude__c);
					
				String boundingCondition = MapUtilities.getBoundingCondition(startingPoint, 1);
				
				List<Account> ProspectList = new List<Account>();
				// Exclude some records depending on bussiness rules
				Set<String> excludedObjs = new Set<String> (); 
				if( ProspectIDs!=null && !ProspectIDs.isEmpty() ){
					excludedObjs.addAll(ProspectIDs);
				}
				if( pendingProspects!=null && !pendingProspects.isEmpty() ){
					excludedObjs.addAll(pendingProspects);
				}
				
				boundingCondition += getAccountsExcludedCondition(excludedObjs) + ' AND Phone <> \'DO NOT CALL\' ';
				
				if(includePhoneNo == true)
					boundingCondition += ' AND Phone <>\'\' '; 
				 
				
				for(Account aProspect:  pSearchObj.findNearbyAccounts(boundingCondition, MaxAllowed, rep, true, false)){
					ProspectIDs.add(aProspect.Id);
				}
				for(Lead aProspect:  pSearchObj.findNearbyLeads(boundingCondition, MaxAllowed, rep, true, false)){
					ProspectIDs.add(aProspect.Id);
				}
				
			}
			return true;
		}
		catch(Exception err){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Unexpected error while querying locator. ' + err.getMessage()+' '+err.getStackTraceString() )); 
		}
		return false;
	} 
	
    private void processProspectsOwnership(Set<String> itemIdSet)
    {
        	String acctprefix = Schema.Sobjecttype.Account.getKeyPrefix();
            String leadprefix = Schema.Sobjecttype.Lead.getKeyPrefix();
            set<Id> accountIds = new set<Id>();
            set<Id> leadIds = new set<Id>();
            list<Account> accountsForOwnershipChange = new list<Account>();
            list<Lead> leadsForOwnershipChange = new list<Lead>();              

            for(String itemId : itemIdSet) {       
                if (itemId.toLowerCase().startsWith(acctprefix.toLowerCase()))
                        accountIds.add(itemId);
                else if(itemId.toLowerCase().startsWith(leadprefix.toLowerCase()))
                        leadIds.add(itemId);                            
            }
                                
            // Reset the fields on an Account where the current user is not the owner.
            if(!accountIds.isEmpty()) {
                for(Account a : [SELECT Id, Name, OwnerId, DateAssigned__c, PreviousOwner__c, NewLead__c, UnassignedLead__c
                                 FROM Account
                                 WHERE Id IN :accountIds AND OwnerId <> :rep.Id]){
	                    a.DateAssigned__c = System.now();
	                    a.NewLead__c = true;                    
	                    a.PreviousOwner__c = a.OwnerId;
	                    a.OwnerId = rep.Id;
	                    
	                    if(isManager) {
	                    	a.UnassignedLead__c = true;
	                    }
	                    else {
	                    	a.UnassignedLead__c = false;
	                    }
	                    accountsForOwnershipChange.add(a);      
                }     
                if(accountsForOwnershipChange!= null && !accountsForOwnershipChange.isEmpty() )
                	update accountsForOwnershipChange;
            }
            
            // Reset the fields on a Lead where the current user is not the owner.
            if(!leadIds.isEmpty()) {
                for(Lead l : [SELECT Id, Name, OwnerId, DateAssigned__c, PreviousOwner__c, NewLead__c, UnassignedLead__c
                              FROM Lead
                              WHERE Id IN :leadIds AND IsConverted = false AND OwnerId !=: rep.Id]) {
                        l.DateAssigned__c 	= System.now();                       
                        l.NewLead__c 		= true;
                        l.PreviousOwner__c 	= l.OwnerId;
                        l.OwnerId 			= rep.Id;
                        
                        if(isManager) {
                        	l.UnassignedLead__c = true;
                        }
                        else {
                        	l.UnassignedLead__c = false;
                        }
                        leadsForOwnershipChange.add(l);
                }
                update leadsForOwnershipChange;
            }
            
    }

	private String getAccountsExcludedCondition(Set<String> aProspectSet){
		String resVal = '';
		if(aProspectSet==null || aProspectSet.isEmpty())
			return resVal;
		resVal = ' AND ID NOT IN (';	 
		for(String s: aProspectSet){
			resVal +='\''+s+'\',';
		}
		resVal = resVal.substring(0, resVal.lastIndexOf(','));
		resVal += ') ';	 
		return resVal;
	}
	
	public Set<String> nonWorkedProspects(){
		Set<String> pIds = new Set<String>();
		for(StreetSheetItem__c sItemObj: [SELECT Id, LeadID__c, AccountID__c 
										  FROM StreetSheetItem__c  StreetSheet__c 
										  WHERE ((LeadID__c <> null AND LeadID__c <> '') OR (AccountID__c<> null AND AccountID__c <> ''))  
										  	AND Worked__c = 0
										  	AND StreetSheet__r.OwnerId = :rep.Id]){
			if(!Utilities.isEmptyOrNull(sItemObj.LeadID__c)){
				pIds.add(sItemObj.LeadID__c);
			}
			if(!Utilities.isEmptyOrNull(sItemObj.AccountID__c)){
				pIds.add(sItemObj.AccountID__c);
			}
		}
		return pIds;
	}
	
	public PageReference evalProspectList(){
		renderMaxResultSize = true;
		renderTargetDateFilter = (!Utilities.isEmptyOrNull(listType) && listType == 'Field');
		return null;
	}
	
	public void noProspectsFound(){
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No unassigned prospects available at this time.'));
	}
	
	private static String prospectListDefaultName(String uName) {
		DateTime currentTime = DateTime.now();
        return uName + '-' + currentTime.format('MM/dd/yyyy HH:mm:ss');	
	}

}