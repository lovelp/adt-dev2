public class SignedMultipleStatus {
    
    public class QuoteDisposition {
        public String DispositionTypeStr {get;set;}
        public QuoteDisposition(Disposition__c d){
            DispositionTypeStr = d.DispositionType__c;
        }
    }
    
    
    public SignedMultipleStatus(){
    }
    
    
    
    public void processQuotes(Account a){
        
        Map<String, scpq__SciQuote__c> signedQuotesMap = new Map<String, scpq__SciQuote__c>();
        
        List<Disposition__c> dispLst = new List<Disposition__c>();
        
        Map<String,Disposition__c> quoteDispMap = new Map<String,Disposition__c>();
        
        for (scpq__SciQuote__c q: [Select Id,Name, scpq__QuoteId__c, Account__c from scpq__SciQuote__c Where Account__c =: a.Id And Status2__c = 'Signed' Order By CreatedDate DESC]){
            signedQuotesMap.put(q.scpq__QuoteId__c, q);
            for(Disposition__c d: [Select Id,Name,DispositionDetail__c,DispositionType__c,DispositionDate__c,QuoteId__c from Disposition__c Where DispositionType__c = 'Quote Signed' And QuoteId__c =:q.scpq__QuoteId__c]){
                    quoteDispMap.put(q.scpq__QuoteId__c,d);         
            }        
        }
    }
    
    

}