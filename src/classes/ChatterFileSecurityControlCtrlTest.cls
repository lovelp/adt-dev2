/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ChatterFileSecurityControlCtrlTest {
	
	static void setChatterSettings() {
	    	// Create Sample settings
	    	ChatterSecurity__c ChatterSettings = new ChatterSecurity__c();
	    	chatterSettings.file_type__c = 'ftype1,ftype2,ftype3';
	    	chatterSettings.Chatter_Super_User_Group__c = 'Chatter_Super_User';
	    	chatterSettings.License_Enabled__c = 'Salesforce';
	    	insert ChatterSettings; 
	}
	
    static testMethod void ChatterCtrlPanelTest() {
    	
    	PageReference pageRef = Page.ChatterFileSecurityControl;
    	
    	Test.setCurrentPage(pageRef);
    	
    	setChatterSettings();
    	ChatterFileSecurityControlCtrl vfCtrl = new ChatterFileSecurityControlCtrl();
    	
    	// Load Admin user
    	User uObj = [SELECT Id FROM User Where Profile.Name = 'System Administrator' AND IsActive=true limit 1];
    	
    	//
    	//	FILE TYPE
    	//
    	
    	//	Read file types in configuration
    	List<SelectOption> v7 = vfCtrl.fileTypeGroup;
    	
    	//	Set new file type as the user will do using the UI
    	ApexPages.currentPage().getParameters().put('fTypeParam','gif');
    	vfCtrl.addFileType();
    	
    	//	Remove a previously added file type
    	ApexPages.currentPage().getParameters().put('fTypeParam','gif');
    	vfCtrl.removeFileType();
    	
    	
    	//
    	//	USER LICENSE
    	//
    	
    	// Read current licenses knowing which are already in configuration
    	List<ChatterFileSecurityControlCtrl.userLicenseWrapper> licList = ChatterFileSecurityControlCtrl.getUserLicenses();
    	system.assert(!licList.isEmpty(),'No licenses found in your org!');
    	
    	// Saves/Overwrites license enabled/disabled for this app
    	ChatterFileSecurityControlCtrl.setUserLicense(true, licList.get(0).Name);
    }
	
	 @isTest(SeeAllData=true) //Isolate this test as it requires dml operations on setup objects
    static void ChatterCtrlPanelConfigurationTest() {

    	ChatterFileSecurityControlCtrl vfCtrl = new ChatterFileSecurityControlCtrl();

    	//
    	//	SUPER USER SELECTION
    	//
    	
		//	Read current org's user configuration
    	List<ChatterFileSecurityControlCtrl.userInfo> uList = ChatterFileSecurityControlCtrl.getUserList();
    	system.assert(!uList.isEmpty(),'No users found in your org!');
    	
    	//	Add a user to our super user configuration
    	ApexPages.currentPage().getParameters().put('uIdSelection',uList.get(0).Id);
    	vfCtrl.saveSuperUserSelection();
    	
    }
}