global class ResendFailedAccountsToTelemar implements Schedulable {
   global void execute(SchedulableContext sc) {
      SFDCtoTelemarAccountSync b = new SFDCtoTelemarAccountSync(); 
      
      database.executebatch(b,Integer.valueof(ResaleGlobalVariables__c.getValues('AsyncAccountsBatchSize').value__c.trim()));
   }
}