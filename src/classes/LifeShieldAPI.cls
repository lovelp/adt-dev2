/* LifeShieldAPI
*
* DESCRIPTION : 
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                        DATE              TICKET            REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shanu Aggarwal                   10/09/2019       HRM-11260           Original
* 
*/
public without sharing class LifeShieldAPI {
    public static LifeShieldResponse doCallout(LifeShieldRequest reqBody){
        LifeShieldResponse result = new LifeShieldResponse();
        String body = JSON.serialize(reqBody, true);
        String userName = IntegrationSettings__c.getInstance().LifeShieldUserName__c;
        String password = IntegrationSettings__c.getInstance().LifeShieldPassword__c;
        String endPointURL = IntegrationSettings__c.getInstance().LifeShieldEndpoint__c;
        Blob headerValue = Blob.valueOf(userName + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        Httprequest request = new HttpRequest();
        HttpResponse res = new HttpResponse();
        try{
            Http http = new Http();
            request.setHeader('Authorization', authorizationHeader);
            request.setHeader('Content-Type', 'application/json');
            request.setEndpoint(endPointURL);
            request.setMethod('POST');
            request.setBody(body);
            system.debug('LifeShield Request: '+ request);
            // Send the request
            res = http.send(request);
            system.debug('LifeShield Response: ' + res);
            if(res != null){
                if(String.isNotBlank(res.getBody())){
                    if(res.getStatusCode() == 200 || res.getStatusCode() == 417){
                        // Success
                        system.debug('LifeShield Success Response: ' + res.getBody());
                        result = (LifeShieldResponse) System.JSON.deserializeStrict(res.getBody(), LifeShieldResponse.class);
                    }else{
                        // Failure with no match in status code
                        result.reqStatus = 'failure';
                        result.message = res.getStatusCode() + ' - ' + res.getStatus();
                        system.debug('DEV: Failure with no match in status code');
                    }
                }else{
                    // Failure with no response
                    result.reqStatus = 'failure';
                    result.message = 'Error: While calling LifeShield';
                    system.debug('DEV: Failure with no response body');
                }
            }else{
                // If there is no response from LifeShield
                result.reqStatus = 'failure';
                result.message = 'Error: While calling LifeShield';
                system.debug('DEV: Failure with no response');
            }
            system.debug('result**** ' + result);
        }catch(Exception e){
            ADTApplicationMonitor.log ('LifeShield Create Request Exception', e.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.FLEXFI);
            result.reqStatus = 'failure';
            result.message = 'Error: While calling LifeShield';
        }
        system.debug('Udpate Response->' + res.getBody());
        system.debug('Udpate Status->' + res.getStatusCode());
        return result;
    }
    
    public class LifeShieldRequest{
        public String phone;
        public String streetAddress;
        public String zipCode;
    }
    
    public class LifeShieldResponse{
        public String reqStatus;
        public String message;
        public cLookup lookupData;
    }
    
    public class cLookup{
        public string accountType;
        public string accountStatus;
    }
}