/************************************* MODIFICATION LOG ********************************************************************************************
* LocationDataMessageParameters
*
* DESCRIPTION : A data structure for input data required to retrieve location data.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					10/14/2011				- Original Version
*
*													
*/
public class LocationDataMessageParameters {
	
	public Date searchDate {get;set;}
	public List<Id> userIdList {get;set;}
	public Boolean historicalDataRequired {get;set;}
	public Boolean isForATeam {get;set;}
	public String teamManagerId {get;set;}
	
	public LocationDataMessageParameters(Date aSearchDate, List<Id> aUserIdList, String teamManagerId,
											Boolean aHistoricalDataRequired) {
		this.searchDate = aSearchDate;
		this.userIdList = aUserIdList;
		this.teamManagerId = teamManagerId;
		this.historicalDataRequired = aHistoricalDataRequired;
		
		if (teamManagerId != null && teamManagerId != LocationDataMessageProcessor.NONE)
			this.isForATeam = true;
		else {
			this.isForATeam = false;
		}
	}
	


}