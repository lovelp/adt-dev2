public class ManagerScheduleController {
    public String userId;
    public User loggedInUser{get;set;}
    public Map<Id,User> subordinateUsers{get;set;}
    public String selectedUserId{get;set;}
    public List<id> FieldRoleids;
    public List<user> assignedUsers{get;set;}
    public List<user> unassignedUsers{get;set;}
    
    public ManagerScheduleController(){
        if(ApexPages.currentPage().getParameters().get('managerId') != null)
            Userid=ApexPages.currentPage().getParameters().get('managerId');
        else
            Userid=userinfo.getUserId();
        
        
        subordinateUsers=new Map<id,User>();
        assignedUsers=new List<user>();
        /*
        List<userrole> userRoles=[select id from userrole where name='NA Resi Program Dir/Sales Ops' or name='NA SB Program Dir/Sales Ops'];
        if(userRoles.size()>0){
            FieldRoleids=new List<id>();
            for(userrole ur:userRoles){
                FieldRoleids.add(ur.id);
            }
        }
        Set<id> roles1=subordinateRolesClass.getSubordinateRoles(FieldRoleids[0]);
        Set<id> roles2=subordinateRolesClass.getSubordinateRoles(FieldRoleids[1]);
        
        roles1.addAll(roles2);
        system.debug('subordinate roles are '+roles1.size());
        
        Map<id,userrole> subordinateRoles=new Map<id,userrole>([select id,name from userrole where ID IN:roles1]);
        if(subordinateRoles.get(loggedInUser.userRoleid)!=null && subordinateRoles.get(loggedInUser.userRoleid).name.contains('Mgr') ){
            subordinateUsers=new Map<id,User>([select id, name, manager.name from user where manager.id=:loggedInUser.id]);
        }
        
        */
        loggedInUser=[select id, profile.name, UserRoleId,name  from user where id=:Userid];
        
        if(loggedInUser.profile.name=='ADT NA Sales Manager'){
            subordinateUsers=new Map<id,User>([select id, name, manager.name from user where manager.id=:loggedInUser.id and profile.name='ADT NA Sales Representative']);
        }
        if(subordinateUsers.keyset().size()>0){
            List<UserTerritory2Association> terrAssocList=[SELECT id, UserId, Territory2id, Territory2.name, Territory2.Territory2Model.Name, Territory2.parentTerritory2.name 
                                                            FROM UserTerritory2Association WHERE userid IN:subordinateUsers.keyset()];
            if(terrAssocList.size()>0){
                for(UserTerritory2Association uta:terrAssocList){
                    if(subordinateUsers.get(uta.userId) != null){
                        assignedUsers.add(subordinateUsers.get(uta.userId));
                        subordinateUsers.remove(uta.userId);
                    }                    
                }
            }     
        }
        unassignedUsers=subordinateUsers.values();       
    }
    
    public pageReference addSchedule(){
        if (selectedUserId != null){
            PageReference pageRef = new PageReference('/apex/UserSchedule?UserId='+selectedUserId);
            return pageRef;
        }
        else
            return null;               
    }
}