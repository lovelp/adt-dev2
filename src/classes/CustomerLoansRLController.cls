/************************************* MODIFICATION LOG ********************************************************************************************
*
* DESCRIPTION : Display customer loans for CustomerLoansRL LC
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                   TICKET              REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Srinivas Yarramsetti          06/17/2019             HRM-9790       - Original Version
*/

public without sharing class CustomerLoansRLController {
    
    public Static set<String> onThisAccCount;
    
    @AuraEnabled
    //Display loans in the billing sections
    public static list<customerLoanWrapper> getExistingLoans(String recId, Boolean runRefresh){
        onThisAccCount = new set<String>();
        Map<String, customerLoanWrapper> custLoanMap = new Map<String, customerLoanWrapper>();
        set<String> quoteNumber = new set<String>();
        list<Account> acclist = [SELECT Id, MMBCustomerNumber__c, MMBSiteNumber__c FROM Account WHERE Id =: recId AND MMBCustomerNumber__c != NULL];
        if(acclist!= null && acclist.size()>0){
            if(runRefresh){
               LoanUtility.customerLoanRefresh(acclist[0].MMBCustomerNumber__c,null);
            }
            list<CustomerLoan__c> allLoans = LoanUtility.queryLoans(new set<String>{acclist[0].MMBCustomerNumber__c});
            Integer outsize = 0;
            outsize = LoanUtility.getLoansForInactiveSites(acclist[0], allLoans).size();
            integer i = 0;
            for(CustomerLoan__c cl: sortloansInOrder(allLoans, acclist[0].MMBSiteNumber__c)){
                if(String.isNotBlank(cl.QuoteOrderNumber__c)){
                    if(quoteNumber.add(cl.QuoteOrderNumber__c)){
                        list<CustomerLoan__c> tempLnlist = new list<CustomerLoan__c>();
                        customerLoanWrapper custWrapper = new customerLoanWrapper();
                        custWrapper.quoteNumber = cl.QuoteOrderNumber__c;
                        custWrapper.customerNumber =  cl.CustomerNumber__c;
                        custWrapper.lastRefreshedon = cl.CreatedDate;
                        custWrapper.outSiteSize = outsize;
                        custWrapper.siteOnthisAcc = onThisAccCount.size();
                        custWrapper.thisAccount = onThisAccCount.contains(cl.SiteNumber__c);
                        custWrapper.colorCode = Math.MOD(i,2) == 0? 'background-color:#ffffff' : 'background-color:#c9c6c680';
                        tempLnlist.add(cl);
                        custWrapper.customerLoans = tempLnlist;
                        custLoanMap.put(cl.QuoteOrderNumber__c, custWrapper);
                        i++;
                    }else{
                         custLoanMap.get(cl.QuoteOrderNumber__c).customerLoans.add(cl);
                    }
                }
            }
            return custLoanMap.values().size()>0?custLoanMap.values():null;
        }
        return null;
    }
     
    public class customerLoanWrapper{
        @AuraEnabled
        public list<CustomerLoan__c> customerLoans;
        @AuraEnabled
        public String quoteNumber;
        @AuraEnabled
        public String customerNumber;
        @AuraEnabled
        public String colorCode;
        @AuraEnabled
        public DateTime lastRefreshedon;
        @AuraEnabled
        public Integer outSiteSize;
        @AuraEnabled
        public Integer siteOnthisAcc;
        @AuraEnabled
        public Boolean thisAccount;
    }
    
    public static list<CustomerLoan__c> sortloansInOrder(list<CustomerLoan__c> cusloans, String siteno){
        list<CustomerLoan__c> currentAcclist = new list<CustomerLoan__c>();
        list<CustomerLoan__c> outSitelist = new list<CustomerLoan__c>();
        list<CustomerLoan__c> remianingAll = new list<CustomerLoan__c>();
        list<CustomerLoan__c> finallist = new list<CustomerLoan__c>();
        for(CustomerLoan__c cl:cusloans){
            if(cl.SiteNumber__c == siteno){
                currentAcclist.add(cl);
                onThisAccCount.add(cl.SiteNumber__c);
            }else if(cl.SiteStatus__c == 'OUT'){
                outSitelist.add(cl);
            }else{
                remianingAll.add(cl);
            }
        }
        //First list which belongs to current account
        if(currentAcclist.size()>0)
            finallist.addAll(currentAcclist);
        //Out sites
        if(outSitelist.size()>0)
            finallist.addAll(outSitelist);
        //Remianing all
        if(remianingAll.size()>0)
            finallist.addAll(remianingAll);
        return finallist;
    }
    
    @AuraEnabled
    public static void refreshRecords(String custNo){
        LoanUtility.customerLoanRefresh(custNo, null);
    }
}