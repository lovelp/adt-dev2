/************************************* MODIFICATION LOG ********************************************************************************
* SciQuotePriceChangeHandler
*
DESCRIPTION : 
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                DATE                 TICKET					REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Siju Vaghese             06/10/2019           						- Original Version
* Siju Varghese            06/10/2019           HRM-10173            	- Ability to Negotiate Price 
*/

public with sharing class SciQuotePriceChangeHandler {

	public static void ProcessSciQuoteAfterUpdate(Map<Id, scpq__SciQuote__c> NewSciQuotes, Map<Id, scpq__SciQuote__c> OldSciQuotes){ 
    	if(runOnce()){
	    	list<scpq__SciQuote__c> newSciQuotesList =  NewSciQuotes.values();
	    	list<scpq__SciQuote__c> oldSciQuotesList =  OldSciQuotes.values();	    	
	    	map<String,priceChangeDetails> newPriceChangeDetailsMap = new map<String,priceChangeDetails>();
	    	map<String,priceChangeDetails> oldPriceChangeDetailsMap = new map<String,priceChangeDetails>();
	    	
	    	for(scpq__SciQuote__c NewSciQuote: newSciQuotesList){
				if(String.isNotBlank(NewSciQuote.Job_Details__c)){  
	            	newPriceChangeDetailsMap = findPriceChange(NewSciQuote.Job_Details__c,NewSciQuote.scpq__QuoteId__c,NewSciQuote.Account__c);
	            }
	        }
	        for(scpq__SciQuote__c oldSciQuote: oldSciQuotesList){
				if(String.isNotBlank(oldSciQuote.Job_Details__c)){     
				      	oldPriceChangeDetailsMap = findPriceChange(oldSciQuote.Job_Details__c,oldSciQuote.scpq__QuoteId__c,oldSciQuote.Account__c);
	            }
	        } 
	        list<disposition__c> dispositionList = new list<disposition__c>();
	        
	        Set<String> userIds = new set<String>();
	        for(priceChangeDetails prCh: newPriceChangeDetailsMap.values()){
	        	userIds.add(prCh.loginId);
	        }
	        list<User> userList = new list<User>();
	        userList = [select username,Name from User Where username IN: userIds];
	        map<String,String> UserNameMap = new map<String,String>();
	        for(user u: userList){
	        	UserNameMap.put(u.username,u.name);
	        }
            // Compare the Price Change - from the old value and new values
	        for(priceChangeDetails prCh: newPriceChangeDetailsMap.values()){
	        	if((String.isNotBlank(prCh.CurrentPrice) &&  string.isNotBlank(prCh.OriginalPrice) && (prCh.CurrentPrice != prCh.OriginalPrice) &&
	        	    (oldPriceChangeDetailsMap.get(prCh.itemID) != null && oldPriceChangeDetailsMap.get(prCh.itemID).CurrentPrice !=  prCh.CurrentPrice)) ||
	        	    oldPriceChangeDetailsMap.get(prCh.itemID) == null){
	        	        
	        		disposition__c disp = new disposition__c();
	                disp.AccountID__c = prCh.accountId;
	                disp.DispositionType__c ='System Generated';
	                disp.DispositionDate__c = Datetime.now();
	                disp.DispositionedBy__c = UserInfo.getUserId();
	                String UserName = UserNameMap.get(prCh.loginId);//UserInfo.getName();
	                String PrevAmt = string.isBlank(prCh.OriginalPrice) ?'0':prCh.OriginalPrice;
	                String CurrentAmt = string.isBlank(prCh.CurrentPrice) ?'0':prCh.CurrentPrice;
	                //String tim = string.isBlank(prCh.PriceChangeDate) ? string.valueOfGmt(Datetime.now()) : prCh.PriceChangeDate ;
	                String tim = string.valueOf(System.Now().format());
	                String DispoDetail = 'Quote ID : ' + prCh.quoteID + ' Item ID: '+ prCh.itemID ;
	                disp.Comments__c = ' Price Negotiated from $'+ PrevAmt +' to $'+ CurrentAmt +'  at '+ tim + ' with User: ' + UserName;
	                disp.DispositionDetail__c = DispoDetail;
	                dispositionList.add(disp);
	            }    	
	        }
	        
			if(!dispositionList.isEmpty()){
	            insert dispositionList;
	        }
    	}
	}
      
    private static map<String,PriceChangeDetails> findPriceChange (String StrXML,String quoteID,String accountId){
    	map<String,PriceChangeDetails> priceChangeMap = new map<String,PriceChangeDetails>();
    	DOM.Document doc = new DOM.Document();
		doc.load(StrXML);
		Dom.XMLNode xmlNodeJobs = doc.getRootElement();
		PriceChangeDetails priceChangeDetail;
		for(Dom.XMLNode n: xmlNodeJobs.getChildElements()){ 
			if(n.getName()=='Job'){
				for(Dom.XMLNode nc: n.getChildElements()){
					if( nc.getName()=='PriceChanges'){
						for(Dom.XMLNode ncc: nc.getChildElements()){ 
							if(ncc.getName()=='PriceChange'){
								priceChangeDetail = new PriceChangeDetails();
								priceChangeDetail.quoteID=quoteID;
								priceChangeDetail.accountId=accountId;
								priceChangeDetail.itemID =  ncc.getAttributeValue('ItemID', null); 
								priceChangeDetail.packageID =  ncc.getAttributeValue('PackageID', null);
								priceChangeDetail.originalPrice =  ncc.getAttributeValue('Original', null);
								priceChangeDetail.currentPrice =  ncc.getAttributeValue('Current', null);
								priceChangeDetail.loginId =  ncc.getAttributeValue('LoginId', null);
								priceChangeDetail.priceChangeDate =  ncc.getAttributeValue('Date', null);
							    priceChangeMap.put(priceChangeDetail.itemID,priceChangeDetail);							    
							}
						}
					}
				}
			}

		}
		return priceChangeMap;	
    }

	//Siju Added-HRM# 9895 - Storing and calculate the price Change
    class PriceChangeDetails{
         String itemID;
         String packageID;
         String originalPrice;
         String currentPrice;
         String loginId;
         String priceChangeDate;
         String quoteID;
         String accountId;
    }
    
    private static boolean run = true;
    public static boolean runOnce(){
	    if(run){
	     	run = false;
	     	return true;
	    }else{
	        return run;
		}
	}
}