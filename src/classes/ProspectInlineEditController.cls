/************************************* MODIFICATION LOG ********************************************************************************************
* ProspectInlineEditController
*
* DESCRIPTION : Controller for street sheet item list VF page on prospect list detail page
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					3/20/2012				- Original Version
*
*													
*/

public with sharing class ProspectInlineEditController {
	
	public List<Account> accounts {get;set;}
	public List<Lead> leads {get;set;}
	public Id ssid {get;set;}
	public String deleteItemId {get;set;}
	
	public Lead dispLead {get;set;}
	public Account dispAccount {get;set;}
	public Id dispLeadId {get;set;}
	public Id dispAccountId {get;set;}
	public Boolean dispAccountStandard {get;set;}
	public Boolean showReasonSystem {get;set;}
	public Boolean showReasonDetail {get;set;}
	public String errormsg {get;set;}
	
	private ApexPages.Standardcontroller stdcon;
	private List<StreetSheetItem__c> ssitems;
	
	public ProspectInlineEditController( ApexPages.Standardcontroller stdcontroller ) {
		stdcon = stdcontroller;
		ssid = stdcon.getId();
		
		getItems();
	}
	
	public PageReference deleteSSItem() {
		
		String acctprefix = Schema.Sobjecttype.Account.getKeyPrefix();
		String leadprefix = Schema.Sobjecttype.Lead.getKeyPrefix();
		if (deleteItemId == null || deleteItemId.length() == 0) {
			return null;
		}
		
		for (StreetSheetItem__c ssi : ssitems) {
			if (ssi.AccountID__c == deleteItemId || ssi.LeadID__c == deleteItemID) {
				delete ssi;
				break;
			}
		}
		
		if (deleteItemId.startsWith(acctprefix)) {
			for (Integer i=accounts.size()-1;i>=0;i--) {
				if (deleteItemId == accounts[i].Id) {
					accounts.remove(i);
					break;
				}
			}
		}
		else if (deleteItemId.startsWith(leadprefix)) {
			for (Integer i=leads.size()-1;i>=0;i--) {
				if (deleteItemId == leads[i].Id) {
					leads.remove(i);
					break;
				}
			}
		}
		
		return null;
	}
	/*
	public PageReference doSave() {
		update accounts;
		update leads;
		getItems();
		return null;
	}*/
	
	public PageReference doCancel() {
		errormsg = null;
		return null;
	}
	
	public PageReference leadDisp() {
		dispLead = null;
		dispAccount = null;		
		try {
			dispLead = [
				Select Id, Name, Company, DispositionCode__c, DispositionDetail__c, Business_ID__c,
					ClosePotential__c, Competitor__c, DispositionDate__c, CompetitorContractEndDate__c,
					DispositionComments__c, ExistingSolutions__c, ExistingEquipment__c, EquipmentSold__c,
					DoNotCall, DoNotMail__c, Channel__c
				from Lead 
				where Id = :dispLeadId
			];
		} catch (Exception ex) {
			return null;
		}
		return null;
	}
	
	public PageReference accountDisp() {
		dispLead = null;
		dispAccount = null;		
		showReasonSystem = false;
		showReasonDetail = false;
		dispAccountStandard = false;
		try {
			dispAccount = [
				Select Id, Name, DispositionCode__c, DispositionDetail__c, Reason1ForSystem__c, 
					Reason1DetailForSystem__c, Business_Id__c, DispositionComments__c, DispositionDate__c,
					Competitor__c, CreateFollowupAfter__c,
					CompetitorContractEndDate__c, DoNotCall__c, DoNotMail__c,
					ExistingSolutions__c, SolutionsNotInstalled__c, ModelSalesCallconducted__c,
					ROIDetermined__c, PulseDemonstrated__c, VideoDiscussed__c, 
					PackageProposed__c,
					AccessDiscussed__c, ResidentialOffer__c, DoesHomeownerOwnOrManageABusines__c,
					ClosePotential__c, ServicesProposed__c, ADSCQuoted__c, ANSCQuoted__c,
					RecordType.DeveloperName
				from Account 
				where Id = :dispAccountId
			];
			dispAccountStandard = dispAccount.RecordType.DeveloperName == RecordTypeDevName.STANDARD_ACCOUNT;
		} catch (Exception ex) {
			return null;
		}
		if (dispAccount.Business_Id__c != null) {
			if (dispAccount.Business_Id__c.contains('1100')) {
				showReasonSystem = true;
				showReasonDetail = true;
			} else if (dispAccount.Business_Id__c.contains('1200')) {
				showReasonSystem = true;
			}
		}
		return null;
	}
	
	public PageReference saveDisp() {
		errormsg = null;
		if (dispLead != null) {
			try {
				update dispLead;
			} catch(Exception ex) {
				errormsg = ex.getMessage();
				ApexPages.addMessages(ex);
			}
		} else if (dispAccount != null) {
			try {
				update dispAccount;
			} catch(Exception ex) {
				errormsg = ex.getMessage();
				ApexPages.addMessages(ex);
			}
		}
		if (errormsg == null) {
			getItems();
		}
		return null;
	}
	
	private void getItems() {
		
		//get ss items
		ssitems = new List<StreetSheetItem__c>([
			select Id,AccountId__c, LeadId__c from StreetSheetItem__c where StreetSheet__c = :ssid
		]);
		
		list<Id> aids = new list<Id>();
		list<id> lids = new List<Id>();
		
		for (StreetSheetItem__c ssi : ssitems) {
			if (ssi.AccountID__c != null) {
				aids.add(ssi.AccountID__c);
			}
			else if (ssi.LeadID__c != null) {
				lids.add(ssi.LeadID__c);
			}
		}
		
		accounts = new List<Account>([
			select Id, Name, DispositionCode__c, DispositionDate__c, SiteStreet__c, SiteCity__c,
				SitePostalCode__c, SitePostalCodeAddOn__c, Phone, DispositionDetail__c, Reason1ForSystem__c,
				Data_Source__c, Type, DisconnectReason__c, Business_ID__c
			from Account 
			where Id IN :aids
		]);
		
		leads = new List<Lead>([
			select Id, Name, DispositionCode__c, DispositionDate__c, SiteStreet__c, SiteCity__c,
				SitePostalCode__c, SitePostalCodeAddOn__c, Phone, DispositionDetail__c, LeadSource,
				Type__c, Company, Business_ID__c, Channel__c
			from Lead
			where Id IN :lids
		]);
	}
	
}