@istest
public class AccountSearchControllerTest {

    @TestSetup
    private static void setup() {
        Profile p = [select id from profile where name='System Administrator'];
        
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'TRUE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));
        insert rgvList;
        User u=new User(alias = 'stand', email='standarduser@testorg.com',
                        emailencodingkey='UTF-8', lastname='ADMIN',firstname='TestGlobal', languagelocalekey='en_US',
                        localesidkey='en_US', profileid = p.Id,
                        timezonesidkey='America/Los_Angeles', username='testing100@adt.com',
                        StartDateInCurrentJob__c = Date.today());
        insert u;
        FlexFiMessaging__c msg = new FlexFiMessaging__c(Name = 'LoanUpdateFailed', ErrorMessage__c = 'Application Update Failed');
        insert msg;
        FlexFiMessaging__c mess400 = new FlexFiMessaging__c();
        mess400.name = '400';
        mess400.ErrorMessage__c = 'Invalid input or Bad request';
        mess400.ErrorCode__c = '000';
        insert mess400;
        
        
        FlexFiMessaging__c mess404 = new FlexFiMessaging__c();
        mess404.name = '404 - Account';
        mess404.ErrorMessage__c = 'No Account found';
        mess400.ErrorCode__c = '001';
        insert mess404;
        
        FlexFiMessaging__c ssn404 = new FlexFiMessaging__c();
        ssn404.name = '404 - SSN';
        ssn404.ErrorMessage__c = 'No Account found with the corresponding SSN/DOB';
        ssn404.ErrorCode__c = '002';
        insert ssn404;
        FlexFiMessaging__c lUpdate200 = new FlexFiMessaging__c();
        lUpdate200.name = 'Loan Updated';
        lUpdate200.ErrorMessage__c = 'Loan Application Updated';
        insert lUpdate200;
        map<String, String> EquifaxSettings = new map<String, String>
        {
            'Order Types For Credit Check'=>'N1;R1'
                , 'Order Types For Credit Check - Renters'=>'N1;R1'
                , 'Failure Risk Grade'=>'X'
                , 'Failure Condition Code'=>'APPROV'
                , 'Default Risk Grade'=>'W'
                , 'Default Condition Code'=>'CAE1'
                , 'Employee Risk Grade'=>'U'
                , 'Employee Condition Code'=>'CME3'     
                , 'Days to allow additional check'=>'1'
                ,'FlexFiApprovalRiskGrade' => 'A;B;C;X'};
                    List<Equifax__c> EquifaxSettingList = new List<Equifax__c>(); 
        for( String sN: EquifaxSettings.keySet()){
            Equifax__c eS = new Equifax__c( name = sN );
            eS.value__c = EquifaxSettings.get(sN);
            EquifaxSettingList.add( eS );
        }
        insert EquifaxSettingList;
         List<FlexFiMessaging__c> lstFlexFiMessages = new List<FlexFiMessaging__c>();
        FlexFiMessaging__c msg1 = new FlexFiMessaging__c(Name = 'FlexFiRiskGradeNotValid');
        msg1.ErrorMessage__c = 'Credit is Run but Risk Grade is Not Approved for Loan Application. Please Proceed to ADT Financing.';
        lstFlexFiMessages.add(msg1);
        FlexFiMessaging__c msg2 = new FlexFiMessaging__c(Name = 'ApmError');
        msg2.ErrorMessage__c = 'Test Error Message. Please Proceed to ADT Financing.';
        lstFlexFiMessages.add(msg2); 
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'AddressNotFoundError');
        msg3.ErrorMessage__c = 'Test Error Message. Please Proceed to ADT Financing.';
        lstFlexFiMessages.add(msg3); 
        FlexFiMessaging__c msg4 = new FlexFiMessaging__c(Name = 'ZipCodeNotFoundError');
        msg4.ErrorMessage__c = 'Test Error Message. Please Proceed to ADT Financing.';
        lstFlexFiMessages.add(msg4);
        FlexFiMessaging__c msg5 = new FlexFiMessaging__c(Name = 'StreetNotFoundError');
        msg5.ErrorMessage__c = 'Test Error Message. Please Proceed to ADT Financing.';
        lstFlexFiMessages.add(msg5);
         FlexFiMessaging__c msg6 = new FlexFiMessaging__c(Name = 'creditOlderThan90');
        msg6.ErrorMessage__c = 'Credit is older than 90';
        lstFlexFiMessages.add(msg6);
         FlexFiMessaging__c msg7 = new FlexFiMessaging__c(Name = 'NoCreditCheck');
        msg7.ErrorMessage__c = 'No credit check';
        lstFlexFiMessages.add(msg7);
        FlexFiMessaging__c msg8 = new FlexFiMessaging__c(Name = 'NoEmailError');
        msg8.ErrorMessage__c = 'No email error';
        lstFlexFiMessages.add(msg8);
        FlexFiMessaging__c msg9 = new FlexFiMessaging__c(Name = 'DOBError');
        msg9.ErrorMessage__c = 'No DOB error';
        lstFlexFiMessages.add(msg9);
        FlexFiMessaging__c msg10 = new FlexFiMessaging__c(Name = 'AdtEmployeeError');
        msg10.ErrorMessage__c = 'ADT Employee error';
        lstFlexFiMessages.add(msg10);
        FlexFiMessaging__c msg11 = new FlexFiMessaging__c(Name = 'SpecialCharacters');
        msg11.ErrorMessage__c = 'Account Name must not contain Special Characters and numbers';
        lstFlexFiMessages.add(msg11);
        insert lstFlexFiMessages;
       
        FlexFiMessaging__c mess500 = new FlexFiMessaging__c();
        mess500.name = '500';
        mess500.ErrorMessage__c = 'Something went wrong while processing the request. Please try again later.';
        ssn404.ErrorCode__c = '003';
        insert mess500;
        Postal_Codes__c pc = new Postal_Codes__c();
        pc.name = '22102';
        pc.BusinessID__c = '1100';
        insert pc;
        
        Account a = TestHelperClass.createAccountData();
        a.Business_Id__c = '1100 - Residential';
        a.Channel__c = 'Resi Direct Sales';
        a.MMBOrderType__c = 'R1';
        a.PostalCodeId__c = pc.Id;
        a.MMBCustomerNumber__c = '300010540';
        a.MMBSiteNumber__c = '48857037';
        a.Equifax_Last_Check_DateTime__c = Datetime.now();
        a.EquifaxApprovalType__c = 'APPROV';
        a.TelemarAccountNumber__c = '12345';
        a.EquifaxRiskGrade__c = 'A';
        update a;

        Opportunity opp = New Opportunity();
        opp.AccountId = a.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp; 
        scpq__SciQuote__c quo = new scpq__SciQuote__c();
        quo.Account__c = a.id;
        quo.scpq__OpportunityId__c = opp.id;
        quo.scpq__OrderHeaderKey__c = 'abcxyz';
        quo.scpq__SciLastModifiedDate__c = Date.today();
        quo.scpq__QuoteId__c = '123';
        quo.MMBJobNumber__c = '123456789';
        quo.MMBOrderType__c = 'N1';
        quo.QuotePaidOn__c = Datetime.now();
        quo.Job_Details__c = '<?xml version="1.0" encoding="UTF-8"?><Jobs AltPricing="Y" DisallowFinancing="N" Installment="36" CommRepHRID="z1232345z" CommRepName="Caroline Maitland" CommRepPhone="44 7740301000" CommRepType="FIELD" ContractSigned="N" CreatedByLoginId="ichang@adt.com.adtdev2" DOALock="N" DepositWaived="N" DepositWaiverLock="N" EasyPay="Y" EquifaxApprovalType="CAE1" EquifaxRiskGrade="A" LastConfigBy="cmaitland@adt.com.adtdev2" PaymentTakenBy="ichang@adt.com.adtdev2"><Job ADSCAddOn="0.00" ADSCBase="649.00" ADSCCorpDiscount="0.00" ADSCInstallPrice="674.00" ADSCInstallerCorpDiscount="0.00" ADSCInstallerLineDiscount="0.00" ADSCOtherAddOn="25.00" ADSCOtherCorpDiscount="0.00" ANSCAdditionalMonitoring="0.00" ANSCAdditionalQSP="0.00" ANSCBaseMonitoring="40.99" ANSCBaseQSP="7.00" ANSCCorpDiscount="0.00" ANSCFinalRMR="47.99" ANSCInstallerCorpDiscount="0.00" ANSCInstallerLineDiscount="0.00" ANSCOtherCorpDiscount="0.00" CSNumber="" DLL="N" DOALock="N" DefaultContract="SP040" JobNo="" JobType="INSTP" ProductFamily="Home Security" RequireElectrician="N" RequireLocksmith="N" SaleType="NewSale" TaskCode="RAI" TechnologyId="1048"><Packages><Package ID="L2 001" Name="Remote" /></Packages><Promotions /><JobLines> <JobLine Item="TS BUNDLE BA" Name="TS Wireless Sensors - Motion - Keyfob Bundle with:" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA020R" Name="1 Heat Detector Wireless" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA015" Name="Broadband External Gateway &amp; Interactive Module" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA027" Name="Interactive Chip (Required for older existing SWPro3K Panels)" Package="L2 001" PackageQty="1.00" /><JobLine Item="SA017-2" Name="Pulse dimming lamp module" Package="L2 001" PackageQty="1.00" /><JobLine Item="L2 TS" Name="TSSC COMPLETE PACK, WHITE, 2-1-1, WITH RADIO - ATT LTE" Package="L2 001" PackageQty="1.00" /></JobLines></Job></Jobs>';
        insert quo;
        Quote_Job__c qJob = new Quote_Job__c();
        qJob.JobNo__c = '123456789';
        qJob.ParentQuote__c = quo.id;
        insert qJob; 
    }
    
    static testMethod void searchAccount_without_jobnumber_returns_no_rows() {
        List<AccountSearchController.AccountWrapper> result = AccountSearchController.searchAccount('','','');
        System.assertEquals(0, result.size());
    }

    static testMethod void searchAccount_with_jobnumber_returns_one_row() {
        string jobNumber = '123456789';
        List<AccountSearchController.AccountWrapper> result = AccountSearchController.searchAccount(jobNumber,'','');
        System.assertEquals(1, result.size());
    }
    static testMethod void searchAccount_with_siteNumber_and_custNumber_returns_one_row() {
        string jobNumber = '';
        string siteNumber = '48857037';
        string custNumber = '300010540';
        List<AccountSearchController.AccountWrapper> result = AccountSearchController.searchAccount(jobNumber,custNumber,siteNumber);
        System.assertEquals(1, result.size());
    }    

    static testMethod void getUIThemeDescription_returns_something() {
        string result = AccountSearchController.getUIThemeDescription();
        System.assertNotEquals(null, result);
    }
}