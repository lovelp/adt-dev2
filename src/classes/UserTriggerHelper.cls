/************************************* MODIFICATION LOG ********************************************************************************************
* *
* DESCRIPTION : Called from User trigger
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Jaydip Bhattacharya           5/23/2012           - Original Version
* Magdiel Herrera               10/14/2013          - Update to support Manager
* Magdiel Herrera               10/16/2013          - Commented ProcessUserAfter method as it was removed from user trigger
*                                                   
*/

public with sharing class UserTriggerHelper {
    
    // Profiles used by the business logic
    public static String MANAGER_PROFILE    = 'ADT NA Sales Manager';
    public static String SALES_REP_PROFILE  = 'ADT NA Sales Representative';
    public static String NSC_SALES_REP_PROFILE  = 'ADT NSC Sales Representative';
    public static String NSC_DLL_Sales_REP_PROFILE = 'ADT NSC DLL Sales Representative';
    public static Profile salesRepProfileObj;                                       
    public static Profile NSCSalesRepProfileObj;                                       
    public static Profile ManagerProfileObj;
    public static Profile NSCDLLSalesRepProfileObj;
    
    /** DEPRECATED: CODE WAS COMMENTED OUT ON USER TRIGGER ***
    //  Removed below method for sprint#10. This is needed for sprint#11 HH changes
    public static void ProcessUserAfter(LIST<User> lstUsers) {
        
        PermissionSet ps=[Select ID from PermissionSet where name='Home_Health_Provider'];
        LIST<PermissionSetAssignment> psAsgnmnt=new LIST<PermissionSetAssignment>();
        psAsgnmnt=[Select ID,PermissionSetId,AssigneeId From PermissionSetAssignment ];
        MAP <string,LIST<PermissionSetAssignment>> mpusrPsasg=new MAP <string,LIST<PermissionSetAssignment>>();
        LIST <PermissionSetAssignment> psAsgnmntNew=New LIST <PermissionSetAssignment>();
        LIST <PermissionSetAssignment> psAsgnmntDel=New LIST <PermissionSetAssignment>();
        boolean bpsExists=false;
        
        for (PermissionSetAssignment psassign:psAsgnmnt){
            LIST <PermissionSetAssignment> psInsLst=new LIST <PermissionSetAssignment> ();
            if (mpusrPsasg.containsKey(psassign.AssigneeId)){
                psInsLst= mpusrPsasg.get(psassign.AssigneeId);
            }
                psInsLst.add(psassign);
                mpusrPsasg.put(psassign.AssigneeId,psInsLst);
            }
        
        PermissionSetAssignment psNewRec;       
        for (User usr:lstUsers){
            
                LIST<PermissionSetAssignment> psChkAsgn=new LIST<PermissionSetAssignment>();
                if (usr.Business_Unit__c=='Home Health'){
                    if (mpusrPsasg.containsKey(usr.id)){
                        psChkAsgn=mpusrPsasg.get(usr.id);
                        if (psChkAsgn.size()>0){
                            for (PermissionSetAssignment psChkforHH:psChkAsgn){
                                if (psChkforHH.PermissionSetId==ps.id){
                                    bpsExists=true;
                                }
                            }
                        }
                    }
                
                
                    if (!mpusrPsasg.containsKey(usr.id)||bpsExists==false){
                        psNewRec=New PermissionSetAssignment();
                        psNewRec.AssigneeId=usr.id;
                        psNewRec.PermissionSetId=ps.id;
                        psAsgnmntNew.add(psNewRec);
                    
                    }
                
                }
            else {  //if user's Business Unit is not Home health, then need to check PS assignment for removal, if any.
                if (mpusrPsasg.containsKey(usr.id)){
                        psChkAsgn=mpusrPsasg.get(usr.id);
                        if (psChkAsgn.size()>0){
                            for (PermissionSetAssignment psChkforHH:psChkAsgn){
                                if (psChkforHH.PermissionSetId==ps.id){
                                    psAsgnmntDel.add(psChkforHH);
                                }
                            }
                        }
                    }
                
                
                }
                bpsExists=false;
        }
    
        if (psAsgnmntNew.size()>0){
            insert psAsgnmntNew;
        }
        
        if (psAsgnmntDel.size()>0){
            delete psAsgnmntDel;
        }   
    }*/
    
    static {
        salesRepProfileObj = [ SELECT ID, Name FROM Profile WHERE Name = :SALES_REP_PROFILE limit 1 ];
        ManagerProfileObj = [SELECT ID, Name  FROM Profile WHERE Name = :MANAGER_PROFILE limit 1];
        NSCSalesRepProfileObj = [SELECT ID, Name  FROM Profile WHERE Name = :NSC_SALES_REP_PROFILE limit 1];
    	NSCDLLSalesRepProfileObj = [SELECT ID, Name  FROM Profile WHERE Name = :NSC_DLL_SALES_REP_PROFILE limit 1];
    }
    
    public static void ProcessUserAfterUpdate(List<User> userList, Map<id, User> userOldMap){

        Set<String> managerRoleIdSet = new Set<String>();
        Set<String> managerIdSet     = new Set<String>();
         

        for (User usr:userList){
           // Deactivating Manager OR Changing a manager's role
            if( usr.ProfileID == ManagerProfileObj.Id 
                && (usr.isActive != userOldMap.get(usr.id).isActive || usr.UserRoleId != userOldMap.get(usr.id).UserRoleId) ) {
                    
                // If changing a manager's role then use his old role otherwise use his current role
                if(usr.UserRoleId != userOldMap.get(usr.id).UserRoleId)
                {
                    managerRoleIdSet.add(userOldMap.get(usr.id).UserRoleId);
                    managerRoleIdSet.add(usr.UserRoleId);
                }
                else
                {   
                    managerRoleIdSet.add(usr.UserRoleId);
                }
                managerIdSet.add(usr.Id);
            }
        }   
            
        // If deactivating a manager or updating his role then re-assign users to next available manager
        if(managerRoleIdSet != null && managerRoleIdSet.size()>0 ){
            SalesRepReAssignManager(managerRoleIdSet, managerIdSet);
        }
        
        
    } 
        
    /**
     *  Make sure ADT NA Sales Representatives cannot change Time Zone.
     *  @method ProcessUserBefore
     *  @param LIST<User> userList      - List of users with new data
     *  @param MAP<id, User> userOldMap - Map containing previous information for users being updated
     */ 
    public static void ProcessUserBefore(Map<Id, User> userNewMap, Map<Id, User> userOldMap){
        
        Set<String> srRoleIdSet  = new Set<String>();
        Set<ID> userRepMapKey    = new Set<ID>();
        User currUsr             = [SELECT Profile.Name FROM User WHERE id=:userinfo.getUserId()];                                      
        
        for (User usr:userNewMap.values()){
             
            // Updating Sales Rep User's Role
            if(usr.UserRoleId != userOldMap.get(usr.id).UserRoleId && usr.ProfileID == salesRepProfileObj.Id){
                srRoleIdSet.add(usr.UserRoleId);
                userRepMapKey.add(usr.Id);
            }
            
            // Time Zone is updated             
            if (usr.TimeZoneSidKey <> userOldMap.get(usr.id).TimeZoneSidKey && currUsr.Profile.Name=='ADT NA Sales Representative'){
                usr.addError('Time Zone cannot be changed. Please contact the Administrator for help.');
            }
            
        }
        
        if(userRepMapKey!=null && !userRepMapKey.isEmpty() ){
            SalesRepAssignManager(userNewMap, userRepMapKey, srRoleIdSet);
        }
        
    }
    
    public static void SalesRepReAssignManager(Set<String> managerRoleIdSet, Set<String> managerIdSet){
        
        Map<String, String> managerIdRoleMap = new Map<String, String>();
        List<User> userList = new List<User>();
         
        // Read active managers on roles being referenced
        for(User uManager: [SELECT Id, UserRoleId 
                            FROM User 
                            WHERE UserRoleId IN :managerRoleIdSet 
                                AND isActive = true ]){
            managerIdRoleMap.put(uManager.UserRoleId, uManager.Id); // only one manager per role assumed, randomly picked no order applied
        }
        
        // Read reps under manager roles and re-assign them to a new manager
        for(User uSalesRep: [SELECT Id, ManagerId, UserRole.ParentRoleId 
                             FROM User 
                             WHERE UserRole.ParentRoleId IN :managerRoleIdSet]){
            uSalesRep.ManagerId =   managerIdRoleMap.get(uSalesRep.UserRole.ParentRoleId);
            userList.add(uSalesRep);
        }
        
        if(userList!=null && userList.size()>0 && !Test.isRunningTest()){
            update userList;
        }
    }
    
    
    /**
     *  Assign Manager to users evaluating profile and role
     *  @method SalesRepAssignManager
     */
    public static void SalesRepAssignManager(Map<ID, User> userMap, Set<ID> userRepMapKey, Set<String> srRoleIdSet){
        
        Map<String, String> RepManagerRoleMap   = new Map<String, String>(); 
        Map<String, String> managerRoleMap      = new Map<String, String>(); 
        
        // Read parent roles for current users on trigger
        for(UserRole uRole: [SELECT Id, ParentRoleId FROM UserRole WHERE ID IN :srRoleIdSet]){
            RepManagerRoleMap.put(uRole.Id, uRole.ParentRoleId);
        }
        
        // Read users assigned to parent roles
        for(User uManager: [SELECT Id, UserRoleId, Name FROM User WHERE UserRoleId IN :RepManagerRoleMap.values() AND isActive = true ]){
            managerRoleMap.put(uManager.UserRoleId, uManager.Id); // only one manager per role assumed, randomly picked no order applied
        }
        
        // Assign manager to users
        for(String uObjKey: userRepMapKey ){
            User uObj = userMap.get(uObjKey);               
            uObj.ManagerId = managerRoleMap.get( RepManagerRoleMap.get(uObj.UserRoleId) );
        }
        
    }
    
    public static void  ProcessUserAfterInsert(Map<Id, User> userNewMap){
        SalesRepAssignManagerFromList(userNewMap);
    }
    
    public static void  ProcessUserBeforeInsert(List<User> userNewList){
        
        Map<Id, List<User>>  userByRoleMap = new Map<Id, List<User>>(); 
        
        for(User uObj: userNewList){
        	
        	// Not supporting NSC users
        	if( uObj.ProfileID == NSCSalesRepProfileObj.Id ){
        		continue;
        	}
        	
            List<User> l = new List<User>(); 
            
            if(userByRoleMap.containsKey(uObj.UserRoleId)){
                l = userByRoleMap.get(uObj.UserRoleId);
            }
            
            l.add(uObj);
            userByRoleMap.put(uObj.UserRoleId, l);
            
        }

        // If creating a rep then assign proper manager
        if(userByRoleMap!=null && !userByRoleMap.isEmpty() ){
            
            Map<Id, Id> managerRoleMap = new Map<Id, Id>(); 
            Map<Id, Id> managerUserRoleMap = new Map<Id, Id>(); 
            
            //  Load manager role by knowing each user's role 
            for(UserRole ur :[SELECT Id, ParentRoleId  FROM UserRole WHERE ID IN :userByRoleMap.KeySet()]){
                managerRoleMap.put(ur.Id, ur.ParentRoleId);
            }
            
            for(UserRole ur :[SELECT Id, (Select Id From Users Where isActive = true)  FROM UserRole WHERE ID IN :managerRoleMap.values()]){
                if(ur.Users != null && ur.Users.size()>0){
                    managerUserRoleMap.put(ur.Id, ur.Users[0].Id);
                }
            }
            
            
            for(Id rId :userByRoleMap.KeySet()){
                
                for(User u: userByRoleMap.get(rId)){
                    u.ManagerId = managerUserRoleMap.get( managerRoleMap.get(u.UserRoleId) );
                }
                
            }
            
            
        }       
            
    }
    
    public static void SalesRepAssignManagerFromList(Map<Id, User> userNewMap){
        
        Set<String> managerRoleIdSet = new Set<String>();
        Set<String> managerIdSet     = new Set<String>();
        
        // Separate reps from managers
        for(User uObj: userNewMap.values()){
            
            // Creating a manager user
            if( uObj.ProfileID == ManagerProfileObj.Id ) {
                managerRoleIdSet.add(uObj.UserRoleId);
                managerIdSet.add(uObj.Id);
            }
            
        }

        // If creating a manager then re-assign users to next available manager for this role(s)
        if(managerRoleIdSet != null && managerRoleIdSet.size()>0 ){
            SalesRepReAssignManager(managerRoleIdSet, managerIdSet);
        }
        
    } 
     
}