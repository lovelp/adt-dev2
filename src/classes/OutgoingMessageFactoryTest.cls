@isTest
private class OutgoingMessageFactoryTest {
	
	
	static testMethod void testCreateOutgoingMessageSetUnavailable() {
		
		Test.startTest();
		
		OutgoingMessage om = OutgoingMessageFactory.createOutgoingMessage('setUnavailableTimeNew');
		
		
		Test.stopTest();
		
		
		System.assert(om.ServiceTransactionType.contains('setUnavailableTime'), 'ServiceTransactionType not as expected.');
		
		
	}
	
	static testMethod void testCreateOutgoingMessageSetAccountAccount() {
		
		Test.startTest();
		
		OutgoingMessage om = OutgoingMessageFactory.createOutgoingMessage('setAccountAccount');
		
		
		Test.stopTest();
		
		
		System.assert(om.ServiceTransactionType.contains('setAccount'), 'ServiceTransactionType not as expected.');
		
		
	}
	
	static testMethod void testCreateOutgoingMessageSetAccountDispOnly() {
		
		Test.startTest();
		
		OutgoingMessage om = OutgoingMessageFactory.createOutgoingMessage('setAccountDispositionOnly');
		
		
		Test.stopTest();
		
		
		System.assert(om.ServiceTransactionType.contains('setAccount'), 'ServiceTransactionType not as expected.');
		
		
	}

}