/**
 Description- This test class used for CampaignMemberTrigger and CampaignMemberTriggerHelper.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class CampaignMemberTriggerTest {
    
      static testMethod void testMethod1() {
         List<CampaignMember> cmList= new List<CampaignMember>();
         List<CampaignMember> cmToDeleteList= new List<CampaignMember>();  
         List<Contact> contactList= new List<Contact>();
         
         test.startTest();
         List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
         rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
         rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
         rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));

         insert rgvList;
         
         Account a = TestHelperClass.createAccountData();
         Account a2= TestHelperClass.createAccountData(Date.today()+30, '1200 -Business');
         
         contact c=new contact();
         c.firstname='test contact';
         c.lastname='test lastname';
         c.accountid=a.id;
         contactList.add(c);
         
         contact c1=new contact();
         c1.firstname='test contact 2';
         c1.lastname='test lastname';
         c1.accountid=a.id;
         contactList.add(c1);
         
         contact c2=new contact();
         c2.firstname='test contact a2';
         c2.lastname='test lastname';
         c2.accountid=a2.id;
         contactList.add(c2);
         
         insert contactList;
         
         Contact checkCon= [select id from Contact where id=: c2.Id];
         system.assertEquals(checkCon.Id, c2.Id);
          
         Dialer_List_Output_Control__c dl= OutboundTestDataUtility.createDLPControl();
         insert dl;
         
         Promotion__c p= OutboundTestDataUtility.CreatePromotion();
         insert p;
         
         DNIS__c dnis= OutboundTestDataUtility.CreateDnis(p.id);
         insert dnis;

         Campaign camp= OutboundTestDataUtility.createCampaign(dnis.id, dl.id);
         camp.type= 'Email';
         insert camp;
         
                  
         CampaignMember cm = New CampaignMember(CampaignId=camp.id, ContactId= c.id,Account__c=c.accountid);
         cmList.add(cm);
         
         date expDate = date.newInstance(2018, 12, 12);
         CampaignMember cm2 = New CampaignMember(CampaignId=camp.id, ContactId= c1.id,Account__c=c1.accountid, expiration_date__c= expDate);
         cmList.add(cm2);
         
         CampaignMember cm3 = New CampaignMember(CampaignId=camp.id, ContactId= c2.id,Account__c=c2.accountid);
         cmList.add(cm3);
         
         insert cmList;
         
         CampaignMember checkCM= [select id from CampaignMember where id=: cm3.Id];
         system.assertEquals(checkCM.Id, cm3.Id);
          
         CampaignMember cmToDelete= [select id from CampaignMember where id =: cm.id limit 1];
         cmToDeleteList.add(cmToDelete);
         
         CampaignMember cmToDelete2= [select id from CampaignMember where id =: cm3.id limit 1];
         cmToDeleteList.add(cmToDelete2);
         
         delete cmToDeleteList;
         
         date expDateNew = date.newInstance(2018, 10, 10);
         date expDateNewTo = date.newInstance(2018, 12, 12);
         
         CampaignMemberTriggerHelper.CampaignDate campDateCompareFrom = new CampaignMemberTriggerHelper.CampaignDate(cm.Campaign.Id,expDateNew);
         CampaignMemberTriggerHelper.CampaignDate campDateCompareTo = new CampaignMemberTriggerHelper.CampaignDate(cm.Campaign.Id,expDateNewTo);
         campDateCompareTo.compareTo(campDateCompareFrom);
         campDateCompareFrom.compareTo(campDateCompareTo);
         
         CampaignMemberTriggerHelper.processCampaignMembersAfterInsert(cmToDeleteList);
         
         test.stopTest();
          
      }
      
}