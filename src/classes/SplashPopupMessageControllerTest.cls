/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class SplashPopupMessageControllerTest {

    static testMethod void SplashPopupMessageTest()  {
        
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        PageReference pageRef = Page.SplashPopupMessage;
        Test.setCurrentPage(pageRef);
                
        SplashPopupMessageController cont = new SplashPopupMessageController();
                
        system.runas(current) {
            u = TestHelperClass.createAdminUser();
        }
        
        Message__c uMsg = new Message__c();
        System.runAs(u){
            uMsg.Message_Text__c = 'This is a TEST message';
            uMsg.Active__c = true;
            uMsg.Dismissible__c = false;
            uMsg.Alert_Begin_Date__c = Date.today();
            uMsg.Alert_End_Date__c = Date.today() + 7;
            insert uMsg;
            
            boolean msg = SplashPopupMessageController.checkMessages();

			cont.save();
			uMsg.Dismissible__c = true;
			update uMsg;
			cont.save();
            
        }
        
    } /* End of SplashPopupMessageTest */

   
}