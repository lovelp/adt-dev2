/*
 * Description: This is a test class for the following classes
 * 1.PopulateAreas
 * Created By: Ravi Pochamalla
 *
 */
 
@isTest
private class PopulateAreasTest{
    private static void createTestData(){
        Postal_Codes__c pc=new Postal_Codes__c (name='12346', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100');
        insert pc;
    }
    private static testMethod void testPopulateAreas(){
        createTestData();
        Test.startTest();
            Id batchId=Database.executeBatch(new PopulateAreas());
        test.stopTest();    
    }
}