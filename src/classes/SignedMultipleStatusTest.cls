/**
 Description- This test class used for SignedMultipleStatus.
 Developer- TCS Developer
 Date- 1/5/2018
 */
@isTest
private class SignedMultipleStatusTest {
    
    static testMethod void test1() {
    
        List<ResaleGlobalVariables__c > rgvList= new List<ResaleGlobalVariables__c >();
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataRecastDisableAccountTrigger',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'RIFAccountOwnerAlias',value__c = 'ADT RIF'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DataConversion',value__c = 'FALSE'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'NewRehashHOARecordOwner',value__c = 'jbelch@adt.com.adtdev2'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvList.add(new ResaleGlobalVariables__c (name = 'GlobalResaleAdmin',value__c = 'Global Admin'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'EventRecTypeNameAllowed',value__c = 'Company Generated Appointment,Field Sales Appointment'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'DisqualifyEventTaskCode',value__c = 'TRV,'));
        rgvList.add(new ResaleGlobalVariables__c (name = 'SameDayDLLAppointmentOffset',value__c = '2'));

        insert rgvList;
         
        Account acc = TestHelperClass.createAccountData();
        
        // Opportunity
        Opportunity o = new opportunity();
        o.Name = 'Test';
        o.StageName = 'test';
        o.closeDate = system.today();
        o.AccountId = acc.Id;
        insert o;
        
        // Quote
        scpq__SciQuote__c quote = new scpq__SciQuote__c();
        quote.Name = 'test Quote';
        quote.scpq__TotalDiscountPercent__c = 20.00;
        quote.MMBOutOrderType__c = 'N4';
        quote.scpq__SciLastModifiedDate__c = System.now();
        quote.MMBJobNumber__c = '1111111';
        quote.scpq__QuoteValueAfterDiscounts__c = 100;
        quote.scpq__OpportunityId__c = o.Id;
        quote.scpq__OrderHeaderKey__c = 'externalId';
        quote.scpq__Status__c = 'Created';
        quote.scpq__QuoteValueBeforeDiscounts__c = 100;
        quote.QuoteValueMonthly__c = 50;
        quote.scpq__QuoteId__c = 'Change';
        quote.DOALockID__c = 'test';
        quote.Account__c = acc.Id;
        quote.scpq__Status__c='Signed';
        insert quote;
        
        scpq__SciQuote__c scQuote= [select id from scpq__SciQuote__c where id=: quote.Id];
        system.assertEquals(scQuote.Id, quote.Id);
        
        Disposition__c d= new Disposition__c(DispositionType__c='Quote Signed', DispositionDetail__c='Pending Proposal (SB)', DispositionDate__c=DateTime.now(), Comments__c ='test comment');
        SignedMultipleStatus.QuoteDisposition sq= new SignedMultipleStatus.QuoteDisposition(d);
        
        SignedMultipleStatus s= new SignedMultipleStatus();
        s.processQuotes(acc);
        
        
    }
}