@isTest
public class ADTPartnerGetInstallAppointmentAPITest 
{
	public static List<opportunity> lstOpp;
    public static string pastDate;
    public static string futureDate;            
    public static List<Call_Data__c> lstCalldata;
    public static List<user> lstUser;
    
    public static void createTestData (User u)
    {  
        pastDate = string.valueOf(date.today() -2).substringBefore(' ');
        futureDate = string.valueOf(date.today() + 2).substringBefore(' ');
        
        // create postal codes
        List<Postal_Codes__c> lstPostalCode = new List<Postal_Codes__c>();
        lstPostalCode.add(new Postal_Codes__c(name='33431', BusinessID__c='1100', MMB_TownID__c='867'));
        lstPostalCode.add(new Postal_Codes__c(name='33431', BusinessID__c='1200', MMB_TownID__c='999'));
    	insert lstPostalCode;
        
        // create accounts
        List<account> lstAcc = new List<account>();
        lstAcc.add(new Account(Name = 'TestAcc1', Business_Id__c ='None'));
        lstAcc.add(new Account(Name = 'TestAcc2',Business_Id__c = '1100 - Residential', PostalCodeID__c=lstPostalCode[0].id));
        lstAcc.add(new Account(Name = 'TestAcc2',Business_Id__c = '1200 - Small Business', PostalCodeID__c=lstPostalCode[1].id));
        insert lstAcc;
        
        //Opportunity opp;
		lstOpp = new List<opportunity> ();
        lstOpp.add(New Opportunity(name='TestOpp1', AccountId=lstAcc[1].id, StageName='Prospecting', CloseDate=Date.today().addDays(5)));
        lstOpp.add(New Opportunity(name='TestOpp2', AccountId=lstAcc[0].id, StageName='Prospecting', CloseDate=Date.today().addDays(5)));
        lstOpp.add(New Opportunity(name='TestOpp3', AccountId=lstAcc[2].id, StageName='Prospecting', CloseDate=Date.today().addDays(5)));
        insert lstOpp;
        
        // create call data record
        lstCalldata = new List<Call_Data__c>();
        lstCalldata.add(new Call_Data__c (name = 'TestCallData', Account__c = lstAcc[1].id, Contact_Type__c = 'Inbound Call'));
        lstCalldata.add(new Call_Data__c (name = 'TestCallData', Account__c = lstAcc[0].id, Contact_Type__c = 'Inbound Call'));
        lstCalldata.add(new Call_Data__c (name = 'TestCallData', Account__c = lstAcc[2].id, Contact_Type__c = 'Inbound Call'));
        lstCalldata.add(new Call_Data__c (name = 'TestCallData1', Contact_Type__c = 'Inbound Call'));
        insert lstCalldata;
    }
    
    public static void createUser ()
    {
        //Profile p = [select id from profile where name='ADT NSC DLL Sales Representative'];
        Profile p = [select id from profile where name='System Administrator'];
        
        lstUser = new List<User>();
        
        User u = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testingAdmin@adt.com', EmployeeNumber__c='T1',
            StartDateInCurrentJob__c = Date.today());
        
        lstUser.add(u);       
        insert lstUser;
    }
    
    public static void createCustomSettingData (user u)
    {   
         
        InstallAppointmentsSPConfig__c installConf = new InstallAppointmentsSPConfig__c ();
        installConf.Appointment_Type__c = 'P';
        installConf.DaysOfTheWeek__c = 'YYYYYYY';
        installConf.Importance__c = 9 ;
        installConf.Prom_Set__c = 'RS';
        installConf.No_Of_Offers__c = 10;
        installConf.No_Of_Days__c = 30;
        insert installConf;
            
        EventDefaults__c ed = new EventDefaults__c();
        ed.DurationMinutes__c = 120;
        ed.IsReminderSet__c = true;
        ed.ReminderMinutesBeforeEvent__c = 15;
        ed.StartDateTimeOffsetMinutes__c = 1;
        insert ed;
        
        list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
        rgvs.add(new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(name='IntegrationUser', value__c='Integration User'));
        rgvs.add(new ResaleGlobalVariables__c(name='RIFAccountOwnerAlias', value__c='ADT RIF'));
        rgvs.add(new ResaleGlobalVariables__c(name='DataConversion', value__c='FALSE'));
        rgvs.add(new ResaleGlobalVariables__c(name='AccountUpdateAPI_AllowedOrderTypes', value__c='N1;N4;R1;R3;A1'));
        rgvs.add(new ResaleGlobalVariables__c(name='NewRehashHOARecordOwner', value__c= u.username));
        rgvs.add(new ResaleGlobalVariables__c(Name='GlobalResaleAdmin', value__c='Testing'));
        rgvs.add(new ResaleGlobalVariables__c(Name='Alternate Pricing Model Order Types', value__c='N1;R1'));
        rgvs.add(new ResaleGlobalVariables__c(Name='EventRecTypeNameAllowed', value__c='Company Generated Appointment,Field Sales Appointment'));
        rgvs.add(new ResaleGlobalVariables__c(Name='DisqualifyEventTaskCode', value__c='TRV'));
        rgvs.add(new ResaleGlobalVariables__c (name = 'GoogleGeoCodeDecodeKey',value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk='));
        rgvs.add(new ResaleGlobalVariables__c(Name='SameDayDLLAppointmentOffset', value__c='60'));
        
        
        insert rgvs;
        
        list<PartnerAPIMessaging__c> partapilist = new list<PartnerAPIMessaging__c>();
        partapilist.add(new PartnerAPIMessaging__c(name='400',Error_Message__c = 'invalid request'));
        partapilist.add(new PartnerAPIMessaging__c(name='404',Error_Message__c= 'Opty not found'));
        partapilist.add(new PartnerAPIMessaging__c(name='500',Error_Message__c= 'Something went wrong while processing the request. Please try again later.'));
        partapilist.add(new PartnerAPIMessaging__c(name='appointmentCallOppIDError',Error_Message__c= 'Account does not match for call Id and Opportunity Id'));        
        partapilist.add(new PartnerAPIMessaging__c(name='BlankBusinessId',Error_Message__c= 'Business Id is blank'));     
        partapilist.add(new PartnerAPIMessaging__c(name='BlankMMBTown',Error_Message__c= '	MMB Town not setup'));
        partapilist.add(new PartnerAPIMessaging__c(name='BlankPostalCode',Error_Message__c='Postal code is blank'));
        partapilist.add(new PartnerAPIMessaging__c(name='installSuccess',Error_Message__c='Success'));
        partapilist.add(new PartnerAPIMessaging__c(name='invalidStartDate', Error_Message__c='Start Date is either not valid or not in YYYY-MM-DD format.'));
        partapilist.add(new PartnerAPIMessaging__c(name='NoInstallPackage', Error_Message__c='No install packages found'));
        partapilist.add(new PartnerAPIMessaging__c(name='pastStartDate', Error_Message__c='Invalid Date. Start date cannot be a past date'));        
        
        insert partapilist;
        
    }
    
    
    public static testMethod void TestBlankPartnerIdMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"callId": "'+lstCalldata[0].id+'","opportunityId": "'+lstOpp[0].id+'","partnerRepName": "John Smith","startDate": ""}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            //test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            //test.stopTest(); 
        }
    }
    
    public static testMethod void TestInvalidStartDateMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RV","callId": "'+lstCalldata[0].id+'","opportunityId": "'+lstOpp[0].id+'","partnerRepName": "John Smith","startDate": "2018/03/03"}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }
    
    public static testMethod void TestInvalidCallIdMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RV","callId": "testCallId","opportunityId": "'+lstOpp[0].id+'","partnerRepName": "John Smith","startDate": ""}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }
    
    public static testMethod void TestInvalidOppIdMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RV","callId": "'+lstCalldata[0].id+'","opportunityId": "testopp","partnerRepName": "John Smith","startDate": ""}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }
    
    public static testMethod void TestNoAccMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RV","callId": "'+lstCalldata[1].id+'","opportunityId": "'+lstOpp[0].id+'","partnerRepName": "John Smith","startDate": ""}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }
    
    public static testMethod void TestNoPostalCodeMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RV","callId": "'+lstCalldata[1].id+'","opportunityId": "'+lstOpp[1].id+'","partnerRepName": "John Smith","startDate": ""}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }
    
    public static testMethod void TestNoMMBTownMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RV","callId": "'+lstCalldata[2].id+'","opportunityId": "'+lstOpp[2].id+'","partnerRepName": "John Smith","startDate": ""}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }
    
    public static testMethod void TestNoInstallPackMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RedVentures","callId": "'+lstCalldata[0].id+'","opportunityId": "'+lstOpp[0].id+'","partnerRepName": "John Smith","startDate": ""}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }
    
    public static testMethod void TestNoStartDateMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RV","callId": "'+lstCalldata[0].id+'","opportunityId": "'+lstOpp[0].id+'","partnerRepName": "John Smith","startDate": ""}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }
    
    public static testMethod void TestFutureStartDateMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RV","callId": "'+lstCalldata[0].id+'","opportunityId": "'+lstOpp[0].id+'","partnerRepName": "John Smith","startDate": "'+futureDate+'"}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }
    
    public static testMethod void TestPastStartDateMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RV","callId": "'+lstCalldata[0].id+'","opportunityId": "'+lstOpp[0].id+'","partnerRepName": "John Smith","startDate": "'+pastDate+'"}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }
    
    public static testMethod void TestNoBusinessMethod() 
    {
        createUser();
        User u = lstUser[0];
        System.runAs(u) 
        {
            // Test Data creation
            createCustomSettingData (u);
            createTestData (u);            
            // Set the request body
            String sBody = '{"partnerId": "RV","callId": "'+lstCalldata[1].id+'","opportunityId": "'+lstOpp[1].id+'","partnerRepName": "John Smith","startDate": ""}';
            
            RestRequest req = new RestRequest();
            RestResponse res1 = new RestResponse();
            
            req.httpMethod = 'Post';//HTTP Request Type
            req.requestBody = Blob.valueof(sBody) ;
            RestContext.request = req;
            RestContext.response= res1;
            
            test.startTest();
            ADTPartnerGetInstallAppointmentAPI.doPost();
            test.stopTest(); 
        }
    }

}