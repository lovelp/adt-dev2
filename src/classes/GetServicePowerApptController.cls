/************************************* MODIFICATION LOG ************************************************************
* GetServicePowerApptController
*
* DESCRIPTION : Get Service Appointment Controller has AM, PM slot allotment
*-------------------------------------------------------------------------------------------------------------------
* DEVELOPER            		DATE			TICKET          REASON
*-------------------------------------------------------------------------------------------------------------------
* Srinivas Yarramsetti     	25/01/2017    	HRM - 6319 		Original Version
*/
public class GetServicePowerApptController{
    // If account ID is only got in matrix, get the postal code and branch number
    public InstallAppointmentsSPConfig__c installApptSettings;    
    public List<AppointmentDateWrapper> lstApptWrapper;   
    public Boolean arePackagesAvailable{get;set;}
    
    // Constructor
    public GetServicePowerApptController(){

        // Holds the value of the request params
        installApptSettings = InstallAppointmentsSPConfig__c.getorgDefaults();         
        lstApptWrapper = new List<AppointmentDateWrapper> ();        
    }
    
    // Wrapper class to display the value in UI
    public class AppointmentDateWrapper{
        public string installConfigName {get; set;}
        public string nextAvailableDate {get; set;}
        public String numberOfAppts {get; set;}
        // added additional fields
        public string statusIndicator {get; set;}
        public List<AdditionalApptInfoWrapper> lstAdditionalInfoWrapper {get; set;}
        //Added by Srini
        public String errorFlag{get;set;}
        public String errorMessage;
    }
    
    public class AdditionalApptInfoWrapper{        
        public String appointmentDate {get; set;}
        public boolean amSlot {get; set;}
        public boolean pmSlot {get; set;}
        public integer totalSlots;
    }
    
    public List<AppointmentDateWrapper> getInstallAppointmentDateInfo(string accountId){
        // Make the callout to Service Provider and get the response
        GetServicePowerApptService objSPApptService = new GetServicePowerApptService();
        GetServicePowerApptService.GetServicePowerApptOffersInstallResponse spResponse = objSPApptService.getOffers(accountId);
        // Check for failed responses
        if (spResponse == null){
            list<GetServicePowerApptService.errorBody> err = new list<GetServicePowerApptService.errorBody>();
            if( objSPApptService != null && objSPApptService.errWrap != null){
                err = objSPApptService.errWrap.errors;
            }
            list<AppointmentDateWrapper> errorAppnt = new list<AppointmentDateWrapper>();
            for(GetServicePowerApptService.errorBody erw:err){
                AppointmentDateWrapper apptDateWrapper = new AppointmentDateWrapper();
                apptDateWrapper.numberOfAppts = '0';
                apptDateWrapper.errorFlag = 'true';
                apptDateWrapper.errorMessage = erw.errorMessage;
                //apptDateWrapper.nextAvailableDate = 'Error:'+erw.errorMessage;
                apptDateWrapper.nextAvailableDate = 'Agent dashboard cannot be displayed due to error in service';
                apptDateWrapper.statusIndicator = 'Red';
                apptDateWrapper.installConfigName = 'None';
                errorAppnt.add(apptDateWrapper);
            }
            system.debug('@@apptDateWrapper: '+errorAppnt);
            return errorAppnt;
        }
        system.debug('@@spResponse.jobRequests: ' + spResponse.jobRequests);
        List<InstallAppointmentsStatusColor__c> lstAppStatusColorSetting = [select name,StartDate__c, EndDate__c from InstallAppointmentsStatusColor__c] ;
        // process the response
        for(GetServicePowerApptService.JobRequestInResponse jobRequest : spResponse.jobRequests){
            List<AdditionalApptInfoWrapper> lstAddApptWrapper = new List<AdditionalApptInfoWrapper>();
            AppointmentDateWrapper apptDateWrapper = new AppointmentDateWrapper();
            // install config name, errorflag, errormessage
            apptDateWrapper.installConfigName = jobRequest.jobRequest;
            apptDateWrapper.errorFlag = jobRequest.ErrorFlag;
            apptDateWrapper.errorMessage = jobRequest.ErrorMessage;            
            set<String> dateSet = new set<String>();
            Map<date, AdditionalApptInfoWrapper> offerMAp = new Map<date, AdditionalApptInfoWrapper>();
            Integer avail = 0;
            //Process AppointmentDate, amSlot, pmSlot
            for(GetServicePowerApptService.Offer offer : jobRequest.ReturnValue){
                system.debug('###'+offer);
                String[] startdate = offer.startDate.split('T');
                String[] startTime = startdate[1].split(':');
                DateTime appntDateTime = DateTime.newInstanceGMT(Date.valueOf(startdate[0]),Time.newInstance(Integer.valueOf(startTime[0]),Integer.valueOf(startTime[1]),0,0));
                //Don't add any offers which are less than now.
                /*if(appntDateTime < system.now()){
                    continue;
                }*/
                if(offerMAp.keyset().contains(appntDateTime.dateGMT())){
                    if(appntDateTime.hourGMT() < 12){
                        offerMAp.get(appntDateTime.dateGMT()).amSlot = true;
                        offerMAp.get(appntDateTime.dateGMT()).totalSlots +=1;
                    }
                    else{
                        offerMAp.get(appntDateTime.dateGMT()).pmSlot = true;
                        offerMAp.get(appntDateTime.dateGMT()).totalSlots +=1;
                    }
                }
                else{
                    AdditionalApptInfoWrapper newappntInfo = new AdditionalApptInfoWrapper();
                    newappntInfo.appointmentDate = appntDateTime.dateGMT().format();
                    newappntInfo.totalSlots = 1;
                    if(appntDateTime.hourGMT() < 12){
                        newappntInfo.amSlot = true;
                    }
                    else{
                        newappntInfo.pmSlot = true;
                    }
                    offerMAp.put(appntDateTime.dateGMT(),newappntInfo);
                }
            }
            //Sorting 
            list<date> listforSorting = new list<Date>();
            listforSorting.addAll(offerMAp.keyset());
            listforSorting.sort();
            //Limit number of rows to display, count availability
            Integer limitrows = 0;
            for(Date dt:listforSorting){
                lstAddApptWrapper.add(offerMAp.get(dt));
                limitrows++;
                avail = avail+offerMAp.get(dt).totalSlots;
                if(limitrows >= installApptSettings.No_of_Rows__c){
                    break;
                }
            }
            //Color, nextAvailableDate
            if(listforSorting !=null && listforSorting.size()>0)
            {
                if(listforSorting[0] !=null){
                    if(offerMAp.get(listforSorting[0]).amSlot== true){
                        apptDateWrapper.nextAvailableDate = listforSorting[0].format()+' AM';
                    }
                    else{
                        apptDateWrapper.nextAvailableDate = listforSorting[0].format()+' PM';
                    }
                    Integer daysInBetween = system.today().daysBetween(listforSorting[0]);
                    system.debug(daysInBetween);
                    for(InstallAppointmentsStatusColor__c appColorSetting : lstAppStatusColorSetting){
                        if(daysInBetween >= appColorSetting.StartDate__c && daysInBetween <= appColorSetting.EndDate__c){
                            apptDateWrapper.statusIndicator = appColorSetting.name;
                        }
                    }
                }
            }
            /*else if (jobRequest.ErrorFlag.equalsIgnoreCase('false')){
                //no Offers in the a package.                
                apptDateWrapper.nextAvailableDate = 'No offers are available';
                apptDateWrapper.statusIndicator = 'Red';
            }*/
            else{
                apptDateWrapper.nextAvailableDate = String.isNotBlank(installApptSettings.Default_Error_Message__c)? installApptSettings.Default_Error_Message__c : 'Unable to retrieve install availability';
                apptDateWrapper.statusIndicator = 'Red';
            }
            //numberOfAppts
            //apptDateWrapper.numberOfAppts = avail>Integer.valueOf(installApptSettings.No_Of_Offers__c)?(String.valueOf(installApptSettings.No_Of_Offers__c.intvalue())+'+'):String.valueOf(avail);
            apptDateWrapper.numberOfAppts = (jobRequest.ReturnValue != null && jobRequest.ReturnValue.size() == Integer.valueOf(installApptSettings.No_Of_Offers__c))? String.valueOf(jobRequest.ReturnValue.size())+'+' : String.valueOf(avail);
            //Final wrapper
            apptDateWrapper.lstAdditionalInfoWrapper = lstAddApptWrapper;
            lstApptWrapper.add(apptDateWrapper);
        }
        system.debug('@@apptDateWrapper: '+lstApptWrapper);
        return lstApptWrapper;
    }
  }