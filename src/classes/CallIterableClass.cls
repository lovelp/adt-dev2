global class CallIterableClass implements iterable<SObject>{
   global Iterator<SObject> Iterator(){
      return new CustomIterableClass();
   }
}