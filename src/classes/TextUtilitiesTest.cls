@isTest
private class TextUtilitiesTest {
	
	static testMethod void testStringToCharArray() {
		
		List<Integer> input = new List<Integer>();
		// Create the string Coat
		input.add(67);
		input.add(111);
		input.add(97);
		input.add(116);
		
		String stringInput = String.fromCharArray(input);
		
		Test.startTest();
		
		List<Integer> output = TextUtilities.stringToCharArray(stringInput);
		
		String stringOutput = String.fromCharArray(output);
		
		
		Test.stopTest();
		
		System.assertEquals('Coat', stringOutput);
		
	}
	
	static testMethod void testStringToCharArrayNull() {
		
		Test.startTest();
		
		List<Integer> output = TextUtilities.stringToCharArray(null);
		
		
		
		Test.stopTest();
		
		System.assertEquals(0, output.size(), 'output should list of size 0');
		
	}
	
	static testMethod void testStringToCharArrayEmptyString() {
		
		Test.startTest();
		
		List<Integer> output = TextUtilities.stringToCharArray('');
		
		String stringOutput = String.fromCharArray(output);
		
		Test.stopTest();
		
		System.assertEquals('', stringOutput, 'output should be empty string');
		
	}
	
	static testMethod void testRemoveDiacriticalMarksNull() {
		
		Test.startTest();
		
		String stringOutput = TextUtilities.removeDiacriticalMarks(null);
		
		Test.stopTest();
		
		System.assert(stringOutput == null, 'output should be null');
		
	}
	
	static testMethod void testRemoveDiacriticalMarksEmptyString() {
		
		Test.startTest();
		
		String stringOutput = TextUtilities.removeDiacriticalMarks('');
		
		Test.stopTest();
		
		System.assertEquals('', stringOutput);
		
	}
	
	static testMethod void testRemoveDiacriticalMarksDropCharacter() {
		
		List<Integer> input = new List<Integer>();
		// Create the string Blvd <code 8206 which is out of allowable range>
		input.add(66);
		input.add(108);
		input.add(118);
		input.add(100);
		input.add(32);
		input.add(8206);
		
		String stringInput = String.fromCharArray(input);
		
		Test.startTest();
		
		String stringOutput = TextUtilities.removeDiacriticalMarks(stringInput);
		
		Test.stopTest();
		
		System.assertEquals('Blvd ', stringOutput);
		
	}
	
	
	static testMethod void testRemoveDiacriticalMarksLetterA() {
		
		List<Integer> input = new List<Integer>();
		// Create the string 
		input.add(192); // LATIN UPPERCASE LETTER A WITH GRAVE
		input.add(193); // LATIN UPPERCASE LETTER A WITH ACUTE
		input.add(194); // LATIN UPPERCASE LETTER A WITH CIRCUMFLEX
		input.add(195); // LATIN UPPERCASE LETTER A WITH TILDE
		input.add(196); // LATIN UPPERCASE LETTER A WITH DIAERESIS
		input.add(197); // LATIN UPPERCASE LETTER A WITH RING ABOVE
		input.add(224); // LATIN LOWERCASE LETTER A WITH GRAVE
		input.add(225); // LATIN LOWERCASE LETTER A WITH ACUTE
		input.add(226); // LATIN LOWERCASE LETTER A WITH CIRCUMFLEX
		input.add(227); // LATIN LOWERCASE LETTER A WITH TILDE
		input.add(228); // LATIN LOWERCASE LETTER A WITH DIAERESIS
		input.add(229); // LATIN LOWERCASE LETTER A WITH RING ABOVE
		
		String stringInput = String.fromCharArray(input);
		
		Test.startTest();
		
		String stringOutput = TextUtilities.removeDiacriticalMarks(stringInput);
		
		Test.stopTest();
		
		System.assertEquals('AAAAAAaaaaaa', stringOutput);
		
	}
	
	static testMethod void testRemoveDiacriticalMarksLetterC() {
		
		List<Integer> input = new List<Integer>();
		// Create the string 
		input.add(199); // LATIN UPPERCASE LETTER C WITH CEDILLA
		input.add(231); // LATIN LOWERCASE LETTER C WITH CEDILLA

		String stringInput = String.fromCharArray(input);
		
		Test.startTest();
		
		String stringOutput = TextUtilities.removeDiacriticalMarks(stringInput);
		
		Test.stopTest();
		
		System.assertEquals('Cc', stringOutput);
		
	}
	
	static testMethod void testRemoveDiacriticalMarksLetterE() {
		
		List<Integer> input = new List<Integer>();
		// Create the string 
		input.add(200); // LATIN UPPERCASE LETTER E WITH GRAVE
		input.add(201); // LATIN UPPERCASE LETTER E WITH ACUTE
		input.add(202); // LATIN UPPERCASE LETTER E WITH CIRCUMFLEX
		input.add(203); // LATIN UPPERCASE LETTER E WITH DIAERESIS
		input.add(232); // LATIN LOWERCASE LETTER E WITH GRAVE
		input.add(233); // LATIN LOWERCASE LETTER E WITH ACUTE
		input.add(234); // LATIN LOWERCASE LETTER E WITH CIRCUMFLEX
		input.add(235); // LATIN LOWERCASE LETTER E WITH DIAERESIS
		
		String stringInput = String.fromCharArray(input);
		
		Test.startTest();
		
		String stringOutput = TextUtilities.removeDiacriticalMarks(stringInput);
		
		Test.stopTest();
		
		System.assertEquals('EEEEeeee', stringOutput);
		
	}
	
	static testMethod void testRemoveDiacriticalMarksLetterI() {
		
		List<Integer> input = new List<Integer>();
		// Create the string 
		input.add(204); // LATIN UPPERCASE LETTER I WITH GRAVE
		input.add(205); // LATIN UPPERCASE LETTER I WITH ACUTE
		input.add(206); // LATIN UPPERCASE LETTER I WITH CIRCUMFLEX
		input.add(207); // LATIN UPPERCASE LETTER I WITH DIAERESIS
		input.add(236); // LATIN LOWERCASE LETTER I WITH GRAVE
		input.add(237); // LATIN LOWERCASE LETTER I WITH ACUTE
		input.add(238); // LATIN LOWERCASE LETTER I WITH CIRCUMFLEX
		input.add(239); // LATIN LOWERCASE LETTER I WITH DIAERESIS
		
		String stringInput = String.fromCharArray(input);
		
		Test.startTest();
		
		String stringOutput = TextUtilities.removeDiacriticalMarks(stringInput);
		
		Test.stopTest();
		
		System.assertEquals('IIIIiiii', stringOutput);
		
	}
	
	static testMethod void testRemoveDiacriticalMarksLetterN() {
		
		List<Integer> input = new List<Integer>();
		// Create the string 
		input.add(209); // LATIN UPPERCASE LETTER N WITH TILDE
		input.add(241); // LATIN LOWERCASE LETTER N WITH TILDE

		String stringInput = String.fromCharArray(input);
		
		Test.startTest();
		
		String stringOutput = TextUtilities.removeDiacriticalMarks(stringInput);
		
		Test.stopTest();
		
		System.assertEquals('Nn', stringOutput);
		
	}
	
	static testMethod void testRemoveDiacriticalMarksLetterO() {
		
		List<Integer> input = new List<Integer>();
		// Create the string 
		input.add(210); // LATIN UPPERCASE LETTER O WITH GRAVE
		input.add(211); // LATIN UPPERCASE LETTER O WITH ACUTE
		input.add(212); // LATIN UPPERCASE LETTER O WITH CIRCUMFLEX
		input.add(213); // LATIN UPPERCASE LETTER O WITH TILDE
		input.add(214); // LATIN UPPERCASE LETTER O WITH DIAERESIS
		input.add(242); // LATIN LOWERCASE LETTER O WITH GRAVE
		input.add(243); // LATIN LOWERCASE LETTER O WITH ACUTE
		input.add(244); // LATIN LOWERCASE LETTER O WITH CIRCUMFLEX
		input.add(245); // LATIN LOWERCASE LETTER O WITH TILDE
		input.add(246); // LATIN LOWERCASE LETTER O WITH DIAERESIS
		
		String stringInput = String.fromCharArray(input);
		
		Test.startTest();
		
		String stringOutput = TextUtilities.removeDiacriticalMarks(stringInput);
		
		Test.stopTest();
		
		System.assertEquals('OOOOOooooo', stringOutput);
		
	}
	
	static testMethod void testRemoveDiacriticalMarksLetterU() {
		
		List<Integer> input = new List<Integer>();
		// Create the string 
		input.add(217); // LATIN UPPERCASE LETTER U WITH GRAVE
		input.add(218); // LATIN UPPERCASE LETTER U WITH ACUTE
		input.add(219); // LATIN UPPERCASE LETTER U WITH CIRCUMFLEX
		input.add(220); // LATIN UPPERCASE LETTER U WITH DIAERESIS
		input.add(249); // LATIN UPPERCASE LETTER U WITH GRAVE
		input.add(250); // LATIN UPPERCASE LETTER U WITH ACUTE
		input.add(251); // LATIN UPPERCASE LETTER U WITH CIRCUMFLEX
		input.add(252); // LATIN UPPERCASE LETTER U WITH DIAERESIS
		
		String stringInput = String.fromCharArray(input);
		
		Test.startTest();
		
		String stringOutput = TextUtilities.removeDiacriticalMarks(stringInput);
		
		Test.stopTest();
		
		System.assertEquals('UUUUuuuu', stringOutput);
		
	}
	

}