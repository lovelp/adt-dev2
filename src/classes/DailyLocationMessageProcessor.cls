/************************************* MODIFICATION LOG ********************************************************************************************
* DailyLocationMessageProcessor
*
* DESCRIPTION : Extends LocationDataMessageProcessor and implements the interface specific to the daily history message.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					10/14/2011				- Original Version
*
*													
*/

public class DailyLocationMessageProcessor extends LocationDataMessageProcessor {
	
	private static final String CLASS_NAME = 'DailyLocationMessageProcessor';
	
	public override String getName() {
		return CLASS_NAME;
	}
	
	// No other overrides of parent class required at this time.
	// Class is retained for clarity within LocationDataFactory and for possible future use

}