/*
 * Description: This is a test class for the following classes
                1. AssignmentMonitor
 * Created By: Ravi Pochamalla
 *
 * ---------------------------------------------------------------------------Changes below--------------------------------------------------------------------------------
 * 
 *
 */
 
 
@isTest
private class AssignmentMonitorTest{
    public static void createTestData(){
        TestHelperClass.createReferenceDataForTestClasses();
    
    }
        private static testmethod void testAssignmentMonitor(){
        createTestData();
        test.startTest();
        AssignmentMonitor am=new AssignmentMonitor(); 
        String chron = '0 0 21 * * ?';        
        system.schedule('Test Assgn Monitor', chron, am);
        test.stopTest();
        }
}