/************************************* MODIFICATION LOG ********************************************************************************************
* PromoterHelper
*
* DESCRIPTION : Utility methods associated with the Promoter custom object
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner               3/5/2012           - Original Version
*
*                                                   
*/

public with sharing class PromoterHelper {
    
    // Finds and inactivates Promoters which were not updated by the most recent data load
    public static void inactivatePromoters() {
            
        // Promoter data loads contain all active promoters and the resulting upserts should modify
        // all records except those which are no longer active.
        Datetime rightNow = Datetime.now();
        Datetime inactivateBeforeDatetime = rightNow.addHours(Integer.valueOf(ResaleGlobalVariables__c.getinstance('PromoterInactivateOffset').value__c));

        // To confirm that the data load did occur, verify that at least one promoter record has been modified in the last 8 hours
        Promoter__c promoter;
        list<Promoter__c> plist = new list<Promoter__c>([select ID from Promoter__c where LastModifiedDate > : inactivateBeforeDatetime limit 1]);
        if (plist.size() > 0) {
            promoter = plist[0];
        }
        
        if (promoter != null) {

            // If so, find all promoters not modified in the past 8 hours
            List<Promoter__c> promoters = [select ID, Active__c from Promoter__c where Active__c = true AND LastModifiedDate <= :inactivateBeforeDatetime limit 10000];
        
            try {
                // And make each one inactive
                for (Promoter__c p: promoters) {
                    p.Active__c = false;
                }
                update promoters;
            } catch (Exception e) {
                System.debug('Unable to inactivate Promoters: ' + e.getMessage());
            }
        }   
        
    }
    
    public static void setOwnerIdFromEmployeeId(List<Promoter__c> newpromoters, map<id, Promoter__c> oldPromoters)
    {
        list<Promoter__c> promotersToProcess = new List<Promoter__c>();
        for(Promoter__c p : newPromoters)
        {
            if(p.EmployeeNumber__c != oldPromoters.get(p.id).EmployeeNumber__c)
            {
                promotersToProcess.add(p);
            }
        } 
        if(promotersToProcess.size() > 0)
        {
            setOwnerIdFromEmployeeId(promotersToProcess);
        }
    }   
    
    public static void setOwnerIdFromEmployeeId(List<Promoter__c> promoters)
    {
        Map<String, List<Promoter__c>> empPromoters = new Map<String, List<Promoter__c>>();
        Map<String, User> empUsers = new Map<String, User>();
        List<String> empIds = new List<String>();
        List<Promoter__c> assignToGlobalUser = new List<Promoter__c>();
        List<Promoter__c> tempPromoter;
        for(Promoter__c p : promoters)
        {
            if(p.EmployeeNumber__c != null && p.EmployeeNumber__c != '')
            {
                if(empPromoters.get(p.EmployeeNumber__c) == null)
                {
                    tempPromoter = new List<Promoter__c>();
                    tempPromoter.add(p);
                    empPromoters.put(p.EmployeeNumber__c, tempPromoter);
                }
                else
                {
                    tempPromoter = empPromoters.get(p.EmployeeNumber__c);
                    tempPromoter.add(p);
                    empPromoters.put(p.EmployeeNumber__c, tempPromoter);
                }
                empIds.add(p.EmployeeNumber__c);
            }
            else
            {
                //assign to global user
                assignToGlobalUser.add(p);
            }
        } 
        List<User> empUsersList = [select id, EmployeeNumber__c from User where EmployeeNumber__c in : empIds];
        for(User u : empUsersList)
        {
            empUsers.put(u.EmployeeNumber__c, u);
        }
        for(String empnum : empPromoters.keyset())
        {
            if(empUsers.get(empnum) == null)
            {
                //add all the records to global user since we couldnt find that user with emp num
                assignToGlobalUser.addAll(empPromoters.get(empnum));
            }
            else
            {
                for(Promoter__c p : empPromoters.get(empnum))
                {
                    p.OwnerId = empUsers.get(empnum).id;
                }
            }
        }
        //now set owner as global admin for rest of the records
        Id globalAdmin = Utilities.getGlobalUnassignedUser();
        for(Promoter__c p : assignToGlobalUser)
        {
            p.OwnerId = globalAdmin;
        }
    }


}