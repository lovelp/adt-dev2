@isTest
private class CannotEditDeleteTerritoryAssignmentsTest {
	
	
	static testMethod void testRedirectToErrorPage() {
		ApexPages.StandardController sc = new ApexPages.StandardController(new TerritoryAssignment__c());
		
		Test.startTest();
	
		CannotEditDeleteTerritoryAssignments cedta = new CannotEditDeleteTerritoryAssignments(sc);
	
		PageReference newRef = cedta.redirectToErrorPage();
		
		System.assert(newRef != null, 'Expect to be navigated');
	
		System.assert(newRef.getUrl().contains('NEDTA'), 'Expect to be navigated to error page with error NEDTA');
		
		Test.stopTest();
	}

}