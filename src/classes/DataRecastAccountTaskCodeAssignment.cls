/************************************* MODIFICATION LOG ********************************************************************************************
* DataRecastAccountTaskCodeAssignment
*
* DESCRIPTION : Update Task Code on Account from Events
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            5/29/2012				- Original Version
*
*													
*/

global class DataRecastAccountTaskCodeAssignment implements Database.Batchable<sObject>, Database.Stateful {

    private static RecordType standardAcctRecType = Utilities.getRecordType(RecordTypeName.STANDARD_ACCOUNT, 'Account');
    private static RecordType compGeneratedEveRecType = Utilities.getRecordType(RecordTypeName.COMPANY_GENERATED_APPOINTMENT, 'Event');
    private static RecordType selfGeneratedEveRecType = Utilities.getRecordType(RecordTypeName.SELF_GENERATED_APPOINTMENT, 'Event');
	public String query = 'SELECT Id, Name, TaskCode__c FROM Account WHERE RecordTypeId = \'' + standardAcctRecType.Id + '\' AND Business_Id__c != null';
    
	global Database.QueryLocator start( Database.Batchablecontext bc ) 
	{
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext bc, List<sObject> scope) 
	{		
		List<Account> UpdateAccountsList = new List<Account>();
		Map<Id, Account> AccountMap = new Map<Id, Account>();
		Map<Id,Event> EventMap = new Map<Id,Event>();
		
		for(Account a : (List<Account>)scope)
		{	
			a.TaskCode__c = null;
			AccountMap.put(a.Id, a);
		}
		
		for(Event eve : [Select WhatId, TaskCode__c, EndDateTime FROM Event WHERE TaskCode__c != null AND TaskCode__c != 'TRV' AND TaskCode__c != 'DLL' AND WhatId != null AND (RecordTypeId = :compGeneratedEveRecType.Id  OR RecordTypeId = :selfGeneratedEveRecType.Id )AND WhatId IN :AccountMap.keySet()])
		{
			Event tempEvent = EventMap.get(eve.WhatId);
			
			if(tempEvent != null && tempEvent.EndDateTime >= eve.EndDateTime)
			{
				Account acc  = AccountMap.get(tempEvent.WhatId);
				if(acc != null)
				{
					acc.TaskCode__c = tempEvent.TaskCode__c;
					AccountMap.put(acc.Id, acc);
				}				
			}
			else
			{
				Account acc  = AccountMap.get(eve.WhatId);
				if(acc != null)
				{
					acc.TaskCode__c = eve.TaskCode__c;
					AccountMap.put(acc.Id, acc);
				}				
			}
			EventMap.put(eve.WhatId, eve);			
		}
		
		UpdateAccountsList = AccountMap.values();
		
		if(UpdateAccountsList != null && UpdateAccountsList.size() > 0)
		{
			update UpdateAccountsList;	
		}			
		
	}
	
	global void finish(Database.BatchableContext bc) 
	{		
	}

}