global class AccountAssignmentToFieldScheduler implements Schedulable {
    global void execute(SchedulableContext SC) {  
        //AccountAssignmentToFieldBatchProcessor aa = new AccountAssignmentToFieldBatchProcessor();
        Database.executeBatch( new AccountAssignmentToFieldBatchProcessor());
    } 
}