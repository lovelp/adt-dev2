public class ClearTestLoadDataController {
    private ApexPages.StandardSetController standardController;
    
    public ClearTestLoadDataController(ApexPages.StandardSetController standardController)
    {
        this.standardController = standardController;
    }
    public pageReference callBatchClass(){
        Id batchJobId = Database.executeBatch(new DeleteRecordsBatchClass(), 1000);
        System.debug('The batch id is'+batchJobId);
        return new PageReference('/' + TestLoad__c.sObjectType.getDescribe().getKeyPrefix());
    }
}