@isTest
global class UpdateOpportunityAgreementCallout implements HttpCalloutMock {
    
global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('');
        response.setStatusCode(400);
        return response; 
    }
}