@isTest
global class CallLoanAPITest {
    static LoanApplication__c LoanApplication;
    static Account acct;
    static void createLoanDecisionData(Boolean isInsert){
		
		User current = [Select Id, Department from User where Id = :UserInfo.getUserId()];
		
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='LoanType';
        fc.value__c='test';
        Insert fc ;
        fc = new FlexFiConfig__c();
        fc.name='Phone Sales Department';
        fc.value__c = current.Department != null ? current.Department : 'test';
        Insert fc ;
        fc = new FlexFiConfig__c();
        fc.name='TransactionType';
        fc.value__c='test';
        Insert fc ;
        fc = new FlexFiConfig__c();
        fc.name='autoBookFlg';
        fc.value__c='test';
        Insert fc ;
        
        fc = new FlexFiConfig__c();
        fc.name='OFACCode';
        fc.value__c='test';
        Insert fc ;
        
        fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;
        
        FlexFiMessaging__c msg = new FlexFiMessaging__c(Name = 'LoanSubmissionFailed', ErrorMessage__c = 'Application Submission Failed');
        insert msg;
        
        FlexFiMessaging__c msg1 = new FlexFiMessaging__c(Name = 'LoanApplicationSubmitted', ErrorMessage__c = 'Application Submitted');
        insert msg1;
        
        FlexFiMessaging__c msg2 = new FlexFiMessaging__c(Name = 'APPROVED:a', ErrorMessage__c = 'Application Approved');
        insert msg2;
        
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'DecessionApplicationSubmitted', ErrorMessage__c = 'Loan Decision Application Submitted');
        insert msg3;
        
        FlexFiMessaging__c msg4 = new FlexFiMessaging__c(Name = 'DecessionApplicationSubmittedAPPROVED', ErrorMessage__c = 'Loan Application Submitted & Approved');
        insert msg4;
        
        FlexFiMessaging__c msg5 = new FlexFiMessaging__c(Name = 'DecisionApplicationSubmitted', ErrorMessage__c = 'Loan Application Submitted & Approved');
        insert msg5;
        
       
        
      
        //Create Account
        //User u; 
        
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '4';
        insert resale;
        ResaleGlobalVariables__c InteUser = new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User');
        Insert     InteUser;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name='RIFAccountOwnerAlias'; 
        rgv.value__c='ADT RIF';
        insert rgv;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.CountryCode__c = 'US';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        acct = new Account(Name = 'Unit Test Account 2');
        acct.AddressID__c = addr.Id;
        
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';
        if(isInsert){
        	acct.FirstName__c='testFirstName';
        	acct.LastName__c='testLastName';
        	acct.Email__c = 'abc@gmail.com';
        	acct.DOB_encrypted__c='11/11/1973';
	        acct.Income__c = '1000';
	        acct.Phone = '14785894';
	        insert acct;
	        
	        Equifax_Log__c elog = new Equifax_Log__c(AccountID__c = acct.Id);
	        elog.TransactionID__c = 'T001';
        	insert elog;
        }
        
        LoanApplication = new LoanApplication__c();
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c =DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.PreviousAddressLine1__c ='Prev Add Line 1';
        LoanApplication.PreviousAddressLine2__c ='Prev Add Line 2';
        LoanApplication.PreviousAddressPostalCd__c ='27518';
        LoanApplication.PreviousAddressStateCd__c ='NC';
        LoanApplication.PreviousAddressCountryCd__c='US';
       // LoanApplication.eConsentAccepted__c=true;
        LoanApplication.PreviousAddressLine1__c = '8952 Brook Rd';
        LoanApplication.PreviousAddressCity__c = 'McLean';
        LoanApplication.PreviousAddressStateCd__c = 'VA';
        LoanApplication.PreviousAddressPostalCd__c = '221o2';
        LoanApplication.PreviousAddressCountryCd__c = 'US';
        LoanApplication.eConsentAcceptedDateTime__c = system.now();
        if(isInsert){
        	LoanApplication.Account__c = acct.id;
        	LoanApplication.PaymentechProfileId__c = '123';
            LoanApplication.FirstName__c='testFirstName';
            LoanApplication.LastName__c='testLastName';
            LoanApplication.DOB__c='11/11/1973';
            LoanApplication.SSN__c='123456789';
            LoanApplication.AnnualIncome__c=1000;
            LoanApplication.BillingAddress__c =addr.id;
        	LoanApplication.ShippingAddress__c =addr.id;
            insert LoanApplication;
        }
    }
     @isTest static void getLoanDecisionRequesttestCallout() {
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootLoanDecisionEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 2;
        is.EncryptedDPKey__c = 'XPdDdNPH129hYhWf1WfYmLuLu9EiMj++c3JiBLXVYS/FliAOwIMo0PfscAGceQ1TREYoMZ1MD3wWWg7ZUJW0bQ==';
        is.KeytoDecrypt__c = 'uHEb3tj9C1BJ5jqVXq3ExfVWRBQQxFJZNNM9l6fbsIo=';
        //is.financePlanType__C='PLAN';
        insert is;
        
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;
        
        
        
        createLoanDecisionData(true);
        
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanDecisionResponse());
        //CallLoanAPI.getLoanDecisionRequest(LoanApplication.id,'222.222.22.22');
        CallLoanAPI.getLoanDecisionRequest(LoanApplication.id,'2342', '123456789');
        LoanApplication__c loanApp = [Select Id, LoanApplicationId__c,
                                         SubmitLoanTerms_ConditionAcceptedDate__c, Account__c,
                                        Account__r.FirstName__c, Account__r.LastName__c, Account__r.DOB_encrypted__c, Account__r.Profile_TaxID__c ,
                                        LoanDecisionResponse__c,SubmittedDateTime__c,SubmittedUser__c,SubmittedUserDepartment__c,Account__r.OFACRestriction__c,Account__r.AddressID__c
                                        From LoanApplication__c limit 1];
                                        
        Account acc = [select Id,FirstName__c,LastName__c,DOB_encrypted__c,Email__c from account where Id =:loanApp.account__c ];                                
        system.debug('=============LoanApplication__c.LoanDecisionResponse__c===:'+loanApp);
        
        List<LoanApplication__c> loanAppDataList = new List<LoanApplication__c> ();
        loanAppDataList.add(LoanApplication);
        Address__c addr = [select id FROM Address__c where ID=: loanApp.Account__r.AddressID__c];
        CallLoanAPI.getLoanDecisionRequest(LoanApplication.id, '998','123456789',loanAppDataList,addr);
        
        CallLoanAPI.getLoanDecisionRequest(LoanApplication.id, '998','123456789',loanAppDataList,addr);
        
        LoanProgramMessageJSON.LoanDecisionRequestMessage dd = new LoanProgramMessageJSON.LoanDecisionRequestMessage();
        dd.citizensLoanDecisionRequest=null;
        
        LoanProgramMessageJSON.PersonalInformation PI = new LoanProgramMessageJSON.PersonalInformation(Acc);
        LoanProgramMessageJSON.UpdateAppRequestMessage dd1 = new LoanProgramMessageJSON.UpdateAppRequestMessage();
        dd1.CitizensUpdateAppRequest=null;
        LoanProgramMessageJSON.CustomerApplicationValidation PI1 = new LoanProgramMessageJSON.CustomerApplicationValidation(Acc);
         
       // PI.PersonalInformation();
        Test.stopTest();
    }
    
    @isTest static void getLoanDecisionRequesttestCallout1() {
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234'; 
        is.ZootLoanDecisionEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 1;
        is.EncryptedDPKey__c = 'XPdDdNPH129hYhWf1WfYmLuLu9EiMj++c3JiBLXVYS/FliAOwIMo0PfscAGceQ1TREYoMZ1MD3wWWg7ZUJW0bQ==';
        is.KeytoDecrypt__c = 'uHEb3tj9C1BJ5jqVXq3ExfVWRBQQxFJZNNM9l6fbsIo=';
        // is.financePlanType__C='PLAN';
        insert is;
        
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;
        
        createLoanDecisionData(true);
        
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanDecisionResponse());
        //CallLoanAPI.getLoanDecisionRequest(LoanApplication.id,'222.222.22.22');
        CallLoanAPI.getLoanDecisionRequest(LoanApplication.id,'434', '123456789');
        LoanApplication__c loanApp = [Select Id, LoanApplicationId__c,
                                         SubmitLoanTerms_ConditionAcceptedDate__c, 
                                        Account__r.FirstName__c, Account__r.LastName__c, Account__r.DOB_encrypted__c, Account__r.Profile_TaxID__c ,
                                        LoanDecisionResponse__c,SubmittedDateTime__c,SubmittedUser__c,SubmittedUserDepartment__c,Account__r.OFACRestriction__c
                                        From LoanApplication__c limit 1];
        system.debug('=============LoanApplication__c.LoanDecisionResponse__c===:'+loanApp);
        is.ZootAPIRetryCounter__c = 1;
        update is;
        CallLoanAPI.getLoanDecisionRequest(LoanApplication.id,'333', '123456789');
        Test.stopTest();
    }
    
    @isTest static void getLoanDecisionRequesttestCallout2() {
        
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootLoanDecisionEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 1;
        is.EncryptedDPKey__c = 'XPdDdNPH129hYhWf1WfYmLuLu9EiMj++c3JiBLXVYS/FliAOwIMo0PfscAGceQ1TREYoMZ1MD3wWWg7ZUJW0bQ==';
        is.KeytoDecrypt__c = 'uHEb3tj9C1BJ5jqVXq3ExfVWRBQQxFJZNNM9l6fbsIo=';
        // is.financePlanType__C='PLAN';
        insert is;
        
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;

        createLoanDecisionData(false);
        insert acct;
        LoanApplication.Account__c = acct.id;
        insert LoanApplication;
        
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class,  new MockHttpLoanErrorResponse());
        CallLoanAPI.getLoanDecisionRequest(LoanApplication.id,'999', '123456789');
        
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
    	acct.Email__c = 'abc@gmail.com';
    	acct.DOB_encrypted__c='11/11/1973';
        acct.Income__c = '1000';
        acct.Phone = '14785894';
        update acct;
	        
        LoanApplication.FirstName__c='testFirstName';
        LoanApplication.LastName__c='testLastName';
        LoanApplication.DOB__c='11/11/1973';
        LoanApplication.SSN__c='123456789';
        LoanApplication.AnnualIncome__c=1000;
        update LoanApplication;
        
        CallLoanAPI.getLoanDecisionRequest(LoanApplication.id,'999', '123456789');
        
        Test.stopTest();
    }
    
    public static void createLoanAcceptData(Boolean isInsert){
        //Create Account
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        FlexFiMessaging__c msg = new FlexFiMessaging__c(Name = 'LoanAcceptanceFailed', ErrorMessage__c = 'Application Acceptance Failed');
        insert msg;
        
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'AcceptApplication', ErrorMessage__c = 'Application Accepted');
        insert msg3;
        
        FlexFiMessaging__c msg2 = new FlexFiMessaging__c(Name = 'APPROVED:a', ErrorMessage__c = 'Application Approved');
        insert msg2;
        FlexFiMessaging__c msg5 = new FlexFiMessaging__c(Name = 'DecisionApplicationSubmitted', ErrorMessage__c = 'Loan Application Submitted & Approved');
        insert msg5;
        
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;
        
        
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '4';
        insert resale;
        ResaleGlobalVariables__c InteUser = new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User');
        Insert     InteUser;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name='RIFAccountOwnerAlias';
        rgv.value__c='ADT RIF';
        insert rgv;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'US';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
        //Create Account
        acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'Unit Test Account 2';
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';
        acct.Email__c = 'abc@gmail.com';
        acct.DOB_encrypted__c='11/11/1973';
        acct.Income__c = '1000';
        acct.Phone = '14785894';
        insert acct;
        
        LoanApplication = new LoanApplication__c();
        LoanApplication.Account__c = acct.id;
        LoanApplication.FirstName__c='testFirstName';
        LoanApplication.LastName__c='testLastName';
        LoanApplication.DOB__c='11/11/1973';
        LoanApplication.BillingAddress__c =addr.id;
        LoanApplication.ShippingAddress__c =addr.id;
        LoanApplication.eConsentAcceptedDateTime__c = system.now();
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c =DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.TilaPnoteAcceptedDateTime__c = system.now();
        if(isInsert){
        	LoanApplication.SSN__c='123456789';
        	LoanApplication.LoanApplicationId__c = '123';
        	insert LoanApplication;
        }
    }
    
     @isTest static void getAcceptLoanRequesttestCallout() {
        
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootLoanAcceptLoanEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 2;
        // is.financePlanType__C='PLAN';
        insert is;
        
        FlexFiConfig__c ofcSetting = new FlexFiConfig__c();
        ofcSetting.name='OFACCode';
        ofcSetting.value__c='test';
        Insert ofcSetting ;
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;
        
        
        createLoanAcceptData(true);
        
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanAcceptResponse());
        CallLoanAPI.callLoanAcceptMethod(LoanApplication.id);
        Test.stopTest();
    }
    
    @isTest static void getAcceptLoanRequesttestCallout1() {
        
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootLoanAcceptLoanEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 1;
        // is.financePlanType__C='PLAN';
        insert is;
        
        FlexFiConfig__c ofcSetting = new FlexFiConfig__c();
        ofcSetting.name='OFACCode';
        ofcSetting.value__c='test';
        Insert ofcSetting ;
        
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;

        createLoanAcceptData(true);
        
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanAcceptResponse());
        CallLoanAPI.callLoanAcceptMethod(LoanApplication.id);
        
        is.ZootAPIRetryCounter__c = 1;
        update is;
        CallLoanAPI.callLoanAcceptMethod(LoanApplication.id);
        Test.stopTest();
    }
    
    @isTest static void getAcceptLoanRequesttestCallout2() {
        
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootLoanAcceptLoanEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 1;
        // is.financePlanType__C='PLAN';
        insert is;
        
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;

        createLoanAcceptData(false);
    	insert LoanApplication;
        
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class,  new MockHttpLoanErrorResponse());
        CallLoanAPI.callLoanAcceptMethod(LoanApplication.id);
        
        Test.stopTest();
        
        LoanApplication.SSN__c='123456789';
    	update LoanApplication;
    	
    	CallLoanAPI.callLoanAcceptMethod(LoanApplication.id);
        
        LoanApplication.LoanApplicationId__c = '123';
    	update LoanApplication;
        CallLoanAPI.callLoanAcceptMethod(LoanApplication.id);
    }
    
    //Loan UpDate 
    @isTest static void getUpdateLoanRequesttestCallout2() {

        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootLoanAcceptLoanEndPoint__C = 'testurl';
        is.ZootUpdateAppEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 2;
         //is.financePlanType__C='PLAN';
        insert is;
        
        FlexFiMessaging__c msg = new FlexFiMessaging__c(Name = 'LoanUpdateFailed', ErrorMessage__c = 'Application Update Failed');
        insert msg;
        
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'UpdateApplication', ErrorMessage__c = 'Application Updated');
        insert msg3;
       FlexFiMessaging__c msg5 = new FlexFiMessaging__c(Name = 'DecisionApplicationSubmitted', ErrorMessage__c = 'Loan Application Submitted & Approved');
        insert msg5;
        
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;
        //Create Account
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '4';
        insert resale;
        ResaleGlobalVariables__c InteUser = new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User');
        Insert     InteUser;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name='RIFAccountOwnerAlias';
        rgv.value__c='ADT RIF';
        insert rgv;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'Unit Test Account 2';
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';
        acct.Email__c = 'abc@gmail.com';
        acct.MMBSiteNumber__c='MMBSTNumber';
        acct.MMBCustomerNumber__c='MMBCusTestNum';
        acct.TelemarAccountNumber__c = 'TAN';
        acct.DOB_encrypted__c='11/11/1973';
        acct.Income__c = '1000';
        acct.Phone = '14785894';
        insert acct;
        
        LoanApplication__c LoanApplication = new LoanApplication__c();
        LoanApplication.Account__c = acct.id;
        //LoanApplication.Active__c = true;
        LoanApplication.BillingAddress__c =addr.id;
        LoanApplication.ShippingAddress__c =addr.id;
        LoanApplication.InterestRate__c = 1.0;
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c =DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.LastZootUpdatedDateTime__c = system.today();
        LoanApplication.SubmittedDateTime__c = system.today();
        LoanApplication.SSN__c='123456789';
        insert LoanApplication;
        
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanUpdateReq());
        ADTLoanApplicationSchema.LoanApplicationUpdateRequest reqJSON = new ADTLoanApplicationSchema.LoanApplicationUpdateRequest();
        reqJSON.customerNumber='MMBCusTestNum';
        reqJSON.siteNumber='MMBSTNumber';
        reqJSON.leadManagementId = 'TAN';
        reqJSON.term=5;
        reqJSON.amtFinanced=2000;
        reqJSON.salesTaxAmt=3;
        reqJSON.purchasePriceAmt=2;
        
        CallLoanAPI.updateLoanApplication(reqJSON);
        
        Test.stopTest();
    }
    
     @isTest static void getUpdateLoanRequesttestCallout() {
        
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';        
        is.ZootUpdateAppEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 2;
        //is.financePlanType__C='PLAN';
        insert is;
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='LoanType';
        fc.value__c='test';
        Insert fc ;
        
        fc = new FlexFiConfig__c();
        fc.name='Phone Sales Department';
        fc.value__c='test';
        Insert fc ;
        
        fc = new FlexFiConfig__c();
        fc.name='TransactionType';
        fc.value__c='test';
        Insert fc ;
        fc = new FlexFiConfig__c();
        fc.name='autoBookFlg';
        fc.value__c='test';
        Insert fc ;
        fc = new FlexFiConfig__c();
        fc.name='OFACCode';
        fc.value__c='test';
        Insert fc ;
        
        fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;
        
        
        //Create Account
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '4';
        insert resale;
        ResaleGlobalVariables__c InteUser = new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User');
        Insert     InteUser;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name='RIFAccountOwnerAlias';
        rgv.value__c='ADT RIF';
        insert rgv;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;

        acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'Unit Test Account 2';
        
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';
        acct.Email__c = 'abc@gmail.com';
        acct.MMBSiteNumber__c='MMBSTNumber';
        acct.MMBCustomerNumber__c='MMBCusTestNum';
        
        acct.DOB_encrypted__c='11/11/1973';
        acct.Income__c = '1000';
        acct.Phone = '14785894';
        insert acct;
        
        LoanApplication__c LoanApplication = new LoanApplication__c();
        LoanApplication.Account__c = acct.id;
        //LoanApplication.Active__c = true;
        LoanApplication.BillingAddress__c =addr.id;
        LoanApplication.ShippingAddress__c =addr.id;
        LoanApplication.PreviousAddressLine1__c ='Prev Add Line 1';
        LoanApplication.PreviousAddressLine2__c ='Prev Add Line 2';
        LoanApplication.PreviousAddressPostalCd__c ='27518';
        LoanApplication.PreviousAddressStateCd__c ='NC';
        LoanApplication.PreviousAddressCountryCd__c='US';
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c =DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.InterestRate__c = 1.0;
        LoanApplication.SSN__c='123456789';
        LoanApplication.DOB__c='11/11/1973';
        Insert LoanApplication;
        
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanUpdateReq());
        ADTLoanApplicationSchema.LoanApplicationUpdateRequest reqJSON = new ADTLoanApplicationSchema.LoanApplicationUpdateRequest();
        reqJSON.customerNumber='MMBCusTestNum';
        reqJSON.siteNumber='MMBSTNumber';
        reqJSON.term=5;
        reqJSON.amtFinanced=2000;
        reqJSON.salesTaxAmt=3;
        reqJSON.purchasePriceAmt=2;
        reqJSON.jobNumber='222';
        reqJSON.orderNumber='222';
        
       
       //CallLoanAPI.updateLoanApplication(reqJSON);
       system.debug('---------------LoanApplication.id'+LoanApplication.id);
       system.debug('---------------reqJSON'+reqJSON);
        /*HttpResponse res = CallLoanAPI.getUpdateLoanRequest1(LoanApplication.id,reqJSON);
        String contentType = res.getHeader('Content-Type');        
        String actualValue = res.getBody();        
        LoanProgramMessageJSON.CitizensUpdateAppResponse_Z result = (LoanProgramMessageJSON.CitizensUpdateAppResponse_Z)System.JSON.deserialize(res.getBody(),LoanProgramMessageJSON.CitizensUpdateAppResponse_Z.class);
        system.debug('================ UpdateLoanRequest==JSONResponse===:'+res.getBody());
        */
        Test.stopTest();
    }
    
    public static void createUpdateLoanData(Boolean isInsert){
        //Create Account
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        
        FlexFiMessaging__c msg = new FlexFiMessaging__c(Name = 'LoanUpdateFailed', ErrorMessage__c = 'Application Update Failed');
        insert msg;
        
        FlexFiMessaging__c msg3 = new FlexFiMessaging__c(Name = 'UpdateApplication', ErrorMessage__c = 'Application Updated');
        insert msg3;
        
        FlexFiMessaging__c msg2 = new FlexFiMessaging__c(Name = 'APPROVED:a', ErrorMessage__c = 'Application Approved');
        insert msg2;
        
        FlexFiMessaging__c msg5 = new FlexFiMessaging__c(Name = 'DecisionApplicationSubmitted', ErrorMessage__c = 'Loan Application Submitted & Approved');
        insert msg5;
        
        FlexFiConfig__c fc = new FlexFiConfig__c();
        fc.name='FinancePlanType';
        fc.value__c='PLAN';
        Insert fc ;
        
        
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c resale = new ResaleGlobalVariables__c();
        resale.name = 'USAAAttempts';
        resale.value__c = '4';
        insert resale;
        ResaleGlobalVariables__c InteUser = new ResaleGlobalVariables__c (name = 'IntegrationUser',value__c = 'Integration User');
        Insert     InteUser;
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.Name='RIFAccountOwnerAlias';
        rgv.value__c='ADT RIF';
        insert rgv;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;

        acct = new Account();
        acct.AddressID__c = addr.Id;
        acct.Name = 'Unit Test Account 2';
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '22o39';
        acct.Email__c = 'abc@gmail.com';
        acct.MMBSiteNumber__c='MMBSTNumber';
        acct.MMBCustomerNumber__c='MMBCusTestNum';
        
        acct.DOB_encrypted__c='11/11/1973';
        acct.Income__c = '1000';
        acct.Phone = '14785894';
        insert acct;
        
        LoanApplication = new LoanApplication__c();
        LoanApplication.Account__c = acct.id;
        LoanApplication.BillingAddress__c =addr.id;
        LoanApplication.ShippingAddress__c =addr.id;
        LoanApplication.SubmitLoanTerms_ConditionAcceptedDate__c =DateTime.parse('12/05/2018 11:46 AM');
        LoanApplication.LoanApplicationId__c = '123';
        LoanApplication.DOB__c='11/11/1973';
        LoanApplication.installmentAmt__c = 100.0;
        LoanApplication.InitialPaymentAmt__c = 100.0;
        if(isInsert){
        	LoanApplication.InterestRate__c = 1.0;
        	LoanApplication.SSN__c='123456789';
        	insert LoanApplication;
        }
      // CallLoanAPI.updateOFAC(acct.id,False);
      CallLoanAPI.updateOFAC(acct.id);
    }
    //Future
     @isTest static void getUpdateLoanRequesttestCalloutFuture() {
        
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootUpdateAppEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 2;
        
        insert is;
        
        createUpdateLoanData(true);
        
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanUpdateReq());
        ADTLoanApplicationSchema.LoanApplicationUpdateRequest reqJSON = new ADTLoanApplicationSchema.LoanApplicationUpdateRequest();
        reqJSON.customerNumber='MMBCusTestNum';
        reqJSON.siteNumber='MMBSTNumber';
        reqJSON.term=5;
        reqJSON.amtFinanced=2000;
        reqJSON.salesTaxAmt=3;
        reqJSON.purchasePriceAmt=2;
        reqJSON.jobNumber='222';
        reqJSON.orderNumber='222';
        
         map<String,String> reqJsonMap  = new Map<String,String>();
         reqJsonMap.put('customerNumber',reqJSON.customerNumber);
         reqJsonMap.put('jobNumber',reqJSON.jobNumber);
         reqJsonMap.put('siteNumber',reqJSON.siteNumber);
         reqJsonMap.put('term',String.valueOf(reqJSON.term));
         reqJsonMap.put('amtFinanced',String.valueOf(reqJSON.amtFinanced));
         reqJsonMap.put('salesTaxAmt',String.valueOf(reqJSON.salesTaxAmt));
         reqJsonMap.put('purchasePriceAmt',String.valueOf(reqJSON.purchasePriceAmt));
         
        
        LoanApplication.SubmittedDateTime__c = system.now();
        LoanApplication.LastZootUpdatedDateTime__c = system.now();
        update LoanApplication;
        CallLoanAPI.getUpdateLoanRequest(LoanApplication.id,reqJsonMap,'ss');
        Test.stopTest();
    }
    
    @isTest static void getUpdateLoanRequesttestCalloutFuture2() {
        
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootUpdateAppEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 2;
       
         //is.financePlanType__C='PLAN';
        insert is;
        
        createUpdateLoanData(false);
        insert LoanApplication;
        
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanUpdateReq());
        ADTLoanApplicationSchema.LoanApplicationUpdateRequest reqJSON = new ADTLoanApplicationSchema.LoanApplicationUpdateRequest();
        reqJSON.customerNumber='MMBCusTestNum';
        reqJSON.siteNumber='MMBSTNumber';
        reqJSON.term=5;
        reqJSON.amtFinanced=2000;
        reqJSON.salesTaxAmt=3;
        reqJSON.purchasePriceAmt=2;
        reqJSON.jobNumber='222';
        reqJSON.orderNumber='222';
        
         map<String,String> reqJsonMap  = new Map<String,String>();
         reqJsonMap.put('jobNumber',reqJSON.jobNumber);
         reqJsonMap.put('customerNumber',reqJSON.customerNumber);
         reqJsonMap.put('siteNumber',reqJSON.siteNumber);
         reqJsonMap.put('term',String.valueOf(reqJSON.term));
         reqJsonMap.put('amtFinanced',String.valueOf(reqJSON.amtFinanced));
         reqJsonMap.put('salesTaxAmt',String.valueOf(reqJSON.salesTaxAmt));
         reqJsonMap.put('purchasePriceAmt',String.valueOf(reqJSON.purchasePriceAmt));
        
         LoanApplication.SubmittedDateTime__c = system.now();
        LoanApplication.LastZootUpdatedDateTime__c = system.now();
        update LoanApplication; 
        
         CallLoanAPI.getUpdateLoanRequest(LoanApplication.id,reqJsonMap,'ee');
        Test.stopTest();
    }
    
    @isTest static void getUpdateLoanRequesttestCalloutFuture1() {
        
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.ZootUpdateAppEndPoint__C = 'testurl';
        is.ZootAPICalloutTimeOut__c=10000;
        is.ZootAPIRetryCounter__c = 1;
       // is.financePlanType__C='PLAN';
        insert is;
        
        createUpdateLoanData(true);
        
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpLoanUpdateReq());
        ADTLoanApplicationSchema.LoanApplicationUpdateRequest reqJSON = new ADTLoanApplicationSchema.LoanApplicationUpdateRequest();
        reqJSON.customerNumber='MMBCusTestNum';
        reqJSON.siteNumber='MMBSTNumber';
        reqJSON.term=5;
        reqJSON.amtFinanced=2000;
        reqJSON.salesTaxAmt=3;
        reqJSON.purchasePriceAmt=2;
        reqJSON.jobNumber='222';
        reqJSON.orderNumber='222';
        
         map<String,String> reqJsonMap  = new Map<String,String>();
         reqJsonMap.put('jobNumber',reqJSON.jobNumber);
         reqJsonMap.put('customerNumber',reqJSON.customerNumber);
         reqJsonMap.put('siteNumber',reqJSON.siteNumber);
         reqJsonMap.put('term',String.valueOf(reqJSON.term));
         reqJsonMap.put('amtFinanced',String.valueOf(reqJSON.amtFinanced));
         reqJsonMap.put('salesTaxAmt',String.valueOf(reqJSON.salesTaxAmt));
         reqJsonMap.put('purchasePriceAmt',String.valueOf(reqJSON.purchasePriceAmt));
         
          LoanApplication.SubmittedDateTime__c = system.now();
        LoanApplication.LastZootUpdatedDateTime__c = system.now();
        update LoanApplication;
        
         CallLoanAPI.getUpdateLoanRequest(LoanApplication.id,reqJsonMap,'ee');
         
         is.ZootAPIRetryCounter__c = 1;
         update is;
         Test.setMock(HttpCalloutMock.class,  new MockHttpLoanErrorResponse());
         CallLoanAPI.getUpdateLoanRequest(LoanApplication.id, reqJsonMap,'ee');
         
         Test.stopTest();
    }
    
    //@isTest
    global class MockHttpLoanErrorResponse implements HttpCalloutMock {
        // Implement this interface method
        global HTTPResponse respond(HTTPRequest req) {
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Resp =  '{"errors":[{"status":"401","errorCode":"-2","errorMessage":"Authorization failed"}]}';
            res.setBody(Resp);
            res.setStatusCode(401);
            return res;
        }
    }
    
    
    
    
}