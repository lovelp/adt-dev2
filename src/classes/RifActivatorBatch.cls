/************************************* MODIFICATION LOG ********************************************************************************************
* RifActivatorBatch
*
* DESCRIPTION : This batch shifts RIF accounts from Archived status to Active status after a configured number of days
*               has passed since the account was created.
*               Once Active, such accounts can be viewed via Locator.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner             		10/17/2012			- Original Version
*
*													
*/

global class RifActivatorBatch implements Database.batchable<sObject> {
	
	//incoming query
	global String query;
	
	//Constructor - set the local variable with the query string
	global RifActivatorBatch() {
		Integer days = Integer.valueof( ResaleGlobalVariables__c.getinstance('DaysBeforeRIFVisibility').value__c );
		
		this.query = 'select id, LeadStatus__c from Account where Type = \'' + IntegrationConstants.TYPE_RIF + 
		             '\' and LeadStatus__c = \'Archived\' and ArchivedReason__c = \'' + IntegrationConstants.ARCHIVED_REASON_IN_SERVICE +
		             '\' and RecordType.DeveloperName = \'' + RecordTypeDevName.RIF_ACCOUNT + 
		             '\' and CreatedDate < LAST_N_DAYS:'+ days + ' limit 10000';

	}
	
	//get Querylocator with the specitied query
	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext bc, List<sObject> accounts) {
		List<Account> updatedAccounts = new List<Account>();
		for(sObject s : accounts)
		{
			Account a = (Account)s;
			a.LeadStatus__c = 'Active';
			updatedAccounts.add(a);
		}
		//update updatedAccounts;
		Database.Saveresult[] SRs = database.update(updatedAccounts, false);
		List<Account> reUpdates = new List<Account>();
		Integer counter = 0;
		for(Database.SaveResult sr:SRs)
		{
			if(!sr.isSuccess())
			{
				reUpdates.add(updatedAccounts[counter]); 
			}
			counter++;
		}
		
		if(reUpdates.size() > 0)
		{
			Database.Saveresult[] nSRs = database.update(reUpdates, false);
			for(Database.SaveResult nsr:nSRs)
			{
				if(!nsr.isSuccess())
				{
					//we can do something here if necessary
				}
			}				
		}
	}
	
	global void finish(Database.BatchableContext bc) {

	}
	

}