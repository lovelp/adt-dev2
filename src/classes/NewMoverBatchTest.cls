/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class NewMoverBatchTest {

    static testMethod void testNewMoverBatch() {
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 0;
        addr.Longitude__c = 0;
        addr.Street__c = '8280 Greensboro Dr';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.OriginalAddress__c = '8280 Greensboro Dr, McLean, VA 221o2';
        
        insert addr;
        
        //set up fake accounts      
        List<Account> fake = new List<Account>();
        
        Account a1 = new Account();
        a1.Name = 'Test';
        a1.NewMover__c = true;
        a1.AddressID__c = addr.Id;
        a1.Business_Id__c = '1100 - Residential';
        a1.CreatedDate = system.today().addDays(-95);
        a1.LastModifiedDate = system.today().addDays(-95);
        fake.add(a1);
        
        Account a2 = new Account();
        a2.Name = 'Test';
        a2.NewMover__c = true;
        a2.Business_Id__c = '1100 - Residential';
        a2.AddressID__c = addr.Id;
        fake.add(a2);
        
        insert fake;
        
        // NewMoverFeedDate is set within trigger with system.today() and NOT using either CreatedDate or LastModifiedDate
        Account fixme = [select Id, NewMover__c, NewMoverFeedDate__c from Account where Id = :a1.Id];
        fixme.NewMoverFeedDate__c = system.today().addDays(-95);
        update fixme;

        test.startTest();
            NewMoverBatch nmb = new NewMoverBatch();
            nmb.query += ' and ID IN (\'' + a1.Id + '\',\'' + a2.Id + '\') limit 200';
            Database.executeBatch(nmb);
        test.stopTest();
        
        List<Account> confirm = [select Id from Account where Id IN :fake and NewMover__c = false];
        system.assertEquals(1,confirm.size());
    }
    
    static testmethod void testNewMoverScheduler() {
        // CRON expression: midnight on March 15.
        // Because this is a test, job executes
        // immediately after Test.stopTest().
        Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        // Schedule the test job
        String jobId = System.schedule('NewMoverScheduler',CRON_EXP, new NewMoverScheduler());
        Test.stopTest();
    }
}