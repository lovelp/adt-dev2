public class ADTTestData{
    public list<PostalWrapper> zips;
    public list<TownWrapper> towns;
    public list<CallDispoWrapper> callDispos;
    public list<APNVDNWrapper> apnvdns;
    public list<LeadConvertWrapper> lConvs;
    public list<DNISWrapper> dnis;
    public list<AffiliateWrapper> affiliates;
    
    // Wrapper class for postal code
    public class PostalWrapper{
     
    }
    
    // Wrapper class for towns
    public class TownWrapper{
     
    }
    
    // Wrapper class for call disposition
    public class CallDispoWrapper{
     
    }
    
    // Wrapper class for apn vdn
    public class APNVDNWrapper{
     
    }
    
    // Wrapper class for lead convert
    public class LeadConvertWrapper{
     
    }
    
    // Wrapper class for dnis
    public class DNISWrapper{
     
    }
    
    // Wrapper class for affiliates
    public class AffiliateWrapper{
    	public String name;
    	public Boolean Active;
    	public Boolean Builder;
    	public Integer Relevance;
    	public Boolean Member_Collected;
    	public Boolean Member_Required;
    	public Boolean Member_validated_Field_Sales;
    	public Boolean Member_Validated;
    }
    
    // Constructor
    public ADTTestData(){
        /* StaticResource static_resource = [SELECT Id,SystemModStamp,Name FROM StaticResource WHERE Name = 'ADTTestData' LIMIT 1];
		String url_file_ref = '/resource/' + String.valueOf(((DateTime)static_resource.get('SystemModStamp')).getTime()) + '/' + static_resource.get('Name');
		 Get the static resource for postal code object
		PageReference zips_pgRef = new PageReference(url_file_ref + '/zips.json');
		String zips_json_body = '';
		if(zips_pgRef != null)
			String zips_json_body = zips_pgRef.getContent().toString();
		
		// Get the static resource for affiliate object
		PageReference aff_pgRef = new PageReference(url_file_ref + '/affiliateData.json');
		String aff_json_body = '';
		if(aff_pgRef != null)
			aff_json_body = aff_pgRef.getContent().toString();
		*/
		/*
		StaticResource sr = [SELECT Id,NamespacePrefix,SystemModstamp FROM StaticResource WHERE Name = 'ADTTestData' LIMIT 1];
		String prefix = sr.NamespacePrefix;
		if( String.isEmpty(prefix) ) {
		    prefix = '';
		} else {
		    //If has NamespacePrefix
		    prefix += '__';
		}
		//Get the path of json file in Zip static resource
		String srPath = '/resource/' + sr.SystemModstamp.getTime() + '/' + prefix + 'ADTTestData/affiliateData.json'; 
		PageReference pg = new PageReference( srPath );
		String body = pg.getContent().toString();

		*/
		StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'AffiliateData' LIMIT 1];
		String body = sr.Body.toString();
		system.debug('Aff JSON: '+body);
    }
}