/* Description: Data load bath for bulk load data.
 *
 *  Developer         Date                  Change log
 *------------------------------------------------------------------------------------------
 *  Srini             08/11/2018         Initial design
 *  Srini             9/11/2018          Changes for account affiliation and data load query
 */


global class DataLoadBatch implements Database.Batchable<SObject>, Database.stateful{

    @TestVisible private String[] selectedGroupIds;
    @TestVisible private Integer totalRecsproc = 0;
    @TestVisible private Integer totalFailedRecs = 0;
    public DataLoadBatch(String selGroups){
        system.debug('Selected ##'+selGroups);
        //selectedGroupIds = selGroups.split(',');
        selectedGroupIds = new list<string>();
        for(String str:selGroups.split(',')){
            selectedGroupIds.add(str.trim());
        }
    } 
    
    private list<DataLoadMapping__mdt> returnDataMapFields(){
        return [SELECT AccountMapField__c, Active__c, DataLoadMapField__c, LeadMapField__c FROM DataLoadMapping__mdt WHERE Active__c = true];
    }
    
    private Id accStandRecordType(){
        return Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //collect the batches of records or objects to be passed to execute
        //Initalize data load mapping
        set<String> fieldset = new set<String>();
        String queryString = 'SELECT Id';
        for(DataLoadMapping__mdt dt: returnDataMapFields()){
            fieldset.add(dt.DataLoadMapField__c.toLowerCase());
            //queryString = queryString+','+dt.DataLoadMapField__c;
        }
        for(String st:fieldset){
            queryString = queryString+','+st;
        }
        queryString = queryString+',UniqueId__c,Type__c,GroupId__c FROM DataloadTemp__c WHERE GroupId__c IN :selectedGroupIds AND Processed__c = false';

        //String queryString = label.dataloadBatchQuery;//+selectedGroupIds;
        return Database.getQueryLocator(queryString);
    }
    
    global void execute(Database.BatchableContext bc, List<DataloadTemp__c> records){
        try{
	        // process each batch of records
	        Map<String, Id> dlMapForlater = new Map<String, Id>();
	        for(DataloadTemp__c rec: records){
	            dlMapForlater.put(rec.UniqueId__c, rec.Id);
	        }
	        Map<String, SObject> finalRecMap = Utilities.bullkDataLoadMapping(records, returnDataMapFields(), accStandRecordType());
	        //Get leads
	        list<String> uniqIdlist = new list<String>();
	        uniqIdlist.addAll(finalRecMap.keyset());
	        if(finalRecMap.keyset().size() >0){
	            //Save records
	            //list of account to upsert
	            list<Account> acntlist = new list<Account>();
	            //list of leads to insert
	            list<Lead> leadlist = new list<Lead>();
	            for(SObject sobj:finalRecMap.values()){
	                //Check if it is a account or lead
	                if(sobj instanceOf Account){
                    	Account acc = (Account)sobj;
	                    acntlist.add(acc);
	                }else if(sobj instanceOf Lead){
	                    Lead led = (Lead)sobj;  
	                    leadlist.add(led);
	                }
	            }
	            list<DataloadTemp__c> dataloadlist = new list<DataloadTemp__c>();
	            //Final DML Operation - Insert Accounts
	            system.debug('==> acntlist: '+ acntlist);
	            if(acntlist.size()>0){
	                Database.UpsertResult[] srAccntList = Database.upsert(acntlist, Account.TelemarAccountNumber__c, false);
	                //Call Account affiliation logic
	                setupAccntAff(acntlist);
	                for(Integer i=0;i<srAccntList.size();i++){
	                    if(srAccntList[i].isSuccess()){
	                        //update corresponding data load record with status and account Id
	                        DataloadTemp__c dataldTemp = new DataloadTemp__c();
	                        dataldTemp.id = dlMapForlater.get(uniqIdlist[i]);
	                        dataldTemp.Processed__c = true;
	                        dataldTemp.Comments__c = 'Record created successfully';
	                        dataldTemp.Account_or_Lead_Id__c = srAccntList[i].getId();
	                        dataloadlist.add(dataldTemp);
	                        totalRecsproc +=1;
	                    }else{
	                        //update corresponding data load record with failed status
	                        String err = String.valueOf(srAccntList[i].getErrors());
	                        DataloadTemp__c dataldTemp = new DataloadTemp__c();
	                        dataldTemp.id = dlMapForlater.get(uniqIdlist[i]);
	                        dataldTemp.Comments__c = err;
	                        dataldTemp.Error__c = true;
	                        dataloadlist.add(dataldTemp);
	                        totalFailedRecs +=1;
	                    }
	                }
	            }
	            //Insert leads
	            system.debug('==> leadlist'+leadlist);
	            if(leadlist.size()>0){
	                Database.SaveResult[] srleadList = Database.insert(leadlist, false);
	                for(Integer i=0;i<srleadList.size();i++){
	                    if(srleadList[i].isSuccess()){
	                        DataloadTemp__c dataldTemp = new DataloadTemp__c();
	                        dataldTemp.id = dlMapForlater.get(uniqIdlist[i]);
	                        dataldTemp.Processed__c = true;
	                        dataldTemp.Comments__c = 'Record created successfully';
	                        dataldTemp.Account_or_Lead_Id__c = srleadList[i].getId();
	                        dataloadlist.add(dataldTemp);
	                        totalRecsproc +=1;
	                    }else{
	                        String err = String.valueOf(srleadList[i].getErrors());
	                        DataloadTemp__c dataldTemp = new DataloadTemp__c();
	                        dataldTemp.id = dlMapForlater.get(uniqIdlist[i]);
	                        dataldTemp.Comments__c = err;
	                        dataldTemp.Error__c = true;
	                        dataloadlist.add(dataldTemp);
	                        totalFailedRecs +=1;
	                    }
	                }
	           }
	           if(dataloadlist.size()>0){
	                Database.SaveResult[] dllist = Database.update(dataloadlist, false);
	           }
	        }
        }
        catch(Exception ae){
            system.debug('Exception occured: '+ae);
            ADTApplicationMonitor.log ('Bulk load job failure: ', ae.getmessage()+ ' Line Number: '+ae.getLineNumber(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);
            totalFailedRecs = records.size();
        }
    }
    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        //Send an email to the Manager if it cannot be assigned to Sales Rep
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //mail.toAddresses =  new String[]{userinfo.getUserEmail()};
        mail.setTargetObjectId(userinfo.getUserId());
        mail.setSubject('Data load Batch Process Complete');
        mail.setPlainTextBody('The batch processing has been completed. Total records proccessed: '+totalRecsproc+'. Total records failed: '+totalFailedRecs);
        mail.setSaveAsActivity(false);
        Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }  
    
    private void setupAccntAff(list<Account> uploadAccnt){
        try{
	        list<Account_Affiliation__c> accAfflist = new list<Account_Affiliation__c>();
	        Map<String, Id> affMap = new Map<String, Id>();	
	        for(Affiliate__c aff:[SELECT Id, Name FROM Affiliate__c WHERE Active__c = true AND Name != 'USAA']){
	            affMap.put(aff.Name.toUpperCase(), aff.Id);
	        }
	        for(Account acc:uploadAccnt){
	        	if(String.isNotBlank(acc.AffiliateName__c)){
		            Account_Affiliation__c accaff = new Account_Affiliation__c();
		            Account accountReference = new Account(TelemarAccountNumber__c = acc.TelemarAccountNumber__c); 
		            accaff.Member__r = accountReference;
		            accaff.Affiliate__c = affMap.get(acc.AffiliateName__c.toUpperCase());
		            accaff.Active__c = true;
		            accaff.MemberNumber__c = String.isNotBlank(acc.AffiliateMemberNumber__c)? acc.AffiliateMemberNumber__c : acc.AffiliateName__c.toUpperCase();
		            if(accaff.Affiliate__c != null){
		            	accAfflist.add(accaff);
		            }
	        	}
	        }
	        system.debug('Affiliates: '+accAfflist);
	        if(accAfflist.size()>0){
	            Database.insert(accAfflist,false);
	        }
        }
        catch(Exception ae){
        	system.debug('Exception occured: '+ae);
            ADTApplicationMonitor.log ('Bulk load affiliate job failure', ae.getmessage(), true, ADTApplicationMonitor.CUSTOM_APP.STANDARD);
        }
    }
}