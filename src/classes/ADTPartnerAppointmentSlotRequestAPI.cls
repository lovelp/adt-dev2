@RestResource(urlMapping='/getFieldSalesAppointmentSlots/*')
global class ADTPartnerAppointmentSlotRequestAPI {
    @HttpPost
    global static void doPost() {
        String jsonCustRequest = RestContext.request.requestBody.toString();
        //String jsonCustRequest = httpRequest.getBody();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Boolean isRequiredFlag = true;
        System.debug('### initial req'+jsonCustRequest);
        Set<String> requiredFieldsStringSet = new Set<String>{'partnerId','callId','opportunityId','partnerRepName'};
        ADTPartnerAppointmentSchema.AppointmentSlotrequest custReq;
        ADTPartnerAppointmentController controller = new ADTPartnerAppointmentController();
        String custResponse;
        
        //response variable for returning the response 
        //check for apex class attribute match and error out if any other attribute is passed in json
        ADTPartnerAppointmentSchema.AppointmentSlotReponse clr = new ADTPartnerAppointmentSchema.AppointmentSlotReponse();
        try{
          custReq = (ADTPartnerAppointmentSchema.AppointmentSlotrequest)JSON.deserializeStrict(jsonCustRequest,ADTPartnerAppointmentSchema.AppointmentSlotrequest.class);
        }
        catch(exception e){
            isRequiredFlag = false;
            res.statusCode = 400;
            //clr.errorMessage = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
            clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
            ADTApplicationMonitor.log(e, 'ADTPartnerAppointmentController', 'deserializeStrict', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        }
        
        // check for required attribute in json and also empty request
        try{
          if(isRequiredFlag){
             Map<String, Object> resMap = (Map<String, Object>)JSON.deserializeUntyped(jsonCustRequest);
             System.debug('### map for request is'+resMap);
             if(resMap.size() > 0){
                //check for invalid request
                for(String k : requiredFieldsStringSet){
                    if(!resMap.containsKey(k) || resMap.get(k) == null || resMap.get(k)== ''){
                        isRequiredFlag = false;
                        res.statusCode = 400;
                        //clr.errorMessage = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                        clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                    }
                }
             }
             else {
                isRequiredFlag = false;
                res.statusCode = 400;
                //clr.errorMessage = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
             }
          } 
        }
        catch(exception e){
               isRequiredFlag = false;
                res.statusCode = 400;
                //clr.errorMessage = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                clr.message = PartnerAPIMessaging__c.getinstance('400').Error_Message__c;
                ADTApplicationMonitor.log(e, 'ADTPartnerAppointmentController', 'deserializeUntyped', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        }
        
        //check for invalid Date
        try{
            if(isRequiredFlag){
                if(String.isBlank(custReq.appointmentRequest.startingDate)){
                    custReq.appointmentRequest.startingDate = String.valueOf(System.today().addDays(1));
                }
                date initialDate = date.valueOf(custReq.appointmentRequest.startingDate);
                System.debug('### initial date is'+initialdate);
                if(initialDate < System.today()){
                    isRequiredFlag = false;
                    res.statusCode = 400;
                    //clr.errorMessage = PartnerAPIMessaging__c.getinstance('backDated').Error_Message__c;
                    clr.message = PartnerAPIMessaging__c.getinstance('backDated').Error_Message__c;
                    ADTApplicationMonitor.log ('ADTPartner Invalid Data Error', PartnerAPIMessaging__c.getinstance('backDated').Error_Message__c, true, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
                }
            }
            
        }catch(exception e){
                isRequiredFlag = false;
                res.statusCode = 400;
                //clr.errorMessage = PartnerAPIMessaging__c.getinstance('InvalidDate').Error_Message__c;
                clr.message = PartnerAPIMessaging__c.getinstance('InvalidDate').Error_Message__c;
                ADTApplicationMonitor.log(e, 'ADTPartnerAppointmentController', PartnerAPIMessaging__c.getinstance('backDated').Error_Message__c, ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        }
        //if no error in request run the api logic
        try{
            if(isRequiredFlag){
                //check if oppId and callId is valid else assign error Message.
                controller.checkValidOppIdCallId(custReq);
            }
        }catch(Exception e){
            res.statusCode = 500;
            isRequiredFlag = false;
            //clr.errorMessage = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
            clr.message = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
            ADTApplicationMonitor.log(e, 'ADTPartnerAppointmentController', 'checkValidOppIdCallId', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        }
        
        try{
            //get response
            if(isRequiredFlag && String.isBlank(ADTPartnerAppointmentController.errorMessageValue)){
                System.debug('callId and OppId are valid');
                 // 1. get callId and OppId passed in the request
                controller.getCallOppId(custReq);
                if(String.isNotBlank(ADTPartnerAppointmentController.oppId)){
                     clr.opportunityID = ADTPartnerAppointmentController.oppId;
                }
                
                //2. find existing appointment if any and populate the message
                clr.existingAppointment = controller.getExistingAppointment(custReq);
                
                // 3. get slots for 
                if(String.isBlank(ADTPartnerAppointmentController.errorMessageValue)){
                    //set the appointment Message
                    
                    clr.slots = controller.getAppointmentSlots(custReq);
                    clr.message = String.isNotBlank(ADTPartnerAppointmentController.existingAppointmentMessage) ? ADTPartnerAppointmentController.existingAppointmentMessage : '';
                }
            }
        }
        catch(exception e){
            res.statusCode = 500;
            isRequiredFlag = false;
            //clr.errorMessage = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
            clr.message = PartnerAPIMessaging__c.getinstance('500').Error_Message__c;
            ADTApplicationMonitor.log(e, 'ADTPartnerAppointmentController', 'getslots', ADTApplicationMonitor.CUSTOM_APP.PARTNER);
        }
        
        //assign error if any
        if(String.isNotBlank(ADTPartnerAppointmentController.errorMessageValue)){
            //clr.errorMessage =ADTPartnerAppointmentController.errorMessageValue;
            clr.message =ADTPartnerAppointmentController.errorMessageValue;
            res.statusCode = 404;
        } 
        res.addHeader('Content-Type', 'application/json');
        res.responseBody = Blob.valueOf(JSON.serialize(clr,true));
        String JsonResp = JSON.serialize(clr); 
        system.debug('json resp to rv:'+JsonResp);
    }
}

/* 
Request 

{
  "partnerID": "Partner001",
  "callID": "a1Gc00000078uKM",
  "opportunityID": "006c000000H4WeQAAV",
  "partnerRepName": "John Smith",
  "appointmentRequest": {
    "startingDate": "2017-11-29",
    "type": "N1"
  }
}
*/