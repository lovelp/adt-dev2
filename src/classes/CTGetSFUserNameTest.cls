/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : GETGetSFUserNameTest is a test class for CTGetSFUserName.apxc
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Jason Pinlac      07/12/2016      - Origininal Version
*
*                           
*/
@isTest
private class CTGetSFUserNameTest {
    static testMethod void testGetSFUsername() {
		Profile p = [select id from profile where name='System Administrator'];
		User sysAdmin = new User(alias = 'utsr1', email='unitTestSysAdmin@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p.Id, EmployeeNumber__c = 'T987654321', FederationIdentifier = '123456789',
                timezonesidkey='America/Los_Angeles', username='unitTestSysAdmin@testorg.com', Business_Unit__c = 'Resi Direct Sales',
                StartDateInCurrentJob__c = Date.today());
        insert sysAdmin;
		System.runAs(sysAdmin){
			String s = CTGetSFUsername.getSFUserName();
		}
    }
}