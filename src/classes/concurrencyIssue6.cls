@isTest
public class concurrencyIssue6{
    public static DNIS__c dnis;
    public static Call_Data__c calldata;
    public static Account acc;
    public static Lead le;
    public static Promotion__c promo;
    public static Lead_Convert__c lconvert;
    public static customer_Interest__c custInt;
    public static PostalCodeReassignmentQueue__c pcrq;
    public static List<ProfileQuestion__c> proQuestionList;
    public static User u;
    public static List<Postal_Codes__c> postalCodes;
    public static Call_Disposition__c callDispo;
    public static Address__c addr;
    public static Campaign newCamp;
    public static void createTestData(){
        ResaleGlobalVariables__c rgvacc = new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger', value__c='TRUE');
        insert rgvacc;
        //create address for creating accounts
        addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '22102';
        addr.OriginalAddress__c = '8952 BROOK RD, MCLEAN, VA 22102';
        insert addr;
        
        //create lead for the batch
        le = TestHelperClass.createLead(u, false);
        le.PostalCodeID__c = postalCodes[0].id;
        le.Business_Id__c = '1100 - Residential';
        le.Affiliation__c='USAA';
        le.MMBCustomerNumber__c='401093713';
        le.Phone='7324317789';
        le.AddressID__c=addr.id;
        insert le;
        
        //create accounts
        acc=TestHelperClass.createAccountData(addr,postalCodes[0].id,false);
        acc.ShippingStreet = '8952 Brook Rd';
        acc.ShippingStreet2__c = '';
        acc.ShippingCity = 'McLean';
        acc.ShippingState = 'VA';
        acc.ShippingPostalCode = '221o2';
        acc.FirstName__c = 'Unit_Test';
        acc.LastName__c = 'Execution_';
        acc.MMBCustomerNumber__c='401093713';
        acc.MMB_Past_Due_Balance__c=740.00;
        acc.MMBSiteNumber__c='47453518';
        acc.channel__c='Resi Resale';
        acc.MMB_Multisite__c=false;
        acc.PreviousBillingCustomerNumber__c = '12345';
        acc.Previous_Billing_Site_Number__c = '112233';
        acc.MMB_Relo_Customer__c=false;
        acc.MMB_Disco_Site__c=false;
        acc.Phone='7324317789';
        acc.AddressID__c=addr.id;
        acc.MMBDisconnectDate__c = null;
        acc.MMB_Relo_Address__c = '';
        acc.MMB_Relo_Site_Number__c = '';
        acc.email__c='test@test.com';
        acc.DOB_encrypted__c = '01/02/2017';
        acc.MMBLookup__c=true;
        acc.MMBBillCodes__c='MONBA';
        acc.MMBOrderType__c ='R1';
        acc.HOA__c = false;
        acc.TelemarAccountNumber__c='39463559';
        acc.Profile_YearsInResidence__c='1';
        acc.recordtypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('ADT NA Resale').getRecordTypeId();
        insert acc;
        apexpages.currentPage().getparameters().put('accid', acc.id);

        // Initial call history
        calldata = new Call_Data__c(Dial__c='1234567890',Notes__c='Test Notes',ANI__c='(123) 456-7890', Lead__c =le.id, Account__c =acc.id,  DNIS__c=dnis.id, call_time__c=120, Call_Disposition__c = callDispo.id);
        Insert calldata;
        Call_data__c calldatanew = new Call_Data__c(Dial__c='1234567890',Notes__c='Test Notes',ANI__c='(123) 456-7890', Lead__c =le.id, Account__c =acc.id,  DNIS__c=dnis.id, call_time__c=120, Call_Disposition__c = callDispo.id);
        Insert calldatanew;
        custInt = new customer_Interest__c(Description__c='This customer interest is for Fire',interest__c='Fire',line_of_business__c='Residential');
        Insert custInt;
        List<call_interest__c> callInterestList = new List<call_interest__c>();
        callInterestList.add(new Call_Interest__c(Customer_Interest__c=custInt.id,Call_Data__c=calldata.id));
        callInterestList.add(new Call_Interest__c(Customer_Interest__c=custInt.id,Call_Data__c=calldatanew.id));
        insert callInterestList;
        
        // Account related information
        ProfileQuestionAnswer__c proQuesAns= new ProfileQuestionAnswer__c(Name='Test answer', Answer__c='YES', Account__c=acc.id, ProfileQuestion__c=proQuestionList[0].id);
        insert proQuesAns;
        
        //Quote
        Opportunity opp = New Opportunity();
        opp.AccountId = acc.Id;
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp;
        
        
        scpq__SciQuote__c sq = New scpq__SciQuote__c();
        sq.Name = 'TestQuote12';
        sq.scpq__OrderHeaderKey__c = 'whoknows1';
        sq.scpq__OpportunityId__c = opp.Id;
        sq.scpq__SciLastModifiedDate__c = Date.today();
        sq.scpq__Status__c = 'Created';
        sq.Activity_Data__c = '<?xml version="1.0" encoding="UTF-8"?><Activity/>';
        insert sq;

        //Alert for APM
        TerritoryAlerts__c ta1 = new TerritoryAlerts__c();
        ta1.Message_Alert__c = 'APM is applied';
        ta1.Active__c = true;
        ta1.Channel__c = 'Residential;Custom Home Sales';
        ta1.Alert_Begin_Date__c = Date.Today();
        ta1.Alert_End_Date__c = Date.today().adddays(20);
        insert ta1;
        
        ResaleGlobalVariables__c rgv = new ResaleGlobalVariables__c();
        rgv.value__c = 'A000234';
        rgv.Name= 'APM Alert';
        insert rgv;
        
        ResaleGlobalVariables__c resaleGlobalVar1,resaleGlobalVar2,resaleGlobalVar3,resaleGlobalVar4,custom,allowedOrdertype;
        resaleGlobalVar1=new ResaleGlobalVariables__c(); //Custom Setting for Contact Fields
        resaleGlobalVar1.value__c='A-02788';
        resaleGlobalVar1.Name='ContractStatus_Alert_ID';//Static record 1 of custom setting
        insert resaleGlobalVar1;

        resaleGlobalVar2=new ResaleGlobalVariables__c(); //Custom Setting for Contact Fields
        resaleGlobalVar2.value__c='0';
        resaleGlobalVar2.Name='PrimaryTabLocked';//Static record 1 of custom setting
        insert resaleGlobalVar2;

        resaleGlobalVar3=new ResaleGlobalVariables__c(); //Custom Setting for Contact Fields
        resaleGlobalVar3.value__c='A-02788';
        resaleGlobalVar3.Name='ContractStatus_Alert_ID';//Static record 1 of custom setting
        insert resaleGlobalVar3;

        resaleGlobalVar4=new ResaleGlobalVariables__c(); //Custom Setting for Contact Fields
        resaleGlobalVar4.value__c='P1 NON-SERVICEABLE TOWN';
        resaleGlobalVar4.Name='P1/ADTNon-ServicableAlert';
        insert resaleGlobalVar4;
        //for contract status alert
        
        custom = new ResaleGlobalVariables__c();
        custom.value__c = 'A10000';
        custom.Name= 'ContractStatus_Alert_ID';
        insert custom;

        allowedOrdertype = new ResaleGlobalVariables__c();
        allowedOrdertype.value__c = 'A1,R2,N4';
        allowedOrdertype.Name= 'PastDueAllowedOrdertype';
        insert allowedOrdertype;
    }
        /*******************************************************
    * This is a Test method for NSCLeadCaptureRightController
    ********************************************************/
        public static void createTestConfigData(){
        //create custom settings
        BatchState__c bs=new BatchState__c();
        bs.name='PCOwnerChgScheduledBatchInterval';
        bs.value__c='2';
        insert bs;
        
        // settings
        Map<String, String> EquifaxSettings = new Map<String, String>
        { 'Employee Risk Grade'=>'W'
            , 'Employee Condition Code'=>'CME3'
            , 'Default Risk Grade'=>'W'
            , 'Default Condition Code'=>'CAE1'
            , 'Failure Risk Grade'=>'X'
            , 'Failure Condition Code'=>'CAE1'
            , 'Days to allow additional check'=>'90'
            , 'Order Types For Credit Check'=>'N1;R1'
            , 'Order Types For Credit Check - Renters'=>'N1;N2;N3;R1;R2;R3;R4'
            , 'URL'=>''
            , 'Password' => ''
            , 'Login ID'=>''};
                List<Equifax__c> EquifaxSettingList = new List<Equifax__c>(); 
        for( String sN: EquifaxSettings.keySet()){
            Equifax__c eS = new Equifax__c( name = sN );
            eS.value__c = EquifaxSettings.get(sN);
            EquifaxSettingList.add( eS );
        }
        insert EquifaxSettingList;
        Equifax_Conditions__c EquifaxConditionSetting = new Equifax_Conditions__c( Name='CAE1');
        EquifaxConditionSetting.Agent_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting.Agent_Quoting_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting.Customer_Message__c = 'We were able to pull your credit and we are able offer you our best pricing';
        EquifaxConditionSetting.Deposit_Required_Percentage__c = 0.0;
        EquifaxConditionSetting.EasyPay_Required__c = true;
        EquifaxConditionSetting.Payment_Frequency__c = 'A';
        EquifaxConditionSetting.Three_Pay_Allowed__c = true;
        insert EquifaxConditionSetting;
        
        //for timecolor picker
        NSCTimecolorpicker__c tc=new NSCTimecolorpicker__c();
        tc.name='10';
        tc.Color__c='ff0505';
        insert tc;
        
        TestHelperClass.createReferenceDataForTestClasses();
        IntegrationSettings__c is=[select id,MMBSearchUsername__c,MMBSearchPassword__c from IntegrationSettings__c limit 1];
        is.MMBSearchUsername__c='salesforceclup';
        is.MMBSearchPassword__c='abcd1234';
        is.MMBSearchCalloutTimeout__c=60000;
        is.MMBSearchEndpoint__c='https://sgdev.adt.com/adtclup';
        update is;
        
        //CPQ order Type
        ResaleGlobalVariables__c rgv=new ResaleGlobalVariables__c();
        CPQ_Order_Types__c cpqOT= new CPQ_Order_Types__c(name='Resi Resale');
        cpqOT.name='Resi Direct Sales';
        insert cpqOT;
        
        //BypassQuoteSecurityProfiles
        rgv=new ResaleGlobalVariables__c();
        rgv.name='BypassQuoteSecurityProfiles';
        rgv.Value__c='System Administrator';
        insert rgv;
        ResaleGlobalVariables__c rgvs=new ResaleGlobalVariables__c();
        rgvs = new ResaleGlobalVariables__c();
        rgvs.name = 'PastDueAllowedOrdertype';
        rgvs.value__c = 'R2,A1,N4';
        insert rgvs;
        
        rgvs = new ResaleGlobalVariables__c();
        rgvs.name = 'runEwcMarketing';
        rgvs.value__c = 'true';
        insert rgvs;
        
        //Lead Convert
        Lead_Convert__c lc = new Lead_Convert__c(Name= 'Default',SourceNo__c ='001' );
        insert lc;
        
        //create postal code
        postalCodes= TestHelperClass.createPostalCodes();
        postalCodes[0].name='22102';
        update postalCodes[0];
        
        //create postal code reasignment queue
        pcrq=new PostalCodeReassignmentQueue__c();    
        pcrq.TerritoryType__c ='Resi Direct Sales';
        pcrq.PostalCodeNewOnwer__c=userinfo.getuserid();
        pcrq.PostalCodeId__c=postalCodes[0].id;
        insert pcrq;
        
        //create users
        u=testHelperClass.createUser(234343);
        List<user> users=new List<user>();
        
        Profile p = [select id from profile where name='System Administrator'];
        integer i= 47598347;
        User integrationUser = new User(alias = 'stand', email='standarduser@testorg.com',
                                        emailencodingkey='UTF-8', lastname='User',firstname='TestIntegration', languagelocalekey='en_US',
                                        localesidkey='en_US', profileid = p.Id,
                                        timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
                                        StartDateInCurrentJob__c = Date.today());
        
        users.add(integrationUser);
        
        i= 387463;
        User NewRehashUser = new User(alias = 'stand', email='standarduser@testorg.com',
                                      emailencodingkey='UTF-8', lastname='User',firstname='NewRehash', languagelocalekey='en_US',
                                      localesidkey='en_US', profileid = p.Id,
                                      timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com', EmployeeNumber__c='T' + i,
                                      StartDateInCurrentJob__c = Date.today());
        
        users.add(NewRehashUser);
        i= 38947;
        User ResaleAdmin=new User(alias = 'stand', email='standarduser@testorg.com',
                                  emailencodingkey='UTF-8', lastname='ADMIN',firstname='TestGlobal', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = p.Id,
                                  timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com', EmployeeNumber__c='T' + i,
                                  StartDateInCurrentJob__c = Date.today());
        
        users.add(ResaleAdmin);
        
        INSERT users;
        
        //Promotions
        promo= new Promotion__c(Name='$200 off on ADT pulse', Description__c='test Desc', PromotionCode__c='1234',lineofBusiness__c='Residential',ordertype__c='R1');
        insert promo;
        
        //Dsiposition
        disposition__c dispo= new disposition__c(DispositionType__c='Mailed', DispositionDetail__c='Called - with Competitor', DispositionDate__c=DateTime.now(), Comments__c ='test comment');
        insert dispo;
        
        //Dsiposition2
        disposition2__c dispo2= new disposition2__c(DispositionType__c='Mailed', DispositionDetail__c='Called - with Competitor', DispositionDate__c=DateTime.now(), Comments__c ='test comment');
        insert dispo2;
        
        //Call Disposition
        callDispo= new Call_Disposition__c(Name='PP - One-Legger - Follow-up Appt Scheduled', Telemar_Dispo_Code__c='1LA', Comments_Required__c=true, Active__c=true,Tier_One__c= dispo.name);
        insert callDispo;
        
        //create DNIS, Call Data and Customer Interest
        dnis= new DNIS__c(name='1113335555', Start_Date__c=system.TODAY(), End_Date__c=system.TODAY().addDays(4),Lead_Source__c='Ving Rhames', LineOfBusiness__c='Residential',Promotions__c=promo.id); //, Partnership__c='USAA');
        Insert dnis;
        lconvert= new Lead_Convert__c(Description__c='Default', Name = '***************', SourceNo__c = '020', PartnerNo__c = '');
        insert lconvert;    
        
        //Profile Question
        proQuestionList=new List<ProfileQuestion__c >();                       
        proQuestionList.add(new ProfileQuestion__c(name='Has the caller used security before?', LineOfBusiness__c='Residential', Required__c=false, Active__c=true,Sequence__c=2,Type__c='Radio',OrderType__c = 'New; Relo; Reinstatement; Resale Relocation; Resale Multi Site; Addon'));
        proQuestionList.add(new ProfileQuestion__c(name='Has the caller used security before?', LineOfBusiness__c='Residential', Required__c=false, Active__c=true,Sequence__c=2,Type__c='Date',OrderType__c = 'New; Relo; Reinstatement; Resale Relocation; Resale Multi Site; Addon'));
        proQuestionList.add(new ProfileQuestion__c(name='Has the caller used security before?', LineOfBusiness__c='Residential', Required__c=false, Active__c=true,Sequence__c=2,Type__c='Text',OrderType__c = 'New; Relo; Reinstatement; Resale Relocation; Resale Multi Site; Addon'));
        proQuestionList.add(new ProfileQuestion__c(name='Has the caller used security before?', DropDownValues__c='pulse', LineOfBusiness__c='Residential', Required__c=false, Active__c=true,Sequence__c=1,Type__c='Picklist',OrderType__c = 'New; Relo; Reinstatement; Resale Relocation; Resale Multi Site; Addon'));
        insert proQuestionList;
        
        //Packages
        Package__c packages=new Package__c(Name='Gold', description__c='test Desc', LineOfBusiness__c='Residential', PreConfigId__c ='NSCProductID1', Price__c=99.00);
        insert packages;
    }
    static testMethod void NSCLeadCaptureRightControllerTest(){
        createTestConfigData();
        // settings
        Map<String, String> EquifaxSettings = new Map<String, String>
        { 'Password' => ''
            , 'CreditCheckAttempts'=>'3'
            , 'Order Types For Credit Check'=>'N1;R1'
            , 'Failure Risk Grade'=>'X'
            , 'Failure Risk Grade Display Value'=>'APPROV'
            , 'Failure Condition Code'=>'APPROV'
            , 'Days to allow additional check'=>'90'
            , 'Failure Condition Code'=>'CAE1'
            , 'Default Risk Grade'=>'W'
            , 'Default Risk Grade Display Value'=>'(Credit not run) Approved: Annual, Easypay, Full Deposit'
            , 'Default Condition Code'=>'CAE1'
            , 'URL'=>''
            , 'Login ID'=>''};
        List<Equifax__c> EquifaxSettingList = new List<Equifax__c>(); 
        for( String sN: EquifaxSettings.keySet()){
            Equifax__c eS = new Equifax__c( name = sN );
            eS.value__c = EquifaxSettings.get(sN);
            EquifaxSettingList.add( eS );
        }
        insert EquifaxSettingList;
        Equifax_Conditions__c EquifaxConditionSetting = new Equifax_Conditions__c( Name='CAE1');
        EquifaxConditionSetting.Agent_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting.Agent_Quoting_Message__c = 'No Payment Restrictions';
        EquifaxConditionSetting.Customer_Message__c = 'We were able to pull your credit and we are able offer you our best pricing';
        EquifaxConditionSetting.Deposit_Required_Percentage__c = 0.0;
        EquifaxConditionSetting.EasyPay_Required__c = true;
        EquifaxConditionSetting.Payment_Frequency__c = 'A';
        EquifaxConditionSetting.Three_Pay_Allowed__c = true;
        insert EquifaxConditionSetting;
        
        Equifax_Mapping__c eMap = new Equifax_Mapping__c();
        eMap.CreditScoreStartValue__c = '';
        eMap.CreditScoreEndValue__c = '';
        eMap.ApprovalType__c = 'CAE1';
        eMap.RiskGrade__c = 'Y';
        eMap.RiskGradeDisplayValue__c = '(NO Hit) Approved: Annual, Easypay, Full Deposit';
        eMap.NoHit__c = true;
        eMap.HitCode__c = '2';
        eMap.HitCodeResponse__c = '';
        insert eMap;
        User RepUser=[SELECT id FROM User WHERE Profile.name='ADT NSC Sales Representative' and isactive=true limit 1];
        
        system.runas(RepUser){
            createTestData();
            //renderSchedulerButton
            ResaleGLobalVariables__c rgv=new ResaleGLobalVariables__c();
            rgv.name='NSCMATRIXSchedulerEnabled';
            rgv.value__c='1';
            insert rgv;
            
            ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
            res.name = 'DataRecastDisableAccountTrigger';
            res.value__c = 'TRUE';
            upsert res;
            Equifax__c eq=new Equifax__c();
            eq.name='Order Types For Credit Check';
            eq.value__c='N1;R1';
            insert eq;
            
            eq=new Equifax__c();
            eq.name='Days to allow additional check';
            eq.value__c='90';
            insert eq;

            Test.startTest();
            NSCLeadCaptureRightController nscRight=new NSCLeadCaptureRightController();
            NSCRight.AccountId = acc.id;
            String EquifaxLastCreditCheck   = nscRight.LastCreditCheck;
            String EquifaxAgentCondition    = nscRight.EquifaxAgentCondition;
            String EquifaxRiskGrade         = nscRight.EquifaxRiskGrade;
            String EquifaxQuoteCondition    = nscRight.EquifaxQuoteCondition;
            NSCLeadCaptureRightController.QuoteWrappercls quotewrap=new NSCLeadCaptureRightController.QuoteWrappercls();
            quotewrap.compareTo(quotewrap);
            NSCLeadCaptureRightController.callHistoryWrapper callhistwrap= new NSCLeadCaptureRightController.callHistoryWrapper(calldata);
            callhistwrap.getciListsize();
            nscRight.calldataId=calldata.id;
            nscRight.AccountId=acc.id;
            nscRight.loadCustomerInterests();
            nscRight.selectedCustomerInterestId=custInt.id;
            nscRight.selectedCustomerInterest='false';
            nscRight.saveCustomerInterests();
            nscRight.selectedCustomerInterest='True';
            nscRight.saveCustomerInterests();
            nscRight.leadAccountId=acc.id;
            nscRight.loadCallHistory();
            
            nscRight.getQuotes();
            nscRight.getOrderActionList();
            
            acc.MMB_Disco_Site__c=false;
            acc.MMBContractNumber__c='';
            nscRight.getOrderActionList();
            
            acc.MMB_Relo_Customer_Number__c='401093712';
            acc.MMB_Relo_Customer__c=true;
            update acc;
            nscRight.getOrderActionList();
            acc.Relo_Request_Date__c = datetime.now();
            acc.MMBSiteNumber__c='47453518';
            acc.MMBCustomerNumber__c='';
            acc.MMB_Relo_Customer__c=false;
            update acc;
            nscRight.getOrderActionList();
            acc.Relo_Request_Date__c = datetime.now();
            acc.MMB_Relo_Customer__c=true;
            update acc;
            nscRight.getOrderActionList();
            NSCLeadCaptureRightController.AccountActivityWrappercls aac = new NSCLeadCaptureRightController.AccountActivityWrappercls();
            aac.activityId = '1234';
            aac.CreatedDate = '9/12/2016';
            aac.StartDateTime = '9/13/2016';
            aac.EndDateTime = '9/16/2016';
            aac.Status = 'Scheduled';
            aac.OwnerName = 'TestUser';
            aac.Description = 'Test User Appointment';
            aac.AccountName = 'Account Test Name';
            aac.Subject = 'Test User appointment';
            aac.RescheduledReason = '';
            aac.RescheduledDateTime = '9/16/2016';
            aac.AppointmentCreatedBy = 'Test';
            aac.LastModifiedDate = '9/23/2016';
            aac.OwnerWhenCanceled = 'Test';
            aac.Rescheduledid = 'test';
            aac.AppointmentType = 'test';
            aac.CreatedByRepDept = 'nsc';
            Test.stopTest(); 
            acc.MMB_Relo_Customer__c=true;
            acc.MMBSiteNumber__c='';
            acc.MMBBillingSystem__c = 'Informix';
            acc.Relo_Request_Date__c = datetime.now();
            update acc;
            
            nscRight.getOrderActionList();
            nscRight.enableFieldSalesAppointmentBtn();
            nscRight.updatePastDue();
            nscRight.orderactionrender();
        }
    }

}