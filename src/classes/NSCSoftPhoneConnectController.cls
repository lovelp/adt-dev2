/************************************* MODIFICATION LOG ********************************************************************************************
* NSCSoftPhoneConnectController
*
* DESCRIPTION : CTI Agent connection class to Salesforce NSC Calldata
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE        Ticket       REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera     2/15/2012                - Original Version
* Mounika Anna      06/12/2018  HRM-7272    - Made modifications to remove the check for the DNIS Start Date and End Date  
* Viraj Shah		06/06/2019  HRM-8080	- Call Data for chat populating wrong Contact Type.                          
*/
global without sharing class NSCSoftPhoneConnectController {
    
    public String LeadRecordTypeId {
        get{
            RecordType rt = Utilities.getRecordType(RecordTypeName.USER_ENTERED_LEAD, 'Lead');
            return rt.Id;
        }
    }   

    private final static ADTApplicationMonitor.CUSTOM_APP appContext = ADTApplicationMonitor.CUSTOM_APP.MATRIX;
    
    global class callInfo {
        public callInfo(){
            isError = false;
            ErrorMsg = '';
        }
        public String callId;
        public String dnisId;
        public Boolean isError;
        public String ErrorMsg;
    }
    
    public string ANI {get;set;}
    public String DNIS {get;set;}
    public String webChatId {get;set;} 
    public String selectedChannel{get;set;}
    
    public NSCSoftPhoneConnectController(){}
    
    @RemoteAction
    global static callInfo startCall(String TFN, String ANI, String selectedchannel) {
        callInfo caller = new callInfo();
        Call_Data__c cData = new Call_Data__c();
        DNIS__c DNISobj;
        try{
            String regexNumeric = '[^a-zA-Z0-9]';
            String srchStr = TFN.replaceAll(regexNumeric, '').trim();
            Date tDate = Date.today();
            cData.Dial__c = srchStr;
            if(ANI!=null)
                cData.ani__c=ANI;
           //HRM-7272 -- Mounika Anna    -- Removed the start and end date check for the Query 
            List<DNIS__c> dnisList =[SELECT Start_Date__c, End_Date__c FROM DNIS__c WHERE ID IN (Select TFN__c From APNVDN__c Where Name = :srchStr) AND lineofbusiness__c=:selectedchannel ];
                
            if( dnisList!=null && !dnisList.isEmpty() ){
                // DNIS found populate required info if any
                DNISobj = dnisList[0];                
                cData.DNIS__c = DNISobj.Id;
            }
            else {
                if( utilities.isEmptyOrNull(selectedchannel) ){
                  //HRM-7272 --- Mounika Anna -- Removed the start and end date check for the Query
                    dnisList=[SELECT Start_Date__c, End_Date__c FROM DNIS__c WHERE ID IN (Select TFN__c From APNVDN__c Where Name = :srchStr) order by lineofbusiness__c];
                }                
                if(dnisList!=null && !dnisList.isEmpty()){
                    DNISobj = dnisList[0];                
                    cData.DNIS__c = DNISobj.Id;
                }else{
                    caller.isError = true;
                    //errMsg = 'The TFN supplied does not match any active DNIS configured in the system.  To continue without a DNIS selected, click OK.  Otherwise click Cancel.';
                    caller.ErrorMsg = 'This number could not be located.';   
                }
            }
            insert cData;
            
            caller.callId = cData.Id;
            if( cData.DNIS__c != null ){
                caller.dnisId = cData.DNIS__c;
            }
        }
        catch (Exception err){
            caller.isError = true;
            caller.ErrorMsg  = err.getMessage();
            ADTApplicationMonitor.log(err, 'NSCSoftPhoneConnectController', 'startCall', appContext);
        }
        //  JSON.serialize(caller);
        return  caller;
    }
    
    
        
    @RemoteAction
    global static callInfo startChatCall(String TFN, String ANI, String webChatid, String selectedchannel) {
          callInfo caller = new callInfo();
        Call_Data__c cData = new Call_Data__c();
        DNIS__c DNISobj;
        Boolean isANIValid = checkForValidANIFormat(ANI);
        if(String.isNotBlank(webChatid.trim()) && String.isNotBlank(TFN.trim()) && isANIValid) {             
            cData.ChatId__c = webChatId;
            cData.isChatRecord__c = true;
            cData.selected_channel__c = selectedchannel;
             // Added by VS - HRM-8080 -- Start
            cData.Name = TFN +'-'+ System.Datetime.now();
            cData.Contact_Type__c = 'Chat';
            // Added by VS - HRM-8080 ---ENDS
        try{
            Date ttDate = Date.today();
            cData.Dial__c = TFN;
            if(ANI!=null)
                cData.ani__c=ANI;
            //HRM-7272 - Mounika Anna ---- Removed the start and end date check for the Query   
            List<DNIS__c> dnisList =[SELECT Name FROM DNIS__c WHERE Name = :TFN  AND lineofbusiness__c=:selectedchannel];
                System.debug('dnis list result is'+ dnisList.size());
            if( dnisList!=null && !dnisList.isEmpty() ){
                // DNIS found populate required info if any
                DNISobj = dnisList[0];                
                cData.DNIS__c = DNISobj.Id;
            }
            else {
                if( utilities.isEmptyOrNull(selectedchannel) ){
                    //HRM-7272 - Mounika Anna ---- Removed the start and end date check for the Query
                    dnisList=[SELECT Start_Date__c, End_Date__c FROM DNIS__c WHERE ID IN (Select TFN__c From APNVDN__c Where Name = :TFN)  order by lineofbusiness__c];
                }                
                if(dnisList!=null && !dnisList.isEmpty()){
                    DNISobj = dnisList[0];                
                    cData.DNIS__c = DNISobj.Id;
                }else{
                    caller.isError = true;
                    caller.ErrorMsg = 'This given DNIS could not be located.';   
                }
            }
            insert cData;
            caller.callId = cData.Id;
            if( cData.DNIS__c != null ){
                caller.dnisId = cData.DNIS__c;
            }
        }
        catch (Exception err){
            caller.isError = true;
            caller.ErrorMsg  = err.getMessage();
            ADTApplicationMonitor.log(err, 'NSCSoftPhoneConnectController', 'startCall', appContext);
        }
    }
    
      else {
         caller.isError = true;
         if(String.isBlank(webChatid) && String.isBlank(TFN) && String.isBlank(ANI)){
            caller.ErrorMsg = 'DNIS, ANI and Chat ID is Required!';
         }
        else if(String.isBlank(TFN.trim())) {
            caller.ErrorMsg = 'DNIS is Required!'; 
         } 
         else if (String.isBlank(ANI.trim())) {
            caller.ErrorMsg = 'ANI is Required!';
         }
         else if( !isANIValid) {
            caller.ErrorMsg = 'ANI is not in a valid format';
         }
           else if(String.isBlank(webChatid.trim())){
            caller.ErrorMsg = 'Chat ID is Required!';
         } 
          
      }
        return  caller;
    }
    
   @TestVisible  
   private static Boolean checkForValidANIFormat(String num) {
   Boolean flag =false;
   if(String.isBlank(num)){
   flag = false;
   }
   else {
     String regexNumeric = '[^a-zA-Z0-9]';
     num = num.replaceAll(regexNumeric, '').trim();
     System.debug(num);
     if(num.length() == 10){
        flag = true;
     }
    }
    return flag;
   }
}