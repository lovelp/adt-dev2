@isTest
private class ChangeAccountOwnerControllerTest {

    static testMethod void ChangeAccountOwner() {
		Test.startTest();
		User salesRep = TestHelperClass.createSalesRepUserWithManager();
		UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
		User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId and isactive=true Limit 1];
		User adminUser = TestHelperClass.createAdminUser();
		List<Account> accounts = new List<Account>();
		
		ApexPages.PageReference ref = new PageReference('/apex/ChangeAccountOwner');
	    Test.setCurrentPageReference(ref);
	    
	    System.runAs(adminUser) {
	    	Account a = TestHelperClass.createAccountData();
	    	a.OwnerId = ManagerUser.id;
	    	update a;
	    	accounts.add(a);
	    }
	    
		ApexPages.Standardsetcontroller sc = new ApexPages.Standardsetcontroller(accounts);
		sc.setSelected(accounts);
		System.runAs(ManagerUser) {

			ChangeAccountOwnerController caet = new ChangeAccountOwnerController(sc);
			caet.Validate();
			caet.getAllSalesReps();
			caet.setSelectedSalesRep(salesRep.Id);
			caet.getSelectedSalesRep();
			caet.save();
			Account acct = [select ownerId from Account where id=: accounts[0].id];
			System.assertEquals(acct.OwnerId, caet.getSelectedSalesRep(), 'Ownership changed');
		}		
		Test.stopTest();		
    }
    
    static testMethod void ChangeAccountOwnerNoSalesRep() {
		Test.startTest();
		User salesRep = TestHelperClass.createSalesRepUserWithManager();
		UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
		User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId and isactive=true Limit 1];
		User adminUser = TestHelperClass.createAdminUser();
		List<Account> accounts = new List<Account>();
		
		ApexPages.PageReference ref = new PageReference('/apex/ChangeAccountOwner');
	    Test.setCurrentPageReference(ref);
	    
	    System.runAs(adminUser) {
	    	Account a = TestHelperClass.createAccountData();
	    	accounts.add(a);
	    }
	    
		ApexPages.Standardsetcontroller sc = new ApexPages.Standardsetcontroller(accounts);
		sc.setSelected(accounts);
		System.runAs(ManagerUser) {

			ChangeAccountOwnerController caet = new ChangeAccountOwnerController(sc);
			caet.save();
			System.assertEquals(ApexPages.getMessages().size(), 1, 'Failure because sales rep not selected. Expected.');
		}		
		Test.stopTest();		
    }
    
    static testMethod void testCanceledAppointment() {
    	Account a;
    	Event e;
    	User u1, u2;
    	User current = [select Id from User where ID = :UserInfo.getUserId()];
    	system.runas(current) {
    		a = TestHelperClass.createAccountData();
    		u1 = TestHelperClass.createSalesRepUser();
    		a.OwnerId = u1.Id;
    		update a;
    		u2 = TestHelperClass.createManagerUser();
    		e = TestHelperClass.createEvent(a, u1, false);
    		e.RecordTypeId = Utilities.getRecordType( RecordTypeName.COMPANY_GENERATED_APPOINTMENT, 'Event').Id;
    		insert e;
    	}
    	list<Account> accounts = new list<Account>{a};
    	ApexPages.Standardsetcontroller sc = new ApexPages.Standardsetcontroller(accounts);
    	sc.setSelected(accounts);
    	test.startTest();
    		ChangeAccountOwnerController ca = new ChangeAccountOwnerController(sc);
    		ca.setSelectedSalesRep(u2.Id);
    		PageReference ref = ca.Save();
    		system.assertEquals(null, ref);
    		EventManager.setEventToCanceled(e);
    		update e;
    		ref = ca.Save();
    		system.assertNotEquals(null, ref);
    	test.stopTest();
    }
    
    static testMethod void RunAccountAssignmentTest() {
		Test.startTest();
		User salesRep = TestHelperClass.createSalesRepUserWithManager();
		UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
		User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId and isactive=true Limit 1];
		User adminUser = TestHelperClass.createAdminUser();
		List<Account> accounts = new List<Account>();
		
		ApexPages.PageReference ref = new PageReference('/apex/ChangeAccountOwner');
	    Test.setCurrentPageReference(ref);
	    
	    System.runAs(adminUser) {
	    	Account a = TestHelperClass.createAccountData();
	    	a.OwnerId = ManagerUser.id;
	    	update a;
	    	accounts.add(a);
	    }
	    
		ApexPages.Standardsetcontroller sc = new ApexPages.Standardsetcontroller(accounts);
		sc.setSelected(accounts);
		System.runAs(ManagerUser) {
			ChangeAccountOwnerController caet = new ChangeAccountOwnerController(sc);
			caet.onLoad();
			caet.getListSizeForFalse();
			caet.getListSizeForTrue();
			ChangeAccountOwnerController.ChangeOwnershipWithListOfAccounts(accounts,false);
		}		
		Test.stopTest();
    }
    
    static testMethod void RunAccountAssignmentFromTriggerTest() {
		Test.startTest();
		User salesRep = TestHelperClass.createSalesRepUser();
		User salesManager = TestHelperClass.createManagerUser();
		User adminUser = TestHelperClass.createAdminUser();
		adminUser.Business_Unit__c = 'Business Direct Sales';
		update adminUser;
		
		list<Account> accounts = new list<Account>();
	    
	    System.runAs(adminUser) {
	    	String postalId = TestHelperClass.inferPostalCodeID('70094', '1300');
	        Address__c addr = new Address__c();
	        addr.Latitude__c = 29.91760000000000;
	        addr.Longitude__c = -90.17870000000000;
	        addr.Street__c = '56528 E Crystal Ct';
	        addr.City__c = 'Westwego';
	        addr.State__c = 'LA';
	        addr.PostalCode__c = '70094';
	        addr.OriginalAddress__c = '56528 E CRYSTAL CT, WESTWEGO, LA, 70094';
	        insert addr;
	        
	        Account acct = new Account();
	        acct.AddressID__c = addr.Id;
	        acct.Name = 'Unit Test Acct';
	        acct.FirstName__c = 'UnitTestFirst';
	        acct.LastName__c = 'UnitTestLast';
	        acct.AssignOwner__c = true;
	        acct.Business_Id__c = Channels.TYPE_COMMERCIAL;
	        acct.Channel__c = Channels.TYPE_BUSINESSDIRECTSALES;
	        acct.PostalCodeID__c = postalId;
	        acct.ShippingPostalCode = '70094';
	        accounts.add(acct);
        	
        	String postalId2 = TestHelperClass.inferPostalCodeID('33445', '1300');
        	
        	TerritoryAssignment__c ta = new TerritoryAssignment__c();
        	ta.OwnerId = salesRep.Id;
        	ta.PostalCodeId__c = postalId2;
        	ta.TerritoryType__c = Channels.TERRITORYTYPE_BUILDER;
        	insert ta;
        	
	        Address__c addr2 = new Address__c();
	        addr2.Latitude__c = 29.91760000000000;
	        addr2.Longitude__c = -90.17870000000000;
	        addr2.Street__c = '4868 N Citation Dr';
	        addr2.City__c = 'Delray Beach';
	        addr2.State__c = 'FL';
	        addr2.PostalCode__c = '33445';
	        addr2.OriginalAddress__c = '4868 N CITATION DR, DELRAY BEACH, FL, 33445';
	        insert addr2;
	        
	        Account acct2 = new Account();
	        acct2.AddressID__c = addr2.Id;
	        acct2.Name = 'Unit Test Acct2';
	        acct2.FirstName__c = 'UnitTestFirst2';
	        acct2.LastName__c = 'UnitTestLast2';
	        acct2.AssignOwner__c = true;
	        acct2.Business_Id__c = Channels.TYPE_COMMERCIAL;
	        acct2.Channel__c = Channels.TYPE_BUSINESSDIRECTSALES;
	        acct2.PostalCodeID__c = postalId2;
	        acct2.ShippingPostalCode = '33445';
	        acct2.ProcessingType__c = IntegrationConstants.PROCESSING_TYPE_NSC;
	        acct2.ownerUsername__c = 'unitTestSalesRep1@testorg.com';
	        accounts.add(acct2);
	        
	        String postalId3 = TestHelperClass.inferPostalCodeID('33431', '1300');
	    	
	    	ManagerTown__c mt = new ManagerTown__c();
	    	mt.ManagerID__c = salesManager.Id;
	    	mt.TownID__c = 'TestTNID';
	    	mt.Type__c = Channels.TYPE_BUSINESSDIRECTSALES;
	    	insert mt;
	    	
	    	Address__c addr3 = new Address__c();
	        addr3.Latitude__c = 29.91760000000000;
	        addr3.Longitude__c = -90.17870000000000;
	        addr3.Street__c = '1501 Yamato Rd';
	        addr3.City__c = 'Boca Raton';
	        addr3.State__c = 'FL';
	        addr3.PostalCode__c = '33431';
	        addr3.OriginalAddress__c = '1501 YAMATO RD, BOCA RATON, FL, 33431';
	        insert addr3;
	        
	        Account acct3 = new Account();
	        acct3.AddressID__c = addr3.Id;
	        acct3.Name = 'Unit Test Acct2';
	        acct3.FirstName__c = 'UnitTestFirst2';
	        acct3.LastName__c = 'UnitTestLast2';
	        acct3.AssignOwner__c = true;
	        acct3.Business_Id__c = Channels.TYPE_COMMERCIAL;
	        acct3.Channel__c = Channels.TYPE_BUSINESSDIRECTSALES;
	        acct3.PostalCodeID__c = postalId2;
	        acct3.ShippingPostalCode = '33431';
	        acct3.ProcessingType__c = IntegrationConstants.PROCESSING_TYPE_NSC;
	        acct3.ownerUsername__c = 'unitTestSalesRep1@testorg.com';
	        accounts.add(acct3);
	    	
	    	insert accounts;
	    }
	    Test.stopTest();
    }
}