/************************************* MODIFICATION LOG ********************************************************************************************
* GoogleApiResponseGenerator
*
* DESCRIPTION : Simulates responses from google to simulate the callout
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan              02/24/2016          - Original Version
*
*                                                   
*/

@isTest
global class GoogleApiResponseGenerator implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req) {      
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        String JsonStr = '{'
                        +'"dstOffset" : 0,'
                        +'"rawOffset" : -18000,'
                        +'"status" : "OK",'
                        +'"timeZoneId" : "America/New_York",'
                        +'"timeZoneName" : "Eastern Standard Time"'
                        +'}';
        res.setBody(JsonStr);
        res.setStatusCode(200);
        return res;
    }
}