@istest
public class FixCPQCreContTest{

        static testMethod void CPQCreateRedirectPageLoadTest() {        
        
        
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
            TestHelperClass.createReferenceDataForTestClasses();
        }
        
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        Lead_Convert__c lConv;
        
        Account a;
        
        
        System.runAs(current) { 
            CPQ_Order_Types__c cpqOT =new CPQ_Order_Types__c(name='Resi Resale',add_on__c=true);
            insert cpqOT;
            list<ResaleGlobalVariables__c> rgvs = new list<ResaleGlobalVariables__c>();
            rgvs.add(new ResaleGlobalVariables__c(name='IntegrationUser', value__c='Integration User'));
            rgvs.add(new ResaleGlobalVariables__c(name='DataRecastDisableAccountTrigger', value__c='TRUE'));
            rgvs.add(new ResaleGlobalVariables__c(name='BypassQuoteSecurityProfiles', value__c='System Administrator'));
            rgvs.add(new ResaleGlobalVariables__c(name='MMBAddressLength', value__c='50'));
            rgvs.add(new ResaleGlobalVariables__c(name='RIFAccountOwnerAlias', value__c='ADT RIF'));
            rgvs.add(new ResaleGlobalVariables__c(name='DataConversion', value__c='FALSE'));
            rgvs.add(new ResaleGlobalVariables__c(name='MMBNameLength', value__c='50'));
            rgvs.add(new ResaleGlobalVariables__c(name='MMBAccountNameLength', value__c='50'));
            rgvs.add(new ResaleGlobalVariables__c(name='Alternate Pricing Model Order Types', value__c=' N1;R1'));
            rgvs.add(new ResaleGlobalVariables__c(name='GlobalResaleAdmin', value__c='Global Admin'));

            insert rgvs;
            
            ProposalOnlyStates__c pos=new ProposalOnlyStates__c(name='States',states__c='AL,FL,IL,LA,MD,MA,MI,MS,SC,TN,TX,UT,VA,NV');
            insert pos;
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            u.mobilephone='1234';
            update u;
            
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
            
            insert addr;
            
            a = TestHelperClass.createAccountData(addr, true);
            a.OwnerId = u.Id;
            
            //shiva pochamalla
            a.FirstName__c = 'Unit Test';
            a.LastName__c = 'Execution';
            a.Name = 'Unit Test Execution';
            a.Email__c = 'test@adt.com';
            a.Phone = '305231123';
            a.TelemarAccountNumber__c = '123321';
            //added by Shiva Pochamalla
            a.Customer_Refused_to_provide_DOB__c = true;
            a.channel__c='Resi Direct Sales';
            a.MMBLookup__c=true;
            a.Profile_RentOwn__c='R';
            a.Profile_BuildingType__c='APT';
            a.Profile_YearsInResidence__c='10';
            a.MMBCustomerNumber__c='1234';
            a.MMBSiteNumber__c='1234';
            a.Equifax_Last_Check_DateTime__c = System.now();
            //a.EquifaxRiskGrade__c = 'R';
            a.EquifaxApprovalType__c = 'Approval Type';
            
            update a;

            AlphaConversion__c alphaCon = new AlphaConversion__c();
            alphaCon.Name = 'a';
            alphaCon.AlphaCharValue__c = 'A';
            insert alphaCon;
            
            //shiva pochamalla
            Equifax__c e=new Equifax__c();
            e.name='Days to be considered out of date';
            e.value__c='180';
            insert e;
            
            // LeadConvert Data
            lConv = new Lead_Convert__c( Name = '***************', SourceNo__c = '020', PartnerNo__c = '', Description__c = 'Default');
            insert lConv;
            
            // Affiliate
            Affiliate__c affObj = new Affiliate__c( Name = 'USAA', Affiliate_Description__c ='USAA Test', Relevance__c=99);
            insert affObj;
            
            // Account Affiliate
            Account_Affiliation__c affAccObj = new Account_Affiliation__c( Affiliate__c = affObj.Id, Member__c = a.Id, MemberNumber__c = '148932');
            insert affAccObj;
            
            // Opportunity
            Opportunity o = new opportunity();
            o.Name = 'Test';
            o.StageName = 'test';
            o.closeDate = system.today();
            o.AccountId = a.Id;
            insert o;
            
        }
        
        System.runAs(u) {
            Equifax_Conditions__c eCon1 = new Equifax_Conditions__c();
            eCon1.Name = 'Approval Type';
            eCon1.Agent_Message__c = 'Test Agent Message';
            eCon1.Agent_Quoting_Message__c = 'Test Agent Quoting Message';
            insert eCon1;
            
            Equifax__c e1 = new Equifax__c();
            e1.name = 'Test';
            e1.value__c = 'Approval Type';
            insert e1;
            
            // Page Initialization
            Test.setCurrentPage(Page.CPQCreateRedirect);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(a);
            CPQCreateRedirectController cpqCtrl = new CPQCreateRedirectController(sc);
            cpqCtrl.acctObj = a;
            
            cpqCtrl.quoteButtonText = 'test';
            cpqCtrl.SrvURL = 'test';
            
            
            //Boolean renderMultiSiteButton = cpqCtrl.renderMultisiteBtn;
            
            
            // Create quote [Validation failed]
            PageReference pRef1 = cpqCtrl.listQuotes();
            
            // Correct requirements
            Postal_Codes__c pCode  = [Select MMB_TownID__c From Postal_Codes__c Where Id = :a.PostalCodeID__c ];
            pCode.MMB_TownID__c = 'Town123';
            update pCode;
            
            a.MMB_Relo_Customer_Number__c='1234';
            a.MMB_Relo_Customer__c=true;
            a.MMB_Disco_Site__c=false;
            update a;
            
            // Create quote [pass validations]
           // PageReference pRef2 = cpqCtrl.listQuotes();
            test.startTest();
            PageReference pRef3 = cpqCtrl.callCreateQuote();
            
            // R2 for reinstatements
            a.MMBCustomerNumber__c = '123';
            a.MMBSiteNumber__c = '5432';
            a.MMB_Multisite__c = false;
            a.MMB_Relo_Customer__c = false;
            a.MMB_Disco_Site__c = true;
            a.MMBOrderType__c = 'R2';
            update a;
            
            PageReference pRef4 = cpqCtrl.listQuotes();
            PageReference pRef5 = cpqCtrl.callCreateQuoteFromSrv();
            
            // Resale
            a.MMBSiteNumber__c = '54321';
            a.MMBSystemNumber__c = '1234';
            a.MMB_Multisite__c = true;
            a.MMB_Disco_Site__c = false;
            a.MMB_Relo_Customer__c = false;
            update a;
            
            PageReference pRef6 = cpqCtrl.listQuotes();
            
            /*
PageReference pRef7 =cpqCtrl.callMultiSiteLookup();

Account a1 = [select id, MMBContractNumber__c, OwnerId, Owner.Name, MMBCustomerNumber__c, MMB_Past_Due_Balance__c, TelemarLeadSource__c , LeadSourceNo__c, Email__c, FirstName__c, LastName__c, Name, SiteStreet__c, SiteStreet2__c, SiteCity__c, SiteState__c, SitePostalCode__c, TelemarAccountNumber__c, SiteCounty__c, Latitude__c, Longitude__c, Phone, Rep_HRID__c, Rep_Phone__c, AddressID__c, MMB_TownID__c, QueriedSource__c, GenericMedia__c, Business_Id__c, Channel__c, (Select Name, CreatedDate, Affiliate_Quote__c, MemberNumber__c, Affiliate__c, Affiliate__r.Relevance__c, Affiliate__r.Name, Affiliate__r.Member_Required__c, Affiliate__r.Affiliate_Description__c, Affiliate__r.Member_Collected__c, Affiliate__r.Member_Validated__c From Account_Affiliations__r) from Account where id = :a.id];
string s1 = CPQCreateRedirectController.getAffiliationJSON(a1, lConv);


a.MMBSiteNumber__c = '1233';
a.MMBCustomerNumber__c = '1234';
a.Informix_Addon__c=false;
a.MMBBillingSystem__c='INF';
a.Informix_Addon_Reason__c='Contract Monitored Account';
a.channel__c='Resi Resale';
a.MMB_Multisite__c=false;
a.MMB_Relo_Customer__c=false;
a.MMB_Disco_Site__c=false;
update a;
PageReference pRef9 = cpqCtrl.listQuotes();

*/
            test.stopTest();
            
            
            
            
        }       
    }
    
  
}