public class ContinuationController{

     // Unique label corresponding to the continuation
    public String requestLabel;
    // Result of callout
    public String result {get;set;}
    // Callout endpoint as a named credential URL 
    // or, as shown here, as the long-running service URL
   
    public object startRequest() {
        // Create continuation with a timeout
        Continuation con = new Continuation(40);
        // Set callback method
        con.continuationMethod='processResponse';
        
        String userName = IntegrationSettings__c.getInstance().LifeShieldUserName__c;
        String password = IntegrationSettings__c.getInstance().LifeShieldPassword__c;
        String endPointURL = IntegrationSettings__c.getInstance().LifeShieldEndpoint__c;
        
        Blob headerValue = Blob.valueOf(userName + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        Httprequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type', 'application/json');
        request.setEndpoint(endPointURL);
        request.setMethod('POST');
        request.setBody('{"phone": "6174877445"}');
        // Add callout request to continuation
        this.requestLabel = con.addHttpRequest(request);
        
        return con;
    }
    
    // Callback method 
    public void processResponse() {   
       
      // Get the response by using the unique label
      HttpResponse response = Continuation.getResponse(this.requestLabel);
      // Set the result variable that is displayed on the Visualforce page
      result = response.getBody();
      System.debug('responseresult---'+result);
      Map<String, Object> mapres = (Map<String, Object>)JSON.deserializeUntyped(result);
      
      /*
      if(mapres.get('reqStatus') == 'success'){
       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Life shield account.'));
      }
      */
      
      System.debug('map value ---'+ mapres.get('reqStatus') );
      
    }

}