public with sharing class PickListController {
    @AuraEnabled        
    public static List<String> getPickListValuesIntoList(String objectType, String selectedField){
        List<String> pickListValuesList = new List<String>();
        List<String> finalSortedList = new List<String>();
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        pickListValuesList.sort();
        for(Integer i = pickListValuesList.size()-1; i>=0;i--)
        {
            finalSortedList.add(pickListValuesList.get(i));
        }
        
        System.debug('The picklius is'+finalSortedList);
        return finalSortedList;
    }
}