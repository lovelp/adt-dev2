public class NSCLeadCaptureLeftController extends NSCPageControllerBase {
    
    private static String accPrefix = Schema.getGlobalDescribe().get('Account').getDescribe().getKeyPrefix();
    private static String leadPrefix = Schema.getGlobalDescribe().get('Lead').getDescribe().getKeyPrefix();
    public List<Call_Script__c> callScripts{get;set;}
    public String orderTypeval{get;set;}
    private static Map<String, String> ordertype = new Map<String, String>{'A1' => 'Addon', 'N4'=> 'Conversion',  'R3' => 'Resale relocation', 'N2' => 'New Relocation',  'R4' => 'Resale Multi Site', 'R1' => 'Resale', 'N3' => 'New Multi Site', 'N1' => 'New',  'R2' => 'Reinstatement'};
    public String orderTypeLabel {get;set;}
    public String leadaccID{get;set;}
    public String TabObjId{get;set;}
    public Account acc{get;set;}
    public String AccountStatus{get;set;}
    public String accChannel{get;set;}
    private static Map<String, String> Accstatus= new Map<String, String>{'prospect' => 'Prospect', 'cancelled_before' => 'Cancelled Before', 'appointment' => 'Appointment', 'cancelled_after' => 'Cancelled After', 'sold' => 'Sold', 'noInstall' => 'At Risk', 'canceled' => 'Cancelled Sale', 'closed' => 'Closed','rif' => 'In Service', 'pendingDisco' => 'Pending Disco', 'former' => 'Out of Service'};
    public String agentType;
    public String campaignid;
    public boolean isCampaign {get;set;}
    public Call_Data__c CallData {get;set;}
    public static String CallDataPrefix{get;set;}
    public List<Campaign_Script__c> campscripts {get;set;}
    public Boolean isOutboundUser{
        get{
            if(u.Profile.name.contains('Outbound')){
                return true;
            }
            else
                return false;     
        }
        set;
    }
    public User u;
    
    //constructor
    public NSCLeadCaptureLeftController(){
        CallDataPrefix = Call_Data__c.sobjecttype.getDescribe().getKeyPrefix();
        system.debug('CallDataPrefix'+CallDataPrefix);
        u=[select agent_type__c,Profile.name from user where id=:userinfo.getUserId()];
        isCampaign = false;
        agentType=u.agent_Type__c;
        if(agentType=='Loyalty')
            callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Loyalty' order by name];
        else if(agentType=='Outbound'){
            callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Outbound' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
        }else
            callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Sales' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];     

    }
    
    public pageReference updateScript(){
        system.debug('Channel is'+accChannel+' and order type is '+orderTypeLabel+' and Status is '+accountstatus);
        if(accountstatus!='Sold' && agentType!='Outbound' && agentType!='Loyality'){
            String orderTypeLabel = ordertype.get(orderTypeval);
            system.debug('Channel is'+accChannel+' and order type is '+orderTypeLabel);
            callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Order_Type__c includes(:orderTypeLabel) and agent_type__c='Sales' and Line_of_business__c=:accChannel order by name]; 
            if(callScripts.size()==0){
                callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Sales' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
            }
            
        }
        else if(accountstatus!='Sold' && agentType == 'Outbound'){
           String orderTypeLabel = ordertype.get(orderTypeval);
           System.debug('Ordertype in show script'+orderTypeLabel);
           System.debug('callid in show script'+TabObjId);
            if(String.isNotBlank(TabObjId)){
                CallData = [SELECT ChatId__c,Campaign_Type__c,Campaign_Type__r.Name,Dial__c,ANI__c,DNIS__r.Tel_Lead_Source__r.Name, DNIS__r.LineOfBusiness__c, DNIS__r.Affiliate__c, DNIS__r.Affiliate__r.name, DNIS__r.name, DNIS__r.Promotions__c, DNIS__r.Promotions__r.Name, DNIS__r.Promotions__r.Description__c FROM Call_Data__c WHERE id=:TabObjId];
                system.debug('getTabObjId'+TabObjId);
                if(Calldata.Campaign_Type__c!=null)  {
                    isCampaign = true;
                    String campid = Calldata.Campaign_Type__c;
                    list<String> scriptids = new list<String>();
                    for(Campaign_Script__c camp: [select id,Call_Script__c from Campaign_Script__c where Campaign__c =: campid]){
                        scriptids.add(camp.Call_Script__c);
                    } 
                    System.debug('Scripts in show script'+scriptids);
                    system.debug('Ordertype in show script check'+orderTypeLabel);
                    system.debug('accChannel'+accChannel);
                    callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE id =:scriptids and Order_Type__c includes(:orderTypeLabel) and Line_of_business__c LIKE: accChannel order by name];
                    system.debug('callScripts'+callScripts);
                    if(callScripts.size()==0){
                        callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Order_Type__c=null and Account_Status__c=null and agent_type__c='Outbound' and Line_of_business__c= null  order by name];
                    }       
                }
                else{
                    callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Outbound' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name]; 
                    if(callScripts.size()==0){
                        callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Sales' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
                    }
                }    
           }
           else{
               callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Outbound' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name]; 
               if(callScripts.size()==0){
                   callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Sales' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
               }
            }   
        }
        // outbound account is sold
        else if(AccountStatus=='Sold' && agentType =='Outbound'){
            if(String.isNotBlank(TabObjId)){
                CallData = [SELECT ChatId__c,Campaign_Type__c,Campaign_Type__r.Name,Dial__c,ANI__c,DNIS__r.Tel_Lead_Source__r.Name, DNIS__r.LineOfBusiness__c, DNIS__r.Affiliate__c, DNIS__r.Affiliate__r.name, DNIS__r.name, DNIS__r.Promotions__c, DNIS__r.Promotions__r.Name, DNIS__r.Promotions__r.Description__c FROM Call_Data__c WHERE id=:TabObjId];
                system.debug('getTabObjId'+TabObjId);
                system.debug('orderType'+orderTypeLabel);
                system.debug('Account Status'+AccountStatus);
                //if the campaign is attached to the call data
                if(Calldata.Campaign_Type__c!=null){
                    String campid = Calldata.Campaign_Type__c;
                    isCampaign = true;
                    list<String> scriptids = new list<String>();
                    for(Campaign_Script__c camp: [select id,Call_Script__c from Campaign_Script__c where Campaign__c =: campid]){
                        scriptids.add(camp.Call_Script__c);
                    } 
                    callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Outbound' and Account_Status__c='sold' and id in:scriptids order by name];
                    system.debug('callScripts'+callScripts);
                    if(callScripts.size()==0){
                        callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Order_Type__c=null and agent_type__c='Outbound' and Line_of_business__c= null order by name];
                    }       
                }
                //no campaign attached to the call data
                else{
                    callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Account_Status__c='Sold' and agent_type__c='Outbound' order by name]; 
                    if(callScripts.size()==0){
                        callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Sales' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
                    }
                }  
            }
        }
        else{
            callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Order_Type__c includes(:orderTypeLabel) and agent_type__c='Sales' and Line_of_business__c=:accChannel order by name]; 
            if(callScripts.size()==0){
                callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Sales' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
            }
        }  
        system.debug('call scripts'+callScripts);
        return null;
    }
   
    public PageReference LeadAccDetails(){
        if(leadaccID!=null){
            if( leadaccID.startsWith( accPrefix )){           
                acc=[select id,Channel__c from Account where id=:leadaccID];
                accChannel=NSCLeadCaptureController.convertChanneltoLOB(acc.channel__c);
                //To Display account status using class=StatusIndicatorController
                sObject s=acc;
                ApexPages.StandardController sc=new ApexPages.StandardController(s);
                StatusIndicatorController StatusIndicator=new StatusIndicatorController(sc);
                try{
                    StatusIndicator.showAccountStatus();
                    AccountStatus=AccStatus.get(StatusIndicator.status);
                    //If the logged in user is a sales agent
                    if(AccountStatus=='Sold' && agentType!='Outbound' && agentType!='Loyality' ){
                        callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Account_Status__c='Sold' order by name];
                        if(callScripts.size()==0){
                            callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Sales' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
                        }
                    }
                    //If the logged in user is an Outbound Agent
                    else if(AccountStatus=='Sold' && agentType =='Outbound'){
                        if(String.isNotBlank(TabObjId)){
                            CallData = [SELECT ChatId__c,Campaign_Type__c,Campaign_Type__r.Name,Dial__c,ANI__c,DNIS__r.Tel_Lead_Source__r.Name, DNIS__r.LineOfBusiness__c, DNIS__r.Affiliate__c, DNIS__r.Affiliate__r.name, DNIS__r.name, DNIS__r.Promotions__c, DNIS__r.Promotions__r.Name, DNIS__r.Promotions__r.Description__c FROM Call_Data__c WHERE id=:TabObjId];
                            system.debug('getTabObjId'+TabObjId);
                            //if the campaign is attached to the call data
                            if(Calldata.Campaign_Type__c!=null){
                               String campid = Calldata.Campaign_Type__c;
                               isCampaign = true;
                               list<String> scriptids = new list<String>();
                               for(Campaign_Script__c camp: [select id,Call_Script__c from Campaign_Script__c where Campaign__c =: campid]){
                                  scriptids.add(camp.Call_Script__c);
                               } 
                               callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Outbound' and Account_Status__c='sold' and id in:scriptids order by name];
                               system.debug('callScripts'+callScripts);
                               if(callScripts.size()==0){
                                  callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Order_Type__c=null and agent_type__c='Outbound' and Line_of_business__c= null order by name];
                               }       
                             }
                            //no campaign attached to the call data
                            else{
                                  callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Account_Status__c='Sold' and agent_type__c='Outbound' order by name]; 
                                  if(callScripts.size()==0){
                                      callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Sales' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
                                  }
                             }  
                        }
                    }
                    //If the account status is not sold.
                    else if(AccountStatus!='Sold' && agentType =='Outbound'){
                        if(String.isNotBlank(TabObjId)){
                            CallData = [SELECT ChatId__c,Campaign_Type__c,Campaign_Type__r.Name,Dial__c,ANI__c,DNIS__r.Tel_Lead_Source__r.Name, DNIS__r.LineOfBusiness__c, DNIS__r.Affiliate__c, DNIS__r.Affiliate__r.name, DNIS__r.name, DNIS__r.Promotions__c, DNIS__r.Promotions__r.Name, DNIS__r.Promotions__r.Description__c FROM Call_Data__c WHERE id=:TabObjId];
                            system.debug('getTabObjId'+TabObjId);
                            //if the campaign is attached to the call data
                            if(Calldata.Campaign_Type__c!=null){
                               String campid = Calldata.Campaign_Type__c;
                               isCampaign = true;
                               list<String> scriptids = new list<String>();
                               for(Campaign_Script__c camp: [select id,Call_Script__c from Campaign_Script__c where Campaign__c =: campid]){
                                  scriptids.add(camp.Call_Script__c);
                               } 
                               callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Outbound' and Account_Status__c!='sold' and Line_of_business__c LIKE: accChannel and id in:scriptids order by name];
                               system.debug('callScripts'+callScripts.size());
                               if(callScripts.size()==0){
                                  callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Order_Type__c=null and agent_type__c='Outbound' and Line_of_business__c= null and Account_Status__c=null  order by name];
                               }       
                             }
                            //no campaign attached to the call data
                            else{
                                  callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Account_Status__c!='Sold' and agent_type__c='Outbound' order by name]; 
                                  if(callScripts.size()==0){
                                      callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Sales' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
                                  }
                             }  
                        }
                    }
                }
                catch(exception e){}
            }
        }
        return null;
    }
    
    public PageReference CampaignScript(){
        System.debug('TabObjId'+TabObjId);
        if(agentType == 'Outbound'){
            if(TabObjId!=null){
                CallData = [SELECT ChatId__c,Campaign_Type__c,Campaign_Type__r.Name,Dial__c,ANI__c,DNIS__r.Tel_Lead_Source__r.Name, DNIS__r.LineOfBusiness__c, DNIS__r.Affiliate__c, DNIS__r.Affiliate__r.name, DNIS__r.name, DNIS__r.Promotions__c, DNIS__r.Promotions__r.Name, DNIS__r.Promotions__r.Description__c FROM Call_Data__c WHERE id=:TabObjId];
                system.debug('getTabObjId'+TabObjId);
                    //check if the campaign value is not null and get all the scriptids based on the campaign selected.
                    if(Calldata.Campaign_Type__c!=null) {
                        String campid = Calldata.Campaign_Type__c;
                        isCampaign = true;
                        list<String> scriptids = new list<String>();
                        for(Campaign_Script__c camp: [select id,Call_Script__c from Campaign_Script__c where Campaign__c =: campid]){
                            scriptids.add(camp.Call_Script__c);
                        }
                        // Query all the scripts based on the ids that we got Quering Campaign script configuration.
                        callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Outbound' and id =:scriptids order by name];
                        //if there is no configuration setup for the campaign then we are assigning the default script.
                        if(callScripts.size() == 0 ){
                            callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Outbound' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
                        }
                    }
                    //if the Campaign is not present. Then load the default outbound scripts based on the ordertype and line of business of the account.
                    else{
                        callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Order_Type__c includes(:orderTypeLabel) and agent_type__c='Outbound' and Line_of_business__c=:accChannel order by name]; 
                        if(callScripts.size()==0){
                            callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Outbound' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
                        }
                    }    
              }
        }
        //If the agent is not an Outbound User
        else{
            callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE Order_Type__c includes(:orderTypeLabel) and agent_type__c='Sales' and Line_of_business__c=:accChannel order by name]; 
            if(callScripts.size()==0){
                callScripts=[select id,name,Header_Id__c,Script_Id__c,script__c from Call_Script__c WHERE agent_type__c='Sales' and Line_of_business__c=null and Order_Type__c=null and Account_Status__c=null order by name];
            }
        }   
        return null;
    }
}