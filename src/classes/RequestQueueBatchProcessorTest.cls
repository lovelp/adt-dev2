/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest(seeAllData = false)
private class RequestQueueBatchProcessorTest {
    private static void createTestData(){
        list<ResaleGlobalVariables__c> reslist = new list<ResaleGlobalVariables__c>();
        ResaleGlobalVariables__c res = new ResaleGlobalVariables__c();
        res.name = 'DataRecastDisableAccountTrigger';
        res.value__c = 'TRUE';
        reslist.add(res);
        ResaleGlobalVariables__c res1 = new ResaleGlobalVariables__c();
        res1.name = 'GlobalResaleAdmin';
        res1.value__c = 'Global Admin';
        reslist.add(res1);
        ResaleGlobalVariables__c res2 = new ResaleGlobalVariables__c();
        res2.name = 'EventRecTypeNameAllowed';
        res2.value__c = 'Company Generated Appointment,Field Sales Appointment';
        reslist.add(res2);
        ResaleGlobalVariables__c res3 = new ResaleGlobalVariables__c();
        res3.name = 'DisqualifyEventTaskCode';
        res3.value__c = 'TRV';
        reslist.add(res3);
        insert reslist;
        IntegrationSettings__c is = new IntegrationSettings__c(RequestQueueMaxErrors__c = 10);
        insert is;
    }
    
    static testMethod void testRequestQueueBatch() {
        createTestdata();
        
        Account a;
        List<RequestQueue__c> rqlist = new List<RequestQueue__c>();
        RequestQueue__c rq;
        RequestQueue__c rqold;
        RequestQueue__c rqnew;
        User current = [Select Id from User where ID=:UserInfo.getUserID()];
        system.runas(current) {
            
            // generic rq
            rq = new RequestQueue__c();
            rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rq.ServiceTransactionType__c = 'sendEBRRequest';
            
            // generic rq
            RequestQueue__c rq1 = new RequestQueue__c();
            rq1.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rq1.ServiceTransactionType__c = 'sendACKEmail';
            
            // generic rq
            RequestQueue__c rq2 = new RequestQueue__c();
            rq2.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rq2.ServiceTransactionType__c = 'sendToOneClick';
            
            // shared account
            a = TestHelperClass.createAccountData();
            
            // old rq
            rqold = new RequestQueue__c();
            rqold.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rqold.AccountID__c = a.Id;
            rqold.CreatedDate = system.now().addDays(-1);
            
            // new rq
            rqnew = new RequestQueue__c();
            rqnew.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rqnew.AccountID__c = a.Id;
            rqnew.EventID__c = 'Test';
            
            insert new List<RequestQueue__c>{rq, rqold, rqnew, rq1, rq2};
                rqlist.add(rq);
            rqlist.add(rqnew);
        }
        
        String rqstr = '';
        for (RequestQueue__c r : rqlist) {
            rqstr += '\'' + r.Id + '\',';
        }
        rqstr = rqstr.substring(0, rqstr.length()-1);
        
        test.startTest();
        RequestQueueBatchProcessor rqb = new RequestQueueBatchProcessor();
        //rqb.query = 'select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate from RequestQueue__c where RequestStatus__c = \'Ready\' and Id IN (' + rqstr + ') order by RequestDate__c limit 200';
        Database.executeBatch(rqb); // in production, scope value is one, but in text context it must be able to hold all records returned in one batch
        test.stopTest();
        
        RequestQueue__c rqconfirm = [select Id,RequestStatus__c from RequestQueue__c where Id = :rq.Id limit 1];
        // this assert fails because it actually tries a callout from outgoing message
        //system.assertEquals( IntegrationConstants.REQUEST_STATUS_SUCCESS, rqconfirm.RequestStatus__c);
        
        RequestQueue__c rqnewconfirm = [select Id,RequestStatus__c from RequestQueue__c where Id = :rqnew.Id];
        //system.assertEquals( IntegrationConstants.REQUEST_STATUS_READY, rqnewconfirm.RequestStatus__c);
    }
    static testMethod void testRequestQueueBatch1() {
        createTestdata();
        Account a;
        List<RequestQueue__c> rqlist = new List<RequestQueue__c>();
        RequestQueue__c rq;
        RequestQueue__c rqold;
        RequestQueue__c rqnew;
        User current = [Select Id from User where ID=:UserInfo.getUserID()];
        system.runas(current) {
            
            // generic rq
            rq = new RequestQueue__c();
            rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            
            // shared account
            a = TestHelperClass.createAccountData();
            
            // old rq
            rqold = new RequestQueue__c();
            rqold.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            rqold.AccountID__c = a.Id;
            rqold.CreatedDate = system.now().addDays(-1);
            
            // new rq
            rqnew = new RequestQueue__c();
            rqnew.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_ERROR;
            rqnew.AccountID__c = a.Id;
            rqnew.EventID__c = 'Test';
            
            insert new List<RequestQueue__c>{rq, rqold, rqnew};
                rqlist.add(rq);
            rqlist.add(rqnew);
        }
        
        String rqstr = '';
        for (RequestQueue__c r : rqlist) {
            rqstr += '\'' + r.Id + '\',';
        }
        rqstr = rqstr.substring(0, rqstr.length()-1);
        
        test.startTest();
        RequestQueueBatchProcessor rqb = new RequestQueueBatchProcessor();
        //rqb.query = 'select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate from RequestQueue__c where RequestStatus__c = \'Ready\' and Id IN (' + rqstr + ') order by RequestDate__c limit 200';
        Database.executeBatch(rqb); // in production, scope value is one, but in text context it must be able to hold all records returned in one batch
        test.stopTest();
        
        RequestQueue__c rqconfirm = [select Id,RequestStatus__c from RequestQueue__c where Id = :rq.Id limit 1];
        
        RequestQueue__c rqnewconfirm = [select Id,RequestStatus__c from RequestQueue__c where Id = :rqnew.Id];
        system.assertEquals( IntegrationConstants.REQUEST_STATUS_ERROR, rqnewconfirm.RequestStatus__c);
    }
    
    static testMethod void testRequestQueueBatch2() {
        createTestdata();
        Account a;
        List<RequestQueue__c> rqlist = new List<RequestQueue__c>();
        RequestQueue__c rq;
        RequestQueue__c rqold;
        RequestQueue__c rqnew;
        User current = [Select Id from User where ID=:UserInfo.getUserID()];
        system.runas(current) {
            
            // generic rq
            rq = new RequestQueue__c();
            rq.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rq.ServiceTransactionType__c  = 'setAccountDispositionOnly';
            
            // shared account
            a = TestHelperClass.createAccountData();
            
            // old rq
            rqold = new RequestQueue__c();
            rqold.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rqold.AccountID__c = a.Id;
            rqold.CreatedDate = system.now().addDays(-1);
            rqold.ServiceTransactionType__c  = 'setAccountAccount';
            // new rq
            rqnew = new RequestQueue__c();
            rqnew.RequestStatus__c = IntegrationConstants.REQUEST_STATUS_READY;
            rqnew.AccountID__c = a.Id;
            rqnew.EventID__c = 'Test';
            rqnew.ServiceTransactionType__c  = 'setUnavailableTimeNew';
            
            insert new List<RequestQueue__c>{rq, rqold, rqnew};
                rqlist.add(rq);
            rqlist.add(rqnew);
        }
        
        String rqstr = '';
        for (RequestQueue__c r : rqlist) {
            rqstr += '\'' + r.Id + '\',';
        }
        rqstr = rqstr.substring(0, rqstr.length()-1); 
      
        test.startTest();
        RequestQueueBatchProcessor rqb = new RequestQueueBatchProcessor();
        //rqb.query = 'select Id,RequestStatus__c,ServiceTransactionType__c,ServiceMessage__c,AccountID__c,CreatedDate,EventID__c from RequestQueue__c where RequestStatus__c = \'Ready\' and Id IN (' + rqstr + ') order by RequestDate__c limit 200';
        Database.executeBatch(rqb); // in production, scope value is one, but in text context it must be able to hold all records returned in one batch
       
        test.stopTest();
        
        RequestQueue__c rqconfirm = [select Id,RequestStatus__c from RequestQueue__c where Id = :rq.Id limit 1];
        // this assert fails because it actually tries a callout from outgoing message
        //system.assertEquals( IntegrationConstants.REQUEST_STATUS_SUCCESS, rqconfirm.RequestStatus__c);
        
        RequestQueue__c rqnewconfirm = [select Id,RequestStatus__c from RequestQueue__c where Id = :rqnew.Id];
        //system.assertEquals( IntegrationConstants.REQUEST_STATUS_READY, rqnewconfirm.RequestStatus__c);
    }
    
}