/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ProfileHelperTest {

    static testMethod void testManager() {
    	//ProfileHelper ph = new ProfileHelper();
        User u = TestHelperClass.createManagerUser();
        
        User newuser = [Select Id,ProfileId,Profile.Name from User where ID = :u.Id];
        Profile userp = [Select Id,Name from Profile where Id = :u.ProfileId];
        
        system.assert(ProfileHelper.isManager(userp.Name));
        
        system.runas(u) {
        	system.assert(ProfileHelper.isManager());
        }
        
        List<User> mgrs = ProfileHelper.getAllManagers();
        Boolean found = false;
        for (User m : mgrs)
        {
        	if (m.Id == newuser.Id)
        	{
        		found = true;
        		break;
        	}
        }
        system.assert(found);
        
        mgrs = ProfileHelper.getAllManagers(new List<User>{newuser});
        found = false;
        for (User m : mgrs)
        {
        	if (m.Id == newuser.Id)
        	{
        		found = true;
        		break;
        	}
        }
        system.assert(found);
        
        mgrs = ProfileHelper.getAllManagers(new List<ID>{newuser.Id});
        found = false;
        for (User m : mgrs)
        {
        	if (m.Id == newuser.Id)
        	{
        		found = true;
        		break;
        	}
        }
        system.assert(found);
    }
    
	static testMethod void testSalesRep() {
        User u = TestHelperClass.createSalesRepUser();
        
        User newuser = [Select Id,ProfileId from User where ID = :u.Id];
        Profile userp = [Select Id, Name from Profile where Id = :u.ProfileId];
        
        system.assert(ProfileHelper.isSalesRep(userp.Name));
        
        system.runas(u) {
        	system.assert(ProfileHelper.isSalesRep());
        }
    }
    
    static testMethod void testSalesExec() {
        User u = TestHelperClass.createSalesExecUser();
        
        User newuser = [Select Id,ProfileId from User where ID = :u.Id];
        Profile userp = [Select Id, Name from Profile where Id = :u.ProfileId];
        
        system.assert(ProfileHelper.isSalesExec(userp.Name));
        
        system.runas(u) {
        	system.assert(ProfileHelper.isSalesExec());
        }
    }
    
    static testMethod void testSysAdmin() {
        User u = TestHelperClass.createAdminUser();
        
        User newuser = [Select Id,ProfileId from User where ID = :u.Id];
        Profile userp = [Select Id, Name from Profile where Id = :u.ProfileId];
        
        system.assert(ProfileHelper.isSysAdmin(userp.Name));
        
        system.runas(u) {
        	system.assert(ProfileHelper.isSysAdmin());
        }
    }
    
    static testMethod void testLimited() {
    	User u = TestHelperClass.createLimitedUser(0);
        User up = [Select Profile.Name from User where Id = :u.Id];
    	
    	system.runas(u) {
    		system.assert( ProfileHelper.isLimitedAccessUser() );
    		system.assert( ProfileHelper.isLimitedAccessUser(up.Profile.Name) );
    	}
    	
    }
    
    static testMethod void testGetAllActiveManagers() {
    	list<User> mgrs;
    	test.startTest();
    		mgrs = ProfileHelper.getAllActiveManagers();
    	test.stopTest();
    	list<User> users = new list<User>([Select Name, Profile.Name, isActive from User where ID IN :mgrs]);
    	for (User u : users) {
    		system.assert(u.isActive, 'User ' + u.Name + ' is not active.' );
    		system.assert( ProfileHelper.isManager(u.Profile.Name), 'User ' + u.Name + ' is not a Manager.');
    	}
    }
    
    static testMethod void testIsIntegrationUser() {
    	User u;
    	User current = [Select Id from User where Id = :UserInfo.getUserId()];
    	system.runas(current) {
    		u = TestHelperClass.createUser(1001);
    		Profile p = [Select Id from Profile where Name IN :ProfileHelper.INTEGRATION_PROFILE_NAMES limit 1];
    		u.ProfileId = p.Id;
    		update u;
    	}
    	
    	system.runas(u) {
    		system.assert( ProfileHelper.isIntegrationUser());
    	}
    }
}