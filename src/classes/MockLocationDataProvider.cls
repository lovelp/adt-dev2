/************************************* MODIFICATION LOG ********************************************************************************************
* MockLocationDataProvider
*
* DESCRIPTION : Extends LocationDataProvider and provides an implementation of the interface that can be used for testing purposes.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE					REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Derek Benner					10/14/2011				- Original Version
*
*													
*/
global class MockLocationDataProvider extends LocationDataProvider {
	
	public override List<LocationItem> getLocationItems(LocationDataMessageParameters parameters) {
		
		
		Dom.Document domDoc=TeleNavLocationDataProvider.testDoc; 
	 	domDoc.load(TeleNavLocationDataProvider.telenavTestResponse);
	 
	 	List<LocationItem> listToReturn = TeleNavLocationDataProvider.handleResponse(domDoc);
		
		//Profile p1 = [select id from profile where name='ADT NA Resale Sales Representative' limit 1];
		User u = [select Name from User where Profile.Name IN :ProfileHelper.SALESREP_PROFILE_NAMES limit 1];
		
		for (LocationItem li : listToReturn) {
			
			li.rUser = u;
			
		}	 
	 	return listToReturn;
	 	
	}
	
	public override map<String,List<LocationItem>> getStopLocations(Datetime startDT, Datetime endDT, String DevicePhoneNumber)
	{
		//Dom.Document domDoc1 = TeleNavLocationDataProvider.testDoc; 
		Dom.Document domDoc = new Dom.Document();
	 	domDoc.load(TeleNavLocationDataProvider.teleNavTestStopReportResponse);
	 	
	 	map<String,List<LocationItem>> stopInfoMap = TeleNavLocationDataProvider.handleResponseForStopReport(domDoc);
	 	
	 	List<LocationItem> startGPSLocationItemList = stopInfoMap.get('startGPS');
	 	List<LocationItem> endGPSLocationItemList = stopInfoMap.get('endGPS');
	 	
	 	LocationItem startGPSLocItem = startGPSLocationItemList[0];
	 	LocationItem endGPSLocItem = endGPSLocationItemList[0];
	 	
	 	System.assertEquals( startGPSLocItem.pLatitude,'37.3731');
	 	System.assertEquals( endGPSLocItem.pLatitude,'37.37319');
	 	
		return stopInfoMap;
	}
	
	static testMethod void testGetLocationItems() {
		
		User salesRep = TestHelperClass.createSalesRepUser();
		
		CurrentLocationMessageProcessor clmp = new CurrentLocationMessageProcessor();
		LocationDataMessageParameters ldmp = clmp.buildParameters(salesRep.Id, null, null);
		
		List<LocationItem> lItems;
		map<String,List<LocationItem>> stopInfoMap;
		Datetime Date1 = datetime.newInstance(2012, 04, 24, 08, 00, 00);
		Datetime Date2 = datetime.newInstance(2012, 04, 24, 09, 00, 00);
		 
		Test.startTest();
		
		MockLocationDataProvider tldp = new MockLocationDataProvider();
		
		lItems = tldp.getLocationItems(ldmp);		
		stopInfoMap = tldp.getStopLocations(Date1, Date2,'5550000014');
		
		TeleNavLocationDataProvider tnldp = new TeleNavLocationDataProvider();
		HttpRequest req1 = TeleNavLocationDataProvider.basicAuthCalloutForStopReport(Date1, Date2,'5550000014');
		System.assertEquals( req1.getHeader('Host'),'integration.telenav.com');
		Test.stopTest();
		
		System.assert(lItems != null, 'Return value should not be null');
		
	}
	
	private static testMethod void testTeleNavLocationDataProvider() {
		User u = [Select Id From User Where Id = :Userinfo.getUserId()];
		u.DevicePhoneNumber__c ='1234567890';
		update u;
		Test.startTest();
		Test.setmock(HttpCalloutMock.class, new MockResponse());
		TeleNavLocationDataProvider ldp = new TeleNavLocationDataProvider();
		ldp.getStopLocations(system.now(), system.now().addDays(30), '');
		LocationDataMessageParameters parameters = new LocationDataMessageParameters(system.today(), new List<Id>{UserInfo.getUserId()}, UserInfo.getUserId(), true);
		ldp.getLocationItems(parameters);
		
		Test.stopTest();
	}
	
	
	global class MockResponse implements HttpCalloutMock { 
		global HTTPResponse respond(HTTPRequest req) {
			// Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
    
   		String Resp =  '<xml></xml>';
        res.setBody(Resp);
        res.setStatusCode(200);
        return res;
		}
	}

}