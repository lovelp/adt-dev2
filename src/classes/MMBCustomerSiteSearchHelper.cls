/************************************* MODIFICATION LOG ********************************************************************************************
* MMBCustomerSiteSearchHelper
*
* DESCRIPTION : Used by the system to access the information needed to comunicate with the service isolating this mechanism.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera          3/11/2014      - Original Version
* Jitendra Kothari        04/12/2019      HRM-9466 - 3G Cell change or Panel Change notification
*                          
*/


public class MMBCustomerSiteSearchHelper {
  
  public static final String SERVICENS = 'http://mastermindservice.adt.com/lookup';
  public static final String HSERVICENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
  public static final String MMB_SEARCH_PREFIX = 'look';
  public static final String MMB_SEARCH_HPREFIX = 'wsse';
  public static final String MMB_SEARCH_SOAPNS = 'soapenv';
  public static final String MMB_SEARCH_SOAPENV = 'http://schemas.xmlsoap.org/soap/envelope/';
  
  public static string getMMBSearchEndpoint(){
    return IntegrationSettings__c.getinstance().MMBSearchEndpoint__c;
  }
  
  public static Integer getMMBSearchCalloutTimeout(){
    return IntegrationSettings__c.getinstance().MMBSearchCalloutTimeout__c.intValue();
  }
  
  public static Boolean isMMBSearchEnabled(){
    return IntegrationSettings__c.getinstance().MMBSearchCalloutsEnabled__c;
  }
  
  public static String getMMBSearchAuth(){
    return EncodingUtil.base64Encode(Blob.valueOf(IntegrationSettings__c.getinstance().MMBSearchUsername__c + ':' + IntegrationSettings__c.getinstance().MMBSearchPassword__c));
  }
  
  public static Dom.Document createEnvelopeHeader(){
    DOM.Document doc = new DOM.Document();
    DOM.Xmlnode envelope = doc.createRootElement('Envelope', MMB_SEARCH_SOAPENV, MMB_SEARCH_SOAPNS);
    envelope.setNamespace(MMB_SEARCH_PREFIX, SERVICENS);
    envelope.setNamespace(MMB_SEARCH_HPREFIX, HSERVICENS);
    return doc;
  }
  
  public static DateTime getDateTimeValueFromXMLNode(Dom.XMLNode xnInfo){
    DateTime dt;
    if( xnInfo != null && !Utilities.isEmptyOrNull( xnInfo.getText() )){
      try{
        String dtStr = xnInfo.getText();
        dtStr = dtStr.replace('T', ' ');
        dt = DateTime.valueOf( dtStr );
      }
      catch(Exception err){ system.debug('EXCEPTION parsing DateTime value ERR:'+err.getMessage()+ ' '+err.getStackTraceString()); }
    }
    return dt;
  }
  
  //Start HRM-9466
  public static String getChangeNotification(MMBCustomerSiteSearchApi.SiteInfoResponse siteResp){
    if(((siteResp.ServiceActive == 'IN' && String.isBlank(siteResp.CellDeactivateDate)) || siteResp.ServiceActive == 'OUT') &&
      String.isNotBlank(siteResp.CellTech) && ResaleGlobalVariables__c.getInstance('CELLTECHVALUES') != null && 
      String.isNotBlank(ResaleGlobalVariables__c.getInstance('CELLTECHVALUES').value__c) &&
      ResaleGlobalVariables__c.getInstance('CELLTECHVALUES').value__c.containsIgnoreCase(siteResp.CellTech)
    ){
      return (String.isBlank(siteResp.CellCOnOff) || (String.isNotBlank(siteResp.CellCOnOff) && siteResp.CellCOnOff != 'NOOFF'))? System.Label.MMBLookupCellularUpgrade : System.Label.MMBLookupPanelUpgrade;
    }
    return '';
  }
  //End HRM-9466
}