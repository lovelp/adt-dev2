/************************************* MODIFICATION LOG ********************************************************************************************
* ADT_SalesAgreementComponentControllerTest
*
* DESCRIPTION : Test class for ADT_SalesAgreementComponentController
*  1. Create
*  2. Update
*  3. Lookup
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                        DATE              TICKET            REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Harsha                        28/Jun/2019        HRM-9710         Sales Agreement Lookup/Create/Update
* Jitendra Kothari              07/15/2019         HRM-10409         Enhance Existing Sales Agreement Lookup to Add Fields 
*/
@isTest
global class ADT_SalesAgreementCompControllerTest {
    
    private static testMethod void testCreateSalesAgreement() {
        User u = [Select Id, EmployeeNumber__c from User where Id = :Userinfo.getUserId()];
        u.EmployeeNumber__c = '81234567';
        update u;
        Opportunity opp = [Select Id From Opportunity where createddate = today limit 1];
        Test.startTest();
        ADT_SalesAgreementComponentController.initActions(opp.Id, '');
        
        ADT_SalesAgreementComponentController.DataWrapper dw = ADT_SalesAgreementComponentController.initActions(opp.Id, 'Create');
        String testWrapperJSON = JSON.serialize(dw, true);
        
        Test.setMock(HttpCalloutMock.class,  new MockHttpLoanErrorResponse());
        dw = ADT_SalesAgreementComponentController.submit(testWrapperJSON);
        
        Test.setMock(HttpCalloutMock.class, new SalesAgreementCreateMock());
        dw = ADT_SalesAgreementComponentController.submit(testWrapperJSON);
        
        system.assertEquals('123456789', dw.sacRes.result, 'Sales Agreement Id in Mock is blank');
        
        ADT_SalesAgreementComponentController.initActions(opp.Id, 'Create');
        
        Test.setMock(HttpCalloutMock.class,  new MockHttpLoanErrorResponse());
        dw = ADT_SalesAgreementComponentController.submit(testWrapperJSON);
        
        Test.stopTest();
    }
    private static testMethod void testCreateSalesAgreementNegative() {
        User u = [Select Id, EmployeeNumber__c from User where Id = :Userinfo.getUserId()];
        u.EmployeeNumber__c = '';
        update u;
        Opportunity opp = [Select Id From Opportunity where createddate = today limit 1];
        opp.OwnerId = u.Id;
        update opp; 
        ADT_SalesAgreementComponentController.initActions(Userinfo.getUserId(), 'Create');
        ADT_SalesAgreementComponentController.initActions(opp.id, 'Create');
        ADT_SalesAgreementComponentController.getInstance('2');
        ADT_SalesAgreementComponentController.getInstance('');
       
    }
    
    private static testMethod void testLookupSalesAgreementNegative() {
        User u = [Select Id, EmployeeNumber__c from User where Id = :Userinfo.getUserId()];
        u.EmployeeNumber__c = '';
        update u;
        Opportunity opp = [Select Id From Opportunity where createddate = today limit 1];
        opp.OwnerId = u.Id;
        update opp; 
        ADT_SalesAgreementComponentController.initActions(Userinfo.getUserId(), 'Create');
        
        opp.SalesAgreementId__c = '1ew2rt347ew';
        update opp;    
        ADT_SalesAgreementComponentController.initActions(opp.id, 'Lookup');
    }
    
     
     
    //@isTest
    global class MockHttpLoanErrorResponse implements HttpCalloutMock {
        // Implement this interface method
        global HTTPResponse respond(HTTPRequest req) {
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Resp =  '{"errors":[{"status":"500","errorCode":"-1","errorMessage":"Authorization failed"}]}';
            res.setBody(Resp);
            res.setStatusCode(500);
            return res;
        }
    }
    
    private static testMethod void testLookupSalesAgreement() {
        Opportunity opp = [Select Id From Opportunity where createddate = today limit 1];
        String Id = TestHelperClass.inferPostalCodeID('32223', '1100 - Residential');
        User u = new User(Id = Userinfo.getUserId(), EmployeeNumber__c = '81234567');
        
        update u;
        Test.startTest();
        
        ADT_SalesAgreementComponentController.DataWrapper dw = ADT_SalesAgreementComponentController.initActions(opp.Id, 'Lookup');
        String testWrapperJSON = JSON.serialize(dw, true);
        ADT_SalesAgreementComponentController.submit(testWrapperJSON);
        
        dw.salReq.SalesAgreementID = '1234';
        testWrapperJSON = JSON.serialize(dw, true);
        
        Test.setMock(HttpCalloutMock.class,  new MockHttpLoanErrorResponse());
        dw = ADT_SalesAgreementComponentController.submit(testWrapperJSON);
        
        Test.setMock(HttpCalloutMock.class, new SalesAgreementLookupMock());
        
        dw = ADT_SalesAgreementComponentController.submit(testWrapperJSON);
        
        system.assertEquals(u.EmployeeNumber__c, dw.salRes.PrimarySalesRepId, 'Primary Sales Rep is not matching');
        system.assertEquals('0064D000004NmQL', dw.salRes.sfdcOpportunityID, 'Opportunity Id is not matching');
        system.assertEquals('Test', dw.salRes.CompanyName, 'Company Name is not matching');
        system.assertEquals('Vision Quest', dw.salRes.Attention, 'Attention Name is not matching');
        system.assertEquals('123 E Main St', dw.salRes.CustomerAddress1, 'Customer Address Line 1 is not matching');
        system.assertEquals('Suite #210', dw.salRes.CustomerAddress2, 'Customer Address Line 2 is not matching');
        
        system.assertEquals('Elliot City', dw.salRes.CustomerCity, 'Customer City is not matching');
        system.assertEquals('MD', dw.salRes.CustomerState, 'Customer State is not matching');
        system.assertEquals('Howard', dw.salRes.CustomerCounty, 'Customer County is not matching');
        system.assertEquals('21042', dw.salRes.CustomerZip, 'Customer Zip is not matching');
        system.assertEquals('myemail@domain.com', dw.salRes.CustomerEmail, 'Customer Address Line 1 is not matching');
        system.assertEquals('4433670422', dw.salRes.CustomerPhone, 'Customer Address Line 1 is not matching');
        system.assertEquals('16000754', dw.salRes.MastermindCustomerNumber, 'Master Mind Customer Number is not matching');
        system.assertEquals(1, dw.salRes.MastermindInstanceId, 'Master Mind Instance is not matching');
        system.assertEquals('1 - P1MMB', dw.MMBBillingSystems, 'Master Mind Instance is not matching');
        
        
        testWrapperJSON = JSON.serialize(dw, true);
        
        Test.setMock(HttpCalloutMock.class,  new MockHttpLoanErrorResponse());
        dw = ADT_SalesAgreementComponentController.selectSalesAgreement(testWrapperJSON, opp.Id, '1234567890');
        
        Test.setMock(HttpCalloutMock.class, new SalesAgreementUpdateMock());
        dw = ADT_SalesAgreementComponentController.selectSalesAgreement(testWrapperJSON, opp.Id, '1234567890');
        
        list<Opportunity> opps = [SELECT Id, AccountId, Installation_Revenue__c, RMR__c, SalesAgreementId__c
                                     FROM Opportunity WHERE Id = :opp.Id limit 1];
        system.assertEquals(opps.isEmpty(), false);
        system.assertEquals(opps.size(), 1);
        system.assertEquals(opps.get(0).Installation_Revenue__c, dw.salRes.InstallCharge);
        //system.assertEquals(opps.get(0).SalesAgreementId__c, dw.salReq.SalesAgreementID);
        system.assertEquals(opps.get(0).RMR__c, dw.salRes.sellingTotalRMR);
        dw = ADT_SalesAgreementComponentController.selectSalesAgreement(testWrapperJSON, opp.Id, '1234567890');
        
        Test.stopTest();
    }
    private static testMethod void testMelissaCode() {
    	Opportunity opp = [Select Id, AccountId, Account.AddressID__c, Account.AddressID__r.PostalCode__c, Account.AddressID__r.City__c,
    						 Account.AddressID__r.State__c, Account.AddressID__r.Street2__c, Account.AddressID__r.County__c,
    						 Account.AddressID__r.Street__c
    						From Opportunity where createddate = today limit 1];
        Test.startTest();
        MelissaDataAPI.MelissaDataResults mdr = SalesAgreementAPI.checkForMelissaData(opp.Account.AddressID__r, 'UI');
        
        SalesAgreementAPI.updateInvalidAddress(mdr, opp.Account.AddressID__r); 
        mdr = SalesAgreementAPI.checkForMelissaData(opp.Account.AddressID__r, 'SCRIPT');
        
        Test.stopTest();
    }
    @TestSetup
    private static void createTestData() {
        IntegrationSettings__c is=new IntegrationSettings__c();
        is.DPUsername__c = 'salesforceclup';
        is.DPPassword__c = 'abcd1234';
        is.SalesPointCalloutTimeout__c = 60000;
        is.SalesPilotURL__c = 'https://salespilottest.protection1.com/Sales/SalesAgreement/SalesAgreement/[SalesAgreementId]/Info';
        is.CreateSalesAgreementURL__c = 'https://salespilottest.protection1.com/SDFC/createSalesAgreement';
        is.LookupSalesAgreementURL__c = 'https://salespilottest.protection1.com/SDFC/lookupSalesAgreement';
        is.UpdateSalesAgreementURL__c ='https://salespilottest.protection1.com/SDFC/updateSalesAgreement';
        insert is;

        //Create Account
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
        ResaleGlobalVariables__c rgv3 = new ResaleGlobalVariables__c();
        rgv3.name = 'DataRecastDisableAccountTrigger';
        rgv3.value__c = 'FALSE';
        insert rgv3;
        ResaleGlobalVariables__c rgv5 = new ResaleGlobalVariables__c();
        rgv5.name = 'DataConversion';
        rgv5.value__c = 'FALSE';
        insert rgv5;
        
        list<CommercialMessages__c> messages = new list<CommercialMessages__c>();
        messages.add(new CommercialMessages__c(Name = 'SalesAgreementPresent', Message__c = 'SalesAgreementPresent'));
        messages.add(new CommercialMessages__c(Name = 'SalesAgreementRequired', Message__c = 'SalesAgreementRequired'));
        messages.add(new CommercialMessages__c(Name = 'requestTypeRequired', Message__c = 'requestTypeRequired'));
        messages.add(new CommercialMessages__c(Name = 'OppRequired', Message__c = 'OppRequired'));
        messages.add(new CommercialMessages__c(Name = 'SalesRepRequired', Message__c = 'OppRequired'));
        messages.add(new CommercialMessages__c(Name = 'SalesAgreementPresentForOtherOpp', Message__c = 'OppRequired'));
        insert messages;
        
        List<Postal_Codes__c> pcs = new List<Postal_Codes__c>();
        Postal_Codes__c pc = new Postal_Codes__c (name='221o2', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100', APM_PostalCode__c = true);
        pcs.add(pc);
        insert pcs;
        
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '221o2';
        addr.County__c = 'County123';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        insert addr;
        
        Account acct = new Account(Name = 'Unit Test Account i');
        acct.AddressID__c = addr.Id;
        acct.FirstName__c='testFirstName';
        acct.LastName__c='testLastName';
        acct.LeadStatus__c = 'Active';
        acct.UnassignedLead__c = false;
        acct.InService__c = true;
        acct.Business_Id__c = '1100 - Residential';
        acct.ShippingPostalCode = '221o2';
        acct.Email__c = 'abc@gmail.com';
        acct.Phone = '1234566789';
        acct.DOB_encrypted__c = '12/12/1900';
        acct.Equifax_Last_Check_DateTime__c = system.now().addDays(-5);
        acct.EquifaxRiskGrade__c = 'A';
        acct.MMBOrderType__c = 'R1';
        acct.PostalCodeId__c = pc.Id;
        insert acct;
        
        Opportunity opp = New Opportunity(AccountId = acct.Id);
        opp.Name = 'TestOpp1';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        insert opp;
    }
}