/************************************* MODIFICATION LOG ********************************************************************************************
* NewMoverBatch
*
* DESCRIPTION : This batch flips the New Mover > 90 Days old indicator to denote that a new mover is no longer a current new mover.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover            		3/12/2012			- Original Version
*
*													
*/
global class NewMoverBatch implements Database.Batchable<sObject>, Database.Stateful {
	
	public String query = 'Select Id, NewMover__c from Account where NewMover__c = true AND NewMoverFeedDate__c!=NULL AND NewMoverFeedDate__c< LAST_90_DAYS';
	
	global Database.QueryLocator start( Database.Batchablecontext bc )
	{
		return Database.getQueryLocator(this.query);
	}
	
	global void execute(Database.BatchableContext bc, List<sObject> scope)
	{
		for (Account a : (List<Account>)scope)
		{
			a.NewMover__c = false;
		}
		update scope;
	}
	
	global void finish(Database.BatchableContext bc)
	{
		
	}
	
}