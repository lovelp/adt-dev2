/*
 * Description: This is a test class for the following classes
 * 1.RequestQueueMonitor
 * Created By: Ravi Pochamalla
 *
 */

@isTest
private class RequestQueueMonitorTest{
    public static void createTestData(){
        TestHelperClass.createReferenceDataForTestClasses();
    }

        static testMethod void TestRequestQueueMonitor(){
        createTestData();
        test.starttest();
        RequestQueueMonitor rqm=new RequestQueueMonitor();
        String chron = '0 0 21 * * ?';   
        system.schedule('Request Monitor Tst',chron,rqm);
        test.stopTest();
        }
}