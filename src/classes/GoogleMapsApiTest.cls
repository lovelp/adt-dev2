/************************************* MODIFICATION LOG ********************************************************************************************
* GoogleMapsApiTest
*
* DESCRIPTION : Provides Coverage to the following classes:
*               - GoogleMapsTimeZone.cls
*               - GoogleDistanceMatrix.cls
*               
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Siddarth Asokan               02/24/2016 
*                                                   
*/
@isTest
private class GoogleMapsApiTest{
    public static User createUser(String name, String profileName){
        // Select a standard profile
        Profile p;
        UserRole r;
        User mgr;
        if(profileName == 'System Administrator')
          p = [SELECT Id from Profile where UserType = 'Standard' and Name =: profileName limit 1];    

        r = [SELECT Id, Name from UserRole limit 1];
        System.assertNotEquals(null, r.Id);
        System.assertNotEquals(null, p.Id);
    
        User u = new User(
            alias = name.substring(0, Math.min(8, name.length())),
            email= name + '@test.org',
            emailencodingkey='UTF-8',
            lastname=name,
            languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = p.id,
            UserRoleId = r.Id,
            timezonesidkey='America/Los_Angeles',
            username=name + Math.random() + '@test.org',
            CompanyName = 'Testing',
            Title = 'Test ' + name
        );
        insert u;
        System.assertNotEquals(null, u.Id);

        return u;
    }

    static testMethod void testGoogleMapsTimeZone() {
        //Create Addresses first
        Address__c addr = new Address__c();
        addr.Latitude__c = 38.94686000000000;
        addr.Longitude__c = -77.25470100000000;
        addr.Street__c = '8952 Brook Rd';
        addr.City__c = 'McLean';
        addr.State__c = 'VA';
        addr.PostalCode__c = '22102';
        addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 22102';       
        insert addr;

        // Insert Global variables
        ResaleGlobalVariables__c globalVar = new ResaleGlobalVariables__c();
        globalVar.Name = 'GoogleGeoCodeDecodeKey';
        globalVar.Value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk=';
        insert globalVar;

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new GoogleApiResponseGenerator());

        Test.startTest();
        GoogleMapsTimeZone.GoogleTZResponse tzResp = new GoogleMapsTimeZone.GoogleTZResponse();
        tzResp.dstOffset = 0;
        tzResp.rawOffset = -18000;
        tzResp.timeZoneId = 'America/New_York';
        tzResp.timeZoneName = 'Eastern Standard Time';
        tzResp.error_message = 'error';

        GoogleMapsTimeZone.getAddressTimeZone(addr);
        Test.stopTest();
    }

    static testMethod void testGoogleDistanceMatrix() {
        // Create User
        User agentUser = GoogleMapsApiTest.createUser('TestAgentUser', 'System Administrator');
        
        System.runAs(agentUser){
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '22102';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 22102';
            addr.StandardizedAddress__c = '8952 Brook Rd, McLean, VA 22102';
            insert addr;

            List<Address__c> addrLst = new List<Address__c>();
            addrLst.add(addr);

            List<String> addrStringLst = new List<String>();
            addrStringLst.add('test');

            // Insert Global variables
            ResaleGlobalVariables__c globalVar = new ResaleGlobalVariables__c();
            globalVar.Name = 'GoogleGeoCodeDecodeKey';
            globalVar.Value__c = 'rE5EevLmL7uzpwPOkxpw1RcZCFk=';
            insert globalVar;

            // Set mock callout class
            Test.setMock(HttpCalloutMock.class, new GoogleDistanceMatrixApiRespGenerator());

            Test.startTest();
            GoogleDistanceMatrix.DistanceMatrixResponse dmResp = new GoogleDistanceMatrix.DistanceMatrixResponse();
            dmResp.status = 'OK';
            dmResp.origin_addresses = addrStringLst;
            dmResp.destination_addresses = addrStringLst;

            GoogleDistanceMatrix.Duration dur = new GoogleDistanceMatrix.Duration();
            dur.value = 10;
            dur.text = 'min';

            GoogleDistanceMatrix.Distance dis = new GoogleDistanceMatrix.Distance();
            dis.value = 10;
            dis.text = 'mi';

            GoogleDistanceMatrix.Element ele = new GoogleDistanceMatrix.Element();
            ele.Status = 'OK';
            ele.Duration = dur;
            ele.Distance = dis;

            GoogleDistanceMatrix.Row rows = new GoogleDistanceMatrix.Row();
            List<GoogleDistanceMatrix.Element> eleLst = new List<GoogleDistanceMatrix.Element>();
            eleLst.add(ele);
            rows.elements = eleLst;
            
            GoogleDistanceMatrix.setDistanceMatrixOrigin(agentUser.Id, addr);
            GoogleDistanceMatrix.setDistanceMatrixDestination(agentUser.Id, addr);
            GoogleDistanceMatrix.isDistanceMatrixAvailable();
            GoogleDistanceMatrix.getDistanceMatrix();
            GoogleDistanceMatrix.setDistanceMatrixOrigins(addrLst);
            GoogleDistanceMatrix.setDistanceMatrixDestinations(addrLst);
            GoogleDistanceMatrix.setDistanceMatrixOrigins(addrStringLst);
            GoogleDistanceMatrix.setDistanceMatrixDestinations(addrStringLst);
            GoogleDistanceMatrix.clearPlaces();            
            Test.stopTest();
        }
    }
}