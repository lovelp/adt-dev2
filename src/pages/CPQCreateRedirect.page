<!--************************************* MODIFICATION LOG ********************************************************************************************
*
* DESCRIPTION : CPQCreateRedirect.page 
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE            Ticket          REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Magdiel Herrera                                               - Original Version
* Mounika Anna              03/22/2018          HRM-6887        - Past Due Relocations
* Siddarth Asokan           09/25/2018          HRM-8017        - Renter Credit Rule changes
* Srinivas Yarramsetti      07/18/2019          HRM-9790        - Customer loan auto refresh on mmb auto refresh.
-->
<apex:page standardController="Account" extensions="CPQCreateRedirectController,SplashPopupMessageController" action="{!listQuotes}" > 

    <!-- CSS -->
    <link rel="stylesheet" href="{!URLFOR($Resource.bootstrap3, 'css/bootstrap.min.css')}" media="all" />
    <link rel="stylesheet" href="{!URLFOR($Resource.bootstrap3, 'css/bootstrap-theme.min.css')}" media="all" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>

    <!-- JavaScript Libraries -->
    <apex:includeScript value="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"/>
    <apex:includeScript value="{!URLFOR($Resource.bootstrap3, 'js/bootstrap.min.js')}"/>    
    
    <style>
    
    /* custom modal css */    
    .modal-backdrop { position: fixed; top: 0; right: 0; bottom: 0; left: 0; z-index: 1040; background-color: #000; opacity: .5; }
    .force-bs-modal .modal-header {background: url('/img/alohaSkin/overlay_crns.png') right top no-repeat; padding: 0px 6px 0px 0px}
    .force-bs-modal .modal-header .modal-header-content { padding: 8px 6px 8px 12px; background: url('/img/alohaSkin/overlayTopLft.png') left top no-repeat; }
    
    .bPageTitle {
        margin-bottom: 0px;
    }
    .bPageTitle .ptBody .content {
        width: 75%;
        padding-left: 0px; 
    }
    
    /*without a parent tab style the default bar doesnt display properly*/
    .header-link {
        float: right;
        width: 15%;
        vertical-align: middle;
        text-align: right;
    }
    .header-link-center {
        float: left;
        /* padding-right: 5px; */
        /* margin-left: 250px; */
        /* vertical-align: middle; */
        /* text-align: left; */
        /* position: absolute; */
        width: 60%;
    }
    .header-link-content {
        float: right;
        font-size: 1.083em;
        font-weight: bold;
        color: #353535;
    }
    .header-link-back{
        background: url(/img/chatter/messages_sprite.png) no-repeat scroll 0 -32px;    
        width: 20px;
        height: 12px;
        float: right;       
        margin: 2px 0px;
    }
    .force-bs .well {
        margin-bottom: 5px;
        margin-top: 10px;
    }
    </style>


    <apex:form id="form">
        <!-- HRM-9790-->
    <apex:actionFunction name="runcustomerLoanUtility" action="{!runcustomerLoanUtility}" reRender=""/> 
    <apex:actionFunction name="openQuoteInCpq" action="{!openQuoteInCpq}" reRender="" >
        <apex:param id="urlValue" name="urlValue" value="" />
        <apex:param id="quoteId" name="quoteId" value="" />
    </apex:actionFunction>
     <script>
        var $j = jQuery.noConflict();
        
        var ObjID = "{!ObjID}";
     </script>
        
    <!-- Page Title -->
        <!--Changes done for #4984----- TCS--->
    <div class="bPageTitle">
        <div class="ptBody secondaryPalette brandSecondaryBrd">
        
            <div class="content">
                <img src="/s.gif" class="pageTitleIcon" title="Home" alt="Home"/>
                <h1 class="pageType">Account</h1>
                <h2 class="pageDescription">{!acctObj.Name}</h2>
                <div style="margin-left: 37px; font-style: normal;">
                    <address>
                      {!acctObj.SiteStreet__c} {!acctObj.SiteStreet2__c}
                      {!acctObj.SiteCity__c}, {!acctObj.AddressID__r.State__c} {!acctObj.SitePostalCode__c}
                    </address>
                </div>
            </div>
            <div class="header-link" style="padding-right:0">
            
              <span class="header-link-content">
                <a href="/{!ObjID}" target="_self">Go Back</a>
              </span>
              <img src="/s.gif" class="header-link-back"/>
              <br/>
              <div class="force-bs" style="display:none;">
                <div class="well well-sm" style="text-align: center;"><span style="font-weight: bold;color: #333;">Order Type: </span><span class="badge">{!acctObj.MMBOrderType__c}</span></div>
              </div>                
              <apex:outputPanel id="redirectLooupPanel" rendered="{!renderMultisiteFeature}">
                  <apex:actionStatus id="callMultiSiteLookupStatus">
                    <apex:facet name="start">
                        <apex:commandButton value="Assign Multisite Customer" disabled="true" style="margin: auto;width: 100%;"/>
                    </apex:facet>
                    <apex:facet name="stop">
                        <apex:commandButton action="{!callMultiSiteLookup}" value="Assign Multisite Customer"  style="margin: auto;width: 100%;" status="callMultiSiteLookupStatus" reRender="redirectLooupPanel"/>
                    </apex:facet>
                  </apex:actionStatus>
              </apex:outputPanel>   
              
            </div>
            </div>
            <div>
            
            <!-- start-6319 changes -- added -->
            <apex:outputPanel id="OuterDiv">
            <!-- end- 6319 changes -- added -->
            <div class="header-link-center" style= "padding-left: 25%;">
                  <apex:repeat value="{!qButtons}" var="qb" id="theRepeat">
                    <br/>
                    <apex:commandButton action="{!callCreateQuote}" value="{!qb.buttonText}" rerender="form" disabled="{!qb.disabled}">
                        <apex:param name="qbOrderType" value="{!qb.orderType}" assignTo="{!qbOrderType}"/>  
                    </apex:commandButton>
                    <div>
                    <i><apex:outputText value="{!qb.message}" escape="false" style="word-wrap:break-word;"/></i></div>
                  </apex:repeat>
                  <br/>
                   <!--start- 6319 changes  -->
                  <apex:commandButton status="loadingStatus" action="{!getAppts}" value="Get Install Appointments" rendered="{!NOT(disableInstallBut)}" reRender="AppointmentOPPanel,OuterDiv,loadingStatus" disabled="{!fadeAppntButton}"/><br/>
                  <i><apex:outputText value="{!errorMsg}" escape="false" style="font-size: inherit;color:red; word-wrap: break-word;"  rendered="{!errorExist}"/></i>
                  <br/><br/>
                  <!-- End- 6319 changes  -->
                  <!--start- 8017 changes  -->
                  <apex:outputLabel rendered="{!IF(AND(ISNULL(acctObj.Equifax_Last_Check_DateTime__c),AllowedCreditCheck),true,false)}" style="font-size: inherit;color:red;word-wrap: break-word;">
                    * Credit check has not been performed. Please run credit check or the following conditions will be applied: {!DefaultAgentCondition}.
                  </apex:outputLabel>
                  <apex:outputLabel rendered="{!CreditConditionObsolete}" style="font-size: inherit;color:red; word-wrap: break-word;">
                    Credit Check is outdated. Please rerun credit check or the following conditions will be applied: {!DefaultAgentCondition}.
                  </apex:outputLabel>
                  <!--End- 8017 changes  -->
                  <br></br>
                  <apex:outputLabel style="font-size: large;color:blue;" rendered="{!alternatePricingModel}" >
                    * PILOT MARKET PRICING IN USE
                  </apex:outputLabel>
                  <br></br>
                  <apex:outputLabel style="font-size: large;color:blue;" escape="false" value="{!allowFinance}">
                  
                  </apex:outputLabel>
            </div>
                          
                <!--start- HRM 6355 changes  -->
                <div style="{!IF(IsManager,'display:block','display:none')}">
                <p><apex:outputLabel value="Add comments for overriding Dealer Hold :" for="delOverCom"/></p>
                <apex:inputTextarea id="delOverCom" value="{!dealerOverrideComments}" rows="4" style="overflow:hidden"/><br/>
                <span id="deloverError" style="color:red;display:none;">Please enter overide comments.</span>
                <br/>
                <apex:commandButton status="loadingStatusDealer" onClick="if(!checkManOverride()) return false;" action="{!dealerOveride}" value="Override Dealer Hold" reRender="OuterDiv"/><br/>
                </div>
                <script>
                //dealerOveride
                    function checkManOverride(){
                        var comments = document.getElementById('{!$Component.form.delOverCom}').value;
                        if(comments !=''){
                            console.log('comments'+comments);
                            return true;
                        }
                        else{
                            document.getElementById('deloverError').style.display = "block";
                            return false;
                        }
                    }
                </script>
                <br/><br/>
                <div class="header-link-center" style= "padding-left: 25%;">   
                    <apex:actionStatus id="loadingStatusDealer">
                        <apex:facet name="start">
                            <apex:outputPanel >
                                <img src="/img/loading32.gif" width="25" height="25" />
                                <apex:outputLabel value="Loading..." style="font-size:inherit;"/>
                            </apex:outputPanel>            
                        </apex:facet>
                    </apex:actionStatus>
                </div>
                <!-- End- HRM 6355 changes  -->
            <!-- start-6319 changes -- added -->    
          </apex:outputPanel>
        <!-- End -6319 changes -- added --> 
          
            
        </div>
    </div><br/><br/><br/>    
    <div id="ErrorDiv" style="width:100%;float:left;">
        <apex:pageMessages escape="false"/>
    </div>
    <!------------ 4984 Changed END ------->      
    
    <!-- 6319 changes -- added appointment panel -->
    <br/>
    <div class="header-link-center" style= "padding-left: 25%;">   
        <apex:actionStatus id="loadingStatus">
            <apex:facet name="start">
                <apex:outputPanel >
                    <img src="/img/loading32.gif" width="25" height="25" />
                    <apex:outputLabel value="Loading..." style="font-size:inherit;"/>
                </apex:outputPanel>            
            </apex:facet>
        </apex:actionStatus>
    </div>
    <apex:outputPanel id="AppointmentOPPanel">
        <apex:outputPanel rendered="{!showApptPanel}">
            <apex:pageBlock id="pgBlock" title="Install Appointments" >
                <apex:pageBlockSection id="section1" columns="1" collapsible="true" showHeader="false">
                <b>Dates shown based on 4 hour duration. Available install appointments may change based on final quote and install duration.</b>
                </apex:pageBlockSection><br/>
                <apex:pageBlockTable id="pgTable" value="{!lstApptWrapper}" var="apptWrapper" onRowClick="expandColumn('{!$Component.pgSection}')" style="cursor:pointer;">
                    <apex:column headerValue="Product"  colspan="1" title="Show section - {!apptWrapper.installConfigName}" >                    
                        <apex:pageblocksection id="pgSection" title="{!apptWrapper.installConfigName}" showheader="true" collapsible="true" columns="1" >
                            <script>
                            twistSection(document.getElementById('{!$Component.pgSection}').getElementsByTagName('img')[0])
                            </script>
                            <apex:pageBlockTable value="{!apptWrapper.lstAdditionalInfoWrapper}" var="addInfo" rendered="{!if(apptWrapper.errorFlag == 'true', false, true)}">
                                <apex:column headerValue="Date" >
                                    <apex:outputText value="{!addInfo.appointmentDate}" />
                                </apex:column>
                                <apex:column headerValue="AM" >
                                    <apex:outputText value="{!if(addInfo.amSlot, 'YES', 'NO')}" />
                                </apex:column>
                                <apex:column headerValue="PM" >
                                    <apex:outputText value="{!if(addInfo.pmSlot, 'YES', 'NO')}" />
                                </apex:column>
                            </apex:pageBlockTable>                    
                        </apex:pageblocksection>                      
                    </apex:column>
                    <apex:column headerValue="Next Available"  title="Show section - {!apptWrapper.installConfigName}" >
                        <apex:outputText value="{!apptWrapper.nextAvailableDate}" style="color:{!apptWrapper.statusIndicator};"/>
                    </apex:column>
                    <apex:column headerValue="Availability" title="Show section - {!apptWrapper.installConfigName}">
                        <b><apex:outputText value="{!apptWrapper.numberOfAppts}" /></b>
                    </apex:column>  
                </apex:pageBlockTable>
            </apex:pageBlock>
        </apex:outputPanel>
    </apex:outputPanel>
    <!-- END: 6319 changes -- added appointment panel -->
    
    <!-- MMB information stored on this account -->
    <apex:pageBlock title="MMB information on this account" rendered="{!renderMMBInformation}">
       <apex:PageBlockSection >
            <apex:PageBlockSectionItem rendered="{!acctObj.MMBCustomerNumber__c<>''}">
               <apex:outputLabel value="Customer #" for="cdetail__criteria__Custno"/>
               <apex:outputText id="cdetail__criteria__Custno" value="{!acctObj.MMBCustomerNumber__c}"/>
            </apex:PageBlockSectionItem>
            <apex:PageBlockSectionItem rendered="{!acctObj.MMB_Relo_Site_Number__c<>''}">
               <apex:outputLabel value="MMB Relo Site #" for="cdetail__criteria__ReloSiteno"/>
               <apex:outputText id="cdetail__criteria__ReloSiteno" value="{!acctObj.MMB_Relo_Site_Number__c}"/>
            </apex:PageBlockSectionItem>            
            <apex:PageBlockSectionItem rendered="{!AND(acctObj.MMB_Past_Due_Balance__c <> null, acctObj.MMB_Past_Due_Balance__c>0)}">
               <apex:outputLabel value="Past Due Balance" for="cdetail__criteria__pastdue"/>
               <apex:outputField id="cdetail__criteria__pastdue" value=" {!acctObj.MMB_Past_Due_Balance__c}" />
            </apex:PageBlockSectionItem>
            <apex:PageBlockSectionItem rendered="{!acctObj.MMBSiteNumber__c<>''}">
               <apex:outputLabel value="Site #" for="cdetail__criteria__Siteno"/>
               <apex:outputText id="cdetail__criteria__Siteno" value="{!acctObj.MMBSiteNumber__c}"/>
            </apex:PageBlockSectionItem>
            <apex:PageBlockSectionItem rendered="{!IF(AND(acctObj.MMB_Disco_Count__c<>null, acctObj.MMB_Disco_Count__c>0), true, false)}">
               <apex:outputLabel value="Non Pay Disco" for="cdetail__criteria__TimesDisco"/>
               <apex:outputText id="cdetail__criteria__TimesDisco" value="{!acctObj.MMB_Disco_Count__c}"/>
            </apex:PageBlockSectionItem>
        </apex:PageBlockSection>
    </apex:pageBlock>
    
    <!-- Existing quotes if any present for this account -->
    <apex:pageBlock id="Quotes" title="Quotes">
          <apex:pageblocktable value="{!qList}" var="q">
              <apex:column >
                  <apex:facet name="header">Quote Name</apex:facet> 
                  <!-- Added by mounika Anna --- HRM-6887 Past Due Relocations -->
                  <apex:outputPanel rendered="{!q.allowQuoteLink}"> 
                    <a href="javascript:openExistingQuote('{!q.url}','{!q.qt}');">{!q.qt.Name}</a>
                  </apex:outputPanel>
                  <apex:outputPanel rendered="{! IF(q.allowQuoteLink!=true,true,false)}">
                  {!q.qt.Name}
                  </apex:outputPanel>
              </apex:column>
              <apex:column >
                  <apex:facet name="header">Quote ID</apex:facet>
                  <apex:outputText value=" {!q.qt.scpq__QuoteId__c}" escape="false" />                  
              </apex:column>
              
              <apex:column >
                  <apex:facet name="header">Order Type</apex:facet>
                  <apex:outputText value=" {!q.qt.MMBOutOrderType__c}" escape="false" />                  
              </apex:column>
              <apex:column >
                  <apex:facet name="header">Status</apex:facet>
                  <apex:outputText value=" {!q.status}" escape="false" />                  
              </apex:column>
              <apex:column >
                  <apex:facet name="header">Job Number(s)</apex:facet>
                  <apex:outputText value=" {!q.qt.MMBJobNumber__c}" escape="false" />                  
              </apex:column>
              <apex:column >
                  <apex:facet name="header">Last Modified Timestamp</apex:facet>                  
                  <apex:outputText value=" {!q.qt.scpq__SciLastModifiedDate__c}" />
              </apex:column>
              <apex:column >
                  <apex:facet name="header">Install Amount Before Discounts</apex:facet>
                  <apex:outputField value=" {!q.qt.scpq__QuoteValueBeforeDiscounts__c}" />
              </apex:column>
              <apex:column >
                  <apex:facet name="header">Install Amount After Discounts</apex:facet>                  
                  <apex:outputField value=" {!q.qt.scpq__QuoteValueAfterDiscounts__c}" />
              </apex:column>
              <apex:column >
                  <apex:facet name="header">Monthly</apex:facet>                  
                  <apex:outputField value=" {!q.qt.QuoteValueMonthly__c}" />
              </apex:column>
          </apex:pageblocktable>
    </apex:pageBlock>
    
   </apex:form>
    
   <script>
 
     function openExistingQuote(url,quoteId){
         console.log(url+' '+quoteId);
         openQuoteInCpq(url,quoteId);
         window.open(url,"_self");
     }
    // Display message to user on document ready
    // Changes made by TCS for 5492
    var AccountId = '{!acctObj.id}';
    $j(document).ready(function(){
        var todaysDate = new Date();
        var lastMMBDate = new Date('{!acctObj.LastMMBLookupRun__c}');
        var siteID = "{!acctObj.MMBSiteNumber__c}";
        console.log('today'+todaysDate.setHours(0,0,0,0)+'lastMMdate'+lastMMBDate.setHours(0,0,0,0));
        //alert('today'+todaysDate.setHours(0,0,0,0)+'lastMMdate'+lastMMBDate.setHours(0,0,0,0));
        var allowrefresh = {!allowMMBRefresh};
        if(allowrefresh && todaysDate.setHours(0,0,0,0) != lastMMBDate.setHours(0,0,0,0) && siteID != ""){
            runcustomerloanRefresh();
            $j('#genDialog').modal('show');
             Visualforce.remoting.Manager.invokeAction(
            '{!$RemoteAction.CPQCreateRedirectController.updateAccount}', AccountId, function(result, event){
                var resultData = result;
                console.log('@@resultData '+resultData );
                if(event.status){
                    if(result != 'failure'){
                    
                    $j('#spinner').hide();
                    $j('#warningLarge').toggle();
                    $j('#modalBtnBar').show(function(){
                        $j('#accountContinue').click(function(){
                            redirectParent('/'+AccountId)});
                        $j('#quotingContinue').click(function(){
                            $j('#genDialog').modal('hide')}); 
                        if(resultData!=''){  
                            $j('#MMBRefresh').html(resultData);
                            var finalStr = '<b>{!$label.MMBLookupReview} </b><br/><br/>';
                            finalStr += $j("#MMBRefresh").text();                            
                            $j('#MMBRefresh').html(finalStr );
                            }
                            else{
                            $j('#MMBRefresh').html('{!$label.MMBSuccessfulMessageField}');
                            }
                    });
                    }
                    else{
                        $j('#MMBRefresh').html('{!$label.MMBFailedField}');
                        $j('#spinner').hide();
                        $j('#errorID').hide();
                        $j('#warningLarge').toggle();
                        $j('#quotingContinue').toggle();
                        $j('#modalBtnBar').show(function(){
                        $j('#accountContinue').click(function(){
                            redirectParent('/'+AccountId)});
                        $j('#quotingContinue').click(function(){
                            $j('#genDialog').modal('hide')});
                    });
                    }
                    //redirectParent('/'+AccountId);
                }
                else {
                    $j('#MMBRefresh').html('{!$label.MMBFailedField}');
                    $j('#spinner').hide();
                    $j('#warningLarge').toggle();
                    $j('#quotingContinue').show();
                    $j('#modalBtnBar').show(function(){
                        $j('#accountContinue').click(function(){
                            redirectParent('/'+AccountId)});
                        $j('#quotingContinue').click(function(){
                            $j('#genDialog').modal('hide')});
                    });
                }
                }, {escape: true});
       }
       //END of changes 5492
        checkMessages();
    });
    
    // Pop-up window with users messages
    function showSplashMessages() {        
        window.open("/apex/SplashPopupMessage", "Messages", "width=650, height=400, menubar=0, top=250, left=350, location=0");
    }
     <!-- HRM 9790 -->
    function runcustomerloanRefresh(){
        console.log('Running customer loans');
        runcustomerLoanUtility();
        return false;
    }
    // Check  to make sure we have messages to display
    function checkMessages() {
            SplashPopupMessageController.checkMessages(function(result, event) {                
                if(event.status) {                    
                    if (result === true) {
                        showSplashMessages();
                     }
                }
            });
        }
      function redirectParent(url){
            window.top.location.href = url; 
        } 
        
        //start- 6319 changes
    function expandColumn(colId)
    {
        twistSection(document.getElementById(colId).getElementsByTagName('img')[0])
    }
     //End- 6319 changes
    
   </script>
   
   
    <!-- HRM-5925 TCS added modal -->
    
    <div class="force-bs force-bs-modal">        
        
        <div class="modal" id="genDialog" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog"><!-- style="width: 450px;margin: 130px auto;">-->
            <div class="modal-content">
              <div class="modal-header">
                <div class="modal-header-content" id="genDialogModalLabel">
                     <span style="color: #000;font-size: 1.25em;font-weight: bold;">Message</span>
                </div>
              </div>
              <div class="modal-body" id="modal-body-content" style="background: #fff url('/img/alohaSkin/overlay_crns.png') left -240px repeat-x;text-align: center;">
                <table><tbody><tr>
                    <td  style="vertical-align: top"><i id="spinner" class="fa fa-spinner fa-spin" style="font-size:40px"></i>
                    <div id="warningLarge" style="display: none;"><img src="/s.gif" class="warningLarge" alt="Warning"/>
                    <img alt="ERROR" class="msgIcon" src="/s.gif" title="ERROR" id="errorID" style="display:none;"/></div> 
                    </td>
                    <td style ="padding-left: 20px; vertical-align: top; line-height: 16px; text-align: left;">
                        <span id="MMBRefresh" style="color: #000;font-size: 1.25em;">{!$label.MMBSearchInProgress}</span></td></tr>
               </tbody> </table>
                <!--<img src="/img/loading.gif" />-->
              </div>
              <div class="modal-footer" >
                 <div id="modalBtnBar" style="display:none;" align="center">
                    <input type="button" class="btn" value="{!$label.AccountButtonLabel}" id="accountContinue"/>
                    <input type="button" class="btn" value="{!$label.QuotingButtonLabel}" id="quotingContinue" style="display:none;"/>
                 </div>
              </div>
            </div>
          </div>
        </div>
        
    </div>

    <!-- bootstrap based dialogs with custom css to match salesforce's reach UI -->
    <!-- 
    <div class="force-bs force-bs-modal">        
        
        <div class="modal" id="genDialog" tabindex="-1" role="dialog" aria-hidden="true" >
          <div class="modal-dialog" style="width: 450px;margin: 130px auto;">
            <div class="modal-content">
              <div class="modal-header">
                <div class="modal-header-content" id="genDialogModalLabel">
                    <span style="color: #000;font-size: 1.25em;font-weight: bold;">&nbsp;</span>
                </div>
              </div>
              <div class="modal-body" id="modal-body-content" style="background: #fff url('/img/alohaSkin/overlay_crns.png') left -240px repeat-x;text-align: left;">
                <img src="/img/loading.gif" /> <span style="color: #000;font-size: 1.25em;font-weight: bold;"> Please wait while we check required information.</span>
              </div>
              <div class="modal-footer" >
                 <div id="modalBtnBar" style="display:none;">
                    <input type="button" class="btn" value="Close" id="btn-close-modal"/>
                 </div>
              </div>
            </div>
          </div>
        </div>
        
    </div>
    -->
    
</apex:page>