<apex:page controller="NSCCreditScoreCheckController">

    <!-- CSS -->
    <link rel="stylesheet" href="{!URLFOR($Resource.bootstrap3, 'css/bootstrap.min.css')}" media="all" />

    <!-- Service Console Toolkit -->
    <apex:includeScript value="/soap/ajax/27.0/connection.js" />
    <apex:includeScript value="/support/console/30.0/integration.js" />

    <!-- JavaScript Libraries -->
    <apex:includeScript value="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" />
    <apex:includeScript value="{!URLFOR($Resource.bootstrap3, 'js/bootstrap.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.NSCAppResources, 'js/jquery.mask.js')}" />

    <style>
    .force-bs .page-header {
        margin: 10px 0 10px;
    }
    
    .ssn_no {
        float: left;
    }
    
    .separator {
        float: left;
        padding-left: 5px;
        padding-right: 5px;
    }
    
    .ssn_no3 {
        width: 25px;
    }
    
    .ssn_no2 {
        width: 15px;
    }
    
    .ssn_no4 {
        width: 30px;
    }
    
    .dob {
        float: left;
    }
    
    .dob_day {
        width: 15px;
    }
    
    .dob_year {
        width: 30px;
    }
    
    .force-bs #modalConfirmSelect .modal-header {
        background: url('/img/alohaSkin/overlay_crns.png') right top no-repeat;
        padding: 0px 6px 0px 0px
    }
    
    .force-bs #modalConfirmSelect .modal-header .modal-header-content {
        padding: 8px 6px 8px 12px;
        background: url('/img/alohaSkin/overlayTopLft.png') left top no-repeat;
    }
    
    .modal-backdrop.in {
        opacity: .5;
    }
    
    .modal-backdrop {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1040;
        background-color: #000;
    }
    
    .confirmMsgHeader {
        font-size: 20px;
        margin-bottom: 10px;
        font-family: inherit;
        font-weight: 500;
        line-height: 1.1;
        color: inherit;
    }
    
    .header-link {
        float: right;
        padding-right: 5px;
        width: 30%;
        vertical-align: middle;
        text-align: right;
    }
    
    .header-link-content {
        float: right;
        font-size: 1.083em;
        font-weight: bold;
        color: #353535;
    }
    
    .header-link-back {
        background: url(/img/chatter/messages_sprite.png) no-repeat scroll 0 -32px;
        width: 20px;
        height: 12px;
        float: right;
        margin: 2px 0px;
    }

    #consentDiv {
        border: 2px solid #1797c0;
        padding: 10px;
        border-radius: 25px;
    }
    </style>

    <!-- Confirmation Message -->
    <div class="force-bs" id="dlg-msg-section">
        <div class="modal fade" id="modalConfirmSelect" tabindex="-1" role="dialog" aria-labelledby="modalConfirmSelectLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="modal-header-content" id="msgDialogModalLabel">
                            <span style="color: #000; font-size: 1.25em; font-weight: bold;">Credit Check</span>
                        </div>
                    </div>
                    <div class="modal-body" style="background: #fff url('/img/alohaSkin/overlay_crns.png') left -240px repeat-x; text-align: center;">
                        <table border="0">
                            <tbody>
                                <tr>
                                    <td style="vertical-align: top"><img src="/s.gif" class="infoLarge" alt="Info" /></td>
                                    <td style="padding-left: 20px; vertical-align: top; line-height: 16px; text-align: left;">
                                        <apex:outputPanel id="confirmMsgContent" styleClass="confirmMsgContent">
                                            <apex:outputText value="{!requireSelectConfirmMsg}" escape="false" styleClass="requireSelectConfirmMsg"/>
                                            <div id="CustomerOfferSelection"></div>
                                        </apex:outputPanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <apex:outputPanel id="confirmMsgButtons" styleClass="confirmMsgButtons">
                            <button type="button" id="retryBtn" data-loading-text="Loading..." class="btn btn-default" autocomplete="off" 
                            onClick="onAgentRetry()" style="display: none;">Retry <span class="badge">{!CreditCheckAttempts}</span></button>
                            <button type="button" id="closeBtn" data-loading-text="Loading..." data-dismiss="modal" class="btn btn-active" 
                            onClick="onCloseBtnAction()" autocomplete="off">Close</button>
                        </apex:outputPanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <apex:form id="mainFrm">     
        <!-- Modal -->
        <apex:outputPanel rendered="{!AND(tncSwitch, showTnCOnce)}">
           <div class="force-bs" id="dlg-msg-section-modal">
                <div class="modal fade" id="tncModal" tabindex="-1" role="dialog" aria-labelledby="tncModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header" style="text-align: center;">
                                <div class="modal-header-content" id="msgDialogModal">
                                   <apex:outputPanel rendered="{!renderFieldView}">
                                        <header class="slds-modal__header">
                                            <h2 id="modal-heading-01" class="modal-title">ADT FIELD SALES DISCLOSURES</h2>
                                        </header>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!renderPhoneView}">
                                        <header>
                                            <h2 id="modal-heading-02" class="modal-title">ADT PHONE SALES DISCLOSURES</h2>
                                        </header>
                                    </apex:outputPanel>
                                </div>
                            </div>
                            <div class="modal-body" style="background: #fff url('/img/alohaSkin/overlay_crns.png') left -240px repeat-x; text-align: justify;">
                               <apex:outputText value="{!termsandconditions}" escape="false"/>
                            </div>
                            <div class="modal-footer">
                                <apex:outputPanel style="float: left;" rendered="{!showTnCCheckBox}">
                                    <apex:inputCheckbox value="{!tncAccepted}"/>
                                    I agree to receive electronic communications from ADT
                                </apex:outputPanel>
                                <a class="btn btn-primary" data-dismiss="modal">Close</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </apex:outputPanel>

        <!-- Phone agent custom page header -->
        <apex:outputPanel styleClass="force-bs" rendered="{!AND(renderMainFrm, renderPhoneView)}">
            <div class="page-header">
                <h3>
                    <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
                    Can you tell us your information ?
                </h3>
            </div>
        </apex:outputPanel>

        <!-- Field agent page header -->
        <apex:outputPanel rendered="{!renderFieldView}">
            <div class="bPageTitle">
                <div class="ptBody secondaryPalette brandSecondaryBrd">
                    <div class="content">
                        <img src="/s.gif" class="pageTitleIcon" title="Account"
                            alt="Account" />
                        <h1 class="pageType">Validate customer credit</h1>
                        <span class="titleSeparatingColon">:</span>
                        <h2 class="pageDescription">{!a.name}</h2>
                        {!a.SiteStreet__c} {!a.SiteStreet2__c} {!a.SiteCity__c}
                        {!a.AddressID__r.State__c} {!a.SitePostalCode__c}
                        <div class="blank"></div>
                    </div>
                    <div class="header-link force-bs">
                        <span class="header-link-content"> 
                            <a href="/{!a.Id}" target="_self" id="goBack">Go Back</a>
                        </span> <img src="/s.gif" class="header-link-back" />
                    </div>
                </div>
            </div>
        </apex:outputPanel>

        <script>
            var CreditCheckAttempts = {!CreditCheckAttempts};
            var isAcceptConditionSuccess = {!isAcceptConditionSuccess};
            var onAcceptConditionComplete;
        </script>
        <apex:pageMessages escape="false" />
        <apex:outputPanel rendered="{!AND(doSoftCheck,!tncSwitch)}">
            <div id="consentDiv">
                <p>{!$Label.creditCheckConsentMessage}</p>
            </div><br/>
        </apex:outputPanel>
        <apex:pageBlock mode="edit" rendered="{!renderMainFrm}">
            <apex:pageBlockSection columns="2">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="First Name" for="cc__FName" />
                    <apex:outputText value="{!FirstName}" id="cc__FName" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Last Name" for="cc__LName" />
                    <apex:outputText value="{!LastName}" id="cc__LName" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >SSN</apex:outputLabel>
                    <apex:outputPanel >
                        <apex:inputSecret maxlength="3" id="ssn_3" value="{!ssnNumber3}" styleClass="ssn_no ssn_no3" html-placeholder="XXX" />
                        <span class="separator">-</span>
                        <apex:inputSecret maxlength="2" id="ssn_2" value="{!ssnNumber2}" styleClass="ssn_no ssn_no2" html-placeholder="XX" />
                        <span class="separator">-</span>
                        <apex:inputText maxlength="4" id="ssn_4" value="{!ssnNumber4}" required="true" styleClass="ssn_no ssn_no4" html-placeholder="XXXX" html-data-rule-required="true" html-data-msg-required="Please enter at least the last four" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <!--Added by TCS HRM 5400 -->
                <apex:pageBlockSectionItem id="pbsi" rendered="{!CONTAINS(a.Business_Id__c,'1100')}">
                    <apex:outputLabel >Date of birth</apex:outputLabel>
                    <apex:outputPanel >
                        <apex:selectList size="1" styleClass="dob" value="{!dobMonth}" id="sl">
                            <apex:selectOption itemValue="1" itemLabel="January" />
                            <apex:selectOption itemValue="2" itemLabel="February" />
                            <apex:selectOption itemValue="3" itemLabel="March" />
                            <apex:selectOption itemValue="4" itemLabel="April" />
                            <apex:selectOption itemValue="5" itemLabel="May" />
                            <apex:selectOption itemValue="6" itemLabel="June" />
                            <apex:selectOption itemValue="7" itemLabel="July" />
                            <apex:selectOption itemValue="8" itemLabel="August" />
                            <apex:selectOption itemValue="9" itemLabel="September" />
                            <apex:selectOption itemValue="10" itemLabel="October" />
                            <apex:selectOption itemValue="11" itemLabel="November" />
                            <apex:selectOption itemValue="12" itemLabel="December" />
                        </apex:selectList>
                        <!--End by TCS HRM 5400-->
                        <span class="separator"> / </span>
                        <apex:inputText value="{!dobDayOfMonth}" maxlength="2" id="dob_day" styleClass="dob dob_day" html-placeholder="dd" />
                        <span class="separator"> / </span>
                        <apex:inputText value="{!dobYear}" maxlength="4" id="dob_year" styleClass="dob dob_year" html-placeholder="yyyy" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            <apex:outputPanel >
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="50%" style="padding-left: 5px;" valign="top">
                            <apex:pageBlockSection title="Previous Address">
                                <apex:pageBlockSectionItem >
                                    <apex:outputLabel value="Street" for="cc__PrevSiteStreet"/>
                                    <apex:inputText value="{!ccPrevStreet}" id="cc__PrevSiteStreet" maxlength="80"/>
                                </apex:pageBlockSectionItem>
                                <apex:pageBlockSectionItem >
                                    <apex:outputLabel >
                                        <!-- Divisor to push inputs -->
                                    </apex:outputLabel>
                                </apex:pageBlockSectionItem>
                                <apex:pageBlockSectionItem >
                                    <apex:outputLabel value="Street 2" for="cc__PrevSiteStreet2"/>
                                    <apex:inputText value="{!ccPrevStreet2}" id="cc__PrevSiteStreet2" maxlength="80"/>
                                </apex:pageBlockSectionItem>
                                <apex:pageBlockSectionItem >
                                    <apex:outputLabel >
                                        <!-- Divisor to push inputs -->
                                    </apex:outputLabel>
                                </apex:pageBlockSectionItem>
                                <apex:pageBlockSectionItem >
                                    <apex:outputLabel value="City" for="cc__PrevCity"/>
                                    <apex:inputText value="{!ccPrevCity}" id="cc__PrevCity" maxlength="30"/>
                                </apex:pageBlockSectionItem>
                                <apex:pageBlockSectionItem >
                                    <apex:outputLabel >
                                        <!-- Divisor to push inputs -->
                                    </apex:outputLabel>
                                </apex:pageBlockSectionItem>
                                <apex:pageBlockSectionItem >
                                    <apex:outputLabel value="State" for="cc__PrevState" />
                                    <apex:selectList size="1" styleClass="dob" value="{!ccPrevState}" id="cc__PrevState">
                                        <apex:selectOption itemValue="" itemLabel="- Select -" />
                                        <apex:selectOption itemValue="AL" itemLabel="Alabama" />
                                        <apex:selectOption itemValue="AK" itemLabel="Alaska" />
                                        <apex:selectOption itemValue="AZ" itemLabel="Arizona" />
                                        <apex:selectOption itemValue="AR" itemLabel="Arkansas" />
                                        <apex:selectOption itemValue="CA" itemLabel="California" />
                                        <apex:selectOption itemValue="CO" itemLabel="Colorado" />
                                        <apex:selectOption itemValue="CT" itemLabel="Connecticut" />
                                        <apex:selectOption itemValue="DE" itemLabel="Delaware" />
                                        <apex:selectOption itemValue="DC" itemLabel="District of Columbia" />
                                        <apex:selectOption itemValue="FL" itemLabel="Florida" />
                                        <apex:selectOption itemValue="GA" itemLabel="Georgia" />
                                        <apex:selectOption itemValue="HI" itemLabel="Hawaii" />
                                        <apex:selectOption itemValue="ID" itemLabel="Idaho" />
                                        <apex:selectOption itemValue="IL" itemLabel="Illinois" />
                                        <apex:selectOption itemValue="IN" itemLabel="Indiana" />
                                        <apex:selectOption itemValue="IA" itemLabel="Iowa" />
                                        <apex:selectOption itemValue="KS" itemLabel="Kansas" />
                                        <apex:selectOption itemValue="KY" itemLabel="Kentucky" />
                                        <apex:selectOption itemValue="LA" itemLabel="Louisiana" />
                                        <apex:selectOption itemValue="ME" itemLabel="Maine" />
                                        <apex:selectOption itemValue="MD" itemLabel="Maryland" />
                                        <apex:selectOption itemValue="MA" itemLabel="Massachusetts" />
                                        <apex:selectOption itemValue="MI" itemLabel="Michigan" />
                                        <apex:selectOption itemValue="MN" itemLabel="Minnesota" />
                                        <apex:selectOption itemValue="MS" itemLabel="Mississippi" />
                                        <apex:selectOption itemValue="MO" itemLabel="Missouri" />
                                        <apex:selectOption itemValue="MT" itemLabel="Montana" />
                                        <apex:selectOption itemValue="NE" itemLabel="Nebraska" />
                                        <apex:selectOption itemValue="NV" itemLabel="Nevada" />
                                        <apex:selectOption itemValue="NH" itemLabel="New Hampshire" />
                                        <apex:selectOption itemValue="NJ" itemLabel="New Jersey" />
                                        <apex:selectOption itemValue="NM" itemLabel="New Mexico" />
                                        <apex:selectOption itemValue="NY" itemLabel="New York" />
                                        <apex:selectOption itemValue="NC" itemLabel="North Carolina" />
                                        <apex:selectOption itemValue="ND" itemLabel="North Dakota" />
                                        <apex:selectOption itemValue="OH" itemLabel="Ohio" />
                                        <apex:selectOption itemValue="OK" itemLabel="Oklahoma" />
                                        <apex:selectOption itemValue="OR" itemLabel="Oregon" />
                                        <apex:selectOption itemValue="PA" itemLabel="Pennsylvania" />
                                        <apex:selectOption itemValue="PR" itemLabel="Puerto Rico" />
                                        <apex:selectOption itemValue="RI" itemLabel="Rhode Island" />
                                        <apex:selectOption itemValue="SC" itemLabel="South Carolina" />
                                        <apex:selectOption itemValue="SD" itemLabel="South Dakota" />
                                        <apex:selectOption itemValue="TN" itemLabel="Tennessee" />
                                        <apex:selectOption itemValue="TX" itemLabel="Texas" />
                                        <apex:selectOption itemValue="UT" itemLabel="Utah" />
                                        <apex:selectOption itemValue="VT" itemLabel="Vermont" />
                                        <apex:selectOption itemValue="VA" itemLabel="Virginia" />
                                        <apex:selectOption itemValue="VI" itemLabel="Virgin Islands" />
                                        <apex:selectOption itemValue="WA" itemLabel="Washington" />
                                        <apex:selectOption itemValue="WV" itemLabel="West Virginia" />
                                        <apex:selectOption itemValue="WI" itemLabel="Wisconsin" />
                                        <apex:selectOption itemValue="WY" itemLabel="Wyoming" />
                                    </apex:selectList>
                                </apex:pageBlockSectionItem>
                                <apex:pageBlockSectionItem >
                                    <apex:outputLabel >
                                        <!-- Divisor to push inputs -->
                                    </apex:outputLabel>
                                </apex:pageBlockSectionItem>
                                <apex:pageBlockSectionItem >
                                    <apex:outputLabel value="ZIP Code" for="cc__PrevPostalCode" />
                                    <apex:inputText value="{!ccPrevPostalCode}" id="cc__PrevPostalCode" maxLength="9"/>
                                </apex:pageBlockSectionItem>
                            </apex:pageBlockSection>
                        </td>
                    </tr>
                </table>
            </apex:outputPanel>
            <apex:pageBlockButtons location="bottom">
                <input type="button" id="getCreditInformationBtn" class="btn" value="Get Credit" />
            </apex:pageBlockButtons>
        </apex:pageBlock>

        <!-- Page Actions -->
        <apex:actionFunction name="getFullCreditScoreJS"
            action="{!GetConsumerScoreInformation}"
            reRender="mainFrm,confirmMsgContent,confirmMsgButtons"
            onComplete="onCreditScoreComplete( '{!EquifaxConditionJSON}', {!isCreditCheckError}, { isNoHit: {!isNoHit}, attempts: {!CreditCheckAttempts} } )" />
    </apex:form>
    <script>
        var fireAcceptConditionComplete = false;
        var EnclosingTabSessionId;
        var isNumberKey = function( e ){
            var e = e || window.event;
            var key = e.keyCode || e.which;
            key = String.fromCharCode( key );
            var regex = /[0-9]|\./; 
            return regex.test( key );       
        }
        
        //-------------------------------------------------------------------------------------------------
        //  SSN Component Handler and features
        //-------------------------------------------------------------------------------------------------
        var SSNEntry = {
            registerComponent: function(){
                // SSN-3
                $("[id$=ssn_3]").mask('000');
                $("[id$=ssn_3]").keyup( function( event ){
                    if( isNumberKey( event ) ){
                        if( $(this).val().length == 3 ){
                            $("[id$=ssn_2]").focus();
                        }
                    }
                });             
                // SSN-2
                $("[id$=ssn_2]").mask('00');
                $("[id$=ssn_2]").keyup( function( event ){
                    if( isNumberKey( event ) ){
                        if( $(this).val().length == 2 ){
                            $("[id$=ssn_4]").focus();
                        }
                    }
                });             
                // SSN-4
                $("[id$=ssn_4]").mask('0000');
            }
        };
                
        //-------------------------------------------------------------------------------------------------
        //  DOB Component Handler and features
        //-------------------------------------------------------------------------------------------------
        var DOBEntry = {
            registerComponent: function(){
                // DOB - Day of month
                $("[id$=dob_day]").mask('00');
                $("[id$=dob_day]").keyup( function( event ){
                    if( isNumberKey( event ) ){
                        if( $(this).val().length == 2 ){
                            $("[id$=dob_year]").focus();
                        }
                    }
                });
                // DOB - Year
                $("[id$=dob_year]").mask('0000');
            }
        };
        
        //-------------------------------------------------------------------------------------------------
        //  Credit Scoring action Handler and callback features
        //-------------------------------------------------------------------------------------------------
        var CreditScoring = {
            setConfirmMsgBtnLoadingState: function(){
                $('#closeBtn').button('loading');
            },
            setConfirmMsgBtnResetState: function(){
                $('#closeBtn').button('reset');
            },
            onGetCreditStart: function( startCreditCheck ){
                var disableCreditCheckBtn = function(){
                    $('#getCreditInformationBtn').prop( "disabled","disabled" );
                    $('#getCreditInformationBtn').prop( "value","validating..." );
                    $('#getCreditInformationBtn').addClass('btnDisabled');
                    $('#CustomerOfferSelection').html( '' );
                    CreditScoring.setConfirmMsgBtnLoadingState();
                    $('#modalConfirmSelect').modal({backdrop: 'static',keyboard:false, show: true});                    
                }();
                var requestMessage= "We're searching for conditions depending on your credit information.";
                loadingConditionStatus = $('<span style="color: #F00; font-weight: bold;"><img src="/apexpages/devmode/img/saveStatus.gif" />Searching customer ...</span>');
                $("#CustomerOfferSelection").append( loadingConditionStatus );   
                $("#retryBtn").button('reset').hide();
                $(".requireSelectConfirmMsg").html( requestMessage );   
                if( startCreditCheck ){
                    getFullCreditScoreJS();
                }
            },
            onGetCreditComplete: function(){
                var enableCreditCheckBtn = function(){
                    $('#getCreditInformationBtn').removeProp( "disabled" );
                    $('#getCreditInformationBtn').prop( "value","Get Credit" );
                    $('#getCreditInformationBtn').removeClass('btnDisabled');
                    CreditScoring.setConfirmMsgBtnResetState();
                }();
                registerUIComponentHandlers();
            },
            registerComponent: function(){
                $("#getCreditInformationBtn").click( onCreditScoreStart );
            }
        };
        
        //-------------------------------------------------------------------------------------------------
        //  Global function expressions
        //-------------------------------------------------------------------------------------------------
        var onCreditScoreStart = function(){
            // run initial validations
            var ssn = $("[id$=ssn_4]").val();
            var isAddressEntered = $('[id$=cc__PrevSiteStreet]').val() || $('[id$=cc__PrevSiteStreet2]').val() || $('[id$=cc__PrevCity]').val() || $('[id$=cc__PrevState]').val() != "" || $('[id$=cc__PrevPostalCode]').val();
            var PostalCode_length = $('[id$=cc__PrevPostalCode]').val().length;
            if(!ssn || ssn.length < 4){       
                alert('Last four of the social required.');
            }else if( !$('[id$=dob_year]').val() || !$('[id$=dob_day]').val() ) {
                alert ("Date of Birth is Required");
            }else if(checkDobIsInValid()){
                //5400 -- DOB validtiy fix
                alert('Invalid Date of Birth');
            }else if(isAddressEntered && (!$('[id$=cc__PrevCity]').val() || !$('[id$=cc__PrevPostalCode]').val() || $('[id$=cc__PrevState]').val() == "")){
                alert ("City, State & Zip Code are mandatory");
            }else if(isAddressEntered && $('[id$=cc__PrevPostalCode]').val() && !(PostalCode_length == 5 || PostalCode_length == 9)){
                alert('Zip Code is not valid');
            }else{
                CreditScoring.onGetCreditStart( true );
            }
        };
        function checkDobIsInValid(){
            // HRM# 5400 - Check valid DOB
            var dobString = $("select[id$='sl']").val()+'/'+ $('[id$=dob_day]').val()+'/'+$('[id$=dob_year]').val();
            var dobDateFormat = new Date(dobString);
            var currentDate = new Date();
            console.log('DOB entered'+dobDateFormat);
            return (dobDateFormat == 'Invalid Date' || (dobDateFormat.getMonth()+1) != $('select[id$=sl]').val() || dobDateFormat > currentDate);
        }
        /*
         *  Callback function executed to complete a credit check operation could be a T2 or Credit
         *
         *  @method onCreditScoreComplete
         *  @param EquifaxConditionJSON Condition applied on credit results, this is an instance of a custom settings object
         *  @param isCreditCheckError   This flag is turned on for errors on the credit check
         *  @param CreditCheckState     Additional information received as an object literal sent to this function
         *  {
         *      isNoHit: false, // This is an example value for this object property, informs if this operation resulted in a no hit from Equifax
         *      attempts: 0     // Number of attempts the user has left, we track this value on the server and inform the client how many attempts he has left
         *  }
         */
        var onCreditScoreComplete = function( EquifaxConditionJSON, isCreditCheckError, CreditCheckState ){
            CreditScoring.onGetCreditComplete();
            if ( CreditCheckState.isNoHit === true || isCreditCheckError === true){
                // Automatically fire the refresh credit event if we're done checking
                sforce.console.fireEvent('NSCServiceConsole.view.CreditCondition.'+EnclosingTabSessionId, 'REFRESH',null );                
            }
            if( isCreditCheckError === true ){
                if( CreditCheckAttempts > 0 ){
                    $("#retryBtn").button('reset').show();
                    creditErrMsg = $('<span style="color: red; font-weight: bold;">Could not complete a credit check. Please retry or contact an admin.</span>');
                }else{
                    creditErrMsg = $('<span style="color: red; font-weight: bold;">Maximum number of attempts reached. This condition was applied.</span>');                    
                }
                fireAcceptConditionComplete = true;
                $("#CustomerOfferSelection").html( creditErrMsg );
            }else if ( CreditCheckState.isNoHit === true ){
                $('#modalConfirmSelect').modal('hide');
            }else {
                $("#CustomerOfferSelection").html( '' );
                fireAcceptConditionComplete = true;
            }
        };
        var registerUIComponentHandlers = function(){
            // because the form is re-rendered we need to re-register component features tied to DOM events
            SSNEntry.registerComponent();
            DOBEntry.registerComponent();
            CreditScoring.registerComponent();
        }; 
        var onAcceptConditionComplete = function( isSuccess ){
            CreditScoring.setConfirmMsgBtnResetState();
            if( isSuccess === true && sforce.console.isInConsole() ){
                var CreditConditionCallBack = function( result ){
                    if ( result.success ){
                        var closeSubtab = function( result ){
                            var tabId = result.id;                      
                            sforce.console.closeTab( tabId );                       
                        };
                        sforce.console.getEnclosingTabId( closeSubtab );
                    }
                };
                // message sent is not handled, only the event itself is relevant
                sforce.console.fireEvent('NSCServiceConsole.view.CreditCondition.'+EnclosingTabSessionId, 'REFRESH', CreditConditionCallBack );
            }else if ( isSuccess === true ){
                $("#goBack").trigger('click');
            }
        };
        var onAgentRetry = function(){
            $('#closeBtn').button('loading');
            $('#retryBtn').hide();
            $(".requireSelectConfirmMsg").html( 'We are searching for conditions depending on your credit information.' );
            $("#CustomerOfferSelection").html( '' );
            getFullCreditScoreJS();
        };
        var onCloseBtnAction = function( ){
            if( fireAcceptConditionComplete ){
                onAcceptConditionComplete( true );
            }
        };
        var setEnclosingTabSessionId = function( result ){
            EnclosingTabSessionId = result.id;
            registerUIComponentHandlers();
        };
        
        /**
         *  DOM Ready
         */
        $( document ).ready( function(){
            $('#tncModal').modal({backdrop: 'static',keyboard:false, show: true});
            if (sforce.console.isInConsole()) {
                sforce.console.getEnclosingPrimaryTabId( setEnclosingTabSessionId );
            } else {
                registerUIComponentHandlers();
            }
        });
    </script>
</apex:page>