<apex:page standardController="StreetSheet__c" extensions="ManageProspectListController" title="Prospect List" action="{!displayPageMessage}">

    <apex:includeScript value="{!$Resource.JQuery}" />
    <apex:includeScript value="{!URLFOR($Resource.JQueryUI, 'JQueryUI/JQueryUI.js')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.JQueryUI, 'JQueryUI/JQueryUI.css')}"/>
    <apex:stylesheet value="{!$Resource.ProspectInlineEdit_CSS}" />
    
    <apex:includeScript value="{!$Resource.iscroll}" />
    
    <style>

		table.p-items a {
		cursor: pointer;
		color: #015BA7;
		}
		.relatedListIcon{
			background-image: url(/img/icon/alohaProfile24.png);
			background-position: 0 0;
			height: 24px;
			width: 24px;		
			margin-top: 1px;
			display: inline;
		}
		.selCol {
			background-color: #e3f3ff;
		}
		.touchedItem {
			background-color: #e3e3e3;		
		}
		
    </style>
    
    <script type="text/javascript">
        var j$ = jQuery.noConflict();
                
        function redirectParent(href) {
            window.top.location.href = href;
        }
    </script>
    
    <apex:form >
        <apex:pageMessages />
        <div id="dispPopup" style="overflow:auto">
            
            <apex:pageBlock id="dispBlock" title="Disposition" >
            
                <apex:pageBlockSection id="dispBlockHeader">
                    <apex:outputField value="{!dispLead.Company}" rendered="{!AND(NOT(ISNULL(dispLead)), contains(dispLead.Channel__c, 'SB'))}" />
                    <apex:outputField value="{!dispLead.Name}" rendered="{!AND(NOT(ISNULL(dispLead)), NOT(contains(dispLead.Channel__c, 'SB')))}" />
                    <apex:outputField value="{!dispAccount.Name}" rendered="{!NOT(ISNULL(dispAccount))}" />
                </apex:pageBlockSection>
            
                <div id="wrapper" class="dispFieldList">
        
                    <div id="scroller">
        
                        <apex:outputPanel layout="block" id="dispFieldList" >
                
                            <apex:inputField value="{!dispLead.Business_Id__c}" style="display: none;" rendered="{!NOT(ISNULL(dispLead))}" />
                        
                            <apex:pageBlockSection columns="1" collapsible="false" id="leadDisposition" rendered="{!NOT(ISNULL(dispLead))}">
                                <!-- <apex:outputField value="{!dispLead.Company}" /> -->
                                <apex:inputField value="{!dispLead.DispositionCode__c}" />
                                <apex:inputField value="{!dispLead.Repeat_Disposition__c}" />
                                <apex:inputField value="{!dispLead.DispositionDetail__c}" />
                                <apex:inputField value="{!dispLead.DispositionComments__c}" />
                                <apex:inputField value="{!dispLead.ClosePotential__c}" />
                                <apex:inputField value="{!dispLead.Competitor__c}" />
                                <apex:inputField value="{!dispLead.CompetitorContractEndDate__c}" />
                                <apex:inputField value="{!dispLead.ExistingSolutions__c}" />
                                <apex:inputField value="{!dispLead.DoNotCall}" />
                                <apex:inputField value="{!dispLead.DoNotMail__c}" />
                            </apex:pageBlockSection>
                            
                            <apex:inputField value="{!dispAccount.Business_Id__c}" style="display: none;" rendered="{!NOT(ISNULL(dispAccount))}" />
                            
                            <apex:pageBlockSection columns="2" collapsible="false" id="accountDispositionResale" rendered="{!AND(NOT(ISNULL(dispAccount)),dispAccountOther)}">
                                <!-- <apex:outputField value="{!dispAccount.Name}" /> -->
                                <apex:inputField value="{!dispAccount.DispositionCode__c}" />
                                <apex:inputField value="{!dispAccount.DispositionDetail__c}" />
                                <apex:inputField value="{!dispAccount.Repeat_Disposition__c}" />
                                <apex:inputField value="{!dispAccount.CreateFollowupAfter__c}" />
                                <apex:inputField value="{!dispAccount.DispositionComments__c}" />
                                <apex:inputField value="{!dispAccount.ClosePotential__c}" />
                                <apex:inputField value="{!dispAccount.Competitor__c}" />
                                <apex:inputField value="{!dispAccount.CompetitorContractEndDate__c}" />
                                <apex:inputField value="{!dispAccount.ExistingSolutions__c}" />
                                <apex:inputField value="{!dispAccount.DoNotCall__c}" />
                                <apex:inputField value="{!dispAccount.DoNotMail__c}" />
                            </apex:pageBlockSection>
                            
                            <!-- <apex:pageBlockSection columns="2" collapsible="false" id="accountDispositionRIF" rendered="{!AND(NOT(ISNULL(dispAccount)),dispAccountRIF)}">
                                <apex:inputField value="{!dispAccount.DispositionCode__c}" />
                                <apex:inputField value="{!dispAccount.DispositionDetail__c}" />
                                <apex:inputField value="{!dispAccount.Repeat_Disposition__c}" />
                                <apex:inputField value="{!dispAccount.CreateFollowupAfter__c}" />
                                <apex:inputField value="{!dispAccount.PackageProposed__c}" />
                                <apex:inputField value="{!dispAccount.ExistingSolutions__c}" />
                                <apex:inputField value="{!dispAccount.ModelSalesCallConducted__c}" />
                                <apex:inputField value="{!dispAccount.ROIDetermined__c}" />
                                <apex:inputField value="{!dispAccount.PulseDemonstrated__c}" />
                                <apex:inputField value="{!dispAccount.VideoDiscussed__c}" />
                                <apex:inputField value="{!dispAccount.AccessDiscussed__c}" />
                                <apex:inputField value="{!dispAccount.ResidentialOffer__c}" />
                                <apex:inputField value="{!dispAccount.DoesHomeownerOwnOrManageABusines__c}" />
                                <apex:inputField value="{!dispAccount.ADSCQuoted__c}" />
                            </apex:pageBlockSection>             -->                
                            
                            <apex:pageBlockSection columns="2" collapsible="false" id="accountDispositionStandard" rendered="{!AND(NOT(ISNULL(dispAccount)),OR(dispAccountStandard,dispAccountRIF))}">
                                <!-- <apex:outputField value="{!dispAccount.Name}" /> -->
                                <apex:inputField value="{!dispAccount.DispositionCode__c}" />
                                <apex:inputField value="{!dispAccount.DispositionDetail__c}" />
                                <apex:inputField value="{!dispAccount.Repeat_Disposition__c}" />
                                <apex:inputField value="{!dispAccount.CreateFollowupAfter__c}" />
                                <apex:inputField value="{!dispAccount.DispositionComments__c}" />
                                <apex:inputField value="{!dispAccount.PackageProposed__c}" />
                                <apex:inputField value="{!dispAccount.Reason1ForSystem__c}" rendered="{!showReasonSystem}" />
                                <apex:inputField value="{!dispAccount.Reason1DetailForSystem__c}" rendered="{!showReasonDetail}" />
                                <apex:inputField value="{!dispAccount.ClosePotential__c}" />
                                <apex:inputField value="{!dispAccount.Competitor__c}" />
                                <apex:inputField value="{!dispAccount.CompetitorContractEndDate__c}" />
                                <apex:inputField value="{!dispAccount.ExistingSolutions__c}" />
                                <apex:inputField value="{!dispAccount.ModelSalesCallConducted__c}" />
                                <apex:inputField value="{!dispAccount.ROIDetermined__c}" />
                                <apex:inputField value="{!dispAccount.PulseDemonstrated__c}" />
                                <apex:inputField value="{!dispAccount.VideoDiscussed__c}" />
                                <apex:inputField value="{!dispAccount.AccessDiscussed__c}" />
                                <apex:inputField value="{!dispAccount.ResidentialOffer__c}" />
                                <apex:inputField value="{!dispAccount.DoesHomeownerOwnOrManageABusines__c}" />
                                <apex:inputField value="{!dispAccount.ServicesProposed__c}" />
                                <apex:inputField value="{!dispAccount.ADSCQuoted__c}" />
                                <apex:inputField value="{!dispAccount.ANSCQuoted__c}" />
                                <apex:inputField value="{!dispAccount.DoNotCall__c}" />
                                <apex:inputField value="{!dispAccount.DoNotMail__c}" />
                            </apex:pageBlockSection>
                    
                        </apex:outputPanel>
                    
                    </div>
                
                </div>
            
                <apex:pageBlockButtons location="bottom" >
                    <apex:commandButton value="Save" action="{!saveDisp}" oncomplete="saveComplete();" rerender="dispFieldList,dispErrors,ssitemtable" />
                    <apex:commandButton value="Cancel" action="{!doCancel}" oncomplete="hideDispPopup();" rerender="dispErrors" />
                    <!--  <input type="button" class="btn" value="Cancel" onclick="hideDispPopup();" /> -->
                </apex:pageBlockButtons>
                
                <style type="text/css">
                
                    div.dispErrors {
                        overflow-y: auto;
                        max-height: 50px;
                        height: expression(this.scrollHeight > 50 ? "50px" : "auto");
                    }
                
                </style>
                
                <apex:outputPanel layout="block" id="dispErrors" styleclass="dispErrors">
                    <!-- <apex:pageMessage severity="error" summary="{!errormsg}" strength="1" rendered="{!NOT(ISNULL(errormsg))}"/> -->
                    <apex:pageMessages rendered="{!NOT(ISNULL(errormsg))}"/>
                    <script type="text/javascript">
                        function saveComplete() {
                            var err = "{!IF(ISNULL(errormsg),'null',errormsg)}";
                            var mcell = j$('[id$=dispErrors]').find('td.messageCell');
                            if (mcell.length == 0) {
                                if (!err || err.length == 0 || err == 'null') {
                                    hideDispPopup();
                                }
                            }
                        }
                    </script>
                </apex:outputPanel>
            
            </apex:pageBlock>
        </div>
        
        <!-- <apex:outputPanel layout="block" id="pageErrors">
            
            <apex:pageMessages />
        
        </apex:outputPanel> -->
        
            <script type="text/javascript">
            
                var dispPopup;
                var relatedList;
            
                j$(function() {
                    dispPopup = document.getElementById('dispPopup');
                    relatedList = document.getElementById('relatedList');
                    detailsection = document.getElemetById('detailsection');
                });
            
                function showDispPopup() {              
                    j$(dispPopup).show();
                    j$(relatedList).hide();
                    j$(relatedList).css('opacity', '1');
                    /*var isiPad = navigator.userAgent.match(/iPad/i) != null;
                    var scroller = null;
                    if (isiPad) {
                        document.addEventListener('touchmove', function(e){ e.preventDefault(); });
                        scroller = new iScroll("wrapper", {
                            scrollbarClass: "iscrollbar",
                            hScrollbar: false
                        });
                    }*/
                }
                
                function hideDispPopup() {
                    j$(dispPopup).hide();
                    j$(relatedList).show();
                    j$(relatedList).css('opacity', '1');
                }
            
            </script>
        
        <!-- Deprecated -->
        <!-- 
        <apex:actionFunction name="leadDisp" action="{!leadDisp}" oncomplete="showDispPopup();" rerender="dispBlockHeader,dispFieldList" immediate="true">
            <apex:param name="dispLeadId" value="" assignTo="{!dispLeadId}" />
        </apex:actionFunction>
         -->
         
        <!-- Deprecated -->
        <!-- 
        <apex:actionFunction name="accountDisp" action="{!accountDisp}" oncomplete="showDispPopup();" rerender="dispBlockHeader,dispFieldList" immediate="true">
            <apex:param name="dispAccountId" value="" assignTo="{!dispAccountId}" />
        </apex:actionFunction>
		 -->
		 
		<!-- Merged account/lead disposition into only one call -->
        <apex:actionFunction name="StreetSheetItemDisp" action="{!StreetSheetItemDisp}" oncomplete="showDispPopup();" rerender="dispBlockHeader,dispFieldList" immediate="true">
            <apex:param name="dispId" value="" assignTo="{!dispId}" />
        </apex:actionFunction>

        <apex:actionFunction action="{!deleteSSItem}" name="deleteSSItem" rerender="ssitemtable">
            <apex:param name="deleteItemId" value="" assignTo="{!deleteItemId}" />
        </apex:actionFunction>
        
        <div id="detailsection">
            <apex:detail relatedList="false" title="true"/> 
        </div>
        <div id="relatedList">
    		
    		<apex:outputPanel >
    			<div style="border:1px solid black; width: 10px;float: left;height: 10px;" class="touchedItem"></div>
    			<span style="float: left;margin-left:5px;">Prospects highlighted with this color were already touched</span>
    		</apex:outputPanel>
    		
    		<div style="clear:both;">&nbsp;</div>
    		
            <apex:pageBlock id="ssitemtable" >
            	
            	<!-- Detail list header -->
                <div class="tbody" id="itemTableContainer">
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tbody>
							<tr>
								<td class="pbTitle" style="padding:0px 0px 10px 0px;">
									<img width="1" height="1" title="" class="minWidth" alt="" src="/s.gif" />									
									<img src="/s.gif" alt="" class="relatedListIcon" title="" />
									<h3 style="margin: 5px 0 0 32px;display: block;margin-left: -2px;">Prospect List</h3>									
								</td>								
								<td class="pbHelp" style="text-align:right;">
								   <span>Sort By</span>
							       <apex:selectList value="{!sortColumn}" multiselect="false" size="1">
							            <apex:selectOptions value="{!sortOptions}"/>
							            <apex:actionSupport event="onchange" action="{!sortProspectItemList}" reRender="ProspectListPanel"/>
							        </apex:selectList><p/>
								</td>
							</tr>
						</tbody>
					</table>                
                	
                	<!-- Detail list items -->
                	<apex:outputPanel id="ProspectListPanel">
                    <apex:PageBlockTable value="{!StreetSheetResultList}" var="ssi" styleClass="p-items">
	                    <apex:column styleClass="{!IF( AND(ssi.ssiObj.Touched__c!=null, ssi.ssiObj.Touched__c>0), 'touchedItem', '')}">
	                        <apex:facet name="header">Action</apex:facet>
	                        <apex:outputPanel styleClass="action">
	                        	<table cellpadding="0" cellspacing="0" width="107">
	                        		<tr>
	                        			<td>
			                                 <a onclick="redirectParent('/{!ssi.Id}/e?retURL=/{!ssid}');">Edit</a>
			                                 &nbsp;|&nbsp;
			                                 <a href="" onclick="deleteSSItem('{!ssi.Id}');">Del</a>
			                                 &nbsp;|&nbsp;
			                                 <a href="" onclick="StreetSheetItemDisp('{!ssi.Id}');">Disp</a>
	                        			</td>
	                        		</tr>
                                </table> 
	                        </apex:outputPanel>
	                    </apex:column>
	                    <apex:column styleClass="{!IF(sortColumn == 'name', 'selCol', '')}">
	                    	<apex:facet name="header">Name</apex:facet>
	                    	<apex:outputPanel >
	                    		<a href="/{!ssi.id}" target="_blank">{!ssi.name}</a>
	                    	</apex:outputPanel>
	                    </apex:column>
	                    <apex:column styleClass="{!IF(sortColumn == 'site', 'selCol', '')} {!IF( AND(ssi.ssiObj.Touched__c!=null, ssi.ssiObj.Touched__c>0), 'touchedItem', '')}">
	                    	<apex:facet name="header">Site Street</apex:facet>
	                    	<apex:outputText value="{!ssi.SiteStreet}" escape="false"/>
	                    </apex:column>
	                    <apex:column styleClass="{!IF(sortColumn == 'city', 'selCol', '')} {!IF( AND(ssi.ssiObj.Touched__c!=null, ssi.ssiObj.Touched__c>0), 'touchedItem', '')}">
	                    	<apex:facet name="header">Site City</apex:facet>
	                    	<apex:outputText value="{!ssi.SiteCity}" escape="false"/>
	                    </apex:column>
	                    <apex:column styleClass="{!IF(sortColumn == 'postalcode', 'selCol', '')} {!IF( AND(ssi.ssiObj.Touched__c!=null, ssi.ssiObj.Touched__c>0), 'touchedItem', '')}">
	                    	<apex:facet name="header">Site Postal Code</apex:facet>	                    	
	                    	<apex:outputText value="{!IF(ISBLANK(ssi.SitePostalCodeAddOn), ssi.SitePostalCode,IF(LEN(ssi.SitePostalCode) == 3, ssi.SitePostalCode + ' ' + ssi.SitePostalCodeAddOn, ssi.SitePostalCode  + '-' + ssi.SitePostalCodeAddOn))}" />
	                    </apex:column>
	                    <apex:column styleClass="{!IF( AND(ssi.ssiObj.Touched__c!=null, ssi.ssiObj.Touched__c>0), 'touchedItem', '')}">
	                    	<apex:facet name="header">Phone</apex:facet>
	                    	<apex:outputText value="{!ssi.Phone}" escape="false"/>
	                    </apex:column>
	                    <apex:column styleClass="{!IF( AND(ssi.ssiObj.Touched__c!=null, ssi.ssiObj.Touched__c>0), 'touchedItem', '')}">
	                    	<apex:facet name="header">Data Source</apex:facet>
	                    	<apex:outputText value="{!ssi.DataSource}" escape="false"/>
	                    </apex:column>
	                    <apex:column styleClass="{!IF( AND(ssi.ssiObj.Touched__c!=null, ssi.ssiObj.Touched__c>0), 'touchedItem', '')}">
	                    	<apex:facet name="header">Disco Reason</apex:facet>
	                    	<apex:outputText value="{!ssi.DisconnectReason}" escape="false"/>
	                    </apex:column>
	                    <apex:column styleClass="{!IF( AND(ssi.ssiObj.Touched__c!=null, ssi.ssiObj.Touched__c>0), 'touchedItem', '')}">
	                    	<apex:facet name="header">Disposition</apex:facet>
	                    	<apex:outputText value="{!HTMLENCODE(ssi.DispositionCode)}" escape="false"/>
	                    </apex:column>
	                    <apex:column styleClass="{!IF( AND(ssi.ssiObj.Touched__c!=null, ssi.ssiObj.Touched__c>0), 'touchedItem', '')}">
	                    	<apex:facet name="header">Type</apex:facet>
	                    	<apex:outputText value="{!ssi.Type}" escape="false"/>
	                    </apex:column>
	                    <apex:column styleClass="{!IF( AND(ssi.ssiObj.Touched__c!=null, ssi.ssiObj.Touched__c>0), 'touchedItem', '')}">
	                    	<apex:facet name="header">Score</apex:facet>
	                    	<apex:outputText value="{!ssi.ssiObj.Score__c}" escape="false"/>
	                    </apex:column>
                    </apex:PageBlockTable>
                    </apex:outputPanel>
                    
                    <!-- Deprecated previous list  
                    <table class="items body" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr class="header">
                                <th class="reason">Reason for System</th>
                                <th class="dispositiondetail">Disposition Detail</th>
                                <th class="action"></th>
                                <th class="name"></th>
                                <th class="sitestreet"></th>
                                <th class="sitecity"></th>
                                <th class="sitepostalcode"></th>
                                <th class="phone"></th>
                                <th class="datasource"></th>
                                <th class="discoreason"></th>
                                <th class="disposition"></th>
                                <th class="dispositiondetail"></th>
                                <th class="reason"></th>
                                <th class="type"></th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                    -->
                     
                </div>
            
            </apex:pageBlock>
        
        </div>
    
    </apex:form>

</apex:page>