<!--************************************* MODIFICATION LOG ********************************************************************************************
*
* DESCRIPTION : NewLead.page This blank page is to allow the assigning by controller of default values for a new Lead record
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani			04/01/2011			- Origininal Version
* Mike Tuckman					03/22/2014			- (Hermes) Added extensive logis to enter and valdiate address fields								
* Leonard Alarcon				04/02/2014			- Added required field for Channel picklist.
* Magdiel Herrera				11/5/2015			- Support for auto-convert initially inteded for BestBuy sales							
-->
<apex:page standardController="Lead" showHeader="true" sidebar="true" extensions="NewLeadController" action="{!init}">
		
		<apex:form id="form">
		
        <apex:pageBlock mode="edit">		
            <apex:pageBlockSection columns="1" collapsible="false" title="{!title}">
            	<apex:inputField value="{!ld.FirstName}"/>
            	<apex:inputField value="{!ld.LastName}"/>
            	<apex:inputField value="{!ld.Company}"/>
            	<apex:inputField value="{!ld.SiteStreet__c}"/>
            	<apex:inputField value="{!ld.SiteStreet2__c}"/> 
            	 <!-- Added by TCS for 6915-->
                <apex:inputField value="{!ld.SiteStreet3__c}"/> 
            	<apex:inputField value="{!ld.SiteCity__c}"/>
            	<apex:inputField value="{!ld.SiteStateProvince__c}"/>
            	<apex:inputField value="{!ld.SitePostalCode__c}"/>
            	<apex:inputField required="true" value="{!ld.Channel__c}"/>
            	<apex:outputField value="{!ld.SiteCounty__c}"/>
            	<apex:outputField value="{!ld.IncomingLatitude__c}"/>
            	<apex:outputField value="{!ld.IncomingLongitude__c}"/>
            </apex:pageBlockSection>
        	<apex:PageBlockButtons location="bottom">
        		<apex:actionStatus id="ldSaveStatus">
        			<apex:facet name="start">
        				<apex:outputPanel >
				        	<apex:commandButton value="loading..." disabled="true"/>
				        	<apex:commandButton value="loading..." rendered="{!allowOverride}"  disabled="true"/>
        				</apex:outputPanel>
        			</apex:facet>
        			<apex:facet name="stop">
        				<apex:outputPanel >
				        	<apex:commandButton action="{!doSave}" value="{!saveButtonTxt}" rerender="form" oncomplete="ConvertLead({!runConversion})" status="ldSaveStatus" styleClass="doSaveBtn"/>
				        	<apex:commandButton action="{!doOverride}" value="Override Error" rerender="form" rendered="{!allowOverride}" oncomplete="ConvertLead({!runConversion})" status="ldSaveStatus" styleClass="doOverrideBtn"/>
        				</apex:outputPanel>
        			</apex:facet>
        		</apex:actionStatus>
        	</apex:PageBlockButtons>
        	
        	<apex:actionFunction name="ConvertLeadJS" action="{!ConvertLead}" rerender="form" status="ldSaveStatus"/>
     	
	    <apex:pageMessages id="msgs"/>        
        
        </apex:pageBlock>

		</apex:form>

     	<script>
     		function ConvertLead( runConvert ){
     			if ( runConvert === true ){
     				ConvertLeadJS();
     			}
     		}
     	</script>

</apex:page>