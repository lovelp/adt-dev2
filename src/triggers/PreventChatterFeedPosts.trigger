/************************************* MODIFICATION LOG ********************************************************************************
 *
 * DESCRIPTION : Chatter Post trigger that executes before insert to prevent non Admins from posting to certain Chatter groups
 *
 *---------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                  REASON
 *---------------------------------------------------------------------------------------------------------------------------------------
 * Erin McGee               07/11/2013         - Original Version
 *
 *
 */
trigger PreventChatterFeedPosts on FeedItem(before insert, after insert)
{
    Map < String, String > chatterObject = new Map < String, String > ();
    List < Schema.SObjectType > gd = Schema.getGlobalDescribe().Values();
    Set<String> RelatedRecordIdSet = new Set<String>();

   if (trigger.isBefore)
    {
        for (FeedItem FI: trigger.new)
        {
            User u = [select ChatterExcluded__c from User where id = :userinfo.getUserId()];
            if (u.ChatterExcluded__c != null && u.ChatterExcluded__c == true) {
            	FI.addError('You are unable to post at this time.'); 
            	break;
            }
        }
    }

    for (Schema.SObjectType f: gd)
    {
        if (f.getDescribe().isFeedEnabled())
        {
            String objectPrefix = f.getDescribe().getKeyPrefix();
            String objectName = f.getDescribe().getName();
            chatterObject.put(objectPrefix, objectName);
        }
    }
    Set < String > chatterGroups = new Set < String > ();
    List < Chatter_Group_Restriction__c > cgList = [Select Name From Chatter_Group_Restriction__c];
    if (!cgList.isEmpty())
    {
        for (Chatter_Group_Restriction__c cg: cgList)
        {
            chatterGroups.add(cg.Name);
        }

        List < CollaborationGroupMember > cgmList = [SELECT collaborationGroupId, memberId, collaborationGroup.Name FROM CollaborationGroupMember WHERE CollaborationRole = 'Admin'
            AND collaborationGroup.Name IN: chatterGroups];
        if (trigger.isBefore)
        {
            for (FeedItem FI: trigger.new)
            {
                String thisFIpID = FI.ParentId;
                String thisFIprefix = thisFIpID.substring(0, 3);

                if (thisFIprefix == CollaborationGroup.sObjectType.getDescribe().getKeyPrefix())
                {
                    Boolean isAuthorizedGroupMember = false;
                    for (CollaborationGroupMember cgm: cgmList)
                    {
                        if ((cgm.collaborationGroupId == FI.ParentId) && (cgm.memberId == userinfo.getUserId()))
                        {
                            isAuthorizedGroupMember = true;
                            break;
                        }
                    }
                    if (!isAuthorizedGroupMember)
                    { // Not Authorized CollaborationGroup Member
                        FI.addError('Posting to this group is limited to group owners and managers.');
                    }
                }
                else
                {
                    if (chatterObject.containskey(thisFIprefix))
                    {}
                    else
                    {
                        FI.addError('Posting to this group is limited to group owners and managers.');
                    }
                }
            }
        }
        else if (trigger.isAfter)
        {
            Set < String > mentionedUsers = new Set < String > ();
            Map < String, FeedItem > feedItems = new Map < String, FeedItem > ();            
			Map < String, FeedItem > feedItemObjMap = new Map < String, FeedItem >(); 
			
            for (FeedItem FI: trigger.new)
            {
                String feedItemId = FI.Id; //Get the feed item id
                String communityId = null;
                
                // Save posts containing file sharing for further evaluation
                system.debug('\n ### feedItemObjMap='+feedItemObjMap+' \n');
				if(FI.RelatedRecordId != null){
					feedItemObjMap.put(FI.Id, FI);
					RelatedRecordIdSet.add(FI.RelatedRecordId);
				}
					
                ConnectApi.FeedItem feedItem = ConnectApi.ChatterFeeds.getFeedItem(communityId, feedItemId); //Get the feed item from ConnectApi

                List < ConnectApi.MessageSegment > messageSegments = feedItem.body.messageSegments; //Get the feed item message segments

                //For each segment in the feed item...
                for (ConnectApi.MessageSegment messageSegment: messageSegments)
                {
                    //If the segment is a mention...
                    if (messageSegment.text.Contains('@'))
                    {
                        //Add the mentioned user to the mentionedUsersMap(userid, username)
                        mentionedUsers.add(messageSegment.text.remove('@'));
                        feedItems.put(messageSegment.text.remove('@'), FI);

                    }
                }
                
                
            }

            Boolean isAuthorizedGroupMemberMention = true;
            FeedItem FI = new FeedItem();
            system.debug(mentionedUsers);
            for (CollaborationGroupMember cgm: cgmList)
            {
                if (mentionedUsers.contains(cgm.collaborationGroup.Name))
                {
                    if (cgm.memberId == userinfo.getUserId())
                    {
                        isAuthorizedGroupMemberMention = true;
                        FI = feedItems.get(cgm.collaborationGroup.Name);
                        break;
                    }
                    else
                    {
                        isAuthorizedGroupMemberMention = false;
                        FI = feedItems.get(cgm.collaborationGroup.Name);
                    }
                }
            }
            if (!isAuthorizedGroupMemberMention)
            { // Not Authorized CollaborationGroup Member
                FI.addError('Mentioning this group is limited to group owners and managers.');
                FeedItem fItem = [Select Id From FeedItem Where Id = : FI.Id];
                delete fItem;
            }
            
			system.debug('\n\n feedItemObjMap='+feedItemObjMap+'\n ChatterUtilities.chatterFilesEvaluated='+ChatterUtilities.chatterFilesEvaluated+'\n ChatterUtilities.isChatterEnhancedSecurity()='+ChatterUtilities.isChatterEnhancedSecurity()+' \n');
            // Evaluate posts containing files
            if(feedItemObjMap!=null && !feedItemObjMap.isEmpty() && ChatterUtilities.chatterFilesEvaluated == false && ChatterUtilities.isChatterEnhancedSecurity()==true)
            	ChatterUtilities.EvalFileContent(feedItemObjMap, RelatedRecordIdSet);
        }
    }
}