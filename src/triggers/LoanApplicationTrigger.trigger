trigger LoanApplicationTrigger on LoanApplication__c (Before Insert,Before Update, After Update) {
    /* Before Insert */
    if(Trigger.isInsert && Trigger.isBefore){
        LoanApplicationTriggerHelper.ProcessRecordsBeforeInsert(trigger.new);
    }
    
    /* Before Update */
    if(Trigger.isUpdate && Trigger.isBefore){
        if(System.isFuture() == false){
            LoanApplicationTriggerHelper.ProcessRecordsbeforeUpdate(trigger.new,trigger.oldMap);
        }
    }
    /* AfterUpdate */
    if(Trigger.isUpdate && Trigger.isAfter){  
        LoanApplicationHistoryTriggerHelper.InsertHistory(trigger.newMap,trigger.oldMap);           
    }
}