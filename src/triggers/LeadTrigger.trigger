/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : Lead trigger that executes before or after the insert, update, delete, merge, upsert and undelete operation
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Shivkant Vadlamani            02/23/2012          - Original Version
*
*                                                    
*/

trigger LeadTrigger on Lead (before insert, after insert, before update, after update) 
{
    // Process all before insert activities
    if(trigger.isBefore && trigger.isInsert)
    {
        LeadTriggerHelper.ProcessLeadsBeforeInsert(trigger.new);
    }
    
    if(trigger.isAfter && trigger.isInsert)
	{
		LeadTriggerHelper.ProcessLeadsAfterInsert(trigger.new);
	}
    
    // Process all before update activities
    if(trigger.isBefore && trigger.isUpdate)
    {
        LeadTriggerHelper.ProcessLeadsBeforeUpdate(trigger.newMap, trigger.oldMap);                 
    }
    
    // Process all after update activities
    if(trigger.isAfter && trigger.isUpdate)
    {
        LeadTriggerHelper.ProcessLeadsAfterUpdate(trigger.newMap, trigger.oldMap);
    }
    
}