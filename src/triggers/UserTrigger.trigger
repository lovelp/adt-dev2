/************************************* MODIFICATION LOG ********************************************************************************************
* *
* DESCRIPTION : Called from User trigger
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Jaydip Bhattacharya         	5/23/2012          	- Original Version
* Magdiel Herrera				10/14/2013			- Update to support Manager
*                                                   
*/
trigger UserTrigger on User (after insert, after update, before insert, 
before update) {

	/*
    Permission_Set_Usage__c ps=Permission_Set_Usage__c.getInstance('HH Assignment');
    
    if (ps.Auto_assignment__c==true){
        if (Trigger.isAfter)
        {
            UserTriggerHelper.ProcessUserAfter(trigger.new);
        }
    }
    */
    
    
    // onBefore update
    if(Trigger.isBefore && Trigger.isUpdate){
    	UserTriggerHelper.ProcessUserBefore(trigger.newMap,trigger.oldMap);
    }

    // onAfter update
    if(Trigger.isAfter && Trigger.isUpdate){
    	UserTriggerHelper.ProcessUserAfterUpdate(trigger.new,trigger.oldMap);
    }
    	
    // onBefore insert
    if(Trigger.isBefore && Trigger.isInsert){
    	UserTriggerHelper.ProcessUserBeforeInsert(trigger.new);
    }
    
    // onAfter insert
    if(Trigger.isAfter && Trigger.isInsert){
    	UserTriggerHelper.ProcessUserAfterInsert(trigger.newMap);
    }

}