/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : Promoter trigger that executes before or after the insert, update, delete, merge, upsert and undelete operation
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli               04/26/2012          - Origininal Version
*
*                                                
*/

trigger PromoterTrigger on Promoter__c (before insert, before update) {
	if(trigger.isBefore && trigger.isInsert)
	{
		PromoterHelper.setOwnerIdFromEmployeeId(trigger.New);
	}
	else if (trigger.isBefore && trigger.isUpdate)
	{
		PromoterHelper.setOwnerIdFromEmployeeId(trigger.new, trigger.oldMap);
	}
}