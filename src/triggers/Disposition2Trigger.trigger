/************************************* MODIFICATION LOG ********************************************************************************************
* DispositionTrigger
*
* DESCRIPTION : Trigger on custom Disposition object that executes before insert operations
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER            DATE          REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*                10/9/2011      - Original Version
*
*                          
*/
trigger Disposition2Trigger on Disposition2__c (before insert) {

  DispositionTriggerHelper dtHelper = new DispositionTriggerHelper();
  if(Trigger.isBefore && Trigger.isInsert)
  {
    //dtHelper.processBeforeInsert(Trigger.New);
  }
}