trigger CampaignMemberTrigger on CampaignMember (after delete,after insert){
    if(trigger.isDelete){
        CampaignMemberTriggerHelper.processCampaignMembersAfterDelete(trigger.old);
    }
}