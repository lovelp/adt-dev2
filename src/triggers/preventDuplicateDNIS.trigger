trigger preventDuplicateDNIS on DNIS__c (before insert,before update) {
    
    //list of new DNIS names
    List<String> newDNISList=new List<String>();
    for(DNIS__c dnis:trigger.new){
        newDNISList.add(dnis.name);   
    }
    
    //Map to hold the list of existing DNIS 
    Map<String,List<DNIS__c>> DNISMap= new Map<String,List<DNIS__c>>();
    Date today=date.today();
    for(DNIS__c dnis:[SELECT id,
                            name,
                            LineofBusiness__c,
                            Start_Date__c,
                            End_Date__c 
                     FROM DNIS__c 
                     WHERE name IN:newDNISList and
                           start_date__c<=:today and
                           end_date__c>=:today and
                           id NOT IN:trigger.new]){
        
        if(DNISMap.get(dnis.name)==null)
            DNISMap.put(dnis.name,new List<DNIS__c>{dnis});    
        else{
            List<DNIS__c> existingDnisList=DNISMap.get(dnis.name);
            existingDnisList.add(dnis);
            DNISMap.put(dnis.name,existingDnisList);
        }
        
    }
    
    for(DNIS__c dnis:trigger.new){
        if(DNISMap.get(dnis.name)!=null){
            for(DNIS__c existingDnis:DNISMap.get(dnis.name)){
                if(existingDnis.lineofBusiness__c==dnis.lineofBusiness__c){
                    dnis.addError('Duplicate active DNIS with same channel found');
                    break;
                }           
            }
        }
    }
    
    
    
}