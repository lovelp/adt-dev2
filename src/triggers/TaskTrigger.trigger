trigger TaskTrigger on Task (after update) {
    if(trigger.isUpdate && trigger.isAfter){
        TaskTriggerHelper.processTasksAfterUpdate(trigger.new,trigger.old,trigger.newMap,trigger.oldMap);        
    }
}