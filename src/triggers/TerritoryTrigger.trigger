/************************************* MODIFICATION LOG ********************************************************************************
* TerritoryTrigger
*
DESCRIPTION : Trigger on custom Territory object that executes before insert, before and after update and before delete
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
*                            10/9/2011           - Original Version
*
*                                                
*/

trigger TerritoryTrigger on Territory__c (before delete, after update, before insert, before update) {

	TerritoryTriggerHelper helper = new TerritoryTriggerHelper();
	//delete zip codes associoated to this territory from territory assignments object
	if (Trigger.isBefore && Trigger.isDelete) {
		helper.DeleteTerritoryAssociation(Trigger.oldMap);
	}

	if (Trigger.isAfter && Trigger.isUpdate)
	{
		helper.UpdateTerritoryOwner(Trigger.newMap , Trigger.oldMap);
	}
	
	if (Trigger.isBefore && Trigger.isInsert)
	{
		//helper.setTerritoryOwner(Trigger.New);
		helper.updateHiddenOwner(Trigger.New);
	}
	
	if(Trigger.isBefore && Trigger.isUpdate)
	{
		helper.updateHiddenOwner(Trigger.New);
	}
}