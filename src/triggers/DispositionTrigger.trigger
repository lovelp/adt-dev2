/************************************* MODIFICATION LOG ********************************************************************************************
* DispositionTrigger
*
* DESCRIPTION : Trigger on custom Disposition object that executes before insert operations
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
*                            10/9/2011          - Original Version
*
*                                                   
*/
trigger DispositionTrigger on Disposition__c (before insert) {
    
    DispositionTriggerHelper dtHelper = new DispositionTriggerHelper();
    if(Trigger.isBefore && Trigger.isInsert)
    {    
        
        dtHelper.processBeforeInsert(Trigger.New);
    }
    
}