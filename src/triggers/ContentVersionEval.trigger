trigger ContentVersionEval on ContentVersion (after insert, after update, before insert, before update) {
	
	Set<String> cDocIDs = new Set<String>(); 
	
	system.debug('\n @@@ trigger.isAfter='+trigger.isAfter+' ChatterUtilities.chatterFilesEvaluated='+ChatterUtilities.chatterFilesEvaluated+' ChatterUtilities.isChatterEnhancedSecurity()='+ChatterUtilities.isChatterEnhancedSecurity()+' \n');
	if(trigger.isAfter && trigger.isInsert && ChatterUtilities.chatterFilesEvaluated == false && ChatterUtilities.isChatterEnhancedSecurity()==true){
		system.debug('@@@ evaluating file upload...');
		for(ContentVersion cVer: trigger.new){
			if(!ChatterUtilities.isFileUploadValid(cVer.FileType, UserInfo.getUserId())){
				cDocIDs.ADd(cVer.ContentDocumentId);
				
		        try {
		            Messaging.reserveSingleEmailCapacity(1);
		            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		            mail.setTargetObjectId(UserInfo.getUserId());
		            mail.saveAsActivity = false;		           
		            mail.setSenderDisplayName('Chatter Approval');
		            mail.setSubject('Chatter File Upload Denied !');
		            mail.setHtmlBody('An action to upload a *.'+cVer.FileType+' document to Chatter files, you recently submitted, was denied and your file deleted.');
		            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
		        }
		        catch (Exception ex) { }

				
			}
		}
	}
	
	if(cDocIDs!=null && !cDocIDs.isEmpty()){
		ChatterUtilities.DeleteContentDocuments(cDocIDs);
	}
	
}