/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : Terms and Conditions trigger that executes before the insert and update
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Erin McGee               07/08/2013         - Original Version
*
*                                                
*/
trigger termsandConditionsTrigger on Terms_and_Condition__c (before insert, before update) 
{

	if(trigger.new[0].Clear_Users_T_C_Acceptance__c)
	{
		TermsAndConditionsReset tcr= new TermsAndConditionsReset();
		Database.executeBatch(tcr);
		trigger.new[0].Clear_Users_T_C_Acceptance__c=false;
	}

}