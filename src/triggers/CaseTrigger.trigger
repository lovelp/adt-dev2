trigger CaseTrigger on Case (after insert, before update, before insert) {
	
	// Before update trigger logic
	if(Trigger.isBefore && Trigger.isUpdate){
		CaseTriggerHelper.ProcessCaseBeforeUpdate(Trigger.newMap, Trigger.oldMap);
	}	

	// After update trigger logic
	if(Trigger.isAfter && Trigger.isUpdate){
		CaseTriggerHelper.ProcessCaseAfterUpdate(Trigger.newMap, Trigger.oldMap);
	}
		
	// Before insert trigger logic
	if(Trigger.isBefore && Trigger.isInsert){
		CaseTriggerHelper.ProcessCaseBeforeInsert(Trigger.new);
	}	
	
	// After insert trigger logic
	if(Trigger.isAfter && Trigger.isInsert){
		CaseTriggerHelper.ProcessCaseAfterInsert(Trigger.new);
	}	

}