trigger NSCProfileDiscoveryQuestion on ProfileQuestion__c (before update) {
	
	if( trigger.isBefore && trigger.isUpdate ){
		NSCHelper.processProfileQuestionBeforeUpdate(trigger.NewMap, trigger.OldMap);
	}
	
}