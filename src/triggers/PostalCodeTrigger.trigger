/************************************* MODIFICATION LOG ********************************************************************************
* PostalCodeTrigger
*
* DESCRIPTION : Trigger on custom Postal Codes object that executes before or after the insert operation
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
*                            10/9/2011           - Original Version
*
*                                                
*/

trigger PostalCodeTrigger on Postal_Codes__c (before insert, after insert) {
    if(Trigger.isBefore && Trigger.isInsert)
    {
        RecordType rt = Utilities.getRecordType('US Postal Code', 'Postal_Codes__c');
        for(Postal_Codes__c pc : Trigger.New)
        {
            if(pc.TownId__c != null && pc.TownId__c !='')
            {
                pc.RecordTypeId = rt.Id;
            }
        }
    }
    
    // Populate the 99999 postal code
    if(Trigger.isAfter && Trigger.isInsert)
    {
        Postal_Codes__c postalCode;
        try{
            postalCode = [select id, All_Areas__c from Postal_Codes__c where Name = '99999' Limit 1];
        } catch (Exception ex){
            postalCode = new Postal_Codes__c();
            postalCode.Name = '99999';
            insert postalCode;          
        }
        // If the 99999 postal code does not exist, insert it
        /*if( postalCode == null )
        {
            postalCode = new Postal_Codes__c();
            postalCode.Name = '99999';
            
            insert postalCode;
        }*/
        String allAreas = '';
        if(postalCode.All_Areas__c != null)
           allAreas = postalCode.All_Areas__c;
        
        
        List<String> allAreasM = new List<String>();
        Map<String, String> allAreasS = new Map<String, String>();
        allAreasM = allAreas.split('~');
        for(String aa : allAreasM)
        {
            allAreasS.put(aa, aa);
        }
        
        if(allAreas == null)
            allAreas = '';
        
        for(Postal_Codes__c pc : Trigger.New)
        {
            if(pc.Area__c != null && pc.Area__c != '' && pc.AreaID__c != null && pc.AreaID__c != '' && pc.BusinessId__c != null && pc.BusinessId__c != ''  && allAreasS.get(pc.AreaID__c + ' - ' + pc.Area__c + ' (' + pc.BusinessID__c + ')') == null)
            {
                allAreas += '~' + pc.AreaID__c + ' - ' + pc.Area__c + ' (' + pc.BusinessID__c + ')';
                allAreasS.put(pc.AreaID__c + ' - ' + pc.Area__c + ' (' + pc.BusinessID__c + ')', pc.AreaID__c + ' - ' + pc.Area__c + ' (' + pc.BusinessID__c + ')');
            }
        }
        postalCode.All_Areas__c = allAreas;
        System.debug('PostalCodeTrigger------------allAreas: ' + allAreas);
        update postalCode;
    }
    
}