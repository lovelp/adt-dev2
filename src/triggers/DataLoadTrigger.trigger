trigger DataLoadTrigger on DataloadTemp__c (before insert) {
    
    
    if(trigger.isBefore && trigger.isInsert){
        DataLoadTriggerHelper.initialprocessing(trigger.new);
    }
}