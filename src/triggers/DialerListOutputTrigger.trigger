trigger DialerListOutputTrigger on Dialer_List_Output_Control__c (before insert) {
    DialerListOutputTriggerHelper.processbeforeInsert(trigger.new);
}