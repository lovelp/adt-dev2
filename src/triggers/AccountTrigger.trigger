/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : Account trigger that executes before or after the insert, update, delete, merge, upsert and undelete operation
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Sunil Addepalli               11/11/2011          - Origininal Version
* Viraj Shah					09/10/2019			- Added ResaleGlobalVariables__c.getinstance('DataRecastDisableAccountTrigger') != null to avoid null exception
*                                                
*/


trigger AccountTrigger on Account (after update, after insert, before insert, before update) 
{
    if(ResaleGlobalVariables__c.getinstance('DataRecastDisableAccountTrigger') != null && ResaleGlobalVariables__c.getinstance('DataRecastDisableAccountTrigger').value__c == 'FALSE')
    {
        if(trigger.isBefore && trigger.isInsert)
        {   
            AccountTriggerHelper.ProcessAccountsBeforeInsert(trigger.new);
        }
        else if(trigger.isAfter && trigger.isInsert)
        {   
            AccountTriggerHelper.ProcessAccountsAfterInsert(trigger.new);
        }
        else if (trigger.isBefore && trigger.isUpdate)
        {
            AccountTriggerHelper.ProcessAccountsBeforeUpdate(trigger.newMap, trigger.oldMap);
        }   
        else if (trigger.isAfter && trigger.isUpdate)
        {
            AccountTriggerHelper.ProcessAccountsAfterUpdate(trigger.newMap, trigger.oldMap);
        }
    }
}