trigger ValidateChatterGroupName on Chatter_Group_Restriction__c(before insert, before update)
{
    List < CollaborationGroup > chatterGroups = [Select Name FROM CollaborationGroup];
    List < Chatter_Group_Restriction__c > chatterRestrictedGroups = [Select Name FROM Chatter_Group_Restriction__c];

    Set < String > existingChatterRestrictedGroups = new Set < String > ();
    for (Chatter_Group_Restriction__c cg: chatterRestrictedGroups)
    {
        existingChatterRestrictedGroups.add(cg.Name);
    }
    Set < String > existingChatterGroups = new Set < String > ();
    for (CollaborationGroup chattergroup: chatterGroups)
    {
        existingChatterGroups.add(chattergroup.Name);
    }
    for (Chatter_Group_Restriction__c cg: trigger.New)
    {
        if (!existingChatterGroups.contains(cg.Name))
        {
            cg.addError('The Chatter Group Name you entered does not exist. Please check the name of the group you are trying to enter and try again.');
        }
        if (trigger.isInsert)
        {
            if (existingChatterRestrictedGroups.contains(cg.Name))
            {
                cg.addError('This Chatter Group already has a Chatter Group Restriction record. Please find this record and change its Restriction Type, if necessary.');
            }
        }
    }

}