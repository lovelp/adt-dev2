/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : Quote Job trigger that executes after the insert operation
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Jitendra Kothari             03/05/2019          - Origininal Version (HRM-9453 - eContract Signed notification to Submitted by Rep and his Manager)
*                                                
*/
trigger QuoteJobTrigger on Quote_Job__c (after insert) {
    //----------------------------------------------
    //  AFTER
    //----------------------------------------------
    if(trigger.isAfter){
        // INSERT
        if(trigger.isInsert){
            QuoteJobTriggerHandler.ProcessQuoteJobAfterInsert(trigger.new);
        }
    }
}