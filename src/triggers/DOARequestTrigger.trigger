/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : DOA Request trigger that executes before or after the insert, update, delete, merge, upsert and undelete operation
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Mike Tuckman                  08/07/2015          - Origininal Version
*                                                
*/

trigger DOARequestTrigger on DOA_Request__c (after insert, after update, before update) {

        //----------------------------------------------
        //  BEFORE
        //----------------------------------------------
        if(trigger.isBefore){
            
            // INSERT
            
            // UPDATE
            if(trigger.isUpdate){
                DOARequestTriggerHelper.ProcessDOARequestBeforeUpdate(trigger.newMap, trigger.oldMap);
            }
            
        }
        
       //----------------------------------------------
       //  AFTER
      //----------------------------------------------
        if(trigger.isAfter){

           // INSERT 
            if(trigger.isInsert){
                DOARequestTriggerHelper.ProcessDOARequestAfterInsert(trigger.new);
            }
            
          
          // UPDATE
            if(trigger.isUpdate){
                DOARequestTriggerHelper.ProcessDOARequestAfterUpdate(trigger.newMap, trigger.oldMap);
            }
        }
      

}