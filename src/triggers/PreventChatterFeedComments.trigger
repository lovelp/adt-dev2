/************************************* MODIFICATION LOG ********************************************************************************
 *
 * DESCRIPTION : Chatter Comment trigger that executes before insert to prevent non Admins from commenting to certain Chatter groups
 *
 *---------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                     DATE                  REASON
 *---------------------------------------------------------------------------------------------------------------------------------------
 * Erin McGee               07/11/2013         - Original Version
 *
 *
 */
trigger PreventChatterFeedComments on FeedComment(before insert, after insert)
{
    Map < String, String > chatterObject = new Map < String, String > ();
    List < Schema.SObjectType > gd = Schema.getGlobalDescribe().Values();
    Set<String> RelatedRecordIdSet = new Set<String>();

    if (trigger.isBefore) {
            for (FeedComment FC: trigger.new)
            {
                User u = [select ChatterExcluded__c from User where id = :userinfo.getUserId()];
                if (u.ChatterExcluded__c != null && u.ChatterExcluded__c == true) {
                	FC.addError('You are unable to comment at this time.');
                	break;
                }
            }
    }
     
    for (Schema.SObjectType f: gd)
    {
        if (f.getDescribe().isFeedEnabled())
        {
            //  system.debug('Object Name='+f.getDescribe().getName()+' Prefix='+f.getDescribe().getKeyPrefix());
            String objectPrefix = f.getDescribe().getKeyPrefix();
            String objectName = f.getDescribe().getName();
            chatterObject.put(objectPrefix, objectName);
        }
    }
    Set < String > chatterGroups = new Set < String > ();
    List < Chatter_Group_Restriction__c > cgList = [Select Name From Chatter_Group_Restriction__c WHERE Restriction_Type__c = 'Cannot Post or Comment'];
    if (!cgList.isEmpty())
    {
        for (Chatter_Group_Restriction__c cg: cgList)
        {
            chatterGroups.add(cg.Name);
        }
        List < CollaborationGroupMember > cgmList = [SELECT collaborationGroupId, memberId, collaborationGroup.Name FROM CollaborationGroupMember WHERE CollaborationRole = 'Admin'
            AND collaborationGroup.Name IN: chatterGroups
        ];
        if (trigger.isBefore)
        {
            for (FeedComment FC: trigger.new)
            {
                User u = [select ChatterExcluded__c from User where id = :userinfo.getUserId()];
                if (u.ChatterExcluded__c != null && u.ChatterExcluded__c == true) {
                	FC.addError('You are unable to comment at this time.');
                	break;
                }
                
                String thisFCpID = FC.ParentId;
                String thisFCprefix = thisFCpID.substring(0, 3);

                if (thisFCprefix == CollaborationGroup.sObjectType.getDescribe().getKeyPrefix())
                {
                    // system.debug('Inside Collaboration Group');
                    Boolean isAuthorizedGroupMember = false;
                    for (CollaborationGroupMember cgm: cgmList)
                    {
                        if ((cgm.collaborationGroupId == FC.ParentId))
                        {
                            if (cgm.memberId == userinfo.getUserId())
                            {
                                system.debug('Is Authorized');
                                isAuthorizedGroupMember = true;
                                break;
                            }
                        }
                        else
                        {
                            system.debug('Is Authorized');
                            isAuthorizedGroupMember = true;
                            break;
                        }
                    }
                    if (!isAuthorizedGroupMember)
                    { // Not Authorized CollaborationGroup Member
                        FC.addError('Commenting to this group is limited to group owners and managers.');
                    }
                }
                else
                {
                    if (chatterObject.containskey(thisFCprefix))
                    {}
                    else
                    {
                        system.debug('Not Authorized');
                        FC.addError('Commenting to this group is limited to group owners and managers.');
                    }
                }
            }
        }
        else if (trigger.isAfter)
        {
            Set < String > mentionedUsers = new Set < String > ();
            Map < String, FeedComment > feedComments = new Map < String, FeedComment > ();
			Map < String, FeedComment > feedCommentObjMap = new Map < String, FeedComment >(); 

            for (FeedComment FI: trigger.new)
            {
                String feedItemId = FI.Id; //Get the feed item id
                String communityId = null;

                ConnectApi.Comment feedItem = ConnectApi.ChatterFeeds.getComment(communityId, feedItemId); //Get the feed item from ConnectApi

                List < ConnectApi.MessageSegment > messageSegments = feedItem.body.messageSegments; //Get the feed item message segments

                //For each segment in the feed item...
                for (ConnectApi.MessageSegment messageSegment: messageSegments)
                {
                    //If the segment is a mention...
                    if (messageSegment.text.Contains('@'))
                    {
                        //Add the mentioned user to the mentionedUsersMap(userid, username)
                        mentionedUsers.add(messageSegment.text.remove('@'));
                        feedComments.put(messageSegment.text.remove('@'), FI);

                    }
                }
                
                // Save posts containing file sharing for further evaluation
				if(FI.RelatedRecordId != null){
					feedCommentObjMap.put(FI.Id, FI);
					RelatedRecordIdSet.add(FI.RelatedRecordId);
				}
                system.debug('\n @@@ [FeedComment] -- RelatedRecordId='+FI.RelatedRecordId+' \n');
                
            }

            Boolean isAuthorizedGroupMemberMention = true;
            FeedComment feedComment = new FeedComment();
            system.debug(mentionedUsers);
            for (CollaborationGroupMember cgm: cgmList)
            {
                if (mentionedUsers.contains(cgm.collaborationGroup.Name))
                {
                    if (cgm.memberId == userinfo.getUserId())
                    {
                        isAuthorizedGroupMemberMention = true;
                        feedComment = feedComments.get(cgm.collaborationGroup.Name);
                        break;
                    }
                    else
                    {
                        isAuthorizedGroupMemberMention = false;
                        feedComment = feedComments.get(cgm.collaborationGroup.Name);
                    }
                }
            }
            if (!isAuthorizedGroupMemberMention)
            { // Not Authorized CollaborationGroup Member
                feedComment.addError('Mentioning this group is limited to group owners and managers.');
                FeedComment comment = [Select Id From FeedComment Where Id = : feedComment.Id];
                delete comment;
            }
			
            // Evaluate posts containing files
            if(feedCommentObjMap!=null && !feedCommentObjMap.isEmpty() && ChatterUtilities.chatterFilesEvaluated == false && ChatterUtilities.isChatterEnhancedSecurity()==true)
            	ChatterUtilities.EvalFileContent(feedCommentObjMap, RelatedRecordIdSet);
            
        }
    }

}