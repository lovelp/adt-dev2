trigger CampaignTrigger on Campaign (before insert, before update) {
     if(trigger.isBefore && trigger.isInsert){
         CampaignTriggerHelper.processCampaignBeforeInsert(trigger.new);
     }
     
     if(trigger.isBefore && trigger.isUpdate){
         CampaignTriggerHelper.processCampaignBeforeUpdate(trigger.newMap, trigger.oldMap);
     }
}