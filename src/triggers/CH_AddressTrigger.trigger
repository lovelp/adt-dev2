trigger CH_AddressTrigger  on CH_Address__c (before Insert) {
    Map<String,CH_Address__c> CHAddrMap = new Map<String,CH_Address__c>();
    List<Address__c> addrToUpdate = new List<Address__c>();
    List<Address__c> addrToInsert = new List<Address__c>();
    for (CH_Address__c ch : Trigger.new){
        String orgStr = '';
        String stdStr = '';
        if (!String.isBlank(ch.Street__c)){
            String street = ch.Street__c.toUpperCase().trim();
            orgStr = street;
            stdStr = street;
        }
        if (!String.isBlank(ch.Street2__c)){
            String street2 = ch.Street2__c.toUpperCase().trim();
            orgStr += '+'+ street2;
            stdStr += ', '+ street2;
        }
        if (!String.isBlank(ch.City__c)){
            String city = ch.City__c.toUpperCase().trim();
            orgStr += '+'+ city;
            stdStr += ', '+ city;
        }
        if (!String.isBlank(ch.State__c)){
            String state = ch.State__c.toUpperCase().trim();
            orgStr += '+'+ state;
            stdStr += ', '+ state;
        }
        if (!String.isBlank(ch.PostalCode__c)){
            String zip = ch.PostalCode__c.toUpperCase().trim();
            orgStr += '+'+ zip;
            stdStr += ', '+ zip;
        }
        ch.OriginalAddress__c = orgStr;
        ch.StandardizedAddress__c = stdStr;
        
        String streetName = '';
        if (!String.isBlank(ch.STR_PRE__c)){
            streetName = ch.STR_PRE__c.trim();
        }
        if (!String.isBlank(ch.STR_NAME__c)){
            if (!String.isBlank(streetName))
                streetName += ' ' + ch.STR_NAME__c.trim();
            else
                streetName = ch.STR_NAME__c.trim();
        }
        if (!String.isBlank(ch.STR_SUFFIX__c)){
            streetName +=  ' ' + ch.STR_SUFFIX__c.trim();
        }
        ch.StreetName__c = streetName;
        ch.CountryCode__c = 'US';
                
        CHAddrMap.put(orgStr,ch);
    }

    for (Address__c addr:[Select OriginalAddress__c, CHS__c from Address__c where OriginalAddress__c in:CHAddrMap.keySet()]){
        if (CHAddrMap.containsKey(addr.OriginalAddress__c)){
            CHAddrMap.get(addr.OriginalAddress__c).Address_Match__c = true;
            //addr.CHS__c = true;
            //addrToUpdate.add(addr);
        }
    }
    /*
    for (CH_Address__c chs : Trigger.new){
        if(!chs.Address_Match__c && !String.isBlank(chs.Street__c)){
            Address__c addres = new Address__c();
            addres.Street__c = chs.Street__c;
            if (!String.isBlank(chs.Street2__c))
                addres.Street2__c =  chs.Street2__c;
            if (!String.isBlank(chs.City__c))
                addres.City__c = chs.City__c;
            if (!String.isBlank(chs.State__c))
                addres.State__c = chs.State__c;
            if (!String.isBlank(chs.PostalCode__c))
                addres.PostalCode__c = chs.PostalCode__c;
            if (!String.isBlank(chs.PostalCodeAddOn__c))
                addres.PostalCodeAddOn__c = chs.PostalCodeAddOn__c;
            if (!String.isBlank(chs.StreetNumber__c))
                addres.StreetNumber__c = chs.StreetNumber__c;
            
            addres.StreetName__c = chs.StreetName__c;
            addres.CountryCode__c = chs.CountryCode__c;
            addres.StandardizedAddress__c = chs.StandardizedAddress__c;
            addres.OriginalAddress__c = chs.OriginalAddress__c;
            addres.CHS__c = true;
            addrToInsert.add(addres);
        }
    }

    // If you specify false as a parameter and a record fails, the remainder of the DML operation can still succeed.
    // The CH Address record is still inserted/updated
    Database.update(addrToUpdate, false);

    Database.insert(addrToInsert, false);
    */
}