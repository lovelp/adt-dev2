/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : Account Affiliation trigger that executes after the insert, update, delete operation
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Kalpesh Bhirud              4/17/2018          - Origininal Version
*
*                                                
*/
trigger AccountAffiliationTrigger on Account_Affiliation__c (after insert, after update, after delete) {
    
    AccountAffiliationTriggerHelper helper = new AccountAffiliationTriggerHelper();
    
      /* After Insert */
      if(Trigger.isInsert && Trigger.isAfter){
        helper.OnAfterInsert(Trigger.new);
      }
      /* After Update */
      else if(Trigger.isUpdate && Trigger.isAfter){
        helper.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
      }
      /* After Delete */
      else if(Trigger.isDelete && Trigger.isAfter){
        helper.onAfterDelete(Trigger.old, Trigger.oldMap);  
      }
    
}