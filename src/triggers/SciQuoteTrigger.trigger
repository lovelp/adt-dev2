/************************************* MODIFICATION LOG ********************************************************************************
*
* DESCRIPTION : SciQuote trigger that executes before or after the insert, update, delete, merge, upsert and undelete operation
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
* Mike Tuckman                  11/20/2013          - Origininal Version
*                                                
*/


trigger SciQuoteTrigger on scpq__SciQuote__c (before insert, before update, after insert, after update) 
{
        //----------------------------------------------
        //  BEFORE
        //----------------------------------------------
        if(trigger.isBefore){
            
            // INSERT
            if(trigger.isInsert){
                SciQuoteTriggerHelper.ProcessSciQuoteBeforeInsert(trigger.new);
            }
            
            // UPDATE
            if(trigger.isUpdate){
                SciQuoteTriggerHelper.ProcessSciQuotesBeforeUpdate(trigger.newMap, trigger.oldMap);
            }
            
        }
        
        //----------------------------------------------
        //  AFTER
      //----------------------------------------------
        if(trigger.isAfter){
          
            // INSERT
            if(trigger.isInsert){
                SciQuoteTriggerHelper.ProcessSciQuotesAfterInsert(trigger.new);
            }
            
          // UPDATE
        if(trigger.isUpdate){
                SciQuoteTriggerHelper.ProcessSciQuotesAfterUpdate(trigger.newMap, trigger.oldMap);
            }
        }
      
}