trigger CallDataTrigger on Call_Data__c (after Update) {

  if(Trigger.isUpdate){
    CallDataTriggerHelper.ProcessAccountDNISUpdate(Trigger.new);
  }   
  
}