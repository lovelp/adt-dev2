/************************************* MODIFICATION LOG ********************************************************************************
* TerritoryAssignmentTrigger
*
* DESCRIPTION : Trigger on custom Territory Assignment object that executes before and after insert, after update and after delete
*
*---------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE                  REASON
*---------------------------------------------------------------------------------------------------------------------------------------
*                            10/9/2011           - Original Version
*
*                                                
*/
trigger TerritoryAssignmentTrigger on TerritoryAssignment__c (after delete, after insert, after update, before insert) {
	
	TerritoryAssignmentTriggerHelper TAHelper = new TerritoryAssignmentTriggerHelper();
	//When a territory assignment is deleted
	if(Trigger.isAfter && Trigger.isDelete)
	{
		TAHelper.RemoveTerritoryAssociation(Trigger.Old);
	}
	
	//When a territory ownership is changed
	if(Trigger.isAfter && Trigger.isUpdate)
	{
		TAHelper.ChangeTerritoryAssociation(Trigger.Old, Trigger.New);	
	}
	
	//When a territory assignment is created
	if(Trigger.isAfter && Trigger.isInsert)
	{
		TAHelper.AddTerritoryAssociation(Trigger.New);
	}
	
	//when data is loaded from the migration process
	if(Trigger.isBefore && Trigger.isInsert)
	{
		TAHelper.relateTerritoryAssignmentToTerritory(Trigger.New);
	}

}