/************************************* MODIFICATION LOG ********************************************************************************************
* EventTrigger
*
* DESCRIPTION : Trigger on the Event object that executes before insert and update operations
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER						DATE				  REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Sahil Grover					3/26/2011			- Original Version
*
*													
*/

trigger EventTrigger on Event (before insert, before update, after insert) {
	if (Trigger.isBefore)
	{
		if (Trigger.isInsert) {
			EventTriggerHelper.processBeforeInsertEvents(Trigger.new);
		}
		else if (Trigger.isUpdate) {
			EventTriggerHelper.processBeforeUpdateEvents(Trigger.new, Trigger.oldMap);
		}
	}
	if ( Trigger.isAfter && Trigger.isInsert ){
	    EventTriggerHelper.processAfterInsertEvent(Trigger.newMap);
	}
	
}