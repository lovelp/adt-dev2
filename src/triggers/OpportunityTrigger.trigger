/************************************* MODIFICATION LOG ********************************************************************************************
 * OpportunityTrigger
 *
 * DESCRIPTION : Trigger on Opportunity object
 *               
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * DEVELOPER                        DATE              TICKET            REASON
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 * Jitendra Kothari                 07/24/2019         HRM-         CD1 - Link disposition to Opportunities as well
 */
trigger OpportunityTrigger on Opportunity (after update, after insert) {
	if(trigger.isAfter && trigger.isUpdate){
		OpportunityTriggerHandler.processAfterUpdate(trigger.new, trigger.oldMap);
	}
	if(trigger.isAfter && trigger.isInsert){
		OpportunityTriggerHandler.processAfterUpdate(trigger.new, null);
	}   
}